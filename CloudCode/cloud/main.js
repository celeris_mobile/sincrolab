///////////////// PACIENTE

Parse.Cloud.beforeSave("Paciente", function(request, response) {
	if(request.object.get("usuario")==null) {

		var user = new Parse.User();
        user.set("email", request.object.get("email"));
        user.set("username", request.object.get("email"));
        user.set("password", request.object.get("password"));
        user.set("tipo", "paciente");

        user.signUp(null, {
	        success: function(user) {
                request.object.set("usuario", user);
                request.object.set("password", null);
                response.success();
	        },
	        error: function(user, error) {	            
	            response.error(error);
	        }
    	});
 	} else {
 		response.success();	
 	}
});

Parse.Cloud.afterDelete("Paciente", function(request, response) {
    Parse.Cloud.useMasterKey();
    var objetos = new Array();
    objetos.push(request.object.get("datos"));
    objetos.push(request.object.get("usuario"));    
    if(request.object.get("cuestionario"))
        objetos.push(request.object.get("cuestionario"));
    if(request.object.get("diagnostico"))
        objetos.push(request.object.get("diagnostico"));
    if(request.object.get("perfilcognitivo"))
        objetos.push(request.object.get("perfilcognitivo"));
    
    Parse.Object.destroyAll(objetos).then(function(success) {

        var query = new Parse.Query("Entrenamiento");
        query.include("estadoEntrenamiento");
        query.equalTo("paciente", request.object);

        query.find().then(function(entrenamientos) {
            entrenamientos[0].get("estadoEntrenamiento").destroy({
                success: function(myObject) {
                    response.success(); 
                },
                error: function(myObject, error) {
                    response.error(error);
                }
            });
        });
    }, function(error) {
        console.error("Error deleting related datos paciente " + error.code + ": " + error.message);
        response.error(error);
    });
});

Parse.Cloud.afterDelete("EstadoEntrenamiento", function(request, response) {
    Parse.Object.destroyAll([request.object.get("estadoGongs"), request.object.get("estadoInvasion"), request.object.get("estadoPoker")]).then(function(success) {
        response.success();
    }, function(error) {
        console.error("Error deleting related partidas " + error.code + ": " + error.message);
        response.error(error);
    });
});

///////////////// ENTRENAMIENTO  ///SOLO A TRAVÉS DEL DASHBOARD
Parse.Cloud.afterDelete("Entrenamiento", function(request, response) {
    var query = new Parse.Query("DiaEntrenamiento");
    query.equalTo("entrenamiento", request.object);

    query.find().then(function(dias) {
        return Parse.Object.destroyAll(dias);
    }).then(function(success) {
        
        response.success();
    }, function(error) {
        console.error("Error deleting related dias " + error.code + ": " + error.message);
        response.error(error);
    });
});

///////////////// DIA ENTRENAMIENTO

Parse.Cloud.afterDelete("DiaEntrenamiento", function(request, response) {
    Parse.Object.destroyAll([request.object.get("partidaGongs"), request.object.get("partidaInvasion"), request.object.get("partidaPoker")]).then(function(success) {
        response.success();
    }, function(error) {
        console.error("Error deleting related partidas " + error.code + ": " + error.message);
        response.error(error);
    });
});

Parse.Cloud.afterDelete("PartidaGongs", function(request, response) {
    var query = new Parse.Query("ResultadoSesionGongs");
    query.equalTo("partida", request.object);

    query.find().then(function(sesiones) {
        return Parse.Object.destroyAll(sesiones);
    }).then(function(success) {
        response.success();
    }, function(error) {
        console.error("Error deleting related sesiones " + error.code + ": " + error.message);
        response.error(error);
    });
});

Parse.Cloud.afterDelete("PartidaInvasion", function(request, response) {
    var query = new Parse.Query("ResultadoSesionInvasion");
    query.equalTo("partida", request.object);

    query.find().then(function(sesiones) {
        return Parse.Object.destroyAll(sesiones);
    }).then(function(success) {
        response.success();
    }, function(error) {
        console.error("Error deleting related sesiones " + error.code + ": " + error.message);
        response.error(error);
    });
});

Parse.Cloud.afterDelete("PartidaPoker", function(request, response) {
    var query = new Parse.Query("ResultadoSesionPoker");
    query.equalTo("partida", request.object);

    query.find().then(function(sesiones) {
        return Parse.Object.destroyAll(sesiones);
    }).then(function(success) {
        response.success();
    }, function(error) {
        console.error("Error deleting related sesiones " + error.code + ": " + error.message);
        response.error(error);
    });
});