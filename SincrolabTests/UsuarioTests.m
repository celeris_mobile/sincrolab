//
//  PacienteTests.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Paciente.h"
#import "Terapeuta.h"

@interface UsuarioTests : XCTestCase

@end

@implementation UsuarioTests {
    Paciente* paciente;
    Terapeuta* terapeuta;
}

- (void)setUp
{
    [super setUp];
    terapeuta = [Terapeuta object];
    terapeuta.nombre = @"Dr. Nado";
    
    paciente = [Paciente creaConTerapeuta:terapeuta];
    paciente.email = @"odrakir@gmail.com";
    paciente.pass  = @"1234";
    paciente.nombre = @"Ricardo";
    paciente.apellidos = @"Sánchez Sotres";
}

- (void)tearDown
{
    paciente = nil;
    [super tearDown];
}

- (void) testSePuedeCrearPaciente {
    XCTAssertNotNil(
                    paciente,
                    @"El paciente se ha podido crear"
                    );
}

- (void) testEsDeTipoPaciente {
    XCTAssertTrue(
                  [paciente isKindOfClass:[Paciente class]],
                  @"Tiene que ser de tipo paciente");
}

- (void) testElPacienteAlmacenaLosDatos {
    XCTAssertEqualObjects(
                          paciente.email, @"odrakir@gmail.com",
                          @"El email debe almacenarse");
    XCTAssertEqualObjects(
                          paciente.pass, @"1234",
                          @"El password debe almacenarse");
    XCTAssertEqualObjects(
                          paciente.nombre, @"Ricardo",
                          @"El nombre debe almacenarse");
    XCTAssertEqualObjects(
                          paciente.apellidos, @"Sánchez Sotres",
                          @"Los apellidos deben almacenarse");
}

- (void) testTerapeutaTieneNombre {
    XCTAssertEqualObjects(
                          terapeuta.nombre,
                          @"Dr. Nado",
                          @"El terapeuta tiene que tener un nombre");
}

- (void) testPacienteTieneTerapeuta {
    XCTAssertEqualObjects(
                          paciente.terapeuta,
                          terapeuta,
                          @"El paciente tiene que tener un terapeuta asignado");
}

@end
