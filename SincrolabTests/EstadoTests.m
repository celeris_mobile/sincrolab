//
//  EstadoTests.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EstadoJuegoGongs.h"

@interface EstadoTests : XCTestCase

@end

@implementation EstadoTests {
    EstadoJuegoGongs* estadoGongs;
}

- (void)setUp
{
    [super setUp];

    estadoGongs = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:@"6" conAfectacion:@"1"];
}

- (void)tearDown
{

    estadoGongs = nil;
    [super tearDown];
}

- (void)testHitoInicialAsignado
{
   EstadoJuegoGongs* estadoIt6 = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:@"6" conAfectacion:@"1"];
    XCTAssertTrue(estadoIt6.hito!=nil, @"El hito tiene que ser el primero");
}

@end
