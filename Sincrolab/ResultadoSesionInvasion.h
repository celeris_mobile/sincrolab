//
//  ResultadoSesionInvasion.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//


@interface ResultadoSesionInvasion:NSObject
+ (ResultadoSesionInvasion *) resultadoConRespuestas:(NSArray *) respuestasPorTrials;
+ (ResultadoSesionInvasion *) resultadoAleatorio;

@property (nonatomic, strong) NSNumber* puntuacion;
@property (nonatomic, strong) NSNumber* duracion;

@property (nonatomic, strong) NSNumber* numeroDeAciertos;
@property (nonatomic, strong) NSNumber* numeroDeComisiones;
@property (nonatomic, strong) NSNumber* numeroDeOmisiones;
@property (nonatomic, strong) NSNumber* numeroDeNoComisiones;
@property (nonatomic, strong) NSNumber* porcentajeAciertos;
@property (nonatomic, strong) NSNumber* porcentajeOmisiones;
@property (nonatomic, strong) NSNumber* porcentajeComisiones;
@property (nonatomic, strong) NSNumber* porcentajeRecuperacion;
@property (nonatomic, strong) NSNumber* recuperacionMedia;
@property (nonatomic, strong) NSNumber* tiempoMedioDeRespuestaAciertos;
@property (nonatomic, strong) NSNumber* tiempoMedioDeRespuestaErrores;
//TODO mirar esto
@property (nonatomic, strong) NSNumber* tiempoMedioDeRespuestaErrorSS;
@property (nonatomic, strong) NSNumber* mediaDemoraStopSigns;
@property (nonatomic, strong) NSNumber* numeroDeStopSigns;
//TODO y esto
@property (nonatomic, strong) NSString* nombreHito;

@property (nonatomic, readonly) NSDictionary* valores;
@end
