//
//  PartidaPoker.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PartidaPoker.h"

@implementation PartidaPoker
@dynamic cambioNivel, sesiones;

+ (NSString *)parseClassName {
    return @"PartidaPoker";
}

- (NSNumber *)duracion {
    return [self.sesiones valueForKeyPath:@"@sum.duracion"];
}

- (NSNumber *)puntos {
    return [self.sesiones valueForKeyPath:@"@sum.puntuacion"];
}
@end