//
//  PacientesModel.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 14/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PacientesTerapeutaModel.h"
#import "Constantes.h"
#import "DiaEntrenamiento.h"
#import "DateUtils.h"
#import "ProgressHUD.h"
#import "DateUtils.h"

@interface PacientesTerapeutaModel()
@property (nonatomic, strong, readwrite) Terapeuta* terapeuta;

@property (nonatomic, strong, readwrite) NSArray* pacientes;
@property (nonatomic, strong, readwrite) NSArray* diasDeEntrenamientoSemana;

@property (nonatomic, strong) NSDate* semanaActual;
@end

@implementation PacientesTerapeutaModel {
    PFCachePolicy cachePolicy;
}
@synthesize terapeuta = _terapeuta;
@synthesize pacientes = _pacientes;
@synthesize diasDeEntrenamientoSemana = _diasDeEntrenamientoSemana;
@synthesize gruposDeEdad = _gruposDeEdad;
@synthesize sexos = _sexos;
@synthesize pacientesDeLaSemana = _pacientesDeLaSemana;
@synthesize entrenamientosPorDia = _entrenamientosPorDia;

- (id)initWithTerapeuta:(Terapeuta *)terapeuta {
    self = [super init];
    if (self) {
        _terapeuta = terapeuta;
        cachePolicy = kPFCachePolicyNetworkElseCache;
        
        [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(pacienteCreado:) name:kNuevoPacienteNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(pacienteBorrado:) name:kPacienteBorradoNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(pacienteModificado:) name:kPacienteModificadoNotification object:nil];

    }
    return self;
}

- (void) carga {
    if(self.pacientes)
        cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [ProgressHUD show:nil Interacton:NO];
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Paciente"];
    [query whereKey:@"terapeuta" equalTo:self.terapeuta];
    [query includeKey:@"datos"];
    [query includeKey:@"cuestionario"];
    [query includeKey:@"diagnostico"];
    [query includeKey:@"perfilcognitivo"];
    [query orderByAscending:@"nombre"];
    
    query.cachePolicy = cachePolicy;
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error);
        } else {
            _pacientes = objects;
            
            _numPacientesActivos = 0;
            for (Paciente* paciente in objects) {
                paciente.entrenamientoCargado = NO;
                
                if(paciente.diasSinJugar<=0)
                    _numPacientesActivos+=1;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kPacientesCargadosNotification object:self userInfo:nil];
            [ProgressHUD dismiss];
        }
    }];
}

- (void) cargaDiasEntrenamientoDeSemana:(NSDate *) lunesSemana {
    _semanaActual = lunesSemana;
    _pacientesDeLaSemana = nil;
    _entrenamientosPorDia = nil;
    
    NSDate *ultimoLunes = [DateUtils primerDiaDeLaSemanaParaFecha:[NSDate date]];

    PFCachePolicy cacheSemana;
    if([DateUtils comparaSinHoraFecha:ultimoLunes conFecha:lunesSemana]==NSOrderedSame)
        cacheSemana = cachePolicy;
    else
        cacheSemana = kPFCachePolicyCacheElseNetwork;
    
    [ProgressHUD show:nil Interacton:NO];
    
    //TODO Aquí estamos viendo todos los entrenamientos. Quizás debería ser solo el último
    PFQuery* queryEntrenamientos = [PFQuery queryWithClassName:@"Entrenamiento"];
    [queryEntrenamientos whereKey:@"paciente" containedIn:self.pacientes];
    queryEntrenamientos.cachePolicy = cacheSemana;
    
    PFQuery* query = [PFQuery queryWithClassName:@"DiaEntrenamiento"];
    [query whereKey:@"entrenamiento" matchesQuery:queryEntrenamientos];
    [query whereKey:@"fecha" greaterThanOrEqualTo:lunesSemana];
    [query includeKey:@"entrenamiento"];
    [query includeKey:@"entrenamiento.paciente"];
    [query setLimit:1000];
    [query orderByAscending:@"fecha"];
    query.cachePolicy = cacheSemana;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {

        for (Entrenamiento* entrenamiento in [objects valueForKeyPath:@"@distinctUnionOfObjects.entrenamiento"]) {
            for (Paciente* paciente in _pacientes) {
                if([paciente.objectId isEqualToString:entrenamiento.paciente.objectId]) {
                    entrenamiento.paciente = paciente;
                    paciente.entrenamientoCargado = NO;                    
                    paciente.entrenamiento = entrenamiento;
                }
            }
        }

        if(objects.count==0)
            _diasDeEntrenamientoSemana = nil;
        else
            _diasDeEntrenamientoSemana = objects.copy;

        if(cachePolicy==kPFCachePolicyNetworkElseCache)
            cachePolicy = kPFCachePolicyCacheElseNetwork;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kEntrenamientosSemanaCargadosNotification object:self userInfo:nil];
        [ProgressHUD dismiss];
    }];
}

- (void)cargaTodosLosDiasDeEntrenamiento:(void(^)(NSError*)) completado {
    
    [ProgressHUD show:nil Interacton:NO];
    
    //TODO Aquí estamos viendo todos los entrenamientos. Quizás debería ser solo el último
    PFQuery* queryEntrenamientos = [PFQuery queryWithClassName:@"Entrenamiento"];
    [queryEntrenamientos whereKey:@"paciente" containedIn:self.pacientes];
    queryEntrenamientos.cachePolicy = kPFCachePolicyCacheElseNetwork;
    
    PFQuery* query = [PFQuery queryWithClassName:@"DiaEntrenamiento"];
    [query whereKey:@"entrenamiento" matchesQuery:queryEntrenamientos];
    [query includeKey:@"entrenamiento"];
    [query includeKey:@"entrenamiento.paciente"];
    [query includeKey:@"partidaGongs"];
    [query includeKey:@"partidaInvasion"];
    [query includeKey:@"partidaPoker"];            
    [query setLimit:1000];
    [query orderByAscending:@"fecha"];
    query.cachePolicy = kPFCachePolicyCacheElseNetwork;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSLog(@"objects: %d", objects.count);
        
        for (Entrenamiento* entrenamiento in [objects valueForKeyPath:@"@distinctUnionOfObjects.entrenamiento"]) {
            for (Paciente* paciente in _pacientes) {
                if([paciente.objectId isEqualToString:entrenamiento.paciente.objectId]) {
                    paciente.entrenamientoCargado = NO;
                    entrenamiento.paciente = paciente;
                    paciente.entrenamiento = entrenamiento;
                }
            }
        }
        
        if(objects.count==0)
            _todosLosDias = nil;
        else
            _todosLosDias = objects.copy;
        
        [ProgressHUD dismiss];
        
        completado(error);
    }];
}
- (void) refresca {
    cachePolicy = kPFCachePolicyNetworkElseCache;
    _gruposDeEdad = nil;
    _sexos = nil;
    _entrenamientosPorDia = nil;
    [self carga];
}

- (NSArray *)pacientes {
    return _pacientes;
}

- (NSArray *)gruposDeEdad {
    if(!self.pacientes)
        return nil;
    
    if(!_gruposDeEdad) {
        NSArray* nacimientos = [self.pacientes valueForKeyPath:@"datos.edad"];
        
        NSMutableDictionary* grupos = [[NSMutableDictionary alloc] init];
        for (NSNumber *edad in nacimientos) {
            int gint = (int)floor(edad.intValue/3.0);
            gint = gint>5?5:gint;
            NSString* grupo = [NSString stringWithFormat:@"%d", gint];
            NSNumber* cantidadGrupo = [grupos objectForKey:grupo];
            if(cantidadGrupo) {
                cantidadGrupo = [NSNumber numberWithInt:cantidadGrupo.intValue+1];
                [grupos setObject:cantidadGrupo forKey:grupo];
            } else {
                [grupos setObject:[NSNumber numberWithInt:1] forKey:grupo];
            }
        }
        
        NSMutableArray* auxValores = [[NSMutableArray alloc] init];
        for (NSString* grupo in grupos) {
            NSNumber* cantidad = [grupos valueForKey:grupo];
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:cantidad, @"cantidad", grupo, @"grupo", nil];
            [auxValores addObject:dict];
        }
        
        NSArray* sortedValores =  [auxValores sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDictionary* dict1 = obj1;
            NSDictionary* dict2 = obj2;
            
            NSNumber* val1 = [dict1 objectForKey:@"grupo"];
            NSNumber* val2 = [dict2 objectForKey:@"grupo"];
            
            return [val1 compare:val2];
        }];
        
        _gruposDeEdad = sortedValores;
    }
    
    return _gruposDeEdad;
}

- (NSArray *)sexos {
    if(!self.pacientes)
        return nil;
    
    if(!_sexos) {
        NSNumber* varones = [self.pacientes valueForKeyPath:@"@sum.datos.sexo"];
        NSNumber* mujeres = [NSNumber numberWithInt:(int)self.pacientes.count-varones.intValue];
        _sexos = @[@{@"grupo":@"Varón", @"cantidad":varones}, @{@"grupo":@"Mujer", @"cantidad":mujeres}];
    }
    
    return _sexos;
}


- (NSArray *)pacientesDeLaSemana {
    if(!self.diasDeEntrenamientoSemana)
        return nil;
    if(!_pacientesDeLaSemana) {
        NSArray *flatArray = [self.entrenamientosPorDia valueForKeyPath: @"@unionOfArrays.self"];
        NSArray* activos = [flatArray valueForKeyPath:@"@distinctUnionOfObjects.entrenamiento.paciente.objectId"];
        NSSet* setActivos = [NSSet setWithArray:activos];

        NSMutableArray* pacientesSemana = [[NSMutableArray alloc] init];
        
        if(self.diasDeEntrenamientoSemana) {
            for (Paciente* paciente in self.pacientes) {
                if([setActivos containsObject:paciente.objectId]) {
                    [pacientesSemana addObject:paciente];
                }
            }
        }
        
        _pacientesDeLaSemana = pacientesSemana.copy;
    }
    
    return _pacientesDeLaSemana;
}

- (NSArray *) entrenamientosPorDia {
    if(!self.diasDeEntrenamientoSemana)
        return nil;
    if(!_entrenamientosPorDia) {
        NSCalendar *calendar = [NSCalendar currentCalendar];

        NSDateComponents *oneDay = [[NSDateComponents alloc] init];
        [oneDay setDay: 1];
        
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 6;
        
        NSDate* fechaFinal = [calendar dateByAddingComponents:dayComponent toDate:self.semanaActual options:0];
        
        NSMutableArray* auxDias = [[NSMutableArray alloc] init];
        for (NSDate* date = self.semanaActual; [date compare:fechaFinal] <= 0;
             date = [calendar dateByAddingComponents: oneDay
                                              toDate: date
                                             options: 0] ) {
                 NSMutableArray* auxEntrenamientos = [[NSMutableArray alloc] init];
                 for (DiaEntrenamiento* dia in self.diasDeEntrenamientoSemana) {
                     if([DateUtils comparaSinHoraFecha:dia.fecha conFecha:date]==NSOrderedSame) {
                         [auxEntrenamientos addObject:dia];
                     }
                 }
                 
                 [auxDias addObject:auxEntrenamientos.copy];
             }
        
        _entrenamientosPorDia = auxDias.copy;
        
    }
    return _entrenamientosPorDia;
}

#pragma mark Notificaciones

- (void) pacienteCreado:(NSNotification *) notification {
    /*/
     NSDictionary *dict = [notification userInfo];
     self.pacientes = [self.pacientes arrayByAddingObject:[dict objectForKey:@"paciente"]];
     [[NSNotificationCenter defaultCenter] postNotificationName:kPacientesCargadosNotification object:self userInfo:nil];
     /*/
    [self refresca];
    //*/
}

- (void) pacienteBorrado:(NSNotification *) notification {
    //*/
    NSDictionary *dict = [notification userInfo];
    Paciente* pacienteABorrar = [dict objectForKey:@"paciente"];
    NSMutableArray* pacientesMutable = self.pacientes.mutableCopy;
    [pacientesMutable removeObject:pacienteABorrar];
    self.pacientes = pacientesMutable.copy;
    
    NSMutableArray* mutableDias = self.diasDeEntrenamientoSemana.mutableCopy;
    for (DiaEntrenamiento* dia in self.diasDeEntrenamientoSemana) {
        if([dia.entrenamiento.paciente.objectId isEqualToString:pacienteABorrar.objectId])
            [mutableDias removeObject:dia];
    }
    self.diasDeEntrenamientoSemana = mutableDias.copy;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kPacientesCargadosNotification object:self userInfo:nil];
    /*/
     [self refresca];
     //*/
}

- (void) pacienteModificado:(NSNotification *) notification {
    //*/
    NSDictionary *dict = [notification userInfo];
    Paciente* pacienteModificado = [dict objectForKey:@"paciente"];
    
    NSInteger indice = [self.pacientes indexOfObject:pacienteModificado];
    NSMutableArray* auxPacientes = self.pacientes.mutableCopy;
    [auxPacientes replaceObjectAtIndex:indice withObject:pacienteModificado];
    
    NSDictionary* userDict = @{@"indice": [NSNumber numberWithInt:(int)indice]};
    [[NSNotificationCenter defaultCenter] postNotificationName:kPacienteActualizadoNotification object:self userInfo:userDict];
    /*/
     [self refresca];
     //*/
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNuevoPacienteNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPacienteBorradoNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPacienteModificadoNotification object:nil];
}

@end
