//
//  EntrenamientoManager.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Paciente.h"
#import "Entrenamiento.h"
#import "DiaEntrenamiento.h"

@interface EntrenamientosManager : NSObject
+ (Entrenamiento *) generaAutomaticoEntrenamientoParaPaciente:(Paciente *) paciente;
+ (Entrenamiento *) generaManualEntrenamientoParaPaciente:(Paciente *) paciente;
+ (Entrenamiento *) generaEntrenamientoParaPacienteAnonimo:(Paciente *) paciente;
+ (void) guardaEntrenamiento:(Entrenamiento*) entrenamiento inicial:(BOOL) inicial completado:(void (^)(NSError *))completado;
+ (NSArray *) calculaItinerarioParaPerfil:(PerfilCognitivoPaciente *) perfil;
+ (void) guardaDiaDeEntrenamiento:(DiaEntrenamiento*) dia dePaciente:(Paciente *) paciente completado:(void (^)(NSError *))completado;
@end
