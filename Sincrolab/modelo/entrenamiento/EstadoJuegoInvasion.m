//
//  EstadoJuegoInvasion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadoJuegoInvasion.h"
#import "Configuraciones.h"
#import "ConfiguracionInvasion.h"
#import "HitoInvasion.h"
#import "DDMathParser.h"
#import "ResultadoSesionInvasion.h"

@interface EstadoJuegoInvasion()
@end

@implementation EstadoJuegoInvasion {
    float p_inhibir;
}
@dynamic sesiones, trials, tee1, tee2, tie, dss, cambio, n_cambios, p_aparicion_ng, p_aparicion_ss, tipo_ss;

+ (NSString *)parseClassName {
    return @"EstadoJuegoInvasion";
}


+ (instancetype)estadoPorDefecto {
    EstadoJuegoInvasion* estado = [EstadoJuegoInvasion object];
    estado.itinerario = 0;
    estado.afectacion = 0;
    
    [estado configuraPorDefecto];
    
    return estado;
}

+ (instancetype) estadoParaHitoInicialdeItinerario:(NSString *) prioridadItinerario conAfectacion:(NSString *) afectacion {
    
    EstadoJuegoInvasion* estado = [EstadoJuegoInvasion object];
    estado.itinerario = prioridadItinerario;
    estado.afectacion = afectacion;
    
    if([estado configuraParaHito:1])
        return estado;
    
    return nil;
}

- (void)configuraPorDefecto {
    self.sesiones = @"4";
    self.trials = @"15";
    self.tee1 = @"1.0";
    self.tee2 = @"1.0";
    self.tie = @"1.5";
    self.dss = @"-0.1:0.05";
    self.cambio = @"1";
    [self setValue:@"3" forKey:@"n_cambios"];
    [self setValue:@"50:70" forKey:@"p_aparicion_ng"];
    [self setValue:@"50:70" forKey:@"p_aparicion_ss"];
    [self setValue:@"1" forKey:@"tipo_ss"];
}

- (BOOL) configuraParaHito:(int) numHito {
    
    ConfiguracionInvasion* confInvasion = (ConfiguracionInvasion*)[[Configuraciones sharedConf] confInvasion];
    
    HitoInvasion* hito = (HitoInvasion*)[confInvasion hito:numHito deItinerario:self.itinerario.intValue];
    
    if(!hito)
        return NO;
    
    self.itinerario = [hito getValor:@"itinerario"];
    self.hito = [hito getValor:@"hito"];
    
    
    self.sesiones = [hito getValor:@"sesiones"];
    self.trials = [hito getValor:@"trials"];
    self.tee1 = [hito getValor:@"tee1"];
    self.tee2 = [hito getValor:@"tee2"];
    self.tie = [hito getValor:@"tie"];
    self.dss = [hito getValor:@"dss"];
    self.cambio = [hito getValor:@"cambio"];
    [self setValue:[hito getValor:@"n_cambios"] forKey:@"n_cambios"];
    [self setValue:[hito getValor:@"p_aparicion_ng"] forKey:@"p_aparicion_ng"];    
    [self setValue:[hito getValor:@"p_aparicion_ss"] forKey:@"p_aparicion_ss"];
    [self setValue:[hito getValor:@"tipo_ss"] forKey:@"tipo_ss"];
    
    return YES;
}



- (EstadoJuegoCambio) compruebaCambioNivelConResultados:(NSArray *) resultados {
    
    //Recuperamos la configuración del juego
    ConfiguracionInvasion* confInvasion = (ConfiguracionInvasion*)[[Configuraciones sharedConf] confInvasion];
    
    
    //Este es el hito actual
    HitoInvasion* hito = (HitoInvasion*)[confInvasion hito:self.hito.intValue deItinerario:self.itinerario.intValue];
    
    int sesiones_ganadas = 0;
    int sesiones_perdidas = 0;
    BOOL sube = NO;
    BOOL baja = NO;
    
    float suma_p_inhibir = 0;
    
    for (ResultadoSesionInvasion* resultado in resultados) {
        
        //Comprueba subida
        if(resultado.porcentajeAciertos.intValue>=hito.porcent_aciertos_subida) {
            sesiones_ganadas += 1;
            if(sesiones_ganadas>=hito.n_sesiones_subida||resultados.count==1)
                sube = YES;
        } else
            sesiones_ganadas = 0;
        
        //Comprueba bajada
        if(resultado.porcentajeAciertos.intValue<=hito.porcent_aciertos_bajada) {
            sesiones_perdidas += 1;
            if(sesiones_perdidas>=hito.n_sesiones_bajada||resultados.count==1)
                baja = YES;
        } else
            sesiones_perdidas = 0;
        
        suma_p_inhibir += resultado.numeroDeNoComisiones.intValue/resultado.numeroDeStopSigns.floatValue;
    }
    
    p_inhibir = suma_p_inhibir/resultados.count;

    NSArray* condiciones_d1 = [hito.condicion_disparador1 componentsSeparatedByString:@"|"];
    if([self compruebaCondiciones:condiciones_d1]) {
        [self aplicaCambios:hito.disparador1 enHito:hito];
    }
    
    NSArray* condiciones_d2 = [hito.condicion_disparador2 componentsSeparatedByString:@"|"];
    if([self compruebaCondiciones:condiciones_d2]) {
        [self aplicaCambios:hito.disparador2 enHito:hito];
    }
    
    NSArray* condiciones_bd = [hito.condicion_disparador_bajada componentsSeparatedByString:@"|"];
    if([self compruebaCondiciones:condiciones_bd]) {
        [self aplicaCambios:hito.disparador_bajada enHito:hito];
    }
    
    if(sube) {
        if([self aplicaCambios:hito.subidas enHito:hito]) {
            if(![self configuraParaHito:self.hito.intValue+1])
                return EstadoJuegoCambioFinal;
        }
        
        return EstadoJuegoCambioSubeNivel;
    }
    
    if(baja) {
        if([self aplicaCambios:hito.bajadas enHito:hito])
            if(![self configuraParaHito:self.hito.intValue-1])
                return EstadoJuegoCambioSeMantiene;
        
        return EstadoJuegoCambioBajaNivel;
    }

    return EstadoJuegoCambioSeMantiene;
}


- (NSString *)description {
    NSArray* props = @[self.itinerario, self.hito, self.sesiones, self.trials, self.tee1, self.tee2, self.tie, self.dss, self.cambio, self.n_cambios, self.p_aparicion_ng, self.p_aparicion_ss, self.tipo_ss];
    
    return [props componentsJoinedByString:@", "];
}



- (NSString *) reemplazaVariables:(NSString *) ecuacion {
    
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"hito" withString:[self numberDePropiedad:self.hito].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"sesiones" withString:[self numberDePropiedad:self.sesiones].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"trials" withString:[self numberDePropiedad:self.trials].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tee1" withString:[self numberDePropiedad:self.tee1].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tee2" withString:[self numberDePropiedad:self.tee2].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tie" withString:[self numberDePropiedad:self.tie].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"dss" withString:[self numberDePropiedad:self.dss].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"cambio" withString:[self numberDePropiedad:self.cambio].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"n_cambios" withString:[self numberDePropiedad:self.n_cambios].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"p_aparicion_ng" withString:[self numberDePropiedad:self.p_aparicion_ng].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"p_aparicion_ss" withString:[self numberDePropiedad:self.p_aparicion_ss].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tipo_ss" withString:[self numberDePropiedad:self.tipo_ss].stringValue];
    
    
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"p_inhibir" withString:[NSString stringWithFormat:@"%f",p_inhibir]];
    
    return ecuacion;
}


@end
