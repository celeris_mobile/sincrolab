//
//  EstadoJuegoInvasion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import "EstadoJuego.h"
#import "ItinerarioSelectorJuegos.h"

typedef NS_ENUM(NSInteger, EstadoInvasionTipoCambio) {
    EstadoInvasionTipoCambioPorTrial,
    EstadoInvasionTipoCambioPorSesion,
    EstadoInvasionTipoCambioPorPartida
};

typedef NS_ENUM(NSInteger, EstadoInvasionTipoStopSign) {
    EstadoInvasionTipoStopSignAuditiva,
    EstadoInvasionTipoStopSignVisual
};

@interface EstadoJuegoInvasion : EstadoJuego

@property (retain) NSString* sesiones;
@property (retain) NSString* trials;
@property (retain) NSString* tee1;
@property (retain) NSString* tee2;
@property (retain) NSString* tie;
@property (retain) NSString* dss;
@property (retain) NSString* cambio;
@property (retain) NSString* n_cambios;
@property (retain) NSString* p_aparicion_ng;
@property (retain) NSString* p_aparicion_ss;
@property (retain) NSString* tipo_ss;

@end
