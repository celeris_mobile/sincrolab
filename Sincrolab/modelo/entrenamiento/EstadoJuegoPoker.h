//
//  EstadoJuegoCartas.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import "EstadoJuego.h"

@interface EstadoJuegoPoker : EstadoJuego

@property (nonatomic, strong) NSString* cartas;
@property (nonatomic, strong) NSString* jugadores;
@property (nonatomic, strong) NSString* categorias;
@property (nonatomic, strong) NSString* tie;
@property (nonatomic, strong) NSString* tresp;
@property (nonatomic, strong) NSString* ayudas;
@property (nonatomic, strong) NSString* tiempo_ayudas;
@property (nonatomic, strong) NSString* p_dianas;
@property (nonatomic, strong) NSString* n_cambios;

@end


