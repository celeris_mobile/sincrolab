//
//  Entrenamiento.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import "Paciente.h"
#import "EstadoEntrenamiento.h"

@interface Entrenamiento : PFObject <PFSubclassing>
@property (retain) NSDate* inicio;
@property (retain) NSString* perfilCognitivo;
@property (retain) Paciente* paciente;
@property (retain) EstadoEntrenamiento* estadoEntrenamiento;

@property (nonatomic, strong) NSMutableArray* diasDeEntrenamiento;

@property (nonatomic, readonly) NSDate* fechaSiguienteEntrenamiento;
- (NSDate *) fechaEntrenamientoDespuesDe:(NSDate *) fecha;


@end
