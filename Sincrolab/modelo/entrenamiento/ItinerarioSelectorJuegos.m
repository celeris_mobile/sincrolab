//
//  ItinerarioSelectorJuegos.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ItinerarioSelectorJuegos.h"

@implementation ItinerarioSelectorJuegos

- (int) valorParaJuego:(int) juego {
    return ((NSString *)[self.valoresPorJuego objectAtIndex:juego]).intValue;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%d: %@", self.prioridad.intValue, self.procesos];
}

@end
