//
//  ItinerarioSelectorJuegos.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItinerarioSelectorJuegos : NSObject

@property (nonatomic, strong) NSNumber * prioridad;
@property (nonatomic, strong) NSString * procesos;
@property (nonatomic, strong) NSArray * valoresPorJuego;
- (int) valorParaJuego:(int) juego;

@end
