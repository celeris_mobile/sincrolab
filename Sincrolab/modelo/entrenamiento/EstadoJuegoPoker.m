//
//  EstadoJuegoPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadoJuegoPoker.h"
#import "Configuraciones.h"
#import "ConfiguracionPoker.h"
#import "HitoPoker.h"
#import "DDMathParser.h"
#import "ResultadoSesionPoker.h"


@implementation EstadoJuegoPoker
@dynamic cartas, jugadores, categorias, tie, tresp, ayudas, tiempo_ayudas, p_dianas, n_cambios;

+ (NSString *)parseClassName {
    return @"EstadoJuegoPoker";
}

+ (instancetype)estadoPorDefecto {
    
    EstadoJuegoPoker* estado = [EstadoJuegoPoker object];
    estado.itinerario = 0;
    estado.afectacion = 0;
    
    [estado configuraPorDefecto];
    
    return estado;
}

+ (instancetype) estadoParaHitoInicialdeItinerario:(NSString *) prioridadItinerario conAfectacion:(NSString *) afectacion {
    
    EstadoJuegoPoker* estado = [EstadoJuegoPoker object];
    estado.itinerario = prioridadItinerario;
    estado.afectacion = afectacion;
    
    if([estado configuraParaHito:1])
        return estado;
    
    return nil;
}

- (void)configuraPorDefecto {
    self.cartas = @"10";
    self.jugadores = @"2";
    self.categorias = @"2";
    self.tie = @"3.0";
    self.tresp = @"1.5";
    self.ayudas = @"1";
    self.afectacion = @"1";
    [self setValue:@"0.0" forKey:@"tiempo_ayudas"];
    [self setValue:@"40:60" forKey:@"p_dianas"];
    [self setValue:@"4" forKey:@"n_cambios"];
}

- (BOOL) configuraParaHito:(int) numHito {
    
    ConfiguracionPoker* confPoker = (ConfiguracionPoker*)[[Configuraciones sharedConf] confPoker];
    
    HitoPoker* hito = (HitoPoker*)[confPoker hito:numHito deItinerario:self.itinerario.intValue];
    
    if(!hito)
        return NO;
    
    self.itinerario = [hito getValor:@"itinerario"];
    self.hito = [hito getValor:@"hito"];
    
    
    self.cartas = [hito getValor:@"cartas"];
    self.jugadores = [hito getValor:@"jugadores"];
    self.categorias = [hito getValor:@"categorias"];
    self.tie = [hito getValor:@"tie"];
    self.tresp = [hito getValor:@"tresp"];
    self.ayudas = [hito getValor:@"ayudas"];
    [self setValue:[hito getValor:@"tiempo_ayudas"] forKey:@"tiempo_ayudas"];
    [self setValue:[hito getValor:@"p_dianas"] forKey:@"p_dianas"];
    
    //El número de cambios se calcula con otros parametros
    NSString* ecuacion = [hito getValor:@"n_cambios"];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"," withString:@"."].lowercaseString;
    ecuacion = [self reemplazaVariables:ecuacion];
    NSExpression* expresion = [NSExpression expressionWithFormat:ecuacion, nil];
    NSNumber* result = [expresion expressionValueWithObject:nil context:nil];
    
    [self setValue:[NSString stringWithFormat:@"%@", result] forKey:@"n_cambios"];
    
    return YES;
}



- (EstadoJuegoCambio) compruebaCambioNivelConResultados:(NSArray *) resultados {
    
    //Recuperamos la configuración del juego
    ConfiguracionPoker* confPoker = (ConfiguracionPoker*)[[Configuraciones sharedConf] confPoker];
    
    
    //Este es el hito actual
    HitoPoker* hito = (HitoPoker*)[confPoker hito:self.hito.intValue deItinerario:self.itinerario.intValue];
    
    BOOL sube = NO;
    BOOL baja = NO;
    
    ResultadoSesionPoker* resultado = [resultados lastObject];

    if(resultado.porcentajeAciertos.intValue>=hito.porcent_aciertos_subida) {
        sube = YES;
    } else {
        if(resultado.porcentajeAciertos.intValue<=hito.porcent_aciertos_bajada) {
            baja = YES;
        }
    }
    
    if(sube) {
        if([self aplicaCambios:hito.subidas enHito:hito]) {
            if(![self configuraParaHito:self.hito.intValue+1])
                return EstadoJuegoCambioFinal;
        }
        
        return EstadoJuegoCambioSubeNivel;
    }

    if(baja) {
        if([self aplicaCambios:hito.bajadas enHito:hito])
            if(![self configuraParaHito:self.hito.intValue-1])
                return EstadoJuegoCambioSeMantiene;
        
        return EstadoJuegoCambioBajaNivel;
    }

    return EstadoJuegoCambioSeMantiene;
}


- (NSString *)description {
    NSArray* props = @[self.itinerario, self.hito, self.cartas, self.jugadores, self.categorias, self.tie, self.tresp, self.ayudas, self.tiempo_ayudas, self.p_dianas, self.n_cambios];
    
    return [props componentsJoinedByString:@", "];
}



- (NSString *) reemplazaVariables:(NSString *) ecuacion {
    
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"hito" withString:[self numberDePropiedad:self.hito].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"cartas" withString:[self numberDePropiedad:self.cartas].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"jugadores" withString:[self numberDePropiedad:self.jugadores].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"categorias" withString:[self numberDePropiedad:self.categorias].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tie" withString:[self numberDePropiedad:self.tie].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tresp" withString:[self numberDePropiedad:self.tresp].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"ayudas" withString:[self numberDePropiedad:self.ayudas].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tiempo_ayudas" withString:[self numberDePropiedad:self.tiempo_ayudas].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"p_dianas" withString:[self numberDePropiedad:self.p_dianas].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"n_cambios" withString:[self numberDePropiedad:self.n_cambios].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"n" withString:[self numberDePropiedad:self.afectacion].stringValue];

    return ecuacion;
}


@end
