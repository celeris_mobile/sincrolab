//
//  Entrenamiento.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Entrenamiento.h"
#import <Parse/PFObject+Subclass.h>
#import "DiaEntrenamiento.h"
#import "DateUtils.h"


@implementation Entrenamiento
@dynamic inicio, perfilCognitivo, paciente, estadoEntrenamiento;
@synthesize fechaSiguienteEntrenamiento = _fechaSiguienteEntrenamiento;
@synthesize diasDeEntrenamiento = _diasDeEntrenamiento;

+ (NSString *)parseClassName {
    return @"Entrenamiento";
}

- (NSDate *) fechaSiguienteEntrenamiento {
    if(!_fechaSiguienteEntrenamiento) {
        NSDate* hoy = [NSDate date];
        
        //Día de la semana de hoy
        NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents* components = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:hoy];
        NSInteger weekday = components.weekday;
        weekday-=2;
        if(weekday<0)
            weekday = 6;
        
        
        //Días en los que hay juegos
        int diasSemana = self.estadoEntrenamiento.diasSemana.intValue;
        
        //Hoy se debería jugar
        if((diasSemana&(int)pow(2, weekday))!=0) {
            if(self.diasDeEntrenamiento.count==0)
                return hoy;
            
            NSDate* ultimo = ((DiaEntrenamiento *)[self.diasDeEntrenamiento lastObject]).fecha;
            NSComparisonResult comparisonResult = [DateUtils comparaSinHoraFecha:hoy conFecha:ultimo];
            
            //No se ha jugado hoy
            if(comparisonResult!=NSOrderedSame) {
                return hoy;
            }
        }
            
        //Cuantos días quedan para jugar
        int diasASumar = 0;
        BOOL encontrado = NO;
        while (!encontrado && diasASumar<7) {
            weekday+=1;
            if(weekday>6)
                weekday = 0;
            
            diasASumar+=1;
            
            if((diasSemana&(int)pow(2, weekday))!=0)
                encontrado = YES;
        }
        
        //Fecha sumando esos días
        NSDateComponents* comp = [[NSDateComponents alloc] init];
        comp.day = diasASumar;
        NSDate* fechaFutura = [calendar dateByAddingComponents:comp toDate:[NSDate date] options:0];

        _fechaSiguienteEntrenamiento = fechaFutura;
    }
    
    return _fechaSiguienteEntrenamiento;
}


- (NSDate *) fechaEntrenamientoDespuesDe:(NSDate *) fecha {
    if(!_fechaSiguienteEntrenamiento) {
//        NSDate* hoy = [NSDate date];
        
        //Día de la semana de hoy
        NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents* components = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:fecha];
        NSInteger weekday = components.weekday;
        weekday-=2;
        if(weekday<0)
            weekday = 6;
        
        
        //Días en los que hay juegos
        int diasSemana = self.estadoEntrenamiento.diasSemana.intValue;
        
        //Hoy se debería jugar
        /*
        if((diasSemana&(int)pow(2, weekday))!=0) {
            if(self.diasDeEntrenamiento.count==0)
                return fecha;
            
            NSDate* ultimo = ((DiaEntrenamiento *)[self.diasDeEntrenamiento lastObject]).fecha;
            NSComparisonResult comparisonResult = [DateUtils comparaSinHoraFecha:fecha conFecha:ultimo];
            
            //No se ha jugado hoy
            if(comparisonResult!=NSOrderedSame) {
                return fecha;
            }
        }
         */
        
        //Cuantos días quedan para jugar
        int diasASumar = 0;
        BOOL encontrado = NO;
        while (!encontrado && diasASumar<7) {
            weekday+=1;
            if(weekday>6)
                weekday = 0;
            
            diasASumar+=1;
            
            if((diasSemana&(int)pow(2, weekday))!=0)
                encontrado = YES;
        }
        
        //Fecha sumando esos días
        NSDateComponents* comp = [[NSDateComponents alloc] init];
        comp.day = diasASumar;
        NSDate* fechaFutura = [calendar dateByAddingComponents:comp toDate:fecha options:0];
        
        _fechaSiguienteEntrenamiento = fechaFutura;
    }
    
    return _fechaSiguienteEntrenamiento;
}


@end
