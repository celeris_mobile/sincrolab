//
//  EstadoJuegoGongs.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ItinerarioSelectorJuegos.h"
#import "EstadoJuego.h"
#import <Parse/Parse.h>

typedef NS_ENUM(NSInteger, EstadoGongsModalidad) {
    EstadoGongsModalidadVisualA,
    EstadoGongsModalidadVisualB,
    EstadoGongsModalidadAuditivaA,
    EstadoGongsModalidadAuditivaB,
    EstadoGongsModalidadMedioVisualMedioAuditiva,
    EstadoGongsModalidadDualVisualyAuditiva,
    EstadoGongsModalidadDualVisual,
    EstadoGongsModalidadDualAuditiva,
    EstadoGongsModalidadTripleVisualVisualAuditiva,
    EstadoGongsModalidadTripleAuditivaAuditivaVisual
};

@interface EstadoJuegoGongs : EstadoJuego

@property (retain) NSString* dmin;
@property (retain) NSString* dmax;
@property (retain) NSString* elementos;
@property (retain) NSString* nback;
@property (retain) NSString* sesiones;
@property (retain) NSString* trials;
@property (retain) NSString* tie;
@property (retain) NSString* texp;
@property (retain) NSString* modalidad;
@property (retain) NSString* porcentaje_dianas;


@end
