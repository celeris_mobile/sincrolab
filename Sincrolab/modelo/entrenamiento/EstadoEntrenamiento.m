//
//  EstadoEntrenamiento.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadoEntrenamiento.h"
#import <Parse/PFObject+Subclass.h>
#import "Constantes.h"

@implementation EstadoEntrenamiento
@dynamic tipo, estadoGongs, estadoInvasion, estadoPoker;
@synthesize diasSemana = _diasSemana;

+ (NSString *)parseClassName {
    return @"EstadoEntrenamiento";
}

- (NSNumber *)diasSemana {
    if(!_diasSemana) {
        int diasTotal = 0;
        if(self.estadoGongs)
            diasTotal = diasTotal|self.estadoGongs.diasSemana.intValue;
        if(self.estadoInvasion)
            diasTotal = diasTotal|self.estadoInvasion.diasSemana.intValue;
        if(self.estadoPoker)
            diasTotal = diasTotal|self.estadoPoker.diasSemana.intValue;
        
        _diasSemana = [NSNumber numberWithInt:diasTotal];
    }
    
    return _diasSemana;
}

- (NSArray *)juegosDeHoy {
    
    //Día de la semana de hoy
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSInteger weekday = components.weekday;
    weekday-=2;
    if(weekday<0)
        weekday = 6;
    
    //Si no es hoy es que se ha perdido un día de juego, así que restamos días hasta encontrarlo
    int diasARestar = 0;
    BOOL encontrado = NO;
    while (!encontrado && diasARestar<7) {
        if((self.diasSemana.intValue&(int)pow(2, weekday))!=0)
            encontrado = YES;
        else {
            weekday-=1;
            if(weekday<0)
                weekday = 6;
            
            diasARestar+=1;
        }
    }
    
    NSMutableArray* juegos = [[NSMutableArray alloc] init];
    
    if((self.estadoGongs.diasSemana.intValue&(int)pow(2, weekday))!=0)
        [juegos addObject:[NSNumber numberWithInt:TipoJuegoGongs]];

    if((self.estadoInvasion.diasSemana.intValue&(int)pow(2, weekday))!=0)
        [juegos addObject:[NSNumber numberWithInt:TipoJuegoInvasion]];

    if((self.estadoPoker.diasSemana.intValue&(int)pow(2, weekday))!=0)
        [juegos addObject:[NSNumber numberWithInt:TipoJuegoPoker]];
    
    return juegos.copy;
}

@end
