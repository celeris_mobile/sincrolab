//
//  DiaEntrenamiento.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import "Entrenamiento.h"
#import "Paciente.h"
#import "Constantes.h"

#import "PartidaGongs.h"
#import "PartidaInvasion.h"
#import "PartidaPoker.h"

@interface DiaEntrenamiento : PFObject <PFSubclassing>
@property (retain) NSDate* fecha;

@property (retain) Entrenamiento* entrenamiento;

@property (retain) PartidaGongs* partidaGongs;
@property (retain) PartidaInvasion* partidaInvasion;
@property (retain) PartidaPoker* partidaPoker;

@property (nonatomic, readonly) NSNumber* cambioNivel;
@property (nonatomic, readonly) NSDate* fechaSinHora;
@property (nonatomic, readonly) NSNumber* duracion;
@end
