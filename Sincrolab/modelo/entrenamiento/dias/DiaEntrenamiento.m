//
//  DiaEntrenamiento.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DiaEntrenamiento.h"
#import <Parse/PFObject+Subclass.h>

@implementation DiaEntrenamiento
@dynamic entrenamiento, fecha, duracion, partidaGongs, partidaInvasion, partidaPoker;

+ (NSString *)parseClassName {
    return @"DiaEntrenamiento";
}

- (NSNumber *)cambioNivel {

    int nivel = 0;

    if(self.partidaGongs)
        nivel += self.partidaGongs.cambioNivel.intValue;
    if(self.partidaInvasion)
        nivel += self.partidaInvasion.cambioNivel.intValue;
    if(self.partidaPoker)
        nivel += self.partidaPoker.cambioNivel.intValue;

    return [NSNumber numberWithInt:nivel];
        
}

- (NSDate *)fechaSinHora {
    if( self.fecha == nil ) {
        return nil;
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.fecha];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}


- (NSNumber *)duracion {
    
    int duracion = 0;
    
    if(self.partidaGongs)
        duracion += self.partidaGongs.duracion.intValue;
    if(self.partidaInvasion)
        duracion += self.partidaInvasion.duracion.intValue;
    if(self.partidaPoker)
        duracion += self.partidaPoker.duracion.intValue;
    
    return [NSNumber numberWithInt:duracion];
    
}

@end
