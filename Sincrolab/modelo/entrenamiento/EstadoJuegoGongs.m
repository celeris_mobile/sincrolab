//
//  EstadoJuegoGongs.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadoJuegoGongs.h"
#import <CoreData/CoreData.h>
#import "Paciente.h"
#import "Configuraciones.h"
#import "ConfiguracionGongs.h"
#import "ResultadoSesionGongs.h"
#import "HitoGongs.h"


@interface EstadoJuegoGongs()
@end

@implementation EstadoJuegoGongs

@dynamic dmin, dmax, elementos, nback, sesiones, trials, tie, texp, modalidad, porcentaje_dianas;

+ (NSString *)parseClassName {
    return @"EstadoJuegoGongs";
}

+ (instancetype)estadoPorDefecto {
    EstadoJuegoGongs* estado = [EstadoJuegoGongs object];
    estado.itinerario = 0;
    estado.hito = 0;
    
    [estado configuraPorDefecto];
    return estado;
}

+ (instancetype) estadoParaHitoInicialdeItinerario:(NSString *) prioridadItinerario conAfectacion:(NSString *) afectacion {
    
    EstadoJuegoGongs* estado = [EstadoJuegoGongs object];
    estado.itinerario = prioridadItinerario;
    estado.afectacion = afectacion;
    
    if([estado configuraParaHito:1])
        return estado;
    
    return nil;
}

- (void)configuraPorDefecto {
    self.dmin = @"40";
    self.dmax = @"60";
    self.elementos = @"8";
    self.nback = @"1";
    self.sesiones = @"4";
    self.trials = @"15";
    self.tie = @"2.5";
    self.texp = @"0.5";
    self.modalidad = @"4";
    [self setValue:@"40:60" forKey:@"porcentaje_dianas"];
}

- (BOOL) configuraParaHito:(int) numHito {
    
    Configuracion* confGongs = [[Configuraciones sharedConf] confGongs];
   
    HitoGongs* hito = (HitoGongs*)[confGongs hito:numHito deItinerario:self.itinerario.intValue];
    
    if(!hito)
        return NO;
    
    self.itinerario = [hito getValor:@"itinerario"];
    self.hito = [hito getValor:@"hito"];
    
    self.elementos = [hito getValor:@"elementos"];
    self.nback = [hito getValor:@"nback"];
    
    self.sesiones = [hito getValor:@"sesiones"];
    self.tie = [hito getValor:@"tie"];
    self.texp = [hito getValor:@"texp"];
    self.trials = [hito getValor:@"trials"];
    self.dmin = [hito getValor:@"dmin"];
    self.dmax = [hito getValor:@"dmax"];
    self.modalidad = [hito getValor:@"modalidad"];
    [self setValue:[hito getValor:@"porcentaje_dianas"] forKey:@"porcentaje_dianas"];
    
    return YES;
}

- (EstadoJuegoCambio) compruebaCambioNivelConResultados:(NSArray *) resultados {

    //Recuperamos la configuración del juego
    ConfiguracionGongs* confGongs = (ConfiguracionGongs*)[[Configuraciones sharedConf] confGongs];
    
    //Este es el hito actual
    HitoGongs* hito = (HitoGongs*)[confGongs hito:self.hito.intValue deItinerario:self.itinerario.intValue];
    
    int sesiones_ganadas = 0;
    int sesiones_perdidas = 0;
    BOOL sube = NO;
    BOOL baja = NO;
    BOOL disparador = NO;
        
    for (ResultadoSesionGongs* resultado in resultados) {
        
        //Comprueba disparador
        if(hito.media_tr_disparador) {
            if((resultado.tiempoMedioDeRespuestaAciertos.floatValue>hito.media_tr_disparador)&&
               (resultado.porcentajeAciertos.intValue>=hito.porcent_aciertos_subida)) {
                disparador = YES;
            } else
                disparador = NO;
        }
        
        //Comprueba subida
        if(resultado.porcentajeAciertos.intValue>=hito.porcent_aciertos_subida) {
            sesiones_ganadas += 1;
            if(sesiones_ganadas>=hito.n_sesiones_subida||resultados.count==1)
                sube = YES;
        } else
            sesiones_ganadas = 0;
        
        //Comprueba bajada
        if(resultado.porcentajeAciertos.intValue<=hito.porcent_aciertos_bajada) {
            sesiones_perdidas += 1;
            if(sesiones_perdidas>=hito.n_sesiones_bajada||resultados.count==1)
                baja = YES;
        } else
            sesiones_perdidas = 0;
    }
    
    if(disparador) {
        [self aplicaCambios:hito.disparador enHito:hito];
        return EstadoJuegoCambioSeMantiene;
    } else {
        
        if(sube) {
            if([self aplicaCambios:hito.subidas enHito:hito]) {
                if(![self configuraParaHito:self.hito.intValue+1])
                    return EstadoJuegoCambioFinal;
            }

            return EstadoJuegoCambioSubeNivel;
        }
        
        if(baja) {
            if([self aplicaCambios:hito.bajadas enHito:hito])
                if(![self configuraParaHito:self.hito.intValue-1])
                    return EstadoJuegoCambioSeMantiene;
            
            return EstadoJuegoCambioBajaNivel;
        }
    }
    
    return EstadoJuegoCambioSeMantiene;
}

- (NSString *)description {
    NSArray* props = @[self.itinerario, self.hito, self.dmin, self.dmax, self.elementos, self.nback, self.sesiones, self.trials, self.tie, self.texp, self.modalidad, self.porcentaje_dianas];
    
    return [props componentsJoinedByString:@", "];
}

- (NSString *) reemplazaVariables:(NSString *) ecuacion {
    
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"hito" withString:[self numberDePropiedad:self.hito].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"dmin" withString:[self numberDePropiedad:self.dmin].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"dmax" withString:[self numberDePropiedad:self.dmax].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"elementos" withString:[self numberDePropiedad:self.elementos].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"nback" withString:[self numberDePropiedad:self.nback].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"sesiones" withString:[self numberDePropiedad:self.sesiones].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"trials" withString:[self numberDePropiedad:self.trials].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"tie" withString:[self numberDePropiedad:self.tie].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"texp" withString:[self numberDePropiedad:self.texp].stringValue];
    ecuacion = [ecuacion stringByReplacingOccurrencesOfString:@"porcentaje_dianas" withString:[self numberDePropiedad:self.porcentaje_dianas].stringValue];
    
    
    
    return ecuacion;
}

@end
