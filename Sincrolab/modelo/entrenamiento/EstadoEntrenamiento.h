//
//  EstadoEntrenamiento.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"

typedef NS_ENUM(NSInteger, TipoEntrenamiento) {
    TipoEntrenamientoAutomatico,
    TipoEntrenamientoManual
};

@interface EstadoEntrenamiento : PFObject <PFSubclassing>
@property (retain) NSNumber* tipo;

@property (retain) EstadoJuegoGongs* estadoGongs;
@property (retain) EstadoJuegoInvasion* estadoInvasion;
@property (retain) EstadoJuegoPoker* estadoPoker;

@property (nonatomic, readonly) NSNumber* diasSemana;
@property (nonatomic, readonly) NSArray* juegosDeHoy;
@end
