//
//  EntrenamientoPacientesModel.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Paciente.h"
#import "Entrenamiento.h"

@interface EntrenamientoPacienteModel : NSObject
- (id) initWithPaciente:(Paciente *) paciente;
- (void) carga:(void(^)(NSError*)) completado conProgreso:(BOOL) muestraProgreso;
- (NSArray *) tiempoJuegos;
@property (nonatomic, assign) BOOL cargado;
@property (nonatomic, readonly) Paciente* paciente;
@property (nonatomic, readonly) Entrenamiento* entrenamiento;
@property (nonatomic, readonly) NSArray* diasDeTodosLosEntrenamientos;
- (void) refresca;
- (void) cancela;
@end

