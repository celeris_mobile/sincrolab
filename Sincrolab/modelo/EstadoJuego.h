//
//  EstadoJuego.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Hito.h"

typedef NS_ENUM(NSInteger, EstadoJuegoCambio) {
    EstadoJuegoCambioSubeNivel,
    EstadoJuegoCambioBajaNivel,
    EstadoJuegoCambioSeMantiene,
    EstadoJuegoCambioFinal,
};

@interface EstadoJuego : PFObject  <PFSubclassing>
//@property (nonatomic, strong) NSDictionary* variables;

@property (retain) NSNumber* diasSemana;
@property (retain) NSString* itinerario;
@property (retain) NSString* hito;

@property (nonatomic, strong) NSString* afectacion;

+ (instancetype) estadoParaHitoInicialdeItinerario:(NSString *) prioridadItinerario conAfectacion:(NSString *) afectacion;
+ (instancetype) estadoPorDefecto;

- (void) configuraPorDefecto;
- (BOOL) configuraParaHito:(int) numHito;
- (EstadoJuegoCambio) compruebaCambioNivelConResultados:(NSArray *) resultados;

- (BOOL) compruebaCondiciones:(NSArray *) condiciones;
- (BOOL) aplicaCambios:(NSString*) cambios enHito:(Hito *) hito;
- (NSNumber *) numberDePropiedad:(NSString *) parametro;
@end
