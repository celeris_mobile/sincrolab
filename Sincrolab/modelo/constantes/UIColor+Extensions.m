//
//  UIColor+Extensions.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "UIColor+Extensions.h"

#define ARC4RANDOM_MAX  0x100000000

@implementation UIColor (Extensions)

+ (UIColor *)verdeAzulado {
    //return [UIColor colorWithRed:4/255.0 green:159/255.0 blue:107/255.0 alpha:1.0];
    return [UIColor colorWithRed:44/255.0 green:199/255.0 blue:147/255.0 alpha:1.0];
}

+ (UIColor *)azulOscuro {
    return [UIColor colorWithRed:45/255.0 green:55/255.0 blue:63/255.0 alpha:1.0];
}

+ (UIColor *) colorGrafica:(NSInteger) indice conAlpha:(float) alpha {
    UIColor * color;
    switch (indice) {
        case 0:
            color = [UIColor colorWithRed:115/255.0 green:218/255.0 blue:164/255.0 alpha:alpha];
            break;
        case 1:
            color = [UIColor colorWithRed:255/255.0 green:96/255.0 blue:56/255.0 alpha:alpha];
            break;
        case 2:
            color = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:25/255.0 alpha:alpha];
            break;
        case 3:
            color = [UIColor colorWithRed:255/255.0 green:203/255.0 blue:73/255.0 alpha:alpha];
            break;
        case 4:
            color = [UIColor colorWithRed:113/255.0 green:198/255.0 blue:210/255.0 alpha:alpha];
            break;
        case 5:
            color = [UIColor colorWithRed:185/255.0 green:208/255.0 blue:101/255.0 alpha:alpha];
            break;
        case 6:
            color = [UIColor colorWithRed:235/255.0 green:105/255.0 blue:21/255.0 alpha:alpha];
            break;
        default:
            color = [UIColor colorWithRed:(double)arc4random()/ARC4RANDOM_MAX green:(double)arc4random()/ARC4RANDOM_MAX blue:(double)arc4random()/ARC4RANDOM_MAX alpha:1.0];
            break;
    }
    
    return color;
}

+ (UIColor *) colorProceso:(Proceso) proceso {
    UIColor * color;
    switch (proceso) {
        case 0:
            color = [UIColor colorWithRed:111/255.0 green:214/255.0 blue:260/255.0 alpha:1.0];
            break;
        case 1:
            color = [UIColor colorWithRed:235/255.0 green:105/255.0 blue:121/255.0 alpha:1.0];
            break;
        case 2:
            color = [UIColor colorWithRed:80/255.0 green:184/255.0 blue:228/255.0 alpha:1.0];
            break;
        case 3:
            color = [UIColor colorWithRed:141/255.0 green:153/255.0 blue:200/255.0 alpha:1.0];
            break;
        case 4:
            color = [UIColor colorWithRed:114/255.0 green:199/255.0 blue:211/255.0 alpha:1.0];
            break;
        case 5:
            color = [UIColor colorWithRed:252/255.0 green:139/255.0 blue:112/255.0 alpha:1.0];
            break;
        case 6:
            color = [UIColor colorWithRed:186/255.0 green:209/255.0 blue:102/255.0 alpha:1.0];
            break;
        case 7:
            color = [UIColor colorWithRed:251/255.0 green:214/255.0 blue:120/255.0 alpha:1.0];
            break;
        default:
            color = [UIColor colorWithRed:(double)arc4random()/ARC4RANDOM_MAX green:(double)arc4random()/ARC4RANDOM_MAX blue:(double)arc4random()/ARC4RANDOM_MAX alpha:1.0];
            break;
    }
    
    return color;
}

+ (UIColor *)juegoVerdeOscuro {
    return [UIColor colorWithRed:22/255.0 green:106/255.0 blue:115/255.0 alpha:1.0];
}

+ (UIColor *)juegoVerdeClaro {
    return [UIColor colorWithRed:56/255.0 green:146/255.0 blue:156/255.0 alpha:1.0];
}

+ (UIColor *)juegoVerdeTexto {
    return [UIColor colorWithRed:123/255.0 green:190/255.0 blue:197/255.0 alpha:1.0];
}

+ (UIColor *)juegoMarronOscuro {
    return [UIColor colorWithRed:74/255.0 green:74/255.0 blue:80/255.0 alpha:1.0];
}

+ (UIColor *)juegoNaranja {
    return [UIColor colorWithRed:255/255.0 green:112/255.0 blue:75/255.0 alpha:1.0];
}

@end
