//
//  UIColor+Extensions.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, Proceso) {
    AtencionSostenida = 0,
    ControlInhibitorio = 1,
    MemoriaOperativa = 2,
    AtencionSelectiva = 3,
    FlexibilidadCognitiva = 4,
    VelocidadDeProcesamiento = 5,
    TomaDeDecisiones = 6,
    AtencionDividida = 7
};

@interface UIColor (Extensions)

+ (UIColor *) verdeAzulado;
+ (UIColor *) azulOscuro;
+ (UIColor *) colorGrafica:(NSInteger) indice conAlpha:(float) alpha;
+ (UIColor *) colorProceso:(Proceso) proceso;

+ (UIColor *) juegoVerdeOscuro;
+ (UIColor *) juegoVerdeClaro;
+ (UIColor *) juegoVerdeTexto;
+ (UIColor *) juegoMarronOscuro;
+ (UIColor *) juegoNaranja;
@end
