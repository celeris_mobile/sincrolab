 //
//  PacientesManager.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PacientesManager.h"
#import "Paciente.h"
#import "Constantes.h"
#import "ProgressHUD.h"

@interface PacientesManager()

@end

@implementation PacientesManager {
    PFCachePolicy cachePolicy;
}
/*

- (id) initWithTerapeuta:(Terapeuta *) terapeuta {
    self = [super init];
    if (self) {
        _terapeuta = terapeuta;
        cachePolicy = kPFCachePolicyNetworkElseCache;
    }
    return self;
}

- (NSString *)nombreTerapeuta {
    return self.terapeuta.nombre;
}

- (void) cargaPacientes {
    PFQuery *query = [PFQuery queryWithClassName:@"Paciente"];
    [query whereKey:@"terapeuta" equalTo:self.terapeuta];
    [query includeKey:@"datos"];
    [query includeKey:@"cuestionario"];
    [query includeKey:@"diagnostico"];
    [query includeKey:@"perfilcognitivo"];
    [query orderByAscending:@"nombre"];
    
    query.cachePolicy = cachePolicy;
    if(cachePolicy==kPFCachePolicyNetworkElseCache)
        cachePolicy = kPFCachePolicyCacheThenNetwork;

    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error);
        } else {
            _pacientes = objects;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificacionPacientesCargados" object:self userInfo:nil];
        }
    }];
}

- (void) refrescaPacientes {
    cachePolicy = kPFCachePolicyNetworkElseCache;
    [self cargaPacientes];
}

- (NSArray *)pacientes {
    return _pacientes;
}
*/
+ (void) nuevoPacienteDeTerapeuta:(Terapeuta *) terapeuta ConCampos:(NSDictionary*) campos completado:(void (^)(NSError *, Paciente *))completado {
    Paciente* paciente = [Paciente creaConTerapeuta:terapeuta];
    paciente.datos = [DatosPaciente object];
    paciente.nivel = [NSNumber numberWithInt:0];
    paciente.puntos = [NSNumber numberWithInt:0];

    for (NSString* ruta in campos) {
        [paciente setValue:[campos valueForKey:ruta] forKeyPath:ruta];
    }
    
    [ProgressHUD show:nil Interacton:NO];
    [paciente saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(error) {
            [ProgressHUD dismiss];
            NSDictionary *errorJSON =
            [NSJSONSerialization JSONObjectWithData: [[error.userInfo objectForKey:@"error"] dataUsingEncoding:NSUTF8StringEncoding]
                                            options: NSJSONReadingMutableContainers
                                              error: nil];
            
            //TO DO algunos errores deberíamos comprobarlos antes de enviar
            NSString* codigo =  [errorJSON objectForKey:@"code"];
            NSString* descripcion;
            switch (codigo.intValue) {
                case -1:
                    descripcion = @"Debe rellenar todos los campos.";
                    break;
                case 202:
                    descripcion = [NSString stringWithFormat:@"El email %@ ya existe en nuestra base de datos", paciente.email];
                    break;
                case 125:
                    descripcion = [NSString stringWithFormat:@"Email no válido %@", paciente.email];
                    break;
                default:
                    descripcion = [errorJSON objectForKey:@"message"];
                    break;
            }
            NSError* error = [NSError errorWithDomain:@"PacienteManagerError" code:codigo.intValue userInfo:[NSDictionary dictionaryWithObjectsAndKeys:descripcion, NSLocalizedDescriptionKey, nil]];
            completado(error, nil);
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNuevoPacienteNotification object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:paciente, @"paciente", nil]];
            
            [ProgressHUD showSuccess:@"Paciente creado"];
            completado(nil, paciente);
        }
    }];

}

+ (void) borraPaciente:(Paciente*) paciente completado:(void (^)(NSError *))completado {

    //TODO borrar en Cloud Code el resto de cosas (incluido el PFUser)
    [ProgressHUD show:nil Interacton:NO];
    [paciente deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(error) {
            [ProgressHUD dismiss];
            NSError* error = [NSError errorWithDomain:@"PacienteManagerError" code:0 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Por favor, inténtelo de nuevo más tarde", NSLocalizedDescriptionKey, nil]];
            completado(error);
            
        } else {
            [ProgressHUD showSuccess:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kPacienteBorradoNotification object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:paciente, @"paciente", nil]];
            
            completado(nil);
        }
    }];
}

+ (void) actualizaPaciente:(Paciente*) paciente conCampos:(NSDictionary*) campos completado:(void (^)(NSError *))completado {

    for (NSString* ruta in campos) {
        [paciente setValue:[campos valueForKey:ruta] forKeyPath:ruta];
    }
    
    [ProgressHUD show:nil Interacton:NO];
    [PFObject saveAllInBackground:@[paciente, paciente.datos] block:^(BOOL succeeded, NSError *error) {
        if(!succeeded) {
            [ProgressHUD dismiss];
            
            NSError* error = [NSError errorWithDomain:@"PacienteManagerError" code:0 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Por favor, inténtelo de nuevo más tarde", NSLocalizedDescriptionKey, nil]];
            completado(error);
        } else {
            [ProgressHUD showSuccess:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kPacienteModificadoNotification object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:paciente, @"paciente", nil]];

            completado(nil);
        }
    }];
}


@end
