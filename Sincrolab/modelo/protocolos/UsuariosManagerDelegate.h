//
//  UsuariosManagerDelegate.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"

@protocol UsuariosManagerDelegate <NSObject>

- (void) usuarioLoggedIn:(Usuario *) usuario;
- (void) usuarioLoggedOut;
- (void) errorLogeandoUsuario:(NSError *) error;

@end
