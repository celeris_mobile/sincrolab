//
//  PacientesTerapeutaDatasource.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 14/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Terapeuta.h"

@protocol PacientesTerapeutaDatasource <NSObject>

- (Terapeuta *) terapeuta;

- (NSNumber *) numPacientes;
- (NSArray *) pacientes;
- (NSArray *) gruposDeEdad;
- (NSArray *) sexos;
- (NSArray *) diasDeEntrenamiento;

- (void) refresca;
@end
