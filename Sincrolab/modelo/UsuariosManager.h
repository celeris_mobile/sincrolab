//
//  UsuariosManager.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UsuariosManagerDelegate.h"

@interface UsuariosManager : NSObject
@property (nonatomic, weak) id<UsuariosManagerDelegate> delegate;

- (BOOL) hayUsuarioLoggeado;
- (void) logOutUsuario;

@end
