//
//  EntrenamientoManager.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientosManager.h"
#import "Constantes.h"
#import "ProcesoAfectado.h"
#import "Entrenamiento.h"
#import "Configuraciones.h"
#import "ConfiguracionSelector.h"
#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"
#import "DiaEntrenamiento.h"
#import "PartidaGongs.h"
#import "PartidaInvasion.h"
#import "PartidaPoker.h"
#import "ResultadoSesionGongs.h"
#import "ResultadoSesionInvasion.h"
#import "ResultadoSesionPoker.h"
#import "ProgressHUD.h"

@implementation EntrenamientosManager

+ (NSString *) afectacionItinerarioPosible:(NSDictionary *) itinerario paraProcesosAfectados:(NSArray*) procesosAfectados {
        
    NSArray* procesos = [[itinerario valueForKey:@"PROCESOS"] componentsSeparatedByString:@"+"];
    NSMutableArray* afectaciones = [[NSMutableArray alloc] init];
    
    for (NSString* proceso in procesos) {
        NSArray* componentes = [proceso componentsSeparatedByString:@":"];
        NSString* nombre = [componentes objectAtIndex:0];
        NSString* niveles_str = [componentes objectAtIndex:1];
        NSArray* niveles = [niveles_str componentsSeparatedByString:@","];
                

        for (ProcesoAfectado* procesoPaciente in procesosAfectados) {
            NSString* afectacion = [NSString stringWithFormat:@"%d", procesoPaciente.afectacion.intValue];
            if([procesoPaciente.nombre.lowercaseString isEqualToString:nombre.lowercaseString] && [niveles indexOfObject:afectacion]!=NSNotFound) {
                [afectaciones addObject:procesoPaciente.afectacion];
                break;
            }
        }
        if(!afectaciones.count)
            return nil;
    }
    
    if(afectaciones.count==procesos.count)
        return [afectaciones componentsJoinedByString:@"+"];
    else
        return nil;
}

+ (NSArray *) ordenaItinerarios:(NSArray *) itinerarios conJuegos:(NSArray *) juegos {
    
    NSMutableArray* auxItinerarios = itinerarios.mutableCopy;
    
    [auxItinerarios sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDictionary* itinerario1 = obj1;
        NSDictionary* itinerario2 = obj2;
        
        int max1 = 0;
        int max2 = 0;
        for (NSNumber *juego in juegos) {
            NSString* nombreJuego = [Constantes shorNameJuego:juego.intValue].uppercaseString;
            float v1 = ((NSString *)[itinerario1 valueForKey:nombreJuego]).floatValue;
            float v2 = ((NSString *)[itinerario2 valueForKey:nombreJuego]).floatValue;
            max1 = MAX(max1, v1);
            max2 = MAX(max2, v2);
        }
        
        int prioridad1 = ((NSString *)[itinerario1 valueForKey:@"PRIORIDAD"]).intValue;
        int prioridad2 = ((NSString *)[itinerario2 valueForKey:@"PRIORIDAD"]).intValue;
        
        if(max1<max2)
            return NSOrderedDescending;
        if(max1>max2)
            return NSOrderedAscending;
        
        
        if(prioridad1>prioridad2)
            return NSOrderedDescending;
        if(prioridad1<prioridad2)
            return NSOrderedAscending;
        
        return NSOrderedSame;
    }];
    
    //Filtra los itinerarios que son 0 para todos los juegos
    NSMutableArray *filteredItinerarios = [[NSMutableArray alloc] init];
    for (NSDictionary* itinerario in auxItinerarios) {
        BOOL es_cero = YES;
        for (NSNumber *juego in juegos) {
            NSString* nombreJuego = [Constantes shorNameJuego:juego.intValue].uppercaseString;
            int valor = ((NSString *)[itinerario valueForKey:nombreJuego]).intValue;
            if(valor>0)
                es_cero = NO;
        }
        
        if(!es_cero)
           [filteredItinerarios addObject:itinerario];
    }
    
    return filteredItinerarios.copy;
}

+ (Entrenamiento *) generaAutomaticoEntrenamientoParaPaciente:(Paciente *) paciente {
    //TODO si estamos sobreescribiendo un entrenamiento existente habría que borrar el estado, ya no hará falta
    
    Entrenamiento* entrenamiento = [Entrenamiento object];
    entrenamiento.inicio = [NSDate date];
    entrenamiento.perfilCognitivo = paciente.perfilcognitivo.resumen;
    entrenamiento.paciente = paciente;
    
    EstadoEntrenamiento* estadoEntrenamiento = [EstadoEntrenamiento object];
    estadoEntrenamiento.tipo = [NSNumber numberWithInt:TipoEntrenamientoAutomatico];
    entrenamiento.estadoEntrenamiento = estadoEntrenamiento;

    NSMutableArray* juegosArray = [Constantes todosLosJuegos].mutableCopy;
    
    //Todos los itinerarios posibles para este paciente.
    NSMutableArray* auxItinerarios = [[NSMutableArray alloc] init];
    ConfiguracionSelector* selector = [[Configuraciones sharedConf] confSelector];
    for (NSDictionary* itinerario in selector.itinerarios) {
        NSString* afectaciones = [self afectacionItinerarioPosible:itinerario
                                             paraProcesosAfectados:paciente.perfilcognitivo.procesosAfectados];
        if(afectaciones) {
            [itinerario setValue:afectaciones forKey:@"AFECTACIONES"];
            [auxItinerarios addObject:itinerario];
        }
    }
    
    
    //Los ordenamos según el valor máximo de cada uno de los juegos
    NSArray* itinerarios = [self ordenaItinerarios:auxItinerarios.copy conJuegos:juegosArray];
    
    //Aquí irán los itinerarios de cada uno de los 3 juegos
    NSMutableArray* itinerariosJuegos = [[NSMutableArray alloc] initWithObjects:[NSNull null], [NSNull null], [NSNull null], nil];
    
    for (int j = 0; j<3; j++) {
        NSDictionary* itinerario;
        
        //Buscamos el siguiente itinerario más prioritario, que no haya sido usado ya
        itinerario = nil;
        for (NSDictionary* itinerarioPosible in itinerarios) {
            BOOL usado = NO;
            for (NSDictionary* itinerarioUsado in itinerariosJuegos) {
                if(itinerarioUsado!=(id)[NSNull null] && [[itinerarioUsado valueForKey:@"PROCESOS"] isEqualToString:[itinerarioPosible valueForKey:@"PROCESOS"]]) {
                    usado = YES;
                }
            }
            if(!usado) {
                itinerario = itinerarioPosible;
                break;
            }
        }
        
        //Si no hay ninguno, buscamos el más prioritario, aunque ya haya sido usado
        if(!itinerario) {
            itinerario = [itinerarios firstObject];
        }
        
        if(itinerario) {
            int max = 0;
            int j = 0;
            NSNumber* juegoAQuitar;
            //Para este itinerario, ¿Cuál es el juego con más valor? Lo localizamos, la añadimos a itinerariosJuegos y tachamos ese juego
            for (NSNumber* juego in juegosArray) {
                NSString* nombreJuego = [Constantes shorNameJuego:juego.intValue].uppercaseString;
                float valorParaJuego = ((NSString *)[itinerario valueForKey:nombreJuego]).floatValue;
                if (valorParaJuego>max) {
                    max = valorParaJuego;
                    j = juego.intValue;
                    juegoAQuitar = juego;
                }
            }
            [itinerariosJuegos setObject:itinerario atIndexedSubscript:j];
            [juegosArray removeObject:juegoAQuitar];
            
            //Volvemos a ordenar los itinerarios comparando los juegos restantes
            itinerarios = [self ordenaItinerarios:itinerarios conJuegos:juegosArray];
        }
    }
    
    
    int j = 0;
    int s = 0;
    for (NSDictionary* itinerario in itinerariosJuegos) {
        NSString* nombreJuego = [Constantes shorNameJuego:j].uppercaseString;
        if(itinerario!=(id)[NSNull null] && ![[itinerario valueForKey:nombreJuego] isEqualToString:@"0"]) {
            
            int tipoJuego = ((NSNumber *)[[Constantes todosLosJuegos] objectAtIndex:j]).intValue;
            
            switch (tipoJuego) {
                case TipoJuegoGongs: {
                    if(!estadoEntrenamiento.estadoGongs) {
                        EstadoJuegoGongs* estado = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:[itinerario valueForKey:@"PRIORIDAD"] conAfectacion:[itinerario valueForKey:@"AFECTACIONES"]];
                        estadoEntrenamiento.estadoGongs = estado;
                    } else {
                        NSString* itinerariostr = [itinerario valueForKey:@"PRIORIDAD"];
                        estadoEntrenamiento.estadoGongs.itinerario = itinerariostr;
                        [estadoEntrenamiento.estadoGongs configuraParaHito:1];
                    }
                    break;
                }
                case TipoJuegoInvasion: {
                    if(!estadoEntrenamiento.estadoInvasion) {
                        EstadoJuegoInvasion* estado = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:[itinerario valueForKey:@"PRIORIDAD"] conAfectacion:[itinerario valueForKey:@"AFECTACIONES"]];
                        estadoEntrenamiento.estadoInvasion = estado;
                    } else {
                        NSString* itinerariostr = [itinerario valueForKey:@"PRIORIDAD"];
                        estadoEntrenamiento.estadoInvasion.itinerario = itinerariostr;
                        [estadoEntrenamiento.estadoInvasion configuraParaHito:1];
                    }
                    break;
                }
                case TipoJuegoPoker: {
                    if(!estadoEntrenamiento.estadoPoker) {
                        EstadoJuegoPoker* estado = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:[itinerario valueForKey:@"PRIORIDAD"] conAfectacion:[itinerario valueForKey:@"AFECTACIONES"]];
                        estadoEntrenamiento.estadoPoker = estado;
                    } else {
                        NSString* itinerariostr = [itinerario valueForKey:@"PRIORIDAD"];
                        estadoEntrenamiento.estadoPoker.itinerario = itinerariostr;
                        [estadoEntrenamiento.estadoPoker configuraParaHito:1];
                    }
                    break;
                }
            }
            
            s+=1;
        }
        
        j++;
    }
    
    return entrenamiento;

}

+ (Entrenamiento *) generaManualEntrenamientoParaPaciente:(Paciente *) paciente {
    //TODO si estamos sobreescribiendo un entrenamiento existente habría que borrar el estado, ya no hará falta
    
    Entrenamiento* entrenamiento = [Entrenamiento object];
    entrenamiento.inicio = [NSDate date];
    entrenamiento.perfilCognitivo = paciente.perfilcognitivo.resumen;
    entrenamiento.paciente = paciente;
    
    EstadoEntrenamiento* estadoEntrenamiento = [EstadoEntrenamiento object];
    estadoEntrenamiento.tipo = [NSNumber numberWithInt:TipoEntrenamientoManual];
    entrenamiento.estadoEntrenamiento = estadoEntrenamiento;
    
    if(!estadoEntrenamiento.estadoGongs) {
        EstadoJuegoGongs* estado = [EstadoJuegoGongs estadoPorDefecto];
        estadoEntrenamiento.estadoGongs = estado;
    } else {
        [estadoEntrenamiento.estadoGongs configuraPorDefecto];
    }
    
    if(!estadoEntrenamiento.estadoInvasion) {
        EstadoJuegoInvasion* estado = [EstadoJuegoInvasion estadoPorDefecto];
        estadoEntrenamiento.estadoInvasion = estado;
    } else {
        [estadoEntrenamiento.estadoInvasion configuraPorDefecto];
    }
    
    if(!estadoEntrenamiento.estadoPoker) {
        EstadoJuegoPoker* estado = [EstadoJuegoPoker estadoPorDefecto];
        estadoEntrenamiento.estadoPoker = estado;
    } else {
        [estadoEntrenamiento.estadoPoker configuraPorDefecto];
    }
    
    return entrenamiento;
    
}

+ (Entrenamiento *) generaEntrenamientoParaPacienteAnonimo:(Paciente *) paciente {
    
    Entrenamiento* entrenamiento = [Entrenamiento object];
    entrenamiento.inicio = [NSDate date];
    entrenamiento.perfilCognitivo = @"-";
    entrenamiento.paciente = paciente;
    
    EstadoEntrenamiento* estadoEntrenamiento = [EstadoEntrenamiento object];
    estadoEntrenamiento.tipo = [NSNumber numberWithInt:TipoEntrenamientoAutomatico];
    entrenamiento.estadoEntrenamiento = estadoEntrenamiento;
    
    estadoEntrenamiento.estadoGongs = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:@"0" conAfectacion:@"1"];
    estadoEntrenamiento.estadoInvasion = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:@"6" conAfectacion:@"1"];
    estadoEntrenamiento.estadoPoker = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:@"10" conAfectacion:@"1"];
    
    estadoEntrenamiento.estadoGongs.diasSemana = [NSNumber numberWithInt:127];
    estadoEntrenamiento.estadoInvasion.diasSemana = [NSNumber numberWithInt:127];
    estadoEntrenamiento.estadoPoker.diasSemana = [NSNumber numberWithInt:127];
    
    return entrenamiento;
}

+ (void) guardaEntrenamiento:(Entrenamiento*) entrenamiento inicial:(BOOL) inicial completado:(void (^)(NSError *))completado
{
    [ProgressHUD show:nil];
    if(!inicial)
        entrenamiento.paciente.siguienteEntrenamiento = [entrenamiento fechaEntrenamientoDespuesDe:[NSDate date]];
    else {
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *components = [cal components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];

        [components setHour:-24];
        [components setMinute:0];
        [components setSecond:0];
        NSDate *yesterday = [cal dateByAddingComponents:components toDate:[NSDate date] options:0];

        entrenamiento.paciente.siguienteEntrenamiento = [entrenamiento fechaEntrenamientoDespuesDe:yesterday];
    }
    
    [entrenamiento saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!succeeded) {
            NSError* error = [NSError errorWithDomain:@"EntrenamientoError" code:0 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Por favor, inténtelo de nuevo más tarde", NSLocalizedDescriptionKey, nil]];
            completado(error);

        } else {
            [ProgressHUD dismiss];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNuevoEntrenamientoNotification object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:entrenamiento, @"entrenamiento", nil]];
            
            completado(nil);
        }
    }];
}

+ (void) guardaDiaDeEntrenamiento:(DiaEntrenamiento*) dia dePaciente:(Paciente *) paciente completado:(void (^)(NSError *))completado {
//    DiaEntrenamiento* dia = [entrenamiento.diasDeEntrenamiento lastObject];
    /*
    NSMutableArray* objects = [[NSMutableArray alloc] init];
    [objects addObject:dia];
    [objects addObjectsFromArray:dia.partidaGongs.sesiones];
    [objects addObjectsFromArray:dia.partidaInvasion.sesiones];
    [objects addObjectsFromArray:dia.partidaPoker.sesiones];
    
    [PFObject saveAllInBackground:objects block:^(BOOL succeeded, NSError *error) {
     */
    //[dia saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    [PFObject saveAllInBackground:@[dia, paciente] block:^(BOOL succeeded, NSError *error) {
        if(!succeeded) {
            NSError* error = [NSError errorWithDomain:@"EntrenamientoError" code:0 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Por favor, inténtelo de nuevo más tarde", NSLocalizedDescriptionKey, nil]];
            completado(error);
            
        } else {
            completado(nil);
        }
    }];
}

+ (NSArray *) calculaItinerarioParaPerfil:(PerfilCognitivoPaciente *) perfil {
    NSMutableArray* juegosArray = [Constantes todosLosJuegos].mutableCopy;
    
    //Todos los itinerarios posibles para este paciente.
    NSMutableArray* auxItinerarios = [[NSMutableArray alloc] init];
    ConfiguracionSelector* selector = [[Configuraciones sharedConf] confSelector];
    for (NSDictionary* itinerario in selector.itinerarios) {
        NSString* afectaciones = [self afectacionItinerarioPosible:itinerario
                                             paraProcesosAfectados:perfil.procesosAfectados];
        if(afectaciones) {
            [itinerario setValue:afectaciones forKey:@"AFECTACIONES"];
            [auxItinerarios addObject:itinerario];
        }
    }
    
    
    //Los ordenamos según el valor máximo de cada uno de los juegos
    NSArray* itinerarios = [self ordenaItinerarios:auxItinerarios.copy conJuegos:juegosArray];
    
    //Aquí irán los itinerarios de cada uno de los 3 juegos
    NSMutableArray* itinerariosJuegos = [[NSMutableArray alloc] initWithObjects:[NSNull null], [NSNull null], [NSNull null], nil];
    
    for (int j = 0; j<3; j++) {
        NSDictionary* itinerario;
        
        //Buscamos el siguiente itinerario más prioritario, que no haya sido usado ya
        itinerario = nil;
        for (NSDictionary* itinerarioPosible in itinerarios) {
            BOOL usado = NO;
            for (NSDictionary* itinerarioUsado in itinerariosJuegos) {
                if(itinerarioUsado!=(id)[NSNull null] && [[itinerarioUsado valueForKey:@"PROCESOS"] isEqualToString:[itinerarioPosible valueForKey:@"PROCESOS"]]) {
                    usado = YES;
                }
            }
            if(!usado) {
                itinerario = itinerarioPosible;
                break;
            }
        }
        
        //Si no hay ninguno, buscamos el más prioritario, aunque ya haya sido usado
        if(!itinerario) {
            itinerario = [itinerarios firstObject];
        }
        
        if(itinerario) {
            int max = 0;
            int j = 0;
            NSNumber* juegoAQuitar;
            //Para este itinerario, ¿Cuál es el juego con más valor? Lo localizamos, la añadimos a itinerariosJuegos y tachamos ese juego
            for (NSNumber* juego in juegosArray) {
                NSString* nombreJuego = [Constantes shorNameJuego:juego.intValue].uppercaseString;
                float valorParaJuego = ((NSString *)[itinerario valueForKey:nombreJuego]).floatValue;
                if (valorParaJuego>max) {
                    max = valorParaJuego;
                    j = juego.intValue;
                    juegoAQuitar = juego;
                }
            }
            [itinerariosJuegos setObject:itinerario atIndexedSubscript:j];
            [juegosArray removeObject:juegoAQuitar];
            
            //Volvemos a ordenar los itinerarios comparando los juegos restantes
            itinerarios = [self ordenaItinerarios:itinerarios conJuegos:juegosArray];
        }
    }
    
    
    NSMutableArray* returnArray = [[NSMutableArray alloc] init];
    
    int j = 0;
    int s = 0;
    for (NSDictionary* itinerario in itinerariosJuegos) {
        NSString* nombreJuego = [Constantes shorNameJuego:j].uppercaseString;
        if(itinerario!=(id)[NSNull null] && ![[itinerario valueForKey:nombreJuego] isEqualToString:@"0"]) {
            
            int tipoJuego = ((NSNumber *)[[Constantes todosLosJuegos] objectAtIndex:j]).intValue;
            
            switch (tipoJuego) {
                case TipoJuegoGongs: {
                    [returnArray addObject:[NSString stringWithFormat:@"G:%@", [itinerario valueForKey:@"PRIORIDAD"]]];
                    break;
                }
                case TipoJuegoInvasion: {
                    [returnArray addObject:[NSString stringWithFormat:@"I:%@", [itinerario valueForKey:@"PRIORIDAD"]]];
                    break;
                }
                case TipoJuegoPoker: {
                    [returnArray addObject:[NSString stringWithFormat:@"P:%@", [itinerario valueForKey:@"PRIORIDAD"]]];
                    break;
                }
            }
            
            s+=1;
        }
        
        j++;
    }
    
    
    return returnArray.copy;
}

@end
