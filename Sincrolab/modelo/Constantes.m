//
//  Constantes.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Constantes.h"


@implementation Constantes

NSString *const kPacientesCargadosNotification = @"NotificacionPacientesCargados";
NSString *const kEntrenamientosSemanaCargadosNotification = @"NotificacionEntrenamientosSemanaCargados";
NSString *const kEntrenamientoCargadoNotification = @"NotificacionEntrenamientoCargado";

NSString *const kPacienteBorradoNotification = @"NotificacionPacienteBorrado";
NSString *const kPacienteModificadoNotification = @"NotificacionPacienteModificado";
NSString *const kNuevoPacienteNotification = @"NotificacionPacienteNuevo";
NSString *const kPacienteActualizadoNotification = @"NotificacionPacienteActualizado";

NSString *const kNuevoEntrenamientoNotification = @"NotificacionNuevoEntrenamiento";

+ (NSString *) nombreDeJuego:(TipoJuego) tipoJuego {
    switch (tipoJuego) {
        case TipoJuegoGongs:
            return @"Togong";
            break;
        case TipoJuegoInvasion:
            return @"Invasion Samurai";
            break;
        case TipoJuegoPoker:
            return @"Poker Samurai";
            break;
    }
}

+ (NSString *) shorNameJuego:(TipoJuego) tipoJuego {
    switch (tipoJuego) {
        case TipoJuegoGongs:
            return @"Gongs";
            break;
        case TipoJuegoInvasion:
            return @"Invasion";
            break;
        case TipoJuegoPoker:
            return @"Poker";
            break;
    }
}

+ (UIImage *) miniaturaJuego:(TipoJuego) tipoJuego {
    switch (tipoJuego) {
        case TipoJuegoGongs:
            return [UIImage imageNamed:@"thumbjuego1.jpg"];
            break;
        case TipoJuegoInvasion:
            return [UIImage imageNamed:@"thumbjuego2.png"];
            break;
        case TipoJuegoPoker:
            return [UIImage imageNamed:@"thumbjuego3.png"];
            break;
    }
    
    return nil;
}


+ (NSArray *) todosLosJuegos {
    return [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:TipoJuegoGongs], [NSNumber numberWithInt:TipoJuegoInvasion], [NSNumber numberWithInt:TipoJuegoPoker], nil];    
}

@end
