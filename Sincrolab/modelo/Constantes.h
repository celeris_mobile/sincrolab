//
//  Constantes.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, TipoJuego) {
    TipoJuegoGongs,
    TipoJuegoInvasion,
    TipoJuegoPoker
};

#define aleatorio(smallNumber, bigNumber) ((((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * (bigNumber - smallNumber)) + smallNumber)

@interface Constantes : NSObject

extern NSString *const kPacientesCargadosNotification;
extern NSString *const kEntrenamientosSemanaCargadosNotification;
extern NSString *const kEntrenamientoCargadoNotification;

extern NSString *const kPacienteBorradoNotification;
extern NSString *const kPacienteModificadoNotification;
extern NSString *const kNuevoPacienteNotification;
extern NSString *const kPacienteActualizadoNotification;

extern NSString *const kNuevoEntrenamientoNotification;


+ (NSString *) nombreDeJuego:(TipoJuego) tipoJuego;
+ (NSString *) shorNameJuego:(TipoJuego) tipoJuego;
+ (UIImage *) miniaturaJuego:(TipoJuego) tipoJuego;

/*! Devuelve un array con la lista de juegos disponibles en la app
 @param none
 @return NSArray de objetos NSNumber
 */
+ (NSArray *) todosLosJuegos;

@end
