//
//  ConfiguracionSelector.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguracionSelector.h"

@implementation ConfiguracionSelector

- (NSArray *)itinerarios {
    return self.filasCSV;
}

@end
