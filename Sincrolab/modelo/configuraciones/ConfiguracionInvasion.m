//
//  ConfiguracionInvasion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguracionInvasion.h"
#import "HitoInvasion.h"

@implementation ConfiguracionInvasion


- (Hito *)configuraHitoConDictionary:(NSDictionary *)dict {
    HitoInvasion* hito = [[HitoInvasion alloc] init];
    
	/*ITINERARIO	HITO	SESIONES	TRIALS	TEE1	TEE2	TIE	DSS	CAMBIO	N_CAMBIOS	P_APARICION_NG	P_APARICION_SS	TIPO_SS	N_SESIONES_SUBIDA	PORCENT_ACIERTOS_SUBIDA	SUBIDAS	CONDICION_DISPARADOR1	DISPARADOR1	CONDICION_DISPARADOR2	DISPARADOR2	N_SESIONES_BAJADA	PORCENT_ACIERTOS_BAJADA	BAJADAS	CONDICION_DISPARADOR_BAJADA	DISPARADOR_BAJADA
     */
    
    [hito setParametro:@"itinerario" conCadena:[dict objectForKey:@"ITINERARIO"]];
    [hito setParametro:@"hito" conCadena:[dict objectForKey:@"HITO"]];
    [hito setParametro:@"sesiones" conCadena:[dict objectForKey:@"SESIONES"]];
    [hito setParametro:@"trials" conCadena:[dict objectForKey:@"TRIALS"]];
    [hito setParametro:@"tee1" conCadena:[dict objectForKey:@"TEE1"]];
    [hito setParametro:@"tee2" conCadena:[dict objectForKey:@"TEE2"]];
    [hito setParametro:@"tie" conCadena:[dict objectForKey:@"TIE"]];
    [hito setParametro:@"dss" conCadena:[dict objectForKey:@"DSS"]];
    [hito setParametro:@"cambio" conCadena:[dict objectForKey:@"CAMBIO"]];
    [hito setParametro:@"n_cambios" conCadena:[dict objectForKey:@"N_CAMBIOS"]];
    [hito setParametro:@"p_aparicion_ng" conCadena:[dict objectForKey:@"P_APARICION_NG"]];
    [hito setParametro:@"p_aparicion_ss" conCadena:[dict objectForKey:@"P_APARICION_SS"]];
    [hito setParametro:@"tipo_ss" conCadena:[dict objectForKey:@"TIPO_SS"]];
    
    
    hito.n_sesiones_subida = ((NSString *)[dict objectForKey:@"N_SESIONES_BAJADA"]).floatValue;
    hito.porcent_aciertos_subida = ((NSString *)[dict objectForKey:@"PORCENT_ACIERTOS_SUBIDA"]).floatValue;
    hito.subidas = ((NSString *)[dict objectForKey:@"SUBIDAS"]);
    hito.n_sesiones_bajada = ((NSString *)[dict objectForKey:@"N_SESIONES_BAJADA"]).floatValue;
    hito.porcent_aciertos_bajada = ((NSString *)[dict objectForKey:@"PORCENT_ACIERTOS_BAJADA"]).floatValue;
    hito.bajadas = ((NSString *)[dict objectForKey:@"BAJADAS"]);
    hito.condicion_disparador1 = ((NSString *)[dict objectForKey:@"CONDICION_DISPARADOR1"]);
    hito.disparador1 = ((NSString *)[dict objectForKey:@"DISPARADOR1"]);
    hito.condicion_disparador2 = ((NSString *)[dict objectForKey:@"CONDICION_DISPARADOR2"]);
    hito.disparador2 = ((NSString *)[dict objectForKey:@"DISPARADOR2"]);
    hito.condicion_disparador_bajada = ((NSString *)[dict objectForKey:@"CONDICION_DISPARADOR_BAJADA"]);
    hito.disparador_bajada = ((NSString *)[dict objectForKey:@"DISPARADOR_BAJADA"]);
    
    return hito;
}

@end
