//
//  Configuraciones.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Configuraciones.h"
#import <Parse/Parse.h>
#import "NSString+NSString_CSVParser.h"

@implementation Configuraciones

+ (id)sharedConf {
    static Configuraciones *configuraciones = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        configuraciones = [[self alloc] init];
    });
    return configuraciones;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

- (void) genera:(void (^)())completion {
    [self carga:^{
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray* selector = (NSArray *)[userDefaults objectForKey:@"selector"];
        _confSelector = [ConfiguracionSelector configuracionConArray:selector];
        
        NSArray* arrayGongs = (NSArray *)[userDefaults objectForKey:@"confGongs"];
        _confGongs = [ConfiguracionGongs configuracionConArray:arrayGongs];
        
        NSArray* arrayInvasion = (NSArray *)[userDefaults objectForKey:@"confInvasion"];
        _confInvasion = [ConfiguracionInvasion configuracionConArray:arrayInvasion];

        NSArray* arrayPoker = (NSArray *)[userDefaults objectForKey:@"confPoker"];
        _confPoker = [ConfiguracionPoker configuracionConArray:arrayPoker];
        
        completion();
    }];
}

- (void) carga:(void (^)())completion {

    PFQuery *query = [PFQuery queryWithClassName:@"Configuraciones"];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];

        NSDate* fechaServidor;
        
        NSMutableArray* docsACargar = [[NSMutableArray alloc] init];
        NSArray* docs = @[@"selector", @"confGongs", @"confInvasion", @"confPoker"];

        for (NSString* doc in docs) {
            NSDate* fechaUsuario = (NSDate *)[userDefaults objectForKey:[NSString stringWithFormat:@"fecha_%@", doc]];
            if(fechaUsuario) {
                fechaServidor = (NSDate *)[object valueForKey:[NSString stringWithFormat:@"fecha_%@", doc]];
                if([fechaServidor compare:fechaUsuario]==NSOrderedDescending)
                    [docsACargar addObject:doc];
            } else
                [docsACargar addObject:doc];
        }
        
        NSLog(@"docs a cargar: %@", docsACargar);
        if(docsACargar.count==0)
            completion();
        else {
            
            int __block d = 0;
            for (NSString* doc in docsACargar) {
                PFFile* csvFile = [object valueForKey:doc];
                [self cargaDocumento:csvFile deTipo:doc completion:^{
                    d++;
                    if(d==docsACargar.count)
                        completion();
                }];
            }
        }
    }];
}

- (void) cargaDocumento:(PFFile *) csvFile deTipo:(NSString *) tipo completion:(void (^)())completion {
    [csvFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (error) {
            NSLog(@"Error reading file.");
            NSLog(@"%@", error);
        } else {
            NSString* fileString = [NSString stringWithUTF8String:[data bytes]];
            NSMutableArray* csv = [fileString csvRows].mutableCopy;
            

            NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];

            [userDefaults setObject:[NSDate date] forKey:[NSString stringWithFormat:@"fecha_%@", tipo]];
            [userDefaults setObject:csv.copy forKey:tipo];
            [userDefaults synchronize];
            
            completion();
        }
    }];
}



@end
