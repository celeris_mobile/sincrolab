//
//  HitoPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 30/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "HitoPoker.h"
#import "ParametroHito.h"

@implementation HitoPoker

- (NSSet *)columnas {
    if (!_columnas) {
        _columnas = [NSSet setWithObjects:@"itinerario", @"hito", @"cartas", @"jugadores", @"categorias", @"tie", @"tresp", @"ayudas", @"tiempo_ayudas", @"p_dianas", @"n_cambios", nil];
    }
    
    return _columnas;
}

@end

/*ITINERARIO	HITO	CARTAS	JUGADORES	CATEGORIAS	TIE	TRES	AYUDAS	P_DIANAS	N_CAMBIOS	PORCENT_ACIERTOS_SUBIDA	SUBIDAS	PORCENT_ACIERTOS_BAJADA
 */