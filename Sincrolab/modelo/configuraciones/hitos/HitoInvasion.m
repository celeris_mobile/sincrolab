//
//  HitoInvasion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "HitoInvasion.h"
#import "ParametroHito.h"

@implementation HitoInvasion

- (NSSet *)columnas {
    if (!_columnas) {
        _columnas = [NSSet setWithObjects:@"itinerario", @"hito", @"sesiones", @"trials", @"tee1", @"tee2", @"tie", @"dss", @"cambio", @"n_cambios", @"p_aparicion_ng", @"p_aparicion_ss", @"tipo_ss", nil];
    }
    
    return _columnas;
}

@end