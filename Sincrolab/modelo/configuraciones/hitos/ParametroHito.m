//
//  ParametroHito.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ParametroHito.h"

@implementation ParametroHito
@synthesize rango = _rango;
@synthesize condiciones = _condiciones;

+ (ParametroHito *) parametroConCadena:(NSString *) cadena {

    cadena = [cadena stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    ParametroHito* parametro = [[ParametroHito alloc] init];
    NSArray* componentes = [cadena componentsSeparatedByString:@"|"];
    
    parametro.rango = [componentes objectAtIndex:0];

    if(componentes.count>1)
        parametro.condiciones = [componentes subarrayWithRange:NSMakeRange(1, componentes.count-1)];
    
    return parametro;
}

@end
