//
//  Hito.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hito : NSObject {
    @protected
    NSSet* _columnas;
}

@property (nonatomic, readonly) NSMutableDictionary *parametros;
@property (nonatomic, readonly) NSSet* columnas;
- (void) setParametro:(NSString *) nombre conCadena:(NSString *) cadena;
- (NSString *) getValor:(NSString *) nombre;
- (NSArray *) getCondiciones:(NSString *) nombre;

@end
