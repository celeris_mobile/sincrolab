//
//  Hito.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Hito.h"
#import "ParametroHito.h"

@implementation Hito {

}
@synthesize parametros = _parametros;

- (NSMutableDictionary *) parametros {
    if(!_parametros)
        _parametros = [[NSMutableDictionary alloc] init];
    
    return _parametros;
}

- (void) setParametro:(NSString *) nombre conCadena:(NSString *) cadena {
    if(![self.columnas containsObject:nombre]) {
        [NSException raise:@"No existe esa columna en el CSV" format:@"La columna %@ no existe", nombre];
    }
    
    ParametroHito* parametro = [ParametroHito parametroConCadena:cadena];
    [self.parametros setObject:parametro forKey:nombre];
    
}

- (NSString *) getValor:(NSString *) nombre {
    ParametroHito* parametro = [self.parametros objectForKey:nombre];
    
    if(parametro==nil)
        [NSException raise:@"No existe ese parametro" format:@"El parametro %@ no existe", nombre];
    
    return parametro.rango;
}

- (NSArray *) getCondiciones:(NSString *) nombre {
    ParametroHito* parametro = [self.parametros objectForKey:nombre];
    
    if(parametro==nil)
        [NSException raise:@"No existe ese parametro" format:@"El parametro %@ no existe", nombre];
    
    return parametro.condiciones;
}

@end
