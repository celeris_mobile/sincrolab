//
//  HitoGongs.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Hito.h"

@interface HitoGongs : Hito
@property (nonatomic, assign) float media_tr_disparador;
@property (nonatomic, retain) NSString * disparador;
@property (nonatomic, assign) float n_sesiones_subida;
@property (nonatomic, assign) float porcent_aciertos_subida;
@property (nonatomic, retain) NSString * subidas;
@property (nonatomic, assign) float n_sesiones_bajada;
@property (nonatomic, assign) float porcent_aciertos_bajada;
@property (nonatomic, retain) NSString * bajadas;
@end


