//
//  HitoPoker.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 30/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Hito.h"

@interface HitoPoker : Hito
@property (nonatomic, assign) CGFloat porcent_aciertos_subida;
@property (nonatomic, retain) NSString * subidas;
@property (nonatomic, assign) CGFloat porcent_aciertos_bajada;
@property (nonatomic, retain) NSString * bajadas;
@end

/*ITINERARIO	HITO	CARTAS POR JUGADOR	JUGADORES	CATEGORIAS	TIE	TRES	AYUDAS	P_DIANAS	N_CAMBIOS	PORCENT_ACIERTOS_SUBIDA	SUBIDAS	PORCENT_ACIERTOS_BAJADA BAJADAS
 */