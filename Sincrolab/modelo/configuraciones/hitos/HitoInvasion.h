//
//  HitoInvasion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Hito.h"

@interface HitoInvasion : Hito
@property (nonatomic, assign) CGFloat n_sesiones_subida;
@property (nonatomic, assign) CGFloat porcent_aciertos_subida;
@property (nonatomic, retain) NSString * subidas;
@property (nonatomic, assign) CGFloat n_sesiones_bajada;
@property (nonatomic, assign) CGFloat porcent_aciertos_bajada;
@property (nonatomic, retain) NSString * bajadas;
@property (nonatomic, retain) NSString* condicion_disparador1;
@property (nonatomic, retain) NSString* disparador1;
@property (nonatomic, retain) NSString* condicion_disparador2;
@property (nonatomic, retain) NSString* disparador2;
@property (nonatomic, retain) NSString* condicion_disparador_bajada;
@property (nonatomic, retain) NSString* disparador_bajada;
@end

/*ITINERARIO	HITO	SESIONES	TRIALS	TEE1	TEE2	TIE	DSS	CAMBIO	N_CAMBIOS	P_APARICION_NG	P_APARICION_SS	TIPO_SS	N_SESIONES_SUBIDA	PORCENT_ACIERTOS_SUBIDA	SUBIDAS	CONDICION_DISPARADOR1	DISPARADOR1	CONDICION_DISPARADOR2	DISPARADOR2	N_SESIONES_BAJADA	PORCENT_ACIERTOS_BAJADA	BAJADAS	CONDICION_DISPARADOR_BAJADA	DISPARADOR_BAJADA
*/