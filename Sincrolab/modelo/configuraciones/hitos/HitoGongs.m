//
//  HitoGongs.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "HitoGongs.h"
#import "ParametroHito.h"

@implementation HitoGongs

- (NSSet *)columnas {
    if (!_columnas) {
        _columnas = [NSSet setWithObjects:@"itinerario", @"hito", @"dmin", @"dmax", @"elementos", @"nback", @"sesiones", @"trials", @"tie", @"texp", @"modalidad", @"porcentaje_dianas", nil];
    }
    
    return _columnas;
}

@end
