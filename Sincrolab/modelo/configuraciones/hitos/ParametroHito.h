//
//  ParametroHito.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParametroHito : NSObject
+ (ParametroHito *) parametroConCadena:(NSString *) cadena;
@property (nonatomic, strong) NSString* rango;
@property (nonatomic, strong) NSArray* condiciones;
@end
