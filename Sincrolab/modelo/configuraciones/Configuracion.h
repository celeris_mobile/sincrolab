//
//  Configuracion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hito.h"

@interface Configuracion : NSObject
@property (nonatomic, strong) NSArray* filasCSV;
+ (instancetype) configuracionConArray:(NSArray *) array;
- (Hito *) hito:(int) numHito deItinerario:(int) numItinerario;
- (Hito *) configuraHitoConDictionary:(NSDictionary *)dict;

@end
