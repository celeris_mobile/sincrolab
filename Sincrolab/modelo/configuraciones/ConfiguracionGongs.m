//
//  ConfiguracionGongs.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguracionGongs.h"
#import "HitoGongs.h"

@implementation ConfiguracionGongs

- (Hito *)configuraHitoConDictionary:(NSDictionary *)dict {
    HitoGongs* hito = [[HitoGongs alloc] init];
    
    /*
	ITINERARIO	HITO	DMIN	DMAX	ELEMENTOS	NBACK	SESIONES	TRIALS	TIE	TEXP	MODALIDAD	PORCENTAJE_DIANAS	MEDIA_TR_DISPARADOR	 DISPARADOR	 N_SESIONES_SUBIDA	PORCENT_ACIERTOS_SUBIDA	SUBIDAS	N_SESIONES_BAJADA	PORCENT_ACIERTOS_BAJADA	BAJADAS
    */
    
    [hito setParametro:@"itinerario" conCadena:[dict objectForKey:@"ITINERARIO"]];
    [hito setParametro:@"hito" conCadena:[dict objectForKey:@"HITO"]];
    [hito setParametro:@"dmin" conCadena:[dict objectForKey:@"DMIN"]];
    [hito setParametro:@"dmax" conCadena:[dict objectForKey:@"DMAX"]];
    [hito setParametro:@"elementos" conCadena:[dict objectForKey:@"ELEMENTOS"]];
    [hito setParametro:@"nback" conCadena:[dict objectForKey:@"NBACK"]];
    [hito setParametro:@"sesiones" conCadena:[dict objectForKey:@"SESIONES"]];
    [hito setParametro:@"trials" conCadena:[dict objectForKey:@"TRIALS"]];
    [hito setParametro:@"tie" conCadena:[dict objectForKey:@"TIE"]];
    [hito setParametro:@"texp" conCadena:[dict objectForKey:@"TEXP"]];
    [hito setParametro:@"modalidad" conCadena:[dict objectForKey:@"MODALIDAD"]];
    [hito setParametro:@"porcentaje_dianas" conCadena:[dict objectForKey:@"PORCENTAJE_DIANAS"]];
    
    hito.media_tr_disparador = ((NSString *)[dict objectForKey:@"MEDIA_TR_DISPARADOR"]).floatValue;
    hito.disparador = [dict objectForKey:@"DISPARADOR"];
    hito.n_sesiones_subida = ((NSString *)[dict objectForKey:@"N_SESIONES_SUBIDA"]).floatValue;
    hito.porcent_aciertos_subida = ((NSString *)[dict objectForKey:@"PORCENT_ACIERTOS_SUBIDA"]).floatValue;
    hito.subidas = [dict objectForKey:@"SUBIDAS"];
    hito.n_sesiones_bajada = ((NSString *)[dict objectForKey:@"N_SESIONES_BAJADA"]).floatValue;
    hito.porcent_aciertos_bajada = ((NSString *)[dict objectForKey:@"PORCENT_ACIERTOS_BAJADA"]).floatValue;
    hito.bajadas = [dict objectForKey:@"BAJADAS"];

    return hito;
}


@end
