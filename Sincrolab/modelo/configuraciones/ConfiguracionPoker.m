//
//  ConfiguracionPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 30/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguracionPoker.h"
#import "HitoPoker.h"

@implementation ConfiguracionPoker

- (HitoPoker *)configuraHitoConDictionary:(NSDictionary *)dict {
    HitoPoker* hito = [[HitoPoker alloc] init];
    
    /*ITINERARIO	HITO	CARTAS POR JUGADOR	JUGADORES	CATEGORIAS	TIE	TRES	AYUDAS	P_DIANAS	N_CAMBIOS	PORCENT_ACIERTOS_SUBIDA	SUBIDAS	PORCENT_ACIERTOS_BAJADA
     */
    
    [hito setParametro:@"itinerario" conCadena:[dict objectForKey:@"ITINERARIO"]];
    [hito setParametro:@"hito" conCadena:[dict objectForKey:@"HITO"]];
    [hito setParametro:@"cartas" conCadena:[dict objectForKey:@"CARTAS"]];
    [hito setParametro:@"jugadores" conCadena:[dict objectForKey:@"JUGADORES"]];
    [hito setParametro:@"categorias" conCadena:[dict objectForKey:@"CATEGORIAS"]];
    [hito setParametro:@"tie" conCadena:[dict objectForKey:@"TIE"]];
    [hito setParametro:@"tresp" conCadena:[dict objectForKey:@"TRESP"]];
    [hito setParametro:@"ayudas" conCadena:[dict objectForKey:@"AYUDAS"]];
    [hito setParametro:@"tiempo_ayudas" conCadena:[dict objectForKey:@"TIEMPO_AYUDAS"]];
    [hito setParametro:@"p_dianas" conCadena:[dict objectForKey:@"P_DIANAS"]];
    [hito setParametro:@"n_cambios" conCadena:[dict objectForKey:@"N_CAMBIOS"]];
    
    
    hito.porcent_aciertos_subida = ((NSString *)[dict objectForKey:@"PORCENT_ACIERTOS_SUBIDA"]).floatValue;
    hito.subidas = ((NSString *)[dict objectForKey:@"SUBIDAS"]);
    hito.porcent_aciertos_bajada = ((NSString *)[dict objectForKey:@"PORCENT_ACIERTOS_BAJADA"]).floatValue;
    hito.bajadas = ((NSString *)[dict objectForKey:@"BAJADAS"]);
    
    return hito;
}

@end
