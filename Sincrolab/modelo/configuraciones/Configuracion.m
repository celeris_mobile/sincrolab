//
//  Configuracion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Configuracion.h"
#import "Hito.h"

@interface Configuracion()
@end

@implementation Configuracion

+ (instancetype) configuracionConArray:(NSArray *) array {

    Configuracion* configuracion = [[[self class] alloc] init];
    NSMutableArray* filas = [[NSMutableArray alloc] init];
    
    NSArray* nombres = [array objectAtIndex:0];
    for (int r = 1; r<array.count; r++) {
        NSArray* fila = [array objectAtIndex:r];
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        for (int c = 0; c<nombres.count; c++) {
            NSString* columna  = [nombres objectAtIndex:c];
            [dict setObject:[fila objectAtIndex:c] forKey:columna];
        }
        
        NSArray* itinerarios = [[dict valueForKey:@"ITINERARIO"] componentsSeparatedByString:@","];
        if(itinerarios.count>0) {
            for (NSString* itinerario in itinerarios) {
                NSMutableDictionary* nuevaFila = dict.mutableCopy;
                [nuevaFila setObject:itinerario forKey:@"ITINERARIO"];
                
                NSArray* hitos = [[nuevaFila valueForKey:@"HITO"] componentsSeparatedByString:@","];
                if(hitos.count>0) {
                    for (NSString* hito in hitos) {
                        NSMutableDictionary* nuevaNuevaFila = nuevaFila.mutableCopy;
                        [nuevaNuevaFila setObject:hito forKey:@"HITO"];
                        [filas addObject:nuevaNuevaFila.copy];
                    }
                } else
                    [filas addObject:nuevaFila.copy];
            }
        } else {
            
            NSArray* hitos = [[dict valueForKey:@"HITO"] componentsSeparatedByString:@","];
            if(hitos.count>0) {
                for (NSString* hito in hitos) {
                    NSMutableDictionary* nuevaFila = dict.mutableCopy;
                    [nuevaFila setObject:hito forKey:@"HITO"];
                    [filas addObject:nuevaFila.copy];
                }
            } else
                [filas addObject:dict];
        }
    }
    
    configuracion.filasCSV = filas.copy;
    return configuracion;
}

- (Hito *) hito:(int) numHito deItinerario:(int) numItinerario {
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(ITINERARIO == %@) AND (HITO == %@)", [NSString stringWithFormat:@"%d",numItinerario], [NSString stringWithFormat:@"%d",numHito]];
    
    NSArray* filteredArray = [self.filasCSV filteredArrayUsingPredicate:predicate];

    if(filteredArray.count==0)
        return nil;
    
    return [self configuraHitoConDictionary:filteredArray.lastObject];
}

- (Hito *)configuraHitoConDictionary:(NSDictionary *)dict {
    return nil;
}

@end
