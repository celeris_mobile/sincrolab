//
//  Configuraciones.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constantes.h"
#import "ConfiguracionSelector.h"
#import "ConfiguracionGongs.h"
#import "ConfiguracionInvasion.h"
#import "ConfiguracionPoker.h"

@interface Configuraciones : NSObject
+ (id)sharedConf;

@property (nonatomic, strong) NSArray* selectorJuegos;

- (void) genera:(void (^)())completion;

@property (nonatomic, readonly) ConfiguracionSelector* confSelector;
@property (nonatomic, readonly) ConfiguracionGongs* confGongs;
@property (nonatomic, readonly) ConfiguracionPoker* confPoker;
@property (nonatomic, readonly) ConfiguracionInvasion* confInvasion;
@end
