//
//  ConfiguracionSelector.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Configuracion.h"

@interface ConfiguracionSelector : Configuracion
- (NSArray *) itinerarios;
@end
