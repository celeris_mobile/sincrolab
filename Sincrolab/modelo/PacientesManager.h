//
//  PacientesManager.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Terapeuta.h"
#import "Paciente.h"

@interface PacientesManager : NSObject
+ (void) nuevoPacienteDeTerapeuta:(Terapeuta *) terapeuta ConCampos:(NSDictionary*) campos completado:(void (^)(NSError *, Paciente *))completado;
+ (void) borraPaciente:(Paciente*) paciente completado:(void (^)(NSError *))completado;
+ (void) actualizaPaciente:(Paciente*) paciente conCampos:(NSDictionary*) campos completado:(void (^)(NSError *))completado;
@end
