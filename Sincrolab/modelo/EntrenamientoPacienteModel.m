//
//  PacientesModel.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 14/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoPacienteModel.h"
#import "Constantes.h"
#import "ProgressHUD.h"
#import "DiaEntrenamiento.h"

@interface EntrenamientoPacienteModel()
@property (nonatomic, strong, readwrite) Paciente* paciente;
@property (nonatomic, strong, readwrite) Entrenamiento* entrenamiento;
@property (nonatomic, strong, readwrite) NSArray* diasDeTodosLosEntrenamientos;

@property (nonatomic, strong) PFQuery *queryEntrenamiento;
@property (nonatomic, strong) PFQuery *query;
@end

@implementation EntrenamientoPacienteModel {
    PFCachePolicy cachePolicy;
}
@synthesize paciente = _paciente;
@synthesize entrenamiento = _entrenamiento;

- (id)initWithPaciente:(Paciente *)paciente {
    self = [super init];
    if (self) {
        _paciente = paciente;
        cachePolicy = kPFCachePolicyNetworkElseCache;

        if(paciente.entrenamiento)
            self.entrenamiento = paciente.entrenamiento;

        [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(entrenamientoCreado:) name:kNuevoEntrenamientoNotification object:nil];
    }
    return self;
}

- (void) carga:(void(^)(NSError*)) completado conProgreso:(BOOL) muestraProgreso {

    if(muestraProgreso)
        [ProgressHUD show:nil Interacton:NO];
    
    self.queryEntrenamiento = [PFQuery queryWithClassName:@"Entrenamiento"];
    [self.queryEntrenamiento whereKey:@"paciente" equalTo:self.paciente];
    [self.queryEntrenamiento includeKey:@"estadoEntrenamiento"];
    [self.queryEntrenamiento includeKey:@"estadoEntrenamiento.estadoGongs"];
    [self.queryEntrenamiento includeKey:@"estadoEntrenamiento.estadoInvasion"];
    [self.queryEntrenamiento includeKey:@"estadoEntrenamiento.estadoPoker"];
    
    [self.queryEntrenamiento orderByAscending:@"inicio"];
    
    self.queryEntrenamiento.cachePolicy = cachePolicy;
    
    [self.queryEntrenamiento findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error);
        } else {
            _entrenamiento = [objects lastObject];
            _entrenamiento.paciente = self.paciente;
            _paciente.entrenamiento = _entrenamiento;

            if(!objects.count) {
                [ProgressHUD dismiss];
                _paciente.entrenamientoCargado = YES;
                completado(error);                
                return;
            }
            
            self.query = [PFQuery queryWithClassName:@"DiaEntrenamiento"];
            [self.query whereKey:@"entrenamiento" containedIn:objects];
            [self.query includeKey:@"entrenamiento.paciente"];
            [self.query includeKey:@"partidaGongs"];
            [self.query includeKey:@"partidaInvasion"];
            [self.query includeKey:@"partidaPoker"];
            [self.query setLimit:1000];
            [self.query orderByAscending:@"fecha"];
            self.query.cachePolicy = cachePolicy;
            [self.query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(muestraProgreso)
                    [ProgressHUD dismiss];
                
                _paciente.entrenamientoCargado = YES;
                _diasDeTodosLosEntrenamientos = objects;
                
                NSPredicate* predicate = [NSPredicate predicateWithFormat:@"entrenamiento.objectId == %@", _entrenamiento.objectId];
                _entrenamiento.diasDeEntrenamiento = [objects filteredArrayUsingPredicate:predicate].mutableCopy;

                
                if(cachePolicy==kPFCachePolicyNetworkElseCache)
                    cachePolicy = kPFCachePolicyCacheElseNetwork;

                
                completado(error);
            }];
        }
    }];
}


- (void) entrenamientoCreado:(NSNotification *) notification {
    /*
    NSDictionary *dict = [notification userInfo];
    Entrenamiento* entrenamientoCreado = [dict objectForKey:@"entrenamiento"];
    _entrenamiento = entrenamientoCreado;
    _entrenamiento.paciente = self.paciente;
    _paciente.entrenamiento = _entrenamiento;
     */
}


- (void) entrenamientoModificado:(NSNotification *) notification {
    NSDictionary *dict = [notification userInfo];
    Entrenamiento* entrenamientoModificado = [dict objectForKey:@"entrenamiento"];
    _entrenamiento = entrenamientoModificado;
    _entrenamiento.paciente = self.paciente;
    _paciente.entrenamiento = _entrenamiento;    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNuevoEntrenamientoNotification object:nil];
}

- (NSArray *) tiempoJuegos {
    if(!self.entrenamiento || !self.entrenamiento.diasDeEntrenamiento || self.entrenamiento.diasDeEntrenamiento.count==0)
        return nil;
    
    NSArray *sesionesGongs = [self.entrenamiento.diasDeEntrenamiento valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
    NSNumber* duracionGongs = [sesionesGongs valueForKeyPath:@"@sum.duracion"];

    NSArray *sesionesInvasion = [self.entrenamiento.diasDeEntrenamiento valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"];
    NSNumber* duracionInvasion = [sesionesInvasion valueForKeyPath:@"@sum.duracion"];

    NSArray *sesionesPoker = [self.entrenamiento.diasDeEntrenamiento valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"];
    NSNumber* duracionPoker = [sesionesPoker valueForKeyPath:@"@sum.duracion"];
    
    return @[duracionGongs, duracionInvasion, duracionPoker];
}

- (void) refresca {
    cachePolicy = kPFCachePolicyNetworkElseCache;
    _paciente.entrenamientoCargado = NO;
}

- (BOOL)cargado {
    return _paciente.entrenamientoCargado;
}

- (void) cancela {
    [self.queryEntrenamiento cancel];
    [self.query cancel];
}


@end
