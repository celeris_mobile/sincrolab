//
//  UsuariosManager.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "UsuariosManager.h"
#import "Terapeuta.h"
#import "Paciente.h"

@implementation UsuariosManager

- (BOOL)hayUsuarioLoggeado {
    if([PFUser currentUser]) {
        [self cargaUsuario:[PFUser currentUser]];
        return YES;
    } else {
        return NO;
    }
}

- (void) logOutUsuario {
    [PFUser logOut];
    [self.delegate usuarioLoggedOut];
}

- (void) cargaUsuario:(PFUser *) user {
    
    if([[user objectForKey:@"tipo"] isEqualToString:@"terapeuta"]||[[user objectForKey:@"tipo"] isEqualToString:@"terapeuta_anonimo"]) {
        PFQuery *query = [PFQuery queryWithClassName:@"Terapeuta"];
        query.cachePolicy =  kPFCachePolicyNetworkOnly;
        [query whereKey:@"usuario" equalTo:user];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                Terapeuta* terapeuta = objects.lastObject;
                [self.delegate usuarioLoggedIn:terapeuta];
            } else {
                [self.delegate errorLogeandoUsuario:error];
            }
        }];
    } else if([[user objectForKey:@"tipo"] isEqualToString:@"paciente"]) {
        
        PFQuery *query = [PFQuery queryWithClassName:@"Paciente"];
        query.cachePolicy =  kPFCachePolicyCacheElseNetwork;
        [query whereKey:@"usuario" equalTo:user];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                Paciente* paciente = objects.lastObject;
                [self.delegate usuarioLoggedIn:paciente];
            } else {
                [self.delegate errorLogeandoUsuario:error];
            }
        }];
    } else {
        [self.delegate errorLogeandoUsuario:nil];
    }
}

@end
