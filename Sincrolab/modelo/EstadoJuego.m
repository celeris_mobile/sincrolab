//
//  EstadoJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadoJuego.h"
#import "Hito.h"
#import "DDMathParser.h"
#import <Parse/PFObject+Subclass.h>

@implementation EstadoJuego
@dynamic diasSemana, itinerario, hito;
@synthesize afectacion = _afectacion;

+ (instancetype)estadoPorDefecto {
    return nil;
}

+ (instancetype) estadoParaHitoInicialdeItinerario:(NSString *) prioridadItinerario conAfectacion:(NSString *) afectacion {
    return nil;
}

- (void)configuraPorDefecto {
    
}

- (BOOL) configuraParaHito:(int) numHito {
    return NO;
}

- (BOOL) compruebaCondiciones:(NSArray *) condiciones {
    NSError* error;
    
    for(NSString* condicion in condiciones) {
       
        if(condicion.length>0&&![condicion isEqualToString:@"0"]&&![condicion isEqualToString:@"-"]) {
            
            NSString* ecuacion = [condicion stringByReplacingOccurrencesOfString:@"," withString:@"."].lowercaseString;
            
            ecuacion = [self reemplazaVariables:ecuacion];
            
            if (error == nil) {
                
                NSPredicate* predicate = [NSPredicate predicateWithFormat:ecuacion];
                BOOL result = [predicate evaluateWithObject:nil];
                
                if(!result) {
                    return NO;
                }
                
            } else {
                NSLog(@"Error: %@", error);
            }
        }
    }
    
    return YES;
}

- (BOOL) topeAlcanzadoEnParametro:(NSString *) parametro conValor:(NSNumber *) siguienteValor paraHito:(Hito *) hito {
    NSArray* topes = [hito getCondiciones:parametro.lowercaseString];
    /*
    if(topes.count>0) {
        
        BOOL topeAlcanzadoEnParametro = NO;
        
        for(NSString* tope in topes) {
            if(tope.length>0) {

                NSString* ecuacion = [tope stringByReplacingOccurrencesOfString:@"," withString:@"."].lowercaseString;
                
                ecuacion = [self reemplazaVariables:ecuacion];
                ecuacion = [ecuacion stringByReplacingOccurrencesOfString:parametro.lowercaseString withString:siguienteValor.stringValue];
                
                if (error == nil) {

                    NSPredicate* predicate = [NSPredicate predicateWithFormat:ecuacion];
                    BOOL result = [predicate evaluateWithObject:nil];
                    
                    if(!result) {
                        topeAlcanzadoEnParametro = YES;
                    }
                    
                } else {
                    NSLog(@"Error: %@", error);
                }
            }
        }
        
        return topeAlcanzadoEnParametro;
    }
    
    return NO;
     */
    
    return ![self compruebaCondiciones:topes];
}

- (BOOL) aplicaCambioSecuencial:(int) numCambio deCambios:(NSArray*) cambios enHito:(Hito *) hito {
    if(numCambio==cambios.count)
        return NO;
    
    NSString* cambio = [cambios objectAtIndex:numCambio];
    
    if(cambio.length>0 && ![cambio isEqualToString:@"0"] && ![cambio isEqualToString:@"-"]) {
        
        NSString* ecuacion = [cambio stringByReplacingOccurrencesOfString:@"," withString:@"."].lowercaseString;
        
        NSArray* componentes = [ecuacion componentsSeparatedByString:@":"];
        if(componentes.count==1) {
            ecuacion = [self reemplazaVariables:ecuacion];
            NSExpression* expresion;
            NSNumber* result;
            @try {
                expresion = [NSExpression expressionWithFormat:ecuacion, nil];
                result = [expresion expressionValueWithObject:nil context:nil];
            }
            @catch (NSException *exception) {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Error con la expresión: %@", ecuacion] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                return NO;
            }
            
            NSString* operador;
            NSScanner* theScanner = [NSScanner scannerWithString:cambio];
            [theScanner scanCharactersFromSet:[[NSCharacterSet characterSetWithCharactersInString:@"+-*/|: "] invertedSet] intoString:&operador];
            
            if(![self topeAlcanzadoEnParametro:operador conValor:result paraHito:hito]) {
                [self setValue:[NSString stringWithFormat:@"%.4f", result.floatValue] forKey:operador.lowercaseString];
                return YES;
            } else {
                if([self aplicaCambioSecuencial:numCambio+1 deCambios:cambios enHito:hito]) {
                    [self setValue:[hito getValor:operador.lowercaseString] forKey:operador.lowercaseString];
                    return YES;
                }
                return NO;
            }
        } else {
            NSString* operador = [componentes objectAtIndex:0];
            NSArray* valores = [((NSString *)[componentes objectAtIndex:1]) componentsSeparatedByString:@">"];
            NSUInteger indiceActual = [valores indexOfObject:[self valueForKey:operador.lowercaseString]];
            if(indiceActual<valores.count-1) {
                [self setValue:[valores objectAtIndex:indiceActual+1] forKey:operador.lowercaseString];
                return YES;
            } else {
                if([self aplicaCambioSecuencial:numCambio+1 deCambios:cambios enHito:hito]) {
                    [self setValue:[valores objectAtIndex:0] forKey:operador.lowercaseString];
                    return YES;
                }
                return NO;
            }
        }
    }
    
    return NO;
}

- (BOOL) aplicaCambios:(NSString*) cambios enHito:(Hito *) hito {
    cambios = [cambios stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if([cambios rangeOfString:@"("].location == 0 && [cambios rangeOfString:@")"].location == cambios.length-1) {
        cambios = [cambios substringWithRange:NSMakeRange(1, cambios.length-2)];
        
        NSArray* cambiosArray = [cambios componentsSeparatedByString:@"|"];
        
        BOOL allTopesAlcanzados = YES;
        for (NSString* cambio in cambiosArray) {
            
            if(cambio.length>0 && ![cambio isEqualToString:@"0"] && ![cambio isEqualToString:@"-"]) {
                
                NSString* ecuacion = [cambio stringByReplacingOccurrencesOfString:@"," withString:@"."].lowercaseString;
                
                NSArray* componentes = [ecuacion componentsSeparatedByString:@":"];
                if(componentes.count==1) {
                    ecuacion = [self reemplazaVariables:ecuacion];
                    
                    NSExpression* expresion;
                    NSNumber* result;
                    @try {
                        expresion = [NSExpression expressionWithFormat:ecuacion, nil];
                        result = [expresion expressionValueWithObject:nil context:nil];
                    }
                    @catch (NSException *exception) {
                        [[[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Error con la expresión: %@", ecuacion] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                        return NO;
                    }
                    
                    NSString* operador;
                    NSScanner* theScanner = [NSScanner scannerWithString:cambio];
                    [theScanner scanCharactersFromSet:[[NSCharacterSet characterSetWithCharactersInString:@"+-*/|: "] invertedSet] intoString:&operador];
                    
                    if(![self topeAlcanzadoEnParametro:operador conValor:result paraHito:hito]) {
                        [self setValue:[NSString stringWithFormat:@"%.4f", result.floatValue] forKey:operador.lowercaseString];
                        allTopesAlcanzados = NO;
                    }
                } else {
                    NSString* operador = [componentes objectAtIndex:0];
                    NSArray* valores = [((NSString *)[componentes objectAtIndex:1]) componentsSeparatedByString:@">"];
                    NSUInteger indiceActual = [valores indexOfObject:[self valueForKey:operador.lowercaseString]];
                    if(indiceActual<valores.count-1) {
                        [self setValue:[valores objectAtIndex:indiceActual+1] forKey:operador.lowercaseString];
                        allTopesAlcanzados = NO;
                    }
                }                
            }
        }
        
        if(allTopesAlcanzados)
            return YES;
        else
            return NO;
        
    } else {
        NSArray* cambiosArray = [cambios componentsSeparatedByString:@"|"];
        return ![self aplicaCambioSecuencial:0 deCambios:cambiosArray enHito:hito];
    }
    
    return NO;
}

- (EstadoJuegoCambio) compruebaCambioNivelConResultados:(NSArray *)resultados {
    return EstadoJuegoCambioSeMantiene;
}


- (NSNumber *) numberDePropiedad:(NSString *) parametro {
    NSArray* componentes = [parametro componentsSeparatedByString:@":"];
    if(componentes.count==1)
        return [NSNumber numberWithFloat:((NSString *)[componentes objectAtIndex:0]).floatValue];
    else {
        //TODO si es un rango, para hacer las operaciones matemáticas, devolvemos el valor medio
        float v1 = ((NSString *)[componentes objectAtIndex:0]).floatValue;
        float v2 = ((NSString *)[componentes objectAtIndex:1]).floatValue;
        return [NSNumber numberWithFloat:(v2-v1)*0.5+v1];
    }
}



- (NSString *) reemplazaVariables:(NSString *) ecuacion {
    return ecuacion;
}
@end
