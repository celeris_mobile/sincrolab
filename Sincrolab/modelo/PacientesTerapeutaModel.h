//
//  PacientesModel.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 14/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Terapeuta.h"
#import "Paciente.h"

@interface PacientesTerapeutaModel : NSObject
- (id) initWithTerapeuta:(Terapeuta *) terapeuta;
@property (nonatomic, readonly) Terapeuta* terapeuta;
@property (nonatomic, readonly) NSArray* pacientes;
@property (nonatomic, readonly) NSInteger numPacientesActivos;
@property (nonatomic, readonly) NSArray* diasDeEntrenamientoSemana;
@property (nonatomic, readonly) NSArray* gruposDeEdad;
@property (nonatomic, readonly) NSArray* sexos;
@property (nonatomic, readonly) NSArray* pacientesDeLaSemana;
@property (nonatomic, readonly) NSArray* entrenamientosPorDia;
@property (nonatomic, readonly) NSArray* todosLosDias;
- (void) refresca;
- (void) carga;
- (void) cargaDiasEntrenamientoDeSemana:(NSDate *) lunesSemana;
- (void)cargaTodosLosDiasDeEntrenamiento:(void(^)(NSError*)) completado;
@end
