//
//  Terapeuta.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Terapeuta.h"

@implementation Terapeuta
@dynamic nombre, topePacientes;


+ (NSString *)parseClassName {
    return @"Terapeuta";
}

@end
