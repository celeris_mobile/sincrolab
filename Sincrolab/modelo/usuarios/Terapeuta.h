//
//  Terapeuta.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Usuario.h"

@interface Terapeuta : Usuario

@property (retain) NSString* nombre;
@property (retain) NSNumber* topePacientes;

@end
