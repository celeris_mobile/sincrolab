//
//  Usuario.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Usuario : PFObject  <PFSubclassing>

@property (retain) PFUser* usuario;
@property (retain) NSString* email;
@property (retain) NSString* pass;

@end
