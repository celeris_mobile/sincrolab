//
//  Paciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"
#import "Terapeuta.h"

#import "DatosPaciente.h"
#import "CuestionarioPaciente.h"
#import "DiagnosticoPaciente.h"
#import "PerfilCognitivoPaciente.h"

@class Entrenamiento;
@interface Paciente : Usuario

@property (retain) NSString* nombre;
@property (retain) NSString* apellidos;
@property (retain) Terapeuta* terapeuta;

@property (retain) NSNumber* nivel;
@property (retain) NSNumber* puntos;

@property (retain) NSDate* ultimoEntrenamiento;
@property (retain) NSDate* siguienteEntrenamiento;

@property (retain) DatosPaciente* datos;
@property (retain) CuestionarioPaciente* cuestionario;
@property (retain) DiagnosticoPaciente* diagnostico;
@property (retain) PerfilCognitivoPaciente* perfilcognitivo;

+ (Paciente *) creaConTerapeuta:(Terapeuta *) terapeuta;

@property (nonatomic, strong) Entrenamiento* entrenamiento;
@property (nonatomic, assign) BOOL entrenamientoCargado;

@property (nonatomic, readonly) NSDate* fechaUltimoEvento;
@property (nonatomic, readonly) NSInteger diasSinJugar;
@end
