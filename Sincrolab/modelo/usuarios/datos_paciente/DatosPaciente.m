//
//  Terapeuta.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DatosPaciente.h"
#import <Parse/PFObject+Subclass.h>

@implementation DatosPaciente
@dynamic foto, fechaNacimiento, curso, repetidor, colegio, sexo, evaluacionesPrevias, descripcionEvaluacionesPrevias, nombreDelPadre, nombreDeLaMadre, estadoCivilDeLosPadres, estudiosDelPadre, estudiosDeLaMadre, observaciones;

+ (NSString *)parseClassName {
    return @"DatosPaciente";
}

- (NSNumber *)edad {
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:self.fechaNacimiento
                                       toDate:[NSDate date]
                                       options:0];
    return [NSNumber numberWithInt:(int)[ageComponents year]];
}

@end
