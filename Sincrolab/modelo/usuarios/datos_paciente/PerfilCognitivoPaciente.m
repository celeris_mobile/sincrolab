//
//  PerfilCognitivoPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PerfilCognitivoPaciente.h"
#import <Parse/PFObject+Subclass.h>
#import "ProcesoAfectado.h"

@implementation PerfilCognitivoPaciente
@dynamic memoriaOperativa, atencionSostenida, atencionSelectiva, atencionDividida, velocidadDeProcesamiento, tomaDeDecisiones, controlInhibitorio, flexibilidadCognitiva;

+ (NSString *)parseClassName {
    return @"PerfilCognitivoPaciente";
}

- (BOOL)vacio {
    if(self.memoriaOperativa.intValue==0 && self.atencionSostenida.intValue==0 && self.atencionSelectiva.intValue == 0 && self.atencionDividida.intValue == 0 && self.velocidadDeProcesamiento.intValue == 0 && self.tomaDeDecisiones.intValue == 0 && self.controlInhibitorio.intValue == 0 && self.flexibilidadCognitiva.intValue == 0)
        return YES;
    
    return NO;        
}

- (NSArray *)procesosAfectados {
    if([self vacio])
        return nil;
    
    NSMutableArray* procesosaux = [[NSMutableArray alloc] init];
    
    if(self.memoriaOperativa.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"mo" caption:@"Memoria Operativa" andAfectacion:[NSNumber numberWithInt:self.memoriaOperativa.intValue]]];
    if(self.atencionSostenida.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"aso" caption:@"Atención Sostenida" andAfectacion:[NSNumber numberWithInt:self.atencionSostenida.intValue]]];
    if(self.atencionSelectiva.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"ase" caption:@"Atención Selectiva" andAfectacion:[NSNumber numberWithInt:self.atencionSelectiva.intValue]]];
    if(self.atencionDividida.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"ad" caption:@"Atención Dividida" andAfectacion:[NSNumber numberWithInt:self.atencionDividida.intValue]]];
    if(self.velocidadDeProcesamiento.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"vp" caption:@"Velocidad de Procesamiento" andAfectacion:[NSNumber numberWithInt:self.velocidadDeProcesamiento.intValue]]];
    if(self.tomaDeDecisiones.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"td" caption:@"Toma de Decisiones" andAfectacion:[NSNumber numberWithInt:self.tomaDeDecisiones.intValue]]];
    if(self.controlInhibitorio.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"ci" caption:@"Control inhibitorio" andAfectacion:[NSNumber numberWithInt:self.controlInhibitorio.intValue]]];
    if(self.flexibilidadCognitiva.intValue)
        [procesosaux addObject:[ProcesoAfectado newWithName:@"fc" caption:@"Flexibilidad Cognitiva" andAfectacion:[NSNumber numberWithInt:self.flexibilidadCognitiva.intValue]]];
    
    return procesosaux.copy;
}

- (NSString *)resumen {
    NSMutableArray* arrayProcesos = [[NSMutableArray alloc] init];
    for (ProcesoAfectado* proceso in self.procesosAfectados) {
        NSString* str = [NSString stringWithFormat:@"%@%d", proceso.nombre, proceso.afectacion.intValue];
        [arrayProcesos addObject:str];
    }
    
    return [arrayProcesos componentsJoinedByString:@", "];
}

@end
