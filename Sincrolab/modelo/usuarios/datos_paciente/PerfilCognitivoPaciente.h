//
//  PerfilCognitivoPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>

@interface PerfilCognitivoPaciente : PFObject <PFSubclassing>
@property (retain) NSNumber* memoriaOperativa;
@property (retain) NSNumber* atencionSostenida;
@property (retain) NSNumber* atencionSelectiva;
@property (retain) NSNumber* atencionDividida;
@property (retain) NSNumber* velocidadDeProcesamiento;
@property (retain) NSNumber* tomaDeDecisiones;
@property (retain) NSNumber* controlInhibitorio;
@property (retain) NSNumber* flexibilidadCognitiva;

@property (nonatomic, readonly) BOOL vacio;
@property (nonatomic, readonly) NSArray * procesosAfectados;
@property (nonatomic, readonly) NSString* resumen;
@end
