//
//  CuestionarioPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CuestionarioPaciente.h"
#import <Parse/PFObject+Subclass.h>

@implementation CuestionarioPaciente
@dynamic antecedentesPadre, antecedentesMadre, descripcionAntecedentes, medicacion, tipoDeMedicacion, tipoDeParto, apgar, apgar2, sufrimiento, reanimacion, pesoAlNacer, controlCefalico, gateo, deambulacion, deglucion, edadDeglucion, habitos, praxias, manias, descripcionManias, comprension, expresion, lectura, escritura, comunicacion, primerasPalabras, edadPrimerasPalabras, primerasFrases, edadPrimerasFrases, relacionesPares, relacionesAdultos, relacionesColegio, relacionesCasa;

+ (NSString *)parseClassName {
    return @"CuestionarioPaciente";
}

@end