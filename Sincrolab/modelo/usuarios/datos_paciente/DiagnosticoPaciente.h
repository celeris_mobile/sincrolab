//
//  DiagnosticoPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>

@interface DiagnosticoPaciente : PFObject <PFSubclassing>
@property (retain) NSNumber* patologiaMedica;
@property (retain) NSString* descripcionPatologiaMedica;
@property (retain) NSNumber* clasificacionNeurofisiologica;
@property (retain) NSString* descripcionClasificacionNeurofisiologica;
@property (retain) NSNumber* factoresGeneticos;
@property (retain) NSString* descripcionFactoresGeneticos;
@property (retain) NSNumber* factoresDeRiesgo;
@property (retain) NSString* descripcionFactoresDeRiesgo;
@property (retain) NSNumber* trastornosGeneralizados;
@property (retain) NSString* descripcionTrastornosGeneralizados;
@property (retain) NSNumber* trastornosAtencion;
@property (retain) NSString* descripcionTrastornosAtencion;
@property (retain) NSNumber* trastornosNeurocognitivos;
@property (retain) NSString* descripcionTrastornosNeurocognitivos;
@property (retain) NSNumber* trastornosPerceptivos;
@property (retain) NSString* descripcionTrastornosPerceptivos;
@property (retain) NSNumber* retrasoMental;
@property (retain) NSString* descripcionRetrasoMental;
@property (retain) NSNumber* trastornosLenguaje;
@property (retain) NSString* descripcionTrastornosLenguaje;
@property (retain) NSNumber* dificultadesBasicas;
@property (retain) NSString* descripcionDificultadesBasicas;
@property (retain) NSNumber* dificultadesAprendizaje;
@property (retain) NSString* descripcionDificultadesAprendizaje;
@property (retain) NSNumber* dificultadesSociales;
@property (retain) NSString* descripcionDificultadesSociales;
@property (retain) NSNumber* trastornosConducta;
@property (retain) NSString* descripcionTrastornosConducta;
@property (retain) NSString* observaciones;
@end
