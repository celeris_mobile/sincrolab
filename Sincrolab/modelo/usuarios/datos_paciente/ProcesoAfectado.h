//
//  ProcesoAfectado.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProcesoAfectado : NSObject

@property (nonatomic, strong) NSString* nombre;
@property (nonatomic, strong) NSString* caption;
@property (nonatomic, strong) NSNumber* afectacion;
+ (ProcesoAfectado *) newWithName:(NSString *) name caption:(NSString*) caption andAfectacion:(NSNumber *) afectacion;

@end
