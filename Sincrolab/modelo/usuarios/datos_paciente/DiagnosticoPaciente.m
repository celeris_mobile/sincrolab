//
//  DiagnosticoPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DiagnosticoPaciente.h"
#import <Parse/PFObject+Subclass.h>

@implementation DiagnosticoPaciente
@dynamic patologiaMedica, descripcionPatologiaMedica, clasificacionNeurofisiologica, descripcionClasificacionNeurofisiologica, factoresGeneticos, descripcionFactoresGeneticos, factoresDeRiesgo, descripcionFactoresDeRiesgo, trastornosGeneralizados, descripcionTrastornosGeneralizados, trastornosAtencion, descripcionTrastornosAtencion, trastornosNeurocognitivos, descripcionTrastornosNeurocognitivos, trastornosPerceptivos, descripcionTrastornosPerceptivos, retrasoMental, descripcionRetrasoMental, trastornosLenguaje, descripcionTrastornosLenguaje, dificultadesBasicas, descripcionDificultadesBasicas, dificultadesAprendizaje, descripcionDificultadesAprendizaje, dificultadesSociales, descripcionDificultadesSociales, trastornosConducta, descripcionTrastornosConducta, observaciones;

+ (NSString *)parseClassName {
    return @"DiagnosticoPaciente";
}

@end
