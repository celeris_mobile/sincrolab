//
//  Terapeuta.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>

@interface DatosPaciente : PFObject <PFSubclassing>
@property (retain) PFFile* foto;
@property (retain) NSNumber* sexo;
@property (retain) NSDate* fechaNacimiento;
@property (retain) NSString *curso;
@property (retain) NSNumber* repetidor;
@property (retain) NSString *colegio;

@property (retain) NSNumber* evaluacionesPrevias;
@property (retain) NSString* descripcionEvaluacionesPrevias;
@property (retain) NSString* nombreDelPadre;
@property (retain) NSString* nombreDeLaMadre;
@property (retain) NSString* estadoCivilDeLosPadres;
@property (retain) NSString* estudiosDelPadre;
@property (retain) NSString* estudiosDeLaMadre;

@property (retain) NSString* observaciones;

@property (nonatomic, readonly) NSNumber* edad;
@end