//
//  CuestionarioPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>

@interface CuestionarioPaciente : PFObject <PFSubclassing>

@property (retain) NSNumber* antecedentesPadre;
@property (retain) NSNumber* antecedentesMadre;
@property (retain) NSString* descripcionAntecedentes;

@property (retain) NSNumber* medicacion;
@property (retain) NSString *tipoDeMedicacion;

@property (retain) NSString* tipoDeParto;
@property (retain) NSNumber* apgar;
@property (retain) NSNumber* apgar2;
@property (retain) NSNumber* sufrimiento;
@property (retain) NSNumber* reanimacion;
@property (retain) NSNumber* pesoAlNacer;

@property (retain) NSNumber* controlCefalico;
@property (retain) NSNumber* gateo;
@property (retain) NSNumber* deambulacion;

@property (retain) NSNumber* deglucion;
@property (retain) NSNumber* edadDeglucion;
@property (retain) NSNumber* habitos;
@property (retain) NSNumber* praxias;
@property (retain) NSNumber* manias;
@property (retain) NSString* descripcionManias;

@property (retain) NSNumber* comprension;
@property (retain) NSNumber* expresion;
@property (retain) NSNumber* lectura;
@property (retain) NSNumber* escritura;
@property (retain) NSNumber* comunicacion;
@property (retain) NSNumber* primerasPalabras;
@property (retain) NSNumber* edadPrimerasPalabras;
@property (retain) NSNumber* primerasFrases;
@property (retain) NSNumber* edadPrimerasFrases;

@property (retain) NSNumber* relacionesPares;
@property (retain) NSNumber* relacionesAdultos;
@property (retain) NSNumber* relacionesColegio;
@property (retain) NSNumber* relacionesCasa;

@end
