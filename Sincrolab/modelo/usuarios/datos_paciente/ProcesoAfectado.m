//
//  ProcesoAfectado.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 08/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ProcesoAfectado.h"

@implementation ProcesoAfectado

+ (ProcesoAfectado *) newWithName:(NSString *) name caption:(NSString*) caption andAfectacion:(NSNumber *) afectacion {    ProcesoAfectado* proceso = [[ProcesoAfectado alloc] init];
    proceso.nombre = name;
    proceso.caption = caption;
    proceso.afectacion = afectacion;
    
    return proceso;
}

@end
