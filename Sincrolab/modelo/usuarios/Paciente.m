//
//  Paciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Paciente.h"
#import "DateUtils.h"

@implementation Paciente
@dynamic nombre, apellidos, terapeuta, nivel, puntos, ultimoEntrenamiento, siguienteEntrenamiento, datos, cuestionario, diagnostico, perfilcognitivo;
@synthesize entrenamiento = _entrenamiento;
@synthesize entrenamientoCargado = _entrenamientoCargado;

+ (Paciente *) creaConTerapeuta:(Terapeuta *) terapeuta {
    Paciente* paciente = [Paciente object];
    paciente.terapeuta = terapeuta;
    return paciente;
}

+ (NSString *)parseClassName {
    return @"Paciente";
}

- (void)setEntrenamiento:(Entrenamiento *)entrenamiento {
    _entrenamiento = entrenamiento;
}

- (NSDate *)fechaUltimoEvento {
    if(!self.ultimoEntrenamiento && !self.siguienteEntrenamiento)
        return nil;

    if([DateUtils comparaSinHoraFecha:[NSDate date] conFecha:self.siguienteEntrenamiento]!=NSOrderedDescending) {
        if(self.ultimoEntrenamiento)
            return self.ultimoEntrenamiento;
        else
            return nil;
    }
    
    if(self.ultimoEntrenamiento) {
        if([DateUtils comparaSinHoraFecha:self.ultimoEntrenamiento conFecha:self.siguienteEntrenamiento]==NSOrderedDescending)
            return self.ultimoEntrenamiento;
        else
            return self.siguienteEntrenamiento;
    }

    return self.siguienteEntrenamiento;
}

- (NSInteger)diasSinJugar {
    if(!self.siguienteEntrenamiento)
        return 0;
    
    if([DateUtils comparaSinHoraFecha:self.siguienteEntrenamiento conFecha:[NSDate date]]!=NSOrderedAscending)
        return 0;

    return [DateUtils diasEntreFecha:self.siguienteEntrenamiento yFecha:[NSDate date]];

        
}
@end
