//
//  SliderAlert.m
//  Sincrolab
//
//  Created by David Hevilla Garcia on 27/08/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SliderAlert.h"

@interface SliderAlert()
@property (nonatomic, strong) UILabel* label;
@end

@implementation SliderAlert

- (id)init
{
    self = [super initWithFrame:CGRectMake(0, 0, 50, 65)];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        
        self.label = [[UILabel alloc] initWithFrame:self.bounds];
        CGRect labelFrame = self.label.frame;
        labelFrame.origin.y -= 5;
        self.label.frame = labelFrame;
        
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = [UIFont fontWithName:@"Lato-Bold" size:14.0];
        self.label.textColor = [UIColor blackColor];
        [self addSubview:self.label];
    }
    return self;
}

- (void)setValor:(NSString *)valor {
    _valor = valor;
    self.label.text = valor;
    
    CGSize tam = [valor sizeWithAttributes:@{NSFontAttributeName: self.label.font}];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, tam.width+10, tam.height+10);
    
    self.label.frame = CGRectMake(5, 0, tam.width, tam.height);
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, rect.size.width, 0);
    CGContextAddLineToPoint(ctx, rect.size.width, rect.size.height-5);
    CGContextAddLineToPoint(ctx, rect.size.width/2+5, rect.size.height-5);
    CGContextAddLineToPoint(ctx, rect.size.width/2, rect.size.height);
    CGContextAddLineToPoint(ctx, rect.size.width/2-5, rect.size.height-5);
    CGContextAddLineToPoint(ctx, 0, rect.size.height-5);
    CGContextClosePath(ctx);
    CGContextSetLineWidth(ctx, 6);
    
    CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextFillPath(ctx);
}

@end
