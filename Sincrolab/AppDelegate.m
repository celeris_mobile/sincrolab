//
//  AppDelegate.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "Paciente.h"
#import "Terapeuta.h"
#import "DatosPaciente.h"
#import "CuestionarioPaciente.h"
#import "DiagnosticoPaciente.h"
#import "PerfilCognitivoPaciente.h"
#import "Entrenamiento.h"
#import "EstadoEntrenamiento.h"
#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"
#import "DiaEntrenamiento.h"

#import "PartidaGongs.h"
#import "PartidaInvasion.h"
#import "PartidaPoker.h"

#import "Flurry.h"
#import <Crashlytics/Crashlytics.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
   
    //Test
    [Parse setApplicationId:@"R54QsM2QjZ8lv9npLv3tfFwuulHK7odGvd8HJt2E"
                  clientKey:@"ai3Um6p7WNmr2T1g8aGotoqoGJmZduAzLRsIDFmt"];
     
     
     /*/
    //Producción
    [Parse setApplicationId:@"MKm1dLjt1uDWvP7tuUl5fe9tn2ThvAwABsWtRPFS"
                  clientKey:@"7iGkwst8HvUXRUFRRgnz3krh9O2pl8RdLcA4IxBc"];
    //*/
    
    [Paciente registerSubclass];
    [Terapeuta registerSubclass];
    [DatosPaciente registerSubclass];
    [CuestionarioPaciente registerSubclass];
    [DiagnosticoPaciente registerSubclass];
    [PerfilCognitivoPaciente registerSubclass];
    
    [Entrenamiento registerSubclass];
    
    [EstadoEntrenamiento registerSubclass];
    [EstadoJuegoGongs registerSubclass];
    [EstadoJuegoInvasion registerSubclass];
    [EstadoJuegoPoker registerSubclass];
    
    [DiaEntrenamiento registerSubclass];
    
    [PartidaGongs registerSubclass];
    [PartidaInvasion registerSubclass];
    [PartidaPoker registerSubclass];

    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
    [Flurry setCrashReportingEnabled:NO];
    
    // Replace YOUR_API_KEY with the api key in the downloaded package
    [Flurry startSession:@"YVNNJ6Q26MXYK3F9FD3V"];

    [Crashlytics startWithAPIKey:@"96c1d2430f667401fd6b56ab657e526ea9f11e57"];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
