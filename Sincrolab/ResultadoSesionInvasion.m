//
//  ResultadoSesionInvasion.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ResultadoSesionInvasion.h"
#import "RespuestaInvasion.h"

@implementation ResultadoSesionInvasion

+ (ResultadoSesionInvasion *) resultadoConRespuestas:(NSArray *)respuestas {
    
    float aciertos = 0;
    float comisiones = 0;
    float omisiones = 0;
    float no_comisiones = 0;
    
    float suma_t_resp_aciertos = 0;
    float suma_t_resp_fallos = 0;
    
    float total_a_recuperar = 0;
    float recuperaciones = 0;
    float trials_recuperacion = 0;
    float r = 0;
    
    float puntuacion = 0;
    
    BOOL error_anterior = NO;
    
    float num_ss = 0;
    //int errores_ss = 0;
    //falta: tiempoMedioDeRespuestaErrorSS
    //mediaDemoraStopSigns
    
    BOOL error = NO;
    for (RespuestaInvasion* respuesta in respuestas) {
        switch (respuesta.tipo) {
            case TipoRespuestaInvasionAcierto:
                aciertos+=1;
                suma_t_resp_aciertos += respuesta.tiempo;
                puntuacion += 100;
                break;
            case TipoRespuestaInvasionNoComision:
                no_comisiones+=1;
                puntuacion += 50;
                break;
            case TipoRespuestaInvasionOmision:
                omisiones+=1;
                error = YES;
                puntuacion -= 50;
                break;
            case TipoRespuestaInvasionComision:
                comisiones+=1;
                suma_t_resp_fallos += respuesta.tiempo;
                error = YES;
                puntuacion -= 100;
                break;
        }
        
        if(respuesta.tieneStopSign)
            num_ss += 1;
        
        //TODO Repasar esto
        if(error && r<respuestas.count-1) {
            total_a_recuperar += 1;
        }
        if(r>0) {
            if((error_anterior && !error) || (error && r==respuestas.count-1 && !error_anterior))
                recuperaciones+=1;
        }
        
        if(error && !(r==respuestas.count-1 && !error_anterior))
            trials_recuperacion += 1;
        
        error_anterior = error;
        r++;
    }
    
    puntuacion = puntuacion<0?0:puntuacion;
    
    float porcentajeAciertos = (no_comisiones/(no_comisiones+comisiones))*(aciertos/(aciertos+omisiones))*100;
    float porcentajeOmisiones = (omisiones/(aciertos+omisiones))*100;
    float porcentajeComisiones = (comisiones/(no_comisiones+comisiones))*100;
    
    if(porcentajeAciertos!=100.0) {
        float newPorcentajeOmisiones = (porcentajeOmisiones*(100-porcentajeAciertos))/(porcentajeOmisiones+porcentajeComisiones);
        float newPorcentajeComisiones = (porcentajeComisiones*(100-porcentajeAciertos))/(porcentajeOmisiones+porcentajeComisiones);
        
        porcentajeOmisiones = newPorcentajeOmisiones;
        porcentajeComisiones = newPorcentajeComisiones;
    }
    
    ResultadoSesionInvasion* resultado = [[ResultadoSesionInvasion alloc] init];
    [resultado setNumber:[NSNumber numberWithInt:aciertos] forKey:@"numeroDeAciertos"];
    [resultado setNumber:[NSNumber numberWithInt:omisiones] forKey:@"numeroDeOmisiones"];
    [resultado setNumber:[NSNumber numberWithInt:comisiones] forKey:@"numeroDeComisiones"];
    [resultado setNumber:[NSNumber numberWithInt:no_comisiones] forKey:@"numeroDeNoComisiones"];
    [resultado setNumber:[NSNumber numberWithInt:puntuacion] forKey:@"puntuacion"];
    
    [resultado setNumber:[NSNumber numberWithFloat:suma_t_resp_aciertos/aciertos] forKey:@"tiempoMedioDeRespuestaAciertos"];
    [resultado setNumber:[NSNumber numberWithFloat:suma_t_resp_fallos/comisiones] forKey:@"tiempoMedioDeRespuestaErrores"];
    
    [resultado setNumber:[NSNumber numberWithInt:num_ss] forKey:@"numeroDeStopSigns"];
    
    [resultado setNumber:[NSNumber numberWithFloat:porcentajeAciertos] forKey:@"porcentajeAciertos"];
    [resultado setNumber:[NSNumber numberWithFloat:porcentajeOmisiones] forKey:@"porcentajeOmisiones"];
    [resultado setNumber:[NSNumber numberWithFloat:porcentajeComisiones] forKey:@"porcentajeComisiones"];
    
    [resultado setNumber:[NSNumber numberWithFloat:(recuperaciones/total_a_recuperar)*100] forKey:@"porcentajeRecuperacion"];
    [resultado setNumber:[NSNumber numberWithFloat:(trials_recuperacion/recuperaciones)] forKey:@"recuperacionMedia"];
    
    
    /*
    resultado.numeroDeAciertos = [NSNumber numberWithInt:aciertos];
    resultado.numeroDeOmisiones = [NSNumber numberWithInt:omisiones];
    resultado.numeroDeComisiones = [NSNumber numberWithInt:comisiones];
    resultado.numeroDeNoComisiones = [NSNumber numberWithInt:no_comisiones];
    resultado.puntuacion = [NSNumber numberWithInt:puntuacion];
    
    resultado.tiempoMedioDeRespuestaAciertos = [NSNumber numberWithFloat:aciertos?suma_t_resp_aciertos/aciertos:0];
    resultado.tiempoMedioDeRespuestaErrores = [NSNumber numberWithFloat:comisiones?suma_t_resp_fallos/comisiones:0];
    
    resultado.numeroDeStopSigns = [NSNumber numberWithInt:num_ss];

    resultado.porcentajeAciertos = [NSNumber numberWithFloat:porcentajeAciertos];
    resultado.porcentajeOmisiones = [NSNumber numberWithFloat:porcentajeOmisiones];
    resultado.porcentajeComisiones = [NSNumber numberWithFloat:porcentajeComisiones];
    
    resultado.porcentajeRecuperacion = [NSNumber numberWithFloat:(recuperaciones/total_a_recuperar)*100];
    resultado.recuperacionMedia = [NSNumber numberWithFloat:recuperaciones?(trials_recuperacion/recuperaciones):0];
    */
    return resultado;
}

- (void) setNumber:(NSNumber *) number forKey:(NSString *) key {
    if([number isEqualToNumber:[NSDecimalNumber notANumber]]||[number isEqualToNumber:[NSNumber numberWithFloat:-INFINITY]]||[number isEqualToNumber:[NSNumber numberWithFloat:INFINITY]]) {
        [self setValue:[NSNumber numberWithFloat:0.0] forKey:key];
    } else {
        [self setValue:number forKey:key];
    }
}

+ (ResultadoSesionInvasion *) resultadoAleatorio {
    
    float aciertos;
    float no_comisiones;
    float omisiones;
    float comisiones;
    
    float aleatorio = ((float)rand())/RAND_MAX;
    if(aleatorio>0.4) {
        aciertos = arc4random()%18+1;
        no_comisiones = 18-aciertos;
        omisiones = 1;
        comisiones = 1;
    } else {
        aciertos = 1;
        no_comisiones = 1;
        omisiones = arc4random()%18+1;
        comisiones = 18-omisiones;
    }
    
    float puntuacion = aciertos*100+no_comisiones*50-omisiones*50-comisiones*100;
    puntuacion = puntuacion<0?0:puntuacion;
    float suma_t_resp_aciertos = ((((float)rand())/RAND_MAX)*2.0+0.5)*aciertos;
    float suma_t_resp_fallos = ((((float)rand())/RAND_MAX)*2.0+0.5)*comisiones;
    
    float total_a_recuperar = omisiones+comisiones;
    float recuperaciones = (int)omisiones+comisiones/2;
    float trials_recuperacion = arc4random()%4;
    
    int num_ss = arc4random()%(int)aciertos;
    
    ResultadoSesionInvasion* resultado = [[ResultadoSesionInvasion alloc] init];
    resultado.numeroDeAciertos = [NSNumber numberWithInt:aciertos];
    resultado.numeroDeOmisiones = [NSNumber numberWithInt:omisiones];
    resultado.numeroDeComisiones = [NSNumber numberWithInt:comisiones];
    resultado.numeroDeNoComisiones = [NSNumber numberWithInt:no_comisiones];
    resultado.puntuacion = [NSNumber numberWithInt:puntuacion];
    resultado.tiempoMedioDeRespuestaAciertos = [NSNumber numberWithFloat:aciertos==0?0:suma_t_resp_aciertos/aciertos];
    resultado.tiempoMedioDeRespuestaErrores = [NSNumber numberWithFloat:comisiones==0?0:suma_t_resp_fallos/comisiones];
    
    resultado.numeroDeStopSigns = [NSNumber numberWithInt:num_ss];
    
    float porcentajeAciertos = (no_comisiones/(no_comisiones+comisiones))*(aciertos/(aciertos+omisiones))*100;
    float porcentajeOmisiones = (omisiones/(aciertos+omisiones))*100;
    float porcentajeComisiones = (comisiones/(no_comisiones+comisiones))*100;
    
    if(porcentajeAciertos!=100.0) {
        float newPorcentajeOmisiones = (porcentajeOmisiones*(100-porcentajeAciertos))/(porcentajeOmisiones+porcentajeComisiones);
        float newPorcentajeComisiones = (porcentajeComisiones*(100-porcentajeAciertos))/(porcentajeOmisiones+porcentajeComisiones);
        
        porcentajeOmisiones = newPorcentajeOmisiones;
        porcentajeComisiones = newPorcentajeComisiones;
    }
    resultado.porcentajeAciertos = [NSNumber numberWithFloat:porcentajeAciertos];
    resultado.porcentajeOmisiones = [NSNumber numberWithFloat:porcentajeOmisiones];
    resultado.porcentajeComisiones = [NSNumber numberWithFloat:porcentajeComisiones];
    
    resultado.porcentajeRecuperacion = [NSNumber numberWithFloat:(recuperaciones/total_a_recuperar)*100];
    resultado.recuperacionMedia = [NSNumber numberWithFloat:(trials_recuperacion/recuperaciones)];
    
    return resultado;
}

- (NSDictionary *) valores {
    NSNumber* zero = [NSNumber numberWithInt:0];
    NSDictionary* dictValores = @{
                                  @"puntuacion":self.puntuacion?:zero,
                                  @"duracion":self.duracion?:zero,
                                  @"numeroDeAciertos":self.numeroDeAciertos?:zero,
                                  @"numeroDeComisiones":self.numeroDeComisiones?:zero,
                                  @"numeroDeOmisiones":self.numeroDeOmisiones?:zero,
                                  @"numeroDeNoComisiones":self.numeroDeNoComisiones?:zero,
                                  @"porcentajeAciertos":self.porcentajeAciertos?:zero,
                                  @"porcentajeOmisiones":self.porcentajeOmisiones?:zero,
                                  @"porcentajeComisiones":self.porcentajeComisiones?:zero,
                                  @"porcentajeRecuperacion":self.porcentajeRecuperacion?:zero,
                                  @"recuperacionMedia":self.recuperacionMedia?:zero,
                                  @"tiempoMedioDeRespuestaAciertos":self.tiempoMedioDeRespuestaAciertos?:zero,
                                  @"tiempoMedioDeRespuestaErrores":self.tiempoMedioDeRespuestaErrores?:zero,
                                  @"tiempoMedioDeRespuestaErrorSS":self.tiempoMedioDeRespuestaErrorSS?:zero,
                                  @"mediaDemoraStopSigns":self.mediaDemoraStopSigns?:zero,
                                  @"nombreHito":self.nombreHito?:@"0"
                                  };
    
    return dictValores;
}

@end