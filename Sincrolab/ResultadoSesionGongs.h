//
//  ResultadosSesionGongs.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

@interface ResultadoSesionGongs : NSObject
+ (ResultadoSesionGongs *) resultadoConRespuestas:(NSArray *) respuestasPorTrials;
+ (ResultadoSesionGongs *) resultadoAleatorio;

@property (nonatomic, strong) NSNumber* puntuacion;
@property (nonatomic, strong) NSNumber* duracion;

@property (nonatomic, strong) NSNumber* numeroDeAciertos;
@property (nonatomic, strong) NSNumber* numeroDeComisiones;
@property (nonatomic, strong) NSNumber* numeroDeOmisiones;
@property (nonatomic, strong) NSNumber* numeroDeNoComisiones;
@property (nonatomic, strong) NSNumber* porcentajeAciertos;
@property (nonatomic, strong) NSNumber* porcentajeOmisiones;
@property (nonatomic, strong) NSNumber* porcentajeComisiones;
@property (nonatomic, strong) NSNumber* porcentajeRecuperacion;
@property (nonatomic, strong) NSNumber* recuperacionMedia;
@property (nonatomic, strong) NSNumber* tiempoMedioDeRespuestaAciertos;
@property (nonatomic, strong) NSNumber* tiempoMedioDeRespuestaErrores;

@property (nonatomic, strong) NSNumber* modalidad;
@property (nonatomic, strong) NSNumber* nback;

@property (nonatomic, readonly) NSDictionary* valores;
@end
