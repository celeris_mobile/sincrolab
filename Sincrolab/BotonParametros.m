//
//  BotonParametros.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "BotonParametros.h"
#import "ImageUtils.h"

@interface BotonParametros()
@property (nonatomic, strong) CCSprite* fondo;
@property (nonatomic, strong) CCSprite* sombra;
@property (nonatomic, strong) CCLabelTTF* titulo;
@end

@implementation BotonParametros

- (id) initWithTitle:(NSString *) title {
    self = [super init];
    if(self) {

        CCTexture2D* textureSombra = [[CCTextureCache sharedTextureCache] textureForKey:@"sombraBotonParametros"];
        if(textureSombra) {
            self.sombra = [CCSprite spriteWithTexture:textureSombra];
        } else {
            UIView* sombra = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 361, 75)];
            sombra.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
            sombra.layer.cornerRadius = 4.0;
            sombra.opaque = NO;
            UIImage* imagenSombra = [ImageUtils imageWithView:sombra];
            self.sombra = [CCSprite spriteWithCGImage:imagenSombra.CGImage key:@"sombraBotonParametros"];
        }
        self.sombra.position = CGPointMake(self.sombra.position.x+10, self.sombra.position.y-10);
        [self addChild:self.sombra];
        
        
        CCTexture2D* textureFondo = [[CCTextureCache sharedTextureCache] textureForKey:@"fondoBotonParametros"];
        if(textureFondo) {
            self.fondo = [CCSprite spriteWithTexture:textureFondo];
        } else {
            UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 361, 75)];
            fondo.backgroundColor = [UIColor colorWithRed:244/255.0 green:237/255.0 blue:214/255.0 alpha:1.0];
            fondo.layer.borderWidth = 5.0;
            fondo.layer.borderColor = [UIColor colorWithRed:198/255.0 green:202/255.0 blue:195/255.0 alpha:1.0].CGColor;
            fondo.layer.cornerRadius = 4.0;
            fondo.opaque = NO;
            UIImage* imagenFondo = [ImageUtils imageWithView:fondo];
            self.fondo = [CCSprite spriteWithCGImage:imagenFondo.CGImage key:@"fondoBotonParametros"];
        }
        [self addChild:self.fondo];
        
        self.titulo = [CCLabelTTF labelWithString:title fontName:@"ChauPhilomeneOne-Regular" fontSize:55];
        self.titulo.position = CGPointMake(self.titulo.position.x, self.titulo.position.y);
        self.titulo.color = ccc3(74.0, 74.0, 80.0);
        
        //TODO El stroke no funciona en iOS 7??
        //[self.titulo enableStrokeWithColor:ccRED size:4.0 updateImage:YES];
        [self addChild:self.titulo];

    }
    
    return self;
}

- (CGRect)boundingBox {
    return self.fondo.boundingBox;
}

- (void) presionado {
    self.fondo.position = CGPointMake(self.fondo.position.x+5, self.fondo.position.y-5);
    self.titulo.position = CGPointMake(self.titulo.position.x+5, self.titulo.position.y-5);
    
    self.sombra.visible = NO;
}

- (void) soltado {
    self.fondo.position = CGPointMake(self.fondo.position.x-5, self.fondo.position.y+5);
    self.titulo.position = CGPointMake(self.titulo.position.x-5, self.titulo.position.y+5);
    
    self.sombra.visible = YES;
}

@end
