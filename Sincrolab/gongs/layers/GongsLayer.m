//
//  GongsLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GongsLayer.h"
#import "Gong.h"
#import "EstimuloGongs.h"
#import "SimpleAudioEngine.h"

static CGPoint locations[8] =
{
    (CGPoint){165, 768},
    (CGPoint){391, 768},
    (CGPoint){663, 768},
    (CGPoint){880, 768},
    (CGPoint){176, 387},
    (CGPoint){391, 382},
    (CGPoint){691, 382},
    (CGPoint){891, 342}
};


static CGPoint locationsFarolillos[8] =
{
    (CGPoint){110, 768+160},
    (CGPoint){220, 768},
    (CGPoint){316, 768+235},
    (CGPoint){426, 768+30},
    (CGPoint){573, 768+132},
    (CGPoint){720, 768+229},
    (CGPoint){815, 768},
    (CGPoint){940, 768+156}
};


@interface GongsLayer()
@property (nonatomic, strong) NSArray* gongsArray;
@property (nonatomic, strong) NSArray* estimulos;
@property (nonatomic, strong) NSArray* colores;
@property (nonatomic, strong) NSArray* sonidosNumeros;
@property (nonatomic, strong) NSArray* sonidosLetras;

@property (nonatomic, strong) NSMutableArray* estimulosEmitidos;
@property (nonatomic, strong) NSMutableDictionary* estimulosTrialActual;

@property (nonatomic, strong) NSMutableArray* posiciones;
@end

@implementation GongsLayer {
    int indiceTrialActual;
}

-(id) init {
	if( (self=[super init]) ) {
        
        self.colores = [NSArray arrayWithObjects:
                        [UIColor colorWithRed:146/255.0 green:239/255.0 blue:108/255.0 alpha:1.0],
                        [UIColor colorWithRed:108/255.0 green:230/255.0 blue:239/255.0 alpha:1.0],
                        [UIColor colorWithRed:239/255.0 green:108/255.0 blue:136/255.0 alpha:1.0],
                        [UIColor colorWithRed:253/255.0 green:131/255.0 blue:1/255.0 alpha:1.0],
                        [UIColor colorWithRed:214/255.0 green:77/255.0 blue:255/255.0 alpha:1.0],
                        [UIColor colorWithRed:68/255.0 green:221/255.0 blue:68/255.0 alpha:1.0],
                        [UIColor colorWithRed:92/255.0 green:108/255.0 blue:255/255.0 alpha:1.0],
                        [UIColor colorWithRed:255/255.0 green:238/255.0 blue:77/255.0 alpha:1.0],
                        nil];
        
        //TODO poner al autor de los sonidos en créditos (freesound.org)
        self.sonidosNumeros = [NSArray arrayWithObjects:@"uno.mp3", @"dos.mp3", @"tres.mp3", @"cuatro.mp3", @"cinco.mp3", @"seis.mp3", @"siete.mp3", @"ocho.mp3", @"nueve.mp3", @"diez.mp3", nil];
        self.sonidosLetras = [NSArray arrayWithObjects:@"letra_a.mp3", @"letra_b.mp3", @"letra_e.mp3", @"letra_f.mp3", @"letra_h.mp3", @"letra_i.mp3", @"letra_j.mp3", @"letra_k.mp3", nil];
	}
	return self;
}

- (void)setSesion:(SesionGongs *)sesion {
    _sesion = sesion;
    
    NSMutableArray* auxPosiciones = [[NSMutableArray alloc] init];
    for (int g = 0; g<8; g++) {
        CGPoint pos;
        if(self.sesion.escenario == SesionEscenarioGongsTipoGongs)
            pos = locations[g];
        else
            pos = locationsFarolillos[g];
        
        [auxPosiciones addObject:[NSValue valueWithCGPoint:pos]];
    }
    self.posiciones = auxPosiciones.copy;
    
    
    //Borra anteriores si los hay
    if(self.gongsArray) {
        for (Gong* gong in self.gongsArray) {
            [gong removeFromParent];
        }
    }
    
    //Genera gongs
    if(self.sesion.modalidad==SesionModalidadVisualB) {

        int numGong;
        if(self.sesion.escenario==SesionEscenarioGongsTipoGongs)
            numGong = 5;
        else
            numGong = 4;
        
        Gong* gong = [[Gong alloc] initWithGongNumber:numGong andEscenario:self.sesion.escenario];
        gong.zOrder = 100+numGong;
        gong.position = ((NSValue *)[self.posiciones objectAtIndex:numGong]).CGPointValue;
        gong.tag = numGong;
        gong.visible = NO;
        [self addChild:gong];
        
        CGPoint pos = ((NSValue *)[self.posiciones objectAtIndex:gong.tag]).CGPointValue;
        
        if(pos.x<1024/2)
            pos.x = -200;
        else
            pos.x = 1024+200;
        
        gong.position = pos;
        
            gong.visible = YES;

        self.gongsArray = @[gong];

    } else {
        NSMutableArray* gongsAux = [[NSMutableArray alloc] init];
        NSMutableArray* listaGongs = @[@0, @1, @2, @3, @4, @5, @6, @7].mutableCopy;
        for (int g = 0; g<self.sesion.elementos; g++) {
            int indice = arc4random()%listaGongs.count;
            int numGong = ((NSNumber *)[listaGongs objectAtIndex:indice]).intValue;
            [listaGongs removeObjectAtIndex:indice];
            
            Gong* gong = [[Gong alloc] initWithGongNumber:numGong andEscenario:self.sesion.escenario];
            gong.zOrder = 100+numGong;
            gong.position = ((NSValue *)[self.posiciones objectAtIndex:numGong]).CGPointValue;
            gong.tag = numGong;
            gong.visible = NO;
            [self addChild:gong];
            [gongsAux addObject:gong];
            
            CGPoint pos = ((NSValue *)[self.posiciones objectAtIndex:gong.tag]).CGPointValue;
            
            if(pos.x<1024/2)
                pos.x = -200;
            else
                pos.x = 1024+200;
            
            gong.position = pos;
            
            if(self.sesion.modalidad==SesionModalidadAuditivaA||self.sesion.modalidad==SesionModalidadAuditivaB)
                gong.visible = NO;
            else
                gong.visible = YES;
        }
        self.gongsArray = gongsAux.copy;
    }
    
    //Genera estímulos
    indiceTrialActual = -1;
    self.estimulosEmitidos =  [[NSMutableArray alloc] init];
    
    /*
    int dianas = (int)self.sesion.trials*sesion.porcentaje_dianas;
    int nodianas = self.gongsArray.count-dianas;
    NSMutableArray* estimulosaux = [[NSMutableArray alloc] init];
    for (int e = 0; e<self.sesion.trials; e++) {
        if(e<self.sesion.nback)
            [estimulosaux addObject:[NSNumber numberWithBool:NO]];
        else {
            float aleatorio = (float)rand() / RAND_MAX;
            if((dianas && aleatorio<0.5) || !nodianas) {
                [estimulosaux addObject:[NSNumber numberWithBool:YES]];
            } else
                [estimulosaux addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    self.estimulos = estimulosaux.copy;
     */
    int num_dianas = self.sesion.trials*self.sesion.porcentaje_dianas/100.0;
    
    NSMutableSet* auxdianas = [[NSMutableSet alloc] init];
    for (int d = 0; d<num_dianas; d++) {
        BOOL encontrado = NO;
        while (!encontrado) {
            int aleatorio = (arc4random()%(self.sesion.trials-1))+1;
            NSNumber* number = [NSNumber numberWithInt:aleatorio];
            if(![auxdianas containsObject:number]) {
                [auxdianas addObject:number];
                encontrado = YES;
            }
        }
    }
    
    NSMutableArray* auxtrials = [[NSMutableArray alloc] init];
    for (int t = 0; t<self.sesion.trials; t++) {
        NSNumber* numero = [NSNumber numberWithInt:t];
        if([auxdianas containsObject:numero]) {
            [auxtrials addObject:[NSNumber numberWithBool:YES]];
        } else
            [auxtrials addObject:[NSNumber numberWithBool:NO]];
    }
    
    self.estimulos = auxtrials.copy;
}

- (void) muestra {
    for (Gong* gong in self.gongsArray) {
        float aleatorio = (float)rand() / RAND_MAX;
        CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.8*aleatorio+0.2 position:((NSValue *)[self.posiciones objectAtIndex:gong.tag]).CGPointValue];
        CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
        [gong runAction:ease];
    }
}

- (void) oculta {
    for (Gong* gong in self.gongsArray) {
        CCMoveTo* mueve;
        if(gong.position.x<1024/2)
            mueve = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(-200, gong.position.y)];
        else
            mueve = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024+200, gong.position.y)];
        
        CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
        CCCallBlock* block = [CCCallBlock actionWithBlock:^{
            gong.visible = NO;
        }];
        CCSequence* secuencia = [CCSequence actions:ease, block, nil];
        [gong runAction:secuencia];
    }
}

- (BOOL) lanzaEstimulos {
    indiceTrialActual+=1;

    if(indiceTrialActual>=self.estimulos.count)
        return NO;
    
    EstimuloGongs* estimulo = [[EstimuloGongs alloc] init];
    EstimuloGongs* estimuloBack;
    
    if(indiceTrialActual-self.sesion.nback>=0)
        estimuloBack = [self.estimulosEmitidos objectAtIndex:indiceTrialActual-self.sesion.nback];
    
    BOOL diana;
    
    if(((NSNumber *)[self.estimulos objectAtIndex:indiceTrialActual]).boolValue) {
        //Tiene que ser una diana
        diana = YES;
    } else {
        //Tiene que ser una no diana
        diana = NO;
    }
    
    self.estimulosTrialActual = [[NSMutableDictionary alloc] init];
    
    Gong* gong;
    switch (self.sesion.modalidad) {
        case SesionModalidadVisualA:
            if(diana) {
                estimulo.numGong = estimuloBack.numGong;
            } else {
                estimulo.numGong = arc4random()%self.gongsArray.count;
                if(estimulo.numGong==estimuloBack.numGong)
                    estimulo.numGong+=1;
                if(estimulo.numGong>=self.gongsArray.count)
                    estimulo.numGong=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:diana] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualA]];
            
            gong = [self.gongsArray objectAtIndex:estimulo.numGong];
            [gong lanzaEstimuloAConDuracion:self.sesion.texp];
            break;
        case SesionModalidadVisualB:
            estimulo.numGong = estimuloBack.numGong;// arc4random()%self.gongsArray.count;
            if(diana) {
                estimulo.numColor = estimuloBack.numColor;
            } else {
                estimulo.numColor = arc4random()%self.colores.count;
                if(estimulo.numColor==estimuloBack.numColor)
                    estimulo.numColor+=1;
                if(estimulo.numColor>=self.colores.count)
                    estimulo.numColor=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:diana] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualB]];
            
            
            gong = [self.gongsArray objectAtIndex:estimulo.numGong];
            [gong lanzaEstimuloBConDuracion:self.sesion.texp yColor:[self.colores objectAtIndex:estimulo.numColor]];
            break;
        case SesionModalidadAuditivaA:
            if(diana) {
                estimulo.numNumero = estimuloBack.numNumero;
            } else {
                estimulo.numNumero = arc4random()%self.sonidosNumeros.count;
                if(estimulo.numNumero==estimuloBack.numNumero)
                    estimulo.numNumero+=1;
                if(estimulo.numNumero>=self.sonidosNumeros.count)
                    estimulo.numNumero=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:diana] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoA]];
            
            
            [[SimpleAudioEngine sharedEngine] playEffect:[self.sonidosNumeros objectAtIndex:estimulo.numNumero]];
            break;
        case SesionModalidadAuditivaB:
            if(diana) {
                estimulo.numLetra = estimuloBack.numLetra;
            } else {
                estimulo.numLetra = arc4random()%self.sonidosLetras.count;
                if(estimulo.numLetra==estimuloBack.numLetra)
                    estimulo.numLetra+=1;
                if(estimulo.numLetra>=self.sonidosLetras.count)
                    estimulo.numLetra=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:diana] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoB]];
            
            
            [[SimpleAudioEngine sharedEngine] playEffect:[self.sonidosLetras objectAtIndex:estimulo.numLetra]];
            break;
        case SesionModalidadDual: {
            BOOL dianaVisual;
            BOOL dianaAuditiva;
            if(!diana) {
                dianaVisual = NO;
                dianaAuditiva = NO;
            } else {
                float aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaVisual = YES;
                else
                    dianaVisual = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5||!dianaVisual)
                    dianaAuditiva = YES;
                else
                    dianaAuditiva = NO;
            }
            
            if(dianaVisual) {
                estimulo.numGong = estimuloBack.numGong;
            } else {
                estimulo.numGong = arc4random()%self.gongsArray.count;
                if(estimulo.numGong==estimuloBack.numGong)
                    estimulo.numGong+=1;
                if(estimulo.numGong>=self.gongsArray.count)
                    estimulo.numGong=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaVisual] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualA]];
            
            
            if(dianaAuditiva) {
                estimulo.numNumero = estimuloBack.numNumero;
            } else {
                estimulo.numNumero = arc4random()%self.sonidosNumeros.count;
                if(estimulo.numNumero==estimuloBack.numNumero)
                    estimulo.numNumero+=1;
                if(estimulo.numNumero>=self.sonidosNumeros.count)
                    estimulo.numNumero=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaAuditiva] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoA]];
            
            
            gong = [self.gongsArray objectAtIndex:estimulo.numGong];
            [gong lanzaEstimuloAConDuracion:self.sesion.texp];
            [[SimpleAudioEngine sharedEngine] playEffect:[self.sonidosNumeros objectAtIndex:estimulo.numNumero]];            
            break;
        }
        case SesionModalidadDualVisual: {
            BOOL dianaA;
            BOOL dianaB;
            if(!diana) {
                dianaA = NO;
                dianaB = NO;
            } else {
                float aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaA = YES;
                else
                    dianaA = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5||!dianaA)
                    dianaB = YES;
                else
                    dianaB = NO;
            }
            
            if(dianaA) {
                estimulo.numGong = estimuloBack.numGong;
            } else {
                estimulo.numGong = arc4random()%self.gongsArray.count;
                if(estimulo.numGong==estimuloBack.numGong)
                    estimulo.numGong+=1;
                if(estimulo.numGong>=self.gongsArray.count)
                    estimulo.numGong=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaA] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualA]];
            
            
            if(dianaB) {
                estimulo.numColor = estimuloBack.numColor;
            } else {
                estimulo.numColor = arc4random()%self.colores.count;
                if(estimulo.numColor==estimuloBack.numColor)
                    estimulo.numColor+=1;
                if(estimulo.numColor>=self.colores.count)
                    estimulo.numColor=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaB] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualB]];
            
            
            gong = [self.gongsArray objectAtIndex:estimulo.numGong];
            [gong lanzaEstimuloBConDuracion:self.sesion.texp yColor:[self.colores objectAtIndex:estimulo.numColor]];
            [gong lanzaEstimuloAConDuracion:self.sesion.texp];
            break;
        }
        case SesionModalidadDualAuditiva: {
            BOOL dianaA;
            BOOL dianaB;
            if(!diana) {
                dianaA = NO;
                dianaB = NO;
            } else {
                float aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaA = YES;
                else
                    dianaA = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5||!dianaA)
                    dianaB = YES;
                else
                    dianaB = NO;
            }
            
            if(dianaA) {
                estimulo.numNumero = estimuloBack.numNumero;
            } else {
                estimulo.numNumero = arc4random()%self.sonidosNumeros.count;
                if(estimulo.numNumero==estimuloBack.numNumero)
                    estimulo.numNumero+=1;
                if(estimulo.numNumero>=self.sonidosNumeros.count)
                    estimulo.numNumero=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaA] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoA]];
            
            
            if(dianaB) {
                estimulo.numLetra = estimuloBack.numLetra;
            } else {
                estimulo.numLetra = arc4random()%self.sonidosLetras.count;
                if(estimulo.numLetra==estimuloBack.numLetra)
                    estimulo.numLetra+=1;
                if(estimulo.numLetra>=self.sonidosLetras.count)
                    estimulo.numLetra=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaB] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoB]];
            
            
            SimpleAudioEngine* engine = [SimpleAudioEngine sharedEngine];
            [engine playEffect:[self.sonidosLetras objectAtIndex:estimulo.numLetra]];
            [engine performSelector:@selector(playEffect:) withObject:[self.sonidosNumeros objectAtIndex:estimulo.numNumero]    afterDelay:0.4];
            break;
        }
        case SesionModalidadTripleVisualVisualAuditiva:{
            BOOL dianaA;
            BOOL dianaB;
            BOOL dianaC;
            if(!diana) {
                dianaA = NO;
                dianaB = NO;
                dianaC = NO;
            } else {
                float aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaA = YES;
                else
                    dianaA = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaB = YES;
                else
                    dianaB = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5||(!dianaA&&!dianaB))
                    dianaC = YES;
                else
                    dianaC = NO;
            }
            
            if(dianaA) {
                estimulo.numGong = estimuloBack.numGong;
            } else {
                estimulo.numGong = arc4random()%self.gongsArray.count;
                if(estimulo.numGong==estimuloBack.numGong)
                    estimulo.numGong+=1;
                if(estimulo.numGong>=self.gongsArray.count)
                    estimulo.numGong=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaA] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualA]];
            
            
            if(dianaB) {
                estimulo.numColor = estimuloBack.numColor;
            } else {
                estimulo.numColor = arc4random()%self.colores.count;
                if(estimulo.numColor==estimuloBack.numColor)
                    estimulo.numColor+=1;
                if(estimulo.numColor>=self.colores.count)
                    estimulo.numColor=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaB] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualB]];
            
            
            if(dianaC) {
                estimulo.numNumero = estimuloBack.numNumero;
            } else {
                estimulo.numNumero = arc4random()%self.sonidosNumeros.count;
                if(estimulo.numNumero==estimuloBack.numNumero)
                    estimulo.numNumero+=1;
                if(estimulo.numNumero>=self.sonidosNumeros.count)
                    estimulo.numNumero=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaC] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoA]];
            
            
            gong = [self.gongsArray objectAtIndex:estimulo.numGong];
            [gong lanzaEstimuloBConDuracion:self.sesion.texp yColor:[self.colores objectAtIndex:estimulo.numColor]];
            [gong lanzaEstimuloAConDuracion:self.sesion.texp];
            [[SimpleAudioEngine sharedEngine] playEffect:[self.sonidosNumeros objectAtIndex:estimulo.numNumero]];
            break;
        }
        case SesionModalidadTripleAuditivaAuditivaVisual: {
            BOOL dianaA;
            BOOL dianaB;
            BOOL dianaC;
            if(!diana) {
                dianaA = NO;
                dianaB = NO;
                dianaC = NO;
            } else {
                float aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaA = YES;
                else
                    dianaA = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5)
                    dianaB = YES;
                else
                    dianaB = NO;
                
                aleatorio = (float)rand() / RAND_MAX;
                if(aleatorio<0.5||(!dianaA&&!dianaB))
                    dianaC = YES;
                else
                    dianaC = NO;
            }
            
            if(dianaA) {
                estimulo.numNumero = estimuloBack.numNumero;
            } else {
                estimulo.numNumero = arc4random()%self.sonidosNumeros.count;
                if(estimulo.numNumero==estimuloBack.numNumero)
                    estimulo.numNumero+=1;
                if(estimulo.numNumero>=self.sonidosNumeros.count)
                    estimulo.numNumero=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaA] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoA]];
            
            
            if(dianaB) {
                estimulo.numLetra = estimuloBack.numLetra;
            } else {
                estimulo.numLetra = arc4random()%self.sonidosLetras.count;
                if(estimulo.numLetra==estimuloBack.numLetra)
                    estimulo.numLetra+=1;
                if(estimulo.numLetra>=self.sonidosLetras.count)
                    estimulo.numLetra=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaB] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonAuditivoB]];
            
            
            if(dianaC) {
                estimulo.numGong = estimuloBack.numGong;
            } else {
                estimulo.numGong = arc4random()%self.gongsArray.count;
                if(estimulo.numGong==estimuloBack.numGong)
                    estimulo.numGong+=1;
                if(estimulo.numGong>=self.gongsArray.count)
                    estimulo.numGong=0;
            }
            
            [self.estimulosTrialActual setObject:[NSNumber numberWithBool:dianaC] forKey:[NSNumber numberWithInt:BotonesLayerTipoBotonVisualA]];
            
            
            gong = [self.gongsArray objectAtIndex:estimulo.numGong];
            [gong lanzaEstimuloAConDuracion:self.sesion.texp];
            SimpleAudioEngine* engine = [SimpleAudioEngine sharedEngine];
            [engine playEffect:[self.sonidosLetras objectAtIndex:estimulo.numLetra]];
            [engine performSelector:@selector(playEffect:) withObject:[self.sonidosNumeros objectAtIndex:estimulo.numNumero]    afterDelay:0.4];

            break;
        }
    }

    [self.estimulosEmitidos addObject:estimulo];
    
    return YES;
}

- (NSDictionary *)dianasTrialActual {
    return self.estimulosTrialActual.copy;
}

@end
