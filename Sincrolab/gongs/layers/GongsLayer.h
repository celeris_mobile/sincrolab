//
//  GongsLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "EstadoJuegoGongs.h"
#import "SesionGongs.h"
#import "BotonesLayer.h"

@interface GongsLayer : CCLayer
@property (nonatomic, strong) SesionGongs* sesion;
- (void) muestra;
- (void) oculta;
- (BOOL) lanzaEstimulos;
- (NSDictionary *) dianasTrialActual;
@end
