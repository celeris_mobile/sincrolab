//
//  Instrucciones.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TutorialGongs.h"


@interface TutorialGongs()
@property (nonatomic, strong) InstruccionesLayer* instrucciones;
@property (nonatomic, strong) GongsLayer* gongs;
@property (nonatomic, strong) BotonesLayer* botones;
@property (nonatomic, strong) NSArray* mensajes;
@end

@implementation TutorialGongs {
    BOOL isInDemo;
    NSInteger indice_mensaje;
    NSInteger trials_reales;
}

- (id) initWithGongs:(GongsLayer *) gongs andBotones:(BotonesLayer *) botones {
    self = [super init];
    if(self) {
        self.gongs = gongs;
        self.botones = botones;
        isInDemo = NO;
        
        indice_mensaje = 0;

    }
    return self;
}

- (void)setSesion:(SesionGongs *)sesion {
    _sesion = sesion;
    trials_reales = self.sesion.trials;
    self.sesion.trials = 1000;
    
    if(self.sesion.nback==1) {
        if(self.sesion.modalidad==SesionModalidadVisualA) {
            self.mensajes = @[@"Fíjate en estos [elemento]s se van a [verbo] de uno en uno",
                              @"[inicia_demo]",
                              @"Si se [accion] el mismo [elemento] dos o más veces seguidas, tienes que darle al botón",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadVisualB) {
            self.mensajes = @[@"Fíjate en este [elemento] se va a [verbo]",
                              @"[inicia_demo]",
                              @"Si se repite el mismo color dos o más veces seguidas, tienes que darle al botón",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadAuditivaA || self.sesion.modalidad==SesionModalidadAuditivaB) {
            self.mensajes = @[@"Escucha atentamente, oirás un[genero]s [audio]s",
                              @"[inicia_demo]",
                              @"Si se escucha [articulo] mism[genero] [audio] dos o más veces seguidas, tienes que darle al botón",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadDual) {
            self.mensajes =  @[@"Fíjate en estos [elemento]s se van a mover de uno en uno",
                               @"Además escucha atentamente, oirás un[genero]s [audio]s",
                               @"[inicia_demo]",
                               @"Si se mueve el mismo [elemento] o se escucha [articulo] mism[genero] [audio] dos o más veces seguidas...",
                               @"...tienes que darle al botón correspondiente, dependiendo de lo que haya sucedido",
                               @"[demo]",
                               @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                               ];
        }
        if(self.sesion.modalidad==SesionModalidadDualVisual) {
            self.mensajes =  @[@"Fíjate en estos [elemento]s se van a iluminar y a mover de uno en uno",
                               @"[inicia_demo]",
                               @"Si se mueve el mismo [elemento] dos o más veces seguidas o se repite el color...",
                               @"...tienes que darle al botón correspondiente, dependiendo de lo que haya sucedido",
                               @"[demo]",
                               @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                               ];
        }
        if(self.sesion.modalidad==SesionModalidadDualAuditiva) {
            self.mensajes =  @[@"Escucha atentamente, oirás unos números y unas letras",
                               @"[inicia_demo]",
                               @"Si se escucha el mismo número o la misma letra dos o más veces seguidas...",
                               @"...tienes que darle al botón correspondiente, dependiendo de lo que haya sucedido",
                               @"[demo]",
                               @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                               ];
        }
        if(self.sesion.modalidad==SesionModalidadTripleVisualVisualAuditiva) {
            self.mensajes = @[@"Tienes que estar muy atento ahora, porque sucederán tres cosas diferentes a la vez",
                              @"Los [elemento]s se moverán, se iluminarán y además escucharás unas letras",
                              @"[inicia_demo]",
                              @"Recuerda, si un mismo elemento vuelve a moverse, se repite el color o escuchas la misma letra...",
                              @"...tienes que darle al botón que corresponda",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadTripleAuditivaAuditivaVisual) {
            self.mensajes = @[@"Tienes que estar muy atento ahora, porque sucederán tres cosas diferentes a la vez",
                              @"Escucharás unas letras y unos números, además los [elemento]s se moverán",
                              @"[inicia_demo]",
                              @"Recuerda, si un mismo elemento vuelve a moverse, se repite el color o escuchas la misma letra...",
                              @"...tienes que darle al botón que corresponda",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
    }
    
    if(self.sesion.nback>1) {
        if(self.sesion.modalidad==SesionModalidadVisualA) {
            self.mensajes = @[@"Ahora aumenta el reto",
                              @"Fíjate en estos [elemento]s se van a mover de uno en uno",
                              @"[inicia_demo]",
                              @"Si se mueve el mismo [elemento] dejando [nback-1] en medio, tienes que darle al botón",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadVisualB) {
            self.mensajes = @[@"Fíjate en estos [elemento]s se van a iluminar de uno en uno",
                              @"[inicia_demo]",
                              @"Si se repite el mismo color dejando [nback-1] en medio, tienes que darle al botón",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadAuditivaA || self.sesion.modalidad==SesionModalidadAuditivaB) {
            self.mensajes = @[@"Escucha atentamente, oirás un[genero]s [audio]s",
                              @"[inicia_demo]",
                              @"Si se escucha [articulo] mism[genero] [audio] dejando [nback-1] en medio, tienes que darle al botón",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadDual) {
            self.mensajes =  @[@"Fíjate en estos [elemento]s se van a mover de uno en uno",
                               @"Además escucha atentamente, oirás un[genero]s [audio]s",
                               @"[inicia_demo]",
                               @"Si se mueve el mismo [elemento] o se escucha [articulo] mism[genero] [audio] dejando [nback-1] en medio...",
                               @"...tienes que darle al botón correspondiente, dependiendo de lo que haya sucedido",
                               @"[demo]",
                               @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                               ];
        }
        if(self.sesion.modalidad==SesionModalidadDualVisual) {
            self.mensajes =  @[@"Fíjate en estos [elemento]s se van a iluminar y a mover de uno en uno",
                               @"[inicia_demo]",
                               @"Si se mueve el mismo [elemento] o se repite el color dejando [nback-1] en medio...",
                               @"...tienes que darle al botón correspondiente, dependiendo de lo que haya sucedido",
                               @"[demo]",
                               @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                               ];
        }
        if(self.sesion.modalidad==SesionModalidadDualAuditiva) {
            self.mensajes =  @[@"Escucha atentamente, oirás unos números y unas letras",
                               @"[inicia_demo]",
                               @"Si se escucha el mismo número o la misma letra dejando [nback-1] en medio...",
                               @"...tienes que darle al botón correspondiente, dependiendo de lo que haya sucedido",
                               @"[demo]",
                               @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                               ];
        }
        if(self.sesion.modalidad==SesionModalidadTripleVisualVisualAuditiva) {
            self.mensajes = @[@"Tienes que estar muy atento ahora, porque sucederán tres cosas diferentes a la vez",
                              @"Los [elemento]s se moverán, se iluminarán y además escucharás unas letras",
                              @"[inicia_demo]",
                              @"Recuerda, si un mismo elemento vuelve a moverse, se repite el color o escuchas la misma letra...",
                              @"...dejando [nback-1] en medio, tienes que darle al botón que corresponda",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
        if(self.sesion.modalidad==SesionModalidadTripleAuditivaAuditivaVisual) {
            self.mensajes = @[@"Tienes que estar muy atento ahora, porque sucederán tres cosas diferentes a la vez",
                              @"Escucharás unas letras y unos números, además los [elemento]s se moverán",
                              @"[inicia_demo]",
                              @"Recuerda, si un mismo elemento vuelve a moverse, se repite el color o escuchas la misma letra...",
                              @"...dejando [nback-1] en medio tienes que darle al botón que corresponda",
                              @"[demo]",
                              @"Parece que estás listo, dale a Comenzar cuando quieras empezar a jugar"
                              ];
        }
    }
    
    
    
    self.instrucciones = [[InstruccionesLayer alloc] init];
    self.instrucciones.delegate = self;
    [self addChild:self.instrucciones];
    
    self.gongs.sesion = self.sesion;
    self.botones.sesion = self.sesion;
}

- (void) muestra {
    [self.gongs muestra];
    [self.botones muestra];
    
    [self muestraMensaje];
}

- (void) muestraMensaje {

    NSString* mensaje = [self.mensajes objectAtIndex:indice_mensaje];
    isInDemo = NO;
    
    if([mensaje isEqualToString:@"[inicia_demo]"]) {
        indice_mensaje+=1;
        [self.gongs lanzaEstimulos];
        [self schedule:@selector(lanzaEstimulo) interval:2.0];
        [self performSelector:@selector(muestraMensaje) withObject:nil afterDelay:5.0];
        return;
    }
    if([mensaje isEqualToString:@"[demo]"]) {
        indice_mensaje+=1;
        isInDemo = YES;
        return;
    }
    
    mensaje = [self reemplazaStrings:mensaje];
    indice_mensaje+=1;
    
    if(indice_mensaje>=self.mensajes.count)
        [self.instrucciones muestraConTexto:mensaje tipoBoton:TipoBotonTerminar];
    else
        [self.instrucciones muestraConTexto:mensaje tipoBoton:TipoBotonSiguiente];
}

- (void) lanzaEstimulo {
    [self.gongs lanzaEstimulos];

    if(isInDemo) {
        [self.botones muestraHint:self.gongs.dianasTrialActual];
    }
}

- (void) acierto {
    if(isInDemo)
        [self muestraMensaje];
}

- (void) error {
    if(isInDemo) {
        indice_mensaje-=1;
        [self muestraMensaje];
    }
}

- (void) botonSiguienteDado:(InstruccionesLayer *)instrucciones {
    [self.instrucciones oculta];
    if(indice_mensaje>=self.mensajes.count) {
        [self unscheduleAllSelectors];
        [self unscheduleUpdate];
        self.sesion.trials = trials_reales;
        
        [self.delegate tutorialFinalizado];
    } else
        [self performSelector:@selector(muestraMensaje) withObject:nil afterDelay:0.5];
}

- (NSString *) reemplazaStrings:(NSString *) mensaje {
    NSString* accion = @"";
    NSString* verbo = @"";
    NSString* audio = @"";
    NSString* genero = @"";
    NSString* articulo = @"";
    NSString* accion2 = @"";
    NSString* verbo2 = @"";
    NSString* audio2 = @"";
    
    switch (self.sesion.modalidad) {
        case SesionModalidadVisualA:
            accion = @"mueve";
            verbo = @"mover";
            break;
        case SesionModalidadVisualB:
            accion = @"ilumina";
            verbo = @"iluminar";
            break;
        case SesionModalidadAuditivaA:
            audio = @"número";
            genero = @"o";
            articulo = @"el";
            break;
        case SesionModalidadAuditivaB:
            audio = @"letra";
            genero = @"a";
            articulo = @"la";
            break;
        case SesionModalidadDual:
            accion = @"mueve";
            verbo = @"mover";
            audio = @"número";
            genero = @"o";
            articulo = @"el";
            break;
        case SesionModalidadDualVisual:
            accion = @"mueve";
            verbo = @"mover";
            accion2 = @"ilumina";
            verbo2 = @"iluminar";
            break;
        case SesionModalidadDualAuditiva:
            audio = @"número";
            audio2 = @"letra";
            break;
        case SesionModalidadTripleVisualVisualAuditiva:
            accion = @"mueve";
            verbo = @"mover";
            accion2 = @"ilumina";
            verbo2 = @"iluminar";
            audio = @"número";
            genero = @"o";
            articulo = @"el";
            break;
        case SesionModalidadTripleAuditivaAuditivaVisual:
            accion = @"mueve";
            verbo = @"mover";
            audio = @"número";
            audio2 = @"letra";
            break;
    }

    NSString* elemento;
    if(self.sesion.escenario == SesionEscenarioGongsTipoGongs)
        elemento = @"gong";
    else
        elemento = @"farolillo";
    
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[accion]" withString:accion];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[verbo]" withString:verbo];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[audio]" withString:audio];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[genero]" withString:genero];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[articulo]" withString:articulo];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[accion2]" withString:accion2];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[verbo2]" withString:verbo2];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[audio2]" withString:audio2];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[elemento]" withString:elemento];
    mensaje = [mensaje stringByReplacingOccurrencesOfString:@"[nback-1]" withString:[NSString stringWithFormat:@"%d", self.sesion.nback-1]];
    
    return mensaje;
}

@end
