//
//  Instrucciones.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionGongs.h"
#import "InstruccionesLayer.h"
#import "GongsLayer.h"
#import "BotonesLayer.h"

@protocol TutorialGongsDelegate <NSObject>
- (void) tutorialFinalizado;
@end

@interface TutorialGongs : CCLayer <InstruccionesLayerDelegate>
@property (nonatomic, strong) SesionGongs* sesion;
@property (nonatomic, weak) id<TutorialGongsDelegate> delegate;
- (id) initWithGongs:(GongsLayer *) gongs andBotones:(BotonesLayer *) botones;
- (void) muestra;
- (void) acierto;
- (void) error;
@end
