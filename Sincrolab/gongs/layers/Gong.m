//
//  Gong.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Gong.h"
static CGPoint posColores[8] =
{
    (CGPoint){0, -227},
    (CGPoint){-4, -262},
    (CGPoint){-2, -284},
    (CGPoint){-2, -248},
    (CGPoint){-4, -106},
    (CGPoint){3, -136},
    (CGPoint){-5, -119},
    (CGPoint){0, -64}
};



@interface Gong()
@property (nonatomic, assign) int gongNumber;
@property (nonatomic, assign) SesionEscenarioGongsTipo escenario;
@property (nonatomic, strong) CCSprite* sprite;
@property (nonatomic, strong) CCSprite* simbolo;
@property (nonatomic, strong) CCSprite* vibracion;
@end

@implementation Gong
@synthesize sprite = _sprite;

- (id)initWithGongNumber:(int) gongNumber andEscenario:(SesionEscenarioGongsTipo) escenario {
    self = [super init];
    if (self) {
        
        self.gongNumber = gongNumber;
        self.escenario = escenario;
        
        if(self.escenario==SesionEscenarioGongsTipoGongs) {
            NSArray* tamanyos = @[@"m", @"l", @"s", @"m", @"s", @"l", @"m", @"s"];
            
            self.sprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"gong%d.png", gongNumber+1]];
            self.sprite.anchorPoint = CGPointMake(0.5, 1);
            
            self.simbolo = [CCSprite spriteWithFile:[NSString stringWithFormat:@"simbolo_%@.png", [tamanyos objectAtIndex:gongNumber]]];
            self.simbolo.position = posColores[gongNumber];
            
            self.vibracion = [CCSprite spriteWithFile:[NSString stringWithFormat:@"vibracion_%@.png", [tamanyos objectAtIndex:gongNumber]]];
            self.vibracion.position = posColores[gongNumber];
        } else {
            self.sprite = [CCSprite spriteWithFile:@"farolillo.png"];
            self.sprite.anchorPoint = CGPointMake(0.5, 1);
            
            self.simbolo = [CCSprite spriteWithFile:@"color_farolillo.png"];
            self.simbolo.position = CGPointMake(0.0, -505);
            
            self.vibracion = [CCSprite spriteWithFile:@"onda_farolillo.png"];
            self.vibracion.position = CGPointMake(0.0, -505);
        }

        self.simbolo.opacity = 0.0;
        self.vibracion.visible = NO;

        [self addChild:self.vibracion];
        [self addChild:self.sprite];
        [self addChild:self.simbolo];

    }
    return self;
}

- (void) lanzaEstimuloAConDuracion:(float) duracion {

    self.vibracion.visible = YES;
    self.vibracion.scale = 0.0;
    
    CCShaky3D* action = [CCShaky3D actionWithDuration:duracion+0.2 size:CGSizeMake(15, 10) range:4 shakeZ:NO];
    CCStopGrid* stop = [CCStopGrid action];
    CCSequence* gridSequence = [CCSequence actions:action, stop, nil];
    [self runAction:gridSequence];
    
    CCScaleTo* escala1 = [CCScaleTo actionWithDuration:duracion/2.0 scale:1.0];
    CCScaleTo* escala2 = [CCScaleTo actionWithDuration:duracion/2.0 scale:0.8];
    CCSequence* escala = [CCSequence actions:escala1, escala2, nil];
    CCRepeatForever* repite = [CCRepeatForever actionWithAction:escala];
    
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCDelayTime* delay = [CCDelayTime actionWithDuration:duracion];
    CCFadeOut *fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSequence* secuencia = [CCSequence actions:fadeIn, delay, fadeOut, nil];
    
    [self.vibracion runAction:repite];
    [self.vibracion runAction:secuencia];
    
}

- (void) lanzaEstimuloBConDuracion:(float) duracion yColor:(UIColor *) color {
    
    const float* colors = CGColorGetComponents(color.CGColor );
    self.simbolo.color = ccc3(colors[0]*255.0, colors[1]*255.0, colors[2]*255.0);
    
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCDelayTime* delay = [CCDelayTime actionWithDuration:duracion];
    CCFadeOut *fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSequence* secuencia = [CCSequence actions:fadeIn, delay, fadeOut, nil];
    
    [self.simbolo runAction:secuencia];
    
}

@end
