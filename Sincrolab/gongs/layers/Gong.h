//
//  Gong.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
#import "SesionGongs.h"

@interface Gong : CCNode
- (id)initWithGongNumber:(int) gongNumber andEscenario:(SesionEscenarioGongsTipo) escenario;
- (void) lanzaEstimuloAConDuracion:(float) duracion;
- (void) lanzaEstimuloBConDuracion:(float) duracion yColor:(UIColor *) color;
@end
