//
//  TiempoLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TiempoLayer.h"

@interface TiempoLayer()
@property (nonatomic, strong) CCLabelTTF* tiempoLabel;
@property (nonatomic, strong) CCSprite* fondo;
@end

@implementation TiempoLayer {
    float tiempo;
}

- (id)init {
    self = [super init];
    if(self) {
        self.fondo = [CCSprite spriteWithFile:@"fondo_tiempo.png"];
        self.fondo.position = CGPointMake(1024/2, 768-85);
        [self addChild:self.fondo];
        
        self.tiempoLabel = [CCLabelTTF labelWithString:@"00:40" fontName:@"Lato-Bold" fontSize:58];
        self.tiempoLabel.position = self.fondo.position;
        [self addChild:self.tiempoLabel];
        
        self.position = CGPointMake(0, 200);
    }
    
    return self;
}

- (void)setTiempoTotal:(float) tiempoTotal  {
    tiempo = tiempoTotal;//
    [self actualizaTiempo];
}

- (void) decrementa:(ccTime)delta {
    tiempo -= delta;
    [self actualizaTiempo];
}

- (void) actualizaTiempo {
    if(tiempo<0) {
        [self.tiempoLabel setString:@"00:00"];
        return;
    }
    
    int segundosTrascurridos = (int) tiempo;
    int minutos = (int) floor(segundosTrascurridos/60.0);
    int secs = tiempo-minutos*60;
    NSString* minutos_str = [NSString stringWithFormat:@"%d", minutos];
    NSString* segundos_str = [NSString stringWithFormat:@"%d", secs];
    minutos_str = minutos_str.length==2?minutos_str:[@"0" stringByAppendingString:minutos_str];
    segundos_str = segundos_str.length==2?segundos_str:[@"0" stringByAppendingString:segundos_str];
    [self.tiempoLabel setString:[NSString stringWithFormat:@"%@:%@", minutos_str, segundos_str]];
}

- (void) muestra {
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.4 position:CGPointZero];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueveIn];
    [self runAction:ease];
}

- (void) oculta {
    CCFadeOut *mueveOut = [CCMoveTo actionWithDuration:0.4 position:CGPointMake(0, 200)];
    CCEaseExponentialIn* ease = [CCEaseExponentialIn actionWithAction:mueveOut];    
    [self runAction:ease];
}
@end
