//
//  Distractores.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Distractores.h"
#import "Colibri.h"
#import "PajaronMarron.h"
#import "PajaroVerde.h"
#import "PajaroRojo.h"
#import "Nave.h"

@interface Distractores()
@property (nonatomic, strong) Distractor* distractor;
@end

@implementation Distractores

- (id) init {
    self = [super init];
    if(self) {

    }
    return self;
}

- (void) lanza {
    float aleatorio = (float)rand() / RAND_MAX;
    if(aleatorio<0.2) {
        self.distractor = [[Colibri alloc] init];
    } else if (aleatorio<0.4) {
        self.distractor = [[PajaronMarron alloc] init];
    } else if (aleatorio<0.6) {
        self.distractor = [[PajaroVerde alloc] init];
    } else if (aleatorio<0.8) {
        self.distractor = [[PajaroRojo alloc] init];
    } else {
        self.distractor = [[Nave alloc] init];
    }
    
    self.distractor.delegate = self;
    [self addChild:self.distractor];
    [self.distractor lanza];
}

- (void)distractorHaTerminado:(Distractor *)distractor {
    distractor.delegate = nil;
    [distractor removeFromParent];
    distractor = nil;
}

@end
