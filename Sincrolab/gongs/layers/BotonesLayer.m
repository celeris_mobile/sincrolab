//
//  Botones.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "BotonesLayer.h"

@interface BotonesLayer()
@property (nonatomic, strong) CCSprite* fondoIzq;
@property (nonatomic, strong) CCSprite* fondoDer;

@property (nonatomic, strong) CCSprite* btnVisualA;
@property (nonatomic, strong) CCSprite* btnVisualB;
@property (nonatomic, strong) CCSprite* btnVisualA_doble;
@property (nonatomic, strong) CCSprite* btnVisualB_doble;
@property (nonatomic, strong) CCSprite* btnAuditivoA;
@property (nonatomic, strong) CCSprite* btnAuditivoB;
@property (nonatomic, strong) CCSprite* btnAuditivoA_doble;
@property (nonatomic, strong) CCSprite* btnAuditivoB_doble;

@property (nonatomic, strong) NSArray* botones;
@property (nonatomic, strong) CCSprite* presionado;

@property (nonatomic, strong) CCSprite* flecha;

@end

@implementation BotonesLayer

- (id)init {
    self = [super init];
    if(self) {
        self.fondoIzq = [CCSprite spriteWithFile:@"fondo_botonera.png"];
        self.fondoIzq.anchorPoint = CGPointMake(0, 0);
        self.fondoIzq.position = CGPointMake(0, 0);
        self.fondoIzq.visible = NO;
        [self addChild:self.fondoIzq];
        
        self.fondoDer = [CCSprite spriteWithFile:@"fondo_botonera.png"];
        self.fondoDer.anchorPoint = CGPointMake(0, 0);
        self.fondoDer.scaleX = -1;
        self.fondoDer.position = CGPointMake(1024, 0);
        self.fondoDer.visible = NO;
        [self addChild:self.fondoDer];
        
        self.btnVisualA = [CCSprite spriteWithFile:@"btn_visuala.png"];
        self.btnVisualA.position = CGPointMake(1024-96, 96);
        self.btnVisualA.visible = NO;
        [self addChild:self.btnVisualA];
        
        self.btnVisualB = [CCSprite spriteWithFile:@"btn_visualb.png"];
        self.btnVisualB.position = CGPointMake(1024-96, 96);
        self.btnVisualB.visible = NO;
        [self addChild:self.btnVisualB];
        
        self.btnVisualA_doble = [CCSprite spriteWithFile:@"btn_visuala_doble.png"];
        self.btnVisualA_doble.position = CGPointMake(1024-161, 66);
        self.btnVisualA_doble.visible = NO;
        [self addChild:self.btnVisualA_doble];
        
        self.btnVisualB_doble = [CCSprite spriteWithFile:@"btn_visualb_doble.png"];
        self.btnVisualB_doble.position = CGPointMake(1024-67, 150);
        self.btnVisualB_doble.visible = NO;
        [self addChild:self.btnVisualB_doble];
        
        self.btnAuditivoA = [CCSprite spriteWithFile:@"btn_audio_numeros.png"];
        self.btnAuditivoA.position = CGPointMake(96, 96);
        self.btnAuditivoA.visible = NO;
        [self addChild:self.btnAuditivoA];

        self.btnAuditivoB = [CCSprite spriteWithFile:@"btn_audio_letras.png"];
        self.btnAuditivoB.position = CGPointMake(96, 96);
        self.btnAuditivoB.visible = NO;
        [self addChild:self.btnAuditivoB];
        
        
        self.btnAuditivoA_doble = [CCSprite spriteWithFile:@"btn_audio_numeros_doble.png"];
        self.btnAuditivoA_doble.position = CGPointMake(161, 66);
        self.btnAuditivoA_doble.visible = NO;
        [self addChild:self.btnAuditivoA_doble];
        
        self.btnAuditivoB_doble = [CCSprite spriteWithFile:@"btn_audio_letras_doble.png"];
        self.btnAuditivoB_doble.position = CGPointMake(67, 150);
        self.btnAuditivoB_doble.visible = NO;
        [self addChild:self.btnAuditivoB_doble];
        
        self.botones = [NSArray arrayWithObjects:self.btnVisualA, self.btnVisualB, self.btnVisualA_doble, self.btnVisualB_doble, self.btnAuditivoA, self.btnAuditivoB, self.btnAuditivoA_doble, self.btnAuditivoB_doble, nil];
        
        [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self
                                                                priority:0
                                                         swallowsTouches:NO];
        
        self.flecha = [CCSprite spriteWithFile:@"flecha.png"];
        self.flecha.opacity = 0.0;
        [self addChild:self.flecha];
        
        self.visible = NO;
    }
    
    return self;
}

- (void) setSesion:(SesionGongs *)sesion {
    _sesion = sesion;
    self.flecha.opacity = 0.0;

    self.fondoIzq.visible = NO;
    self.fondoDer.visible = NO;
    self.btnVisualA.visible = NO;
    self.btnVisualB.visible = NO;
    self.btnVisualA_doble.visible = NO;
    self.btnVisualB_doble.visible = NO;
    self.btnAuditivoA.visible = NO;
    self.btnAuditivoB.visible = NO;
    self.btnAuditivoA_doble.visible = NO;
    self.btnAuditivoB_doble.visible = NO;
    
    switch (self.sesion.modalidad) {
        case SesionModalidadVisualA:
            self.fondoDer.visible = YES;
            self.btnVisualA.visible = YES;
            break;
        case SesionModalidadVisualB:
            self.fondoDer.visible = YES;
            self.btnVisualB.visible = YES;
            break;
        case SesionModalidadAuditivaA:
            self.fondoIzq.visible = YES;
            self.btnAuditivoA.visible = YES;
            break;
        case SesionModalidadAuditivaB:
            self.fondoIzq.visible = YES;
            self.btnAuditivoB.visible = YES;
            break;
        case SesionModalidadDual:
            self.fondoIzq.visible = YES;
            self.fondoDer.visible = YES;
            self.btnVisualA.visible = YES;
            self.btnAuditivoA.visible = YES;
            break;
        case SesionModalidadDualVisual:
            self.fondoDer.visible = YES;
            self.btnVisualA_doble.visible = YES;
            self.btnVisualB_doble.visible = YES;
            break;
        case SesionModalidadDualAuditiva:
            self.fondoIzq.visible = YES;
            self.btnAuditivoA_doble.visible = YES;
            self.btnAuditivoB_doble.visible = YES;
            break;
        case SesionModalidadTripleVisualVisualAuditiva:
            self.fondoIzq.visible = YES;
            self.fondoDer.visible = YES;
            self.btnVisualA_doble.visible = YES;
            self.btnVisualB_doble.visible = YES;
            self.btnAuditivoA.visible = YES;
            break;
        case SesionModalidadTripleAuditivaAuditivaVisual:
            self.fondoIzq.visible = YES;
            self.fondoDer.visible = YES;
            self.btnAuditivoA_doble.visible = YES;
            self.btnAuditivoB_doble.visible = YES;
            self.btnVisualA.visible = YES;
            break;

    }
}

- (void) muestra {
    self.position = CGPointMake(0, -400);
    self.visible = YES;
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.4 position:CGPointZero];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
    [self runAction:ease];
}

- (void) oculta {
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.4 position:CGPointMake(0, -400)];
    CCEaseExponentialIn* ease = [CCEaseExponentialIn actionWithAction:mueve];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        self.visible = NO;
    }];
    CCSequence* sequence = [CCSequence actions:ease, callBlock, nil];
    [self runAction:sequence];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    if(!self.visible)
        return NO;
    
    CGPoint location = [self convertTouchToNodeSpace: touch];
    
    BOOL isTouchHandled = NO;
    for (CCSprite* boton in self.botones) {
        isTouchHandled = CGRectContainsPoint(boton.boundingBox, location) && boton.visible;
        if (isTouchHandled) {
            self.presionado = boton;
            self.presionado.opacity = 150;

            if(self.presionado==self.btnVisualA_doble || self.presionado==self.btnVisualA)
                [self.delegate botonDado:BotonesLayerTipoBotonVisualA];
            if(self.presionado==self.btnVisualB_doble || self.presionado==self.btnVisualB)
                [self.delegate botonDado:BotonesLayerTipoBotonVisualB];
            if(self.presionado==self.btnAuditivoA || self.presionado==self.btnAuditivoA_doble)
                [self.delegate botonDado:BotonesLayerTipoBotonAuditivoA];
            if(self.presionado==self.btnAuditivoB || self.presionado==self.btnAuditivoB_doble)
                [self.delegate botonDado:BotonesLayerTipoBotonAuditivoB];
            
        }
    }
    
    
    return isTouchHandled || self.presionado.visible;
}

- (void)ccTouchEnded:(UITouch*)touch withEvent:(UIEvent *)event {
    if(!self.visible)
        return ;
    
    self.presionado.opacity = 255;
    self.presionado = nil;
}

- (void) muestraHint:(NSDictionary *) dianas {
    CCSprite* boton;
    
    for (NSNumber* estimulo in dianas) {
        BOOL diana = ((NSNumber *)[dianas objectForKey:estimulo]).boolValue;
        if(diana) {
            BotonesLayerTipoBoton tipo = estimulo.intValue;

            switch (tipo) {
                case BotonesLayerTipoCualquiera:
                    break;
                case BotonesLayerTipoBotonVisualA:
                    boton = self.btnVisualA;
                    if(self.sesion.modalidad==SesionModalidadDualVisual || self.sesion.modalidad==SesionModalidadTripleVisualVisualAuditiva)
                        boton = self.btnVisualA_doble;
                    break;
                case BotonesLayerTipoBotonVisualB:
                    boton = self.btnVisualB;
                    if(self.sesion.modalidad==SesionModalidadDualVisual || self.sesion.modalidad==SesionModalidadTripleVisualVisualAuditiva)
                        boton = self.btnVisualB_doble;
                    break;
                case BotonesLayerTipoBotonAuditivoA:
                    boton = self.btnAuditivoA;
                    if(self.sesion.modalidad==SesionModalidadDualAuditiva || self.sesion.modalidad==SesionModalidadTripleAuditivaAuditivaVisual)
                        boton = self.btnAuditivoA_doble;
                    break;
                case BotonesLayerTipoBotonAuditivoB:
                    boton = self.btnAuditivoB;
                    if(self.sesion.modalidad==SesionModalidadDualAuditiva || self.sesion.modalidad==SesionModalidadTripleAuditivaAuditivaVisual)
                        boton = self.btnAuditivoB_doble;
                    break;
                default:
                    break;
            }
        }
    }

    if(!boton)
        return;
    
    if(boton.position.x>1024/2) {
        self.flecha.scaleX = -1;
        self.flecha.position = CGPointMake(boton.position.x-boton.contentSize.width/2, boton.position.y+boton.contentSize.height/2);
        
    } else {
        self.flecha.scaleX = 1;
        self.flecha.position = CGPointMake(boton.position.x+boton.contentSize.width/2, boton.position.y+boton.contentSize.height/2);
        
    }
    
    
    NSLog(@"acciona");
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.3];
    CCBlink *parpadea = [CCBlink actionWithDuration:1.4 blinks:3];
    CCFadeOut *fadeOut = [CCFadeOut actionWithDuration:0.3];
    CCSequence* secuencia = [CCSequence actions:fadeIn, parpadea, fadeOut, nil];
    [self.flecha runAction:secuencia];
}

@end
