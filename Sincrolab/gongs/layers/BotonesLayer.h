//
//  Botones.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionGongs.h"

typedef NS_ENUM(NSInteger, BotonesLayerTipoBoton) {
    BotonesLayerTipoCualquiera,
    BotonesLayerTipoBotonVisualA,
    BotonesLayerTipoBotonVisualB,
    BotonesLayerTipoBotonAuditivoA,
    BotonesLayerTipoBotonAuditivoB,
};

@protocol BotonesLayerDelegate <NSObject>
- (void) botonDado:(BotonesLayerTipoBoton) tipo;
@end

@interface BotonesLayer : CCLayer
@property (nonatomic, weak) SesionGongs* sesion;
@property (nonatomic, weak) id<BotonesLayerDelegate> delegate;
- (void) muestra;
- (void) oculta;
- (void) muestraHint:(NSDictionary *) dianas;
@end
