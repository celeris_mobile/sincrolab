//
//  Distractores.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "Distractor.h"

@interface Distractores : CCLayer <DistractorDelegate>
- (void) lanza;
@end
