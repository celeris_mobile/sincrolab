//
//  HelloWorldLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright Ricardo Sánchez Sotres 2013. All rights reserved.
//


#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "EstadoEntrenamiento.h"
#import "PanelParametrosLayer.h"
#import "BotonesLayer.h"
#import "ResultadoPartidaGongsLayer.h"
#import "PanelLayerDelegate.h"
#import "Paciente.h"
#import "TutorialGongs.h"

@interface JuegoGongsLayer : CCLayer <BotonesLayerDelegate, PanelLayerDelegate, TutorialGongsDelegate>
- (id) initWithPaciente:(Paciente *) paciente estado:(EstadoEntrenamiento *)estadoEntrenamiento;
- (void) finalizaSesion;
- (void) finalizaPartida;
@end
