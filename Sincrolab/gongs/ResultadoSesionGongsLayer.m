//
//  ResultadosGongsLayer.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ResultadoSesionGongsLayer.h"

@interface ResultadoSesionGongsLayer()
@property (nonatomic, strong) NSArray* mensajes;
@end

@implementation ResultadoSesionGongsLayer

- (id) initWithResultado:(ResultadoSesionGongs *) resultado {
    self = [super initWithNumLineas:3 boton:@"Continuar" andTitulo:@"Fin de sesión"];
    if(self) {
        NSArray* mensajes60 = @[@"No desesperes.", @"¡Concéntrate!", @"¡Sigue intentándolo!", @"¡Fíjate bien!", @"¡Ánimo!", @"¡No te rindas!", @"¡Sigue esforzándote!", @"Inténtalo otra vez", @"Te vas acercando ¡Sigue así!", @"¡Concéntrate un poco más!", @"¡Recuerda...la concentración es importante!", @"Poco a poco"];
        NSArray* mensajes85 = @[@"¡Genial! Sigue así.", @"¡Vas por buen camino!", @"¡No está mal!", @"¡Ánimo!", @"¡Estás a punto de conseguirlo!"];
        NSArray* mensajes100 = @[@"¡Que bestia!", @"Buen Trabajo", @"¡Muy bien!", @"¡Que crack!", @"¡Bravo!", @"¡Buenísimo!", @"¡Genial!", @"¡Alucinante!", @"¡Enhorabuena!", @"¡Que máquina!"];
        if(resultado.porcentajeAciertos.intValue<60)
            self.mensajes = mensajes60;
        else if(resultado.porcentajeAciertos.intValue<85)
            self.mensajes = mensajes85;
        else
            self.mensajes = mensajes100;
        
        NSString* aciertos = [NSString stringWithFormat:@"%d", resultado.numeroDeAciertos.intValue + resultado.numeroDeNoComisiones.intValue];
        NSString* fallos = [NSString stringWithFormat:@"%d", resultado.numeroDeComisiones.intValue + resultado.numeroDeOmisiones.intValue];
        NSString* puntuacion = [NSString stringWithFormat:@"%d", resultado.puntuacion.intValue];
        
        
        [self configuraLineas:@[@"Aciertos", @"Fallos", @"Puntuación"] conValores:@[aciertos, fallos, puntuacion]];
    }
    
    return self;
}

- (void)muestra {
    [super muestra];
    
    
    CCSprite* personaje = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"mensaje.png"].CGImage key:@"personajeMensaje"];
    personaje.position = CGPointMake(104, 125);
    [self addChild:personaje];
    
    CCSprite* bocadillo = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"bocadillo.png"].CGImage key:@"bocadilloPersonaje"];
    bocadillo.position = CGPointMake(155, 457);
    [self addChild:bocadillo];
    

    NSString* mensaje = [self.mensajes objectAtIndex:arc4random()%self.mensajes.count];
    CCLabelTTF *texto = [CCLabelTTF labelWithString:mensaje
                       fontName:@"ChauPhilomeneOne-Italic" fontSize:20 dimensions:CGSizeMake(179, 95) hAlignment:kCCTextAlignmentCenter lineBreakMode:kCCLineBreakModeWordWrap];
    texto.verticalAlignment=kCCVerticalTextAlignmentCenter;
    texto.color = ccc3(255,255,255);
    texto.position = ccp(155, 483);
    [self addChild:texto z:1+10];
    
    [texto setString:mensaje];
    
    bocadillo.visible = NO;
    texto.visible = NO;
    
    personaje.position = CGPointMake(104, -300);
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.6];
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(103, 125)];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        bocadillo.visible = YES;
        texto.visible = YES;
    }];
    
    CCSequence* secuencia = [CCSequence actions:delay, ease, callBlock, nil];
    [personaje runAction:secuencia];
    
    
}


@end
