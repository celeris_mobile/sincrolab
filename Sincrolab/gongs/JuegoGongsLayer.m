//
//  JuegoGongsLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright Ricardo Sánchez Sotres 2013. All rights reserved.
//

#import "JuegoGongsLayer.h"
#import "Paciente.h"

#import "DateUtils.h"

#import "SesionGongs.h"

#import "FondoGongs.h"
#import "GongsLayer.h"
#import "BotonesLayer.h"
#import "TiempoLayer.h"
#import "Distractores.h"

#import "RespuestaGongs.h"
#import "ResultadoSesionGongs.h"
#import "PartidaGongs.h"


#import "ConfiguracionSesionGongsLayer.h"
#import "ResultadoSesionGongsLayer.h"
#import "ResultadoPartidaGongsLayer.h"


@interface JuegoGongsLayer()
@property (nonatomic, strong) Paciente* paciente;
@property (nonatomic, strong) EstadoEntrenamiento* estadoEntrenamiento;
@property (nonatomic, strong) EstadoJuegoGongs* estado;

@property (nonatomic, strong) FondoGongs* fondo;
@property (nonatomic, strong) GongsLayer* gongs;
@property (nonatomic, strong) BotonesLayer* botones;
@property (nonatomic, strong) Distractores* distractores;
@property (nonatomic, strong) TiempoLayer* tiempo;

@property (nonatomic, strong) CCSprite* errorSprite;
@property (nonatomic, strong) CCSprite* aciertoSprite;

@property (nonatomic, strong) SesionGongs* sesion;

@property (nonatomic, strong) NSMutableSet* botonesDados;
@property (nonatomic, strong) NSMutableDictionary* tiemposDeRespuestaBotonesDados;

@property (nonatomic, strong) NSMutableArray* respuestasPorTrial;
@property (nonatomic, strong) NSMutableArray* resultadoSesiones;

@property (nonatomic, strong) ConfiguracionSesionGongsLayer* panelSesion;
@property (nonatomic, strong) ResultadoSesionGongsLayer* panelResultadoSesion;
@property (nonatomic, strong) ResultadoPartidaGongsLayer* panelResultadoPartida;

@property (nonatomic, strong) PartidaGongs* partida;
@property (nonatomic, strong) TutorialGongs* tutorial;

@end

@implementation JuegoGongsLayer {
    float secs_transcurridos;
    float t_estimulo;
    int num_trial;
    NSInteger nback_anterior;
    NSInteger modalidad_anterior;
}

- (id) initWithPaciente:(Paciente *) paciente estado:(EstadoEntrenamiento *)estadoEntrenamiento {
    
    self = [super init];

	if(self) {
        self.estadoEntrenamiento = estadoEntrenamiento;
        self.estado = self.estadoEntrenamiento.estadoGongs;
        self.paciente = paciente;
        
        self.fondo = [[FondoGongs alloc] init];
        self.fondo.anchorPoint = CGPointMake(0, 0);
        [self addChild:self.fondo];
        
        self.gongs = [[GongsLayer alloc] init];
        [self addChild:self.gongs];
        
        self.distractores = [[Distractores alloc] init];
        [self addChild:self.distractores];
        
        self.botones = [[BotonesLayer alloc] init];
        self.botones.delegate = self;
        [self addChild:self.botones];
        
        self.tiempo = [[TiempoLayer alloc] init];
        [self addChild:self.tiempo];
        
        self.errorSprite = [CCSprite spriteWithFile:@"error.png"];
        self.errorSprite.position = CGPointMake(1024-80, 768-80);
        self.errorSprite.opacity = 0.0;
        [self addChild:self.errorSprite];
        
        self.aciertoSprite = [CCSprite spriteWithFile:@"check.png"];
        self.aciertoSprite.position = CGPointMake(1024-80, 768-80);
        self.aciertoSprite.opacity = 0.0;
        [self addChild:self.aciertoSprite];
        
        self.resultadoSesiones = [[NSMutableArray alloc] init];
        
	}
    
	return self;
}

- (void)onEnterTransitionDidFinish {
    self.sesion = nil;
    [self performSelector:@selector(configuraSesion) withObject:nil afterDelay:0.5];
}

- (void) configuraSesion {
    num_trial = 0;
    self.respuestasPorTrial = [[NSMutableArray alloc] init];
    
    int numSesion = 1;
    if(self.sesion)
        numSesion = self.sesion.numSesion+1;

    self.sesion = [SesionGongs sesionNum:numSesion conEstado:self.estado];
        
    self.panelSesion = [[ConfiguracionSesionGongsLayer alloc] initWithSesion:self.sesion de:self.estado.sesiones.intValue];
    self.panelSesion.delegate = self;
    [self addChild:self.panelSesion];
        
    if(self.sesion.numSesion==1 || self.sesion.nback!=nback_anterior || self.sesion.modalidad!= modalidad_anterior) {
        [self muestraTutorial];
    } else
        [self muestraSesion];
}

- (void) muestraTutorial {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber* instruccionesVistas = (NSNumber *)[userDefaults objectForKey:[NSString stringWithFormat:@"instrucciones_gongs_%d_%d", self.sesion.nback, self.sesion.modalidad]];
    
    if(instruccionesVistas.boolValue) {
        [self muestraSesion];
    } else {
        
        self.tutorial = [[TutorialGongs alloc] initWithGongs:self.gongs andBotones:self.botones];
        self.tutorial.delegate = self;
        [self addChild:self.tutorial];
        self.tutorial.sesion = self.sesion;
        [self.tutorial muestra];
        
        self.fondo.sesion = self.sesion;
    }
}

- (void) tutorialFinalizado {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"instrucciones_gongs_%d_%d", self.sesion.nback, self.sesion.modalidad]];
    [userDefaults synchronize];
    
    self.tutorial.delegate = nil;
    [self.tutorial removeFromParent];
    self.tutorial = nil;
    [self muestraSesion];
}

- (void) muestraSesion {
    nback_anterior = self.sesion.nback;
    modalidad_anterior = self.sesion.modalidad;
    
    secs_transcurridos = 0.0;
    num_trial = 0;
    self.respuestasPorTrial = [[NSMutableArray alloc] init];
    
    self.fondo.sesion = self.sesion;
    [self.tiempo setTiempoTotal:self.sesion.tiempoTotal + 0.5];
    self.gongs.sesion = self.sesion;
    self.botones.sesion = self.sesion;

    [self.tiempo muestra];
    [self.gongs muestra];
    [self.botones muestra];
    
    [self.panelSesion muestra];
}

- (void) iniciaSesion {
    [self scheduleUpdate];
    [self scheduleOnce:@selector(lanzaPrimerEstimulo) delay:0.5];
}

- (void) lanzaPrimerEstimulo {
    [self lanzaEstimulo];    
    [self schedule:@selector(lanzaEstimulo) interval:self.sesion.tie];
}

- (void) lanzaEstimulo {
    if(num_trial>=1) {
        [self compruebaAciertos];
    }
    
    self.botonesDados = [[NSMutableSet alloc] init];
    self.tiemposDeRespuestaBotonesDados = [[NSMutableDictionary alloc] init];
    
    
    float aleatorio = (float)rand() / RAND_MAX;
    if(aleatorio<self.sesion.distractores/100.0)
        [self.distractores lanza];
    
    
    //Cuándo se lanzó este estímulo
    t_estimulo = secs_transcurridos;
    
    if(num_trial>=self.sesion.trials)
        [self performSelector:@selector(finSesion) withObject:nil afterDelay:0.5];
    else
        [self.gongs lanzaEstimulos];
    
    num_trial += 1;
}

- (void)update:(ccTime)delta {
    secs_transcurridos += delta;
    [self.tiempo decrementa:delta];
}

- (void) finSesion {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    
    ResultadoSesionGongs* resultado = [ResultadoSesionGongs resultadoConRespuestas:self.respuestasPorTrial];
    resultado.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
    resultado.modalidad = [NSNumber numberWithInt:self.sesion.modalidad];
    resultado.nback = [NSNumber numberWithInt:self.sesion.nback];
    [self.resultadoSesiones addObject:resultado];
    
    self.panelResultadoSesion = [[ResultadoSesionGongsLayer alloc] initWithResultado:resultado];
    self.panelResultadoSesion.delegate = self;
    [self addChild:self.panelResultadoSesion];
    [self.panelResultadoSesion muestra];
    
    [self.tiempo oculta];
    [self.gongs oculta];
    [self.botones oculta];
}


- (void) finPartida {
    self.partida = [PartidaGongs object];
    self.partida.sesiones = [[NSMutableArray alloc] init];
    
    for (ResultadoSesionGongs* resultado in self.resultadoSesiones) {
        [self.partida.sesiones addObject:resultado.valores];
    }
    
    if(self.estadoEntrenamiento.tipo.intValue==TipoEntrenamientoAutomatico) {
        EstadoJuegoCambio cambioNivel = [self.estado compruebaCambioNivelConResultados:self.resultadoSesiones];
        
        switch (cambioNivel) {
            case EstadoJuegoCambioSubeNivel:
                self.partida.cambioNivel = [NSNumber numberWithInt:1];
                break;
            case EstadoJuegoCambioBajaNivel:
                self.partida.cambioNivel = [NSNumber numberWithInt:-1];
                break;
            case EstadoJuegoCambioSeMantiene:
                self.partida.cambioNivel = [NSNumber numberWithInt:0];
                break;
            case EstadoJuegoCambioFinal:
                //TODO Haz algo con esto
                [[[UIAlertView alloc] initWithTitle:@"¡Atención!" message:@"Entrenamiento finalizado" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                break;
        }
    } else {
        //TODO cuando es un entrenamiento manual subimos el nivel?
        self.partida.cambioNivel = [NSNumber numberWithInt:0];
    }
    
    self.panelResultadoPartida = [[ResultadoPartidaGongsLayer alloc] initWithResultadoSesiones:self.resultadoSesiones paciente:self.paciente andCambioNivel:self.partida.cambioNivel];
    
    self.panelResultadoPartida.delegate = self;
    [self addChild:self.panelResultadoPartida];
    [self.panelResultadoPartida muestra];
}

- (void) compruebaAciertos {
    NSMutableArray* auxRespuestas = [[NSMutableArray alloc] init];
    for (NSNumber* estimulo in self.gongs.dianasTrialActual) {
        if(![self.botonesDados containsObject:estimulo]) {
            RespuestaGongs* respuesta = [[RespuestaGongs alloc] init];
            
            BOOL diana = ((NSNumber *)[self.gongs.dianasTrialActual objectForKey:estimulo]).boolValue;
            if(diana) {
                respuesta.tipo = TipoRespuestaGongsOmision;
            } else {
                respuesta.tipo = TipoRespuestaGongsNoComision;
            }
            
            [auxRespuestas addObject:respuesta];
        } else {
            RespuestaGongs* respuesta = [[RespuestaGongs alloc] init];
            
            BOOL diana = ((NSNumber *)[self.gongs.dianasTrialActual objectForKey:estimulo]).boolValue;
            if(diana) {
                respuesta.tipo = TipoRespuestaGongsAcierto;
            } else {
                respuesta.tipo = TipoRespuestaGongsComision;
            }
            
            respuesta.tiempo = ((NSNumber *)[self.tiemposDeRespuestaBotonesDados objectForKey:estimulo]).floatValue;
            [auxRespuestas addObject:respuesta];
        }
    }
    
    [self.respuestasPorTrial addObject:auxRespuestas.copy];
}

- (void) muestraError {
    self.errorSprite.position = CGPointMake(1024-80, 768-120);
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 768-80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.5];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 768-40)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction]];
    
    [self.errorSprite runAction:secuencia];
}

- (void) muestraAcierto {
    self.aciertoSprite.position = CGPointMake(1024-80, 768-120);
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 768-80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.5];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 768-40)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction]];
    
    [self.aciertoSprite runAction:secuencia];
}

#pragma mark BotonesLayer Delegate
- (void)botonDado:(BotonesLayerTipoBoton)tipo {
    NSLog(@"dado");
    //Por cada trial solo se tiene en cuenta la primera vez que se le da a cada botón
    if(self.tutorial||![self.botonesDados containsObject:[NSNumber numberWithInt:tipo]]) {
        [self.botonesDados addObject:[NSNumber numberWithInt:tipo]];
        
        //secs_transcurridos-t_estimulo porque queremos el tiempo desde el estímulo, no desde el inicio
        [self.tiemposDeRespuestaBotonesDados setObject:[NSNumber numberWithFloat:secs_transcurridos-t_estimulo] forKey:[NSNumber numberWithInt:tipo]];
        
        //Aquí comprobamos si es fallo o acierto, solo para mostrar el icono al momento
        BOOL fallo = YES;
        for (NSNumber* estimulo in self.gongs.dianasTrialActual) {
            BOOL diana = ((NSNumber *)[self.gongs.dianasTrialActual objectForKey:estimulo]).boolValue;
            if(diana) {
                if(tipo==estimulo.intValue) {
                    fallo = NO;
                }
            }
        }
        
        if(fallo) {
            [self muestraError];
            if(self.tutorial)
                [self.tutorial error];
        } else {
            [self muestraAcierto];
            if(self.tutorial)
                [self.tutorial acierto];            
        }
    }
}


#pragma mark PanelDelegate
- (void)panelFinalizado:(id) panelLayer {
    if(panelLayer==self.panelSesion) {
        [self.panelSesion removeFromParent];
        self.panelSesion = nil;
        [self iniciaSesion];
    }
    if(panelLayer==self.panelResultadoSesion) {
        [self.panelResultadoSesion removeFromParent];
        self.panelResultadoSesion = nil;

        if(self.sesion.numSesion<self.estado.sesiones.intValue)
            [self configuraSesion];
        else
            [self finPartida];
    }
    if(panelLayer==self.panelResultadoPartida) {
        NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.partida, @"partida", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificacionPartidaTerminada" object:self userInfo:userInfo];
    }
}


#pragma mark Para Debug
- (void) finalizaSesion {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    
    ResultadoSesionGongs* resultado = [ResultadoSesionGongs resultadoAleatorio];
    resultado.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
    
    [self.resultadoSesiones addObject:resultado];
    
    self.panelResultadoSesion = [[ResultadoSesionGongsLayer alloc] initWithResultado:resultado];
    self.panelResultadoSesion.delegate = self;
    [self addChild:self.panelResultadoSesion];
    [self.panelResultadoSesion muestra];
    
    [self.tiempo oculta];
    [self.gongs oculta];
    [self.botones oculta];
}

- (void) finalizaPartida {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    
    
    for(int s = self.resultadoSesiones.count; s<self.estado.sesiones.intValue; s++) {
        ResultadoSesionGongs* resultado = [ResultadoSesionGongs resultadoAleatorio];
        resultado.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
        
        [self.resultadoSesiones addObject:resultado];
    }
    
    [self finPartida];
    
    [self.tiempo oculta];
    [self.gongs oculta];
    [self.botones oculta];
}
@end

