//
//  FondoGongs.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionGongs.h"

@interface FondoGongs : CCNode
@property (nonatomic, weak) SesionGongs* sesion;
@end
