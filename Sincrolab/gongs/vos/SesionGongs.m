//
//  Sesion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 22/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SesionGongs.h"
#import "Constantes.h"

@implementation SesionGongs

+ (float) floatConPropiedad:(NSString *) propiedad {
    propiedad = [propiedad stringByReplacingOccurrencesOfString:@"," withString:@"."];
    NSArray* components = [propiedad componentsSeparatedByString:@":"];
    if(components.count==1)
        return ((NSString *)[components objectAtIndex:0]).floatValue;
    else {
        float v1 = ((NSString *)[components objectAtIndex:0]).floatValue;
        float v2 = ((NSString *)[components objectAtIndex:1]).floatValue;
        float aleatorio = (float)rand() / RAND_MAX;
        return (v2-v1)*aleatorio + v1;
    }
}

+ (int) intConPropiedad:(NSString *) propiedad {
    NSArray* components = [propiedad componentsSeparatedByString:@":"];
    if(components.count==1)
        return ((NSString *)[components objectAtIndex:0]).intValue;
    else {
        float v1 = ((NSString *)[components objectAtIndex:0]).intValue;
        float v2 = ((NSString *)[components objectAtIndex:1]).intValue;
        float aleatorio = (float)rand() / RAND_MAX;
        return (int)roundf((v2-v1)*aleatorio + v1);
    }
}

+ (SesionGongs *) sesionNum:(int) numSesion conEstado:(EstadoJuegoGongs *) estado {
    SesionGongs *sesion = [[SesionGongs alloc] init];
    sesion.numSesion = numSesion;
    sesion.distractores = (int)aleatorio(estado.dmin.floatValue, estado.dmax.floatValue);
    sesion.elementos = [self intConPropiedad:estado.elementos];
    sesion.nback = [self intConPropiedad:estado.nback];
    sesion.trials = [self intConPropiedad:estado.trials];
    sesion.tie = [self floatConPropiedad:estado.tie];
    sesion.texp = [self floatConPropiedad:estado.texp];
    switch (estado.modalidad.intValue) {
        case EstadoGongsModalidadVisualA:
            sesion.modalidad = SesionModalidadVisualA;
            break;
        case EstadoGongsModalidadVisualB:
            sesion.modalidad = SesionModalidadVisualB;
            break;
        case EstadoGongsModalidadAuditivaA:
            sesion.modalidad = SesionModalidadAuditivaA;
            break;
        case EstadoGongsModalidadAuditivaB:
            sesion.modalidad = SesionModalidadAuditivaB;
            break;
        case EstadoGongsModalidadMedioVisualMedioAuditiva:
            if(sesion.numSesion<=estado.sesiones.intValue/2)
                sesion.modalidad = SesionModalidadVisualA;
            else
                sesion.modalidad = SesionModalidadAuditivaA;
            break;
        case EstadoGongsModalidadDualVisualyAuditiva:
            sesion.modalidad = SesionModalidadDual;
            break;
        case EstadoGongsModalidadDualVisual:
            sesion.modalidad = SesionModalidadDualVisual;
            break;
        case EstadoGongsModalidadDualAuditiva:
            sesion.modalidad = SesionModalidadDualAuditiva;
            break;
        case EstadoGongsModalidadTripleVisualVisualAuditiva:
            sesion.modalidad = SesionModalidadTripleVisualVisualAuditiva;
            break;
        case EstadoGongsModalidadTripleAuditivaAuditivaVisual:
            sesion.modalidad = SesionModalidadTripleAuditivaAuditivaVisual;
            break;
    }
    
    if(sesion.nback>=2) {
        float aleatorio = (float)rand() / RAND_MAX;
        if(aleatorio<0.5)
            sesion.escenario = SesionEscenarioGongsTipoGongs;
        else
            sesion.escenario = SesionEscenarioGongsTipoFarolillos;
    } else
        sesion.escenario = SesionEscenarioGongsTipoGongs;
    
    sesion.porcentaje_dianas = [self floatConPropiedad:estado.porcentaje_dianas];
    
    return sesion;
}

- (NSInteger)tiempoTotal {
    return self.tie*self.trials;
}
@end
