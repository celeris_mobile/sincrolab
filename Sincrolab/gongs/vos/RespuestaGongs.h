//
//  RespuestaGongs.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 26/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TipoRespuestaGongs) {
    TipoRespuestaGongsAcierto,
    TipoRespuestaGongsOmision,
    TipoRespuestaGongsComision,
    TipoRespuestaGongsNoComision,
};

@interface RespuestaGongs : NSObject
@property (nonatomic, assign) float tiempo;
@property (nonatomic, assign) TipoRespuestaGongs tipo;
@end
