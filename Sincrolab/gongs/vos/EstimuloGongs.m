//
//  EstimuloGongs.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 22/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstimuloGongs.h"

@implementation EstimuloGongs

- (id)init {
    self = [super init];
    if(self) {
        self.numGong = -1;
        self.numColor = -1;
        self.numNumero = -1;
        self.numLetra = -1;
    }
    return self;
}
@end
