//
//  EstimuloGongs.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 22/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EstimuloGongs : NSObject
@property (nonatomic, assign) NSInteger numGong;
@property (nonatomic, assign) NSInteger numColor;
@property (nonatomic, assign) NSInteger numNumero;
@property (nonatomic, assign) NSInteger numLetra;
@end
