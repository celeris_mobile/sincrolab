//
//  Sesion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 22/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EstadoJuegoGongs.h"

typedef NS_ENUM(NSInteger, SesionModalida) {
    SesionModalidadVisualA,
    SesionModalidadVisualB,
    SesionModalidadAuditivaA,
    SesionModalidadAuditivaB,
    SesionModalidadDual,
    SesionModalidadDualVisual,
    SesionModalidadDualAuditiva,
    SesionModalidadTripleVisualVisualAuditiva,
    SesionModalidadTripleAuditivaAuditivaVisual
};

typedef NS_ENUM(NSInteger, SesionEscenarioGongsTipo) {
    SesionEscenarioGongsTipoGongs,
    SesionEscenarioGongsTipoFarolillos,
};

@interface SesionGongs : NSObject
+ (SesionGongs *) sesionNum:(int) numSesion conEstado:(EstadoJuegoGongs *) estado;
@property (nonatomic, assign) NSInteger numSesion;
@property (nonatomic, assign) NSInteger distractores;
@property (nonatomic, assign) NSInteger elementos;
@property (nonatomic, assign) NSInteger nback;
@property (nonatomic, assign) NSInteger trials;
@property (nonatomic, assign) double tie;
@property (nonatomic, assign) double texp;
@property (nonatomic, assign) SesionModalida modalidad;
@property (nonatomic, assign) double porcentaje_dianas;
@property (nonatomic, assign) SesionEscenarioGongsTipo escenario;

@property (nonatomic, readonly) NSInteger tiempoTotal;

@end
