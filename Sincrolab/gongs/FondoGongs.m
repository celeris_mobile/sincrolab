//
//  FondoGongs.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "FondoGongs.h"

@interface FondoGongs()
@property (nonatomic, strong) CCSprite* fondo;
@end

@implementation FondoGongs

- (id)init {
    self = [super init];
    if(self) {
        self.fondo = [CCSprite spriteWithFile:@"fondo_gongs.jpg"];
        self.fondo.anchorPoint = CGPointZero;
        [self addChild:self.fondo];
    }
    
    return self;
}

- (void)setSesion:(SesionGongs *)sesion {
    if(!_sesion||_sesion.escenario!=sesion.escenario)
        [self cambiaFondo:sesion.escenario];
    
    _sesion = sesion;
}

- (void) cambiaFondo:(SesionEscenarioGongsTipo)escenario {
    CCSprite* aBorrar;
    if(self.fondo)
        aBorrar = self.fondo;
    
    CCSprite* fondo;
    switch (escenario) {
        case SesionEscenarioGongsTipoGongs:
            fondo = [CCSprite spriteWithFile:@"fondo_gongs.jpg"];
            break;
        case SesionEscenarioGongsTipoFarolillos:
            fondo = [CCSprite spriteWithFile:@"fondo_farolillos.jpg"];
            break;
        default:
            fondo = [CCSprite spriteWithFile:@"fondo_gongs.jpg"];
            break;
    }

    fondo.anchorPoint = CGPointZero;
    self.fondo = fondo;
    self.fondo.opacity = 0.0;
    [self addChild:self.fondo];
    
    CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:0.5];
    CCCallBlock* block = [CCCallBlock actionWithBlock:^{
        if(aBorrar)
          [aBorrar removeFromParent];
    }];
    CCSequence* secuencia = [CCSequence actions:fadeIn, block, nil];
    [self.fondo runAction:secuencia];
    
}

@end
