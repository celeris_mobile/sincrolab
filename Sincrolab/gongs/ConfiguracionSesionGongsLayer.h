//
//  ParametrosGongsLayer.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelParametrosLayer.h"
#import "SesionGongs.h"

@interface ConfiguracionSesionGongsLayer : PanelParametrosLayer
- (id) initWithSesion:(SesionGongs *)sesion de:(int) totalSesiones;
@end
