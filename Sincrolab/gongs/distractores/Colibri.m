//
//  Colibri.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Colibri.h"
#import "Constantes.h"

@interface Colibri()
@property (nonatomic, strong) CCSprite *sprite;
@property (nonatomic, strong) CCAction *animacion;
@property (nonatomic, strong) CCSpriteBatchNode *spriteSheet;
@end

NSArray* animFrames;

@implementation Colibri

- (id) init {
    self = [super init];
    if(self) {
        CCSpriteFrameCache* spriteFrameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [spriteFrameCache addSpriteFramesWithFile:@"colibri.plist"];
        
        self.spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"colibri.png"];
        [self addChild:self.spriteSheet];
        
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        for (int i=1; i<=34; i++) {
            [walkAnimFrames addObject:
             [spriteFrameCache spriteFrameByName:[NSString stringWithFormat:@"Colibri%04d.png",i]]];
        }
        
        CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:1/30.0];
        
        self.sprite = [CCSprite spriteWithSpriteFrameName:@"Colibri0001.png"];
        self.animacion = [CCRepeatForever actionWithAction:
                                    [CCAnimate actionWithAnimation:walkAnim]];

    }
    return self;
}

- (void) lanza {
    if(self.sprite.parent)
        return;
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGPoint position = CGPointMake(((float)rand() / RAND_MAX)*(winSize.width-200)+100, ((float)rand() / RAND_MAX)*(winSize.height-200)+100);

    self.sprite.position = CGPointMake(position.x, 800);
    CCMoveTo* moveIn = [CCMoveTo actionWithDuration:0.5 position:position];
    CCDelayTime* wait = [CCDelayTime actionWithDuration:2.0];
    CCMoveTo* moveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(position.x+500, 800)];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        [self.sprite removeFromParent];
        [self.sprite.actionManager removeAllActionsFromTarget:self.sprite];
        [self.delegate distractorHaTerminado:self];
    }];
    
    CCSequence* secuencia = [CCSequence actions:moveIn, wait, moveOut, callBlock, nil];
    
    [self.spriteSheet addChild:self.sprite];    
    [self.sprite runAction:secuencia];
    [self.sprite runAction:self.animacion];
}


@end
