//
//  Distractor.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CCNode.h"

@class Distractor;

@protocol DistractorDelegate <NSObject>
- (void) distractorHaTerminado:(Distractor *) distractor;
@end

@interface Distractor : CCNode
@property (nonatomic, weak) id<DistractorDelegate> delegate;
- (void) lanza;
@end
