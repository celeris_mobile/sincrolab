//
//  PajaronMarron.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PajaronMarron.h"

@interface PajaronMarron()
@property (nonatomic, strong) CCSprite *sprite;
@property (nonatomic, strong) CCAction *animacion;
@property (nonatomic, strong) CCSpriteBatchNode *spriteSheet;
@end

NSArray* animFrames;

@implementation PajaronMarron

- (id) init {
    self = [super init];
    if(self) {
        CCSpriteFrameCache* spriteFrameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [spriteFrameCache addSpriteFramesWithFile:@"marron.plist"];
        
        self.spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"marron.png"];
        [self addChild:self.spriteSheet];
        
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        for (int i=1; i<=11; i++) {
            [walkAnimFrames addObject:
             [spriteFrameCache spriteFrameByName:[NSString stringWithFormat:@"PajaroMarron%04d.png",i]]];
        }
        
        CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:1/30.0];
        
        self.sprite = [CCSprite spriteWithSpriteFrameName:@"PajaroMarron0001.png"];
        self.animacion = [CCRepeatForever actionWithAction:
                          [CCAnimate actionWithAnimation:walkAnim]];
        
    }
    return self;
}

- (void) lanza {
    if(self.sprite.parent)
        return;
    
    float aleatorio = (float)rand() / RAND_MAX;
    int direccion;
    if(aleatorio>0.5)
        direccion = 1.0;
    else
        direccion = -1.0;
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGPoint position = CGPointMake(direccion==1.0?1024+20:-20, ((float)rand() / RAND_MAX)*(winSize.height-200)+100);
    
    self.sprite.scaleX = direccion;
    self.sprite.position = CGPointMake(direccion==-1.0?1024+20:-20, position.y);
    aleatorio = (float)rand() / RAND_MAX;
    float t = 2.5*aleatorio+2.5;
    CCMoveTo* move = [CCMoveTo actionWithDuration:t position:position];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        [self.sprite removeFromParent];
        [self.sprite.actionManager removeAllActionsFromTarget:self.sprite];
        [self.delegate distractorHaTerminado:self];   
    }];
    
    CCSequence* secuencia = [CCSequence actions:move, callBlock, nil];
    
    [self.spriteSheet addChild:self.sprite];
    [self.sprite runAction:secuencia];
    [self.sprite runAction:self.animacion];
}


@end
