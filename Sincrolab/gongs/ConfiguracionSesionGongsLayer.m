//
//  ParametrosGongsLayer.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguracionSesionGongsLayer.h"
#import "DateUtils.h"
#import "ImageUtils.h"

@implementation ConfiguracionSesionGongsLayer


- (id) initWithSesion:(SesionGongs *)sesion de:(int) totalSesiones {
    self = [super initWithNumLineas:3 boton:@"Empezar" andTitulo:[NSString stringWithFormat:@"Sesión %d de %d", sesion.numSesion, totalSesiones]];
    if(self) {
        NSString* tiempo = [DateUtils segundosAReloj:sesion.tiempoTotal + 0.5];
        NSLog(@"texto de texp:%f",sesion.texp);
        NSLog(@"texto de tie:%f",sesion.tie);
        NSLog(@"texto de sesion actual:%ld",(long)sesion.numSesion);
        NSLog(@"texto de elementos:%ld", (long)sesion.elementos);
        NSLog(@"texto de n back:%ld", (long)sesion.nback);
        NSLog(@"texto de sesiones totales:%ld",(long)totalSesiones);         
        
        
        
        
        [self configuraLineas:@[@"Memoria", @"Tiempo", @"Modalidad"] conValores:@[[self imagenParaNBack:sesion.nback], tiempo, [self imagenParaModalidad:sesion.modalidad]]];
        
        
    }
    
    return self;
}


- (UIImage *) imagenParaNBack:(int) nback {
    UIView* imgAux = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 26*nback+18, 18)];
    imgAux.backgroundColor = [UIColor clearColor];
    imgAux.opaque = NO;
    for (int n = 0; n<nback+1; n++) {
        UIView* circulo = [[UIView alloc] initWithFrame:CGRectMake(26*n, 0, 18, 18)];
        if(n==0 || n==nback)
            circulo.backgroundColor = [UIColor whiteColor];
        else {
            circulo.backgroundColor = [UIColor clearColor];
            circulo.layer.borderWidth = 1.0;
            circulo.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        circulo.layer.cornerRadius = 9.0;
        [imgAux addSubview:circulo];
    }
    
    return [ImageUtils imageWithView:imgAux];
}

- (UIImage *) imagenParaModalidad:(int) modalidad {

    NSArray* iconos;

    switch (modalidad) {
        case SesionModalidadVisualA:
        case SesionModalidadVisualB:
            iconos = @[@"v"];
            break;
        case SesionModalidadAuditivaA:
        case SesionModalidadAuditivaB:
            iconos = @[@"a"];
            break;
        case SesionModalidadDual:
            iconos = @[@"v",@"a"];
            break;
        case SesionModalidadDualAuditiva:
            iconos = @[@"a",@"a"];
            break;
        case SesionModalidadDualVisual:
            iconos = @[@"v",@"v"];
            break;
        case SesionModalidadTripleVisualVisualAuditiva:
            iconos = @[@"v",@"v", @"a"];
            break;
        case SesionModalidadTripleAuditivaAuditivaVisual:
            iconos = @[@"a",@"a", @"v"];
            break;
    }

    UIImage* imgVisual = [UIImage imageNamed:@"modalidadVisual.png"];
    UIImage* imgAudio = [UIImage imageNamed:@"modalidadAuditiva.png"];
    UIView* imgAux = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imgVisual.size.width*iconos.count+5*(iconos.count-1), imgVisual.size.height)];
    imgAux.backgroundColor = [UIColor clearColor];
    imgAux.opaque = NO;
    for (int n = 0; n<iconos.count; n++) {
        UIImageView* icono = [[UIImageView alloc] initWithFrame:CGRectMake((imgVisual.size.width+5)*n, 0, imgVisual.size.width, imgVisual.size.height)];
        NSString* tipo = [iconos objectAtIndex:n];
        if([tipo isEqualToString:@"v"])
            icono.image = imgVisual;
        else
            icono.image = imgAudio;

        [imgAux addSubview:icono];
    }
    
    return [ImageUtils imageWithView:imgAux];
}

@end
