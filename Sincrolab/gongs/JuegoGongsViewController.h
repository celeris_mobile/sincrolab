//
//  MainViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JuegoViewController.h"
#import "EstadoJuegoGongs.h"

@interface JuegoGongsViewController : JuegoViewController
@end
