//
//  CocosViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 29/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CocosViewController.h"

@interface CocosViewController ()

@end

@implementation CocosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setupCocos];
}

- (void) setupCocos {
    self.director = [CCDirector sharedDirector];
    [self.director setDelegate:self];
    [self.director setDisplayStats:NO];
	[self.director setAnimationInterval:1.0/60];
    
    if(![self.director isViewLoaded]) {
        /*
        CCGLView *glView = [CCGLView viewWithFrame:self.view.bounds
                                       pixelFormat:kEAGLColorFormatRGB565
                                       depthFormat:0
                                preserveBackbuffer:NO
                                        sharegroup:nil
                                     multiSampling:NO
                                   numberOfSamples:0];
         */

        CCGLView *glView = [CCGLView viewWithFrame:self.view.bounds
                                       pixelFormat:kEAGLColorFormatRGBA8
                                       depthFormat:0
                                preserveBackbuffer:NO
                                        sharegroup:nil
                                     multiSampling:0
                                   numberOfSamples:0];

        
        glView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        [self.director setView:glView];
        [self.director setProjection:kCCDirectorProjection3D];
        [self.director setDepthTest:YES];
        if(![self.director enableRetinaDisplay:YES])
            CCLOG(@"Retina Display Not supported");

        
    }

	
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-hd"];	// Default on iPad RetinaDisplay is "-ipadhd"
	
	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
	
    [self addChildViewController:self.director];
    [self.view insertSubview:self.director.view atIndex:0];
    
}

-(void) directorDidReshapeProjection:(CCDirector*)director {
	if([CCDirector sharedDirector].runningScene == nil) {
		[[CCDirector sharedDirector] runWithScene:self.mainScene];
	}
}

- (void)viewDidDisappear:(BOOL)animated {
    [[CCDirector sharedDirector] setDelegate:nil];
    [[CCDirector sharedDirector] end];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
