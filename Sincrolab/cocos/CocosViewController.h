//
//  CocosViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 29/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface CocosViewController : UIViewController <CCDirectorDelegate>
@property (nonatomic, strong) CCDirector* director;
@property (nonatomic, readonly) CCScene *mainScene;
@end
