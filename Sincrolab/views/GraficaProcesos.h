//
//  GraficaProcesos.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaBarras.h"

@protocol GraficaProcesosDelegate <NSObject>
- (void) cargaSemanaAnterior;
- (void) cargaSemanaSiguiente;
@end

@protocol GraficaProcesosDatasource <NSObject>
- (NSArray *) pacientesDeLaSemana;
@end

@interface GraficaProcesos : UIView <GraficaBarrasDelegate, GraficaBarrasDatasource>
@property (nonatomic, weak) id<GraficaProcesosDelegate> delegate;
@property (nonatomic, weak) id<GraficaProcesosDatasource> datasource;
@property (nonatomic, strong) NSDate* semana;
@property (nonatomic, strong) NSString* titulo;
@end
