//
//  GraficaProcesos.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaProcesos.h"
#import "UIColor+Extensions.h"

@interface GraficaProcesos()
@property (nonatomic, strong) NSArray* puntos;
@property (nonatomic, strong) UIButton* btnAnterior;
@property (nonatomic, strong) UIButton* btnSiguiente;
@property (nonatomic, strong) UILabel* rango;
@property (nonatomic, strong) UILabel* tituloLabel;

@property (nonatomic, strong) GraficaBarras* grafica;
@property (nonatomic, strong) NSArray* procesos;

@end

@implementation GraficaProcesos

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor whiteColor];
    
    self.grafica = [[GraficaBarras alloc] initWithFrame:CGRectMake(132, 31, 362, 179)];
    self.grafica.dataSource = self;
    self.grafica.delegate = self;
    self.grafica.graphInset = CGSizeMake(10, 10);
    self.grafica.labelFont = [UIFont fontWithName:@"Lato-Regular" size:11.0];
    [self.grafica setLimites:CGRectMake(0.0, 0.0, 8.0, 10.0)];
    [self addSubview:self.grafica];
    
    UIFont *font = [UIFont fontWithName:@"Lato-Bold" size:10.0];
    NSDictionary *atributos = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, [UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    UIFont *fontBold = [UIFont fontWithName:@"Lato-Bold" size:10.0];
    NSDictionary *atributosBold = [NSDictionary dictionaryWithObjectsAndKeys:fontBold, NSFontAttributeName, [UIColor azulOscuro], NSForegroundColorAttributeName, nil];
    
    
    self.btnAnterior = [[UIButton alloc] initWithFrame:CGRectMake(293, 11, 92, 21)];
    NSAttributedString* attrString = [[NSAttributedString alloc] initWithString:@"Semana anterior" attributes:atributos];
    NSAttributedString* attrStringBold = [[NSAttributedString alloc] initWithString:@"Semana anterior" attributes:atributosBold];
    [self.btnAnterior setAttributedTitle:attrString forState:UIControlStateNormal];
    [self.btnAnterior setAttributedTitle:attrStringBold forState:UIControlStateHighlighted];
    self.btnAnterior.layer.borderWidth = 1.0;
    self.btnAnterior.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.btnAnterior addTarget:self action:@selector(semanaAnteriorDado) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnAnterior];
    
    self.btnSiguiente = [[UIButton alloc] initWithFrame:CGRectMake(390, 11, 95, 21)];
    attrString = [[NSAttributedString alloc] initWithString:@"Siguiente semana" attributes:atributos];
    attrStringBold = [[NSAttributedString alloc] initWithString:@"Siguiente semana" attributes:atributosBold];
    [self.btnSiguiente setAttributedTitle:attrString forState:UIControlStateNormal];
    [self.btnSiguiente setAttributedTitle:attrStringBold forState:UIControlStateHighlighted];
    self.btnSiguiente.layer.borderWidth = 1.0;
    self.btnSiguiente.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.btnSiguiente addTarget:self action:@selector(semanaSiguienteDado) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnSiguiente];
    
    self.rango = [[UILabel alloc] initWithFrame:CGRectMake(137, 15, 135, 12)];
    self.rango.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    self.rango.textAlignment = NSTextAlignmentRight;
    self.rango.textColor = [UIColor verdeAzulado];
    [self addSubview:self.rango];
    
    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(17, 12, 120, 15)];
    self.tituloLabel.backgroundColor = [UIColor clearColor];    
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Regular" size:12.5];
    self.tituloLabel.textColor = [UIColor azulOscuro];
    [self addSubview:self.tituloLabel];
}

- (void) semanaAnteriorDado {
    [self.delegate cargaSemanaAnterior];
}

- (void) semanaSiguienteDado {
    [self.delegate cargaSemanaSiguiente];
}

- (void)setSemana:(NSDate *)semana {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSDate* siguiente = [semana dateByAddingTimeInterval:6*24*60*60];
    self.rango.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:semana], [formatter stringFromDate:siguiente]];
    
    [self recarga];
}

- (void) recarga {

    NSArray* pacientes = [self.datasource pacientesDeLaSemana];
    self.procesos = nil;
    
    if(pacientes.count) {
        
        NSString* proceso;
        NSMutableArray* auxprocesos = [[NSMutableArray alloc] init];
        for (int i = 0; i<8; i++) {
            switch (i) {
                case 0:
                    proceso = @"atencionSostenida";
                    break;
                case 1:
                    proceso = @"controlInhibitorio";
                    break;
                case 2:
                    proceso = @"memoriaOperativa";
                    break;
                case 3:
                    proceso = @"atencionSelectiva";
                    break;
                case 4:
                    proceso = @"flexibilidadCognitiva";
                    break;
                case 5:
                    proceso = @"velocidadDeProcesamiento";
                    break;
                case 6:
                    proceso = @"tomaDeDecisiones";
                    break;
                case 7:
                    proceso = @"atencionDividida";
                    break;
            }
            
            [auxprocesos addObject:[pacientes valueForKeyPath:[NSString stringWithFormat:@"@sum.perfilcognitivo.%@", proceso]]];
        }
        
        self.procesos = auxprocesos;
        NSNumber* max = [self.procesos valueForKeyPath:@"@max.self"];
        
        [self.grafica setLimites:CGRectMake(0.0, 0.0, 8.0, max.floatValue?:1.0)];
    }
    
    [self.grafica reloadData];
}


- (void)setTitulo:(NSString *)titulo {
    _titulo = titulo;
    
    self.tituloLabel.text = _titulo;
}

#pragma mark GraficaBarrasDatasource
- (int)numeroDeValoresEnGraficaDeBarras:(GraficaBarras *)grafica {
    return 8;
}

- (NSNumber *)graficaDeBarras:(GraficaBarras *)grafica valorParaIndice:(NSInteger)indice {
    if(!self.procesos)
        return [NSNumber numberWithInt:0];
    
    NSNumber* valor =  [self.procesos objectAtIndex:indice];
    if(!valor)
        return [NSNumber numberWithInt:0.0];

    return valor;
}

- (NSString *)graficaDeBarras:(GraficaBarras *)grafica etiquetaParaIndice:(NSInteger)indice {
    NSArray* procesos = [NSArray arrayWithObjects:@"Atención sostenida", @"Control inhibitorio", @"Memoria operativa", @"Atención selectiva", @"Flexibilidad cognitiva", @"Vel. procesamiento", @"Toma de decisiones", @"Atención dividida", nil];
    
    return [procesos objectAtIndex:indice];
}

#pragma mark GraficaBarras Delegate

- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context {
    NSArray* procesos = [NSArray arrayWithObjects:[NSNumber numberWithInt:AtencionSostenida], [NSNumber numberWithInt:ControlInhibitorio], [NSNumber numberWithInt:MemoriaOperativa], [NSNumber numberWithInt:AtencionSelectiva], [NSNumber numberWithInt:FlexibilidadCognitiva], [NSNumber numberWithInt:VelocidadDeProcesamiento], [NSNumber numberWithInt:TomaDeDecisiones], [NSNumber numberWithInt:AtencionDividida], nil];
    
    NSNumber* numProceso = [procesos objectAtIndex:numSet];
    UIColor * color = [UIColor colorProceso:numProceso.intValue];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
}

@end
