//
//  NumPacientesView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "NumPacientesView.h"
#import "UIColor+Extensions.h"

@interface NumPacientesView()
@property (nonatomic, strong) UILabel* labelActivos;
@property (nonatomic, strong) UILabel* labelTotales;
@end
@implementation NumPacientesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor whiteColor];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(4, 13, 131, 11)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Lato-Regular" size:12.5];
    label.textColor = [UIColor azulOscuro];
    label.text = @"Mis Pacientes";
    [self addSubview:label];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(5, 44, 131, 11)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Lato-Regular" size:11];
    label.textColor = [UIColor lightGrayColor];
    label.text = @"Activos";
    [self addSubview:label];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(5, 106, 131, 11)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Lato-Regular" size:11];
    label.textColor = [UIColor lightGrayColor];
    label.text = @"Totales";
    [self addSubview:label];
    
    self.labelActivos = [[UILabel alloc] initWithFrame:CGRectMake(7, 61, 131, 28)];
    self.labelActivos.backgroundColor = [UIColor clearColor];
    self.labelActivos.textAlignment = NSTextAlignmentCenter;
    self.labelActivos.font = [UIFont fontWithName:@"Lato-Bold" size:33];
    self.labelActivos.textColor = [UIColor verdeAzulado];
    self.labelActivos.text = @"0";
    [self addSubview:self.labelActivos];
    
    self.labelTotales = [[UILabel alloc] initWithFrame:CGRectMake(7, 123, 131, 28)];
    self.labelTotales.backgroundColor = [UIColor clearColor];
    self.labelTotales.textAlignment = NSTextAlignmentCenter;
    self.labelTotales.font = [UIFont fontWithName:@"Helvetica-Bold" size:33];
    self.labelTotales.textColor = [UIColor azulOscuro];
    self.labelTotales.text = @"0";
    [self addSubview:self.labelTotales];
    
}

- (void)setPacientesActivos:(NSInteger)pacientesActivos {
    self.labelActivos.text = [NSString stringWithFormat:@"%d", pacientesActivos];
}

- (void)setPacientesTotales:(NSInteger)pacientesTotales {
    self.labelTotales.text = [NSString stringWithFormat:@"%d", pacientesTotales];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(ctx, 1.0);
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    
    CGContextStrokeRect(ctx, CGRectMake(18, 36, 107, 123));
    
    CGContextMoveToPoint(ctx, 18, 98);
    CGContextAddLineToPoint(ctx, 125, 98);
    CGContextStrokePath(ctx);
}


@end
