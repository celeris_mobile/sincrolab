//
//  CeldaPacienteActivo.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CeldaPacienteActivo.h"
#import "Entrenamiento.h"
#import "DiaEntrenamiento.h"
#import "EntrenamientoPacienteModel.h"
#import "UIColor+Extensions.h"
#import "Constantes.h"
#import "ProgressHUD.h"
#import "DateUtils.h"

@interface CeldaPacienteActivo()
@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UILabel *fecha;
@property (weak, nonatomic) IBOutlet UILabel *nivel;
@property (weak, nonatomic) IBOutlet UILabel *duracion;

@property (weak, nonatomic) IBOutlet UILabel *labelGongs;
@property (weak, nonatomic) IBOutlet UILabel *labelInvasion;
@property (weak, nonatomic) IBOutlet UILabel *labelPoker;

@property (strong, nonatomic) EntrenamientoPacienteModel* model;

@property (strong, nonatomic) GraficaTarta *grafica;
@property (strong, nonatomic) NSArray* duraciones;
@end

@implementation CeldaPacienteActivo


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.grafica = [[GraficaTarta alloc] initWithFrame:CGRectMake(55, 47, 96, 96)];
        self.grafica.dataSource = self;
        self.grafica.delegate = self;
        [self addSubview:self.grafica];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.nombre.font = [UIFont fontWithName:@"Lato-Bold" size:12.0];
    self.fecha.font = [UIFont fontWithName:@"Lato-Bold" size:12.0];
    
    self.labelGongs.font = [UIFont fontWithName:@"Lato-Bold" size:12.0];
    self.labelGongs.textColor = [UIColor colorGrafica:0 conAlpha:1.0];
    self.labelGongs.text = [Constantes nombreDeJuego:TipoJuegoGongs];
    
    self.labelInvasion.font = [UIFont fontWithName:@"Lato-Bold" size:12.0];
    self.labelInvasion.textColor = [UIColor colorGrafica:1 conAlpha:1.0];
    self.labelInvasion.text = [Constantes nombreDeJuego:TipoJuegoInvasion];
    
    self.labelPoker.font = [UIFont fontWithName:@"Lato-Bold" size:12.0];
    self.labelPoker.textColor = [UIColor colorGrafica:2 conAlpha:1.0];
    self.labelPoker.text = [Constantes nombreDeJuego:TipoJuegoPoker];
}

- (void)setPaciente:(Paciente *)paciente {
    _paciente = paciente;
    
    self.nivel.text = @"";
    self.duracion.text = @"";
    self.grafica.hidden = YES;
    

    
    NSDictionary* atributosLabel = @{
                                     NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                     NSFontAttributeName:[UIFont fontWithName:@"Lato-Bold" size:11.0]
                                     };
    NSDictionary* atributosValor = @{
                                     NSForegroundColorAttributeName:[UIColor azulOscuro],
                                     NSFontAttributeName:[UIFont fontWithName:@"Lato-Bold" size:11.0]
                                     };

    if(self.model)
        [self.model cancela];

    self.model = [[EntrenamientoPacienteModel alloc] initWithPaciente:self.paciente];
    [self.model carga:^(NSError * error) {
        if(!error) {
            self.grafica.hidden = NO;
            
            DiaEntrenamiento* dia = [self.paciente.entrenamiento.diasDeEntrenamiento lastObject];

            if(!dia)
                return;
            
            //nivel
            NSMutableAttributedString* nivelStr = [[NSMutableAttributedString alloc] initWithString:@"Nivel: " attributes:atributosLabel];
            NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
            [formatter setPositivePrefix:@"+"];
            NSAttributedString* valorStr = [[NSAttributedString alloc] initWithString:[formatter stringFromNumber:dia.cambioNivel] attributes:atributosValor];
            [nivelStr appendAttributedString:valorStr];
            self.nivel.attributedText = nivelStr;
            
            //duracion
            NSMutableAttributedString* duracionStr = [[NSMutableAttributedString alloc] initWithString:@"Duración: " attributes:atributosLabel];
            

            NSString * strTiempo = [DateUtils segundosAString:dia.duracion.intValue];
            
            valorStr = [[NSAttributedString alloc] initWithString:strTiempo attributes:atributosValor];
            [duracionStr appendAttributedString:valorStr];
            self.duracion.attributedText = duracionStr;
            
            
            float suma = 0;
            suma += dia.partidaGongs.duracion.floatValue?:0.0;
            suma += dia.partidaInvasion.duracion.floatValue?:0.0;
            suma += dia.partidaPoker.duracion.floatValue?:0.0;
            self.duraciones = @[[NSNumber numberWithFloat:100.0*dia.partidaGongs.duracion.floatValue/suma],
                                [NSNumber numberWithFloat:100.0*dia.partidaInvasion.duracion.floatValue/suma],
                                [NSNumber numberWithFloat:100.0*dia.partidaPoker.duracion.floatValue/suma]];
            [self.grafica reloadData];
        }
    } conProgreso:NO];

    
    self.nombre.text = [NSString stringWithFormat:@"%@ %@", paciente.nombre, paciente.apellidos];
    
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
     self.fecha.text = [dateFormatter stringFromDate:paciente.fechaUltimoEvento];

}

- (int)numeroDeValoresEnGraficaDeTarta:(GraficaTarta *)grafica {
    return 3;
}


- (NSNumber *)graficaDeTarta:(GraficaTarta *)grafica valorParaIndice:(NSInteger)indice {
    if(!self.duraciones)
        return [NSNumber numberWithInt:0];
    
    return [self.duraciones objectAtIndex:indice];
}

- (void)configuraValor:(NSInteger)numSet enContexto:(CGContextRef)context {
    CGContextSetFillColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:1.0].CGColor);
}

@end
