//
//  GraficaSexo.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaSexo.h"
#import "UIColor+Extensions.h"

@interface GraficaSexo()
@property (nonatomic, strong) NSArray* puntos;
@property (nonatomic, strong) UILabel* tituloLabel;
@property (nonatomic, strong) UIView* etiquetas;

@property (nonatomic, strong) NSArray* valores;
@property (nonatomic, assign) NSInteger total;
@end

@implementation GraficaSexo

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor whiteColor];
    
    self.grafica = [[GraficaTarta alloc] initWithFrame:CGRectMake(115, 14, 113, 113)];
    self.grafica.labelFont = [UIFont fontWithName:@"Lato-Regular" size:9.0];
    self.grafica.delegate = self;
    self.grafica.dataSource = self;
    [self addSubview:self.grafica];
    
    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 12, 100, 15)];
    self.tituloLabel.backgroundColor = [UIColor clearColor];
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Regular" size:12];
    self.tituloLabel.textColor = [UIColor azulOscuro];
    [self addSubview:self.tituloLabel];
}

- (void)setTitulo:(NSString *)titulo {
    _titulo = titulo;
    
    self.tituloLabel.text = _titulo;
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* vista in self.etiquetas.subviews) {
            [vista removeFromSuperview];
        }
        
        [self.etiquetas removeFromSuperview];
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:self.etiquetas];
    
    for (int e = 0; e<[self numeroDeValoresEnGraficaDeTarta:self.grafica]; e++) {
        NSString* etiqueta = [self graficaDeTarta:self.grafica etiquetaParaIndice:e];
        UIColor* color = [UIColor colorGrafica:e conAlpha:1.0];
        
        UIView* cuadrito = [[UIView alloc] initWithFrame:CGRectMake(18, 53+e*19, 6, 6)];
        cuadrito.backgroundColor = color;
        [self.etiquetas addSubview:cuadrito];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(30, 51+e*19, 67, 9)];
        label.font = [UIFont fontWithName:@"Lato-Regular" size:9.0];
        label.text = etiqueta;
        [self.etiquetas addSubview:label];
    }
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(ctx, 1.0);
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    CGContextMoveToPoint(ctx, 100.0, 0.0);
    CGContextAddLineToPoint(ctx, 100.0, rect.size.height);
    
    CGContextStrokePath(ctx);
}

- (void)refresca {
    self.valores = [self.datasource valoresGraficaSexo];
    self.total = 0;
    for (NSDictionary* valor in self.valores) {
        self.total += ((NSNumber *)[valor objectForKey:@"cantidad"]).intValue;
    }
    
    [self.grafica reloadData];
    [self dibujaEtiquetas];
}

#pragma mark GraficaBarras Delegate

- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context {
    UIColor * color = [UIColor colorGrafica:numSet conAlpha:1.0];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
}

#pragma mark GraficaTarta DataSource
- (int)numeroDeValoresEnGraficaDeTarta:(GraficaTarta *)grafica {
    return (int)self.valores.count;
}

- (NSNumber *)graficaDeTarta:(GraficaTarta *)grafica valorParaIndice:(NSInteger)indice {
    NSDictionary* valor = [self.valores objectAtIndex:indice];
    NSInteger cantidad = ((NSNumber *)[valor objectForKey:@"cantidad"]).intValue;
    NSNumber* percent = [NSNumber numberWithFloat:100*((float)cantidad)/self.total];
    return percent;
    
}

- (NSString *)graficaDeTarta:(GraficaTarta *)grafica etiquetaParaIndice:(NSInteger)indice {
    NSDictionary* valor = [self.valores objectAtIndex:indice];
    return [valor objectForKey:@"grupo"];
}

@end
