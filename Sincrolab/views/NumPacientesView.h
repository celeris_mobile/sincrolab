//
//  NumPacientesView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NumPacientesView : UIView
@property (nonatomic, assign) NSInteger pacientesActivos;
@property (nonatomic, assign) NSInteger pacientesTotales;
@end
