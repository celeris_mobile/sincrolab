//
//  GraficaEdad.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaTarta.h"

@class GraficaEdad;

@protocol GraficaEdadDatasource <NSObject>
- (NSArray *) valoresGraficaEdad;
@end

@interface GraficaEdad : UIView <GraficaTartaDelegate, GraficaTartaDatasource>
@property (nonatomic, strong) id<GraficaEdadDatasource> datasource;
@property (nonatomic, strong) GraficaTarta* grafica;
@property (nonatomic, strong) NSString* titulo;
- (void) refresca;
@end
