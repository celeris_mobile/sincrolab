//
//  GraficaActividadSemanal.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaActividadSemanal.h"
#import "UIColor+Extensions.h"
#import "UIColor+Extensions.h"

@interface GraficaActividadSemanal()
@property (nonatomic, strong) GraficaLineas* grafica;
@property (nonatomic, strong) UIButton* btnAnterior;
@property (nonatomic, strong) UIButton* btnSiguiente;
@property (nonatomic, strong) UILabel* rango;
@property (nonatomic, strong) UILabel* tituloLabel;
@property (nonatomic, strong) NSArray* entrenamientos;
@end

@implementation GraficaActividadSemanal

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor whiteColor];
    
    self.grafica = [[GraficaLineas alloc] initWithFrame:CGRectMake(20, 62, 317, 107)];
    self.grafica.ejes = GFBordesWidthMake(0, 0.5, 0, 0);
    self.grafica.delegate = self;
    self.grafica.dataSource = self;
    self.grafica.minY = 0.0;
    [self addSubview:self.grafica];
    
    UIFont *font = [UIFont fontWithName:@"Lato-Bold" size:10.0];
    NSDictionary *atributos = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, [UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    UIFont *fontBold = [UIFont fontWithName:@"Lato-Bold" size:10.0];
    NSDictionary *atributosBold = [NSDictionary dictionaryWithObjectsAndKeys:fontBold, NSFontAttributeName, [UIColor azulOscuro], NSForegroundColorAttributeName, nil];

    
    self.btnAnterior = [[UIButton alloc] initWithFrame:CGRectMake(20, 33, 104, 21)];
    NSAttributedString* attrString = [[NSAttributedString alloc] initWithString:@"Semana anterior" attributes:atributos];
    NSAttributedString* attrStringBold = [[NSAttributedString alloc] initWithString:@"Semana anterior" attributes:atributosBold];
    [self.btnAnterior setAttributedTitle:attrString forState:UIControlStateNormal];
    [self.btnAnterior setAttributedTitle:attrStringBold forState:UIControlStateHighlighted];
    self.btnAnterior.layer.borderWidth = 1.0;
    self.btnAnterior.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.btnAnterior addTarget:self action:@selector(semanaAnteriorDado) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnAnterior];

    self.btnSiguiente = [[UIButton alloc] initWithFrame:CGRectMake(231, 33, 104, 21)];
    attrString = [[NSAttributedString alloc] initWithString:@"Siguiente semana" attributes:atributos];
    attrStringBold = [[NSAttributedString alloc] initWithString:@"Siguiente semana" attributes:atributosBold];
    [self.btnSiguiente setAttributedTitle:attrString forState:UIControlStateNormal];
    [self.btnSiguiente setAttributedTitle:attrStringBold forState:UIControlStateHighlighted];
    self.btnSiguiente.layer.borderWidth = 1.0;
    self.btnSiguiente.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.btnSiguiente addTarget:self action:@selector(semanaSiguienteDado) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnSiguiente];
    
    self.rango = [[UILabel alloc] initWithFrame:CGRectMake(171, 13, 165, 12)];
    self.rango.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    self.rango.textAlignment = NSTextAlignmentRight;
    self.rango.textColor = [UIColor verdeAzulado];
    [self addSubview:self.rango];
    
    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 12, 120, 15)];
    self.tituloLabel.backgroundColor = [UIColor clearColor];
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Regular" size:12.5];
    self.tituloLabel.textColor = [UIColor azulOscuro];
    [self addSubview:self.tituloLabel];
}

- (void) semanaAnteriorDado {
    [self.delegate cargaSemanaAnterior];
}

- (void) semanaSiguienteDado {
    [self.delegate cargaSemanaSiguiente];
}

- (void)setSemana:(NSDate *)semana {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSDate* siguiente = [semana dateByAddingTimeInterval:6*24*60*60];
    self.rango.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:semana], [formatter stringFromDate:siguiente]];
    
    NSDate* hoy = [NSDate date];
    if([semana compare:hoy]!=NSOrderedDescending && [hoy compare:siguiente]!=NSOrderedDescending)
        self.btnSiguiente.enabled = NO;
    else
        self.btnSiguiente.enabled = YES;
    
    [self recarga];
}

- (void)setTitulo:(NSString *)titulo {
    _titulo = titulo;
    
    self.tituloLabel.text = _titulo;
}

- (void) recarga {
    self.entrenamientos = [self.datasource entrenamientosDeLaSemana];
    [self.grafica dibuja];
}

#pragma mark GraficaLineas Delegate

- (void) grafica:(GraficaLineas *)grafica dibujaPunto:(CGPoint)punto deSet:(NSInteger) numSet conValor:(NSNumber *) valor enContexto:(CGContextRef)context {
    CGContextSaveGState(context);
    
    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(punto.x-1, punto.y-1, 2, 2));
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor darkGrayColor].CGColor);
    CGContextSetLineWidth(context, 0.5);
    CGContextAddEllipseInRect(context, CGRectMake(punto.x-6, punto.y-6, 12, 12));
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGContextSetFillColorWithColor(context, [UIColor orangeColor].CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(punto.x-2.5, punto.y-2.5, 5, 5));
    
    int mult = 1;
    if(punto.y>=self.grafica.frame.size.height-40)
        mult = -1;
    
    CGContextSetFillColorWithColor(context, [UIColor azulOscuro].CGColor);    
    CGContextMoveToPoint(context, punto.x-11, punto.y+(mult*30));
    CGContextAddLineToPoint(context, punto.x+11, punto.y+(mult*30));
    CGContextAddLineToPoint(context, punto.x+11, punto.y+(mult*12));
    CGContextAddLineToPoint(context, punto.x+3, punto.y+(mult*12));
    CGContextAddLineToPoint(context, punto.x, punto.y+(mult*9));
    CGContextAddLineToPoint(context, punto.x-3, punto.y+(mult*12));
    CGContextAddLineToPoint(context, punto.x-11, punto.y+(mult*12));
    CGContextFillPath(context);
    
    CGContextTranslateCTM(context, 0.0, 0.0);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    NSString* str = [NSString stringWithFormat:@"%d", valor.intValue];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    NSDictionary* atributos = [NSDictionary dictionaryWithObjectsAndKeys:
                               [UIFont fontWithName:@"Lato-Bold" size:11.0], NSFontAttributeName,
                               [UIColor whiteColor], NSForegroundColorAttributeName,
                               style, NSParagraphStyleAttributeName,
                               nil];
    CGRect textRect;
    if(punto.y<self.grafica.frame.size.height-40)
       textRect = CGRectMake(punto.x-11, -punto.y-28, 22, 22);
    else
        textRect = CGRectMake(punto.x-11, -punto.y+15, 22, 22);
    
    [str drawInRect:textRect withAttributes:atributos];
    
    CGContextRestoreGState(context);
}

- (void) grafica:(GraficaLineas *) grafica configuraSet:(NSInteger) numSet enContexto:(CGContextRef) context {
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:44/255.0 green:199/255.0 blue:147/255.0 alpha:1.0].CGColor);
    CGContextSetLineWidth(context, 3);

    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:44/255.0 green:199/255.0 blue:147/255.0 alpha:0.0].CGColor);
    
}

#pragma mark GraficaDeLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(!self.entrenamientos)
        return @[@"0", @"0", @"0", @"0", @"0", @"0", @"0"];
    
    NSArray* array = [self.entrenamientos valueForKeyPath:@"@unionOfObjects.@count"];
    return array;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return [NSArray arrayWithObjects:@"L", @"M", @"X", @"J", @"V", @"S", @"D", nil];
}


@end
