//
//  PanelGraficaSexo.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaTarta.h"

@class GraficaSexo;

@protocol GraficaSexoDatasource <NSObject>
- (NSArray *) valoresGraficaSexo;
@end

@interface GraficaSexo : UIView <GraficaTartaDelegate, GraficaTartaDatasource>
@property (nonatomic, strong) id<GraficaSexoDatasource> datasource;
@property (nonatomic, strong) GraficaTarta* grafica;
@property (nonatomic, strong) NSString* titulo;
- (void) refresca;
@end
