//
//  GraficaActividadSemanal.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaLineas.h"

@protocol GraficaActividadSemanaDelegate <NSObject>
- (void) cargaSemanaAnterior;
- (void) cargaSemanaSiguiente;
@end

@protocol GraficaActividadSemanaDatasource <NSObject>
- (NSArray *) entrenamientosDeLaSemana;
- (NSInteger) maximoNumeroPorDia;
@end

@interface GraficaActividadSemanal : UIView <GraficaLineasDelegate, GraficaLineasDatasource>
@property (nonatomic, weak) id<GraficaActividadSemanaDelegate> delegate;
@property (nonatomic, weak) id<GraficaActividadSemanaDatasource> datasource;
@property (nonatomic, strong) NSDate* semana;
@property (nonatomic, strong) NSString* titulo;

- (void) recarga;
@end
