//
//  CeldaPacienteActivo.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paciente.h"
#import "GraficaTarta.h"

@interface CeldaPacienteActivo : UITableViewCell <GraficaTartaDatasource, GraficaTartaDelegate>
@property (nonatomic, strong) Paciente* paciente;
@end
