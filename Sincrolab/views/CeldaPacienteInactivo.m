//
//  CeldaPacienteInactivo.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CeldaPacienteInactivo.h"

@interface CeldaPacienteInactivo()
@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UILabel *numDias;
@property (weak, nonatomic) IBOutlet UILabel *labelDias;
@end

@implementation CeldaPacienteInactivo

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.nombre.font = [UIFont fontWithName:@"Lato-Bold" size:12.0];
        self.numDias.font = [UIFont fontWithName:@"Lato-Bold" size:36.0];
        self.labelDias.font = [UIFont fontWithName:@"Lato-Regular" size:13.0];
    }
    return self;
}

- (void)setPaciente:(Paciente *)paciente {
    _paciente = paciente;
    
    self.nombre.text = [NSString stringWithFormat:@"%@ %@", paciente.nombre, paciente.apellidos];

    self.numDias.text = [NSString stringWithFormat:@"%d", paciente.diasSinJugar];
    if(paciente.diasSinJugar==1)
        self.labelDias.text = @"Día";
    else
        self.labelDias.text = @"Días";
    
}
@end
