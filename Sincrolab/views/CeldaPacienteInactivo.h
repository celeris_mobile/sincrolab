//
//  CeldaPacienteInactivo.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paciente.h"

@interface CeldaPacienteInactivo : UITableViewCell
@property (nonatomic, strong) Paciente* paciente;
@end
