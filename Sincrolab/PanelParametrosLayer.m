//
//  ParametrosLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelParametrosLayer.h"
#import "UIColor+Extensions.h"
#import "ImageUtils.h"

@interface PanelParametrosLayer()
@property (nonatomic, strong) CCSprite* fondo;
@property (nonatomic, strong) CCLabelTTF* titulo;
@end

@implementation PanelParametrosLayer {
    CGPoint origen;
}

- (id)initWithNumLineas:(int) numLineas boton:(NSString *) boton andTitulo:(NSString*) titulo {

    self = [super init];
    if(self) {
        NSString* imageKey = [NSString stringWithFormat:@"fondoParams:%d", numLineas];
        
        CCTexture2D* texturaFondo = [[CCTextureCache sharedTextureCache] textureForKey:imageKey];
        if(texturaFondo) {
            self.fondo = [CCSprite spriteWithTexture:texturaFondo];
        } else {
            self.fondo = [CCSprite spriteWithCGImage:[self generaTexturaFondo:numLineas].CGImage key:imageKey];
        }
        
        self.fondo.position = CGPointMake(1024/2, 768/2+50);
        [self addChild:self.fondo];
        
        origen = CGPointMake(self.fondo.position.x-self.fondo.contentSize.width/2, self.fondo.position.y+self.fondo.contentSize.height/2);
        
        self.titulo = [CCLabelTTF labelWithString:titulo fontName:@"ChauPhilomeneOne-Regular" fontSize:44];
        self.titulo.anchorPoint = CGPointMake(0, 0.5);
        self.titulo.position = CGPointMake(origen.x+23, origen.y-41);
        self.titulo.color = ccc3(255.0, 255.0, 255.0);
        [self addChild:self.titulo];
        
        self.btn = [[BotonParametros alloc] initWithTitle:boton];
        self.btn.position = CGPointMake(self.fondo.position.x, origen.y-self.fondo.contentSize.height-62);
        [self addChild:self.btn];
        
    }
    
    return self;
}

- (void) configuraLineas:(NSArray *) lineas conValores:(NSArray *) valores {
    int l = 0;
    for (NSString* etiqueta in lineas) {
        CCLabelTTF* label = [CCLabelTTF labelWithString:etiqueta fontName:@"ChauPhilomeneOne-Regular" fontSize:33];
        label.anchorPoint = CGPointZero;
        label.color = ccc3(122.0, 189.0, 196.0);
        label.position = CGPointMake(origen.x+37, origen.y-(143+68*l));
        [self addChild:label];
        
        id valor = [valores objectAtIndex:l];
        if([valor isKindOfClass:[NSString class]]) {
            CCLabelTTF* valor = [CCLabelTTF labelWithString:[valores objectAtIndex:l] fontName:@"ChauPhilomeneOne-Regular" fontSize:33];
            valor.anchorPoint = CGPointMake(1.0, 0);
            valor.color = ccc3(255.0, 255.0, 255.0);
            valor.position = CGPointMake(origen.x+375, origen.y-(143+68*l));
            [self addChild:valor];
        }
        if([valor isKindOfClass:[UIImage class]]) {
            UIImage* img = (UIImage *)valor;
            CCSprite* imgSprite = [CCSprite spriteWithCGImage:img.CGImage key:nil];
            imgSprite.anchorPoint = CGPointMake(1.0, 0.5);
            imgSprite.position = CGPointMake(origen.x+375, origen.y-(84+34+68*l));
            
            [self addChild:imgSprite];
        }
        
        l+=1;
    }
}

- (UIImage *) generaTexturaFondo:(int) numLineas {
    float alto = numLineas*68 + 108;
    float ancho = 424;
    
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho+20, alto+20)];
    fondo.backgroundColor = [UIColor clearColor];
    fondo.opaque = NO;

    UIView* sombra = [[UIView alloc] initWithFrame:CGRectMake(20, 20, ancho, alto)];
    sombra.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
    sombra.layer.cornerRadius = 10.0;
    sombra.opaque = NO;
    [fondo addSubview:sombra];
    
    UIView* fondoBlanco = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho, alto)];
    fondoBlanco.backgroundColor = [UIColor juegoVerdeOscuro];
    fondoBlanco.layer.cornerRadius = 10.0;
    fondoBlanco.opaque = NO;
    [fondo addSubview:fondoBlanco];
    
    UIView* bordeInterior = [[UIView alloc] initWithFrame:CGRectMake(16, 84, 424-32, 68*numLineas)];
    bordeInterior.backgroundColor = [UIColor clearColor];
    bordeInterior.layer.borderWidth = 5.0;
    bordeInterior.layer.borderColor = [UIColor juegoVerdeClaro].CGColor;
    bordeInterior.layer.cornerRadius = 5.0;
    [fondo addSubview:bordeInterior];
    
    for (int l = 0; l<numLineas-1; l++) {
        UIView* linea = [[UIView alloc] initWithFrame:CGRectMake(32, 83+68+68*l, 360, 5)];
        linea.backgroundColor = [UIColor juegoVerdeClaro];
        [fondo addSubview:linea];
    }
    
    return [ImageUtils imageWithView:fondo];
}


@end
