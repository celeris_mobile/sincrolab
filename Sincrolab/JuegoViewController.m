//
//  JuegoViewController.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 13/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "JuegoViewController.h"

@interface JuegoViewController ()
@property (nonatomic, strong, readwrite) EstadoEntrenamiento* estadoEntrenamiento;
@property (nonatomic, strong, readwrite) Paciente* paciente;
@end

@implementation JuegoViewController

- (id) initWithPaciente:(Paciente *) paciente estadoEntrenamiento:(EstadoEntrenamiento *) estado {
    
    self = [super init];
    if (self) {
        self.paciente = paciente;
        self.estadoEntrenamiento = estado;
        
        CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
        [sharedFileUtils setiPadRetinaDisplaySuffix:@"@2x"];    // Default on iPad RetinaDisplay is "-ipadhd"
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIButton* boton = [[UIButton alloc] initWithFrame:CGRectMake(10, 768-40, 100, 20)];
    [boton setBackgroundColor:[UIColor blackColor]];
    [boton setTitle:@"Fin Sesión" forState:UIControlStateNormal];
    [boton addTarget:self action:@selector(finSesionDado) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:boton];
    
    boton = [[UIButton alloc] initWithFrame:CGRectMake(10, 768-70, 100, 20)];
    [boton setBackgroundColor:[UIColor blackColor]];
    [boton setTitle:@"Fin Partida" forState:UIControlStateNormal];
    [boton addTarget:self action:@selector(finPartidaDado) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:boton];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) finSesionDado {
}

- (void) finPartidaDado {
}

@end
