//
//  DateUtils.m
//  Funginote
//
//  Created by Ricardo on 02/10/13.
//  Copyright (c) 2013 Wake App. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+ (NSString *) fechaToIntervalo: (NSDate *) pastDate {

    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    NSDate *currentDate = [[NSDate alloc] init];
    
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
    
    NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:currentDate  toDate:pastDate  options:0];
    
    NSString *intervalString;
    if ([breakdownInfo month]) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
        [formatter setDateFormat:@"d' de 'MMMM"];
        intervalString = [formatter stringFromDate:pastDate];
    }
    else if ([breakdownInfo day]) {
        if (-[breakdownInfo day] > 1)
            intervalString = [NSString stringWithFormat:@"hace %d días", -[breakdownInfo day]];
        else
            intervalString = @"ayer";
    }
    else if ([breakdownInfo hour]) {
        if (-[breakdownInfo hour] > 1)
            intervalString = [NSString stringWithFormat:@"hace %d horas", -[breakdownInfo hour]];
        else
            intervalString = @"hace 1 hora";
    }
    else {
        if (-[breakdownInfo minute] > 1)
            intervalString = [NSString stringWithFormat:@"hace %d minutos", -[breakdownInfo minute]];
        else
            intervalString = @"hace 1 minuto";
    }
    
    return intervalString;
}

+ (NSString *) fechaToTexto: (NSDate *) pastDate {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    [formatter setDateFormat:@"d' de 'MMMM' de 'yyyy"];
    return [formatter stringFromDate:pastDate];
}

+ (NSInteger) diaDeLaSemanaParaFecha:(NSDate *) date {
    unsigned units = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:units fromDate:date];
    NSInteger dia = [components weekday];
    dia-=1;
    if(dia==0)
        dia = 7;
    return  dia;
}

+ (NSDate *) primerDiaDeLaSemanaParaFecha:(NSDate *) date {
    unsigned units = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:units fromDate:date];
    NSInteger dia = [components weekday];
    dia-=1;
    if(dia==0)
        dia = 7;
    components.day -= (dia-1);
    return  [gregorian dateFromComponents:components];
}

+ (NSString *) segundosAReloj:(NSInteger) segundos {
    int minutos = (int) floor(segundos/60.0);
    int secs = segundos-minutos*60;
    NSString* minutos_str = [NSString stringWithFormat:@"%d", minutos];
    NSString* segundos_str = [NSString stringWithFormat:@"%d", secs];
    minutos_str = minutos_str.length==2?minutos_str:[@"0" stringByAppendingString:minutos_str];
    segundos_str = segundos_str.length==2?segundos_str:[@"0" stringByAppendingString:segundos_str];

    return [NSString stringWithFormat:@"%@:%@", minutos_str, segundos_str];
}

+ (NSString *) segundosACronometro:(float) segundos {
    int secs = (int) floor(segundos);
    int milisecs = (segundos-floor(segundos))*10;
    NSString* segundos_str = [NSString stringWithFormat:@"%d", secs];
    NSString* milisegundos_str = [NSString stringWithFormat:@"%d", (int)milisecs];
    
    return [NSString stringWithFormat:@"%@''%@'''", segundos_str, milisegundos_str];
}

+ (NSComparisonResult) comparaSinHoraFecha:(NSDate *) fecha1 conFecha:(NSDate *) fecha2 {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents* components = [gregorian components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:fecha1];
    fecha1 = [gregorian dateFromComponents:components];

    components = [gregorian components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:fecha2];
    fecha2 = [gregorian dateFromComponents:components];

    
    return [fecha1 compare:fecha2];
}

+ (NSDate *)fechaSinHora:(NSDate *)fecha {
    if( fecha == nil ) {
        return nil;
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:fecha];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

+ (NSInteger) diasEntreFecha:(NSDate *)fecha1 yFecha:(NSDate *)fecha2 {
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:fecha1 toDate:fecha2 options:0];
    return [components day];
}

+ (NSString *) segundosAString:(NSInteger) segundos {
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents* comps = [calendar components:NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
                                          fromDate:[NSDate date]
                                            toDate:[NSDate dateWithTimeInterval:segundos sinceDate:[NSDate date]]
                                           options:0];

    NSMutableArray* partes = [[NSMutableArray alloc] init];
    [self addComponentIfPresent:[comps day] withSuffix:@"d" toParts:partes];
    [self addComponentIfPresent:[comps hour] withSuffix:@"h" toParts:partes];
    [self addComponentIfPresent:[comps minute] withSuffix:@"m" toParts:partes];
    [self addComponentIfPresent:[comps second] withSuffix:@"s" toParts:partes];

    if(partes.count==0)
        return @"0s";
    
    return [partes componentsJoinedByString:@" "];
}

+ (void) addComponentIfPresent:(NSUInteger) component withSuffix:(NSString *) suffix toParts:(NSMutableArray *) parts {
    if(component > 0) {
        [parts addObject:[NSString stringWithFormat:@"%d%@", component, suffix]];
    }
}
@end
