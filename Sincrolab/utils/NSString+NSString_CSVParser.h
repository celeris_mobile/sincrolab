//
//  NSString+NSString_CSVParser.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_CSVParser)

- (NSArray *)csvRows;

@end
