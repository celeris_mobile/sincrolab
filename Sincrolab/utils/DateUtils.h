//
//  DateUtils.h
//  Funginote
//
//  Created by Ricardo on 02/10/13.
//  Copyright (c) 2013 Wake App. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject
+ (NSString *) fechaToIntervalo: (NSDate *) pastDate;
+ (NSString *) fechaToTexto: (NSDate *) pastDate;
+ (NSInteger) diaDeLaSemanaParaFecha:(NSDate *) date;
+ (NSDate *) primerDiaDeLaSemanaParaFecha:(NSDate *) date;
+ (NSString *) segundosAReloj:(NSInteger) segundos;
+ (NSString *) segundosACronometro:(float) segundos;
+ (NSComparisonResult) comparaSinHoraFecha:(NSDate *) fecha1 conFecha:(NSDate *) fecha2;
+ (NSDate *) fechaSinHora:(NSDate *) fecha;
+ (NSInteger) diasEntreFecha:(NSDate *)fecha1 yFecha:(NSDate *)fecha2;
+ (NSString *) segundosAString:(NSInteger) segundos;
@end
