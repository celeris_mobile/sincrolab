//
//  ImageUtils.h
//  Tres
//
//  Created by Ricardo Sánchez Sotres on 28/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageUtils : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)imageWithImage:(UIImage *)image croppedToFrame:(CGRect)newFrame;
+ (UIImage *) imageWithColor:(UIColor *) color andSize:(CGSize) size;
+ (UIImage *) imageWithGradientofColors:(NSArray *) colors andSize:(CGSize) size;
+ (UIImage *) imageWithView:(UIView *)view;

@end
