//
//  ImageUtils.m
//  Tres
//
//  Created by Ricardo Sánchez Sotres on 28/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImageUtils.h"
#import <QuartzCore/QuartzCore.h>

@implementation ImageUtils

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    /*
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(newSize, YES, 2.0);
        } else {
            UIGraphicsBeginImageContext(newSize);
        }
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
     */
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *) imageWithColor:(UIColor *) color andSize:(CGSize) size {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return colorImage;
}

+ (UIImage *) imageWithGradientofColors:(NSArray *) colors andSize:(CGSize) size {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Create gradient
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)colors, NULL);
    
    // Apply gradient
    CGContextAddRect(context, CGRectMake(0, 0, size.width, size.height));
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0,0), CGPointMake(size.width, 0), 0);
    
    UIImage *gradientImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return gradientImage;
}

+ (UIImage *) imageWithView:(UIView *)view
{

    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *)imageWithImage:(UIImage *)image croppedToFrame:(CGRect)newFrame {
    
    CGFloat scale;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            scale = 2.0;
        } else {
            scale = 1.0;
        }
    } else {
        scale = 1.0;
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, CGRectMake(newFrame.origin.x*scale, newFrame.origin.y*scale, newFrame.size.width*scale, newFrame.size.height*scale));
    
    UIImage* img = [UIImage imageWithCGImage:imageRef];
    return img;
    /*
    [image drawInRect:CGRectMake(-newFrame.origin.x, -newFrame.origin.y, newFrame.size.width, newFrame.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
     */
     
}
@end
