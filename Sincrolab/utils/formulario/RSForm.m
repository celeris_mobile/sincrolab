//
//  RSForm.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 06/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSForm.h"
#import "RSBloqueForm.h"
#import "RSCampo.h"
#import "RSCampoSwitch.h"
#import "RSCampoView.h"

@interface RSForm()
@property (nonatomic, strong) NSArray* bloques;
@end

@implementation RSForm 
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _colorTitulos = [UIColor redColor];
        _separacionBloques = 30;
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)configuraCampos:(NSArray *)camposFormulario {
    NSMutableArray* bloquesAux = [[NSMutableArray alloc] init];
    for (id elementos in camposFormulario) {
        RSBloqueForm* bloque = [[RSBloqueForm alloc] initWithIndice:bloquesAux.count];
        bloque.delegate = self.delegate;
        [bloque configuraCampos:elementos];
        [self addSubview:bloque];
        [bloquesAux addObject:bloque];
        [bloque addObserver:self forKeyPath:@"frame" options:0 context:NULL];
    }
    
    self.bloques = bloquesAux.copy;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self configuraViews];
}

- (void) configuraViews {
    UIView* anterior;
    for (UIView* bloque in self.bloques) {
        float ancho = bloque.frame.size.width?bloque.frame.size.width:self.bounds.size.width;
        if(anterior) {
            if(anterior.frame.origin.x + anterior.frame.size.width < self.bounds.size.width/3) {
                float alto = MAX(anterior.frame.size.height, bloque.frame.size.height);
                ancho = self.bounds.size.width - anterior.frame.size.width - 10;
                bloque.frame = CGRectMake(anterior.frame.origin.x + anterior.frame.size.width + 10, anterior.frame.origin.y, ancho, alto);
            } else
                bloque.frame = CGRectMake(0, anterior.frame.origin.y + anterior.frame.size.height + self.separacionBloques, ancho, bloque.frame.size.height);
            
        } else
            bloque.frame = CGRectMake(0, 0, ancho, bloque.frame.size.height);
        
        anterior = bloque;
    }
    
    CGRect formFrame = self.frame;
    formFrame.size.height = anterior.frame.origin.y + anterior.frame.size.height;
    self.frame = formFrame;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    for (UIView* bloque in self.bloques) {
        [bloque removeObserver:self forKeyPath:@"frame"];
    }
    
    [self configuraViews];
     
    for (UIView* bloque in self.bloques) {
        [bloque addObserver:self forKeyPath:@"frame" options:0 context:NULL];
    }
}

- (void)dealloc {
    for (UIView* bloque in self.bloques) {
        [bloque removeObserver:self forKeyPath:@"frame"];
    }
}

- (RSCampoView *) buscaCampoConNombre:(NSString *) nombre {
    for (RSBloqueForm* bloque in self.bloques) {
        RSCampoView* campoView = [bloque campoConNombre:nombre];
        if(campoView)
            return campoView;
    }
    return nil;
}

- (void) setValor:(id) valor paraCampoConNombre:(NSString *) nombre {
    RSCampoView* campoView;
    for (RSBloqueForm* bloque in self.bloques) {
        campoView = [bloque campoConNombre:nombre];
        if(campoView)
            break;
    }

    campoView.data.valor = valor;
}

- (NSArray *)listaDeCampos {
    NSMutableArray* auxCampos = [[NSMutableArray alloc] init];
    for (RSBloqueForm* bloque in self.bloques) {
        NSArray* camposViews = [bloque listaDeCamposViews];
        for (RSCampoView* campoView in camposViews) {
            [auxCampos addObject:campoView];
        }
    }
    
    return auxCampos.copy;
}

- (void) agregaCampoEnlazado:(NSString *) nombreCampo aSwitch:(NSString *) nombreSwitch {
    RSCampoView* auxCampoSwitch = [self buscaCampoConNombre:nombreSwitch];
    RSCampoView* campoEnlazado = [self buscaCampoConNombre:nombreCampo];
    if(!campoEnlazado || !auxCampoSwitch) {
        NSLog(@"No se encuentran los campos: %@ : %@", campoEnlazado, auxCampoSwitch);
        return;
    }
    if(![auxCampoSwitch isKindOfClass:[RSCampoSwitch class]]) {
        NSLog(@"No es un campo switch");
        return;
    }
    RSCampoSwitch* campoSwitch = (RSCampoSwitch*)auxCampoSwitch;
    [campoSwitch agregaCampoEnlazado:campoEnlazado];
}

@end
