//
//  RSCampoCombo.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoCombo.h"
#import "UIColor+Extensions.h"
#import "DateUtils.h"

@interface RSCampoCombo()
@property (nonatomic, strong) UIButton* boton;

@property (nonatomic, strong) UIDatePicker* datePicker;
@property (nonatomic, strong) UIPickerView* picker;

@property (nonatomic, strong) UIPopoverController* popOver;
@end

@implementation RSCampoCombo

- (id)initWithCampo:(RSCampo *)campo {
    self = [super initWithCampo:campo];
    if(self) {
        self.boton = [[UIButton alloc] init];
        [self.boton setTitle:@"" forState:UIControlStateNormal];
        [self.boton addTarget:self action:@selector(botonDado) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.boton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self actualizaValor];
    
    self.boton.frame = self.bounds;
}

- (void) botonDado {
    if(self.data.tipo==TipoCampoComboFecha) {
        self.datePicker = [[UIDatePicker alloc] init];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        
        [self.datePicker addTarget:self action:@selector(cambioPickerDate:) forControlEvents:UIControlEventValueChanged];
        
        UIViewController* vc = [[UIViewController alloc] init];
        vc.title = self.data.caption;
        vc.preferredContentSize = self.datePicker.frame.size;
        vc.view = self.datePicker;

        [self muestraPopOver:vc desdeBoton:self.boton paraCampo:self.data];
    }
    
    if(self.data.tipo==TipoCampoComboString || self.data.tipo==TipoCampoComboNumber) {
        self.picker = [[UIPickerView alloc] init];
        self.picker.dataSource = self;
        self.picker.delegate = self;
        
        NSUInteger indice = 0;
        if(self.data.tipo==TipoCampoComboString) {
            indice = [[self.delegate opcionesParaCombo:self.data] indexOfObject:(NSString *)self.data.valor];
        }
        if(self.data.tipo==TipoCampoComboNumber) {
            indice = [[self.delegate opcionesParaCombo:self.data] indexOfObject:[NSNumber numberWithInt:((NSString *)self.data.valor).intValue]];
        }
        
        if(!self.data.valor||indice==NSNotFound) {
            indice = 0;
            self.data.valor = [[self.delegate opcionesParaCombo:self.data] objectAtIndex:0];
            if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
                [self.delegate campoHaCambiado:self.data];
            [self actualizaValor];
        }
        
        [self.picker selectRow:indice inComponent:0 animated:NO];
        
        UIViewController* vc = [[UIViewController alloc] init];
        vc.title = self.data.caption;
        vc.preferredContentSize = self.picker.frame.size;
        vc.view = self.picker;
        
        [self muestraPopOver:vc desdeBoton:self.boton paraCampo:self.data];

    }
}



- (void)muestraPopOver:(UIViewController *)vc desdeBoton:(UIButton *)boton paraCampo:(RSCampo *) campo {
    
    if(self.popOver)
        [self.popOver dismissPopoverAnimated:YES];
    
    if(![vc isKindOfClass:[UINavigationController class]]) {
        UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
        //TO DO ajustar este popOver en iOS 6
        nc.navigationBar.backgroundColor = [UIColor verdeAzulado];
        nc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
        self.popOver = [[UIPopoverController alloc] initWithContentViewController:nc];
    } else {
        self.popOver = [[UIPopoverController alloc] initWithContentViewController:vc];
    }
    
    
    [self.popOver setPopoverContentSize:vc.view.frame.size animated:NO];
    [self.popOver presentPopoverFromRect:self.boton.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight animated:YES];
}


- (void) cambioPickerDate:(UIDatePicker *) picker {
    self.data.valor = picker.date;
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
    [self actualizaValor];
}

- (void) actualizaValor {    
    if([self.data.valor isKindOfClass:[NSDate class]]) {
        /*
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-YYYY"];
        NSString *dateString = [dateFormatter stringFromDate:(NSDate *) self.data.valor];
        */
        [self.textField setText:[DateUtils fechaToTexto:(NSDate *) self.data.valor]];
    }
    if([self.data.valor isKindOfClass:[NSNumber class]]) {
        NSNumber* numero = (NSNumber *)self.data.valor;
        [self.textField setText:numero.stringValue];
    }
    if([self.data.valor isKindOfClass:[NSString class]]) {
        [self.textField setText:self.data.valor];
    }
    
    if(self.data.vacio)
        [self ocultaHint];
    else
        [self muestraHint];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();


    if(self.data.tipo==TipoCampoComboFecha) {
        UIImage* image = [UIImage imageNamed:@"calendario.png"];
        float alturaImg = self.textField.frame.origin.y+(self.textField.frame.size.height/2-image.size.height/2);
        CGContextDrawImage(context, CGRectMake(rect.size.width-image.size.width, alturaImg, image.size.width, image.size.height), image.CGImage);
    }
    
    if(self.data.tipo==TipoCampoComboString || self.data.tipo==TipoCampoComboNumber) {
        UIImage* image = [UIImage imageNamed:@"combo.png"];
        float alturaImg = self.textField.frame.origin.y+(self.textField.frame.size.height/2-image.size.height/2);
        
        CGContextDrawImage(context, CGRectMake(rect.size.width-image.size.width, alturaImg, image.size.width, image.size.height), image.CGImage);
    }
}

- (void)activa {
    [super activa];
    self.boton.enabled = YES;
}

- (void)desactiva {
    [super desactiva];
    self.boton.enabled = NO;
}

- (void) deselecciona {
    self.textField.text = @"";
    self.data.valor = nil;
}
#pragma mark UIPicker DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.delegate opcionesParaCombo:self.data].count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSArray* opciones = [self.delegate opcionesParaCombo:self.data];
    id opcion = [opciones objectAtIndex:row];
    
    if([opcion isKindOfClass:[NSNumber class]])
        return ((NSNumber *)opcion).stringValue;

    if([opcion isKindOfClass:[NSString class]])
        return opcion;
        
    return nil;
}

#pragma mark UIPicker Delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSArray* opciones = [self.delegate opcionesParaCombo:self.data];

    self.data.valor = [opciones objectAtIndex:row];
    
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
    
    [self actualizaValor];
}


@end
