//
//  RSBloqueForm.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSFormDelegate.h"
#import "RSCampoView.h"

@interface RSBloqueForm : UIView
@property (nonatomic, weak) id<RSFormDelegate> delegate;
@property (nonatomic, assign) float separacionLineas;
@property (nonatomic, assign) float separacionCampos;
@property (nonatomic, assign) float alturaLineas;

- (id)initWithIndice:(int) indice;
- (void)configuraCampos:(NSArray *)camposFormulario;
- (RSCampoView *) campoConNombre:(NSString *) nombre;
- (NSArray *) listaDeCamposViews;
@end
