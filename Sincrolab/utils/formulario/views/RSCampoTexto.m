//
//  RSCampoTexto.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoTexto.h"

@interface RSCampoTexto()
@property (nonatomic, strong) NSString* ultimoTexto;
@end

@implementation RSCampoTexto

- (id)initWithCampo:(RSCampo *)campo {
    self = [super initWithCampo:campo];
    if(self) {
        self.textField = [[UITextField alloc] init];
        self.textField.delegate = self;
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.placeholder = campo.caption;
        [self addSubview:self.textField];

        switch (campo.tipo) {
            case TipoCampoEmail: {
                self.textField.keyboardType = UIKeyboardTypeEmailAddress;
                self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                break;
            }
            case TipoCampoNumerico:
                self.textField.keyboardType = UIKeyboardTypeNumberPad;
                break;
            case TipoCampoPassword:
                self.textField.secureTextEntry = YES;
                break;
            default:
                self.textField.keyboardType = UIKeyboardTypeDefault;                
                break;
        }
        
        
        [self.textField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
        
    }
    return self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if (!string.length) {
        return YES;
    }
    
    // remove invalid characters from input, if keyboard is numberpad
    if (textField.keyboardType == UIKeyboardTypeNumberPad) {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) {
            return NO;
        }
    }
    

    
    return YES;
}

- (void) textFieldDidChange {
    if(self.data.tipo == TipoCampoEmail)
        self.data.valor = self.textField.text.lowercaseString;
    else
        self.data.valor = self.textField.text;
    
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
}

- (void) actualizaValor {
    if(![self.textField.text isEqualToString:(NSString *)self.data.valor])
        self.textField.text = (NSString *)self.data.valor;
    
    self.ultimoTexto = self.textField.text;
    
    if(self.data.vacio)
        [self ocultaHint];
    else
        [self muestraHint];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if([self.delegate respondsToSelector:@selector(tipoParaCampo:)])
        self.textField.font = [self.delegate tipoParaCampo:self.data];
    if([self.delegate respondsToSelector:@selector(colorTextoCampo:)])
        self.textField.textColor = [self.delegate colorTextoCampo:self.data];
    
    CGSize tam = [@"Campo Texto" sizeWithAttributes:@{NSFontAttributeName:self.textField.font}];
    self.textField.frame = CGRectInset(CGRectMake(0, (int)(self.bounds.size.height-tam.height), self.bounds.size.width, tam.height), 10, 0);


}

- (void)activa {
    [super activa];
    self.textField.enabled = YES;
    if(self.ultimoTexto)
        self.textField.text = self.ultimoTexto;
}

- (void)desactiva {
    [super desactiva];
    self.textField.text = @"";
    self.textField.enabled = NO;
    self.ultimoTexto = self.textField.text;
}

- (UIResponder *)campoDeTexto {
    return self.textField;
}

@end
