//
//  RSCampoView.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 06/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoView.h"

@interface RSCampoView()
@property (nonatomic, strong) UILabel* labelHint;
@end

@implementation RSCampoView

- (id) initWithCampo:(RSCampo *) campo {
    self = [super init];
    if (self) {
        _data = campo;
        self.backgroundColor = [UIColor clearColor];
        [_data addObserver:self forKeyPath:@"valor" options:0 context:NULL];
        
    }
    return self;
}

- (UILabel *)labelHint {
    if(!_labelHint) {
        _labelHint = [[UILabel alloc] init];
        _labelHint.text = self.data.caption;
        
        if([self.delegate respondsToSelector:@selector(tipoParaHint:)])
            _labelHint.font = [self.delegate tipoParaHint:self.data];
        
        if([self.delegate respondsToSelector:@selector(colorTextoHint:)])
            _labelHint.textColor = [self.delegate colorTextoHint:self.data];
        
        CGSize tam = [@"Campo Texto" sizeWithAttributes:@{NSFontAttributeName:_labelHint.font}];
        _labelHint.frame = CGRectMake(0, 0, self.bounds.size.width, tam.height);
    }
    
    return _labelHint;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self actualizaValor];
}

- (void) actualizaValor {
    NSLog(@"No hace falta llamar a super");
}
    
- (NSString *)description {
    return [NSString stringWithFormat:@"CampoView: %@", self.data.caption];
}

- (void)drawRect:(CGRect)rect {
    if([self.delegate respondsToSelector:@selector(dibujaFondoEnRect:paraCampo:)])
        [self.delegate dibujaFondoEnRect:rect paraCampo:self.data];
    else {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(context, [UIColor lightGrayColor].CGColor);
        CGContextSetLineWidth(context, 1);
        CGContextMoveToPoint(context, 0, rect.size.height);
        CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
        CGContextStrokePath(context);
    }
}

- (void)layoutSubviews {
    CGRect hintFrame = self.labelHint.frame;
    hintFrame.size.width = self.bounds.size.width;
    self.labelHint.frame = hintFrame;
}

- (void) muestraHint {
    [self addSubview:self.labelHint];
}

- (void) ocultaHint {
    [self.labelHint removeFromSuperview];
}

- (void) activa {
    self.alpha = 1.0;
}

- (void) desactiva {
    self.alpha = 0.5;
}

- (UIResponder *)campoDeTexto {
    return nil;
}

- (void)dealloc {
    [self.data removeObserver:self forKeyPath:@"valor"];
}

@end
