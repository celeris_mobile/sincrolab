//
//  RSCampoCombo.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoTexto.h"

@interface RSCampoCombo : RSCampoTexto <UIPickerViewDataSource, UIPickerViewDelegate>
- (void) deselecciona;
@end
