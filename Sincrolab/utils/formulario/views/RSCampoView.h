//
//  RSCampoView.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 06/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSCampo.h"
#import "RSFormDelegate.h"

@interface RSCampoView : UIView
@property (nonatomic, weak) id<RSFormDelegate> delegate;
@property (nonatomic, strong) RSCampo* data;
@property (nonatomic, readonly) UIResponder* campoDeTexto;
- (id) initWithCampo:(RSCampo *) campo;
- (void) actualizaValor;

- (void) muestraHint;
- (void) ocultaHint;

- (void) activa;
- (void) desactiva;

@end
