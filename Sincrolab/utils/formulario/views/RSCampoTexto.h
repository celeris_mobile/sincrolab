//
//  RSCampoTexto.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSCampoView.h"

@interface RSCampoTexto : RSCampoView <UITextFieldDelegate>
@property (nonatomic, strong) UITextField* textField;
@end
