//
//  RSBloqueForm.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSBloqueForm.h"
#import "RSCampoView.h"
#import "RSCampoTexto.h"
#import "RSCampoCombo.h"
#import "RSCampoSwitch.h"
#import "RSCampoTextoLargo.h"
#import "RSCampoPFImagen.h"
#import "RSCampoRadio.h"

@interface RSBloqueForm()
@property (nonatomic, strong) NSDictionary* campos;
@property (nonatomic, strong) NSArray* camposViews;
@property (nonatomic, strong) UILabel* titulo;
@property (nonatomic, assign) int indice;
@end

@implementation RSBloqueForm {
    NSMutableDictionary* auxCamposDict;
}


- (id)initWithIndice:(int) indice {
    self = [super init];
    if (self) {
        _indice = indice;
        
        self.backgroundColor = [UIColor clearColor];
        _separacionLineas = 10;
        _separacionCampos = 15;
        _alturaLineas = 30;
        
    }
    return self;
}

- (UILabel *)titulo {
    if(!_titulo) {
        self.titulo = [[UILabel alloc] init];
        if([self.delegate respondsToSelector:@selector(tipoParaTituloBloque:)]) {
            self.titulo.font = [self.delegate tipoParaTituloBloque:self.indice];
        }
        
        if([self.delegate respondsToSelector:@selector(colorTituloBloque:)]) {
            self.titulo.textColor = [self.delegate colorTituloBloque:self.indice];
        }
    }
    
    return _titulo;
}

- (RSCampoView *) campoViewParaCampo:(RSCampo *) campo {
    RSCampoView* campoView;
    switch (campo.tipo) {
        case TipoCampoTexto:
        case TipoCampoEmail:
        case TipoCampoPassword:
        case TipoCampoNumerico:
            campoView = [[RSCampoTexto alloc] initWithCampo:campo];
            break;
        case TipoCampoTextoLargo:
            campoView = [[RSCampoTextoLargo alloc] initWithCampo:campo];
            break;
        case TipoCampoComboString:
        case TipoCampoComboNumber:
        case TipoCampoComboFecha:
            campoView = [[RSCampoCombo alloc] initWithCampo:campo];
            break;
        case TipoCampoSwitch:
            campoView = [[RSCampoSwitch alloc] initWithCampo:campo];
            break;
        case TipoCampoImagen:
            campoView = [[RSCampoPFImagen alloc] initWithCampo:campo];
            break;
        case TipoCampoRadio:
            campoView = [[RSCampoRadio alloc] initWithCampo:campo];
            break;
        default:
            campoView = nil;
            break;
    }
    
    [auxCamposDict setObject:campoView forKey:campo.nombre];
    campoView.delegate = self.delegate;
    return campoView;
}

- (NSArray *) lineaConCampos:(id) campos {
    NSMutableArray* auxCampos = [[NSMutableArray alloc] init];
    if([campos isKindOfClass:[NSArray class]]) {
        for (id elemento in campos) {
            RSCampoView* campoView = [self campoViewParaCampo:elemento];
            [auxCampos addObject:campoView];
            [self addSubview:campoView];
        }
    } else {
        RSCampoView* campoView = [self campoViewParaCampo:campos];
        [auxCampos addObject:campoView];
        [self addSubview:campoView];
    }
    
    return auxCampos.copy;
}

- (void) configuraCampos:(NSArray *) bloque {
    
    if([self.delegate respondsToSelector:@selector(tituloDeBloque:)]) {
        NSString* titulo = [self.delegate tituloDeBloque:self.indice];
        if(titulo) {
            self.titulo.text = titulo;
            [self addSubview:self.titulo];
        }
    }
    
    auxCamposDict = [[NSMutableDictionary alloc] init];
    
    NSMutableArray* auxLineas = [[NSMutableArray alloc] init];
    if([bloque isKindOfClass:[NSArray class]]) {
        for (id elemento in bloque) {
            [auxLineas addObject:[self lineaConCampos:elemento]];
        }
    } else {
        RSCampoView* campoView = [self campoViewParaCampo:(RSCampo *)bloque];
        [auxLineas addObject:campoView];
        [self addSubview:campoView];
    }
    
    self.camposViews = auxLineas.copy;
    self.campos = auxCamposDict.copy;
    auxCamposDict = nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    float altura = 0;
    float maxAncho = 0;
    
    if(_titulo) {
        CGSize tam = [self.titulo.text sizeWithAttributes:@{NSFontAttributeName:self.titulo.font}];
        self.titulo.frame = CGRectMake(0, self.alturaLineas-tam.height, self.bounds.size.width, self.alturaLineas);
        altura+=self.alturaLineas+self.separacionLineas;
    }
    
    
    for (int l = 0; l<self.camposViews.count; l++) {
        NSArray* linea = [self.camposViews objectAtIndex:l];
        
        NSMutableArray* camposFijos = [[NSMutableArray alloc] init];
        float anchoFijos = 0;
        for (RSCampoView * campo in linea) {
            if(campo.data.ancho) {
                [camposFijos addObject:campo];
                anchoFijos += campo.data.ancho;
            }
        }
        
        float altoCampo = 0;
        RSCampoView* anterior;
        float ancho = (self.bounds.size.width-anchoFijos-self.separacionCampos*(linea.count-1))/(linea.count-camposFijos.count);
        float anchoLinea = 0;
        for(RSCampoView * campo in linea) {
            altoCampo = self.alturaLineas;
            if(campo.data.alto!=0)
                altoCampo = campo.data.alto;
            
            float anchoCampo = campo.data.ancho?:ancho;
            if(!anterior)
                campo.frame = CGRectMake(0, altura, (int)anchoCampo, altoCampo);
            else
                campo.frame = CGRectMake((int)(anterior.frame.origin.x+anterior.frame.size.width+self.separacionCampos), altura, (int)anchoCampo, altoCampo);
            
            anchoLinea+= anchoCampo;
            anterior = campo;
        }
        
        anchoLinea += self.separacionCampos*(linea.count-1);
        maxAncho = MAX(maxAncho, anchoLinea);
        
        if(l==self.camposViews.count-1)
            altura += altoCampo;
        else
            altura += altoCampo+self.separacionLineas;
    }
    
    CGRect viewFrame = self.frame;
    viewFrame.size.height = altura;
    viewFrame.size.width = maxAncho;
    self.frame = viewFrame;
}

- (RSCampoView *) campoConNombre:(NSString *) nombre {
    return [self.campos objectForKey:nombre];
}

- (NSArray *) listaDeCamposViews {
    return self.campos.allValues;
}

@end
