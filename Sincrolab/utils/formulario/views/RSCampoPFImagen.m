//
//  RSCampoImagen.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoPFImagen.h"
#import <Parse/Parse.h>

@interface RSCampoPFImagen()
@property (nonatomic, strong) PFImageView* imageView;
@property (nonatomic, strong) UIButton* boton;

@property (nonatomic, strong) UIPopoverController* popOver;
@end

@implementation RSCampoPFImagen

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[PFImageView alloc] init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        [self addSubview:self.imageView];

        self.layer.borderWidth = 1.0;
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        self.boton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.boton.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.7];
        self.boton.tintColor = [UIColor darkGrayColor];
        [self.boton setTitle:@"Seleccionar" forState:UIControlStateNormal];
        [self.boton addTarget:self action:@selector(botonDado) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.boton];
    }
    return self;
}

- (void)layoutSubviews {
    self.imageView.frame = self.bounds;
    
    self.boton.frame = CGRectMake(0, self.bounds.size.height-30, self.bounds.size.width, 30);
}

- (void)actualizaValor {
    self.imageView.image = nil;
    
    if([self.data.valor isKindOfClass:[UIImage class]])
        self.imageView.image = (UIImage *)self.data.valor;

    if([self.data.valor isKindOfClass:[PFFile class]]) {
        self.imageView.file = self.data.valor;
        [self.imageView loadInBackground];
    }
}

- (void)drawRect:(CGRect)rect {

}

- (void) botonDado {
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Selecciona fuente" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Cámara", @"Galería", nil];
    
    [actionSheet showFromRect:self.boton.frame inView:self animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex==0)
        [self muestraPicker:UIImagePickerControllerSourceTypeCamera];
    if(buttonIndex==1)
        [self muestraPicker:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void) muestraPicker:(UIImagePickerControllerSourceType) fuente {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    picker.sourceType = fuente;
    picker.delegate = self;
    picker.allowsEditing = YES;

    if(picker.sourceType != UIImagePickerControllerSourceTypeCamera) {
        if(self.popOver)
            [self.popOver dismissPopoverAnimated:YES];
        
        
        self.popOver = [[UIPopoverController alloc] initWithContentViewController:picker];
        [self.popOver presentPopoverFromRect:self.boton.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight animated:YES];
    } else {
        [self.vc presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage* imagen = [info objectForKey:UIImagePickerControllerEditedImage];
    
    self.data.valor = imagen;
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
    [self actualizaValor];
    
    if(self.popOver)
        [self.popOver dismissPopoverAnimated:YES];
    else {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}


@end
