//
//  RSCampoSwitch.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoSwitch.h"

@interface RSCampoSwitch()
@property (nonatomic, strong) UILabel* label;
@property (nonatomic, strong) UIButton* boton;
@property (nonatomic, strong) NSArray* camposEnlazados;
@end

@implementation RSCampoSwitch

- (id)initWithCampo:(RSCampo *)campo {
    self = [super initWithCampo:campo];
    if(self) {
        // Initialization code

        self.label = [[UILabel alloc] init];
        self.label.text = self.data.caption;
        [self addSubview:self.label];
        
        UIImage* imgSwitchOn = [UIImage imageNamed:@"switchon.png"];
        UIImage* imgSwitchOff = [UIImage imageNamed:@"switchoff.png"];
        self.boton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgSwitchOn.size.width, imgSwitchOn.size.height)];
        [self.boton setImage:imgSwitchOn forState:UIControlStateSelected];
        [self.boton setImage:imgSwitchOff forState:UIControlStateNormal];
        [self.boton addTarget:self action:@selector(botonDado) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.boton];
        
        self.data.valor = [NSNumber numberWithBool:self.boton.selected];

    }
    return self;
}

- (void) botonDado {
    self.boton.selected = !self.boton.selected;
    self.data.valor = [NSNumber numberWithBool:self.boton.selected];
    
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
    

}

- (void) actualizaValor {
    self.boton.selected = ((NSNumber *)self.data.valor).boolValue;
    
    if(self.boton.selected) {
        for (RSCampoView* campo in self.camposEnlazados) {
            [campo activa];
        }
    } else {
        for (RSCampoView* campo in self.camposEnlazados) {
            [campo desactiva];
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self actualizaValor];
    
    if([self.delegate respondsToSelector:@selector(tipoParaCampo:)])
        self.label.font = [self.delegate tipoParaCampo:self.data];
    if([self.delegate respondsToSelector:@selector(colorTextoCampo:)])
        self.label.textColor = [self.delegate colorTextoCampo:self.data];
    
    //self.label.frame = CGRectMake(0, 0, self.bounds.size.width-self.boton.frame.size.width-5, self.bounds.size.height);
    CGSize tam = [self.label.text sizeWithAttributes:@{NSFontAttributeName:self.label.font}];
    self.label.frame = CGRectMake(0, (int)(self.bounds.size.height-tam.height), self.bounds.size.width, tam.height);

    CGRect botonFrame = self.boton.frame;
    botonFrame.origin.x = self.bounds.size.width-self.boton.frame.size.width;
    self.boton.frame = botonFrame;
    self.boton.center = CGPointMake(self.boton.center.x, self.label.center.y);
    
    self.data.ancho = tam.width + self.boton.frame.size.width + 10;
    
}

- (void)drawRect:(CGRect)rect {
    
}

- (void)activa {
    [super activa];
    self.boton.enabled = YES;
}

- (void)desactiva {
    [super desactiva];
    self.boton.selected = NO;
    self.boton.enabled = NO;
}

- (NSArray *)camposEnlazados {
    if(!_camposEnlazados)
        _camposEnlazados = [[NSArray alloc] init];
    
    return _camposEnlazados;
}

- (void) agregaCampoEnlazado:(RSCampoView *) campo {
    NSMutableArray* mutableCampos = self.camposEnlazados.mutableCopy;
    [mutableCampos addObject:campo];
    self.camposEnlazados = mutableCampos.copy;
    
    if(self.boton && !self.boton.selected)
        [campo desactiva];
    else
        [campo activa];
}

@end
