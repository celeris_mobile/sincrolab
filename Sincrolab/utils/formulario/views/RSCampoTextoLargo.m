//
//  RSCampoTextoLargo.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoTextoLargo.h"
#import "UIPlaceHolderTextView.h"

@interface RSCampoTextoLargo()
@property (nonatomic, strong) UIPlaceHolderTextView* textView;
@property (nonatomic, strong) NSString* ultimoTexto;
@end

@implementation RSCampoTextoLargo

- (id)initWithCampo:(RSCampo *)campo {
    self = [super initWithCampo:campo];
    if(self) {
        self.textView = [[UIPlaceHolderTextView alloc] init];
        self.textView.placeholderColor = [UIColor lightGrayColor];
        self.textView.backgroundColor = [UIColor clearColor];
        self.textView.placeholder = campo.caption;
        [self addSubview:self.textView];
        
        switch (campo.tipo) {
            case TipoCampoEmail:
                self.textView.keyboardType = UIKeyboardTypeEmailAddress;
                break;
            case TipoCampoNumerico:
                self.textView.keyboardType = UIKeyboardTypeNumberPad;
                break;
            case TipoCampoPassword:
                self.textView.secureTextEntry = YES;
                break;
            default:
                self.textView.keyboardType = UIKeyboardTypeDefault;
                break;
        }
        
        
        self.textView.delegate = self;
    }
    return self;
}

- (void) textViewDidChange:(UITextView *)textView {
    self.data.valor = self.textView.text;
    
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
}

- (void) actualizaValor {
    self.textView.text = (NSString *)self.data.valor;
    self.ultimoTexto = self.textView.text;    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self actualizaValor];
    
    if([self.delegate respondsToSelector:@selector(tipoParaCampo:)])
        self.textView.font = [self.delegate tipoParaCampo:self.data];
    if([self.delegate respondsToSelector:@selector(colorTextoCampo:)])
        self.textView.textColor = [self.delegate colorTextoCampo:self.data];
    
    self.textView.frame = self.bounds;
}

- (void)activa {
    [super activa];
    self.textView.editable = YES;
    if(self.ultimoTexto)
        self.textView.text = self.ultimoTexto;
}

- (void)desactiva {
    [super desactiva];
    self.ultimoTexto = self.textView.text;
    self.textView.text = @"";
    self.textView.editable = NO;
}

- (UIResponder *)campoDeTexto {
    return self.textView;
}

@end
