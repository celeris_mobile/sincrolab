//
//  RSCampoRadio.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampoRadio.h"

@interface RSCampoRadio()
@property (nonatomic, strong) UILabel* label;
@property (nonatomic, strong) NSArray* botones;
@property (nonatomic, strong) NSArray* labels;

@property (nonatomic, weak) UIButton* seleccionado;
@end

@implementation RSCampoRadio

- (id)initWithCampo:(RSCampo *)campo {
    self = [super initWithCampo:campo];
    if(self) {
        
        self.label = [[UILabel alloc] init];
        self.label.text = [NSString stringWithFormat:@"%@:",self.data.caption];
        [self addSubview:self.label];

        
    }
    return self;
}

- (void)setDelegate:(id<RSFormDelegate>)delegate {
    [super setDelegate:delegate];

    if(self.botones) {
        for (UIButton* boton in self.botones) {
            [boton removeFromSuperview];
        }
        self.botones = nil;
        for (UILabel* label in self.labels) {
            [label removeFromSuperview];
        }
        self.labels = nil;
    }
    
    UIImage* imgRadionOn = [UIImage imageNamed:@"radioon.png"];
    UIImage* imgRadionOff = [UIImage imageNamed:@"radiooff.png"];
    
    NSMutableArray* auxBotones = [[NSMutableArray alloc] init];
    NSMutableArray* auxLabels = [[NSMutableArray alloc] init];

    NSArray* opciones = [self.delegate opcionesParaRadio:self.data];
    for (int o = 0; o<opciones.count; o++) {
        UIButton* boton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgRadionOn.size.width, imgRadionOn.size.height)];
        boton.tag = o;
        [boton setImage:imgRadionOn forState:UIControlStateSelected];
        [boton setImage:imgRadionOff forState:UIControlStateNormal];
        [boton addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:boton];
        [auxBotones addObject:boton];
        
        UILabel* label = [[UILabel alloc] init];
        if([self.delegate respondsToSelector:@selector(tipoParaCampo:)])
            label.font = [self.delegate tipoParaCampo:self.data];
        if([self.delegate respondsToSelector:@selector(colorTextoCampo:)])
            label.textColor = [self.delegate colorTextoCampo:self.data];
        [self addSubview:label];
        [auxLabels addObject:label];
    }
    
    self.botones = auxBotones.copy;
    self.labels = auxLabels.copy;
    
    self.seleccionado = [self.botones objectAtIndex:0];
    self.seleccionado.selected = YES;
}

- (void) botonDado:(UIButton *) boton {
    self.seleccionado.selected = NO;
    self.seleccionado = boton;
    self.seleccionado.selected = YES;
    
    self.data.valor = [NSNumber numberWithInt:boton.tag];
    
    if([self.delegate respondsToSelector:@selector(campoHaCambiado:)])
        [self.delegate campoHaCambiado:self.data];
}

- (void) actualizaValor {
    self.seleccionado.selected = NO;
    self.seleccionado = [self.botones objectAtIndex:((NSNumber *)self.data.valor).intValue];
    self.seleccionado.selected = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if([self.delegate respondsToSelector:@selector(tipoParaCampo:)])
        self.label.font = [self.delegate tipoParaCampo:self.data];
    if([self.delegate respondsToSelector:@selector(colorTextoCampo:)])
        self.label.textColor = [self.delegate colorTextoCampo:self.data];
    
    CGSize tam = [self.label.text sizeWithAttributes:@{NSFontAttributeName:self.label.font}];
    self.label.frame = CGRectMake(0, (int)(self.bounds.size.height/2-tam.height/2), tam.width, tam.height);

    float xinicial = self.label.frame.origin.x + self.label.frame.size.width + 10;
    UILabel *anterior;
    UIButton* boton;
    for(int o=0; o<self.botones.count; o++) {
        boton = [self.botones objectAtIndex:o];


        UILabel* label  = [self.labels objectAtIndex:o];
        label.text = [[self.delegate opcionesParaRadio:self.data] objectAtIndex:o];
        CGSize tam = [label.text sizeWithAttributes:@{NSFontAttributeName:self.label.font}];
        if(anterior)
            label.frame = CGRectMake(anterior.frame.origin.x+anterior.frame.size.width+15+boton.frame.size.width, self.label.frame.origin.y, tam.width, tam.height);
        else
            label.frame = CGRectMake(xinicial, self.label.frame.origin.y, tam.width, tam.height);
        
        boton.center = CGPointMake(label.frame.origin.x + tam.width + 15, label.center.y);
        
        
        anterior = label;
    }
 
    self.data.ancho = boton.frame.origin.x + boton.frame.size.width + 10;
}

- (void)activa {
    [super activa];
    self.alpha = 1.0;
    for (UIButton* boton in self.botones) {
        boton.enabled = YES;
    }
}

- (void)desactiva {
    [super desactiva];
    self.alpha = 0.5;
    for (UIButton* boton in self.botones) {
        boton.enabled = NO;
    }
}

- (void)drawRect:(CGRect)rect {
    
}

@end
