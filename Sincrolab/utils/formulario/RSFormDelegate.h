//
//  RSFormDelegate.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSCampo.h"

@protocol RSFormDelegate <NSObject>

@optional
- (UIFont *) tipoParaCampo:(RSCampo *) campo;
- (UIColor *) colorTextoCampo:(RSCampo *) campo;
- (UIFont *) tipoParaHint:(RSCampo *) campo;
- (UIColor *) colorTextoHint:(RSCampo *) campo;
- (UIFont *)tipoParaTituloBloque:(NSInteger) numBloque;
- (UIColor *) colorTituloBloque:(NSInteger) numBloque;
- (void) dibujaFondoEnRect:(CGRect)rect paraCampo:(RSCampo *) campo;

- (void) campoHaCambiado:(RSCampo *) campo;

- (NSString *) tituloDeBloque:(NSInteger) numBloque;
- (NSArray *) opcionesParaCombo:(RSCampo *) campo;
- (NSArray *) opcionesParaRadio:(RSCampo *) campo;
@end
