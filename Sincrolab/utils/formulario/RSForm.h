//
//  RSForm.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 06/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSCampoView.h"
#import "RSCampo.h"
#import "RSFormDelegate.h"

@interface RSForm : UIView
@property (nonatomic, weak) id<RSFormDelegate> delegate;
@property (nonatomic, strong) UIColor* colorTitulos;
@property (nonatomic, assign) float separacionBloques;

@property (nonatomic, readonly) NSArray* campos;
- (void)configuraCampos:(NSArray *)camposFormulario;
- (void) setValor:(id) valor paraCampoConNombre:(NSString *) nombreCampo;
- (RSCampoView *) buscaCampoConNombre:(NSString *) nombre;
- (NSArray *) listaDeCampos;

- (void) agregaCampoEnlazado:(NSString *) nombreCampo aSwitch:(NSString *) nombreSwitch;
@end
