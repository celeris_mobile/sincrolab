//
//  RSCampo.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 06/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RSCampo.h"


@implementation RSCampo

+ (RSCampo *) campoConNombre:(NSString *) nombre {
    return [RSCampo campoConNombre:nombre Tipo:TipoCampoTexto];
}

+ (RSCampo *) campoConNombre:(NSString *) nombre Tipo:(TipoCampo) tipo {
    return [RSCampo campoConNombre:nombre Tipo:tipo Ancho:0.0];
}

+ (RSCampo *) campoConNombre:(NSString *) nombre Ancho:(float)ancho {
    return [RSCampo campoConNombre:nombre Tipo:TipoCampoTexto Ancho:ancho];
}

+ (RSCampo *) campoConNombre:(NSString *) nombre Tipo:(TipoCampo) tipo Ancho:(float) ancho {
    RSCampo* campo = [[RSCampo alloc] initWithNombre:nombre tipo:tipo yAncho:ancho];
    return campo;
}

- (NSString *) camelCaseToSpaces:(NSString *)input {
    NSMutableString *output = [NSMutableString string];
    NSCharacterSet *uppercase = [NSCharacterSet uppercaseLetterCharacterSet];
    for (NSInteger idx = 0; idx < [input length]; idx += 1) {
        unichar c = [input characterAtIndex:idx];
        if ([uppercase characterIsMember:c]) {
            [output appendFormat:@" %@", [[NSString stringWithCharacters:&c length:1] lowercaseString]];
        } else {
            if(idx==0)
                [output appendFormat:@"%@", [[NSString stringWithCharacters:&c length:1] uppercaseString]];
            else
                [output appendFormat:@"%C", c];
        }
    }
    return output;
}

- (id)initWithNombre:(NSString *) nombre tipo:(TipoCampo) tipo yAncho:(float) ancho {
    self = [super init];
    if(self) {
        NSString* nombreFinal = [[nombre componentsSeparatedByString:@"."] lastObject];
        _caption = [self camelCaseToSpaces:nombreFinal];
        _nombre = nombre;
        _tipo = tipo;
        _ancho = ancho;
    }
    return self;
}

-(NSString *)description {
    return [NSString stringWithFormat:@"campo: %@", _caption];
}

- (float)alto {
    if(self.tipo==TipoCampoImagen)
        return 150;
    if(self.tipo==TipoCampoTextoLargo)
        return 100;
    else
        return 0;
}

- (BOOL) vacio {
    if(!self.valor)
        return YES;
    if([self.valor isKindOfClass:[NSString class]]) {
        if([self.valor isEqualToString:@""])
            return YES;
    }
    
    return NO;
}

@end
