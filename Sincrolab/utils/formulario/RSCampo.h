//
//  RSCampo.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 06/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TipoCampo) {
    TipoCampoTexto,
    TipoCampoTextoLargo,
    TipoCampoComboFecha,
    TipoCampoComboString,
    TipoCampoComboNumber,
    TipoCampoEmail,
    TipoCampoPassword,
    TipoCampoNumerico,
    TipoCampoSwitch,
    TipoCampoLabel,
    TipoCampoImagen,
    TipoCampoRadio
};

@interface RSCampo : NSObject
@property (nonatomic, strong) NSString* caption;
@property (nonatomic, readonly) NSString* nombre;
@property (nonatomic, readonly) TipoCampo tipo;
@property (nonatomic, assign) float ancho;
@property (nonatomic, readonly) float alto;

@property (nonatomic, strong) id valor;
@property (nonatomic, readonly) BOOL vacio;

+ (RSCampo *) campoConNombre:(NSString *) nombre;
+ (RSCampo *) campoConNombre:(NSString *) nombre Tipo:(TipoCampo) tipo;
+ (RSCampo *) campoConNombre:(NSString *) nombre Ancho:(float) ancho;
+ (RSCampo *) campoConNombre:(NSString *) nombre Tipo:(TipoCampo) tipo Ancho:(float) ancho;

@end
