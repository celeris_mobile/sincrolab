//
//  JuegoViewController.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 13/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CocosViewController.h"
#import "DiaEntrenamiento.h"
#import "EstadoEntrenamiento.h"
#import "Paciente.h"

@class JuegoViewController;

@protocol JuegoViewControllerDelegate <NSObject>
- (void) juegoTerminado:(id) partida;
@end

@interface JuegoViewController : CocosViewController
- (id) initWithPaciente:(Paciente *) paciente estadoEntrenamiento:(EstadoEntrenamiento *) estado;
@property (nonatomic, strong) id<JuegoViewControllerDelegate> delegate;
@property (nonatomic, readonly) EstadoEntrenamiento* estadoEntrenamiento;
@property (nonatomic, readonly) Paciente* paciente;
@end
