//
//  InstruccionesLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"

typedef NS_ENUM(NSInteger, TipoBoton) {
    TipoBotonNinguno,
    TipoBotonSiguiente,
    TipoBotonTerminar
};

@class InstruccionesLayer;

@protocol InstruccionesLayerDelegate <NSObject>
- (void) botonSiguienteDado:(InstruccionesLayer *) instrucciones;
@end

@interface InstruccionesLayer : CCLayer
@property (nonatomic, weak) id<InstruccionesLayerDelegate> delegate;
- (void) muestraConTexto:(NSString *) texto tipoBoton:(TipoBoton) tipoBoton;
- (void) oculta;
@end
