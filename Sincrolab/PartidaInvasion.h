//
//  PartidaInvasion.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@interface PartidaInvasion : PFObject <PFSubclassing>
@property (retain) NSNumber* cambioNivel;
@property (retain) NSMutableArray* sesiones;

@property (nonatomic, readonly) NSNumber* duracion;
@property (nonatomic, readonly) NSNumber* puntos;

@end
