//
//  EstadisticasViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaEdad.h"
#import "GraficaSexo.h"
#import "PacientesTerapeutaModel.h"
#import "MenuSeccionEstadisticas.h"

@interface EstadisticasViewController : UIViewController <GraficaEdadDatasource, GraficaSexoDatasource, MenuSeccionEstadisticasDelegate>
@property (nonatomic, strong) PacientesTerapeutaModel* modelo;
@end
