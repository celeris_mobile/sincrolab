//
//  EstadisticasViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadisticasViewController.h"
#import "PanelGrande.h"
#import "UIColor+Extensions.h"
#import "DiaEntrenamiento.h"
#import "PanelPerfiles.h"

@interface EstadisticasViewController ()
@property (weak, nonatomic) IBOutlet GraficaEdad *graficaEdad;
@property (weak, nonatomic) IBOutlet GraficaSexo *graficaSexo;
@property (weak, nonatomic) IBOutlet PanelGrande *panelGrande;
@property (weak, nonatomic) IBOutlet PanelPerfiles *panelPerfiles;
@property (nonatomic, strong) MenuSeccionEstadisticas* menu;

@end

@implementation EstadisticasViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.graficaEdad.datasource = self;
    self.graficaEdad.titulo = @"Edad";
    self.graficaEdad.grafica.frame = CGRectMake(128, 26, 130, 132);

    self.graficaSexo.datasource = self;
    self.graficaSexo.titulo = @"Sexo";
    self.graficaSexo.grafica.frame = CGRectMake(128, 26, 130, 132);
    
    self.menu = [[MenuSeccionEstadisticas alloc] init];
    self.menu.delegate = self;
    [self.view addSubview:self.menu];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

    [self actualizaGraficas];
}

- (void)cargaEstadisticas:(TipoEstadisticas)tipo {
    if(self.modelo.todosLosDias.count)
        self.panelGrande.tipo = tipo;
}

- (void)setModelo:(PacientesTerapeutaModel *)modelo {
    _modelo = modelo;
    
    self.panelGrande.tipo = TipoEstadisticasGenerales;
    [self actualizaGraficas];
}

- (void) actualizaGraficas {
    if(!self.modelo.pacientes.count)
        return;
    
    [self.graficaEdad refresca];
    [self.graficaSexo refresca];
    self.panelPerfiles.modelo = self.modelo;

    if(!self.modelo.todosLosDias) {
        [self.modelo cargaTodosLosDiasDeEntrenamiento:^(NSError * error) {
            if(!error)
                self.panelGrande.modelo = self.modelo;
        }];
    } else {
        self.panelGrande.modelo = self.modelo;
    }

}

#pragma mark GraficaEdadDatasource
- (NSArray *) valoresGraficaEdad {
    return [self.modelo gruposDeEdad];
}

#pragma mark GraficaSexoDatasource
- (NSArray *) valoresGraficaSexo {
    return [self.modelo sexos];
}

@end
