//
//  PanelPerfiles.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelPerfiles.h"
#import "UIColor+Extensions.h"
#import "GraficaBarras.h"

@interface PanelPerfiles()
@property (nonatomic, strong) GraficaBarras* grafica;
@property (nonatomic, strong) NSArray* procesos;
@end

@implementation PanelPerfiles

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configura];
    }
    return self;
}

- (void) configura {
    UILabel* tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 12, 250, 14)];
    tituloLabel.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    tituloLabel.text = @"Perfiles Cognitivos";
    tituloLabel.textColor = [UIColor azulOscuro];
    [self addSubview:tituloLabel];
    
    self.grafica = [[GraficaBarras alloc] initWithFrame:CGRectMake(113, 41, 172, 179)];
    self.grafica.dataSource = self;
    self.grafica.delegate = self;
    self.grafica.graphInset = CGSizeMake(10, 10);
    self.grafica.labelFont = [UIFont fontWithName:@"Lato-Regular" size:11.0];
    [self.grafica setLimites:CGRectMake(0.0, 0.0, 8.0, 10.0)];
    [self addSubview:self.grafica];
}

- (void)setModelo:(PacientesTerapeutaModel *)modelo {
    _modelo = modelo;
    
    if(self.modelo.pacientes) {
        NSString* proceso;
        NSMutableArray* auxprocesos = [[NSMutableArray alloc] init];
        for (int i = 0; i<8; i++) {
            switch (i) {
                case 0:
                    proceso = @"atencionSostenida";
                    break;
                case 1:
                    proceso = @"controlInhibitorio";
                    break;
                case 2:
                    proceso = @"memoriaOperativa";
                    break;
                case 3:
                    proceso = @"atencionSelectiva";
                    break;
                case 4:
                    proceso = @"flexibilidadCognitiva";
                    break;
                case 5:
                    proceso = @"velocidadDeProcesamiento";
                    break;
                case 6:
                    proceso = @"tomaDeDecisiones";
                    break;
                case 7:
                    proceso = @"atencionDividida";
                    break;
            }
            
            [auxprocesos addObject:[self.modelo.pacientes valueForKeyPath:[NSString stringWithFormat:@"@sum.perfilcognitivo.%@", proceso]]];
        }
        
        self.procesos = auxprocesos;
        NSNumber* max = [self.procesos valueForKeyPath:@"@max.self"];
        
        [self.grafica setLimites:CGRectMake(0.0, 0.0, 8.0, max.floatValue?:1.0)];
    }
    
    [self.grafica reloadData];
}

#pragma mark GraficaBarrasDatasource
- (int)numeroDeValoresEnGraficaDeBarras:(GraficaBarras *)grafica {
    return 8;
}

- (NSNumber *)graficaDeBarras:(GraficaBarras *)grafica valorParaIndice:(NSInteger)indice {
    NSNumber* valor =  [self.procesos objectAtIndex:indice];
    if(!valor)
        return [NSNumber numberWithInt:0.0];
    
    return valor;
}

- (NSString *)graficaDeBarras:(GraficaBarras *)grafica etiquetaParaIndice:(NSInteger)indice {
    NSArray* procesos = [NSArray arrayWithObjects:@"Atención sostenida", @"Control inhibitorio", @"Memoria operativa", @"Atención selectiva", @"Flexibilidad cognitiva", @"Vel. procesamiento", @"Toma de decisiones", @"Atención dividida", nil];
    
    return [procesos objectAtIndex:indice];
}

#pragma mark GraficaBarras Delegate

- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context {
    NSArray* procesos = [NSArray arrayWithObjects:[NSNumber numberWithInt:AtencionSostenida], [NSNumber numberWithInt:ControlInhibitorio], [NSNumber numberWithInt:MemoriaOperativa], [NSNumber numberWithInt:AtencionSelectiva], [NSNumber numberWithInt:FlexibilidadCognitiva], [NSNumber numberWithInt:VelocidadDeProcesamiento], [NSNumber numberWithInt:TomaDeDecisiones], [NSNumber numberWithInt:AtencionDividida], nil];
    
    NSNumber* numProceso = [procesos objectAtIndex:numSet];
    UIColor * color = [UIColor colorProceso:numProceso.intValue];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
}

@end
