//
//  PanelPerfiles.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PacientesTerapeutaModel.h"
#import "GraficaBarras.h"

@interface PanelPerfiles : UIView <GraficaBarrasDatasource, GraficaBarrasDelegate>
@property (nonatomic, strong) PacientesTerapeutaModel* modelo;
@end
