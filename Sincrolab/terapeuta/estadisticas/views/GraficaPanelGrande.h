//
//  GraficaPanelGrande.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaLineas.h"

@interface GraficaPanelGrande : UIView <GraficaLineasDelegate>
- (id) initWithTitulo:(NSString *) titulo;
@property (nonatomic, strong) GraficaLineas* grafica;
- (void) dibuja;
@property (nonatomic, assign) BOOL isProcesos;
@end
