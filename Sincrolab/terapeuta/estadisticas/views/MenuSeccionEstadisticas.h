//
//  MenuEstadisticasView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, TipoEstadisticas) {
    TipoEstadisticasGenerales,
    TipoEstadisticasGongs,
    TipoEstadisticasInvasion,
    TipoEstadisticasPoker,
};

@protocol MenuSeccionEstadisticasDelegate <NSObject>
- (void) cargaEstadisticas:(TipoEstadisticas) tipo;
@end

@interface MenuSeccionEstadisticas : UIView
@property (nonatomic, weak) id<MenuSeccionEstadisticasDelegate> delegate;
@end
