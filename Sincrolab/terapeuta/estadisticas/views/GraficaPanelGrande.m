//
//  GraficaPanelGrande.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaPanelGrande.h"
#import "UIColor+Extensions.h"

@interface GraficaPanelGrande()
@property (nonatomic, strong) NSArray* botones;
@end

@implementation GraficaPanelGrande

- (id) initWithTitulo:(NSString *) titulo {
    self = [super initWithFrame:CGRectMake(0, 0, 610, 268)];
    if (self) {
        UILabel* tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 14)];
        tituloLabel.font = [UIFont fontWithName:@"Lato-Bold" size:13];
        tituloLabel.text = titulo;
        tituloLabel.textColor = [UIColor azulOscuro];
        [self addSubview:tituloLabel];
        
        self.grafica = [[GraficaLineas alloc] initWithFrame:CGRectMake(0, 24, 563, 148)];
        self.grafica.dibujaLineasX = NO;
        self.grafica.dibujaLineasY = YES;
        self.grafica.dibujaEtiquetasY = YES;
        self.grafica.delegate = self;
        [self addSubview:self.grafica];
        
        self.isProcesos = NO;
        
       // if(self.leyenda)
         //   [self dibujaLeyenda];
    }
    return self;
}

- (void)dibuja {
    if(self.botones) {
        for (UIButton* boton in self.botones) {
            [boton removeFromSuperview];
        }
    }
    
    UIImage* imageOjo = [UIImage imageNamed:@"ojo.png"];
    NSMutableArray* auxBotones = [[NSMutableArray alloc] init];
    for (int s = 0; s<[self.grafica.dataSource numeroDeSetsEnGraficaDeLineas:self.grafica]; s++) {
        UIButton* boton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width-100-50*s, 6, imageOjo.size.width, imageOjo.size.height)];
        [boton setImage:imageOjo forState:UIControlStateNormal];
        [boton addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
        boton.selected = YES;
        [self addSubview:boton];
        [auxBotones addObject:boton];
        
        UIView* circulo = [[UIView alloc] initWithFrame:CGRectMake(boton.frame.origin.x+26, 8, 9, 9)];
        circulo.backgroundColor = [UIColor colorGrafica:s conAlpha:1.0];
        circulo.layer.cornerRadius = 4.5;
        [self addSubview:circulo];
    }
    self.botones = auxBotones.copy;
    
    [self.grafica dibuja];
}

- (void) botonDado:(UIButton *) boton {
    boton.selected = !boton.selected;
    boton.alpha = boton.selected?1.0:0.3;
    [self.grafica dibuja];
}

#pragma mark GraficaLineasDelegate
- (void)grafica:(GraficaLineas *)grafica configuraSet:(NSInteger)numSet enContexto:(CGContextRef)context {
    UIButton* boton = [self.botones objectAtIndex:numSet];
    UIColor* color = [UIColor colorGrafica:numSet conAlpha:1.0*boton.selected?1.0:0.0];
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextSetLineWidth(context, 3);
    
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
}

- (void)grafica:(GraficaLineas *)grafica dibujaPunto:(CGPoint)punto deSet:(NSInteger) numSet conValor:(NSNumber *)valor enContexto:(CGContextRef)context {
    UIButton* boton = [self.botones objectAtIndex:numSet];
    
    UIColor* color;
    if(!self.isProcesos)
        color = [UIColor colorGrafica:numSet conAlpha:1.0*boton.selected?1.0:0.0];
    else
        color = [[UIColor colorProceso:numSet] colorWithAlphaComponent:boton.selected?1.0:0.0];
    
    CGFloat radio = 10;
    CGContextAddEllipseInRect(context, CGRectMake(punto.x-radio/2, punto.y-radio/2, radio, radio));
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetStrokeColorWithColor(context, [[UIColor whiteColor] colorWithAlphaComponent:boton.selected?1.0:0.0].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextDrawPath(context, kCGPathEOFillStroke);
}

- (void)grafica:(GraficaLineas *)grafica dadoValor:(NSString *) valor enIndexPath:(NSIndexPath *)indexPath posicion:(CGPoint)punto {
}



@end
