//
//  PanelGrande.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelGrande.h"
#import "GraficaPanelGrande.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "DateUtils.h"

@interface PanelGrande()
@property (nonatomic, strong) GraficaPanelGrande* graficaEdades;
@property (nonatomic, strong) GraficaPanelGrande* graficaSexos;
@property (nonatomic, strong) GraficaPanelGrande* graficaPerfiles;
@property (nonatomic, strong) NSArray* etiquetas;

@property (nonatomic, strong) NSArray* nivelesPorEdad;
@property (nonatomic, strong) NSArray* nivelesPorSexo;
@property (nonatomic, strong) NSArray* nivelesPorPerfil;


@property (nonatomic, strong) UIButton* btnInicio;
@property (nonatomic, strong) UIButton* btnFinal;

@property (nonatomic, strong) NSDate* fechaInicial;
@property (nonatomic, strong) NSDate* fechaFinal;
@property (nonatomic, strong) UIDatePicker* datePicker;
@property (nonatomic, strong) UIPopoverController* popOver;

@end

@implementation PanelGrande

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self configura];
    }
    return self;
}

- (void) configura {

    self.graficaEdades = [[GraficaPanelGrande alloc] initWithTitulo:@"Por edades"];
    self.graficaEdades.grafica.dataSource = self;
    CGRect edadesFrame = self.graficaEdades.frame;
    edadesFrame.origin.x = 24;
    edadesFrame.origin.y = 40;
    self.graficaEdades.frame = edadesFrame;
    [self addSubview:self.graficaEdades];
    
    self.graficaSexos = [[GraficaPanelGrande alloc] initWithTitulo:@"Por sexos"];
    self.graficaSexos.grafica.dataSource = self;
    CGRect sexosFrame = self.graficaSexos.frame;
    sexosFrame.origin.x = 24;
    sexosFrame.origin.y = 235;
    self.graficaSexos.frame = sexosFrame;
    [self addSubview:self.graficaSexos];
    
    self.graficaPerfiles = [[GraficaPanelGrande alloc] initWithTitulo:@"Por perfiles"];
    self.graficaPerfiles.isProcesos = YES;
    self.graficaPerfiles.grafica.dataSource = self;
    CGRect trastornosFrame = self.graficaPerfiles.frame;
    trastornosFrame.origin.x = 24;
    trastornosFrame.origin.y = 423;
    self.graficaPerfiles.frame = trastornosFrame;
    [self addSubview:self.graficaPerfiles];

    UIImage *imgFondoBoton = [UIImage imageNamed:@"btnfechabarra.png"];
    
    self.btnInicio = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnInicio.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    [self.btnInicio setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnInicio setTitleEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 20)];
    [self.btnInicio setBackgroundImage:imgFondoBoton forState:UIControlStateNormal];
    self.btnInicio.frame = CGRectMake(400.0, 10.0, imgFondoBoton.size.width, imgFondoBoton.size.height);
    [self.btnInicio addTarget:self action:@selector(botonFechaDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnInicio];
    
    self.btnFinal = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnFinal.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    [self.btnFinal setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnFinal setTitleEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 20)];
    self.btnFinal.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.btnFinal setBackgroundImage:imgFondoBoton forState:UIControlStateNormal];
    self.btnFinal.frame = CGRectMake(500.0, 10.0, imgFondoBoton.size.width, imgFondoBoton.size.height);
    [self.btnFinal addTarget:self action:@selector(botonFechaDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnFinal];
    
    [self actualizaBotonesFecha];
}


- (void) botonFechaDado:(UIButton *) button {
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.datePicker addTarget:self action:@selector(cambioPickerDate:) forControlEvents:UIControlEventValueChanged];
    
    UIViewController* vc = [[UIViewController alloc] init];
    vc.preferredContentSize = self.datePicker.frame.size;
    vc.view = self.datePicker;
    
    if(button==self.btnInicio) {
        vc.title = @"Fecha Inicial";
        self.datePicker.tag = 0;
        self.datePicker.date = self.fechaInicial;
    } else {
        vc.title = @"Fecha Final";
        self.datePicker.tag = 1;
        self.datePicker.date = self.fechaFinal;
    }
    
    if(self.popOver)
        [self.popOver dismissPopoverAnimated:YES];
    
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
    //TODO ajustar este popOver en iOS 6
    nc.navigationBar.backgroundColor = [UIColor verdeAzulado];
    nc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:nc];
    
    
    [self.popOver setPopoverContentSize:vc.view.frame.size animated:NO];
    [self.popOver presentPopoverFromRect:button.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

- (void) cambioPickerDate:(UIDatePicker *) picker {
    NSDate* fecha = picker.date;
    switch (picker.tag) {
        case 0:
            if([fecha compare:self.fechaFinal]==NSOrderedAscending)
                self.fechaInicial = fecha;
            break;
        case 1:
            if([self.fechaInicial compare:fecha]==NSOrderedAscending)
                self.fechaFinal = fecha;
            break;
    }
    
    [self actualizaBotonesFecha];

    [self actualizaModelo];
}

- (void) actualizaBotonesFecha {
    if(self.fechaInicial && self.fechaFinal) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [self.btnInicio setTitle:[formatter stringFromDate:self.fechaInicial] forState:UIControlStateNormal];
        [self.btnFinal setTitle:[formatter stringFromDate:self.fechaFinal] forState:UIControlStateNormal];
    }
}

- (void) setModelo:(PacientesTerapeutaModel *)modelo {
    _modelo = modelo;

    if(self.modelo.todosLosDias.count) {
        DiaEntrenamiento* primerDia = [self.modelo.todosLosDias firstObject];
        DiaEntrenamiento* ultimoDia = [self.modelo.todosLosDias lastObject];
        //TODO hacer comprobaciones
        
        NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:primerDia.fecha];
        self.fechaInicial = [[NSCalendar currentCalendar] dateFromComponents:comps];
        
        comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:ultimoDia.fecha];
        self.fechaFinal = [[NSCalendar currentCalendar] dateFromComponents:comps];
        
        [self actualizaBotonesFecha];
        
        [self actualizaModelo];
    }
}

- (void)setTipo:(TipoEstadisticas)tipo {
    _tipo = tipo;
    [self actualizaModelo];
}

- (void) actualizaModelo {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *oneDay = [[NSDateComponents alloc] init];
    [oneDay setDay: 1];

    NSArray* diasEntrenados = self.modelo.todosLosDias;

    NSMutableArray* auxGruposEdad = [[NSMutableArray alloc] init];
    for (int g = 0; g<=5; g++) {
        [auxGruposEdad addObject:[[NSMutableArray alloc] init]];
    }
    
    NSMutableArray* auxSexos = [[NSMutableArray alloc] init];
    [auxSexos addObject:[[NSMutableArray alloc] init]];
    [auxSexos addObject:[[NSMutableArray alloc] init]];
    
    NSArray* perfiles = @[@"memoriaOperativa", @"atencionSostenida", @"atencionSelectiva", @"atencionDividida", @"velocidadDeProcesamiento", @"tomaDeDecisiones", @"controlInhibitorio", @"flexibilidadCognitiva"];
    NSMutableArray* auxPerfiles = [[NSMutableArray alloc] init];
    for(int p = 0; p<perfiles.count; p++) {
        [auxPerfiles addObject:[[NSMutableArray alloc] init]];
    }

    NSMutableArray* auxEtiquetas = [[NSMutableArray alloc] init];
    for (NSDate* date = self.fechaInicial; [DateUtils comparaSinHoraFecha:date conFecha:self.fechaFinal] <= 0;
         date = [calendar dateByAddingComponents: oneDay
                                          toDate: date
                                         options: 0] ) {
             
             NSDate* diaSiguiente = [calendar dateByAddingComponents:oneDay toDate:date options:0];

             NSPredicate* predicate = [NSPredicate predicateWithFormat:@"fecha >= %@ AND fecha < %@", date, diaSiguiente];
             NSArray* filtered = [diasEntrenados filteredArrayUsingPredicate:predicate];
             
             //EDADES
             for (int g = 0; g<=5; g++) {
                 NSPredicate* predicate;
                 if(g<5) {
                     predicate = [NSPredicate predicateWithFormat:@"entrenamiento.paciente.datos.edad.intValue >= %d AND entrenamiento.paciente.datos.edad.intValue < %d", g*3, (g+1)*3];
                 } else {
                     predicate = [NSPredicate predicateWithFormat:@"entrenamiento.paciente.datos.edad.intValue >= %d", g*3];
                 }
                 NSArray* filtradoPorEdad = [filtered filteredArrayUsingPredicate:predicate];
                 if(filtradoPorEdad.count) {
                     NSArray* arrayGrupo = [auxGruposEdad objectAtIndex:g];
                     NSNumber* mediaAnterior = [NSNumber numberWithFloat:0.0];
                     if(arrayGrupo.count && [arrayGrupo lastObject]!=(id)[NSNull null])
                         mediaAnterior = [arrayGrupo lastObject];
                     
                     NSNumber* media;
                     switch (self.tipo) {
                         case TipoEstadisticasGenerales: {
                             NSNumber* mediaGongs = [filtradoPorEdad valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                             NSNumber* mediaInvasion = [filtradoPorEdad valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                             NSNumber* mediaPoker = [filtradoPorEdad valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaInvasion.floatValue+mediaPoker.floatValue+mediaAnterior.floatValue];

                             break;
                         }
                         case TipoEstadisticasGongs: {
                             NSNumber* mediaGongs = [filtradoPorEdad valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                             media = [NSNumber numberWithInt:mediaGongs.floatValue+mediaAnterior.floatValue];
                             break;
                         }
                         case TipoEstadisticasInvasion: {
                             NSNumber* mediaInvasion = [filtradoPorEdad valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaInvasion.floatValue+mediaAnterior.floatValue];
                             break;
                         }
                         case TipoEstadisticasPoker: {
                             NSNumber* mediaPoker = [filtradoPorEdad valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaPoker.floatValue+mediaAnterior.floatValue];
                             break;
                         }
                     }
                     [[auxGruposEdad objectAtIndex:g] addObject:media];
                 } else {
                     [[auxGruposEdad objectAtIndex:g] addObject:[NSNull null]];
                 }
             }
             
             //SEXOS
             NSPredicate* predicateMujeres = [NSPredicate predicateWithFormat:@"entrenamiento.paciente.datos.sexo.intValue == 0"];
             NSArray* mujeresDelDia = [filtered filteredArrayUsingPredicate:predicateMujeres];
             if(mujeresDelDia.count) {
                 NSArray* arrayMujeres = [auxSexos objectAtIndex:0];
                 NSNumber* mediaAnterior = [NSNumber numberWithFloat:0.0];
                 if(arrayMujeres.count && [arrayMujeres lastObject]!=(id)[NSNull null])
                     mediaAnterior = [arrayMujeres lastObject];
                 
                 NSNumber* media;
                 switch (self.tipo) {
                     case TipoEstadisticasGenerales: {
                         NSNumber* mediaGongs = [mujeresDelDia valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                         NSNumber* mediaInvasion = [mujeresDelDia valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                         NSNumber* mediaPoker = [mujeresDelDia valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaInvasion.floatValue+mediaPoker.floatValue+mediaAnterior.floatValue];
                         
                         break;
                     }
                     case TipoEstadisticasGongs: {
                         NSNumber* mediaGongs = [mujeresDelDia valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaAnterior.floatValue];
                         break;
                     }
                     case TipoEstadisticasInvasion: {
                         NSNumber* mediaInvasion = [mujeresDelDia valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaInvasion.floatValue+mediaAnterior.floatValue];
                         break;
                     }
                     case TipoEstadisticasPoker: {
                         NSNumber* mediaPoker = [mujeresDelDia valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaPoker.floatValue+mediaAnterior.floatValue];
                         break;
                     }
                 }
                 
                 [[auxSexos objectAtIndex:0] addObject:media];
             } else {
                 [[auxSexos objectAtIndex:0] addObject:[NSNull null]];
             }
             NSPredicate* predicateHombres = [NSPredicate predicateWithFormat:@"entrenamiento.paciente.datos.sexo.intValue == 1"];
             NSArray* hombresDelDia = [filtered filteredArrayUsingPredicate:predicateHombres];
             if(hombresDelDia.count) {
                 NSArray* arrayHombres = [auxSexos objectAtIndex:1];
                 NSNumber* mediaAnterior = [NSNumber numberWithFloat:0.0];
                 if(arrayHombres.count && [arrayHombres lastObject]!=(id)[NSNull null])
                     mediaAnterior = [arrayHombres lastObject];
                 
                 
                 NSNumber* media;
                 switch (self.tipo) {
                     case TipoEstadisticasGenerales: {
                         NSNumber* mediaGongs = [hombresDelDia valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                         NSNumber* mediaInvasion = [hombresDelDia valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                         NSNumber* mediaPoker = [hombresDelDia valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaInvasion.floatValue+mediaPoker.floatValue+mediaAnterior.floatValue];
                         
                         break;
                     }
                     case TipoEstadisticasGongs: {
                         NSNumber* mediaGongs = [hombresDelDia valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaAnterior.floatValue];
                         break;
                     }
                     case TipoEstadisticasInvasion: {
                         NSNumber* mediaInvasion = [hombresDelDia valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaInvasion.floatValue+mediaAnterior.floatValue];
                         break;
                     }
                     case TipoEstadisticasPoker: {
                         NSNumber* mediaPoker = [hombresDelDia valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                         media = [NSNumber numberWithFloat:mediaPoker.floatValue+mediaAnterior.floatValue];
                         break;
                     }
                 }

                 [[auxSexos objectAtIndex:1] addObject:media];
             } else {
                 [[auxSexos objectAtIndex:1] addObject:[NSNull null]];
             }
             
             //PERFILES
             int p = 0;
             for (NSString* perfil in perfiles) {
                 NSPredicate* predicate = [NSPredicate predicateWithFormat:@"entrenamiento.paciente.perfilcognitivo.%@.intValue > 0", perfil];
                 
                 NSArray* filtradoPorPerfil = [filtered filteredArrayUsingPredicate:predicate];
                 if(filtradoPorPerfil.count) {
                     NSArray* arrayPerfil = [auxPerfiles objectAtIndex:p];
                     NSNumber* mediaAnterior = [NSNumber numberWithFloat:0.0];
                     if(arrayPerfil.count && [arrayPerfil lastObject]!=(id)[NSNull null])
                         mediaAnterior = [arrayPerfil lastObject];
                     
                     NSNumber* media;
                     switch (self.tipo) {
                         case TipoEstadisticasGenerales: {
                             NSNumber* mediaGongs = [filtradoPorPerfil valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                             NSNumber* mediaInvasion = [filtradoPorPerfil valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                             NSNumber* mediaPoker = [hombresDelDia valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaInvasion.floatValue+mediaPoker.floatValue+mediaAnterior.floatValue];
                             
                             break;
                         }
                         case TipoEstadisticasGongs: {
                             NSNumber* mediaGongs = [filtradoPorPerfil valueForKeyPath:@"@avg.partidaGongs.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaGongs.floatValue+mediaAnterior.floatValue];
                             break;
                         }
                         case TipoEstadisticasInvasion: {
                             NSNumber* mediaInvasion = [filtradoPorPerfil valueForKeyPath:@"@avg.partidaInvasion.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaInvasion.floatValue+mediaAnterior.floatValue];
                             break;
                         }
                         case TipoEstadisticasPoker: {
                             NSNumber* mediaPoker = [filtradoPorPerfil valueForKeyPath:@"@avg.partidaPoker.cambioNivel"];
                             media = [NSNumber numberWithFloat:mediaPoker.floatValue+mediaAnterior.floatValue];
                             break;
                         }
                     }

                     [[auxPerfiles objectAtIndex:p] addObject:media];
                 } else {
                     [[auxPerfiles objectAtIndex:p] addObject:[NSNull null]];
                 }
                 p+=1;
             }
             
             NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
             [formatter setDateFormat:@"d'/'M"];
             [auxEtiquetas addObject:[formatter stringFromDate:date]];
         }
    
    self.etiquetas = auxEtiquetas.copy;
    self.nivelesPorEdad = auxGruposEdad.copy;
    self.nivelesPorSexo = auxSexos.copy;
    self.nivelesPorPerfil = auxPerfiles.copy;
    
    [self.graficaEdades dibuja];
    [self.graficaSexos dibuja];
    [self.graficaPerfiles dibuja];
}

#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    if(grafica==self.graficaEdades.grafica) {
        return self.nivelesPorEdad.count;
    }
    if(grafica==self.graficaSexos.grafica) {
        return self.nivelesPorSexo.count;
    }
    if(grafica==self.graficaPerfiles.grafica) {
        return self.nivelesPorPerfil.count;
    }
    
    return 0;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(grafica==self.graficaEdades.grafica) {
        return [self.nivelesPorEdad objectAtIndex:numSet];
    }
    if(grafica==self.graficaSexos.grafica) {
        return [self.nivelesPorSexo objectAtIndex:numSet];
    }
    if(grafica==self.graficaPerfiles.grafica) {
        return [self.nivelesPorPerfil objectAtIndex:numSet];
    }
    
    return nil;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.etiquetas;
}

@end
