//
//  PanelGrande.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PacientesTerapeutaModel.h"
#import "GraficaLineas.h"
#import "MenuSeccionEstadisticas.h"

@interface PanelGrande : UIView <GraficaLineasDatasource>
@property (nonatomic, strong) PacientesTerapeutaModel* modelo;
@property (nonatomic, assign) TipoEstadisticas tipo;
@end
