//
//  TerapeutaViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Terapeuta.h"
#import "GranSeccionDelegate.h"
#import "PacientesTerapeutaDatasource.h"
#import "HomeTerapeutaViewController.h"

@interface TerapeutaViewController : UIViewController <UIAlertViewDelegate, HomeTerapeutaDelegate>
@property (nonatomic, strong) Terapeuta* terapeuta;
@property (nonatomic, weak) id<GranSeccionDelegate> delegate;
@end
