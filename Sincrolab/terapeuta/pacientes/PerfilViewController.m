//
//  PerfilCognitivoViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PerfilViewController.h"
#import "ProcesoView.h"
#import "SubprocesoView.h"
#import "EntrenamientosManager.h"

#import "PacientesManager.h"

@interface PerfilViewController ()
@property (nonatomic, strong) ProcesoView* memoria;
@property (nonatomic, strong) ProcesoView* atencion;
@property (nonatomic, strong) ProcesoView* funciones;
@property (nonatomic, strong) UIView* botonView;

@property (nonatomic, strong) UIButton* botonGuardar;

@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIView* contentView;

@property (nonatomic, strong) NSArray* camposArray;

@end

@implementation PerfilViewController {
    BOOL _cambios;
}
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    UIView* fondoTitulo = [[UIView alloc] initWithFrame:CGRectMake(20, 10, 607, 45)];
    fondoTitulo.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:fondoTitulo];
    
    UILabel* tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(203, 13, 203, 21)];
    tituloLabel.font = [UIFont fontWithName:@"Lato-Bold" size:14.0];
    tituloLabel.textColor = [UIColor blackColor];
    tituloLabel.text = @"PROCESOS COGNITIVOS";
    [fondoTitulo addSubview:tituloLabel];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, self.view.bounds.size.width, self.view.bounds.size.height-64)];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view insertSubview:self.scrollView atIndex:0];
    
    self.contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.scrollView addSubview:self.contentView];
    
    self.memoria = [[ProcesoView alloc] initWithFrame:CGRectMake(20, 2, 605, 98+42*1)];
    self.memoria.titulos_subprocesos = [NSArray arrayWithObject:@"Memoria operativa"];
    self.memoria.titulo = @"MEMORIA";
    [self.contentView addSubview:self.memoria];
    
    self.atencion = [[ProcesoView alloc] initWithFrame:CGRectMake(20, self.memoria.frame.origin.y+self.memoria.frame.size.height+6, 605, 98+42*5)];
    self.atencion.titulos_subprocesos = [NSArray arrayWithObjects:@"Atención sostenida.", @"Atención selectiva.", @"Atención dividida.", @"Velocidad de procesamiento.", nil];
    self.atencion.titulo = @"ATENCIÓN";
    [self.contentView addSubview:self.atencion];
    
    self.funciones = [[ProcesoView alloc] initWithFrame:CGRectMake(20, self.atencion.frame.origin.y+self.atencion.frame.size.height+6, 605, 98+42*3)];
    self.funciones.titulos_subprocesos = [NSArray arrayWithObjects:@"Toma de decisiones.", @"Control inhibitorio.", @"Flexibilidad cognitiva.", nil];
    self.funciones.titulo = @"FUNCTIONES EJECUTIVAS FRIAS";
    [self.contentView addSubview:self.funciones];
    
    self.botonView = [[UIView alloc] initWithFrame:CGRectMake(20, self.funciones.frame.origin.y+self.funciones.frame.size.height+6, 605, 70)];
    self.botonView.backgroundColor = [UIColor whiteColor];
    self.botonGuardar = [[UIButton alloc] initWithFrame:CGRectMake(405, 10, 188, 49)];
    [self.botonGuardar setImage:[UIImage imageNamed:@"btnguardar"] forState:UIControlStateNormal];
    [self.botonGuardar addTarget:self action:@selector(guardaDatos) forControlEvents:UIControlEventTouchUpInside];
    [self.botonView addSubview:self.botonGuardar];
    [self.contentView addSubview:self.botonView];
    
    self.contentView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.botonView.frame.origin.y + self.botonView.frame.size.height + 12);
    
    
    NSArray *nestedValues = @[self.memoria.subprocesos.allValues, self.atencion.subprocesos.allValues, self.funciones.subprocesos.allValues];
    NSArray *flattenedValues = [nestedValues valueForKeyPath:@"@unionOfArrays.self"];
    
    
    for (SubprocesoView* subproceso in flattenedValues) {
        subproceso.delegate = self;
    }
    
    
    if(self.modelo.paciente)
        [self configuraDatos];

}

- (BOOL)hayCambios {
    return _cambios;
}

- (void)haCambiado:(SubprocesoView *)subproceso {
    _cambios = YES;
    
    
    PerfilCognitivoPaciente* perfilCognitivo = [PerfilCognitivoPaciente object];
    
    NSMutableDictionary* campos = [[NSMutableDictionary alloc] init];
    [campos setValue:((SubprocesoView *)[self.memoria.subprocesos objectForKey:@"MemoriaOperativa"]).valor forKey:@"memoriaOperativa"];
    
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónSostenida"]).valor forKey:@"atencionSostenida"];
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónSelectiva"]).valor forKey:@"atencionSelectiva"];
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónDividida"]).valor forKey:@"atencionDividida"];
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"VelocidadDeProcesamiento"]).valor forKey:@"velocidadDeProcesamiento"];
    
    [campos setValue:((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"TomaDeDecisiones"]).valor forKey:@"tomaDeDecisiones"];
    [campos setValue:((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"ControlInhibitorio"]).valor forKey:@"controlInhibitorio"];
    [campos setValue:((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"FlexibilidadCognitiva"]).valor forKey:@"flexibilidadCognitiva"];
   
    for (NSString* ruta in campos) {
        [perfilCognitivo setValue:[campos valueForKey:ruta] forKeyPath:ruta];
    }
    
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.contentView.frame.size.height)];
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    [self configuraDatos];
}

- (void) configuraDatos {
    
    ((SubprocesoView *)[self.memoria.subprocesos objectForKey:@"MemoriaOperativa"]).valor = self.modelo.paciente.perfilcognitivo.memoriaOperativa;
    
    ((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónSostenida"]).valor = self.modelo.paciente.perfilcognitivo.atencionSostenida;
    ((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónSelectiva"]).valor = self.modelo.paciente.perfilcognitivo.atencionSelectiva;
    ((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónDividida"]).valor = self.modelo.paciente.perfilcognitivo.atencionDividida;
    ((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"VelocidadDeProcesamiento"]).valor = self.modelo.paciente.perfilcognitivo.velocidadDeProcesamiento;
    
    ((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"TomaDeDecisiones"]).valor = self.modelo.paciente.perfilcognitivo.tomaDeDecisiones;
    ((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"ControlInhibitorio"]).valor = self.modelo.paciente.perfilcognitivo.controlInhibitorio;
    ((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"FlexibilidadCognitiva"]).valor = self.modelo.paciente.perfilcognitivo.flexibilidadCognitiva;
    
    _cambios = NO;
    
    [self.scrollView setContentOffset:CGPointZero animated:NO];    
    
}

- (void) guardaDatos {
    if(!self.modelo.paciente.perfilcognitivo) {
        PerfilCognitivoPaciente* perfilCognitivo = [PerfilCognitivoPaciente object];
        self.modelo.paciente.perfilcognitivo = perfilCognitivo;
    }
    
    NSMutableDictionary* campos = [[NSMutableDictionary alloc] init];
    [campos setValue:((SubprocesoView *)[self.memoria.subprocesos objectForKey:@"MemoriaOperativa"]).valor forKey:@"perfilcognitivo.memoriaOperativa"];

    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónSostenida"]).valor forKey:@"perfilcognitivo.atencionSostenida"];
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónSelectiva"]).valor forKey:@"perfilcognitivo.atencionSelectiva"];
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"AtenciónDividida"]).valor forKey:@"perfilcognitivo.atencionDividida"];
    [campos setValue:((SubprocesoView *)[self.atencion.subprocesos objectForKey:@"VelocidadDeProcesamiento"]).valor forKey:@"perfilcognitivo.velocidadDeProcesamiento"];

    [campos setValue:((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"TomaDeDecisiones"]).valor forKey:@"perfilcognitivo.tomaDeDecisiones"];
    [campos setValue:((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"ControlInhibitorio"]).valor forKey:@"perfilcognitivo.controlInhibitorio"];
    [campos setValue:((SubprocesoView *)[self.funciones.subprocesos objectForKey:@"FlexibilidadCognitiva"]).valor forKey:@"perfilcognitivo.flexibilidadCognitiva"];

    
    [PacientesManager actualizaPaciente:self.modelo.paciente conCampos:campos completado:^(NSError * error) {
        if(error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            _cambios = NO;
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
