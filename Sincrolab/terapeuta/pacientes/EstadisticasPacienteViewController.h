//
//  EstadisticasPacienteViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuEstadisticasView.h"
#import "EntrenamientoPacienteModel.h"

@interface EstadisticasPacienteViewController : UIViewController <MenuEstadisticasDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
