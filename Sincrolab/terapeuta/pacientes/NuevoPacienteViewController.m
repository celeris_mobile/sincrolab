//
//  NuevoPacienteViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "NuevoPacienteViewController.h"
#import "Paciente.h"
#import "DatosPaciente.h"
#import "RSForm.h"
#import "UIColor+Extensions.h"

@interface NuevoPacienteViewController ()
@property (nonatomic, strong) RSForm* formulario;
@property (nonatomic, strong) UIButton* botonGuardar;
@property (nonatomic, strong) UIButton* botonCancelar;
@property (nonatomic, strong) RSCampo* campoEmail;
@end

@implementation NuevoPacienteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView* fondoTitulo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 428, 41)];
    fondoTitulo.backgroundColor = [UIColor azulOscuro];
    [self.view addSubview:fondoTitulo];
    
    UILabel* labelTitulo = [[UILabel alloc] initWithFrame:CGRectMake(17, 10, 200, 20)];
    labelTitulo.textColor = [UIColor whiteColor];
    labelTitulo.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    labelTitulo.text = @"NUEVO PACIENTE";
    [fondoTitulo addSubview:labelTitulo];
    
    
    self.formulario = [[RSForm alloc] init];
    self.formulario.delegate = self;
    self.formulario.colorTitulos = [UIColor verdeAzulado];
    
    RSCampo* campoPassword = [RSCampo campoConNombre:@"password" Tipo:TipoCampoPassword];
    campoPassword.caption = @"Contraseña";
    
    self.campoEmail = [RSCampo campoConNombre:@"email" Tipo:TipoCampoEmail];
    
    NSArray* campos = @[
                        @[
                            @[[RSCampo campoConNombre:@"nombre"], [RSCampo campoConNombre:@"datos.fechaNacimiento" Tipo:TipoCampoComboFecha]],
                            @[[RSCampo campoConNombre:@"apellidos"]],
                            @[[RSCampo campoConNombre:@"datos.curso"], [RSCampo campoConNombre:@"datos.repetidor" Tipo:TipoCampoSwitch]],
                            @[self.campoEmail],
                            @[campoPassword]
                            ]
                        ];
    
    
    [self.formulario configuraCampos:campos];
    
    [self.view addSubview:self.formulario];
    
    
    
    UIImage* imgGuardar = [UIImage imageNamed:@"btnguardar_entrenamiento.png"];
    self.botonGuardar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgGuardar.size.width, imgGuardar.size.height)];
    [self.botonGuardar setImage:imgGuardar forState:UIControlStateNormal];
    [self.botonGuardar addTarget:self action:@selector(guardarDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.botonGuardar];
    
    UIImage* imgCancelar = [UIImage imageNamed:@"btncancelar_entrenamient.png"];
    self.botonCancelar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgCancelar.size.width, imgCancelar.size.height)];
    [self.botonCancelar setImage:imgCancelar forState:UIControlStateNormal];
    [self.botonCancelar addTarget:self action:@selector(cancelarDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.botonCancelar];
    
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, 428, 342);
    
    CGRect espacioForm = self.view.bounds;
    espacioForm.origin.y += 41;
    espacioForm.size.height -= 41;
    
    self.formulario.frame = CGRectInset(espacioForm, 20, 20);
    
    CGRect guardarFrame = self.botonGuardar.frame;
    guardarFrame.origin.x =  self.formulario.frame.origin.x+self.formulario.frame.size.width-self.botonGuardar.frame.size.width;
    guardarFrame.origin.y = self.formulario.frame.origin.y+self.formulario.frame.size.height-self.botonGuardar.frame.size.height;
    self.botonGuardar.frame = guardarFrame;
    
    CGRect cancelarFrame = self.botonCancelar.frame;
    cancelarFrame.origin.x =  self.botonGuardar.frame.origin.x-self.botonCancelar.frame.size.width-20;
    cancelarFrame.origin.y = self.formulario.frame.origin.y+self.formulario.frame.size.height-self.botonCancelar.frame.size.height;
    self.botonCancelar.frame = cancelarFrame;
}

- (void) cancelarDado:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void) guardarDado:(UIButton *) sender {
    
    BOOL incompleto = NO;
    for (RSCampoView* campoView in self.formulario.listaDeCampos) {
        if(campoView.data.vacio)
            incompleto = YES;
    }
    if (incompleto) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Debe rellenar todos los campos" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        return;
    }
    
    if(![self isValidEmail:(NSString *)self.campoEmail.valor]) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"El email no es válido" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        return;
    }
    

    NSMutableDictionary* campos = [[NSMutableDictionary alloc] init];
    
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        if(campo.data.valor) {
            [campos setValue:campo.data.valor forKey:campo.data.nombre];
        }
    }
    
    [self.delegate creaPaciente:campos.copy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark RSFormDelegate
- (UIFont *)tipoParaCampo:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:12.5];
}

- (UIColor *)colorTextoCampo:(RSCampo *)campo {
    return [UIColor darkGrayColor];
}

- (UIFont *)tipoParaHint:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:9.0];
}

- (UIColor *)colorTextoHint:(RSCampo *)campo {
    return [UIColor verdeAzulado];
}


@end

