//
//  DatosViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeccionDatosPaciente.h"
#import "EntrenamientoPacienteModel.h"

@interface DatosViewController : UIViewController <SeccionDatosPaciente, UIAlertViewDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
