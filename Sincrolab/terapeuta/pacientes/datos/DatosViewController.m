//
//  DatosViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DatosViewController.h"
#import "UIColor+Extensions.h"
#import "DatosViewController.h"
#import "PersonalesViewController.h"
#import "HistoriaViewController.h"
#import "DiagnosticoViewController.h"

#import "EntrenamientoPacienteModel.h"

@interface DatosViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnPersonales;
@property (weak, nonatomic) IBOutlet UIButton *btnHistoria;
@property (weak, nonatomic) IBOutlet UIButton *btnDiagnostico;

@property (nonatomic, strong) UIViewController* currentViewController;
@property (nonatomic, weak) UIButton* botonProvisional;
@property (nonatomic, weak) UIButton* botonSeleccionado;
@property (weak, nonatomic) IBOutlet UIView *placeHolderView;
@end

@implementation DatosViewController
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIFont *fontNormal = [UIFont fontWithName:@"Lato-Regular" size:11.0];
    NSDictionary *atributosNormal = [NSDictionary dictionaryWithObjectsAndKeys:fontNormal, NSFontAttributeName, [UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    
    
    UIFont *fontBold = [UIFont fontWithName:@"Lato-Bold" size:11.0];
    NSDictionary *atributosBold = [NSDictionary dictionaryWithObjectsAndKeys:fontBold, NSFontAttributeName, [UIColor azulOscuro], NSForegroundColorAttributeName, nil];
    
    NSArray* botones = @[self.btnPersonales, self.btnHistoria, self.btnDiagnostico];
    for (int i=0; i<botones.count;i++) {
        UIButton *button = (UIButton *)[botones objectAtIndex:i];
        NSAttributedString* tituloNormal = [[NSAttributedString alloc] initWithString:button.titleLabel.text attributes:atributosNormal];
        [button setAttributedTitle:tituloNormal forState:UIControlStateNormal];
        
        NSAttributedString* tituloBold = [[NSAttributedString alloc] initWithString:button.titleLabel.text attributes:atributosBold];
        [button setAttributedTitle:tituloBold forState:UIControlStateSelected];
    }
    
    
    [self muestraSeccion:self.btnPersonales];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    
    if([self.currentViewController respondsToSelector:NSSelectorFromString(@"modelo")]) {
        [self.currentViewController setValue:self.modelo forKey:@"modelo"];
    }
}

- (IBAction)botonDado:(UIButton *)boton {
    if([self.currentViewController conformsToProtocol:@protocol(SeccionDatosPaciente)]&&
       ([((id<SeccionDatosPaciente>)self.currentViewController) hayCambios])) {
        
        self.botonProvisional = boton;
        
        [[[UIAlertView alloc] initWithTitle:@"Cambios sin guardar"
                                    message:@"Si continua se perderán todos los cambios que no haya guardado"
                                   delegate:self
                          cancelButtonTitle:@"Cancelar"
                          otherButtonTitles:@"Descartar", nil] show];
    } else {
        [self muestraSeccion:boton];
    }
}

- (void) muestraSeccion:(UIButton *) boton {
    
    UIViewController* vc;
    if([boton isEqual:self.btnPersonales]) {
        vc = [[PersonalesViewController alloc] init];
    }
    
    if([boton isEqual:self.btnHistoria]) {
        vc = [[HistoriaViewController alloc] init];
    }
    
    if([boton isEqual:self.btnDiagnostico]) {
        vc = [[DiagnosticoViewController alloc] init];
    }
    
    if(self.currentViewController) {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
    }
    
    self.currentViewController = vc;
    
    [self addChildViewController:self.currentViewController];
    self.currentViewController.view.frame = self.placeHolderView.bounds;
    [self.placeHolderView addSubview:self.currentViewController.view];
    
    
    if(self.botonSeleccionado)
        self.botonSeleccionado.selected = NO;
    
    self.botonSeleccionado = boton;
    self.botonSeleccionado.selected = YES;
    
    if(self.modelo) {
        if([self.currentViewController respondsToSelector:NSSelectorFromString(@"modelo")]) {
            [self.currentViewController setValue:self.modelo forKey:@"modelo"];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex==alertView.cancelButtonIndex)
        self.botonProvisional = nil;
    else
        [self muestraSeccion:self.botonProvisional];
}

- (BOOL)hayCambios {
    return [((id<SeccionDatosPaciente>)self.currentViewController) hayCambios];;
}

@end
