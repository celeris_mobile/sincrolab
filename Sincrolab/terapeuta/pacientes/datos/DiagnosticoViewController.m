//
//  HistoriaViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DiagnosticoViewController.h"
#import "DiagnosticoPaciente.h"
#import "RSForm.h"
#import "UIColor+Extensions.h"
#import "PacientesManager.h"

@interface DiagnosticoViewController ()
@property (nonatomic, strong) UIPopoverController* popOver;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) RSForm* formulario;

@property (nonatomic, strong) UIButton* botonGuardar;
@end

@implementation DiagnosticoViewController {
    BOOL _cambios;
}
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectInset(self.scrollView.bounds, 20, 20)];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.contentView];
    
    UIImage* imgGuardar = [UIImage imageNamed:@"btnguardar.png"];
    self.botonGuardar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgGuardar.size.width, imgGuardar.size.height)];
    [self.botonGuardar setImage:imgGuardar forState:UIControlStateNormal];
    [self.botonGuardar addTarget:self action:@selector(guardarDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.botonGuardar];
    
    
    self.formulario = [[RSForm alloc] init];
    self.formulario.colorTitulos = [UIColor verdeAzulado];
    self.formulario.delegate = self;
    
    
    RSCampo* campoPatologiaMedica = [RSCampo campoConNombre:@"diagnostico.patologiaMedica" Tipo:TipoCampoSwitch Ancho:180];
    campoPatologiaMedica.caption = @"Patología médica";
    RSCampo* campoDescripcionPatologia = [RSCampo campoConNombre:@"diagnostico.descripcionPatologiaMedica"];
    campoDescripcionPatologia.caption = @"Descripción";
    
    RSCampo* campoClasificacionNeurofisiologica = [RSCampo campoConNombre:@"diagnostico.clasificacionNeurofisiologica" Tipo:TipoCampoSwitch Ancho:180];
    campoClasificacionNeurofisiologica.caption = @"Clasificación neurofisiológica";
    RSCampo* campoDescripcionClasificacion = [RSCampo campoConNombre:@"diagnostico.descripcionClasificacionNeurofisiologica"];
    campoDescripcionClasificacion.caption = @"Descripción";
    
    RSCampo* campoFactoresGeneticos = [RSCampo campoConNombre:@"diagnostico.factoresGeneticos" Tipo:TipoCampoSwitch Ancho:180];
    campoFactoresGeneticos.caption = @"Factores genéticos";
    RSCampo* campoDescripcionGeneticos = [RSCampo campoConNombre:@"diagnostico.descripcionFactoresGeneticos"];
    campoDescripcionGeneticos.caption = @"Descripción";
    
    RSCampo* campoFactoresRiesgo = [RSCampo campoConNombre:@"diagnostico.factoresDeRiesgo" Tipo:TipoCampoSwitch Ancho:180];
    campoFactoresRiesgo.caption = @"Factores de riesgo";
    RSCampo* campoDescripcionRiesgo = [RSCampo campoConNombre:@"diagnostico.descripcionFactoresDeRiesgo"];
    campoDescripcionRiesgo.caption = @"Descripción";
    
    RSCampo* campoTrastornosGeneralizados = [RSCampo campoConNombre:@"diagnostico.trastornosGeneralizados" Tipo:TipoCampoSwitch Ancho:180];
    campoTrastornosGeneralizados.caption = @"Trastornos generalizados del desarrollo";
    RSCampo* campoDescripcionGeneralizados = [RSCampo campoConNombre:@"diagnostico.descripcionTrastornosGeneralizados"];
    campoDescripcionGeneralizados.caption = @"Descripción";
    
    RSCampo* campoTrastornosAtencion = [RSCampo campoConNombre:@"diagnostico.trastornosAtencion" Tipo:TipoCampoSwitch Ancho:180];
    campoTrastornosAtencion.caption = @"Trastornos de la atención";
    RSCampo* campoDescripcionAtencion = [RSCampo campoConNombre:@"diagnostico.descripcionTrastornosAtencion"];
    campoDescripcionAtencion.caption = @"Descripción";
    
    RSCampo* campoTrastornosNeurocognitivos = [RSCampo campoConNombre:@"diagnostico.trastornosNeurocognitivos" Tipo:TipoCampoSwitch Ancho:180];
    campoTrastornosNeurocognitivos.caption = @"Trastornos neurocognitivos";
    RSCampo* campoDescripcionNeurocognitivos = [RSCampo campoConNombre:@"diagnostico.descripcionTrastornosNeurocognitivos"];
    campoDescripcionNeurocognitivos.caption = @"Descripción";
    
    RSCampo* campoTrastornosPerceptivos = [RSCampo campoConNombre:@"diagnostico.trastornosPerceptivos" Tipo:TipoCampoSwitch Ancho:180];
    campoTrastornosPerceptivos.caption = @"Trastornos perceptivos y/o motores";
    RSCampo* campoDescripcionPerceptivos = [RSCampo campoConNombre:@"diagnostico.descripcionTrastornosPerceptivos"];
    campoDescripcionPerceptivos.caption = @"Descripción";
    
    RSCampo* campoRetrasoMental = [RSCampo campoConNombre:@"diagnostico.retrasoMental" Tipo:TipoCampoSwitch Ancho:180];
    campoRetrasoMental.caption = @"Retraso mental";
    RSCampo* campoDescripcionRetraso = [RSCampo campoConNombre:@"diagnostico.descripcionRetrasoMental"];
    campoDescripcionRetraso.caption = @"Descripción";
    
    RSCampo* campoTrastornosLenguaje = [RSCampo campoConNombre:@"diagnostico.trastornosLenguaje" Tipo:TipoCampoSwitch Ancho:180];
    campoTrastornosLenguaje.caption = @"Trastornos del lenguaje";
    RSCampo* campoDescripcionLenguaje = [RSCampo campoConNombre:@"diagnostico.descripcionTrastornosLenguaje"];
    campoDescripcionLenguaje.caption = @"Descripción";
    
    RSCampo* campoDificultadesBasicas = [RSCampo campoConNombre:@"diagnostico.dificultadesBasicas" Tipo:TipoCampoSwitch Ancho:180];
    campoDificultadesBasicas.caption = @"Dificultades en las actividades básicas de la vida diaria";
    RSCampo* campoDescripcionBasicas = [RSCampo campoConNombre:@"diagnostico.descripcionDificultadesBasicas"];
    campoDescripcionBasicas.caption = @"Descripción";
    
    RSCampo* campoDificultadesAprendizaje = [RSCampo campoConNombre:@"diagnostico.dificultadesAprendizaje" Tipo:TipoCampoSwitch Ancho:180];
    campoDificultadesAprendizaje.caption = @"Dificultades de aprendizaje";
    RSCampo* campoDescripcionAprendizaje = [RSCampo campoConNombre:@"diagnostico.descripcionDificultadesAprendizaje"];
    campoDescripcionAprendizaje.caption = @"Descripción";
    
    RSCampo* campoDificultadesSociales = [RSCampo campoConNombre:@"diagnostico.dificultadesSociales" Tipo:TipoCampoSwitch Ancho:180];
    campoDificultadesSociales.caption = @"Dificultades de aprendizaje";
    RSCampo* campoDescripcionSociales = [RSCampo campoConNombre:@"diagnostico.descripcionDificultadesSociales"];
    campoDescripcionSociales.caption = @"Descripción";
    
    RSCampo* campoTrastornosConducta = [RSCampo campoConNombre:@"diagnostico.trastornosConducta" Tipo:TipoCampoSwitch Ancho:180];
    campoTrastornosConducta.caption = @"Dificultades de aprendizaje";
    RSCampo* campoDescripcionConducta = [RSCampo campoConNombre:@"diagnostico.descripcionTrastornosConducta"];
    campoDescripcionConducta.caption = @"Descripción";
    
    
    NSArray* campos = @[
                        @[
                            @[campoPatologiaMedica],
                            @[campoDescripcionPatologia],
                            @[campoClasificacionNeurofisiologica],
                            @[campoDescripcionClasificacion],
                            @[campoFactoresGeneticos],
                            @[campoDescripcionGeneticos],
                            @[campoFactoresRiesgo],
                            @[campoDescripcionRiesgo]
                            ],
                        @[
                            @[campoTrastornosGeneralizados],
                            @[campoDescripcionGeneralizados],
                            @[campoTrastornosAtencion],
                            @[campoDescripcionAtencion],
                            @[campoTrastornosNeurocognitivos],
                            @[campoDescripcionNeurocognitivos],
                            @[campoTrastornosPerceptivos],
                            @[campoDescripcionPerceptivos],
                            @[campoRetrasoMental],
                            @[campoDescripcionRetraso],
                            @[campoTrastornosLenguaje],
                            @[campoDescripcionLenguaje]
                            ],
                        @[
                            @[campoDificultadesBasicas],
                            @[campoDescripcionBasicas],
                            @[campoDificultadesAprendizaje],
                            @[campoDescripcionAprendizaje],
                            @[campoDificultadesSociales],
                            @[campoDescripcionSociales],
                            @[campoTrastornosConducta],
                            @[campoDescripcionConducta]
                            ],
                        @[[RSCampo campoConNombre:@"observaciones" Tipo:TipoCampoTextoLargo]
                          ]
                        ];
    
    [self.formulario configuraCampos:campos];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionPatologiaMedica" aSwitch:@"diagnostico.patologiaMedica"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionClasificacionNeurofisiologica" aSwitch:@"diagnostico.clasificacionNeurofisiologica"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionFactoresGeneticos" aSwitch:@"diagnostico.factoresGeneticos"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionFactoresDeRiesgo" aSwitch:@"diagnostico.factoresDeRiesgo"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionTrastornosGeneralizados" aSwitch:@"diagnostico.trastornosGeneralizados"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionTrastornosAtencion" aSwitch:@"diagnostico.trastornosAtencion"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionTrastornosNeurocognitivos" aSwitch:@"diagnostico.trastornosNeurocognitivos"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionTrastornosPerceptivos" aSwitch:@"diagnostico.trastornosPerceptivos"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionRetrasoMental" aSwitch:@"diagnostico.retrasoMental"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionTrastornosLenguaje" aSwitch:@"diagnostico.trastornosLenguaje"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionDificultadesBasicas" aSwitch:@"diagnostico.dificultadesBasicas"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionDificultadesAprendizaje" aSwitch:@"diagnostico.dificultadesAprendizaje"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionDificultadesSociales" aSwitch:@"diagnostico.dificultadesSociales"];
    [self.formulario agregaCampoEnlazado:@"diagnostico.descripcionTrastornosConducta" aSwitch:@"diagnostico.trastornosConducta"];
    
    [self.contentView addSubview:self.formulario];
    
    if(self.modelo.paciente)
        [self configuraDatos];

    _cambios = NO;
    
    [self registerForKeyboardNotifications];
}

- (void)viewWillLayoutSubviews {
    self.contentView.frame = CGRectInset(self.scrollView.bounds, 20, 20);
    self.formulario.frame = CGRectMake(20, 20, self.contentView.bounds.size.width-40, 1400);
    
    CGRect guardarFrame = self.botonGuardar.frame;
    guardarFrame.origin.x = self.formulario.frame.origin.x+self.formulario.frame.size.width-self.botonGuardar.frame.size.width;
    guardarFrame.origin.y = self.formulario.frame.origin.y + self.formulario.frame.size.height + 20;
    self.botonGuardar.frame = guardarFrame;
    
    
    CGRect contentFrame = self.contentView.frame;
    contentFrame.size.height = self.formulario.frame.size.height + self.botonGuardar.frame.size.height+60;
    self.contentView.frame = contentFrame;
    
    [self.scrollView setContentSize:CGSizeMake(self.contentView.frame.size.width+40, self.contentView.frame.size.height+40)];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)hayCambios {
    return _cambios;
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    [self configuraDatos];
}

- (void) configuraDatos {
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        campo.data.valor = [self.modelo.paciente valueForKeyPath:campo.data.nombre];
    }
    
    [self.scrollView setContentOffset:CGPointZero animated:NO];    
    _cambios = NO;
}


- (void) guardarDado:(id)sender {
    if(!self.modelo.paciente.diagnostico) {
        DiagnosticoPaciente* diagnostico = [DiagnosticoPaciente object];
        self.modelo.paciente.diagnostico = diagnostico;
    }
    
    NSMutableDictionary* campos = [[NSMutableDictionary alloc] init];
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        if(campo.data.valor) {
            [campos setValue:campo.data.valor forKey:campo.data.nombre];
        }
    }
    
    [PacientesManager actualizaPaciente:self.modelo.paciente conCampos:campos completado:^(NSError * error) {
        if(error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            _cambios = NO;
        }
    }];
}

#pragma mark Keyboard
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    UIView* activeField;
    for (RSCampoView *campoView in self.formulario.listaDeCampos) {
        if ([campoView.campoDeTexto isFirstResponder]) {
            activeField = (UIView *)campoView;
        }
    }
    CGRect activeFrame = [self.scrollView convertRect:activeField.frame fromView:activeField.superview];
    
    NSDictionary* info = [aNotification userInfo];
    CGRect kbFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbFrame = [self.view convertRect:kbFrame fromView:nil];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbFrame.size.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= kbFrame.size.height;
    if (!CGRectContainsPoint(aRect, activeFrame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeFrame.origin.y - aRect.size.height/2);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.scrollView setContentInset:contentInsets];
    self.scrollView.scrollIndicatorInsets = contentInsets;
}


#pragma mark RSForm Delegate
- (void)campoHaCambiado:(RSCampo *)campo {
    _cambios = YES;
}

- (UIFont *)tipoParaCampo:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:12.5];
}

- (UIColor *)colorTextoCampo:(RSCampo *)campo {
    return [UIColor darkGrayColor];
}

- (UIFont *)tipoParaHint:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:9.0];
}

- (UIColor *)colorTextoHint:(RSCampo *)campo {
    return [UIColor verdeAzulado];
}

- (UIFont *)tipoParaTituloBloque:(NSInteger)numBloque {
    return [UIFont fontWithName:@"Lato-Regular" size:15.0];
}

- (UIColor *)colorTituloBloque:(NSInteger)numBloque {
    return [UIColor verdeAzulado];
}

- (NSString *) tituloDeBloque:(NSInteger) numBloque {
    switch (numBloque) {
        case 0:
            return @"MARCADORES BIOLÓGICOS";
            break;
        case 1:
            return @"TRASTORNOS DEL NEURODESARROLLO";
            break;
        case 2:
            return @"COMPETENCIAS FUNCIONALES";
            break;
    }
    
    return nil;
}

@end
