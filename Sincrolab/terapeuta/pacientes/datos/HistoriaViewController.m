//
//  HistoriaViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "HistoriaViewController.h"
#import "RSForm.h"
#import "UIColor+Extensions.h"
#import "PacientesManager.h"

@interface HistoriaViewController ()
@property (nonatomic, strong) UIPopoverController* popOver;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) RSForm* formulario;

@property (nonatomic, strong) UIButton* botonGuardar;
@end

@implementation HistoriaViewController {
    BOOL _cambios;
}
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectInset(self.scrollView.bounds, 20, 20)];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.contentView];
    
    UIImage* imgGuardar = [UIImage imageNamed:@"btnguardar.png"];
    self.botonGuardar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgGuardar.size.width, imgGuardar.size.height)];
    [self.botonGuardar setImage:imgGuardar forState:UIControlStateNormal];
    [self.botonGuardar addTarget:self action:@selector(guardarDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.botonGuardar];
    
    
    self.formulario = [[RSForm alloc] init];
    self.formulario.colorTitulos = [UIColor verdeAzulado];
    self.formulario.delegate = self;
    
    RSCampo* campoAntecedentesPadre = [RSCampo campoConNombre:@"cuestionario.antecedentesPadre" Tipo:TipoCampoSwitch];
    campoAntecedentesPadre.caption = @"Padre";
    RSCampo* campoAntecedentesMadre = [RSCampo campoConNombre:@"cuestionario.antecedentesMadre" Tipo:TipoCampoSwitch];
    campoAntecedentesMadre.caption = @"Madre";
    RSCampo* campoDescripcionAntecedentes = [RSCampo campoConNombre:@"cuestionario.descripcionAntecedentes" Tipo:TipoCampoTextoLargo];
    campoDescripcionAntecedentes.caption = @"Descripcion";
    
    
    RSCampo* campoSufrimientoSwitch = [RSCampo campoConNombre:@"cuestionario.sufrimiento" Tipo:TipoCampoSwitch Ancho:210];
    campoSufrimientoSwitch.caption = @"Sufrimiento fetal agudo";
    RSCampo* campoPesoAlNacer = [RSCampo campoConNombre:@"cuestionario.pesoAlNacer" Tipo:TipoCampoNumerico Ancho:200];
    campoPesoAlNacer.caption = @"Peso al nacer (gramos)";
    
    RSCampo* campoControlCefalico = [RSCampo campoConNombre:@"cuestionario.controlCefalico" Tipo:TipoCampoComboString];
    campoControlCefalico.caption = @"Control cefálico (meses)";
    RSCampo* campoGateo = [RSCampo campoConNombre:@"cuestionario.gateo" Tipo:TipoCampoComboString];
    campoGateo.caption = @"Gateo (meses)";
    RSCampo* campoDeambulacion = [RSCampo campoConNombre:@"cuestionario.deambulacion" Tipo:TipoCampoComboString];
    campoDeambulacion.caption = @"Deambulación (meses)";
    
    RSCampo* campoDeglucion = [RSCampo campoConNombre:@"cuestionario.deglucion" Tipo:TipoCampoSwitch Ancho:130];
    campoDeglucion.caption = @"Deglución";
    RSCampo* campoEdadDeglucion = [RSCampo campoConNombre:@"cuestionario.edadDeglucion" Tipo:TipoCampoNumerico Ancho:100];
    campoEdadDeglucion.caption = @"Edad (meses)";
    RSCampo* campoHabitos = [RSCampo campoConNombre:@"cuestionario.habitos" Tipo:TipoCampoRadio Ancho:130];
    campoHabitos.caption = @"Hábitos";
    RSCampo* campoManias = [RSCampo campoConNombre:@"cuestionario.manias" Tipo:TipoCampoSwitch Ancho:130];
    campoManias.caption = @"Manías";
    RSCampo* campoDescripcionManias = [RSCampo campoConNombre:@"cuestionario.descripcionManias" Tipo:TipoCampoTextoLargo];
    campoDescripcionManias.caption = @"Descripción";
    
    
    RSCampo* campoComprension = [RSCampo campoConNombre:@"cuestionario.comprension" Tipo:TipoCampoRadio];
    campoComprension.caption = @"Comprensión";
    RSCampo* campoExpresion = [RSCampo campoConNombre:@"cuestionario.expresion" Tipo:TipoCampoRadio];
    campoExpresion.caption = @"Expresión";
    RSCampo* campoLectura = [RSCampo campoConNombre:@"cuestionario.lectura" Tipo:TipoCampoRadio];
    campoLectura.caption = @"Lectura";
    RSCampo* campoEscritura = [RSCampo campoConNombre:@"cuestionario.escritura" Tipo:TipoCampoRadio];
    campoEscritura.caption = @"Escritura";
    RSCampo* campoComunicacion = [RSCampo campoConNombre:@"cuestionario.comunicacion" Tipo:TipoCampoRadio];
    campoComunicacion.caption = @"Comunicacion";
    RSCampo* campoEdadPrimerasPalabras = [RSCampo campoConNombre:@"cuestionario.edadPrimerasPalabras" Tipo:TipoCampoNumerico Ancho:100];
    campoEdadPrimerasPalabras.caption = @"Edad (meses)";
    RSCampo* campoEdadPrimerasFrases = [RSCampo campoConNombre:@"cuestionario.edadPrimerasFrases" Tipo:TipoCampoNumerico Ancho:100];
    campoEdadPrimerasFrases.caption = @"Edad (meses)";
    
    NSArray* campos = @[
                        @[
                            @[campoAntecedentesPadre, campoAntecedentesMadre],
                            @[campoDescripcionAntecedentes]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"cuestionario.medicacion" Tipo:TipoCampoSwitch]],
                            @[[RSCampo campoConNombre:@"cuestionario.tipoDeMedicacion" Tipo:TipoCampoTextoLargo]]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"cuestionario.tipoDeParto" Tipo:TipoCampoComboString], [RSCampo campoConNombre:@"cuestionario.apgar" Tipo:TipoCampoComboNumber], [RSCampo campoConNombre:@"cuestionario.apgar2" Tipo:TipoCampoComboNumber]],
                            @[campoSufrimientoSwitch],
                            @[[RSCampo campoConNombre:@"cuestionario.reanimacion" Tipo:TipoCampoSwitch]],
                            @[campoPesoAlNacer]
                            ],
                        @[
                            @[campoControlCefalico, campoGateo, campoDeambulacion]
                            ],
                        @[
                            @[campoDeglucion, campoEdadDeglucion],
                            @[campoHabitos],
                            @[[RSCampo campoConNombre:@"cuestionario.praxias" Tipo:TipoCampoRadio Ancho:130]],
                            @[campoManias],
                            @[campoDescripcionManias]
                            ],
                        @[
                            @[campoComprension],
                            @[campoExpresion],
                            @[campoLectura],
                            @[campoEscritura],
                            @[campoComunicacion],
                            @[[RSCampo campoConNombre:@"cuestionario.primerasPalabras" Tipo:TipoCampoSwitch], campoEdadPrimerasPalabras],
                            @[[RSCampo campoConNombre:@"cuestionario.primerasFrases" Tipo:TipoCampoSwitch], campoEdadPrimerasFrases]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"cuestionario.relacionesPares" Tipo:TipoCampoRadio]],
                            @[[RSCampo campoConNombre:@"cuestionario.relacionesAdultos" Tipo:TipoCampoRadio]],
                            @[[RSCampo campoConNombre:@"cuestionario.relacionesColegio" Tipo:TipoCampoRadio]],
                            @[[RSCampo campoConNombre:@"cuestionario.relacionesCasa" Tipo:TipoCampoRadio]]
                            ],
                        ];
    
    
    [self.formulario configuraCampos:campos];
    [self.formulario agregaCampoEnlazado:@"cuestionario.tipoDeMedicacion" aSwitch:@"cuestionario.medicacion"];
    [self.formulario agregaCampoEnlazado:@"cuestionario.edadDeglucion" aSwitch:@"cuestionario.deglucion"];
    [self.formulario agregaCampoEnlazado:@"cuestionario.descripcionManias" aSwitch:@"cuestionario.manias"];
    [self.formulario agregaCampoEnlazado:@"cuestionario.edadPrimerasPalabras" aSwitch:@"cuestionario.primerasPalabras"];
    [self.formulario agregaCampoEnlazado:@"cuestionario.edadPrimerasFrases" aSwitch:@"cuestionario.primerasFrases"];
    
    self.formulario.separacionBloques = 15;
    [self.contentView addSubview:self.formulario];
    
    if(self.modelo.paciente)
        [self configuraDatos];

    
    _cambios = NO;
    
    [self registerForKeyboardNotifications];
    
}

- (void)viewWillLayoutSubviews {
    self.contentView.frame = CGRectInset(self.scrollView.bounds, 20, 20);
    self.formulario.frame = CGRectMake(20, 20, self.contentView.bounds.size.width-40, 1550);
    
    CGRect guardarFrame = self.botonGuardar.frame;
    guardarFrame.origin.x = self.formulario.frame.origin.x+self.formulario.frame.size.width-self.botonGuardar.frame.size.width;
    guardarFrame.origin.y = self.formulario.frame.origin.y + self.formulario.frame.size.height + 20;
    self.botonGuardar.frame = guardarFrame;
    
    
    CGRect contentFrame = self.contentView.frame;
    contentFrame.size.height = self.formulario.frame.size.height + self.botonGuardar.frame.size.height+60;
    self.contentView.frame = contentFrame;
    
    [self.scrollView setContentSize:CGSizeMake(self.contentView.frame.size.width+40, self.contentView.frame.size.height+40)];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)hayCambios {
    return _cambios;
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    [self configuraDatos];
}

- (void) configuraDatos {
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        campo.data.valor = [self.modelo.paciente valueForKeyPath:campo.data.nombre];
    }
    
    [self.scrollView setContentOffset:CGPointZero animated:NO];    
    _cambios = NO;
}


- (void) guardarDado:(id)sender {
    if(!self.modelo.paciente.cuestionario) {
        CuestionarioPaciente* cuestionario = [CuestionarioPaciente object];
        self.modelo.paciente.cuestionario = cuestionario;
    }
    
    NSMutableDictionary* campos = [[NSMutableDictionary alloc] init];
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        if(campo.data.valor) {
            [campos setValue:campo.data.valor forKey:campo.data.nombre];
        }
    }
    
    [PacientesManager actualizaPaciente:self.modelo.paciente conCampos:campos completado:^(NSError * error) {
        if(error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            _cambios = NO;
        }
    }];
}

#pragma mark Keyboard
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    UIView* activeField;
    for (RSCampoView *campoView in self.formulario.listaDeCampos) {
        if ([campoView.campoDeTexto isFirstResponder]) {
            activeField = (UIView *)campoView;
        }
    }
    CGRect activeFrame = [self.scrollView convertRect:activeField.frame fromView:activeField.superview];
    
    NSDictionary* info = [aNotification userInfo];
    CGRect kbFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbFrame = [self.view convertRect:kbFrame fromView:nil];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbFrame.size.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= kbFrame.size.height;
    if (!CGRectContainsPoint(aRect, activeFrame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeFrame.origin.y - aRect.size.height/2);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.scrollView setContentInset:contentInsets];
    self.scrollView.scrollIndicatorInsets = contentInsets;
}


#pragma mark RSForm Delegate
#pragma mark RSFormDelegate
- (void)campoHaCambiado:(RSCampo *)campo {
    _cambios = YES;
}

- (UIFont *)tipoParaCampo:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:12.5];
}

- (UIColor *)colorTextoCampo:(RSCampo *)campo {
    return [UIColor darkGrayColor];
}

- (UIFont *)tipoParaHint:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:9.0];
}

- (UIColor *)colorTextoHint:(RSCampo *)campo {
    return [UIColor verdeAzulado];
}

- (UIFont *)tipoParaTituloBloque:(NSInteger)numBloque {
    return [UIFont fontWithName:@"Lato-Regular" size:15.0];
}

- (UIColor *)colorTituloBloque:(NSInteger)numBloque {
    return [UIColor verdeAzulado];
}

- (NSString *) tituloDeBloque:(NSInteger) numBloque {
    switch (numBloque) {
        case 0:
            return @"ANTECEDENTES FAMILIARES";
            break;
        case 1:
            return @"MEDICACIÓN";
            break;
        case 2:
            return @"PERINATAL";
            break;
        case 3:
            return @"DESARROLLO MOTOR";
            break;
        case 4:
            return @"ALIMENTACIÓN";
            break;
        case 5:
            return @"DESARROLLO DEL LENGUAJE";
            break;
        case 6:
            return @"RELACIONES SOCIALES";
            break;
    }
    
    return nil;
}

- (NSArray *) opcionesParaCombo:(RSCampo *) campo {
    
    if([campo.nombre isEqualToString:@"cuestionario.tipoDeParto"]) {
        NSArray* opciones = @[@"Espontáneo", @"Instrumental", @"Cesárea"];
        return opciones;
    }
    
    if([campo.nombre isEqualToString:@"cuestionario.apgar"]||[campo.nombre isEqualToString:@"cuestionario.apgar2"]) {
        NSArray* opciones = @[@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10];
        return opciones;
    }
    
    
    if([campo.nombre isEqualToString:@"cuestionario.controlCefalico"]||[campo.nombre isEqualToString:@"cuestionario.gateo"]||[campo.nombre isEqualToString:@"cuestionario.deambulacion"]) {
        NSArray* opciones = @[@"1-3", @"4-6", @"7-10", @"10-16", @"17-26", @"27-36"];
        return opciones;
    }
    return nil;
}

- (NSArray *)opcionesParaRadio:(RSCampo *)campo {
    if([campo.nombre isEqualToString:@"cuestionario.habitos"]||[campo.nombre isEqualToString:@"cuestionario.praxias"])
        return @[@"adecuados", @"inadecuados"];
    
    if([campo.nombre isEqualToString:@"cuestionario.comprension"]||[campo.nombre isEqualToString:@"cuestionario.expresion"]||
       [campo.nombre isEqualToString:@"cuestionario.lectura"]||[campo.nombre isEqualToString:@"cuestionario.escritura"]||
       [campo.nombre isEqualToString:@"cuestionario.comunicacion"])
        return @[@"alto", @"normal", @"bajo"];
    
    if([campo.nombre isEqualToString:@"cuestionario.relacionesPares"]||
       [campo.nombre isEqualToString:@"cuestionario.relacionesAdultos"]||
       [campo.nombre isEqualToString:@"cuestionario.relacionesColegio"]||
       [campo.nombre isEqualToString:@"cuestionario.relacionesCasa"])
        return @[@"adecuada", @"inadecuada"];
    
    return nil;
}


@end
