//
//  PersonalesViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PersonalesViewController.h"
#import "RSForm.h"
#import "UIColor+Extensions.h"
#import "UIImage+ResizeAdditions.h"
#import "UIBAlertView.h"
#import "PacientesManager.h"
#import "RSCampoTexto.h"
#import "RSCampoPFImagen.h"

@interface PersonalesViewController ()
@property (nonatomic, strong) UIPopoverController* popOver;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) RSForm* formulario;

@property (nonatomic, strong) UIButton* botonGuardar;
@property (nonatomic, strong) UIButton* botonBorrar;
@end

@implementation PersonalesViewController {
    BOOL _cambios;
}
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectInset(self.scrollView.bounds, 20, 20)];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.contentView];
    
    
    UIImage* imgGuardar = [UIImage imageNamed:@"btnguardar.png"];
    self.botonGuardar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgGuardar.size.width, imgGuardar.size.height)];
    [self.botonGuardar setImage:imgGuardar forState:UIControlStateNormal];
    [self.botonGuardar addTarget:self action:@selector(guardarDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.botonGuardar];
    
    UIImage* imgBorrar = [UIImage imageNamed:@"btnborrar.png"];
    self.botonBorrar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgBorrar.size.width, imgBorrar.size.height)];
    [self.botonBorrar setImage:imgBorrar forState:UIControlStateNormal];
    [self.botonBorrar addTarget:self action:@selector(borrarDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.botonBorrar];
    
    
    self.formulario = [[RSForm alloc] init];
    self.formulario.colorTitulos = [UIColor verdeAzulado];
    self.formulario.delegate = self;
    
    RSCampo* campoEmail = [RSCampo campoConNombre:@"email" Tipo:TipoCampoEmail];

    RSCampo* campoAdaptacionCurricularSignificativa = [RSCampo campoConNombre:@"datos.adaptacionCurricularSignificativa" Tipo:TipoCampoSwitch Ancho:140];
    campoAdaptacionCurricularSignificativa.caption = @"Significativa";
    
    RSCampo* campoDescripcionAdaptacionCurricular = [RSCampo campoConNombre:@"datos.descripcionAdaptacionCurricular" Tipo:TipoCampoTextoLargo];
    campoDescripcionAdaptacionCurricular.caption = @"Descripción";
    
    RSCampo* campoDescripcionEvaluacionesPrevias = [RSCampo campoConNombre:@"datos.descripcionEvaluacionesPrevias" Tipo:TipoCampoTextoLargo];
    campoDescripcionEvaluacionesPrevias.caption = @"Descripción";
    
    NSArray* campos = @[
                        @[
                            @[[RSCampo campoConNombre:@"datos.foto" Tipo:TipoCampoImagen Ancho:150]]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"nombre"]],
                            @[[RSCampo campoConNombre:@"apellidos"]],
                            @[campoEmail],
                            @[[RSCampo campoConNombre:@"datos.sexo" Tipo:TipoCampoRadio], [RSCampo campoConNombre:@"datos.fechaNacimiento" Tipo:TipoCampoComboFecha]]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"datos.curso"], [RSCampo campoConNombre:@"datos.repetidor" Tipo:TipoCampoSwitch]],
                            @[[RSCampo campoConNombre:@"datos.colegio"]]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"datos.evaluacionesPrevias" Tipo:TipoCampoSwitch Ancho:180]],
                            @[campoDescripcionEvaluacionesPrevias]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"datos.nombreDelPadre" Tipo:TipoCampoTexto], [RSCampo campoConNombre:@"datos.estudiosDelPadre" Tipo:TipoCampoComboString Ancho:140]],
                            @[[RSCampo campoConNombre:@"datos.nombreDeLaMadre" Tipo:TipoCampoTexto], [RSCampo campoConNombre:@"datos.estudiosDeLaMadre" Tipo:TipoCampoComboString Ancho:140]],
                            @[[RSCampo campoConNombre:@"datos.estadoCivilDeLosPadres" Tipo:TipoCampoTexto Ancho:270]]
                            ],
                        @[
                            @[[RSCampo campoConNombre:@"datos.observaciones" Tipo:TipoCampoTextoLargo]]
                            ]
                        ];
    
    
    [self.formulario configuraCampos:campos];
    self.formulario.separacionBloques = 15;
    [self.formulario agregaCampoEnlazado:@"datos.descripcionEvaluacionesPrevias" aSwitch:@"datos.evaluacionesPrevias"];
    
    RSCampoView* campoEmailView = [self.formulario buscaCampoConNombre:@"email"];
    ((RSCampoTexto *)campoEmailView).textField.enabled = NO;
    ((RSCampoTexto *)campoEmailView).textField.alpha = 0.5;
    
    RSCampoPFImagen* campoImagen = (RSCampoPFImagen*)[self.formulario buscaCampoConNombre:@"datos.foto"];
    campoImagen.vc = self;
    
    [self.contentView addSubview:self.formulario];
    
    if(self.modelo.paciente)
        [self configuraDatos];

    _cambios = NO;
    
    [self registerForKeyboardNotifications];

}


- (void)viewWillLayoutSubviews {
    self.contentView.frame = CGRectInset(self.scrollView.bounds, 20, 20);
    self.formulario.frame = CGRectMake(20, 20, self.contentView.bounds.size.width-40, 800);
    
    CGRect guardarFrame = self.botonGuardar.frame;
    guardarFrame.origin.x = self.formulario.frame.origin.x+self.formulario.frame.size.width-self.botonGuardar.frame.size.width;
    guardarFrame.origin.y = self.formulario.frame.origin.y + self.formulario.frame.size.height + 20;
    self.botonGuardar.frame = guardarFrame;
    
    CGRect borrarFrame = self.botonBorrar.frame;
    borrarFrame.origin.x = self.botonGuardar.frame.origin.x-self.botonBorrar.frame.size.width-20;
    borrarFrame.origin.y = self.formulario.frame.origin.y + self.formulario.frame.size.height + 20;
    self.botonBorrar.frame = borrarFrame;
    
    CGRect contentFrame = self.contentView.frame;
    contentFrame.size.height = self.formulario.frame.size.height + self.botonGuardar.frame.size.height+60;
    self.contentView.frame = contentFrame;
    
    [self.scrollView setContentSize:CGSizeMake(self.contentView.frame.size.width+40, self.contentView.frame.size.height+40)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)hayCambios {
    return _cambios;
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    [self configuraDatos];
}

- (void) configuraDatos {
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        campo.data.valor = [self.modelo.paciente valueForKeyPath:campo.data.nombre];
    }
    
    [self.scrollView setContentOffset:CGPointZero animated:NO];
    _cambios = NO;
}


- (void)borrarDado:(UIButton *)sender {
    UIBAlertView* alerta = [[UIBAlertView alloc] initWithTitle:@"Atención" message:@"Se perderán todos los datos del paciente, incluidos los entrenamientos" cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Borrar", nil];
    [alerta showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
        if(!didCancel) {
            [PacientesManager borraPaciente:self.modelo.paciente completado:^(NSError * error) {
                if(error) {
                    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                } else {
                    _cambios = NO;
                }
            }];
        }
    }];
}


- (void) guardarDado:(UIButton *)sender {
    NSMutableDictionary* campos = [[NSMutableDictionary alloc] init];
    
    //TODO comprobar que no se han borrado los campos obligatorios
    for (RSCampoView* campo in [self.formulario listaDeCampos]) {
        if(campo.data.valor) {
            if(campo.data.tipo==TipoCampoImagen) {
                if([campo.data.valor isKindOfClass:[UIImage class]]) {
                    UIImage* image = (UIImage *)campo.data.valor;
                    UIImage *resizedImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                        bounds:CGSizeMake(582.0f, 582.0f)
                                                          interpolationQuality:kCGInterpolationHigh];
                    NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.8f);
                    PFFile *photoFile = [PFFile fileWithData:imageData];
                    [photoFile saveInBackground];
                    [campos setValue:photoFile forKey:campo.data.nombre];
                }
            } else {
                [campos setValue:campo.data.valor forKey:campo.data.nombre];
            }
        }
    }
    
    [PacientesManager actualizaPaciente:self.modelo.paciente conCampos:campos completado:^(NSError * error) {
        if(error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            _cambios = NO;
        }
    }];
}

#pragma mark KeyboardNotifications
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    UIView* activeField;
    for (RSCampoView *campoView in self.formulario.listaDeCampos) {
        if ([campoView.campoDeTexto isFirstResponder]) {
            activeField = (UIView *)campoView;
        }
    }
    CGRect activeFrame = [self.scrollView convertRect:activeField.frame fromView:activeField.superview];
    
    NSDictionary* info = [aNotification userInfo];
    CGRect kbFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbFrame = [self.view convertRect:kbFrame fromView:nil];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbFrame.size.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= kbFrame.size.height;
    if (!CGRectContainsPoint(aRect, activeFrame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeFrame.origin.y - aRect.size.height/2);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.scrollView setContentInset:contentInsets];
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark RSFormDelegate
- (void)campoHaCambiado:(RSCampo *)campo {
    _cambios = YES;
}

- (UIFont *)tipoParaCampo:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:12.5];
}

- (UIColor *)colorTextoCampo:(RSCampo *)campo {
    return [UIColor darkGrayColor];
}

- (UIFont *)tipoParaHint:(RSCampo *)campo {
    return [UIFont fontWithName:@"Lato-Regular" size:9.0];
}

- (UIColor *)colorTextoHint:(RSCampo *)campo {
    return [UIColor verdeAzulado];
}

- (NSArray *)opcionesParaRadio:(RSCampo *)campo {
    if([campo.nombre isEqualToString:@"datos.sexo"])
        return @[@"Mujer", @"Varón"];
    
    return nil;
}

- (NSArray *)opcionesParaCombo:(RSCampo *)campo {
    if([campo.nombre isEqualToString:@"datos.estudiosDelPadre"]||[campo.nombre isEqualToString:@"datos.estudiosDeLaMadre"])
        return @[@"Sin estudios", @"Estudios primarios", @"Estudios secundarios", @"Formación profesional", @"Estudios universitarios medios", @"Estudios universitarios superiores", @"Doctorado/master"];
    
    return nil;
}

- (UIFont *)tipoParaTituloBloque:(NSInteger)numBloque {
    return [UIFont fontWithName:@"Lato-Regular" size:15.0];
}

- (UIColor *)colorTituloBloque:(NSInteger)numBloque {
    return [UIColor verdeAzulado];
}


- (NSString *)tituloDeBloque:(NSInteger)numBloque {
    switch (numBloque) {
        case 2:
            return @"COLEGIO";
            break;
        case 3:
            return @"EVALUACIONES PREVIAS";
            break;
        case 4:
            return @"PADRES";
            break;
        case 5:
            return @"OBSERVACIONES";
            break;
    }
    
    return nil;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}
@end
