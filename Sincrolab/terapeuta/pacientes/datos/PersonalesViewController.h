//
//  PersonalesViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeccionDatosPaciente.h"
#import "RSFormDelegate.h"
#import "EntrenamientoPacienteModel.h"

@interface PersonalesViewController : UIViewController <SeccionDatosPaciente, RSFormDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;

@end
