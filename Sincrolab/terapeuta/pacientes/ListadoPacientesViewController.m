//
//  PacientesViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ListadoPacientesViewController.h"
#import "UIColor+Extensions.h"

#import "ActividadViewController.h"
#import "DatosViewController.h"
#import "PerfilViewController.h"
#import "EntrenamientoViewController.h"
#import "EstadisticasPacienteViewController.h"

#import "PacienteCell.h"
#import "NuevoPacienteViewController.h"
#import "UIBAlertView.h"
#import "DiaEntrenamiento.h"

#import "PacientesManager.h"
#import "Constantes.h"

#import "EntrenamientoPacienteModel.h"

static NSString *CellIdentifier = @"pacienteCell";

@interface ListadoPacientesViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnActividad;
@property (weak, nonatomic) IBOutlet UIButton *btnDatos;
@property (weak, nonatomic) IBOutlet UIButton *btnPerfil;
@property (weak, nonatomic) IBOutlet UIButton *btnEntrenamiento;
@property (weak, nonatomic) IBOutlet UIButton *btnEstadisticas;
@property (weak, nonatomic) IBOutlet UIView *fondoTabla;
@property (weak, nonatomic) IBOutlet UIButton *btnNuevoPaciente;
@property (weak, nonatomic) IBOutlet UILabel *labelPacientes;

@property (nonatomic, strong) UIViewController* currentViewController;
@property (nonatomic, weak) UIButton* botonSeleccionado;
@property (weak, nonatomic) IBOutlet UIView *placeHolderView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl* refreshControl;

@property (nonatomic, strong) NuevoPacienteViewController* nuevoPaciente;

@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end

@implementation ListadoPacientesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.labelPacientes.font = [UIFont fontWithName:@"Lato-Light" size:self.labelPacientes.font.pointSize];
    
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.fondoTabla.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    self.fondoTabla.layer.shadowOffset = CGSizeMake(2, 0);
    self.fondoTabla.layer.shadowColor = [UIColor azulOscuro].CGColor;
    self.fondoTabla.layer.shadowOpacity = 0.4;
    
    [self.btnActividad setImage:[UIImage imageNamed:@"btn_actividad_marcado.png"] forState:UIControlStateSelected];
    [self.btnDatos setImage:[UIImage imageNamed:@"btn_datos_marcado.png"] forState:UIControlStateSelected];
    [self.btnPerfil setImage:[UIImage imageNamed:@"btn_perfil_marcado.png"] forState:UIControlStateSelected];
    [self.btnEntrenamiento setImage:[UIImage imageNamed:@"btn_entrenamiento_marcado.png"] forState:UIControlStateSelected];
    [self.btnEstadisticas setImage:[UIImage imageNamed:@"btn_estadisticas_marcado.png"] forState:UIControlStateSelected];
    
    [self.btnNuevoPaciente setImage:[UIImage imageNamed:@"btn_nuevoPaciente.png"] forState:UIControlStateNormal];
    [self.btnNuevoPaciente setImage:[UIImage imageNamed:@"btn_nuevoPaciente_dado.png"] forState:UIControlStateHighlighted];
    
    
    //TABLE VIEW
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];

    [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(pacientesCargados) name:kPacientesCargadosNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(pacienteActualizado:) name:kPacienteActualizadoNotification object:nil];
    
    if(self.datasource.pacientes)
        [self pacientesCargados];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self.datasource refresca];
}


- (void) pacientesCargados {
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    
    if(self.datasource.pacientes.count==0) {
        self.btnActividad.enabled = NO;
        self.btnDatos.enabled = NO;
        self.btnPerfil.enabled = NO;
        self.btnEntrenamiento.enabled = NO;
        self.btnEstadisticas.enabled = NO;
        //TODO [self muestraSeccion:vacia];
        return;
    }
    
    self.btnActividad.enabled = YES;
    self.btnDatos.enabled = YES;
    self.btnPerfil.enabled = YES;
    self.btnEntrenamiento.enabled = YES;
    self.btnEstadisticas.enabled = YES;
    
    NSString* idSeleccionado;
    if(!self.primeraSeleccion) {
        Paciente* seleccionado = nil;
        for (Paciente* paciente in [self.datasource pacientes]) {
            if(!seleccionado)
                seleccionado = paciente;
            else {
                if([seleccionado.updatedAt compare:paciente.updatedAt]==NSOrderedAscending)
                    seleccionado = paciente;
            }
        }
        idSeleccionado = seleccionado.objectId;
    } else {
        idSeleccionado = self.primeraSeleccion;
        self.primeraSeleccion = nil;
    }
    
    int indice = 0;
    for (Paciente* paciente in [self.datasource pacientes]) {
        if([idSeleccionado isEqualToString:paciente.objectId])
            break;
        indice+=1;
    }
    if(indice>=[self.datasource pacientes].count)
        indice = 0;
    
    
    [self seleccionaCelda:indice];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:indice inSection:0];    
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    if(!self.currentViewController)
        [self botonDado:self.btnActividad];
}

- (void) pacienteActualizado:(NSNotification *) notification {
    NSNumber* indice = [notification.userInfo valueForKey:@"indice"];
    NSIndexPath* selectedIndex = [NSIndexPath indexPathForRow:indice.intValue inSection:0];
    if(selectedIndex) {
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndex] withRowAnimation:UITableViewRowAnimationLeft];
    }
    [self.tableView selectRowAtIndexPath:selectedIndex animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (IBAction)botonDado:(UIButton *)boton {
    
    if([self.currentViewController conformsToProtocol:@protocol(SeccionDatosPaciente)]&&
       ([((id<SeccionDatosPaciente>)self.currentViewController) hayCambios])) {
        
        UIBAlertView* alertView = [[UIBAlertView alloc] initWithTitle:@"Cambios sin guardar"
                                    message:@"Si continuas se perderán todos los cambios que no hayas guardado"
                          cancelButtonTitle:@"Cancelar"
                          otherButtonTitles:@"Descartar", nil];
        
        [alertView showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
            if(!didCancel) {
                [self muestraSeccion:boton];
            }
        }];        

    } else {
        [self muestraSeccion:boton];
    }
}

- (void) muestraSeccion:(UIButton *) boton {
    UIViewController* vc;
    if([boton isEqual:self.btnActividad]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"actividad"];
        ((ActividadViewController *)vc).delegate = self;
    }
    
    if([boton isEqual:self.btnDatos]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"datosPaciente"];
    }
    
    if([boton isEqual:self.btnPerfil]) {
        vc = [[PerfilViewController alloc] init];
    }
    
    if([boton isEqual:self.btnEntrenamiento]) {
        vc = [[EntrenamientoViewController alloc] init];
    }
    
    if([boton isEqual:self.btnEstadisticas]) {
        vc = [[EstadisticasPacienteViewController alloc] init];
    }

    
    if(self.currentViewController) {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
    }
    
    self.currentViewController = vc;
    
    [self addChildViewController:self.currentViewController];
    self.currentViewController.view.frame = self.placeHolderView.bounds;
    [self.placeHolderView addSubview:self.currentViewController.view];
    
    
    if(self.botonSeleccionado)
        self.botonSeleccionado.selected = NO;
    
    self.botonSeleccionado = boton;
    self.botonSeleccionado.selected = YES;
    

    if(self.modelo) {
        if([self.currentViewController respondsToSelector:NSSelectorFromString(@"modelo")]) {
            [self.currentViewController setValue:self.modelo forKey:@"modelo"];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPacienteSeleccionado:(Paciente *)pacienteSeleccionado {
    self.modelo = [[EntrenamientoPacienteModel alloc] initWithPaciente:pacienteSeleccionado];
    if([self.currentViewController respondsToSelector:NSSelectorFromString(@"modelo")]) {
        [self.currentViewController setValue:self.modelo forKey:@"modelo"];
    }
}

#pragma mark Nuevo Paciente
- (IBAction)nuevoPacienteDado:(id)sender {
    if(self.datasource.pacientes.count >= self.datasource.terapeuta.topePacientes.intValue) {
        [[[UIAlertView alloc] initWithTitle:@"Límite alcanzado" message:[NSString stringWithFormat:@"Ya ha alcanzado el límite de %d pacientes", self.datasource.terapeuta.topePacientes.intValue] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        self.nuevoPaciente = [[NuevoPacienteViewController alloc] init];
        self.nuevoPaciente.modalPresentationStyle = UIModalPresentationFormSheet;
        self.nuevoPaciente.delegate = self;
        [self presentViewController:self.nuevoPaciente animated:YES completion:nil];
    }
}

- (void)creaPaciente:(NSDictionary *) campos {
    [PacientesManager nuevoPacienteDeTerapeuta:[self.datasource terapeuta] ConCampos:campos completado:^(NSError * error, Paciente* paciente) {
        if(error) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        } else {
            [self.nuevoPaciente dismissViewControllerAnimated:YES completion:nil];
        }
    }];

}

- (BOOL)hayCambios {
    if([self.currentViewController conformsToProtocol:@protocol(SeccionDatosPaciente)])
        return [((id<SeccionDatosPaciente>)self.currentViewController) hayCambios];
    else
        return NO;
}

#pragma mark TableViewDataSource & TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.datasource pacientes].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PacienteCell* cell = (PacienteCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (PacienteCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                     reuseIdentifier:CellIdentifier];
    }
    cell.paciente = [[self.datasource pacientes] objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.currentViewController conformsToProtocol:@protocol(SeccionDatosPaciente)]&&
       ([((id<SeccionDatosPaciente>)self.currentViewController) hayCambios])) {
        
        UIBAlertView* alertView = [[UIBAlertView alloc] initWithTitle:@"Cambios sin guardar"
                                                              message:@"Si continuas se perderán todos los cambios que no hayas guardado"
                                                    cancelButtonTitle:@"Cancelar"
                                                    otherButtonTitles:@"Descartar", nil];
        
        [alertView showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
            if(!didCancel) {
                [self seleccionaCelda:indexPath.row];
            }
        }];
        
        return nil;
    } else {
        [self seleccionaCelda:indexPath.row];
    }

    return indexPath;
}

- (void) seleccionaCelda:(NSInteger) indice {
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:indice inSection:0];
   // [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    self.pacienteSeleccionado = [[self.datasource pacientes] objectAtIndex:indexPath.row];
}

#pragma mark ActividadViewControllerDelegate
- (void)muestraSeccionDatos {
    [self botonDado:self.btnDatos];
}

- (void)muestraSeccionPerfil {
    [self botonDado:self.btnPerfil];
}


@end
