//
//  PerfilViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeccionDatosPaciente.h"
#import "SubprocesoView.h"
#import "EntrenamientoPacienteModel.h"

@interface PerfilViewController : UIViewController <SeccionDatosPaciente, SubprocesoViewDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
