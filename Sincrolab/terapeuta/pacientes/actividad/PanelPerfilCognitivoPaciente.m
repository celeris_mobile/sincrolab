//
//  PanelPerfilCognitivoPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelPerfilCognitivoPaciente.h"
#import "UIColor+Extensions.h"
#import "ProcesoAfectado.h"

@interface PanelPerfilCognitivoPaciente()
@property (nonatomic, strong) UIView* contenedor;
@property (nonatomic, strong) UIScrollView* scrollView;
@end

@implementation PanelPerfilCognitivoPaciente
@synthesize paciente = _paciente;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 150, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Perfil Cognitivo";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];

    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 34, self.bounds.size.width, self.bounds.size.height-44)];
    [self addSubview:self.scrollView];
    
    self.contenedor = [[UIView alloc] initWithFrame:self.bounds];
    [self.scrollView addSubview:self.contenedor];
    
    UIImage* imagenBoton = [UIImage imageNamed:@"btn_masinfo.png"];
    UIButton* btnMasInfo = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imagenBoton.size.width, imagenBoton.size.height)];
    [btnMasInfo setImage:imagenBoton forState:UIControlStateNormal];
    CGRect imgFrame = btnMasInfo.frame;
    imgFrame.origin.x = self.frame.size.width - imgFrame.size.width;
    imgFrame.origin.y = self.frame.size.height - imgFrame.size.height;
    btnMasInfo.frame = imgFrame;
    [self addSubview:btnMasInfo];
    
    [btnMasInfo addTarget:self action:@selector(botonAmpliaDado) forControlEvents:UIControlEventTouchUpInside];
}

- (void) botonAmpliaDado {
    [self.delegate amplia:self];
}

- (void)setPaciente:(Paciente *)paciente {
    _paciente = paciente;
    
    
    if(_paciente==nil)
        return;
    
    [self.scrollView setContentOffset:CGPointZero animated:NO];

    
    for (UIView* subview in self.contenedor.subviews) {
        [subview removeFromSuperview];
    }
    
    if(!self.paciente.perfilcognitivo||self.paciente.perfilcognitivo.vacio) {
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 190, 12)];
        label.font = [UIFont fontWithName:@"Lato-Bold" size:11];
        label.text = @"No tiene perfil";
        label.textColor = [UIColor lightGrayColor];
        [self.contenedor addSubview:label];
        
    } else {
        NSArray* procesos = self.paciente.perfilcognitivo.procesosAfectados;
        
        int d = 0;
        for (ProcesoAfectado* proceso in procesos) {
            
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, d*17, 190, 12)];
            label.font = [UIFont fontWithName:@"Lato-Bold" size:11];
            label.text = [NSString stringWithFormat:@"%@:", proceso.caption];
            label.textColor = [UIColor lightGrayColor];
            [self.contenedor addSubview:label];
            
            UILabel* atributo = [[UILabel alloc] initWithFrame:CGRectMake(20, d*17+17, 190, 12)];
            atributo.font = [UIFont fontWithName:@"Lato-Bold" size:11];
            switch (proceso.afectacion.intValue) {
                case 1:
                    atributo.text = @"Leve";
                    atributo.textColor = [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0];
                    break;
                case 2:
                    atributo.text = @"Moderado";
                    atributo.textColor = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:19/255.0 alpha:1.0];
                    break;
                case 3:
                    atributo.text = @"Grave";
                    atributo.textColor = [UIColor colorWithRed:231/255.0 green:82/255.0 blue:82/255.0 alpha:1.0];
                    break;
            }
            [self.contenedor addSubview:atributo];
            
            d+=2;
        }
        
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, procesos.count*2*17+37+17);
    }
}

@end
