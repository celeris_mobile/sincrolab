//
//  PanelCalendarioPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelCalendarioPaciente.h"
#import "UIColor+Extensions.h"
#import "CalendarioSemanal.h"
#import "DiaEntrenamiento.h"
#import "DateUtils.h"
#import "DiaCalendarioViewController.h"
#import "CustomPopOverBackgroundView.h"


@interface PanelCalendarioPaciente()
@property (nonatomic, strong) UILabel* labelNoHayDatos;
@property (nonatomic, strong) NSCalendar* gregorianCalendar;
@property (nonatomic, strong) CalendarioSemanal* calendario;
@property (nonatomic, strong) UIButton* botonSiguiente;
@property (nonatomic, strong) UIButton* botonAnterior;
@property (nonatomic, strong) UILabel* labelMes;
@property (nonatomic, strong) UIView* leyenda;
@property (nonatomic, strong) NSDictionary* diasEntrenamiento;

@property (nonatomic, strong) UIPopoverController* popOver;
@end

@implementation PanelCalendarioPaciente
@synthesize modelo = _modelo;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [self.gregorianCalendar setFirstWeekday:2];
    
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 200, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Calendario del tratamiento";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    self.labelNoHayDatos = [[UILabel alloc] initWithFrame:CGRectMake(21, 34, 250, 14)];
    self.labelNoHayDatos.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    self.labelNoHayDatos.text = @"No hay un entrenamiento activo";
    self.labelNoHayDatos.textColor = [UIColor lightGrayColor];
    [self addSubview:self.labelNoHayDatos];
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    if(!_modelo)
        return;
    
    [self configura];
}

- (void) configura {

    
    if(!self.modelo.entrenamiento) {
        self.labelNoHayDatos.hidden = NO;
        if(self.calendario) {
            [self borraCalendario];
        }
        return;
    }
    
    self.labelNoHayDatos.hidden = YES;
    
    NSMutableDictionary* auxDias = [[NSMutableDictionary alloc] init];
    for (DiaEntrenamiento* dia in self.modelo.entrenamiento.diasDeEntrenamiento) {
        NSDateComponents* components = [self.gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dia.fecha];
        NSString* key = [NSString stringWithFormat:@"%d%d%d", (int)components.year, (int)components.month, (int)components.day];
        [auxDias setObject:dia forKey:key];
    }
    
    self.diasEntrenamiento = auxDias.copy;
    [self generaCalendario];
}

- (void) borraCalendario {
    
    [self.botonSiguiente removeFromSuperview];
    self.botonSiguiente = nil;
    [self.botonAnterior removeFromSuperview];
    self.botonAnterior = nil;
    
    [self.labelMes removeFromSuperview];
    self.labelMes = nil;
    
    
    [self.leyenda removeFromSuperview];
    self.leyenda = nil;
    
    [self.calendario removeFromSuperview];
    self.calendario.delegate = nil;
    self.calendario = nil;
}

- (void) generaCalendario {
    if(self.calendario) {
        [self borraCalendario];
    }
    
    NSDate* fechaInicio = self.modelo.entrenamiento.inicio;
    

    _calendario = [[CalendarioSemanal alloc] initWithInicio:fechaInicio andFinal:[NSDate date]];
    _calendario.delegate = self;
    [self addSubview:_calendario];

    
    self.botonSiguiente = [[UIButton alloc] initWithFrame:CGRectMake(311, 63, 104, 20)];
    self.botonSiguiente.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.botonSiguiente.layer.borderWidth = 1.0;
    [self.botonSiguiente setTitle:@"Siguiente semana" forState:UIControlStateNormal];
    self.botonSiguiente.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:10.0];
    [self.botonSiguiente setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.botonSiguiente addTarget:self action:@selector(botonSiguienteDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonSiguiente];
    

    self.botonAnterior = [[UIButton alloc] initWithFrame:CGRectMake(20, 63, 104, 20)];
    self.botonAnterior.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.botonAnterior.layer.borderWidth = 1.0;
    [self.botonAnterior setTitle:@"Anterior semana" forState:UIControlStateNormal];
    self.botonAnterior.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:10.0];
    [self.botonAnterior setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.botonAnterior addTarget:self action:@selector(botonAnteriorDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonAnterior];
    
    self.labelMes = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 430, 25)];
    self.labelMes.font = [UIFont fontWithName:@"Lato-Bold" size:12];
    self.labelMes.textColor = [UIColor azulOscuro];
    self.labelMes.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.labelMes];
    
    
    self.leyenda = [[UIView alloc] init];
    NSArray* labels = @[@"Entrenado", @"Inactividad", @"Pendiente de entrenamiento", @"Hoy", @"Dia libre"];
    NSArray* colores = @[[UIColor colorWithRed:171/255.0 green:233/255.0 blue:212/255.0 alpha:1.0],
                         [UIColor colorWithRed:231/255.0 green:82/255.0 blue:82/255.0 alpha:1.0],
                         [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0],
                         [UIColor colorWithRed:88/255.0 green:200/255.0 blue:247/255.0 alpha:1.0],
                         [UIColor whiteColor]];
    [self addSubview:self.leyenda];
    
    CGRect frameAnterior = CGRectZero;
    for (int l = 0; l<labels.count; l++) {
        UILabel* label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:@"Lato-Regular" size:11.0];
        label.textColor = [UIColor lightGrayColor];
        label.text = [labels objectAtIndex:l];
        CGSize tam = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}];
        
        UIView* cuadrado = [[UIView alloc] init];
        cuadrado.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cuadrado.layer.borderWidth = 0.5;
        cuadrado.backgroundColor = [colores objectAtIndex:l];
        cuadrado.frame = CGRectMake(frameAnterior.origin.x + frameAnterior.size.width + 6, 3, 10, 10);
        
        label.frame = CGRectMake(cuadrado.frame.origin.x + cuadrado.frame.size.width + 2, 0, tam.width, tam.height);
        frameAnterior = label.frame;
        
        [self.leyenda addSubview:cuadrado];
        [self.leyenda addSubview:label];
    }
    
    [self.calendario ultimaSemana];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if(self.calendario) {
        CGRect calendarFrame = self.calendario.frame;
        calendarFrame.origin.x = 20;
        calendarFrame.origin.y = 90;
        self.calendario.frame = calendarFrame;
        
       // self.botonAnterior.center = CGPointMake(self.calendario.frame.origin.x - 20, self.calendario.center.y);
       // self.botonSiguiente.center = CGPointMake(self.calendario.frame.origin.x + self.calendario.frame.size.width + 40, self.calendario.center.y);
        
        self.labelMes.frame = CGRectMake(self.calendario.frame.origin.x, self.calendario.frame.origin.y-30, self.calendario.frame.size.width, 30);
        
        self.leyenda.frame = CGRectMake(20, 35, self.calendario.frame.size.width, 20);
    }
}

- (void) botonSiguienteDado:(UIButton *) boton {
    [self.calendario siguienteSemana];
}

- (void) botonAnteriorDado:(UIButton *) boton {
    [self.calendario anteriorSemana];
}

#pragma mark CalendarioDelegate
- (void)calendar:(CalendarioSemanal *)calendario haCambiadoASemana:(NSDate *)diaUno {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"MMMM yyyy"];
    
    self.labelMes.text = [formatter stringFromDate:diaUno].uppercaseString;
}

- (TipoCelda) calendar:(CalendarioSemanal *)calendario tipoDeCeldaParaDeFecha:(NSDate *)fecha {
    TipoCelda tipo = TipoCeldaLibre;
    
    NSLog(@"celda: %@", fecha);
    
    NSDateComponents* components = [self.gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:fecha];
    NSInteger weekday = components.weekday;
    weekday-=2;
    if(weekday<0)
        weekday = 6;
    NSLog(@"components");
    int diasSemana = self.modelo.entrenamiento.estadoEntrenamiento.diasSemana.intValue;
    NSLog(@"dias");
    BOOL seJuega = (diasSemana&(int)pow(2, weekday))!=0;
    NSLog(@"juega?");
    if(seJuega) {
        if([DateUtils comparaSinHoraFecha:fecha conFecha:[NSDate date]]==NSOrderedAscending)
            tipo = TipoCeldaFallado;
        else
            tipo = TipoCeldaFuturo;
    }
    NSLog(@"key");
    NSString* key = [NSString stringWithFormat:@"%d%d%d", (int)components.year, (int)components.month, (int)components.day];
    DiaEntrenamiento* diaEntrenamiento = [self.diasEntrenamiento objectForKey:key];
    if(diaEntrenamiento)
        tipo = TipoCeldaRealizado;
    NSLog(@"tipo");                

    return tipo;
}

- (void)calendar:(CalendarioSemanal *)calendario diaSeleccionado:(NSDate *)fecha deTipo:(TipoCelda) tipo conFrame:(CGRect)frame {
    if(self.popOver) {
        [self.popOver dismissPopoverAnimated:NO];
    }
    
    if(tipo==TipoCeldaFueraDeRango)
        return;
    
    NSDateComponents* components = [self.gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:fecha];
    NSString* key = [NSString stringWithFormat:@"%d%d%d", (int)components.year, (int)components.month, (int)components.day];
    
    DiaCalendarioViewController* vc = [[DiaCalendarioViewController alloc] init];
    vc.tipoCelda = tipo;
    vc.fecha = fecha;
    vc.dia = [self.diasEntrenamiento objectForKey:key];
    
    
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:vc];
    self.popOver.passthroughViews = [self.calendario celdasVisibles];
    
    self.popOver.popoverBackgroundViewClass = [CustomPopOverBackgroundView class];
    CGRect cellFrame = [self convertRect:frame fromView:self.calendario];
    [self.popOver presentPopoverFromRect:cellFrame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

@end
