//
//  PanelDatosPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelDatosPaciente.h"
#import "UIColor+Extensions.h"

@interface PanelDatosPaciente()
@property (nonatomic, strong) UIView* contenedor;
@end

@implementation PanelDatosPaciente

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 150, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Paciente";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    UIView* separador = [[UIView alloc] initWithFrame:CGRectMake(217, 36, 1, 143)];
    separador.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separador];
    
    self.contenedor = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:self.contenedor];
        
    UIImage* imagenBoton = [UIImage imageNamed:@"btn_masinfo.png"];
    UIButton* btnMasInfo = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imagenBoton.size.width, imagenBoton.size.height)];
    [btnMasInfo setImage:imagenBoton forState:UIControlStateNormal];
    CGRect imgFrame = btnMasInfo.frame;
    imgFrame.origin.x = self.frame.size.width - imgFrame.size.width;
    imgFrame.origin.y = self.frame.size.height - imgFrame.size.height;
    btnMasInfo.frame = imgFrame;
    [self addSubview:btnMasInfo];
    
    [btnMasInfo addTarget:self action:@selector(botonAmpliaDado) forControlEvents:UIControlEventTouchUpInside];
}

- (void) botonAmpliaDado {
    [self.delegate amplia:self];
}
- (void)setPaciente:(Paciente *)paciente {
    _paciente = paciente;

    if(_paciente==nil)
        return;
    
    for (UIView* subview in self.contenedor.subviews) {
        [subview removeFromSuperview];
    }
    
    NSMutableArray* datos = [[NSMutableArray alloc] init];
    
    [datos addObject:@"Nombre:"];
    [datos addObject:[NSString stringWithFormat:@"%@ %@", self.paciente.nombre, self.paciente.apellidos]];
    [datos addObject:@"Fecha de Nacimiento:"];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [datos addObject:[dateFormatter stringFromDate:self.paciente.datos.fechaNacimiento]];
    [datos addObject:@"Edad:"];
    [datos addObject:self.paciente.datos.edad.stringValue];
    
    
    
    for (int d = 0; d<datos.count; d+=2) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 37+d*17, 190, 12)];
        label.font = [UIFont fontWithName:@"Lato-Bold" size:11];
        label.text = [datos objectAtIndex:d];
        label.textColor = [UIColor lightGrayColor];
        [self.contenedor addSubview:label];
        
        UILabel* atributo = [[UILabel alloc] initWithFrame:CGRectMake(20, 37+d*17+17, 190, 12)];
        atributo.font = [UIFont fontWithName:@"Lato-Bold" size:11];
        atributo.text = [datos objectAtIndex:d+1];
        atributo.textColor = [UIColor azulOscuro];
        [self.contenedor addSubview:atributo];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
