//
//  PanelPerfilCognitivoPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paciente.h"

@class PanelPerfilCognitivoPaciente;
@protocol PanelPerfilCognitivoPacienteDelegate <NSObject>
- (void) amplia:(PanelPerfilCognitivoPaciente *) panel;
@end

@interface PanelPerfilCognitivoPaciente : UIView
@property (nonatomic, weak) id<PanelPerfilCognitivoPacienteDelegate> delegate;
@property (nonatomic, weak) Paciente* paciente;
@end
