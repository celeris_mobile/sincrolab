//
//  PanelProgresoPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelProgresoPaciente.h"
#import "UIColor+Extensions.h"
#import "GraficaLineas.h"
#import "UIColor+Extensions.h"
#import "DiaEntrenamiento.h"

@interface PanelProgresoPaciente()
@property (nonatomic, strong) GraficaLineas* grafica;
@property (nonatomic, strong) UIButton* botonGongs;
@property (nonatomic, strong) UIButton* botonInvasion;
@property (nonatomic, strong) UIButton* botonPoker;
@property (nonatomic, strong) UILabel* labelNoHayDatos;

@property (nonatomic, strong) NSArray* dias;
@end

@implementation PanelProgresoPaciente


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 150, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Progreso en Juegos";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    self.labelNoHayDatos = [[UILabel alloc] initWithFrame:CGRectMake(21, 34, 150, 14)];
    self.labelNoHayDatos.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    self.labelNoHayDatos.text = @"No hay datos disponibles";
    self.labelNoHayDatos.textColor = [UIColor lightGrayColor];
    [self addSubview:self.labelNoHayDatos];
   
    self.grafica = [[GraficaLineas alloc] initWithFrame:CGRectMake(20, 72, 570, 139)];
    self.grafica.ejes = GFBordesWidthMake(0, 0.5, 0, 0);
    self.grafica.dibujaLineasY = YES;
    self.grafica.dibujaLineasX = NO;
    self.grafica.dibujaEtiquetasY = YES;
//    self.grafica.minY = 0;
    self.grafica.delegate = self;
    self.grafica.dataSource = self;
    [self addSubview:self.grafica];
    [self.grafica dibuja];
    
    self.botonGongs = [[UIButton alloc] initWithFrame:CGRectMake(21, 38, 119, 22)];
    self.botonGongs.tag = 0;
    [self.botonGongs setTitle:@"ToGong" forState:UIControlStateNormal];
    [self.botonGongs setBackgroundColor:[UIColor colorGrafica:0 conAlpha:1.0]];
    [self.botonGongs.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:12.0]];
    [self.botonGongs addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    self.botonGongs.selected = YES;
    [self addSubview:self.botonGongs];
    
    self.botonInvasion = [[UIButton alloc] initWithFrame:CGRectMake(143, 38, 119, 22)];
    self.botonInvasion.tag = 1;
    [self.botonInvasion setTitle:@"Invasion Samurai" forState:UIControlStateNormal];
    [self.botonInvasion setBackgroundColor:[UIColor colorGrafica:1 conAlpha:1.0]];
    [self.botonInvasion.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:12.0]];
    [self.botonInvasion addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    self.botonInvasion.selected = YES;
    [self addSubview:self.botonInvasion];
    
    self.botonPoker = [[UIButton alloc] initWithFrame:CGRectMake(265, 38, 119, 22)];
    self.botonPoker.tag = 2;
    [self.botonPoker setTitle:@"Poker Samurai" forState:UIControlStateNormal];
    [self.botonPoker setBackgroundColor:[UIColor colorGrafica:2 conAlpha:1.0]];
    [self.botonPoker.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:12.0]];
    [self.botonPoker addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    self.botonPoker.selected = YES;
    [self addSubview:self.botonPoker];
}

- (void) botonDado:(UIButton *) boton {
    boton.selected = !boton.selected;
    [boton setBackgroundColor:[UIColor colorGrafica:boton.tag conAlpha:boton.selected?1.0:0.5]];
    [self.grafica dibuja];
}

- (void)setPaciente:(Paciente *)paciente {
    _paciente = paciente;
    
    if(!_paciente)
        return;
    
    self.dias = [self.datasource diasDeEntrenamiento];
    
    if(!self.dias||self.dias.count==0) {
        self.botonGongs.hidden = YES;
        self.botonInvasion.hidden = YES;
        self.botonPoker.hidden = YES;
        self.grafica.hidden = YES;
        self.labelNoHayDatos.hidden = NO;
        return;
    }
    
    self.botonGongs.hidden = NO;
    self.botonInvasion.hidden = NO;
    self.botonPoker.hidden = NO;
    self.grafica.hidden = NO;
    self.labelNoHayDatos.hidden = YES;
    
    [self.grafica dibuja];
}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    if(!self.dias||self.dias.count==0)
        return 0;
    return 3;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    NSNumber* nivelAnterior = [NSNumber numberWithInt:0];
    NSMutableArray* array = [[NSMutableArray alloc] init];
    for (DiaEntrenamiento* dia in self.dias) {
        PFObject* partida;
        switch (numSet) {
            case 0:
                partida = dia.partidaGongs;
                break;
            case 1:
                partida = dia.partidaInvasion;
                break;
            case 2:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
//            [array addObject:[partida valueForKey:@"cambioNivel"]];
            NSNumber* cambioNivel = [partida valueForKey:@"cambioNivel"];
            [array addObject:[NSNumber numberWithInt:cambioNivel.intValue+nivelAnterior.intValue]];
            nivelAnterior = cambioNivel;
            
        } else {
            [array addObject:[NSNull null]];
        }
    }

    /*
    NSMutableArray *valores = [[NSMutableArray alloc] init];
    NSNumber* anterior = [NSNumber numberWithInt:0];
    for (NSNumber* valor in array) {
        if(valor!=(id)[NSNull null]) {
            [valores addObject:[NSNumber numberWithInt:anterior.intValue+valor.intValue]];
            anterior = [NSNumber numberWithInt:anterior.intValue+valor.intValue];
        } else {
            [valores addObject:[NSNull null]];
        }
    }
*/

    return array.copy;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    for (DiaEntrenamiento* dia in self.dias) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        //[array addObject:[formatter stringFromDate:dia.fecha]];
        [array addObject:@""];
    }
    
    return array.copy;
}


#pragma mark GraficaLineasDelegate
- (void)grafica:(GraficaLineas *)grafica configuraSet:(NSInteger)numSet enContexto:(CGContextRef)context {
    UIButton* boton;
    switch (numSet) {
        case 0:
            boton = self.botonGongs;
            break;
        case 1:
            boton = self.botonInvasion;
            break;
        case 2:
            boton = self.botonPoker;
            break;
            
    }
    float alpha = boton.selected?1.0:0.0;
    CGContextSetStrokeColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:1.0*alpha].CGColor);
    CGContextSetLineWidth(context, 3);
    
    CGContextSetFillColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:0.4*alpha].CGColor);
}


@end
