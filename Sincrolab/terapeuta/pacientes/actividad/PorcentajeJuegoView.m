//
//  PorcentajeJuegoView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PorcentajeJuegoView.h"
#import "UIColor+Extensions.h"

@interface PorcentajeJuegoView()
@property (nonatomic, strong) UIImageView* thumb;
@property (nonatomic, strong) UILabel* labelPorcentaje;
@property (nonatomic, strong) UIView* barra;
@end

@implementation PorcentajeJuegoView

- (id) initWithJuego:(TipoJuego) tipoJuego porcentaje:(float)p {
    
    self = [super initWithFrame:CGRectMake(0, 0, 29, 120)];
    if (self) {
        
        self.thumb = [[UIImageView alloc] initWithImage:[Constantes miniaturaJuego:tipoJuego]];
        self.thumb.frame = CGRectMake(0, 120-27, 27, 27);
        [self addSubview:self.thumb];
        
        self.labelPorcentaje = [[UILabel alloc] initWithFrame:CGRectMake(29/2-35/2, 120-80*p-50, 35, 14)];
        self.labelPorcentaje.textAlignment = NSTextAlignmentCenter;
        self.labelPorcentaje.font = [UIFont fontWithName:@"Lato-Black" size:12.5];
        self.labelPorcentaje.textColor = [UIColor azulOscuro];
        self.labelPorcentaje.text = [NSString stringWithFormat:@"%.0f%%", p*100];
        [self addSubview:self.labelPorcentaje];
        
        self.barra = [[UIView alloc] initWithFrame:CGRectMake(0, 120-80*p-35, 27, 80*p)];
        self.barra.backgroundColor = [UIColor colorGrafica:tipoJuego conAlpha:1.0];
        [self addSubview:self.barra];
    }
    return self;
}

@end
