//
//  PanelProgresoPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paciente.h"
#import "GraficaLineas.h"

@protocol PanelProgresoPacienteDatasource <NSObject>
- (NSArray*) diasDeEntrenamiento;
@end

@interface PanelProgresoPaciente : UIView <GraficaLineasDatasource, GraficaLineasDelegate>
@property (nonatomic, weak) Paciente* paciente;
@property (nonatomic, weak) id<PanelProgresoPacienteDatasource> datasource;
@end
