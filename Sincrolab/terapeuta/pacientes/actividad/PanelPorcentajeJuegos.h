//
//  PanelPorcentajeJuegos.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntrenamientoPacienteModel.h"

@interface PanelPorcentajeJuegos : UIView
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
