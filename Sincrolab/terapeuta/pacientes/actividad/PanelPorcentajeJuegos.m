//
//  PanelPorcentajeJuegos.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelPorcentajeJuegos.h"
#import "UIColor+Extensions.h"
#import "PorcentajeJuegoView.h"

@interface PanelPorcentajeJuegos()
@property (nonatomic, strong) UILabel* labelNoHayDatos;
@property (nonatomic, strong) UIView* juegos;
@end

@implementation PanelPorcentajeJuegos
@synthesize modelo = _modelo;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 150, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Porcentaje Juegos";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    self.labelNoHayDatos = [[UILabel alloc] initWithFrame:CGRectMake(21, 34, 250, 14)];
    self.labelNoHayDatos.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    self.labelNoHayDatos.text = @"No hay datos";
    self.labelNoHayDatos.textColor = [UIColor lightGrayColor];
    [self addSubview:self.labelNoHayDatos];
    
    UIView* separador = [[UIView alloc] initWithFrame:CGRectMake(25, 132, 125, 1)];
    separador.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separador];
    
    self.juegos = [[UIView alloc] init];
    [self addSubview:self.juegos];
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    [self configura];
}

- (void) configura {
    NSArray* tiempos = [self.modelo tiempoJuegos];
    
    for (PorcentajeJuegoView* porcent in self.juegos.subviews) {
        [porcent removeFromSuperview];
    }
    
    if(!tiempos) {
        self.labelNoHayDatos.hidden = NO;
        return;
    }
    
    self.labelNoHayDatos.hidden = YES;
    
    NSNumber* total = [tiempos valueForKeyPath:@"@sum.self"];
    for (int j = 0; j<3; j++) {
        float p = ((NSNumber *)[tiempos objectAtIndex:j]).floatValue/total.floatValue;
        PorcentajeJuegoView* porcent = [[PorcentajeJuegoView alloc] initWithJuego:j porcentaje:p];
        CGRect pframe = porcent.frame;
        pframe.origin.x = 25+34*j;
        pframe.origin.y = 43;
        porcent.frame = pframe;
        [self.juegos addSubview:porcent];
    }
}

@end
