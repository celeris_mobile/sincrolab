//
//  PanelCalendarioPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntrenamientoPacienteModel.h"
#import "CalendarioSemanal.h"

@interface PanelCalendarioPaciente : UIView <CalendarioSemanalDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
