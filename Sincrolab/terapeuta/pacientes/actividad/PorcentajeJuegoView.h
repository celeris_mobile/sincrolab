//
//  PorcentajeJuegoView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"

@interface PorcentajeJuegoView : UIView
- (id) initWithJuego:(TipoJuego) tipoJuego porcentaje:(float) p;
@end
