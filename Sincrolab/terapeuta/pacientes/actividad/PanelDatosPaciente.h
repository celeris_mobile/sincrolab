//
//  PanelDatosPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paciente.h"

@class PanelDatosPaciente;
@protocol PanelDatosPacienteDelegate <NSObject>
- (void) amplia:(PanelDatosPaciente *) panel;
@end

@interface PanelDatosPaciente : UIView
@property (nonatomic, weak) id<PanelDatosPacienteDelegate> delegate;
@property (nonatomic, weak) Paciente* paciente;
@end
