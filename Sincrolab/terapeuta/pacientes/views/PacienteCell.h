//
//  PacienteCell.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paciente.h"

@interface PacienteCell : UITableViewCell
@property (nonatomic, weak) Paciente* paciente;
@end
