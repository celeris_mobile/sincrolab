//
//  PacienteCell.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PacienteCell.h"
#import "DateUtils.h"

@interface PacienteCell()
@property (weak, nonatomic) IBOutlet UILabel *fecha;
@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UILabel *diagnostico;
@property (weak, nonatomic) IBOutlet UIView *foto;
@property (weak, nonatomic) IBOutlet UIView *selectorView;

@end

@implementation PacienteCell

- (void)awakeFromNib {
    self.selectorView.hidden = YES;
    self.backgroundColor = [UIColor clearColor];
    
    self.fecha.font = [UIFont fontWithName:@"Lato-Bold" size:self.fecha.font.pointSize];
    self.nombre.font = [UIFont fontWithName:@"Lato-Bold" size:self.nombre.font.pointSize];
    self.diagnostico.font = [UIFont fontWithName:@"Lato-Bold" size:self.diagnostico.font.pointSize];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    self.selectorView.hidden = !selected;
}


- (void) setPaciente:(Paciente *)paciente {
    self.nombre.text = [NSString stringWithFormat:@"%@ %@", paciente.nombre, paciente.apellidos];
    self.fecha.text = [DateUtils fechaToIntervalo:paciente.createdAt];
    self.diagnostico.text = paciente.email;
    self.backgroundColor = [UIColor clearColor];
    
}

@end
