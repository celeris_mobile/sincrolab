//
//  ProcesoView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProcesoView : UIView
@property (nonatomic, weak) NSString* titulo;
@property (nonatomic, weak) NSArray* titulos_subprocesos;
@property (nonatomic, readonly) NSDictionary* subprocesos;
@end
