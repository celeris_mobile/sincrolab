//
//  SubprocesoView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SubprocesoView;

@protocol SubprocesoViewDelegate <NSObject>
- (void) haCambiado:(SubprocesoView *) subproceso;
@end

@interface SubprocesoView : UIView
@property (nonatomic, weak) id<SubprocesoViewDelegate> delegate;
@property (nonatomic, weak) NSString* titulo;
@property (nonatomic, strong) NSNumber* valor;
@end
