//
//  SubprocesoView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SubprocesoView.h"
#import "UIColor+Extensions.h"

@interface SubprocesoView()
@property (nonatomic, strong) UILabel* tituloLabel;
@property (nonatomic, weak) UIButton* seleccionado;
@property (nonatomic, strong) NSArray* botones;
@end

@implementation SubprocesoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor clearColor];
    
    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, 17, 240, 16)];
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    self.tituloLabel.textColor = [UIColor azulOscuro];
    [self addSubview:self.tituloLabel];
    
    UILabel* etiqueta1 = [[UILabel alloc] initWithFrame:CGRectMake(294, 17, 64, 20)];
    etiqueta1.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    etiqueta1.textColor = [UIColor azulOscuro];
    etiqueta1.text = @"No";
    [self addSubview:etiqueta1];

    UILabel* etiqueta2 = [[UILabel alloc] initWithFrame:CGRectMake(341, 17, 64, 20)];
    etiqueta2.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    etiqueta2.textColor = [UIColor azulOscuro];
    etiqueta2.text = @"Leve";
    [self addSubview:etiqueta2];

    UILabel* etiqueta3 = [[UILabel alloc] initWithFrame:CGRectMake(401, 17, 80, 20)];
    etiqueta3.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    etiqueta3.textColor = [UIColor azulOscuro];
    etiqueta3.text = @"Moderado";
    [self addSubview:etiqueta3];

    UILabel* etiqueta4 = [[UILabel alloc] initWithFrame:CGRectMake(500, 17, 64, 20)];
    etiqueta4.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    etiqueta4.textColor = [UIColor azulOscuro];
    etiqueta4.text = @"Grave";
    [self addSubview:etiqueta4];
    
    UIButton* boton1 = [[UIButton alloc] initWithFrame:CGRectMake(308, 8, 38, 38)];
    [boton1 setImage:[UIImage imageNamed:@"radiooff.png"] forState:UIControlStateNormal];
    [boton1 setImage:[UIImage imageNamed:@"radioon.png"] forState:UIControlStateSelected];
    [boton1 addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    boton1.tag = 0;
    [self addSubview:boton1];
    
    UIButton* boton2 = [[UIButton alloc] initWithFrame:CGRectMake(369, 8, 38, 38)];
    [boton2 setImage:[UIImage imageNamed:@"radiooff.png"] forState:UIControlStateNormal];
    [boton2 setImage:[UIImage imageNamed:@"radioleve.png"] forState:UIControlStateSelected];
    [boton2 addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    boton2.tag = 1;
    [self addSubview:boton2];

    UIButton* boton3 = [[UIButton alloc] initWithFrame:CGRectMake(467, 8, 38, 38)];
    [boton3 setImage:[UIImage imageNamed:@"radiooff.png"] forState:UIControlStateNormal];
    [boton3 setImage:[UIImage imageNamed:@"radiomoderado.png"] forState:UIControlStateSelected];
    [boton3 addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    boton3.tag = 2;
    [self addSubview:boton3];

    UIButton* boton4 = [[UIButton alloc] initWithFrame:CGRectMake(537, 8, 38, 38)];
    [boton4 setImage:[UIImage imageNamed:@"radiooff.png"] forState:UIControlStateNormal];
    [boton4 setImage:[UIImage imageNamed:@"radiograve.png"] forState:UIControlStateSelected];
    [boton4 addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    boton4.tag = 3;
    [self addSubview:boton4];
    
    self.botones = [NSArray arrayWithObjects:boton1, boton2, boton3, boton4, nil];
    
    self.seleccionado = boton1;
    self.seleccionado.selected = YES;
    
}

- (void)activa {
    
}

- (void)desactiva {
    
}

- (void)setTitulo:(NSString *)titulo {
    _titulo = titulo;
    self.tituloLabel.text = self.titulo;
}

- (void) botonDado: (UIButton *) boton {
    if(boton==self.seleccionado)
        return;
    
    self.seleccionado.selected = NO;
    self.seleccionado = boton;
    self.seleccionado.selected = YES;
    
    [self.delegate haCambiado:self];
}

- (NSNumber *)valor {
    return [NSNumber numberWithInt:self.seleccionado.tag];
}

- (void)setValor:(NSNumber *)valor {
    self.seleccionado.selected = NO;
    if(!valor)
        self.seleccionado = [self.botones objectAtIndex:0];
    else
        self.seleccionado = [self.botones objectAtIndex:((NSNumber *)valor).intValue];
    
    self.seleccionado.selected = YES;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(ctx, 1.0);
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    
    CGContextStrokeRect(ctx, CGRectMake(2,2, 570, 42));
    
    CGContextMoveToPoint(ctx, 282, 2);
    CGContextAddLineToPoint(ctx, 282, 44);
    CGContextStrokePath(ctx);
}


@end
