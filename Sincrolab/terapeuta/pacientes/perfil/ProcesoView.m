//
//  ProcesoView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ProcesoView.h"
#import "SubprocesoView.h"
#import "UIColor+Extensions.h"

@interface ProcesoView()
@property (nonatomic, strong) UILabel* tituloLabel;
@end

@implementation ProcesoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor whiteColor];

    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(22, 22, 300, 15)];
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15.0];
    self.tituloLabel.textColor = [UIColor verdeAzulado];
    [self addSubview:self.tituloLabel];
    
    UILabel* columna1 = [[UILabel alloc] initWithFrame:CGRectMake(31, 59, 240, 13)];
    columna1.font = [UIFont fontWithName:@"Lato-Regular" size:12];
    columna1.textColor = [UIColor darkGrayColor];
    columna1.text = @"Componentes";
    [self addSubview:columna1];
    
    UILabel* columna2 = [[UILabel alloc] initWithFrame:CGRectMake(312, 59, 240, 13)];
    columna2.font = [UIFont fontWithName:@"Lato-Regular" size:12];
    columna2.textColor = [UIColor darkGrayColor];
    columna2.text = @"Niveles de afectación";
    [self addSubview:columna2];
    
}

- (void)setTitulo:(NSString *)titulo {
    _titulo = titulo;
    self.tituloLabel.text = _titulo;
}

- (void) setTitulos_subprocesos:(NSArray *)titulos_subprocesos {
    _titulos_subprocesos = titulos_subprocesos;

    NSMutableDictionary* subprocaux = [[NSMutableDictionary alloc] init];
    
    int s = 0;
    for (NSString* titulo in self.titulos_subprocesos) {
        SubprocesoView* subproceso = [[SubprocesoView alloc] initWithFrame:CGRectMake(18, 80+42*s-2, 575, 46)];
        subproceso.titulo = [self.titulos_subprocesos objectAtIndex:s];
        s++;
        [self addSubview:subproceso];
        
        NSString* auxKey = [[titulo.capitalizedString componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@""];
        [subprocaux setObject:subproceso forKey:auxKey];
    }
    
    _subprocesos = subprocaux.copy;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(ctx, 1.0);
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    CGContextStrokeRect(ctx, CGRectMake(20, 50, 570, 30));
    
    CGContextMoveToPoint(ctx, 300, 50);
    CGContextAddLineToPoint(ctx, 300, 80);
    CGContextStrokePath(ctx);
}


@end
