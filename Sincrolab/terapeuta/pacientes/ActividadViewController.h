//
//  ActividadViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PanelProgresoPaciente.h"
#import "PanelDatosPaciente.h"
#import "PanelPerfilCognitivoPaciente.h"
#import "EntrenamientoPacienteModel.h"

@protocol ActividadViewControllerDelegate <NSObject>
- (void) muestraSeccionDatos;
- (void) muestraSeccionPerfil;
@end

@interface ActividadViewController : UIViewController <PanelProgresoPacienteDatasource, PanelDatosPacienteDelegate, PanelPerfilCognitivoPacienteDelegate>
@property (nonatomic, weak) id<ActividadViewControllerDelegate> delegate;
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
