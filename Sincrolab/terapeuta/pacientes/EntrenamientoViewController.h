//
//  EntrenamientoViewController.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeccionDatosPaciente.h"
#import "EntrenamientoPacienteModel.h"

@interface EntrenamientoViewController : UINavigationController <SeccionDatosPaciente>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
