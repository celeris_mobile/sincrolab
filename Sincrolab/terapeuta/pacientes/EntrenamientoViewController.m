//
//  EntrenamientoViewController.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 02/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoViewController.h"
#import "EntrenamientoActualViewController.h"
#import "EntrenamientoPacienteModel.h"
#import "SeccionEntrenamiento.h"


@implementation EntrenamientoViewController
@synthesize modelo = _modelo;

- (id)init
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EntrenamientoActualViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"entrenamientoActual"];

    self = [super initWithRootViewController:vc];
    if (self) {
        self.navigationBarHidden = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    if([self.topViewController respondsToSelector:NSSelectorFromString(@"modelo")]) {
        [self.topViewController setValue:self.modelo forKey:@"modelo"];
    }
}

- (BOOL) hayCambios {
    if([self.topViewController isKindOfClass:[EntrenamientoActualViewController class]])
        return NO;
    
    return YES;
}

@end
