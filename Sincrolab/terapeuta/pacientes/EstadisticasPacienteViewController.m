//
//  EstadisticasPacienteViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadisticasPacienteViewController.h"
#import "MenuEstadisticasView.h"
#import "EstadisticasDiasViewController.h"
#import "UIColor+Extensions.h"

@interface EstadisticasPacienteViewController ()
@property (nonatomic, strong) MenuEstadisticasView* menu;
@property (nonatomic, strong) EstadisticasDiasViewController* statsDias;
@property (nonatomic, strong) UINavigationController* contentNavController;
@end

@implementation EstadisticasPacienteViewController
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.menu = [[MenuEstadisticasView alloc] init];
    self.menu.delegate = self;
    [self.view addSubview:self.menu];
    
    self.statsDias = [[EstadisticasDiasViewController alloc] init];
    self.contentNavController = [[UINavigationController alloc] initWithRootViewController:self.statsDias];
    self.contentNavController.view.frame = CGRectMake(0, 47, self.view.frame.size.width, self.view.frame.size.height-47);
    UINavigationBar* bar = self.contentNavController.navigationBar;
    //TODO podemos moverlo un poco a la derecha? Quizás haya que cambiarlo por uno custom
    
    [bar setTintColor:[UIColor lightGrayColor]];
    
    [self addChildViewController:self.contentNavController];
    [self.view addSubview:self.contentNavController.view];
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self cargaEstadisticas:TipoEstadisticasGenerales];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    if(self.contentNavController.topViewController != [EstadisticasDiasViewController class])
        [self.contentNavController popToRootViewControllerAnimated:YES];
    
    if(self.modelo.cargado) {
        self.statsDias.modelo = self.modelo;
    } else {
        [self.modelo carga:^(NSError * error) {
            if(!error) {
                self.statsDias.modelo = self.modelo;
            }
        } conProgreso:YES];
    }
}

- (void)cargaEstadisticas:(TipoEstadisticas)tipo {
    if(self.contentNavController.topViewController != [EstadisticasDiasViewController class])
        [self.contentNavController popToRootViewControllerAnimated:YES];
    
    [self.statsDias cargaEstadisticas:tipo];
}


@end
