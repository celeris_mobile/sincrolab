//
//  NuevoPacienteViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSForm.h"

@protocol NuevoPacienteViewControllerDelegate <NSObject>
- (void)creaPaciente:(NSDictionary *) campos;
@end

@interface NuevoPacienteViewController : UIViewController <RSFormDelegate>
@property (nonatomic, weak) id<NuevoPacienteViewControllerDelegate> delegate;
@end
