//
//  EstadisticasSesionesViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadisticasSesionesViewController.h"
#import "PanelProgresoJuego.h"
#import "PanelPromedioDia.h"
#import "PanelAciertosSesiones.h"
#import "PanelErroresSesiones.h"


@interface EstadisticasSesionesViewController ()
@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIView* contentView;
@property (nonatomic, strong) NSArray* paneles;
@property (nonatomic, assign) TipoJuego tipo;
@property (nonatomic, assign) DiaEntrenamiento* dia;
@end

@implementation EstadisticasSesionesViewController


- (id) initWithDia:(DiaEntrenamiento *) dia tipo:(TipoJuego) tipo {
    self = [super init];
    if(self) {
        _dia = dia;
        _tipo = tipo;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.scrollView addSubview:self.contentView];
    
    [self cargaEstadisticas];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.scrollView.frame = self.view.bounds;
 
    CGRect contentFrame = self.contentView.frame;
    contentFrame.size.width = self.scrollView.bounds.size.width;
    self.contentView.frame = contentFrame;
    
    [self.scrollView setContentSize:CGSizeMake(self.contentView.bounds.size.width, self.contentView.frame.size.height)];
    
}

- (void) layoutPaneles {
    PanelEstadisticas* panelAnterior;
    for (PanelEstadisticas* panel in self.paneles) {
        CGRect panelFrame = panel.frame;
        if(!panelAnterior)
            panelFrame.origin.y = 20;
        else
            panelFrame.origin.y = panelAnterior.frame.origin.y + [panelAnterior altura] + 20;
        
        panel.frame = panelFrame;
        
        panelAnterior = panel;
    }
    
    self.contentView.frame = CGRectMake(0, 0, self.view.bounds.size.width, panelAnterior.frame.origin.y + panelAnterior.frame.size.height + 20);
    
    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.contentView.frame.size.height)];
}


- (void)cargaEstadisticas {
    if(self.paneles) {
        for (PanelEstadisticas* panel in self.paneles) {
            [panel removeFromSuperview];
        }
        self.paneles = nil;
    }
    
    switch (self.tipo) {
        case TipoJuegoGongs: {
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelPromedioDia* panel = [[PanelPromedioDia alloc] initWithJuego:TipoJuegoGongs];
            panel.dia = self.dia;
            [paneles addObject:panel];
            [self.contentView addSubview:panel];        
            
            PanelAciertosSesiones* panel2 = [[PanelAciertosSesiones alloc] initWithJuego:TipoJuegoGongs];
            panel2.dia = self.dia;
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];
            
            PanelErroresSesiones* panel3 = [[PanelErroresSesiones alloc] initWithJuego:TipoJuegoGongs];
            panel3.dia = self.dia;
            [paneles addObject:panel3];
            [self.contentView addSubview:panel3];
            
            self.paneles = paneles.copy;
            break;
        }
            
        case TipoJuegoInvasion: {
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelPromedioDia* panel = [[PanelPromedioDia alloc] initWithJuego:TipoJuegoInvasion];
            panel.dia = self.dia;
            [paneles addObject:panel];
            [self.contentView addSubview:panel];
            
            PanelAciertosSesiones* panel2 = [[PanelAciertosSesiones alloc] initWithJuego:TipoJuegoInvasion];
            panel2.dia = self.dia;
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];
            
            PanelErroresSesiones* panel3 = [[PanelErroresSesiones alloc] initWithJuego:TipoJuegoInvasion];
            panel3.dia = self.dia;
            [paneles addObject:panel3];
            [self.contentView addSubview:panel3];
            
            self.paneles = paneles.copy;
            break;
        }
        case TipoJuegoPoker: {
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelPromedioDia* panel = [[PanelPromedioDia alloc] initWithJuego:TipoJuegoPoker];
            panel.dia = self.dia;
            [paneles addObject:panel];
            [self.contentView addSubview:panel];
            
            /*
            PanelAciertosSesiones* panel2 = [[PanelAciertosSesiones alloc] initWithJuego:TipoJuegoPoker];
            panel2.dia = self.dia;
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];
            
            PanelErroresSesiones* panel3 = [[PanelErroresSesiones alloc] initWithJuego:TipoJuegoPoker];
            panel3.dia = self.dia;
            [paneles addObject:panel3];
            [self.contentView addSubview:panel3];
            */
            self.paneles = paneles.copy;
            break;
        }
    }
    
    [self layoutPaneles];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
