//
//  PanelEstadisticas.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaEntrenamiento.h"

@protocol PanelEstadisticasDelegate <NSObject>
- (void) muestraDia:(DiaEntrenamiento *) dia;
@end

@protocol PanelEstadisticasDatasource <NSObject>
- (NSArray *) diasDeEntrenamientoEntreFechas;
@end

@interface PanelEstadisticas : UIView
- (void) configura;
- (CGFloat) altura;
- (void) generaExtras:(NSArray*) nombres conValores:(NSArray*) valores enVista:(UIView *) vista;
@property (nonatomic, weak) id<PanelEstadisticasDelegate> delegate;
@property (nonatomic, weak) id<PanelEstadisticasDatasource> datasource;
@end

