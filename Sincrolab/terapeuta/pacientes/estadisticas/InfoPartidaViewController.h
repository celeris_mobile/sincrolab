//
//  InfoPartidaViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"
#import "DiaEntrenamiento.h"

@class InfoPartidaViewController;
@protocol InforPartidaDelegate <NSObject>
- (void) botonDadoDeInfo:(InfoPartidaViewController *) infoVc;
@end

@interface InfoPartidaViewController : UIViewController
- (id) initWithTipoJuego:(TipoJuego) tipo diaDeEntrenamiento:(DiaEntrenamiento *) dia valor:(NSString *) valor;
@property (nonatomic, weak) id<InforPartidaDelegate> delegate;
@property (nonatomic, readonly) DiaEntrenamiento* dia;
@end
