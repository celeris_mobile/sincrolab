//
//  PanelPromedio.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelEstadisticas.h"
#import "GraficaPromedioJuego.h"

@interface PanelPromedio : PanelEstadisticas <GraficaPromedioJuegoDatasource>

@end
