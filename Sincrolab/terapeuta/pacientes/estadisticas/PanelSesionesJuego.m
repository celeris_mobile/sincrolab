//
//  PanelSesionesJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelSesionesJuego.h"
#import "CustomPopOverBackgroundView.h"
#import "InfoSesionViewController.h"

@interface PanelSesionesJuego()
@property (nonatomic, strong) UIPopoverController* popOver;
@end
@implementation PanelSesionesJuego

- (id)initWithJuego:(TipoJuego)tipo {
    self = [super init];
    if(self) {
        self.tipo = tipo;
    }
    
    return self;
}

#pragma mark GraficaPartidasDelegate
- (void)muestraPopoverParaGrafica:(GraficaPartidas *)grafica dePunto:(NSIndexPath *)indexPath enPosicion:(CGPoint)punto conValor:(NSString*) valor {
    if(self.popOver) {
        [self.popOver dismissPopoverAnimated:NO];
    }
    
    InfoSesionViewController* vc = [[InfoSesionViewController alloc] initWithValor:valor];
    
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:vc];
    self.popOver.popoverBackgroundViewClass = [CustomPopOverBackgroundView class];
    [self.popOver presentPopoverFromRect:CGRectMake(punto.x-4, punto.y-4, 8, 8) inView:grafica permittedArrowDirections:UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp animated:YES];
    
}

@end
