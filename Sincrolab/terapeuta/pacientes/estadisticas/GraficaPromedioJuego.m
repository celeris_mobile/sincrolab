//
//  GraficaPromedioJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaPromedioJuego.h"
#import "GraficaTarta.h"
#import "UIColor+Extensions.h"

@interface GraficaPromedioJuego()
@property (nonatomic, assign) TipoJuego tipo;
@property (nonatomic, strong) UILabel* titulo;
@property (nonatomic, strong) GraficaTarta* grafica;
@property (nonatomic, strong) UIView* etiquetas;
@property (nonatomic, strong) NSArray* valores;
@property (nonatomic, strong) NSArray* otrosValores;
@property (nonatomic, strong) UIView* otrosView;
@property (nonatomic, assign) float total;
@end

@implementation GraficaPromedioJuego

- (id)initWithJuego:(TipoJuego)tipo {
    self = [super initWithFrame:CGRectMake(0, 0, 194, 252)];
    if (self) {
        self.tipo = tipo;
        [self configura];
    }
    return self;
}

- (void) configura {
    self.titulo = [[UILabel alloc] initWithFrame:CGRectMake(12, 14, 165, 16)];
    self.titulo.text = [Constantes nombreDeJuego:self.tipo];
    self.titulo.textColor = [UIColor colorGrafica:self.tipo conAlpha:1.0];
    self.titulo.font = [UIFont fontWithName:@"Lato-Bold" size:12];
    [self addSubview:self.titulo];
    
    self.grafica = [[GraficaTarta alloc] initWithFrame:CGRectMake(14, 52, 96, 96)];
    self.grafica.dataSource = self;
    self.grafica.delegate = self;
    [self addSubview:self.grafica];
}

- (void)setDatasource:(id<GraficaPromedioJuegoDatasource>)datasource {
    _datasource = datasource;
    
    self.valores = [self.datasource valoresParaJuego:self.tipo];
    self.total = 0;
    for (NSDictionary* valor in self.valores) {
        self.total += ((NSNumber *)[valor objectForKey:@"cantidad"]).intValue;
    }

    [self.grafica reloadData];
    [self dibujaEtiquetas];
    
    self.otrosValores = [self.datasource otrosValores:self.tipo];
    [self dibujaOtrosValores];
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* vista in self.etiquetas.subviews) {
            [vista removeFromSuperview];
        }
        
        [self.etiquetas removeFromSuperview];
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:self.etiquetas];
    
    for (int e = 0; e<[self numeroDeValoresEnGraficaDeTarta:self.grafica]; e++) {
        NSString* etiqueta = [self graficaDeTarta:self.grafica etiquetaParaIndice:e];
        UIColor* color = [self colorDeSet:e];
        
        UIView* cuadrito = [[UIView alloc] initWithFrame:CGRectMake(120, 80+e*16, 12, 12)];
        cuadrito.backgroundColor = color;
        [self.etiquetas addSubview:cuadrito];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(135, 80+e*16+2, 67, 9)];
        label.textColor = [UIColor azulOscuro];        
        label.font = [UIFont fontWithName:@"Lato-Bold" size:11.0];
        label.text = etiqueta;
        [self.etiquetas addSubview:label];
    }
}

- (void) dibujaOtrosValores {
    if(self.otrosView) {
        for (UIView* valorView in self.otrosView.subviews) {
            [valorView removeFromSuperview];
        }
        
        [self.otrosView removeFromSuperview];
        self.otrosView = nil;
    }
        
    self.otrosView = [[UIView alloc] init];
    [self addSubview:self.otrosView];
    
    int v = 0;
    for (NSDictionary* valor in self.otrosValores) {
        NSDictionary* atributosLabel = @{
                                         NSForegroundColorAttributeName:[UIColor azulOscuro],
                                         NSFontAttributeName:[UIFont fontWithName:@"Lato-Bold" size:12.0]
                                         };
        NSDictionary* atributosValor = @{
                                         NSForegroundColorAttributeName:[UIColor colorWithRed:255/255.0 green:101/255.0 blue:62/255.0 alpha:1.0],
                                         NSFontAttributeName:[UIFont fontWithName:@"Lato-Bold" size:12.0]
                                         };
        

        NSMutableAttributedString* labelStr = [[NSMutableAttributedString alloc] initWithString:[valor valueForKey:@"grupo"] attributes:atributosLabel];
        NSAttributedString* valorStr = [[NSAttributedString alloc] initWithString:[valor valueForKey:@"cantidad"] attributes:atributosValor];
        [labelStr appendAttributedString:valorStr];
        
        UILabel* labelOtroValor = [[UILabel alloc] initWithFrame:CGRectMake(13, 176+v*17, 175, 15)];
        labelOtroValor.attributedText = labelStr;
        [self.otrosView addSubview:labelOtroValor];

        v++;
    }
}

- (UIColor *) colorDeSet:(int) set {
    UIColor * color;
    switch (set) {
        case 0://Aciertos
            color = [UIColor colorWithRed:122/255.0 green:203/255.0 blue:161/255.0 alpha:1.0];
            break;
        case 1://Comisiones
            color = [UIColor colorWithRed:227/255.0 green:7/255.0 blue:32/255.0 alpha:1.0];
            break;
        case 2://Omisiones
            color = [UIColor colorWithRed:255/255.0 green:106/255.0 blue:68/255.0 alpha:1.0];
            break;
        case 3://Perseverativos
            color = [UIColor colorWithRed:249/255.0 green:195/255.0 blue:29/255.0 alpha:1.0];
            break;
    }
    
    return color;
}

#pragma mark GraficaTarta Delegate
- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context {

    UIColor* color = [self colorDeSet:numSet];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
}

#pragma mark GraficaTarta DataSource
- (int)numeroDeValoresEnGraficaDeTarta:(GraficaTarta *)grafica {
    return self.valores.count;
}

- (NSNumber *)graficaDeTarta:(GraficaTarta *)grafica valorParaIndice:(NSInteger)indice {
    NSDictionary* valor = [self.valores objectAtIndex:indice];
    NSInteger cantidad = ((NSNumber *)[valor objectForKey:@"cantidad"]).intValue;
    NSNumber* percent = [NSNumber numberWithFloat:100*((float)cantidad)/self.total];

    return percent;
}

- (NSString *)graficaDeTarta:(GraficaTarta *)grafica etiquetaParaIndice:(NSInteger)indice {
    NSDictionary* valor = [self.valores objectAtIndex:indice];
    
    return [valor objectForKey:@"grupo"];
}

@end
