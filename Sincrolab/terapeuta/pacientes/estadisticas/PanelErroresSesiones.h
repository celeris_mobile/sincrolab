//
//  PanelErroresSesiones.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelSesionesJuego.h"
#import "GraficaLineas.h"
#import "Constantes.h"

@interface PanelErroresSesiones : PanelSesionesJuego <GraficaLineasDatasource>

@end
