//
//  PanelPromedioDia.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelPromedioDia.h"
#import "UIColor+Extensions.h"
#import "DateUtils.h"

@interface PanelPromedioDia()
@property (nonatomic, strong) GraficaTarta* grafica;
@property (nonatomic, strong) NSArray* valores;
@property (nonatomic, strong) UIView* etiquetas;
@property (nonatomic, strong) NSArray* valoresExtra;
@end

@implementation PanelPromedioDia
@synthesize dia = _dia;

- (CGFloat)altura {
    return 210.0;
}

- (void)configura {
    UILabel* tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 250, 14)];
    tituloLabel.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    tituloLabel.text = @"Promedio del día";
    tituloLabel.textColor = [UIColor azulOscuro];
    [self addSubview:tituloLabel];
    
    self.grafica = [[GraficaTarta alloc] initWithFrame:CGRectMake(23, 60, 114, 114)];
    self.grafica.dataSource = self;
    self.grafica.delegate = self;
    [self addSubview:self.grafica];
    
    //GONGS: duración, número de sesiones, nback, modalidad
}

- (void)setDia:(DiaEntrenamiento *)dia {
    _dia = dia;

    NSArray* sesiones;
    switch (self.tipo) {
        case TipoJuegoGongs: {
            sesiones = self.dia.partidaGongs.sesiones;
            
            NSNumber* duracion = [sesiones valueForKeyPath:@"@sum.duracion"];
            NSNumber* numsesiones = [NSNumber numberWithInt:sesiones.count];
            NSNumber* nback = [[sesiones objectAtIndex:0] valueForKeyPath:@"nback"];
            NSNumber* modalidad = [[sesiones objectAtIndex:0] valueForKeyPath:@"modalidad"];
            
            NSArray* modalidades = @[@"Visual A", @"Visual B", @"Auditivo A", @"Auditivo B", @"Visual/Auditivo", @"Dual Visual/Auditivo", @"Dual Visual", @"Dual Auditivo", @"Triple VVA", @"Triple AAV"];
            
            self.valoresExtra = @[
                                  @{@"etiqueta":@"Duración  ",@"valor":[DateUtils segundosAString:duracion.intValue]},
                                  @{@"etiqueta":@"Nº Sesiones  ",@"valor":numsesiones.stringValue},
                                  @{@"etiqueta":@"NBACK  ",@"valor":nback?nback.stringValue:@"0"},
                                  @{@"etiqueta":@"Modalidad  ",@"valor":modalidad?[modalidades objectAtIndex:modalidad.intValue]:@"-"}
                                  ];
            break;
        }
        case TipoJuegoInvasion: {
            sesiones = self.dia.partidaInvasion.sesiones;
            
            NSNumber* duracion = [sesiones valueForKeyPath:@"@sum.duracion"];
            NSNumber* numsesiones = [NSNumber numberWithInt:sesiones.count];
            NSString* tipo = [[sesiones objectAtIndex:0] valueForKeyPath:@"nombre_hito"];
            
            self.valoresExtra = @[
                                  @{@"etiqueta":@"Duración  ",@"valor":[DateUtils segundosAString:duracion.intValue]},
                                  @{@"etiqueta":@"Nº Sesiones  ",@"valor":numsesiones.stringValue},
                                  @{@"etiqueta":@"Tipo  ",@"valor":tipo?tipo:@"-"}
                                  ];
            
            break;
        }
        case TipoJuegoPoker:
            //HAY SOLO UNA SESIÓN DE POKER
            sesiones = self.dia.partidaPoker.sesiones;
            
            NSNumber* duracion = [sesiones valueForKeyPath:@"@sum.duracion"];

            
            self.valoresExtra = @[
                                  @{@"etiqueta":@"Duración  ",@"valor":[DateUtils segundosAString:duracion.intValue]},
                                  ];
            
            break;
    }
    
    NSMutableArray * auxValores = [[NSMutableArray alloc] init];
    NSNumber* aciertos = [sesiones valueForKeyPath:@"@avg.porcentajeAciertos"];
    NSNumber* comisiones = [sesiones valueForKeyPath:@"@avg.porcentajeComisiones"];
    NSNumber* omisiones = [sesiones valueForKeyPath:@"@avg.porcentajeOmisiones"];
    [auxValores addObject:aciertos];
    [auxValores addObject:comisiones];
    [auxValores addObject:omisiones];
    
    self.valores = auxValores.copy;
    


    [self.grafica reloadData];
    [self dibujaEtiquetas];
    [self dibujaExtras];
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* vista in self.etiquetas.subviews) {
            [vista removeFromSuperview];
        }
        
        [self.etiquetas removeFromSuperview];
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:self.etiquetas];
    
    for (int e = 0; e<[self numeroDeValoresEnGraficaDeTarta:self.grafica]; e++) {
        NSString* etiqueta = [self graficaDeTarta:self.grafica etiquetaParaIndice:e];
        UIColor* color = [self colorDeSet:e];
        
        UIView* cuadrito = [[UIView alloc] initWithFrame:CGRectMake(160, 82+e*16, 12, 12)];
        cuadrito.backgroundColor = color;
        [self.etiquetas addSubview:cuadrito];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(178, 82+e*16+2, 67, 9)];
        label.font = [UIFont fontWithName:@"Lato-Bold" size:11.0];
        label.textColor = [UIColor azulOscuro];
        label.text = etiqueta;
        [self.etiquetas addSubview:label];
    }
}

- (void) dibujaExtras {
    
    NSDictionary *attrEtiqueta = @{
                                 NSFontAttributeName: [UIFont fontWithName:@"Lato-Bold" size:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]
                                 };
    NSDictionary *attrValor = @{
                                   NSFontAttributeName: [UIFont fontWithName:@"Lato-Bold" size:14.0],
                                   NSForegroundColorAttributeName: [UIColor redColor]
                                   };
    
    
    int l = 0;
    for (NSDictionary* dict in self.valoresExtra) {
        NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[dict valueForKey:@"etiqueta"] attributes:attrEtiqueta];
        NSAttributedString* strValor = [[NSAttributedString alloc] initWithString:[dict valueForKey:@"valor"] attributes:attrValor];
        [attrString appendAttributedString:strValor];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(380, 60+l*20, 200, 20)];
        label.attributedText = attrString;
        [self addSubview:label];
        l+=1;
    }
}

- (UIColor *) colorDeSet:(int) set {
    UIColor * color;
    switch (set) {
        case 0://Aciertos
            color = [UIColor colorWithRed:122/255.0 green:203/255.0 blue:161/255.0 alpha:1.0];
            break;
        case 1://Comisiones
            color = [UIColor colorWithRed:227/255.0 green:7/255.0 blue:32/255.0 alpha:1.0];
            break;
        case 2://Omisiones
            color = [UIColor colorWithRed:255/255.0 green:106/255.0 blue:68/255.0 alpha:1.0];
            break;
        case 3://Perseverativos
            color = [UIColor colorWithRed:249/255.0 green:195/255.0 blue:29/255.0 alpha:1.0];
            break;
    }
    
    return color;
}

#pragma mark GraficaTartaDelegate
- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context {
    CGContextSetFillColorWithColor(context, [self colorDeSet:numSet].CGColor);
}

#pragma mark GraficaTartaDatasource
- (int) numeroDeValoresEnGraficaDeTarta:(GraficaTarta *) grafica {
    return self.valores.count;
}

- (NSNumber *) graficaDeTarta:(GraficaTarta *) grafica valorParaIndice:(NSInteger) indice {
    return [self.valores objectAtIndex:indice];
}

- (NSString *) graficaDeTarta:(GraficaTarta *) grafica etiquetaParaIndice:(NSInteger) indice {
    switch (indice) {
        case 0:
            return @"Aciertos";
            break;
        case 1:
            return @"Comisiones";
            break;
        case 2:
            return @"Omisiones";
            break;
            
    }
    
    return @"";
}

@end
