//
//  AciertosJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelAciertosSesiones.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "GraficaPartidas.h"

@interface PanelAciertosSesiones()
@property (nonatomic, strong) GraficaPartidas* graficaAciertos;
@property (nonatomic, strong) GraficaPartidas* graficaTiempos;


@property (nonatomic, strong) NSArray* paciertos;
@property (nonatomic, strong) NSArray* tiemposMedios;
@property (nonatomic, strong) NSArray* etiquetas;
@end

@implementation PanelAciertosSesiones
@synthesize dia = _dia;

- (CGFloat)altura {
    return 540;
}

- (void) configura {

    
    self.graficaAciertos = [[GraficaPartidas alloc] initWithTitulo:@"Aciertos" colores:@[[UIColor colorWithRed:255/255.0 green:202/255.0 blue:68/255.0 alpha:1.0]]];
    self.graficaAciertos.grafica.dataSource = self;
    self.graficaAciertos.delegate = self;
    self.graficaAciertos.grafica.minY = 0.0;
    self.graficaAciertos.grafica.maxY = 100.0;
    self.graficaAciertos.grafica.prefijoEtiquetasY = @"%";
    self.graficaAciertos.grafica.saltoY = 25.0;
    [self addSubview:self.graficaAciertos];
    
    self.graficaTiempos = [[GraficaPartidas alloc] initWithTitulo:@"Tiempo de Respuesta Aciertos" colores:@[[UIColor colorWithRed:149/255.0 green:189/255.0 blue:13/255.0 alpha:1.0]]];
    self.graficaTiempos.grafica.dataSource = self;
    self.graficaTiempos.delegate = self;
    self.graficaTiempos.grafica.prefijoEtiquetasY = @"s";
    self.graficaTiempos.grafica.minY  =0.0;
    
    
    CGRect frameGrafica = self.graficaTiempos.frame;
    frameGrafica.origin.y = 266;
    self.graficaTiempos.frame = frameGrafica;
    
    [self addSubview:self.graficaTiempos];
    
}


- (void)setDia:(DiaEntrenamiento *)dia  {
    _dia = dia;
    
    id partida;
    switch (self.tipo) {
        case TipoJuegoGongs:
            partida = dia.partidaGongs;
            break;
        case TipoJuegoInvasion:
            partida = dia.partidaInvasion;
            break;
        case TipoJuegoPoker:
            partida = dia.partidaPoker;
            break;
    }
    NSArray* sesiones = (NSArray *)[partida valueForKey:@"sesiones"];
    
    if(!_dia || !partida || ! sesiones.count) {
        //TODO mensaje de que no hay datos
        self.graficaAciertos.hidden = YES;
        self.graficaTiempos.hidden = YES;
        return;
    }
    
    self.graficaAciertos.hidden = NO;
    self.graficaTiempos.hidden = NO;
    

    self.paciertos = [sesiones valueForKey:@"porcentajeAciertos"];
    self.tiemposMedios = [sesiones valueForKey:@"tiempoMedioDeRespuestaAciertos"];
    
    NSMutableArray* auxTiempos = [[NSMutableArray alloc] init];
    for (NSNumber* tiempo in self.tiemposMedios) {
        [auxTiempos addObject:[NSNumber numberWithInt:(int)(tiempo.floatValue*1000)]];
    }
    self.tiemposMedios = auxTiempos.copy;

    NSExpression *expression = [NSExpression expressionForFunction:@"stddev:" arguments:@[[NSExpression expressionForConstantValue:self.tiemposMedios]]];
    NSNumber* desviacionTipica = [expression expressionValueWithObject:nil context:nil];
    NSArray* nombres = @[@"Desviación Típica", @"Media"];
    NSArray* valores = @[desviacionTipica, [self.tiemposMedios valueForKeyPath:@"@avg.self"]];
    
    [self generaExtras:nombres conValores:valores enVista:self.graficaTiempos];
    
    NSMutableArray* auxetiquetas = [[NSMutableArray alloc] init];
    for (int s = 0; s<self.paciertos.count; s++) {
        [auxetiquetas addObject:[NSString stringWithFormat:@"S%d", s+1]];
    }
    self.etiquetas = auxetiquetas.copy;
    
    [self.graficaAciertos.grafica dibuja];
    [self.graficaTiempos.grafica dibuja];
}



#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(grafica==self.graficaAciertos.grafica)
        return self.paciertos;
    else
        return self.tiemposMedios;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.etiquetas;
}


@end
