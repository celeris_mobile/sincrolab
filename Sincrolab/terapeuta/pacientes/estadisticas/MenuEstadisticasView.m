//
//  MenuEstadisticasView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "MenuEstadisticasView.h"
#import "UIColor+Extensions.h"
#import "ImageUtils.h"


@interface MenuEstadisticasView()
@property (nonatomic, strong) UIButton* botonGenerales;
@property (nonatomic, strong) UIButton* botonGongs;
@property (nonatomic, strong) UIButton* botonInvasion;
@property (nonatomic, strong) UIButton* botonPoker;

@property (nonatomic, strong) UIButton* seleccionado;
@end

@implementation MenuEstadisticasView

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 647, 47)];
    if (self) {
        [self configura];
    }
    return self;
}

- (void) configura {
    self.backgroundColor = [UIColor azulOscuro];
    
    UIView* viewNormal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 144, 47)];
    viewNormal.backgroundColor = [UIColor azulOscuro];
    UIImage* imgNormal = [ImageUtils imageWithView:viewNormal];
    
    UIView* barrita = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 144, 7)];
    barrita.backgroundColor = [UIColor verdeAzulado];
    [viewNormal addSubview:barrita];
    UIImage* imgSeleccionado  = [ImageUtils imageWithView:viewNormal];
    
    self.botonGenerales = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, 144, 47)];
    self.botonGenerales.titleLabel.font = [UIFont fontWithName:@"Lato-Black" size:11];
    [self.botonGenerales setTitleColor:[UIColor verdeAzulado] forState:UIControlStateSelected];
    [self.botonGenerales setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.botonGenerales setBackgroundImage:imgSeleccionado forState:UIControlStateSelected];
    [self.botonGenerales setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [self.botonGenerales setTitle:@"GLOBALES" forState:UIControlStateNormal];
    [self.botonGenerales addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonGenerales];
    
    self.botonGongs = [[UIButton alloc] initWithFrame:CGRectMake(249, 0, 124, 47)];
    self.botonGongs.titleLabel.font = [UIFont fontWithName:@"Lato-Black" size:11];
    [self.botonGongs setTitleColor:[UIColor verdeAzulado] forState:UIControlStateSelected];
    [self.botonGongs setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.botonGongs setBackgroundImage:imgSeleccionado forState:UIControlStateSelected];
    [self.botonGongs setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [self.botonGongs setTitle:@"TOGONG!!" forState:UIControlStateNormal];
    [self.botonGongs addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonGongs];
    
    
    self.botonInvasion = [[UIButton alloc] initWithFrame:CGRectMake(373, 0, 124, 47)];
    self.botonInvasion.titleLabel.font = [UIFont fontWithName:@"Lato-Black" size:11];
    [self.botonInvasion setTitleColor:[UIColor verdeAzulado] forState:UIControlStateSelected];
    [self.botonInvasion setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.botonInvasion setBackgroundImage:imgSeleccionado forState:UIControlStateSelected];
    [self.botonInvasion setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [self.botonInvasion setTitle:@"INVASIÓN SAMURAI" forState:UIControlStateNormal];
    [self.botonInvasion addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonInvasion];
    
    
    self.botonPoker = [[UIButton alloc] initWithFrame:CGRectMake(497, 0, 124, 47)];
    self.botonPoker.titleLabel.font = [UIFont fontWithName:@"Lato-Black" size:11];
    [self.botonPoker setTitleColor:[UIColor verdeAzulado] forState:UIControlStateSelected];
    [self.botonPoker setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.botonPoker setBackgroundImage:imgSeleccionado forState:UIControlStateSelected];
    [self.botonPoker setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [self.botonPoker setTitle:@"POKER SAMURAI" forState:UIControlStateNormal];
    [self.botonPoker addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonPoker];
    
    [self botonDado:self.botonGenerales];
}

- (void) botonDado:(UIButton *) boton {
    if(boton.selected)
        return;
    
    if(self.seleccionado)
        self.seleccionado.selected = NO;
    
    self.seleccionado = boton;
    self.seleccionado.selected = YES;

    TipoEstadisticas tipo;
    if(boton==self.botonGenerales)
        tipo = TipoEstadisticasGenerales;
    if(boton==self.botonGongs)
        tipo = TipoEstadisticasGongs;
    if(boton==self.botonInvasion)
        tipo = TipoEstadisticasInvasion;
    if(boton==self.botonPoker)
        tipo = TipoEstadisticasPoker;


    [self.delegate cargaEstadisticas:tipo];
}


@end
