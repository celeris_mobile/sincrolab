//
//  InfoSesionViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"
#import "DiaEntrenamiento.h"

@interface InfoSesionViewController : UIViewController
- (id) initWithValor:(NSString *) valor;
@end
