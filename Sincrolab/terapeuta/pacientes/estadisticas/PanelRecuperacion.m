//
//  PanelRecuperacion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelRecuperacion.h"
#import "GraficaPartidas.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"

@interface PanelRecuperacion()
@property (nonatomic, strong) GraficaPartidas* grafica;
@property (nonatomic, strong) NSArray* recuperacion;
@property (nonatomic, strong) NSArray* fechas;
@end

@implementation PanelRecuperacion

- (CGFloat)altura {
    return 270;
}

- (void) configura {
    self.grafica = [[GraficaPartidas alloc] initWithTitulo:@"Recuperación" colores:@[[UIColor colorWithRed:23/255.0 green:181/255.0 blue:212/255.0 alpha:1.0]]];
    self.grafica.delegate = self;
    self.grafica.grafica.minY = 0.0;
    self.grafica.grafica.maxY = 100.0;
    self.grafica.grafica.saltoY = 25.0;
    self.grafica.grafica.prefijoEtiquetasY = @"%";
    [self addSubview:self.grafica];
}

- (void)setDatasource:(id<PanelEstadisticasDatasource>)datasource {
    [super setDatasource:datasource];
    

    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        self.grafica.hidden = YES;
        self.grafica.grafica.dataSource = nil;
        return;
    }
    
    
    self.grafica.grafica.dataSource = self;
    self.grafica.hidden = NO;
        
    NSMutableArray* auxRecuperacion = [[NSMutableArray alloc] init];
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    NSMutableArray* auxRecuperacionMedia = [[NSMutableArray alloc] init];
    
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* recup = [partida valueForKeyPath:@"sesiones.@avg.porcentajeRecuperacion"];
            [auxRecuperacion addObject:recup];
            
            NSNumber* recupMdia = [partida valueForKeyPath:@"sesiones.@avg.recuperacionMedia"];
            [auxRecuperacionMedia addObject:[NSNumber numberWithInt:(int)(recupMdia.floatValue*100)]];
        } else {
            [auxRecuperacion addObject:[NSNull null]];
        }
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
        
    }
    
    self.recuperacion = auxRecuperacion.copy;
    self.fechas = auxFechas.copy;
    
    NSArray* nombres = @[@"Recuperación Media"];
    NSArray* valores = @[[auxRecuperacionMedia valueForKeyPath:@"@avg.self"]];    
    [self generaExtras:nombres conValores:valores enVista:self.grafica];
    
    [self.grafica.grafica dibuja];
}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    return self.recuperacion;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}



@end
