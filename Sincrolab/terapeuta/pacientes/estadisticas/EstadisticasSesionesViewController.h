//
//  EstadisticasSesionesViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaEntrenamiento.h"
#import "Constantes.h"

@interface EstadisticasSesionesViewController : UIViewController
- (id) initWithDia:(DiaEntrenamiento *) dia tipo:(TipoJuego) tipo;
@end
