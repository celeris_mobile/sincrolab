//
//  PanelPromedio.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelPromedio.h"
#import "GraficaPromedioJuego.h"
#import "UIColor+Extensions.h"

@interface PanelPromedio()
@property (nonatomic, strong) GraficaPromedioJuego* graficaGongs;
@property (nonatomic, strong) GraficaPromedioJuego* graficaInvasion;
@property (nonatomic, strong) GraficaPromedioJuego* graficaPoker;
@end

@implementation PanelPromedio
@synthesize datasource = _datasource;

- (CGFloat)altura {
    return 279;
}

- (void) configura {
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 150, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Promedio";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    self.graficaGongs = [[GraficaPromedioJuego alloc] initWithJuego:TipoJuegoGongs];
    CGRect gongsFrame = self.graficaGongs.frame;
    gongsFrame.origin = CGPointMake(8, 31);
    self.graficaGongs.frame = gongsFrame;
    [self addSubview:self.graficaGongs];
    
    self.graficaInvasion = [[GraficaPromedioJuego alloc] initWithJuego:TipoJuegoInvasion];
    CGRect invasionFrame = self.graficaInvasion.frame;
    invasionFrame.origin = CGPointMake(205, 31);
    self.graficaInvasion.frame = invasionFrame;
    [self addSubview:self.graficaInvasion];
    
    self.graficaPoker = [[GraficaPromedioJuego alloc] initWithJuego:TipoJuegoPoker];
    CGRect pokerFrame = self.graficaPoker.frame;
    pokerFrame.origin = CGPointMake(405, 31);
    self.graficaPoker.frame = pokerFrame;
    [self addSubview:self.graficaPoker];
    
    UIView* separador1 = [[UIView alloc] initWithFrame:CGRectMake(0, 66, self.bounds.size.width, 1)];
    separador1.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separador1];
    
    UIView* separador2 = [[UIView alloc] initWithFrame:CGRectMake(0, 194, self.bounds.size.width, 1)];
    separador2.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separador2];
    
    UIView* separadorj1 = [[UIView alloc] initWithFrame:CGRectMake(203, 76, 1, 107)];
    separadorj1.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separadorj1];
    
    UIView* separadorj2 = [[UIView alloc] initWithFrame:CGRectMake(402, 76, 1, 107)];
    separadorj2.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separadorj2];
    
    UIView* separadoro1 = [[UIView alloc] initWithFrame:CGRectMake(203, 205, 1, 64)];
    separadoro1.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separadoro1];
    
    UIView* separadoro2 = [[UIView alloc] initWithFrame:CGRectMake(402, 205, 1, 64)];
    separadoro2.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separadoro2];
    
}

- (void)setDatasource:(id<PanelEstadisticasDatasource>)datasource {
    _datasource = datasource;
    
    BOOL hayValores = YES;
    if(!self.datasource.diasDeEntrenamientoEntreFechas.count)
        hayValores = NO;
    
    if(hayValores) {
        NSArray* sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"]];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"]];
        
        if(!sesiones.count)
            hayValores = NO;
    }
    
    if(!hayValores) {
        self.graficaGongs.hidden = YES;
        self.graficaInvasion.hidden = YES;
        self.graficaPoker.hidden = YES;
        
        self.graficaGongs.datasource = nil;
        self.graficaInvasion.datasource = nil;
        self.graficaPoker.datasource = nil;
        
        return;
    }
    
    
    self.graficaGongs.datasource = self;
    self.graficaInvasion.datasource = self;
    self.graficaPoker.datasource = self;
    
    self.graficaGongs.hidden = NO;
    self.graficaInvasion.hidden = NO;
    self.graficaPoker.hidden = NO;
    
}

- (NSArray *) valoresParaJuego:(TipoJuego) tipoJuego {
    NSNumber* pComisiones;
    NSNumber* pOmisiones;
    NSNumber* pAciertos;
    NSArray* sesiones;
    switch (tipoJuego) {
        case TipoJuegoGongs: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
            break;
        }
        case TipoJuegoInvasion: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"];
            break;
        }
        case TipoJuegoPoker: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"];
            break;
        }
    }
    
    
    pAciertos = [sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
    pOmisiones = [sesiones valueForKeyPath:@"@sum.numeroDeOmisiones"];
    pComisiones = [sesiones valueForKeyPath:@"@sum.numeroDeComisiones"];
    
    return @[@{@"grupo":@"Aciertos", @"cantidad":pAciertos}, @{@"grupo":@"Comisiones", @"cantidad":pComisiones}, @{@"grupo":@"Omisiones", @"cantidad":pOmisiones}];
}

- (NSArray *)otrosValores:(TipoJuego)tipoJuego {
    NSArray* sesiones;
    
    switch (tipoJuego) {
        case TipoJuegoGongs: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];

            break;
        }
        case TipoJuegoInvasion: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"];

            break;
        }
        case TipoJuegoPoker: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"];

            break;
        }
    }
    
    NSNumber* tRespMedio = [sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
    NSString* tRespMedioStr = [NSString stringWithFormat:@"  %d ms", (int)(tRespMedio.floatValue*1000)];
    
    
    NSArray *numbers = [sesiones valueForKeyPath:@"tiempoMedioDeRespuestaAciertos"];
    NSExpression *expression = [NSExpression expressionForFunction:@"stddev:" arguments:@[[NSExpression expressionForConstantValue:numbers]]];
    NSNumber* desvTipica = [expression expressionValueWithObject:nil context:nil];
    
    
    NSString* desvTipicaStr = [NSString stringWithFormat:@" %.02f", desvTipica.floatValue*1000];
    return @[@{@"grupo":@"TR Medio", @"cantidad":tRespMedioStr}, @{@"grupo":@"Desviación Típica", @"cantidad":desvTipicaStr}];
}



@end
