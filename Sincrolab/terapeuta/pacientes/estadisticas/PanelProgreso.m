//
//  PanelProgreso.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelProgreso.h"
#import "GraficaLineas.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "Constantes.h"

@interface PanelProgreso()
@property (nonatomic, strong) GraficaLineas* grafica;
@property (nonatomic, strong) UIButton* botonGongs;
@property (nonatomic, strong) UIButton* botonInvasion;
@property (nonatomic, strong) UIButton* botonPoker;
@end

@implementation PanelProgreso
@synthesize datasource = _datasource;

- (CGFloat)altura {
    return 225;
}

- (void) configura {
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(21, 14, 150, 14)];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:13];
    titulo.text = @"Progreso";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    self.grafica = [[GraficaLineas alloc] initWithFrame:CGRectMake(30, 55, 560, 158)];
    self.grafica.ejes = GFBordesWidthMake(0, 0.5, 0, 0);
    self.grafica.dibujaLineasY = YES;
    self.grafica.dibujaLineasX = NO;
    self.grafica.dibujaEtiquetasY = YES;
    [self addSubview:self.grafica];
    
    self.botonGongs = [[UIButton alloc] initWithFrame:CGRectMake(212, 16, 119, 22)];
    self.botonGongs.tag = 0;
    [self.botonGongs setTitle:[Constantes nombreDeJuego:TipoJuegoGongs] forState:UIControlStateNormal];
    [self.botonGongs setBackgroundColor:[UIColor colorGrafica:0 conAlpha:1.0]];
    [self.botonGongs.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:12.0]];
    [self.botonGongs addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    self.botonGongs.selected = YES;
    [self addSubview:self.botonGongs];
    
    self.botonInvasion = [[UIButton alloc] initWithFrame:CGRectMake(334, 16, 119, 22)];
    self.botonInvasion.tag = 1;
    [self.botonInvasion setTitle:[Constantes nombreDeJuego:TipoJuegoInvasion] forState:UIControlStateNormal];
    [self.botonInvasion setBackgroundColor:[UIColor colorGrafica:1 conAlpha:1.0]];
    [self.botonInvasion.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:12.0]];
    [self.botonInvasion addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    self.botonInvasion.selected = YES;
    [self addSubview:self.botonInvasion];
    
    self.botonPoker = [[UIButton alloc] initWithFrame:CGRectMake(456, 16, 119, 22)];
    self.botonPoker.tag = 2;
    [self.botonPoker setTitle:[Constantes nombreDeJuego:TipoJuegoPoker] forState:UIControlStateNormal];
    [self.botonPoker setBackgroundColor:[UIColor colorGrafica:2 conAlpha:1.0]];
    [self.botonPoker.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:12.0]];
    [self.botonPoker addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    self.botonPoker.selected = YES;
    [self addSubview:self.botonPoker];
}

- (void) botonDado:(UIButton *) boton {
    boton.selected = !boton.selected;
    [boton setBackgroundColor:[UIColor colorGrafica:boton.tag conAlpha:boton.selected?1.0:0.5]];
    [self.grafica dibuja];
}

- (void)setDatasource:(id<PanelEstadisticasDatasource>)datasource {
    _datasource = datasource;
    
    BOOL hayValores = YES;
    if(!self.datasource.diasDeEntrenamientoEntreFechas.count)
        hayValores = NO;
    
    if(hayValores) {
        NSArray* sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"]];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"]];
        
        if(!sesiones.count)
            hayValores = NO;
    }
    
    if(!hayValores) {
        self.grafica.hidden = YES;
        self.grafica.dataSource = nil;
        self.grafica.delegate = nil;
        
        return;
    }
    
    
    self.grafica.hidden = NO;
    self.grafica.delegate = self;
    self.grafica.dataSource = self;
    
    [self.grafica dibuja];
}

#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    if(!self.datasource.diasDeEntrenamientoEntreFechas)
        return 0;
    return 3;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {

    NSNumber* nivelAnterior = [NSNumber numberWithInt:0]; 
    NSMutableArray* array = [[NSMutableArray alloc] init];
    //for (DiaEntrenamiento* dia in self.modelo.entrenamiento.diasDeEntrenamiento) {
    for (DiaEntrenamiento* dia in self.datasource.diasDeEntrenamientoEntreFechas) {
        PFObject* partida;
        switch (numSet) {
            case 0:
                partida = dia.partidaGongs;
                break;
            case 1:
                partida = dia.partidaInvasion;
                break;
            case 2:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
//            [array addObject:[partida valueForKey:@"cambioNivel"]];
            NSNumber* cambioNivel = [partida valueForKey:@"cambioNivel"];
            [array addObject:[NSNumber numberWithInt:cambioNivel.intValue+nivelAnterior.intValue]];
            nivelAnterior = cambioNivel;

        } else {
            [array addObject:[NSNull null]];
        }
    }
    
    
    /*
    NSMutableArray *valores = [[NSMutableArray alloc] init];
    NSNumber* anterior = [NSNumber numberWithInt:0];
    for (NSNumber* valor in array) {
        if(valor!=(id)[NSNull null]) {
            [valores addObject:[NSNumber numberWithInt:anterior.intValue+valor.intValue]];
            anterior = [NSNumber numberWithInt:anterior.intValue+valor.intValue];
        } else {
            [valores addObject:[NSNull null]];
        }
    }
    */
    return array.copy;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    //for (DiaEntrenamiento* dia in self.modelo.entrenamiento.diasDeEntrenamiento) {
    for (DiaEntrenamiento* dia in self.datasource.diasDeEntrenamientoEntreFechas) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [array addObject:[formatter stringFromDate:dia.fecha]];
    }
    
    return array.copy;
}

#pragma mark GraficaLineasDelegate
- (void)grafica:(GraficaLineas *)grafica configuraSet:(NSInteger)numSet enContexto:(CGContextRef)context {

    UIButton* boton;
    switch (numSet) {
        case 0:
            boton = self.botonGongs;
            break;
        case 1:
            boton = self.botonInvasion;
            break;
        case 2:
            boton = self.botonPoker;
            break;
            
    }

    float alpha = boton.selected?1.0:0.0;
    CGContextSetStrokeColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:1.0*alpha].CGColor);
    CGContextSetLineWidth(context, 3);
    
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
}

- (void)grafica:(GraficaLineas *)grafica dibujaPunto:(CGPoint)punto deSet:(NSInteger) numSet conValor:(NSNumber *)valor enContexto:(CGContextRef)context {
    
    UIButton* boton;
    switch (numSet) {
        case 0:
            boton = self.botonGongs;
            break;
        case 1:
            boton = self.botonInvasion;
            break;
        case 2:
            boton = self.botonPoker;
            break;
            
    }
    
    float alpha = boton.selected?1.0:0.0;
    
    CGFloat radio = 10;
    CGContextAddEllipseInRect(context, CGRectMake(punto.x-radio/2, punto.y-radio/2, radio, radio));
    CGContextSetFillColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:1.0*alpha].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:1.0 alpha:alpha].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextDrawPath(context, kCGPathEOFillStroke);
}

@end
