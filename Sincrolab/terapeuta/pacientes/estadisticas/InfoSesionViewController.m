//
//  InfoSesionViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "InfoSesionViewController.h"

@interface InfoSesionViewController ()
@property (nonatomic, assign) NSString* valor;
@end

@implementation InfoSesionViewController

- (id) initWithValor:(NSString *) valor {
    self = [super init];
    if(self) {
        _valor = valor;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    self.preferredContentSize = CGSizeMake(80, 40);
    self.view.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    UILabel* labelValor = [[UILabel alloc] initWithFrame:CGRectMake(10, 6, 60, 20)];
    labelValor.textAlignment = NSTextAlignmentCenter;
    labelValor.font = [UIFont fontWithName:@"Lato-Black" size:22];
    labelValor.textColor = [UIColor colorWithRed:255/255.0 green:112/255.0 blue:75/255.0 alpha:1.0];
    labelValor.text = self.valor;
    labelValor.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:labelValor];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
