//
//  PanelSesionesJuego.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelEstadisticas.h"
#import "Constantes.h"
#import "DiaEntrenamiento.h"
#import "GraficaPartidas.h"

@interface PanelSesionesJuego : PanelEstadisticas <GraficaPartidasDelegate>
- (id) initWithJuego:(TipoJuego) tipo;
@property (nonatomic, assign) TipoJuego tipo;
@property (nonatomic, assign) DiaEntrenamiento* dia;
@end
