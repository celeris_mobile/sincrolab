//
//  EstaditicasDiasViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PanelEstadisticas.h"
#import "Constantes.h"
#import "MenuEstadisticasView.h"
#import "EntrenamientoPacienteModel.h"


@interface EstadisticasDiasViewController : UIViewController <PanelEstadisticasDelegate, PanelEstadisticasDatasource>
- (void)cargaEstadisticas:(TipoEstadisticas)tipo;
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
