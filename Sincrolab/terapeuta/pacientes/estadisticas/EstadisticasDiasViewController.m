//
//  EstaditicasDiasViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadisticasDiasViewController.h"
#import "SelectorFechas.h"
#import "PanelProgreso.h"
#import "PanelPromedio.h"
#import "PanelProgresoJuego.h"
#import "PanelAciertosJuego.h"
#import "PanelErroresJuego.h"
#import "PanelRecuperacion.h"
#import "EstadisticasSesionesViewController.h"
#import "DateUtils.h"
#import "UIColor+Extensions.h"
#import "PanelPromedioDia.h"

@interface EstadisticasDiasViewController ()
@property (nonatomic, strong) SelectorFechas* selectorFechas;
@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIView* contentView;
@property (nonatomic, strong) NSArray* paneles;
@property (nonatomic, assign) TipoJuego tipoJuego;

@property (nonatomic, strong) UIButton* btnInicio;
@property (nonatomic, strong) UIButton* btnFinal;

@property (nonatomic, strong) NSDate* fechaInicial;
@property (nonatomic, strong) NSDate* fechaFinal;
@property (nonatomic, strong) UIDatePicker* datePicker;
@property (nonatomic, strong) UIPopoverController* popOver;
@end

@implementation EstadisticasDiasViewController
@synthesize modelo = _modelo;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.scrollView addSubview:self.contentView];
    
    
    UIImage *imgFondoBoton = [UIImage imageNamed:@"btnfechabarra.png"];
    
    self.btnInicio = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnInicio.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    [self.btnInicio setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.btnInicio setTitleEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 20)];
    [self.btnInicio setBackgroundImage:imgFondoBoton forState:UIControlStateNormal];
    self.btnInicio.frame = CGRectMake(0.0, 0.0, imgFondoBoton.size.width, imgFondoBoton.size.height);
    [self.btnInicio addTarget:self action:@selector(botonFechaDado:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *inicioButton = [[UIBarButtonItem alloc] initWithCustomView:self.btnInicio];

    self.btnFinal = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnFinal.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    [self.btnFinal setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.btnFinal setTitleEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 20)];
    self.btnFinal.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.btnFinal setBackgroundImage:imgFondoBoton forState:UIControlStateNormal];
    self.btnFinal.frame = CGRectMake(0.0, 0.0, imgFondoBoton.size.width, imgFondoBoton.size.height);
    [self.btnFinal addTarget:self action:@selector(botonFechaDado:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *finalButton = [[UIBarButtonItem alloc] initWithCustomView:self.btnFinal];
    

    self.navigationItem.rightBarButtonItems = @[finalButton, inicioButton];
    

    [self actualizaBotonesFecha];
    
    // Set this in every view controller so that the back button displays back instead of the root view controller name
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Volver" style:UIBarButtonItemStylePlain target:nil action:nil];

}

- (void) botonFechaDado:(UIButton *) button {
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.datePicker addTarget:self action:@selector(cambioPickerDate:) forControlEvents:UIControlEventValueChanged];
    
    UIViewController* vc = [[UIViewController alloc] init];
    vc.preferredContentSize = self.datePicker.frame.size;
    vc.view = self.datePicker;
    
    if(button==self.btnInicio) {
        vc.title = @"Fecha Inicial";
        self.datePicker.tag = 0;
        self.datePicker.date = self.fechaInicial;
    } else {
        vc.title = @"Fecha Final";
        self.datePicker.tag = 1;
        self.datePicker.date = self.fechaFinal;
    }
    
    if(self.popOver)
        [self.popOver dismissPopoverAnimated:YES];
    
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
    //TODO ajustar este popOver en iOS 6
    nc.navigationBar.backgroundColor = [UIColor verdeAzulado];
    nc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:nc];
    
    
    [self.popOver setPopoverContentSize:vc.view.frame.size animated:NO];
    [self.popOver presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
   // [self.popOver presentPopoverFromBarButtonItem:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void) cambioPickerDate:(UIDatePicker *) picker {
    NSDate* fecha = picker.date;
    switch (picker.tag) {
        case 0:
            if([fecha compare:self.fechaFinal]==NSOrderedAscending)
                self.fechaInicial = fecha;
            break;
        case 1:
            if([self.fechaInicial compare:fecha]==NSOrderedAscending)
                self.fechaFinal = fecha;
            break;
    }
    
    [self actualizaBotonesFecha];
    for (PanelEstadisticas* panel in self.paneles) {
        panel.datasource = self;
    }
    
}


- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    
    if(self.modelo.entrenamiento && self.modelo.entrenamiento.diasDeEntrenamiento.count) {
        DiaEntrenamiento* primerDia = [self.modelo.entrenamiento.diasDeEntrenamiento firstObject];
        DiaEntrenamiento* ultimoDia = [self.modelo.entrenamiento.diasDeEntrenamiento lastObject];
        //TODO hacer comprobaciones
        self.fechaInicial = primerDia.fecha;
        self.fechaFinal = ultimoDia.fecha;
        
    } else {
        self.fechaFinal = nil;
        self.fechaInicial = nil;
    }
    
    [self actualizaBotonesFecha];
    
    for (PanelEstadisticas* panel in self.paneles) {
        panel.datasource = self;
    }

}

- (void) actualizaBotonesFecha {
    if(self.fechaInicial && self.fechaFinal) {
        self.btnInicio.enabled = YES;
        self.btnFinal.enabled = YES;
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [self.btnInicio setTitle:[formatter stringFromDate:self.fechaInicial] forState:UIControlStateNormal];
        [self.btnFinal setTitle:[formatter stringFromDate:self.fechaFinal] forState:UIControlStateNormal];
    } else {
        self.btnInicio.enabled = NO;
        self.btnFinal.enabled = NO;
    }
}


- (void) layoutPaneles {
    PanelEstadisticas* panelAnterior;
    for (PanelEstadisticas* panel in self.paneles) {
        CGRect panelFrame = panel.frame;
        if(!panelAnterior)
            panelFrame.origin.y = 20;
        else
            panelFrame.origin.y = panelAnterior.frame.origin.y + [panelAnterior altura] + 20;
        
        panel.frame = panelFrame;
        
        panelAnterior = panel;
    }
    
    self.contentView.frame = CGRectMake(0, 0, self.view.bounds.size.width, panelAnterior.frame.origin.y + panelAnterior.frame.size.height + 20);
    
    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.contentView.frame.size.height)];
}


- (void)cargaEstadisticas:(TipoEstadisticas)tipo {
    if(self.paneles) {
        for (PanelEstadisticas* panel in self.paneles) {
            [panel removeFromSuperview];
        }
        self.paneles = nil;
    }
    
    switch (tipo) {
        case TipoEstadisticasGenerales: {
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgreso* panel = [[PanelProgreso alloc] init];
            panel.datasource = self;
            [paneles addObject:panel];
            [self.contentView addSubview:panel];

            PanelPromedio* panel2 = [[PanelPromedio alloc] init];
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];

            self.paneles = paneles.copy;
            
            break;
        }
            
        case TipoEstadisticasGongs: {
            self.tipoJuego = TipoJuegoGongs;
            
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJuego* panel = [[PanelProgresoJuego alloc] initWithJuego:TipoJuegoGongs];
            panel.delegate = self;
            panel.datasource = self;
            [paneles addObject:panel];
            [self.contentView addSubview:panel];
            
            PanelAciertosJuego* panel2 = [[PanelAciertosJuego alloc] initWithJuego:TipoJuegoGongs];
            panel2.delegate = self;
            panel2.datasource = self;            
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];
            
            PanelRecuperacion* panel3 = [[PanelRecuperacion alloc] initWithJuego:TipoJuegoGongs];
            panel3.delegate = self;
            panel3.datasource = self;
            [paneles addObject:panel3];
            [self.contentView addSubview:panel3];
            
            PanelErroresJuego* panel4 = [[PanelErroresJuego alloc] initWithJuego:TipoJuegoGongs];
            panel4.delegate = self;
            panel4.datasource = self;
            [paneles addObject:panel4];
            [self.contentView addSubview:panel4];
            
            self.paneles = paneles.copy;
            break;
        }
            
        case TipoEstadisticasInvasion: {
            self.tipoJuego = TipoJuegoInvasion;
            
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJuego* panel = [[PanelProgresoJuego alloc] initWithJuego:TipoJuegoInvasion];
            panel.delegate = self;
            panel.datasource = self;
            [paneles addObject:panel];
            [self.contentView addSubview:panel];
            
            PanelAciertosJuego* panel2 = [[PanelAciertosJuego alloc] initWithJuego:TipoJuegoInvasion];
            panel2.delegate = self;
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];
            
            PanelRecuperacion* panel3 = [[PanelRecuperacion alloc] initWithJuego:TipoJuegoInvasion];
            panel3.delegate = self;
            panel3.datasource = self;
            [paneles addObject:panel3];
            [self.contentView addSubview:panel3];
            
            PanelErroresJuego* panel4 = [[PanelErroresJuego alloc] initWithJuego:TipoJuegoInvasion];
            panel4.delegate = self;
            panel4.datasource = self;
            [paneles addObject:panel4];
            [self.contentView addSubview:panel4];
            
            self.paneles = paneles.copy;
            break;
        }
        case TipoEstadisticasPoker: {
            self.tipoJuego = TipoJuegoPoker;
            
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJuego* panel = [[PanelProgresoJuego alloc] initWithJuego:TipoJuegoPoker];
            panel.delegate = self;
            panel.datasource = self;            
            [paneles addObject:panel];
            [self.contentView addSubview:panel];
            
            PanelAciertosJuego* panel2 = [[PanelAciertosJuego alloc] initWithJuego:TipoJuegoPoker];
            panel2.delegate = self;
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.contentView addSubview:panel2];
            
            PanelRecuperacion* panel3 = [[PanelRecuperacion alloc] initWithJuego:TipoJuegoPoker];
            panel3.delegate = self;
            panel3.datasource = self;
            [paneles addObject:panel3];
            [self.contentView addSubview:panel3];
            
            PanelErroresJuego* panel4 = [[PanelErroresJuego alloc] initWithJuego:TipoJuegoPoker];
            panel4.delegate = self;
            panel4.datasource = self;
            [paneles addObject:panel4];
            [self.contentView addSubview:panel4];
            
            self.paneles = paneles.copy;
            break;
        }
    }
    
    
    [self layoutPaneles];
}

#pragma mark PanelEstadisticasDatasource
- (NSArray *) diasDeEntrenamientoEntreFechas {
    if(!self.modelo.entrenamiento || !self.modelo.entrenamiento.diasDeEntrenamiento.count)
        return nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSArray* diasEntrenados = self.modelo.entrenamiento.diasDeEntrenamiento;
    
    NSDateComponents *oneDay = [[NSDateComponents alloc] init];
    [oneDay setDay: 1];
    
    NSMutableArray* auxDias = [[NSMutableArray alloc] init];
    for (NSDate* date = self.fechaInicial; [DateUtils comparaSinHoraFecha:date conFecha:self.fechaFinal] <= 0;
         date = [calendar dateByAddingComponents: oneDay
                                          toDate: date
                                         options: 0] ) {
    
             BOOL encontrado = NO;
             for (DiaEntrenamiento* diaEntrenado in diasEntrenados) {
                 if([DateUtils comparaSinHoraFecha:diaEntrenado.fecha conFecha:date]==NSOrderedSame) {
                     encontrado = YES;
                     [auxDias addObject:diaEntrenado];
                 }
             }
             if(!encontrado) {
                 DiaEntrenamiento* dia = [DiaEntrenamiento object];
                 dia.fecha = date;
                 [auxDias addObject:dia];
             }
    }
    
    return auxDias.copy;
}


#pragma mark PanelEstadisticasDelegate
- (void)muestraDia:(DiaEntrenamiento *)dia {
    EstadisticasSesionesViewController* vc = [[EstadisticasSesionesViewController alloc] initWithDia:dia tipo:self.tipoJuego];
    vc.view.frame = self.view.frame;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
