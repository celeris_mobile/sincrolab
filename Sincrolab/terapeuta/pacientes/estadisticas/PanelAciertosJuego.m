//
//  AciertosJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelAciertosJuego.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"

@interface PanelAciertosJuego()
@property (nonatomic, strong) GraficaPartidas* graficaAciertos;
@property (nonatomic, strong) GraficaPartidas* graficaTiempos;


@property (nonatomic, strong) NSArray* paciertos;
@property (nonatomic, strong) NSArray* tiemposMedios;
@property (nonatomic, strong) NSArray* fechas;

@end

@implementation PanelAciertosJuego

- (CGFloat)altura {
    return 540;
}

- (void) configura {

    self.graficaAciertos = [[GraficaPartidas alloc] initWithTitulo:@"Aciertos" colores:@[[UIColor colorWithRed:255/255.0 green:202/255.0 blue:68/255.0 alpha:1.0]]];
    self.graficaAciertos.delegate = self;
    self.graficaAciertos.grafica.minY = 0.0;
    self.graficaAciertos.grafica.maxY = 100.0;
    self.graficaAciertos.grafica.prefijoEtiquetasY = @"%";
    self.graficaAciertos.grafica.saltoY = 25.0;
    [self addSubview:self.graficaAciertos];
    
    self.graficaTiempos = [[GraficaPartidas alloc] initWithTitulo:@"Tiempo de Respuesta Aciertos" colores:@[[UIColor colorWithRed:149/255.0 green:189/255.0 blue:13/255.0 alpha:1.0]]];
    self.graficaTiempos.delegate = self;
    self.graficaTiempos.grafica.prefijoEtiquetasY = @"ms";
    self.graficaTiempos.grafica.minY = 0.0;

    
    CGRect frameGrafica = self.graficaTiempos.frame;
    frameGrafica.origin.y = 266;
    self.graficaTiempos.frame = frameGrafica;
    
    [self addSubview:self.graficaTiempos];
    
}

- (void)setDatasource:(id<PanelEstadisticasDatasource>)datasource {
    [super setDatasource:datasource];
    
    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        //TODO mensaje de que no hay datos
        self.graficaAciertos.hidden = YES;
        self.graficaTiempos.hidden = YES;
        self.graficaTiempos.grafica.dataSource = nil;
        self.graficaAciertos.grafica.dataSource = nil;
        
        return;
    }
    
    
    self.graficaTiempos.grafica.dataSource = self;
    self.graficaAciertos.grafica.dataSource = self;
    
    self.graficaAciertos.hidden = NO;
    self.graficaTiempos.hidden = NO;

    NSMutableArray* auxPAciertos = [[NSMutableArray alloc] init];
    NSMutableArray* auxTiempos = [[NSMutableArray alloc] init];
    NSMutableArray* auxTiemposCalc = [[NSMutableArray alloc] init];
    
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* aciertos = [partida valueForKeyPath:@"sesiones.@avg.porcentajeAciertos"];
            [auxPAciertos addObject:aciertos];

            NSNumber* tiemposSecs = [partida valueForKeyPath:@"sesiones.@avg.tiempoMedioDeRespuestaAciertos"];
            NSNumber* tiempos = [NSNumber numberWithInt:(int)(tiemposSecs.floatValue*1000)];
            [auxTiempos addObject:tiempos];
            
            [auxTiemposCalc addObject:tiempos];
        } else {
            [auxPAciertos addObject:[NSNull null]];
            [auxTiempos addObject:[NSNull null]];
        }
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
    }
    
    self.paciertos = auxPAciertos.copy;
    self.fechas = auxFechas.copy;
    self.tiemposMedios = auxTiempos.copy;

    NSExpression *expression = [NSExpression expressionForFunction:@"stddev:" arguments:@[[NSExpression expressionForConstantValue:auxTiemposCalc]]];
    NSNumber* desviacionTipica = [expression expressionValueWithObject:nil context:nil];
    NSArray* nombres = @[@"Desviación Típica", @"Media"];
    NSArray* valores = @[desviacionTipica, [auxTiemposCalc valueForKeyPath:@"@avg.self"]];
    
    [self generaExtras:nombres conValores:valores enVista:self.graficaTiempos];
    
    [self.graficaAciertos.grafica dibuja];
    [self.graficaTiempos.grafica dibuja];
}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(grafica==self.graficaAciertos.grafica)
        return self.paciertos;
    else
        return self.tiemposMedios;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}


@end
