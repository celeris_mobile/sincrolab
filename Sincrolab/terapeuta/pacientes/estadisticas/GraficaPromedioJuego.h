//
//  GraficaPromedioJuego.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"
#import "GraficaTarta.h"


@protocol GraficaPromedioJuegoDatasource <NSObject>
- (NSArray *) valoresParaJuego:(TipoJuego) tipoJuego;
- (NSArray *) otrosValores:(TipoJuego) tipoJuego;
@end

@interface GraficaPromedioJuego : UIView <GraficaTartaDatasource, GraficaTartaDelegate>
@property (nonatomic, weak) id<GraficaPromedioJuegoDatasource> datasource;
- (id) initWithJuego:(TipoJuego) tipo;
@end
