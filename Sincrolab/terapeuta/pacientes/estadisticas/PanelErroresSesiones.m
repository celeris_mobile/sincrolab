//
//  AciertosJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelErroresSesiones.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "GraficaPartidas.h"

@interface PanelErroresSesiones()
@property (nonatomic, strong) GraficaPartidas* graficaErrores;
@property (nonatomic, strong) GraficaPartidas* graficaTiempos;

@property (nonatomic, strong) NSArray* pcomisiones;
@property (nonatomic, strong) NSArray* pomisiones;
@property (nonatomic, strong) NSArray* perrores;
@property (nonatomic, strong) NSArray* tiemposMedios;
@property (nonatomic, strong) NSArray* etiquetas;

@end

@implementation PanelErroresSesiones
@synthesize dia = _dia;

- (CGFloat)altura {
    return 540;
}

- (void) configura {
    
    NSArray* colores = @[[UIColor colorWithRed:255/255.0 green:144/255.0 blue:116/255.0 alpha:1.0],
                         [UIColor colorWithRed:190/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                         [UIColor colorWithRed:175/255.0 green:103/255.0 blue:26/255.0 alpha:1.0]];
    
    self.graficaErrores = [[GraficaPartidas alloc] initWithTitulo:@"Errores" colores:colores leyenda:@[@"Errores", @"Comisiones", @"Omisiones"]];
    self.graficaErrores.grafica.dataSource = self;
    self.graficaErrores.delegate = self;
    self.graficaErrores.grafica.minY = 0.0;
    //    self.graficaErrores.grafica.maxY = 100.0;
    self.graficaErrores.grafica.prefijoEtiquetasY = @"%";
    self.graficaErrores.grafica.saltoY = 10.0;
    [self addSubview:self.graficaErrores];
    
    self.graficaTiempos = [[GraficaPartidas alloc] initWithTitulo:@"Tiempos de Respuesta Errores" colores:@[[UIColor colorWithRed:190/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]]];
    self.graficaTiempos.grafica.dataSource = self;
    self.graficaTiempos.delegate = self;
    self.graficaTiempos.grafica.prefijoEtiquetasY = @"s";
    
    
    CGRect frameGrafica = self.graficaTiempos.frame;
    frameGrafica.origin.y = 266;
    self.graficaTiempos.frame = frameGrafica;
    
    [self addSubview:self.graficaTiempos];
}

- (void)setDia:(DiaEntrenamiento *)dia {
    _dia = dia;
    
    id partida;
    switch (self.tipo) {
        case TipoJuegoGongs:
            partida = dia.partidaGongs;
            break;
        case TipoJuegoInvasion:
            partida = dia.partidaInvasion;
            break;
        case TipoJuegoPoker:
            partida = dia.partidaPoker;
            break;
    }
    NSArray* sesiones = (NSArray *)[partida valueForKey:@"sesiones"];
    
    if(!_dia || !partida || ! sesiones.count) {
        //TODO mensaje de que no hay datos
        self.graficaErrores.hidden = YES;
        self.graficaTiempos.hidden = YES;
        return;
    }
    self.graficaErrores.hidden = NO;
    self.graficaTiempos.hidden = NO;
    
    self.pcomisiones = [sesiones valueForKey:@"porcentajeComisiones"];
    self.pomisiones = [sesiones valueForKey:@"porcentajeOmisiones"];
    
    NSMutableArray* auxErrores = [[NSMutableArray alloc] init];
    NSMutableArray* auxEtiquetas = [[NSMutableArray alloc] init];
    for (int i = 0; i<self.pcomisiones.count; i++) {
        NSNumber* comisiones = [self.pcomisiones objectAtIndex:i];
        NSNumber* omisiones = [self.pomisiones objectAtIndex:i];
        [auxErrores addObject:[NSNumber numberWithFloat:comisiones.floatValue+omisiones.floatValue]];
        [auxEtiquetas addObject:[NSString stringWithFormat:@"S%d", i+1]];
    }
    self.perrores = auxErrores.copy;
    self.tiemposMedios = [sesiones valueForKey:@"tiempoMedioDeRespuestaErrores"];
    self.etiquetas = auxEtiquetas.copy;
    
    NSMutableArray* auxTiempos = [[NSMutableArray alloc] init];
    for (NSNumber* tiempo in self.tiemposMedios) {
        [auxTiempos addObject:[NSNumber numberWithInt:(int)(tiempo.floatValue*1000)]];
    }
    self.tiemposMedios = auxTiempos.copy;

    NSExpression *expression = [NSExpression expressionForFunction:@"stddev:" arguments:@[[NSExpression expressionForConstantValue:self.tiemposMedios]]];
    NSNumber* desviacionTipica = [expression expressionValueWithObject:nil context:nil];
    NSArray* nombres = @[@"Desviación Típica", @"Media"];
    NSArray* valores = @[desviacionTipica, [self.tiemposMedios valueForKeyPath:@"@avg.self"]];
    
    [self generaExtras:nombres conValores:valores enVista:self.graficaTiempos];
    
    [self.graficaErrores.grafica dibuja];
    [self.graficaTiempos.grafica dibuja];
}

#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    if(self.graficaErrores.grafica==grafica)
        return 3;
    
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(grafica==self.graficaErrores.grafica) {
        switch (numSet) {
            case 0:
                return self.perrores;
                break;
            case 1:
                return self.pcomisiones;
                break;
            case 2:
                return self.pomisiones;
                break;
        }
    } else
        return self.tiemposMedios;
    
    return nil;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.etiquetas;
}


@end
