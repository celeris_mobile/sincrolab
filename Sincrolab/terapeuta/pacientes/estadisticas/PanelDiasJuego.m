//
//  PanelDiasJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelDiasJuego.h"
#import "CustomPopOverBackgroundView.h"

@interface PanelDiasJuego()
@property (nonatomic, strong) UIPopoverController* popOver;
@property (nonatomic, strong, readwrite) NSArray* diasDeJuego;
@end

@implementation PanelDiasJuego

- (id)initWithJuego:(TipoJuego)tipo {
    self = [super init];
    if(self) {
        self.tipo = tipo;
    }
    
    return self;
}


- (void)setDatasource:(id<PanelEstadisticasDatasource>)datasource {
    [super setDatasource:datasource];

    BOOL hayPartidas = NO;
    NSMutableArray* auxDiasDeJuego = [[NSMutableArray alloc] init];
    for (DiaEntrenamiento* dia in self.datasource.diasDeEntrenamientoEntreFechas) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida)
            hayPartidas = YES;

        [auxDiasDeJuego addObject:dia];
    }
    
    if(hayPartidas)
        self.diasDeJuego = auxDiasDeJuego.copy;
    else
        self.diasDeJuego = nil;

}

#pragma mark GraficaPartidasDelegate
- (void) muestraPopoverParaGrafica:(GraficaPartidas *)grafica dePunto:(NSIndexPath *)indexPath enPosicion:(CGPoint)punto conValor:(NSString *)valor {

    if(self.popOver) {
        [self.popOver dismissPopoverAnimated:NO];
    }
    
    InfoPartidaViewController* vc = [[InfoPartidaViewController alloc] initWithTipoJuego:self.tipo diaDeEntrenamiento:[self.diasDeJuego objectAtIndex:indexPath.row] valor:valor];
    vc.delegate = self;
    
    
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:vc];
    self.popOver.popoverBackgroundViewClass = [CustomPopOverBackgroundView class];
    [self.popOver presentPopoverFromRect:CGRectMake(punto.x-4, punto.y-4, 8, 8) inView:grafica permittedArrowDirections:UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp animated:YES];

}

#pragma mark InfoPartidaDelegate
- (void)botonDadoDeInfo:(InfoPartidaViewController *)infoVc {
    if(self.popOver) {
        [self.popOver dismissPopoverAnimated:NO];
    }
    [self.delegate muestraDia:infoVc.dia];
}

@end
