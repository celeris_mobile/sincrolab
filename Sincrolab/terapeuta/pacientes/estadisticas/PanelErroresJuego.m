//
//  AciertosJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelErroresJuego.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "GraficaPartidas.h"

@interface PanelErroresJuego()
@property (nonatomic, strong) GraficaPartidas* graficaErrores;
@property (nonatomic, strong) GraficaPartidas* graficaTiempos;

@property (nonatomic, strong) NSArray* pcomisiones;
@property (nonatomic, strong) NSArray* pomisiones;
@property (nonatomic, strong) NSArray* perrores;
@property (nonatomic, strong) NSArray* tiemposMedios;
@property (nonatomic, strong) NSArray* fechas;

@end

@implementation PanelErroresJuego

- (CGFloat)altura {
    return 540;
}

- (void) configura {
    
    NSArray* colores = @[[UIColor colorWithRed:255/255.0 green:144/255.0 blue:116/255.0 alpha:1.0],
                         [UIColor colorWithRed:190/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                         [UIColor colorWithRed:175/255.0 green:103/255.0 blue:26/255.0 alpha:1.0]];
    
    self.graficaErrores = [[GraficaPartidas alloc] initWithTitulo:@"Errores" colores:colores leyenda:@[@"Errores", @"Comisiones", @"Omisiones"]];
    self.graficaErrores.delegate = self;
    self.graficaErrores.grafica.minY = 0.0;
    self.graficaErrores.grafica.prefijoEtiquetasY = @"%";
    self.graficaErrores.grafica.saltoY = 10.0;
    [self addSubview:self.graficaErrores];
    
    self.graficaTiempos = [[GraficaPartidas alloc] initWithTitulo:@"Tiempos de Respuesta Errores" colores:@[[UIColor colorWithRed:190/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]]];
    self.graficaTiempos.delegate = self;
    self.graficaTiempos.grafica.minY = 0.0;
    self.graficaTiempos.grafica.prefijoEtiquetasY = @"s";
    
    
    CGRect frameGrafica = self.graficaTiempos.frame;
    frameGrafica.origin.y = 266;
    self.graficaTiempos.frame = frameGrafica;
    
    [self addSubview:self.graficaTiempos];
}

- (void)setDatasource:(id<PanelEstadisticasDatasource>)datasource {
    [super setDatasource:datasource];
    
    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        //TODO mensaje de que no hay datos
        self.graficaErrores.hidden = YES;
        self.graficaTiempos.hidden = YES;
        self.graficaErrores.grafica.dataSource = nil;
        self.graficaTiempos.grafica.dataSource = nil;
        
        return;
    }

    
    self.graficaErrores.grafica.dataSource = self;
    self.graficaTiempos.grafica.dataSource = self;
    
    self.graficaErrores.hidden = NO;
    self.graficaTiempos.hidden = NO;
    
    NSMutableArray* auxComisiones = [[NSMutableArray alloc] init];
    NSMutableArray* auxOmisiones = [[NSMutableArray alloc] init];
    NSMutableArray* auxErrores = [[NSMutableArray alloc] init];
    NSMutableArray* auxTiempos = [[NSMutableArray alloc] init];
    NSMutableArray* auxTiemposCalc = [[NSMutableArray alloc] init];
    
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* comisiones = [partida valueForKeyPath:@"sesiones.@avg.porcentajeComisiones"];
            [auxComisiones addObject:comisiones];
            NSNumber* omisiones = [partida valueForKeyPath:@"sesiones.@avg.porcentajeOmisiones"];
            [auxOmisiones addObject:omisiones];
            [auxErrores addObject:[NSNumber numberWithInt:comisiones.intValue+omisiones.intValue]];
            
            NSNumber* tiemposSecs = [partida valueForKeyPath:@"sesiones.@avg.tiempoMedioDeRespuestaErrores"];
            NSNumber* tiempos = [NSNumber numberWithInt:(int)(tiemposSecs.floatValue*1000)];

            [auxTiempos addObject:tiempos];
            [auxTiemposCalc addObject:tiempos];
        } else {
            [auxComisiones addObject:[NSNull null]];
            [auxOmisiones addObject:[NSNull null]];
            [auxErrores addObject:[NSNull null]];
            [auxTiempos addObject:[NSNull null]];
            
        }
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
    }
    
    self.pcomisiones = auxComisiones.copy;
    self.pomisiones = auxOmisiones.copy;
    self.perrores = auxErrores.copy;
    self.fechas = auxFechas.copy;
    self.tiemposMedios = auxTiempos.copy;
    
    
    NSExpression *expression = [NSExpression expressionForFunction:@"stddev:" arguments:@[[NSExpression expressionForConstantValue:auxTiemposCalc]]];
    NSNumber* desviacionTipica = [expression expressionValueWithObject:nil context:nil];
    NSArray* nombres = @[@"Desviación Típica", @"Media"];
    NSArray* valores = @[desviacionTipica, [auxTiemposCalc valueForKeyPath:@"@avg.self"]];
    
    [self generaExtras:nombres conValores:valores enVista:self.graficaTiempos];
    
    [self.graficaErrores.grafica dibuja];
    [self.graficaTiempos.grafica dibuja];
}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    if(self.graficaErrores.grafica==grafica)
        return 3;
    
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(grafica==self.graficaErrores.grafica) {
        switch (numSet) {
            case 0:
                return self.perrores;
                break;
            case 1:
                return self.pcomisiones;
                break;
            case 2:
                return self.pomisiones;
                break;
        }
    } else
        return self.tiemposMedios;
    
    return nil;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}


@end
