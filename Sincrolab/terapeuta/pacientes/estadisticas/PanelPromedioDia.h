//
//  PanelPromedioDia.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelSesionesJuego.h"
#import "GraficaTarta.h"

@interface PanelPromedioDia : PanelSesionesJuego <GraficaTartaDatasource, GraficaTartaDelegate>

@end
