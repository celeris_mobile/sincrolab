//
//  AciertosJuego.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelDiasJuego.h"
#import "GraficaLineas.h"
#import "Constantes.h"
#import "GraficaPartidas.h"

@interface PanelAciertosJuego : PanelDiasJuego <GraficaLineasDatasource>

@end
