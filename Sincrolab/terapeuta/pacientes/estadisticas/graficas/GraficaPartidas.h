//
//  GraficaPartidas.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 23/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaLineas.h"

@class GraficaPartidas;

@protocol GraficaPartidasDelegate <NSObject>
- (void) muestraPopoverParaGrafica:(GraficaPartidas *)grafica dePunto:(NSIndexPath *) indexPath enPosicion:(CGPoint) punto conValor:(NSString *) valor;
@end

@interface GraficaPartidas : UIView <GraficaLineasDelegate>
- (id) initWithTitulo:(NSString *) titulo colores:(NSArray *) colores;
- (id) initWithTitulo:(NSString *) titulo colores:(NSArray *) colores leyenda:(NSArray *) leyenda;
@property (nonatomic, weak) id<GraficaPartidasDelegate> delegate;

@property (nonatomic, strong) GraficaLineas* grafica;
@end
