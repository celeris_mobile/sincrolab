//
//  SelectorFechas.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SelectorFechas.h"

@implementation SelectorFechas

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 647, 42)];
    if (self) {
        [self configura];
    }
    return self;
}

- (void) configura {
    self.backgroundColor = [UIColor whiteColor];
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(1.0,1.0f);
    self.layer.shadowOpacity = .5f;
    self.layer.shadowRadius = 2.0f;
}

@end
