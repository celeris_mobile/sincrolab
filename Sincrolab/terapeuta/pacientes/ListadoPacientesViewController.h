//
//  PacientesViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NuevoPacienteViewController.h"
#import "ActividadViewController.h"
#import "PacientesTerapeutaModel.h"

@interface ListadoPacientesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NuevoPacienteViewControllerDelegate, ActividadViewControllerDelegate>
@property (nonatomic, weak) PacientesTerapeutaModel* datasource;
- (BOOL)hayCambios;
@property (nonatomic, strong) NSString* primeraSeleccion;
@end
