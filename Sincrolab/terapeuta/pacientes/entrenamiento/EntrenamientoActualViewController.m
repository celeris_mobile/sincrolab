//
//  EntrenamientoActualViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoActualViewController.h"
#import "EntrenamientoAutomaticoViewController.h"
#import "EntrenamientoManualViewController.h"
#import "CalendarioTerapeuta.h"
#import "EntrenamientosManager.h"
#import "UIBAlertView.h"
#import "EntrenamientoPacienteModel.h"

@interface EntrenamientoActualViewController ()
@property (weak, nonatomic) IBOutlet UIView *textoNoHay;
@property (nonatomic, strong) CalendarioTerapeuta* calendario;
@property (nonatomic, strong) EntrenamientoPacienteModel* modelEntrenamiento;
@property (weak, nonatomic) IBOutlet UILabel *labelGenerar;
@end

@implementation EntrenamientoActualViewController
@synthesize modelo = _modelo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    self.calendario = [[CalendarioTerapeuta alloc] initWithFrame:CGRectMake(75, 50, 500, 400)];
    [self.view addSubview:self.calendario];
}


- (void) setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;

    if(self.isViewLoaded)
        [self compruebaEntrenamiento];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self compruebaEntrenamiento];
}

- (void) compruebaEntrenamiento {
    if(self.modelo.cargado)
        [self generaCalendario];
    else {
        [self.modelo carga:^(NSError * error) {
            if(!error) {
                [self generaCalendario];
            }
        } conProgreso:YES];
    }
}

- (void) generaCalendario {
    if(self.modelo.paciente.entrenamiento) {
        self.textoNoHay.hidden = YES;
        [self.calendario setEntrenamiento:self.modelo.paciente.entrenamiento];
        self.calendario.hidden = NO;
        self.labelGenerar.text = @"MODIFICAR ENTRENAMIENTO";
    } else {
        self.textoNoHay.hidden = NO;
        self.calendario.hidden = YES;
        self.labelGenerar.text = @"GENERAR ENTRENAMIENTO";
    }
}


- (IBAction)botonAutomaticoDado:(id)sender {
    if(self.modelo.paciente.entrenamiento) {
        UIBAlertView* alertView = [[UIBAlertView alloc] initWithTitle:@"Atención" message:@"Esto eliminará el entrenamiento actual. ¿Estás seguro?" cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Continuar", nil];
        [alertView showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
            if(!didCancel) {
                if(!self.modelo.paciente.perfilcognitivo) {
                    [[[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debes rellenar el perfil cognitivo antes de generar un entrenamiento para este paciente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    return;
                }
                
                [self muestraEntrenamientoAutomatico];
            }
        }];
    } else {
        if(!self.modelo.paciente.perfilcognitivo) {
            [[[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debes rellenar el perfil cognitivo antes de generar un entrenamiento para este paciente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return;
        }
        
        [self muestraEntrenamientoAutomatico];
    }
}

- (IBAction)botonManualDado:(id)sender {
    if(self.modelo.paciente.entrenamiento && self.modelo.paciente.entrenamiento.estadoEntrenamiento.tipo.intValue==TipoEntrenamientoAutomatico) {
        UIBAlertView* alertView = [[UIBAlertView alloc] initWithTitle:@"Atención" message:@"Esto eliminará el entrenamiento actual. ¿Estás seguro?" cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Continuar", nil];
        [alertView showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
            if(!didCancel)
                [self muestraEntrenamientoManual];
        }];
    } else {
        [self muestraEntrenamientoManual];
    }
}

- (void) muestraEntrenamientoAutomatico {
    EntrenamientoAutomaticoViewController* vc = [[EntrenamientoAutomaticoViewController alloc] init];
    vc.delegate = self;
    vc.entrenamiento = [EntrenamientosManager generaAutomaticoEntrenamientoParaPaciente:self.modelo.paciente];
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void) muestraEntrenamientoManual {
    EntrenamientoManualViewController* vc = [[EntrenamientoManualViewController alloc] init];
    if(!self.modelo.paciente.entrenamiento || self.modelo.paciente.entrenamiento.estadoEntrenamiento.tipo.intValue==TipoEntrenamientoAutomatico)
        vc.entrenamiento = [EntrenamientosManager generaManualEntrenamientoParaPaciente:self.modelo.paciente];
    else
        vc.entrenamiento = self.modelo.paciente.entrenamiento;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark EntrenamientoAutomaticoDelegate EntrenamientoManualDelegate
- (void) entrenamientoGenerado:(Entrenamiento *)entrenamiento {
    [self.modelo refresca];
}

@end
