//
//  EntrenamientoActualViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntrenamientoAutomaticoViewController.h"
#import "EntrenamientoManualViewController.h"
#import "EntrenamientoPacienteModel.h"

@interface EntrenamientoActualViewController : UIViewController <EntrenamientoManualDelegate, EntrenamientoAutomaticoDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
