//
//  EntrenamientoManualViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeccionEntrenamiento.h"
#import "SelectorJuego.h"

@protocol EntrenamientoManualDelegate <NSObject>
- (void) entrenamientoGenerado:(Entrenamiento *) entrenamiento;
@end

@interface EntrenamientoManualViewController : UIViewController <SeccionEntrenamiento, SelectorJuegoDelegate>
@property (nonatomic, weak) id<EntrenamientoManualDelegate> delegate;



@end
