//
//  EntrenamientoAutomaticoViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeccionEntrenamiento.h"
#import "SelectorJuego.h"

@protocol EntrenamientoAutomaticoDelegate <NSObject>
- (void) entrenamientoGenerado:(Entrenamiento *) entrenamiento;
@end

@interface EntrenamientoAutomaticoViewController : UIViewController <SeccionEntrenamiento, SelectorJuegoDelegate>
@property (nonatomic, strong) id<EntrenamientoAutomaticoDelegate> delegate;
@end
