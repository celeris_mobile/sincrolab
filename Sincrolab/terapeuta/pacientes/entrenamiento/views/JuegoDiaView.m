//
//  JuegoDiaView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "JuegoDiaView.h"
#import "Constantes.h"

@interface JuegoDiaView()
@property (nonatomic, strong) UIImageView* thumb;
@property (nonatomic, strong) UILabel* labelPuntos;
@property (nonatomic, strong) UIImageView* iconoAciertos;
@property (nonatomic, strong) UILabel* labelAciertos;
@property (nonatomic, strong) UIImageView* iconoTiempo;
@property (nonatomic, strong) UILabel* labelTiempo;

@end
@implementation JuegoDiaView

- (id) initWithJuego:(TipoJuego) tipoJuego {

    self = [super initWithFrame:CGRectMake(0, 0, 35, 250)];
    if (self) {
        
        self.thumb = [[UIImageView alloc] initWithImage:[Constantes miniaturaJuego:tipoJuego]];
        self.thumb.frame = CGRectMake(0, 0, 35, 35);
        [self addSubview:self.thumb];
        
        self.labelPuntos = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 150, 14)];
        self.labelPuntos.font = [UIFont fontWithName:@"Lato-Black" size:12.5];
        self.labelPuntos.textColor = [UIColor orangeColor];
        [self addSubview:self.labelPuntos];
        
        self.iconoAciertos = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"miniaciertos"]];
        self.iconoAciertos.center = CGPointMake(56, 24);
        self.iconoAciertos.hidden = YES;
        [self addSubview:self.iconoAciertos];
        
        self.labelAciertos = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, 150, 14)];
        self.labelAciertos.font = [UIFont fontWithName:@"Lato-Black" size:12.5];
        self.labelAciertos.textColor = [UIColor whiteColor];
        [self addSubview:self.labelAciertos];
        
        self.iconoTiempo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"minitiempo"]];
        self.iconoTiempo.center = CGPointMake(112, 25);
        self.iconoTiempo.hidden = YES;
        [self addSubview:self.iconoTiempo];
        
        self.labelTiempo = [[UILabel alloc] initWithFrame:CGRectMake(127, 20, 150, 14)];
        self.labelTiempo.font = [UIFont fontWithName:@"Lato-Black" size:12.5];
        self.labelTiempo.textColor = [UIColor whiteColor];
        [self addSubview:self.labelTiempo];
    }
    return self;
}

- (void)setPuntos:(NSNumber *)puntos {
    _puntos = puntos;
    
    self.iconoAciertos.hidden = NO;
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.labelPuntos.text = [NSString stringWithFormat:@"%@ puntos", [numberFormatter stringFromNumber:puntos]];
}

- (void)setAciertos:(NSNumber *)aciertos {
    _aciertos = aciertos;
    
    self.iconoTiempo.hidden = NO;
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.labelAciertos.text = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:aciertos]];

}

- (void)setTiempoRespuesta:(NSNumber *)tiempoRespuesta {
    _tiempoRespuesta = tiempoRespuesta;

    self.labelTiempo.text = [NSString stringWithFormat:@"%.0f ms", tiempoRespuesta.floatValue*1000];
}



@end
