//
//  SelectorJuego.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"

@class SelectorJuego;
@protocol SelectorJuegoDelegate <NSObject>
- (void) botonDadoConRect:(CGRect) rect deSelector:(SelectorJuego *) selector;
@end

@interface SelectorJuego : UIView
@property (nonatomic, weak) id<SelectorJuegoDelegate> delegate;
@property (nonatomic, strong) UIColor* color;
@property (nonatomic, strong) NSString* nombre;
@property (nonatomic, strong) UIImage* miniatura;
@property (nonatomic, readonly) NSNumber* diasSemana;
@property (nonatomic, assign) TipoJuego tipoJuego;

- (void) marcaDias:(NSNumber *) dias;
@end
