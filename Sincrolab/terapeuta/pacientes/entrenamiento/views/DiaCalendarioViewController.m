//
//  DiaCalendarioViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DiaCalendarioViewController.h"
#import "ResultadoSesionGongs.h"
#import "JuegoDiaView.h"

@interface DiaCalendarioViewController ()
@property (nonatomic, strong) UILabel* tituloLabel;
@property (nonatomic, strong) UILabel* labelFecha;

@property (nonatomic, strong) JuegoDiaView* juego1;
@property (nonatomic, strong) JuegoDiaView* juego2;
@property (nonatomic, strong) JuegoDiaView* juego3;

@end

@implementation DiaCalendarioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.preferredContentSize = CGSizeMake(274, 188);
    self.view.backgroundColor = [UIColor clearColor];
    
    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, 200, 18)];
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Black" size:12.0];
    self.tituloLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.tituloLabel];
    
    self.labelFecha = [[UILabel alloc] initWithFrame:CGRectMake(16, 27, 200, 18)];
    self.labelFecha.font = [UIFont fontWithName:@"Lato-Black" size:15.0];
    self.labelFecha.textColor = [UIColor colorWithRed:171/255.0 green:233/255.0 blue:212/255.0 alpha:1.0];
    [self.view addSubview:self.labelFecha];
    
    self.juego1 = [[JuegoDiaView alloc] initWithJuego:TipoJuegoGongs];
    self.juego1.frame = CGRectMake(17, 55, 35, 250);
    [self.view addSubview:self.juego1];

    self.juego2 = [[JuegoDiaView alloc] initWithJuego:TipoJuegoInvasion];
    self.juego2.frame = CGRectMake(17, 98, 35, 250);
    [self.view addSubview:self.juego2];

    self.juego3 = [[JuegoDiaView alloc] initWithJuego:TipoJuegoPoker];
    self.juego3.frame = CGRectMake(17, 141, 35, 250);
    [self.view addSubview:self.juego3];
    
    if(self.dia)
        [self configura];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configura];
}

- (void) configura {
    
    if(self.tipoCelda==TipoCeldaRealizado && self.dia) {
        self.tituloLabel.text = @"Entrenamiento realizado";
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE dd' de 'MMMM"];
        self.labelFecha.text = [formatter stringFromDate:self.fecha];
        
        if(self.dia.partidaGongs) {
            self.juego1.puntos = [self.dia.partidaGongs.sesiones valueForKeyPath:@"@sum.puntuacion"];
            self.juego1.aciertos = [self.dia.partidaGongs.sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
            self.juego1.tiempoRespuesta = [self.dia.partidaGongs.sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
        } else {
            self.juego1.alpha = 0.5;
        }
        
        if(self.dia.partidaInvasion) {
            self.juego2.puntos = [self.dia.partidaInvasion.sesiones valueForKeyPath:@"@sum.puntuacion"];
            self.juego2.aciertos = [self.dia.partidaInvasion.sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
            self.juego2.tiempoRespuesta = [self.dia.partidaInvasion.sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
        } else {
            self.juego2.alpha = 0.5;
        }
        
        if(self.dia.partidaPoker) {
            self.juego3.puntos = [self.dia.partidaPoker.sesiones valueForKeyPath:@"@sum.puntuacion"];
            self.juego3.aciertos = [self.dia.partidaPoker.sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
            self.juego3.tiempoRespuesta = [self.dia.partidaPoker.sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
        } else {
            self.juego3.alpha = 0.5;
        }
    } else {
        self.juego1.alpha = 0.0;
        self.juego2.alpha = 0.0;
        self.juego3.alpha = 0.0;
        CGRect tituloFrame = self.tituloLabel.frame;
        tituloFrame.origin = CGPointMake(50, 70);
        self.tituloLabel.frame = tituloFrame;
        CGRect fechaFrame = self.labelFecha.frame;
        fechaFrame.origin = CGPointMake(50, 85);
        self.labelFecha.frame = fechaFrame;
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE dd' de 'MMMM"];
        
        switch (self.tipoCelda) {
            case TipoCeldaFallado:
                self.tituloLabel.text = @"Entrenamiento no realizado";
                self.labelFecha.textColor = [UIColor colorWithRed:231/255.0 green:82/255.0 blue:82/255.0 alpha:1.0];
                self.labelFecha.text = [formatter stringFromDate:self.fecha];
                break;
            case TipoCeldaFuturo:
                self.tituloLabel.text = @"Día de entrenamiento";
                self.labelFecha.textColor = [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0];
                self.labelFecha.text = [formatter stringFromDate:self.fecha];
                break;
            case TipoCeldaLibre:
                self.tituloLabel.text = @"Día de libre";
                self.labelFecha.textColor = [UIColor lightGrayColor];
                self.labelFecha.text = [formatter stringFromDate:self.fecha];
                break;
            default:
                break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
