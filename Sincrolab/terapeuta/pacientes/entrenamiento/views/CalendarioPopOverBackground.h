//
//  CalendarioPopOverBackground.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 13/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarioPopOverBackground : UIPopoverBackgroundView {
    UIImageView *_borderImageView;
    UIImageView *_arrowView;
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
}

@end
