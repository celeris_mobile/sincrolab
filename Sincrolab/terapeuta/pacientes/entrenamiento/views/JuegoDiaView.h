//
//  JuegoDiaView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"

@interface JuegoDiaView : UIView
- (id) initWithJuego:(TipoJuego) tipoJuego;
@property (nonatomic, strong) NSNumber* puntos;
@property (nonatomic, strong) NSNumber* aciertos;
@property (nonatomic, strong) NSNumber* tiempoRespuesta;
@end
