//
//  ParametroFijo.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 01/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ParametroFijo.h"
#import "UIColor+Extensions.h"

@interface ParametroFijo()
@property (nonatomic, strong) UILabel* nombreLabel;
@property (nonatomic, strong) UILabel* valorLabel;
@end

@implementation ParametroFijo

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 141, 52)];
    if (self) {
        [self setup];
    }
    return self;
}


- (void) setup {
    UIView* fondoValor = [[UIView alloc] initWithFrame:CGRectMake(0, 26, 141, 26)];
    fondoValor.backgroundColor = [UIColor blackColor];
    [self addSubview:fondoValor];
    
    self.nombreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 141, 26)];
    self.nombreLabel.textAlignment = NSTextAlignmentCenter;
    self.nombreLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
    self.nombreLabel.textColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
    self.nombreLabel.text = self.nombre;
    [self addSubview:self.nombreLabel];
    
    
    self.valorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 141, 26)];
    self.valorLabel.textAlignment = NSTextAlignmentCenter;
    self.valorLabel.font = [UIFont fontWithName:@"Lato-Bold" size:15];
    self.valorLabel.textColor = [UIColor verdeAzulado];
    self.valorLabel.text = self.valor;
    CGRect valorFrame = self.valorLabel.frame;
    valorFrame.origin.y = 26;
    self.valorLabel.frame = valorFrame;
    [self addSubview:self.valorLabel];
}

- (void)setNombre:(NSString *)nombre {
    _nombre = nombre;
    self.nombreLabel.text = nombre;
}

- (void)setValor:(NSString *)valor {
    _valor = valor;
    self.valorLabel.text = valor;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
