//
//  FlechaPopOver.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "FlechaPopOver.h"
#import "UIColor+Extensions.h"

@implementation FlechaPopOver

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(ctx, [UIColor azulOscuro].CGColor);
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetLineWidth(ctx, 1.0);
    
    CGContextMoveToPoint(ctx, 0.0, rect.size.height);
    CGContextAddLineToPoint(ctx, rect.size.width/2.0, 0.0);
    CGContextAddLineToPoint(ctx, rect.size.width, rect.size.height);

    CGContextDrawPath(ctx, kCGPathFillStroke);
}


@end
