//
//  DiaCalendarioViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 17/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaEntrenamiento.h"
#import "DiaCell.h"

@interface DiaCalendarioViewController : UIViewController
@property (nonatomic, strong) DiaEntrenamiento* dia;
@property (nonatomic, assign) TipoCelda tipoCelda;
@property (nonatomic, assign) NSDate* fecha;
@end
