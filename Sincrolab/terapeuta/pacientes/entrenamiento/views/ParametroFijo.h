//
//  ParametroFijo.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 01/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParametroFijo : UIView
@property (nonatomic, strong) NSString* nombre;
@property (nonatomic, strong) NSString* valor;
@end
