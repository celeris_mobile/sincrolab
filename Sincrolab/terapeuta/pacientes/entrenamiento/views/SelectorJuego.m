//
//  SelectorJuego.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 07/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SelectorJuego.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageUtils.h"


@interface SelectorJuego()
@property (nonatomic, strong) UIView* colorView;
@property (nonatomic, strong) UILabel* nombreLabel;

@property (nonatomic, strong) UIButton* botonInfo;
@property (nonatomic, strong) NSArray* botonesSemana;

@end

@implementation SelectorJuego

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    
    return self;
}

- (void) setup {
    UIView* nombreView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 146, 48)];
    nombreView.layer.borderWidth = 2.0;
    nombreView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 48)];
    [nombreView addSubview:self.colorView];
    
    [self addSubview:nombreView];
    
    self.nombreLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 18, 120, 14)];
    self.nombreLabel.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    self.nombreLabel.textColor = [UIColor darkGrayColor];
    [self addSubview:self.nombreLabel];
    
    self.botonInfo = [[UIButton alloc] initWithFrame:CGRectMake(144, 0, 48, 48)];
    [self.botonInfo setImage:[UIImage imageNamed:@"btninfo.png"] forState:UIControlStateNormal];
    [self.botonInfo addTarget:self action:@selector(botonInfoDado) forControlEvents:UIControlEventTouchUpInside];
    self.botonInfo.layer.borderWidth = 2.0;
    self.botonInfo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self addSubview:self.botonInfo];
    
    NSMutableArray* auxbotones = [[NSMutableArray alloc] init];
    UIImage* imagenBlanca = [ImageUtils imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(48, 48)];
    for (int d = 0; d<7; d++) {
        UIButton* boton = [[UIButton alloc] initWithFrame:CGRectMake(202+52*d, 0, 48, 48)];
        [boton setImage:imagenBlanca forState:UIControlStateNormal];
        boton.tag = d;
        [boton addTarget:self action:@selector(botonSemanaDado:) forControlEvents:UIControlEventTouchUpInside];
        boton.layer.borderWidth = 2.0;
        boton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        [self addSubview:boton];
        [auxbotones addObject:boton];
    }
    
    self.botonesSemana = auxbotones.copy;
}

- (void) botonInfoDado {    
    [self.delegate botonDadoConRect:self.botonInfo.frame deSelector:self];
}

- (void) botonSemanaDado:(UIButton *) boton {
    boton.selected = !boton.selected;
}

- (NSNumber *)diasSemana {
    int dias = 0;
    for (int d = 0; d<self.botonesSemana.count; d++) {
        UIButton* boton = [self.botonesSemana objectAtIndex:d];
        if(boton.selected)
            dias = dias|(int)pow(2, d);
    }

    return [NSNumber numberWithInt:dias];
}

- (void) setColor:(UIColor *)color {
    _color = color;
    self.colorView.backgroundColor = color;
    
    UIImage* imagenMarcada = [ImageUtils imageWithColor:self.color andSize:CGSizeMake(48, 48)];
    for (UIButton* boton in self.botonesSemana) {
        [boton setImage:imagenMarcada forState:UIControlStateSelected];
    }
}

- (void) setNombre:(NSString *)nombre {
    _nombre = nombre;
    self.nombreLabel.text = nombre;
}

- (void) marcaDias:(NSNumber *) dias {

    for (int d = 0; d<self.botonesSemana.count; d++) {
        UIButton* boton = [self.botonesSemana objectAtIndex:d];
        boton.selected = ((int)pow(2, d) & dias.intValue)>0;
    }

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
