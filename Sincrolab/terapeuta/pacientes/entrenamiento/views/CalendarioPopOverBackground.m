//
//  CalendarioPopOverBackground.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 13/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CalendarioPopOverBackground.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Extensions.h"
#import "ImageUtils.h"


#define CAP_INSET 5.0
#define CAP_INSET_H 6.0

//#define CONTENT_INSET 5.0
#define ARROW_BASE 36.0
#define ARROW_HEIGHT 18.0

@implementation CalendarioPopOverBackground

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        UIView* borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        borderView.backgroundColor = [UIColor azulOscuro];
        borderView.layer.cornerRadius = 5.0;
        borderView.opaque = NO;
        
        UIImage* borderImage = [ImageUtils imageWithView:borderView];
        
        _borderImageView = [[UIImageView alloc] initWithImage:[borderImage resizableImageWithCapInsets:UIEdgeInsetsMake(CAP_INSET_H, CAP_INSET, CAP_INSET,CAP_INSET)]];
        
        UIView* arrowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 36, 18)];
        UIView* square = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        square.backgroundColor = [UIColor azulOscuro];
        square.layer.cornerRadius = 2.0;
        square.transform = CGAffineTransformMakeRotation(45*M_PI/180);
        square.center = CGPointMake(36/2, 36/2);
        [arrowView addSubview:square];
        arrowView.opaque = NO;
        
        
        UIImage* arrowImage = [ImageUtils imageWithView:arrowView];
        
        
        _arrowView = [[UIImageView alloc] initWithImage:arrowImage];
        
        [self addSubview:_borderImageView];
        [self addSubview:_arrowView];
        
        /*
         self.layer.shadowColor = [UIColor blackColor].CGColor;
         self.layer.shadowOffset = CGSizeMake(5.0,5.0f);
         self.layer.shadowOpacity = .5f;
         self.layer.shadowRadius = 10.0f;
         */
        self.layer.shadowColor = [UIColor clearColor].CGColor;
    }
    return self;
}


- (CGFloat) arrowOffset {
    return _arrowOffset;
}

- (void) setArrowOffset:(CGFloat)arrowOffset {
    _arrowOffset = arrowOffset;
}

- (UIPopoverArrowDirection)arrowDirection {
    return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
    _arrowDirection = arrowDirection;
}


+(UIEdgeInsets)contentViewInsets{
    return UIEdgeInsetsMake(CAP_INSET_H, CAP_INSET, CAP_INSET, CAP_INSET);
}

+(CGFloat)arrowHeight{
    return ARROW_HEIGHT;
}

+(CGFloat)arrowBase{
    return ARROW_BASE;
}

-  (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat _height = self.frame.size.height;
    CGFloat _width = self.frame.size.width;
    CGFloat _left = 0.0;
    CGFloat _top = 0.0;
    CGFloat _coordinate = 0.0;
    CGAffineTransform _rotation = CGAffineTransformIdentity;
    
    
    switch (self.arrowDirection) {
        case UIPopoverArrowDirectionAny:
        case UIPopoverArrowDirectionUnknown:
        case UIPopoverArrowDirectionUp:
            _top += ARROW_HEIGHT;
            _height -= ARROW_HEIGHT;
            _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, 0, ARROW_BASE, ARROW_HEIGHT);
            break;
            
            
        case UIPopoverArrowDirectionDown:
            _height -= ARROW_HEIGHT;
            _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, _height, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI );
            break;
            
        case UIPopoverArrowDirectionLeft:
            _left += ARROW_BASE;
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(ARROW_HEIGHT/2+2, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
            break;
            
        case UIPopoverArrowDirectionRight:
            
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(_width, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI_2 );
            
            break;
            
    }
    
    _borderImageView.frame =  CGRectMake(_left, _top, _width, _height);
    
    [_arrowView setTransform:_rotation];
    
}

@end