//
//  EntrenamientoAutomaticoViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoAutomaticoViewController.h"
#import "CustomPopOverBackgroundView.h"

#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"

#import "EntrenamientosManager.h"
#import "ParametrosViewController.h"



@interface EntrenamientoAutomaticoViewController ()
@property (nonatomic, strong) UIView* contentView;
@property (nonatomic, strong) UIButton* btnGuardar;
@property (nonatomic, strong) UIButton* btnCancelar;
@property (nonatomic, strong) UIView* juegosHolder;

@property (nonatomic, strong) UIPopoverController* popOver;

@property (nonatomic, strong) NSArray* selectores;
@end

@implementation EntrenamientoAutomaticoViewController
@synthesize entrenamiento = _entrenamiento;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    UIView* fondoTitulo = [[UIView alloc] initWithFrame:CGRectMake(20, 10, 607, 45)];
    fondoTitulo.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:fondoTitulo];
    
    UILabel* tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(103, 13, 403, 21)];
    tituloLabel.textAlignment = NSTextAlignmentCenter;
    tituloLabel.font = [UIFont fontWithName:@"Lato-Bold" size:14.0];
    tituloLabel.textColor = [UIColor blackColor];
    tituloLabel.text = @"ENTRENAMIENTO AUTOMÁTICO";
    [fondoTitulo addSubview:tituloLabel];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(20, 64, 607, 580)];
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.contentView];
    
    self.btnCancelar = [[UIButton alloc] initWithFrame:CGRectMake(310, 511, 136, 49)];
    [self.btnCancelar setImage:[UIImage imageNamed:@"btncancelar_entrenamient.png"] forState:UIControlStateNormal];
    [self.btnCancelar addTarget:self action:@selector(btnCancelarDado) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.btnCancelar];
    
    self.btnGuardar = [[UIButton alloc] initWithFrame:CGRectMake(454, 511, 136, 49)];
    [self.btnGuardar setImage:[UIImage imageNamed:@"btnguardar_entrenamiento.png"] forState:UIControlStateNormal];
    [self.btnGuardar addTarget:self action:@selector(btnGuardarDado) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.btnGuardar];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSArray* dias = @[@"Lunes", @"Martes", @"Miércoles", @"Jueves", @"Viernes", @"Sábado", @"Domingo"];
    int d = 0;
    for (NSString* dia in dias) {
        UILabel* labelDia = [[UILabel alloc] initWithFrame:CGRectMake(220+52*d, 19, 50, 17)];
        labelDia.font = [UIFont fontWithName:@"Lato-Regular" size:11];
        labelDia.textAlignment = NSTextAlignmentCenter;
        labelDia.text = dia;
        [self.contentView addSubview:labelDia];
        d++;
    }
    
    self.juegosHolder = [[UIView alloc] initWithFrame:CGRectMake(21, 39, 566, 307)];
    [self.contentView addSubview:self.juegosHolder];
    
    
    NSMutableArray* estados = [[NSMutableArray alloc] init];
    if(self.entrenamiento.estadoEntrenamiento.estadoGongs)
        [estados addObject:self.entrenamiento.estadoEntrenamiento.estadoGongs];
    if(self.entrenamiento.estadoEntrenamiento.estadoInvasion)
        [estados addObject:self.entrenamiento.estadoEntrenamiento.estadoInvasion];
    if(self.entrenamiento.estadoEntrenamiento.estadoPoker)
        [estados addObject:self.entrenamiento.estadoEntrenamiento.estadoPoker];
    
    
    NSArray* colores = [NSArray arrayWithObjects:
                        [UIColor colorWithRed:245/255.0 green:102/255.0 blue:65/255.0 alpha:1.0],
                        [UIColor colorWithRed:255/255.0 green:202/255.0 blue:68/255.0 alpha:1.0],
                        [UIColor colorWithRed:44/255.0 green:199/255.0 blue:147/255.0 alpha:1.0],
                        nil];
    
    
    NSMutableArray* auxSelectores = [[NSMutableArray alloc] init];
    for (int i = 0; i<estados.count; i++) {
        id estado = [estados objectAtIndex:i];
        
        SelectorJuego* juego = [[SelectorJuego alloc] initWithFrame:CGRectMake(0, 46*i, 566, 46)];
        juego.delegate = self;
        int tipoJuego;
        if([estado isKindOfClass:[EstadoJuegoGongs class]]) {
            tipoJuego = TipoJuegoGongs;
        } else if ([estado isKindOfClass:[EstadoJuegoInvasion class]]) {
            tipoJuego = TipoJuegoInvasion;
        } else if ([estado isKindOfClass:[EstadoJuegoPoker class]]) {
            tipoJuego = TipoJuegoPoker;
        }
        juego.tipoJuego = tipoJuego;
        juego.nombre = [Constantes nombreDeJuego:tipoJuego];
        juego.color = [colores objectAtIndex:tipoJuego];
        [self.juegosHolder addSubview:juego];
        [auxSelectores addObject:juego];
        
    }
    
    self.selectores = auxSelectores.copy;
}

- (void) btnCancelarDado {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) btnGuardarDado {
    BOOL incompleto = NO;
    for (SelectorJuego* selector in self.selectores) {
        if(selector.diasSemana.intValue==0)
            incompleto = YES;
    }
    if(incompleto) {
        [[[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debe seleccionar días de la semana para todos los juegos." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        int diasSemanaTotal = 0;
        for (SelectorJuego* selector in self.selectores) {
            diasSemanaTotal = diasSemanaTotal|selector.diasSemana.intValue;
            switch (selector.tipoJuego) {
                case TipoJuegoGongs:
                    self.entrenamiento.estadoEntrenamiento.estadoGongs.diasSemana = selector.diasSemana;
                    break;
                case TipoJuegoInvasion:
                    self.entrenamiento.estadoEntrenamiento.estadoInvasion.diasSemana = selector.diasSemana;
                    break;
                case TipoJuegoPoker:
                    self.entrenamiento.estadoEntrenamiento.estadoPoker.diasSemana = selector.diasSemana;
                    break;
                    
            }
        }
        
        [EntrenamientosManager guardaEntrenamiento:self.entrenamiento inicial:YES completado:^(NSError * error) {
            if(error) {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            } else {
               // NSDictionary* info = [NSDictionary dictionaryWithObjectsAndKeys:self.entrenamiento, @"entrenamiento", nil];
               // [[NSNotificationCenter defaultCenter] postNotificationName:kNuevoEntrenamientoNotification object:self userInfo:info];

                [self.delegate entrenamientoGenerado:self.entrenamiento];                
                [self.navigationController popViewControllerAnimated:YES];                
                /*
                [EntrenamientosManager cargaEntrenamientoDePaciente:self.entrenamiento.paciente onCompletado:^(NSError * error, Entrenamiento * entrenamiento) {
                    if(error) {
                        [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }
                    [self.delegate entrenamientoGenerado:self.entrenamiento];
                    [self.navigationController popViewControllerAnimated:YES];

                }];
                 */
            }
        }];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SelectorJuegoDelegate
- (void)botonDadoConRect:(CGRect)rect deSelector:(SelectorJuego *)selector {
    ParametrosViewController* popOverVc = [[ParametrosViewController alloc] init];
    switch (selector.tipoJuego) {
        case TipoJuegoGongs:
            
            popOverVc.estado = self.entrenamiento.estadoEntrenamiento.estadoGongs;
            break;
        case TipoJuegoInvasion:
            popOverVc.estado = self.entrenamiento.estadoEntrenamiento.estadoInvasion;
            break;
        case TipoJuegoPoker:
            popOverVc.estado = self.entrenamiento.estadoEntrenamiento.estadoPoker;
            break;
    }

    if(self.popOver) {
        [self.popOver dismissPopoverAnimated:YES];
        self.popOver = nil;
    }
    
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:popOverVc];
    self.popOver.popoverBackgroundViewClass = [CustomPopOverBackgroundView class];
    self.popOver.passthroughViews = self.selectores;
    
    rect = [self.view convertRect:rect fromView:selector];
    
    self.popOver.popoverLayoutMargins = UIEdgeInsetsMake(150, 0, 0, 0);

    [self.popOver presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

@end
