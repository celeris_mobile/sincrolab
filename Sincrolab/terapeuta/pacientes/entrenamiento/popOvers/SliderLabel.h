//
//  SliderLabel.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderLabel : UIView
@property (nonatomic, strong) NSString* valor;
@end
