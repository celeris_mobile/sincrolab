//
//  ParametroSlider.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ParametroSlider.h"
#import "SliderLabel.h"

//importamos la clase sliderAlert
#import "SliderAlert.h"
#import "EstadoJuegoGongs.h"

@interface ParametroSlider()


@property (nonatomic, strong) NSDictionary* configuracion;

@property (nonatomic, strong) NSDictionary* myDic;

@property (nonatomic, assign) float inicial;
@property (nonatomic, assign) float final;
@property (nonatomic, assign) float step;

//el nuestro

@property (nonatomic,strong)NSString *nombre;


//@property (nonatomic, assign) double myStep;



@property (nonatomic, assign) double valor;

@property (nonatomic, strong) UILabel* titulo;
@property (nonatomic, strong) UISlider* slider;
@property (nonatomic, strong) UIView* etiquetas;
@property (nonatomic, strong) SliderLabel* sliderLabel;

//añadimos un slider para la alerta
@property (nonatomic,strong)SliderAlert* sliderAlert;

@property (nonatomic,strong)NSString *tie;

@end

@implementation ParametroSlider {
    BOOL decimal;

    
}
@synthesize LastConfTexp;
@synthesize LastConfTie;
@synthesize globalDefaultTexp,globalDefaultTie;


static ParametroSlider *instance = nil;

+(ParametroSlider *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [ParametroSlider new];
        }
    }
    return instance;
    
   
}



- (id)initWithConfiguracion:(NSDictionary*) configuracion
{
    self = [super initWithFrame:CGRectMake(0, 0, 352, 65)];
    if (self) {
        self.configuracion = configuracion;
        
//       NSLog(@"valor de configuracion:%@",self.configuracion);
        
        
        
        
        
        

        [self setup];
        
        
        
        
        
        
        
        
        
        
        

    }
    return self;
}

- (void) setup {
    self.titulo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-20, 10)];
    self.titulo.textAlignment = NSTextAlignmentCenter;
    self.titulo.text = [self.configuracion valueForKey:@"caption"];
    self.titulo.font = [UIFont fontWithName:@"Lato-Bold" size:11];
    self.titulo.textColor = [UIColor lightGrayColor];
    [self addSubview:self.titulo];
    
    
    
    
    self.slider = [[UISlider alloc] initWithFrame:CGRectMake(10, 12, self.frame.size.width-20, 30)];

    UIImage* imgLleno = [[UIImage imageNamed:@"sliderlleno.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    UIImage* imgVacio = [[UIImage imageNamed:@"slidervacio.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    [self.slider setThumbImage:[UIImage imageNamed:@"handle_slider.png"] forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:imgVacio forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:imgLleno forState:UIControlStateNormal];
    
    NSString* rango = [self.configuracion valueForKey:@"rango"];

    
    
    
    NSArray* topes = [rango componentsSeparatedByString:@":"];
    
    
//    NSLog(@"topes es:%@",topes);
//    NSLog(@"names es:%@",nameFirst);
    self.inicial = ((NSString *)[topes objectAtIndex:0]).floatValue;
//    NSLog(@"self inicial vale:%f",self.inicial);
    
    self.final = ((NSString *)[topes objectAtIndex:1]).floatValue;
//   NSLog(@"self final vale:%f",self.final);
    

    
    NSString* valores = [self.configuracion valueForKey:@"valor"];
//    NSLog(@"valores devuelve:%@",valores);
    NSArray* arr_valores = [valores componentsSeparatedByString:@":"];
    

    
    if(arr_valores.count==1)
        self.valor = valores.floatValue;
    
    

    else {
        //TODO Slider con dos cabezales
        float valor1 = ((NSString *)[arr_valores objectAtIndex:0]).floatValue;
        float valor2 = ((NSString *)[arr_valores objectAtIndex:1]).floatValue;
        self.valor = (valor2-valor1)/2.0+valor1;
        
//        NSLog(@"arr_valores devuelve:%f",self.valor);
    }
    
    if([valores rangeOfString:@"."].location == NSNotFound) {
        self.step = 1.0;
        decimal = NO;
//        NSLog(@"NO ES UN DECIMAL");
    } else {
        self.step = 0.1;
        decimal = YES;
//        NSLog(@"ES UN DECIMAL");
    }
    
    
//    if([rango rangeOfString:@"."].location == NSNotFound) {
//        self.step = 1.0;
//        decimal = NO;
//    } else {
//        self.step = 0.1;
//        decimal = YES;
//    }   

    
    self.slider.minimumValue = 0.0;
    self.slider.maximumValue = 1.0;
    self.slider.value = (self.valor-self.inicial)/(self.final-self.inicial);
    
    [self.slider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.slider addTarget:self action:@selector(editEnd:) forControlEvents:UIControlEventTouchUpInside];
    [self.slider addTarget:self action:@selector(editEnd:) forControlEvents:UIControlEventTouchUpOutside];
    
    [self addSubview:self.slider];
    
    self.sliderLabel = [[SliderLabel alloc] init];
    
    //inicializamos el SliderAlert
    self.sliderAlert = [[SliderAlert alloc]init];
    self.sliderAlert.hidden = YES;
    [self addSubview:self.sliderAlert];
    self.sliderLabel.hidden = YES;
    [self addSubview:self.sliderLabel];
    
    [self dibujaEtiquetas];
    
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* etiqueta in self.etiquetas.subviews) {
            [etiqueta removeFromSuperview];
        }
        [self.etiquetas removeFromSuperview];
        self.etiquetas = nil;
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    self.etiquetas.backgroundColor = [UIColor clearColor];
    self.etiquetas.userInteractionEnabled = NO;
    [self insertSubview:self.etiquetas atIndex:0];
    
    
    //Calcula el ancho máximo
    NSString* etiqueta;
    CGFloat maxwidth = CGFLOAT_MIN;
    
    //Lo hemos cambiado sustituyendo step por step
    for (double e = self.inicial; e<=self.final; e+=self.step) {
        etiqueta = [NSString stringWithFormat:@"%d", (int)e];

        CGSize tam = [etiqueta sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Bold" size:9.0]}];

        maxwidth = MAX(maxwidth, tam.width);
    }
    maxwidth = maxwidth>30?maxwidth:30;
    
//    NSLog(@"el numero de pasos es:%f",self.step);
    UILabel* anterior;
    float numValores = (self.final-self.inicial)/self.step;
    
    
    CGFloat delta = (self.frame.size.width - 40)/numValores;
    int nume = 0;
    for (float e = self.inicial; e<=self.final; e+=self.step) {
        if(!decimal)
            etiqueta = [NSString stringWithFormat:@"%d", (int)e];
        else
            etiqueta = [NSString stringWithFormat:@"%.1f", e];
        
        CGPoint posicion = CGPointMake(delta*nume + 20, 35);
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectIntegral(CGRectMake((int)posicion.x-maxwidth/2, (int)posicion.y, maxwidth, 15))];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Lato-Bold" size:9.0];
        label.textColor = [UIColor lightGrayColor];
        label.text = etiqueta;
        
        if(!anterior||!CGRectIntersectsRect(label.frame, anterior.frame)) {
            [self.etiquetas addSubview:label];
            anterior = label;
        } else {
            //Hacemos la comparación así porque los números no tienen suficiente precisión
            if([[NSString stringWithFormat:@"%.2f", e] isEqualToString:[NSString stringWithFormat:@"%.2f", self.final]]) {
//                [anterior removeFromSuperview];
                [self.etiquetas addSubview:label];
                if(CGRectIntersectsRect(label.frame, anterior.frame)) {
                    [anterior removeFromSuperview];
                }
            }
        }
        
        nume+=1;
    }
}


- (void) valueChanged:(UISlider *) slider {
    
    
   
    //modificado el 21 de Agosto David Hevilla
    
    float valor = slider.value*(self.final-self.inicial)+self.inicial;
    
    float valor_n = round((valor/self.step))*self.step;
    
    float valor_aux = 0.0;
    
    if (decimal){
        valor_aux=(round((valor/self.step)*10))/10.0*self.step;
    
    
    }else{
        valor_aux=round((valor/self.step))*self.step;
    }
    
//    NSLog(@"el numero de valor_n es:%f",valor_n);
    
    
//    NSLog(@"el numero de valor es:%f",valor);
//    NSLog(@"el numero de valor_aux es:%f",valor_aux);


    valor = (valor_n-self.inicial)/(self.final-self.inicial);
    
    
    [slider setValue:valor animated:NO];
    
    self.sliderLabel.hidden = NO;
    self.sliderAlert.hidden = YES;
    self.sliderLabel.center = CGPointMake((slider.frame.size.width-20)*valor+slider.frame.origin.x+10, -3);
    NSString* valorStr;
    
    self.sliderAlert.center = CGPointMake((slider.frame.size.width-20)*valor+slider.frame.origin.x+10, -3);
    NSString* valorAlert;
    valorAlert = @"Cargando Alerta";
    
    
    if(!self.etiquetasLabel) {
        
        
        if(!decimal)
            valorStr = [NSString stringWithFormat:@"%.0f", valor_n];
        
        else
            valorStr = [NSString stringWithFormat:@"%.1f", valor_n];
        

    } else {
        valorStr = [self.etiquetasLabel objectAtIndex:valor_n];
    
        
    }
    
    self.sliderLabel.valor = valorStr;
    self.sliderAlert.valor = valorAlert;
    
    
    //Añadimos un condicional para evaluar si es un float o int
    if(!decimal)
        [self.delegate valorDe:[self.configuracion valueForKey:@"nombre"] cambiado:[NSString stringWithFormat:@"%d",(int)valor_n]];
    else
        [self.delegate valorDe:[self.configuracion valueForKey:@"nombre"] cambiado:[NSString stringWithFormat:@"%.1f",valor_n]];
        
    
    //comenzamos con los condicionales



    #pragma mark - Condicional TIE
//Añadimos un condicional para evaluar si es un float o int
if ([[self.configuracion valueForKey:@"nombre"] isEqual: @"tie"]) {
    
    
    //capturamos los parámetros automáticos de Tie y Texp
    NSLog(@"ESTAMOS EN TIE");
    
    //inicializamos ParametrosSlider para capturar las variables globales
    ParametroSlider *obj3=[ParametroSlider getInstance];
    ParametroSlider *obj4=[ParametroSlider getInstance];
    //
    obj3.LastConfTie = valorStr;
    
    LastConfTie = obj3.LastConfTie;
    LastConfTexp = obj4.LastConfTexp;
    
    
    NSLog(@"Configuración actual de TIE:%@",LastConfTie);
    NSLog(@"Configuración actual de TEXP:%@",LastConfTexp);
    
    float LastConfigurationTie = [obj3.LastConfTie floatValue];
    float LastConfigurationTexp = [obj4.LastConfTexp floatValue];
    
    NSLog(@"Ultima configuración TIE en float:%f",LastConfigurationTie);
    NSLog(@"Ultima configuración TEXP en float:%f",LastConfigurationTexp);
    
    
    
<<<<<<< HEAD
    if ([[self.configuracion valueForKey:@"nombre"] isEqual: @"tie"]) {
        
        

        //capturamos los parámetros del slider de Tie y Texp
        
        ParametroSlider *tieSlider=[ParametroSlider getInstance];
         ParametroSlider *texpSlider=[ParametroSlider getInstance];
=======
>>>>>>> 5ef67c10b4ff6dd80e115391adb8bd01bf619e8a
        
        
}
    

<<<<<<< HEAD
        tieSlider.globalDefaultTie= valorStr;
        texpSlider.globalDefaultTexp = valorStr;
        
        
        
        //Monitorizamos que slider estamos presionando y sus valores
        NSLog(@"ESTAMOS EN TIE");
        NSLog(@"Slider Tie devuelve:%@", tieSlider.globalDefaultTie);
        NSLog(@"Slider Texp devuelve:%@", texpSlider.globalDefaultTexp);
        
        //capturamos el valor almacenado de texp para compararlo con sliderTIE
      
        //capturamos los parámetros automáticos de Tie y Texp
        NSLog(@"ESTAMOS EN TIE");
        
        //inicializamos ParametrosSlider para capturar las variables globales
        ParametroSlider *obj3=[ParametroSlider getInstance];
        ParametroSlider *obj4=[ParametroSlider getInstance];
        
        obj3.LastConfTie= valorStr;
        
        LastConfTie = obj3.LastConfTie;
        LastConfTexp = obj4.globalDefaultTexp;

        float LastConfigurationTie = [obj3.globalDefaultTie floatValue];
        float LastConfigurationTexp = [obj4.globalDefaultTexp floatValue];
//        NSLog(@"Configuración actual de TIE:%@",LastConfTie);
//        NSLog(@"Configuración actual de TEXP:%@",LastConfTexp);
        
        
        
       

       
        
        
        NSLog(@"Ultima configuración TIE en float:%f",LastConfigurationTie);
        NSLog(@"Ultima configuración TEXP en float:%f",LastConfigurationTexp);
=======


#pragma mark - Condicional TEX
>>>>>>> 5ef67c10b4ff6dd80e115391adb8bd01bf619e8a

if ([[self.configuracion valueForKey:@"nombre"] isEqual:@"texp"]) {
    
    //capturamos los parámetros automáticos de Tie y Texp
    NSLog(@"ESTAMOS EN TEXP");
    
    ParametroSlider *obj3=[ParametroSlider getInstance];
    ParametroSlider *obj4=[ParametroSlider getInstance];
    
    obj4.LastConfTexp = valorStr;
    
    LastConfTie = obj3. LastConfTie;
    LastConfTexp = obj4.LastConfTexp;
    
    
   float LastConfigurationTie = [obj3.LastConfTie floatValue];
   float LastConfigurationTexp = [obj4.LastConfTexp floatValue];
    
    NSLog(@"Ultima configuración TIE en float:%f",LastConfigurationTie);
    NSLog(@"Ultima configuración TEXP en float:%f",LastConfigurationTexp);
    
    
}

<<<<<<< HEAD
        if (LastConfigurationTie < LastConfigurationTexp) {
            NSLog(@"el valor es erróneo");
            
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Atención" message:@"La configuración del tiempo interestimulo no puede ser menor que el de exposicion" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
            [alert addButtonWithTitle:@"Aceptar"];
            [alert show];
        }
        else {
            NSLog(@"valor correcto");
            
           
        }

    }else {
        
        
    ([[self.configuracion valueForKey:@"nombre"] isEqual:@"texp"]); {
        
        ParametroSlider *obj=[ParametroSlider getInstance];
        ParametroSlider *obj2=[ParametroSlider getInstance];
        
        obj2.LastConfTexp = valorStr;
        ;

        
        
        
        NSString *tie =  obj.LastConfTie;
        NSString *texp = obj2.LastConfTexp;
        //capturamos los parámetros automáticos de Tie y Texp
        NSLog(@"ESTAMOS EN TEXP");
        
        ParametroSlider *obj3=[ParametroSlider getInstance];
        ParametroSlider *obj4=[ParametroSlider getInstance];
        
        obj3.LastConfTexp = valorStr;
        
        LastConfTie = obj4.globalDefaultTie;
        LastConfTexp = obj3.LastConfTexp;
        
        NSLog(@"Configuración actual de TEXP:%@",LastConfTexp);
        NSLog(@"Configuración actual de TIE:%@",LastConfTie);
=======


#pragma mark - Condicional TEE1
>>>>>>> 5ef67c10b4ff6dd80e115391adb8bd01bf619e8a

if ([[self.configuracion valueForKey:@"nombre"] isEqual:@"tee1"]) {
    
    //capturamos los parámetros automáticos de Tie y Texp
    NSLog(@"ESTAMOS EN TEE1");
    
   
    


}


#pragma mark - Condicional TEE2
    //Añadimos un condicional para evaluar si es un float o int
    if ([[self.configuracion valueForKey:@"nombre"] isEqual: @"tee2"]) {
        
        
        //capturamos los parámetros automáticos de Tie y Texp
        NSLog(@"ESTAMOS EN TEE2");
        
    }
    
}

















//
//    NSLog(@"valor_n nos muestra:%f",valor_n);
//
//  NSLog(@"valorStr nos muestra:%@",valorStr);
//
//
//    NSLog(@"diccionario principal:%@",self.configuracion);


//    NSLog(@"dic vale:%@",dic);
//    NSLog(@"dic2 vale:%@",dic2);









- (void) editEnd:(UISlider *) slider {
    self.sliderLabel.hidden = YES;
    self.sliderAlert.hidden = YES;
    
    
   
    
        self.sliderLabel.hidden = NO;
    self.sliderAlert.hidden = NO;
    }



@end
