//
//  ParametrosViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ParametrosViewController.h"
#import "UIColor+Extensions.h"
#import "ParametroFijo.h"

#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"
#import "Configuraciones.h"

@interface ParametrosViewController ()
@property (nonatomic, strong) NSDictionary* parametros;
@property (nonatomic, strong) NSArray* parametrosArray;

@end


@implementation ParametrosViewController

- (id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.preferredContentSize = CGSizeMake(372, 368);
    self.view.backgroundColor = [UIColor clearColor];
    
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(372/2-100, 22, 200, 20)];
    titulo.textAlignment = NSTextAlignmentCenter;
    titulo.textColor = [UIColor verdeAzulado];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    titulo.text = @"PARÁMETROS DEL JUEGO";
    [self.view addSubview:titulo];

    if(_parametros)
        [self configura];
    
}


- (NSString *) stringFromIntParam:(NSString *) param {
    NSArray* components = [param componentsSeparatedByString:@":"];
    if(components.count==1)
        return [NSString stringWithFormat:@"%d", param.intValue];
    else {
        NSString* param1 = [components objectAtIndex:0];
        NSString* param2 = [components objectAtIndex:1];
        return [NSString stringWithFormat:@"%d - %d", param1.intValue, param2.intValue];
    }
}

- (NSString *) stringFromFloatParam:(NSString *) param {
    NSArray* components = [param componentsSeparatedByString:@":"];
    if(components.count==1)
        return [NSString stringWithFormat:@"%.2f", param.floatValue];
    else {
        NSString* param1 = [components objectAtIndex:0];
        NSString* param2 = [components objectAtIndex:1];
        return [NSString stringWithFormat:@"%.2f - %.2f", param1.floatValue, param2.floatValue];
    }
}

- (NSString *) stringFromBoolParam:(NSString *) param {
    NSArray* components = [param componentsSeparatedByString:@":"];
    if(components.count==1)
        return param.intValue?@"SÍ":@"NO";
    else {
        NSString* param1 = [components objectAtIndex:0];
        NSString* param2 = [components objectAtIndex:1];
        return [NSString stringWithFormat:@"%@ - %@", param1.intValue?@"SÍ":@"NO", param2.intValue?@"SÍ":@"NO"];
    }
}

- (void)setEstado:(id)estado {
    _estado = estado;
    
    if([estado isKindOfClass:[EstadoJuegoGongs class]]) {
        EstadoJuegoGongs* estado = (EstadoJuegoGongs *)self.estado;
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[self stringFromIntParam:estado.elementos] forKey:@"ELEMENTOS"];
        [dict setObject:[self stringFromIntParam:estado.nback] forKey:@"N BACK"];
        [dict setObject:[self stringFromIntParam:estado.sesiones] forKey:@"SESIONES"];
        [dict setObject:[self stringFromIntParam:estado.trials] forKey:@"TRIALS"];
        [dict setObject:[self stringFromFloatParam:estado.tie] forKey:@"TIE"];
        [dict setObject:[self stringFromFloatParam:estado.texp] forKey:@"T.EXP."];
        [dict setObject:[NSString stringWithFormat:@"%d%% - %d%%",estado.dmin.intValue, estado.dmax.intValue] forKey:@"DISTRACTORES"];
        
        NSString* modalidad;
        switch (estado.modalidad.intValue) {
            case EstadoGongsModalidadVisualA:
            case EstadoGongsModalidadVisualB:
                modalidad = @"Visual";
                break;
            case EstadoGongsModalidadAuditivaA:
            case EstadoGongsModalidadAuditivaB:
                modalidad = @"Auditiva";
                break;
            case EstadoGongsModalidadMedioVisualMedioAuditiva:
                modalidad = @"Medio V y A";
                break;
            case EstadoGongsModalidadDualVisualyAuditiva:
                modalidad = @"Dual V A";
                break;
            case EstadoGongsModalidadDualVisual:
                modalidad = @"Dual V";
                break;
            case EstadoGongsModalidadDualAuditiva:
                modalidad = @"Dual A";
                break;
            case EstadoGongsModalidadTripleVisualVisualAuditiva:
                modalidad = @"Triple VVA";
                break;
            case EstadoGongsModalidadTripleAuditivaAuditivaVisual:
                modalidad = @"Triple AAV";
                break;
        }
        
        [dict setObject:modalidad forKey:@"MODALIDAD"];
        [dict setObject:[NSString stringWithFormat:@"%.2f",estado.tie.floatValue*estado.trials.floatValue] forKey:@"DURACIÓN"];
        [dict setObject:[self stringFromFloatParam:estado.porcentaje_dianas] forKey:@"% DIANAS."];

        self.parametros = dict;
    }
    
    if([self.estado isKindOfClass:[EstadoJuegoInvasion class]]) {
        EstadoJuegoInvasion* estado = (EstadoJuegoInvasion *)self.estado;
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[self stringFromIntParam:estado.sesiones] forKey:@"SESIONES"];
        [dict setObject:[self stringFromIntParam:estado.trials] forKey:@"TRIALS"];
        [dict setObject:[self stringFromFloatParam:estado.tee1] forKey:@"T.EXP.E1"];
        [dict setObject:[self stringFromFloatParam:estado.tee2] forKey:@"T.EXP.E2"];
        [dict setObject:[self stringFromFloatParam:estado.tie] forKey:@"TIE"];
        [dict setObject:[self stringFromFloatParam:estado.dss] forKey:@"DSS"];
        [dict setObject:[self stringFromIntParam:estado.p_aparicion_ng] forKey:@"% NoGo"];
        [dict setObject:[self stringFromIntParam:estado.p_aparicion_ss] forKey:@"% SS"];
        [dict setObject:[self stringFromIntParam:estado.tipo_ss] forKey:@"TIPO SS"];
        
        self.parametros = dict;
    }
    
    if([self.estado isKindOfClass:[EstadoJuegoPoker class]]) {
        EstadoJuegoPoker* estado = (EstadoJuegoPoker *)self.estado;

        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[self stringFromIntParam:estado.cartas] forKey:@"CARTAS"];
        [dict setObject:[self stringFromIntParam:estado.jugadores] forKey:@"JUGADORES"];
        [dict setObject:[self stringFromFloatParam:estado.categorias] forKey:@"CATEGORIAS"];
        [dict setObject:[self stringFromFloatParam:estado.tie] forKey:@"TIE"];
        [dict setObject:[self stringFromFloatParam:estado.tresp] forKey:@"T.RESP."];
        [dict setObject:[self stringFromBoolParam:estado.ayudas] forKey:@"AYUDAS"];
        [dict setObject:[self stringFromIntParam:estado.tiempo_ayudas] forKey:@"T.AYUDAS"];
        [dict setObject:[self stringFromIntParam:estado.p_dianas] forKey:@"% DIANAS"];
        [dict setObject:[self stringFromIntParam:estado.n_cambios] forKey:@"Nº CAMBIOS"];
        
        self.parametros = dict;
    }
}

- (void)setParametros:(NSDictionary *)parametros {
    _parametros = parametros;
    //[self configura];
}

- (void) configura {
    
    if(self.parametrosArray) {
        for (ParametroFijo* parametroFijo in self.parametrosArray) {
            [parametroFijo removeFromSuperview];
        }
    }
    
    NSMutableArray* parametrosAux = [[NSMutableArray alloc] init];
    int p = 0;
    int total = self.parametros.count;
    for (NSString* parametro in self.parametros) {
        NSString* valor = [self.parametros objectForKey:parametro];
        ParametroFijo* param = [[ParametroFijo alloc] init];
        param.nombre = parametro;
        param.valor = valor;
        
        int posx = p%2;
        int posy = (int)floor(p/2.0);
        
        param.center = CGPointMake(115+posx*153, 85+posy*56);
        if(p==total-1 && p%2==0)
            param.center = CGPointMake(115+76, 85+posy*56);
        
        [parametrosAux addObject:param];
        [self.view addSubview:param];
        p++;
    }
    
    self.parametrosArray = parametrosAux.copy;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end