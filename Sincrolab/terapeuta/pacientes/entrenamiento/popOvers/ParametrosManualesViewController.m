 //
//  ParametrosManualesViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ParametrosManualesViewController.h"
#import "UIColor+Extensions.h"
#import "ParametroSlider.h"

#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"
#import "Configuraciones.h"

@interface ParametrosManualesViewController ()
@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) NSArray* parametros;
@property (nonatomic, strong) NSArray* parametrosArray;

@property (nonatomic, strong) NSDictionary* configuracion;


@end

@implementation ParametrosManualesViewController
@synthesize LastConfTexp,LastConfTie;


static ParametroSlider *instance = nil;

+(ParametroSlider *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [ParametroSlider new];
        }
    }
    return instance;
    
    
}

- (id)init {
    self = [super init];
    if (self) {
        
        self.configuracion = _configuracion;
        
//        NSLog(@"configuracion:%@",self.configuracion);
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.preferredContentSize = CGSizeMake(372, 580);
    self.view.backgroundColor = [UIColor clearColor];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.scrollView];
    
   

    
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(372/2-100, 22, 200, 20)];
    titulo.textAlignment = NSTextAlignmentCenter;
    titulo.textColor = [UIColor verdeAzulado];
    titulo.font = [UIFont fontWithName:@"Lato-Bold" size:15.0];
    titulo.text = @"PARÁMETROS DEL JUEGO";
    [self.scrollView addSubview:titulo];
    
    if(_parametros)
        
    [self configura];
    
}

- (void)setEstado:(id)estado {
    _estado = estado;
    
    NSString* caption = @"caption";
    NSString* nombre = @"nombre";
    NSString* rango = @"rango";
    NSString* valor = @"valor";
    
    if([estado isKindOfClass:[EstadoJuegoGongs class]]) {
        EstadoJuegoGongs* estado = (EstadoJuegoGongs *)self.estado;
          
        

        NSMutableArray* auxParams = [[NSMutableArray alloc] init];
        [auxParams addObject:@{caption:@"ELEMENTOS", nombre:@"elementos", rango:@"2:8", valor:estado.elementos}];
//        NSLog(@"el numero de elementos es:%@",estado.elementos);
        [auxParams addObject:@{caption:@"N BACK", nombre:@"nback", rango:@"1:5", valor:estado.nback}];
//        NSLog(@"el numero de elementos es:%@",estado.elementos);

        [auxParams addObject:@{caption:@"SESIONES", nombre:@"sesiones", rango:@"1:20", valor:estado.sesiones}];
        [auxParams addObject:@{caption:@"TRIALS", nombre:@"trials", rango:@"5:250", valor:estado.trials}];
        [auxParams addObject:@{caption:@"TIEMPO ENTRE ESTÍMULOS", nombre:@"tie", rango:@"1.0:5.0", valor:estado.tie}];
        
        //modificado 19 de Agosto David Hevilla
        [auxParams addObject:@{caption:@"TIEMPO DE EXPOSICIÓN", nombre:@"texp", rango:@"0.1:4.0", valor:estado.texp}];
        [auxParams addObject:@{caption:@"% MÍNIMO DE DISTRACTORES", nombre:@"dmin", rango:@"0:100", valor:estado.dmin}];
        [auxParams addObject:@{caption:@"% MÁXIMO DE DISTRACTORES", nombre:@"dmax", rango:@"0:100", valor:estado.dmax}];
        [auxParams addObject:@{caption:@"MODALIDAD", nombre:@"modalidad", rango:@"0:9", valor:estado.modalidad}];
        [auxParams addObject:@{caption:@"% DIANAS", nombre:@"porcentaje_dianas", rango:@"10:90", valor:estado.porcentaje_dianas}];
        
        ///////////////////////////////////////////////
        //david hevilla
        
        
        ParametroSlider *obj3=[ParametroSlider getInstance];
        ParametroSlider *obj4=[ParametroSlider getInstance];
        
        obj3.LastConfTie = estado.tie;
        obj4.LastConfTexp = estado.texp;
        
        NSLog(@"el parámetro en PARSE de tie es:%@",obj3.LastConfTie);
        NSLog(@"el parámetro en PARSE de texp es:%@",obj4.LastConfTexp);

        
        float LastConfigurationTie = [obj3.LastConfTie floatValue];
        float LastConfigurationTexp = [obj4.LastConfTexp floatValue];
        
        NSLog(@"TIE:%f",LastConfigurationTie);
        NSLog(@"TEXP:%f",LastConfigurationTexp);
        

        
        
        if (LastConfigurationTie < LastConfigurationTexp) {
            NSLog(@"La última configuración es errónea");
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Atención" message:@"La configuración del tiempo interestimulo no puede ser menor que el de exposición" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:nil];
            [alert addButtonWithTitle:@"Acepto"];
            [alert show];
            
        }

        
        else {
            
            
            NSLog(@"la última configuración es correcta");
        }
        
        

        self.parametros = auxParams.copy;
                
        

    }
    
    
    
    if([self.estado isKindOfClass:[EstadoJuegoInvasion class]]) {
        EstadoJuegoInvasion* estado = (EstadoJuegoInvasion *)self.estado;
        
        
        
        NSMutableArray* auxParams = [[NSMutableArray alloc] init];
        [auxParams addObject:@{caption:@"SESIONES", nombre:@"sesiones", rango:@"1:20", valor:estado.sesiones}];
        [auxParams addObject:@{caption:@"TRIALS", nombre:@"trials", rango:@"5:250", valor:estado.trials}];
        
        //modificado 20 de Agosto
        //modificado texp1 y texp2 por tee1 y tee2
        //david hevilla
        [auxParams addObject:@{caption:@"TIEMPO DE APARICIÓN SAMURAI", nombre:@"tee1", rango:@"0.5:4.0", valor:estado.tee1}];
        [auxParams addObject:@{caption:@"TIEMPO DE APARICIÓN VILLANO", nombre:@"tee2", rango:@"0.5:4.0", valor:estado.tee2}];
        [auxParams addObject:@{caption:@"TIEMPO ENTRE ESTÍMULOS", nombre:@"tie", rango:@"1.0:6.0", valor:estado.tie}];
        [auxParams addObject:@{caption:@"DEMORA SEÑAL DE STOP", nombre:@"dss", rango:@"-0.5:0.5", valor:estado.dss}];
        
        [auxParams addObject:@{caption:@"TIPO CAMBIO", nombre:@"cambio", rango:@"0:3", valor:estado.cambio}];
        [auxParams addObject:@{caption:@"NÚMERO DE CAMBIOS", nombre:@"n_cambios", rango:@"0:20", valor:estado.n_cambios}];
        
        [auxParams addObject:@{caption:@"% ESTÍMULO DISTRACTOR", nombre:@"p_aparicion_ng", rango:@"10:90", valor:estado.p_aparicion_ng}];
        [auxParams addObject:@{caption:@"% SEÑAL DE STOP", nombre:@"p_aparicion_ss", rango:@"10:90", valor:estado.p_aparicion_ss}];
        [auxParams addObject:@{caption:@"TIPO SEÑAL STOP", nombre:@"tipo_ss", rango:@"0:2", valor:estado.tipo_ss}];

        self.parametros = auxParams.copy;
        
    }

    if([self.estado isKindOfClass:[EstadoJuegoPoker class]]) {
        EstadoJuegoPoker* estado = (EstadoJuegoPoker *)self.estado;
        
        
        NSMutableArray* auxParams = [[NSMutableArray alloc] init];
        [auxParams addObject:@{caption:@"CARTAS", nombre:@"cartas", rango:@"5:20", valor:estado.cartas}];
        [auxParams addObject:@{caption:@"JUGADORES", nombre:@"jugadores", rango:@"2:5", valor:estado.jugadores}];
        [auxParams addObject:@{caption:@"CATEGORIAS", nombre:@"categorias", rango:@"1:4", valor:estado.categorias}];
        [auxParams addObject:@{caption:@"TIEMPO ENTRE ESTÍMULOS", nombre:@"tie", rango:@"1.0:6.0", valor:estado.tie}];
        [auxParams addObject:@{caption:@"TIEMPO PARA RESPONDER", nombre:@"tresp", rango:@"0.5:4.0", valor:estado.tresp}];
        [auxParams addObject:@{caption:@"AYUDAS", nombre:@"ayudas", rango:@"0:1", valor:estado.ayudas}];
        [auxParams addObject:@{caption:@"TIEMPO DE AYUDAS", nombre:@"tiempo_ayudas", rango:@"0.0:10.0", valor:estado.tiempo_ayudas}];
        [auxParams addObject:@{caption:@"% DIANAS", nombre:@"p_dianas", rango:@"10:90", valor:estado.p_dianas}];
        [auxParams addObject:@{caption:@"NÚMERO DE CAMBIOS", nombre:@"n_cambios", rango:@"0:20", valor:estado.n_cambios}];
        
        self.parametros = auxParams.copy;
    }
}

- (void)setParametros:(NSArray *)parametros {
    _parametros = parametros;
    
    //modificado 21 de Agosto
    //David hevilla
    //estaba comentado el self.configura
//   [self configura];
}

- (void) configura {
    
    if(self.parametrosArray) {
        for (UIView* parametro in self.parametrosArray) {
            [parametro removeFromSuperview];
        }
    }
    
    NSMutableArray* parametrosAux = [[NSMutableArray alloc] init];
    int p = 0;
    for (NSDictionary* parametro in self.parametros) {
        ParametroSlider* param = [[ParametroSlider alloc] initWithConfiguracion:parametro];
        if([[parametro valueForKey:@"nombre"] isEqualToString:@"ayudas"]) {
            param.etiquetasLabel = @[@"NO", @"SÍ"];
        }
        if([[parametro valueForKey:@"nombre"] isEqualToString:@"modalidad"]) {
            param.etiquetasLabel = @[@"Visual A", @"Visual B", @"Auditivo A", @"Auditivo B", @"Visual/Auditivo", @"Dual Visual/Auditivo", @"Dual Visual", @"Dual Auditivo", @"Triple VVA", @"Triple AAV"];
        }
        if([[parametro valueForKey:@"nombre"] isEqualToString:@"tipo_ss"]) {
            param.etiquetasLabel = @[@"No Hay", @"Auditiva", @"Visual"];
        }
        if([[parametro valueForKey:@"nombre"] isEqualToString:@"cambio"]) {
            param.etiquetasLabel = @[@"No Hay", @"Por trial", @"Por sesión", @"Por partida"];
        }
        
        param.delegate = self;
        CGRect paramFrame = param.frame;
        paramFrame.origin.y = 70+65*p;
        param.frame = paramFrame;
        
        [parametrosAux addObject:param];
        [self.scrollView addSubview:param];
        p++;
    }
    
    self.scrollView.contentSize = CGSizeMake(372, self.parametros.count*65+100);
    
    self.parametrosArray = parametrosAux.copy;
    
}

- (void)valorDe:(NSString *)campo cambiado:(NSString *)valorNuevo {
    [self.estado setObject:valorNuevo forKey:campo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end