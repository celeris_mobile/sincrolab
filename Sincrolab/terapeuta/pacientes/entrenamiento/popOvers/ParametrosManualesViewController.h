//
//  ParametrosManualesViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ParametroSlider.h"

@interface ParametrosManualesViewController : UIViewController <ParametroSliderDelegate>
{
    

}
@property (nonatomic, strong) id estado;

@property (nonatomic, strong)NSString *texp;
@property (nonatomic, strong)NSString *tie;


//definimos las variables globales TIE y TEXP
@property(nonatomic,retain)NSString *LastConfTie;
@property(nonatomic,retain)NSString *LastConfTexp;



+(ParametroSlider*)getInstance;

@end
