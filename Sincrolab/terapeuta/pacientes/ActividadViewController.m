//
//  ActividadViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ActividadViewController.h"

#import "Constantes.h"

#import "PanelDatosPaciente.h"
#import "PanelPerfilCognitivoPaciente.h"
#import "PanelCalendarioPaciente.h"
#import "PanelPorcentajeJuegos.h"
#import "EntrenamientoPacienteModel.h"

@interface ActividadViewController ()
@property (weak, nonatomic) IBOutlet PanelDatosPaciente *datos;
@property (weak, nonatomic) IBOutlet PanelPerfilCognitivoPaciente *perfil;
@property (weak, nonatomic) IBOutlet PanelProgresoPaciente *progreso;
@property (weak, nonatomic) IBOutlet PanelCalendarioPaciente *calendario;
@property (weak, nonatomic) IBOutlet PanelPorcentajeJuegos *porcentajeJuegos;

@property (nonatomic, strong) EntrenamientoPacienteModel* modeloEntrenamiento;

@end

@implementation ActividadViewController
@synthesize modelo = _modelo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.datos.delegate = self;
    self.perfil.delegate = self;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    
    if(self.modelo.cargado)
        [self configuraPaneles];
    else {
        [self.modelo carga:^(NSError * error) {
            if(!error)
                [self configuraPaneles];
        } conProgreso:YES];
    }

}

- (void) configuraPaneles {
    if(!self.modelo.paciente)
        return;
    
    self.datos.paciente = self.modelo.paciente;
    self.perfil.paciente = self.modelo.paciente;

    self.progreso.datasource = self;
    self.progreso.paciente = self.modelo.paciente;

    self.calendario.modelo = self.modelo;
    self.porcentajeJuegos.modelo = self.modelo;


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark PanelProgresoPacienteDatasource
- (NSArray *)diasDeEntrenamiento {
    if(!self.modelo)
        return nil;
    
    if(self.modelo.cargado)
        return self.modelo.entrenamiento.diasDeEntrenamiento;

    [self.modelo carga:^(NSError * error) {
        if(!error) {
            self.progreso.datasource = self;
            self.progreso.paciente = self.modelo.paciente;
        }
    } conProgreso:YES];
    
    return nil;
    
}

#pragma mark PanelDatosPacienteDelegate PanelPerfilCognitivoPacienteDelegate
- (void)amplia:(id) panel {

    if([panel isKindOfClass:[PanelDatosPaciente class]])
        [self.delegate muestraSeccionDatos];
    
    if([panel isKindOfClass:[PanelPerfilCognitivoPaciente class]])
        [self.delegate muestraSeccionPerfil];
}

@end
