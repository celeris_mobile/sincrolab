//
//  HomeTerapeutaViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaActividadSemanal.h"
#import "GraficaProcesos.h"
#import "GraficaEdad.h"
#import "GraficaSexo.h"
#import "PacientesTerapeutaModel.h"

@protocol HomeTerapeutaDelegate <NSObject>
- (void) cargaFichaPaciente:(NSString *) pacienteId;
@end

@interface HomeTerapeutaViewController : UIViewController <GraficaActividadSemanaDelegate, GraficaActividadSemanaDatasource, GraficaProcesosDelegate, GraficaProcesosDatasource, GraficaEdadDatasource, GraficaSexoDatasource, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) PacientesTerapeutaModel* datasource;
@property (nonatomic, weak) id<HomeTerapeutaDelegate> delegate;
@end
