//
//  HomeTerapeutaViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DateUtils.h"

#import "Constantes.h"

#import "HomeTerapeutaViewController.h"
#import "TestEstadoGongsViewController.h"
#import "TestEstadoInvasionViewController.h"
#import "TestEstadoPokerViewController.h"

#import "NumPacientesView.h"
#import "GraficaActividadSemanal.h"
#import "GraficaProcesos.h"
#import "GraficaEdad.h"
#import "GraficaSexo.h"

#import "Paciente.h"
#import "DiaEntrenamiento.h"

#import "ProgressHUD.h"
#import "EntrenamientosManager.h"
#import "ResultadoSesionGongs.h"
#import "ResultadoSesionInvasion.h"
#import "ResultadoSesionPoker.h"

#import "CeldaPacienteActivo.h"
#import "CeldaPacienteInactivo.h"

@interface HomeTerapeutaViewController ()
@property (weak, nonatomic) IBOutlet UILabel *saludoLabel;
@property (weak, nonatomic) IBOutlet UILabel *grupoLabel;
@property (weak, nonatomic) IBOutlet UILabel *fechaLabel;
@property (weak, nonatomic) IBOutlet UILabel *horaLabel;

@property (weak, nonatomic) IBOutlet NumPacientesView *numPacientes;
@property (weak, nonatomic) IBOutlet GraficaActividadSemanal *graficaActividadSemanal;
@property (weak, nonatomic) IBOutlet GraficaProcesos *graficaProcesos;
@property (weak, nonatomic) IBOutlet GraficaEdad *graficaEdad;
@property (weak, nonatomic) IBOutlet GraficaSexo *graficaSexo;
@property (weak, nonatomic) IBOutlet UITableView *actividadTableView;

@property (nonatomic, strong) NSDate* semanaActual;
@property (nonatomic, strong) NSArray* pacientesPorEvento;

@property (nonatomic, strong) NSMutableArray* pacientesBorrame;
@property (weak, nonatomic) IBOutlet UIView *botoneraDebug;
@end

@implementation HomeTerapeutaViewController  {
    int semana;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.botoneraDebug.hidden = YES;

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];

    self.saludoLabel.text = [NSString stringWithFormat:@"Hola %@", [self.datasource terapeuta].nombre   ];
    
    NSDate* ahora = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateStyle = NSDateFormatterLongStyle;
    self.fechaLabel.text = [formatter stringFromDate:ahora];
    formatter = [[NSDateFormatter alloc] init];
    formatter.timeStyle = NSDateFormatterShortStyle;
    formatter.dateStyle = NSDateFormatterNoStyle;
    self.horaLabel.text = [formatter stringFromDate:ahora];
    
    self.graficaActividadSemanal.datasource = self;
    self.graficaActividadSemanal.delegate = self;
    self.graficaActividadSemanal.titulo = @"Actividad semanal";

    self.graficaProcesos.datasource = self;
    self.graficaProcesos.delegate = self;
    self.graficaProcesos.titulo = @"Actividad de Perfiles";
    
    self.graficaEdad.datasource = self;
    self.graficaEdad.titulo = @"Edad";
    
    self.graficaSexo.datasource = self;
    self.graficaSexo.titulo = @"Sexo";

    self.actividadTableView.backgroundColor = [UIColor clearColor];
    self.actividadTableView.delegate = self;
    self.actividadTableView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(refrescaGraficasPacientes) name:kPacientesCargadosNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self	selector:@selector(refrescaGraficasEntrenamientos) name:kEntrenamientosSemanaCargadosNotification object:nil];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refrescaGraficasPacientes];
    [self refrescaGraficasEntrenamientos];
}

- (void) refrescaGraficasPacientes {
    if(!self.datasource.pacientes.count)
        return;
    
    self.numPacientes.pacientesTotales = self.datasource.pacientes.count;
    self.numPacientes.pacientesActivos = self.datasource.numPacientesActivos;
    
    [self.graficaEdad refresca];
    [self.graficaSexo refresca];
    
    semana = 0;
    self.semanaActual = [DateUtils primerDiaDeLaSemanaParaFecha:[NSDate date]];
}

- (void) refrescaGraficasEntrenamientos {

    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"fechaUltimoEvento != nil"];
    NSArray* pacientesFiltrados = [self.datasource.pacientes filteredArrayUsingPredicate:predicate];

    self.pacientesPorEvento = [pacientesFiltrados sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Paciente* p1 = obj1;
        Paciente* p2 = obj2;
        return [DateUtils comparaSinHoraFecha:p2.fechaUltimoEvento conFecha:p1.fechaUltimoEvento];
    }];
    

    
    [self.actividadTableView reloadData];
    [self actualizaGraficas];
}

- (void)setSemanaActual:(NSDate *)semanaActual {
    _semanaActual = semanaActual;
    [self.datasource cargaDiasEntrenamientoDeSemana:self.semanaActual];
}

- (void) actualizaGraficas {
    self.graficaActividadSemanal.semana = self.semanaActual;
    self.graficaProcesos.semana = self.semanaActual;
}

#pragma mark GraficaActividadSemanalDelegate GraficaProcesosDelegate
- (void) cargaSemanaAnterior {
    semana -= 1;
    //TODO If más allá del inicio del enternamiento, no o hagas
    //if(semana<=[self.datasource diasDeEntrenamientoSemana].count/7)
    //    semana+=1;

//    else {
        NSDate* fecha = [self.semanaActual dateByAddingTimeInterval:-7*24*60*60];
        self.semanaActual = fecha;
  //  }
    
}

- (void) cargaSemanaSiguiente {
    semana += 1;
    if(semana>0)
        semana = 0;
    else {
        NSDate* fecha = [self.semanaActual dateByAddingTimeInterval:7*24*60*60];
        self.semanaActual = fecha;
    }
}

#pragma mark GraficaActividadSemanalDatasource
- (NSInteger)maximoNumeroPorDia {
    return [self.datasource pacientes].count;
}

- (NSArray *)entrenamientosDeLaSemana {
    return self.datasource.entrenamientosPorDia;
}

#pragma mark GraficaProcesosDatasource
- (NSArray *)pacientesDeLaSemana {
    return self.datasource.pacientesDeLaSemana;
}

#pragma mark GraficaEdadDatasource
- (NSArray *) valoresGraficaEdad {
    return [self.datasource gruposDeEdad];
}

#pragma mark GraficaSexoDatasource
- (NSArray *) valoresGraficaSexo {
    return [self.datasource sexos];
}

#pragma mark UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pacientesPorEvento.count;
}

#pragma mark UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell;

    Paciente* paciente = [self.pacientesPorEvento objectAtIndex:indexPath.row];
    if(paciente.diasSinJugar>0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"inactivoCell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:@"inactivoCell"];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"activoCell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:@"activoCell"];
        }
    }
    
    [cell setValue:paciente forKey:@"paciente"];
    
    [[cell contentView] setBackgroundColor:[UIColor clearColor]];
    [[cell backgroundView] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Paciente* paciente = [self.pacientesPorEvento objectAtIndex:indexPath.row];
    if(paciente.diasSinJugar>0)
        return 71;
    return 156;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Paciente* paciente = [self.pacientesPorEvento objectAtIndex:indexPath.row];
    [self.delegate cargaFichaPaciente:paciente.objectId];
}

//pragma mark DEBUG
- (IBAction)testGongs:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TestEstadoGongsViewController* test = [storyboard instantiateViewControllerWithIdentifier:@"testGongs"];
    [self presentViewController:test animated:YES completion:nil];
}

- (IBAction)testInvasion:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TestEstadoInvasionViewController* test = [storyboard instantiateViewControllerWithIdentifier:@"testInvasion"];
    [self presentViewController:test animated:YES completion:nil];
}

- (IBAction)testPoker:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TestEstadoPokerViewController* test = [storyboard instantiateViewControllerWithIdentifier:@"testPoker"];
    [self presentViewController:test animated:YES completion:nil];
}

- (IBAction)generaPacientes:(id)sender {
    NSArray* hombres = @[@"ALEJANDRO", @"DAVID", @"DANIEL", @"PABLO", @"ADRIAN", @"ALVARO", @"JAVIER", @"SERGIO", @"CARLOS", @"MARCOS", @"IVAN", @"HUGO", @"MARIO", @"JORGE", @"DIEGO", @"MIGUEL", @"MANUEL", @"RAUL", @"RUBEN", @"ANTONIO", @"JUAN", @"ALEX", @"VICTOR", @"MARC", @"ALBERTO", @"IKER", @"JESUS", @"ANGEL", @"FRANCISCO", @"OSCAR", @"JOSE", @"SAMUEL", @"JAIME", @"ISMAEL", @"PAU", @"NICOLAS", @"MIGUEL ANGEL", @"LUIS", @"PEDRO", @"GONZALO", @"IGNACIO", @"GUILLERMO", @"RAFAEL", @"GABRIEL", @"JOEL", @"FERNANDO", @"FRANCISCO JAVIER", @"LUCAS", @"CRISTIAN", @"AARON", @"JOSE ANTONIO", @"ANDRES", @"POL", @"JOAN", @"RODRIGO", @"JOSE MANUEL", @"HECTOR", @"AITOR", @"ARNAU", @"ENRIQUE", @"SANTIAGO", @"IZAN", @"ROBERTO", @"ERIC", @"EDUARDO", @"JORDI", @"MARTIN", @"UNAI", @"JOSE LUIS", @"ASIER", @"JUAN JOSE", @"SERGI", @"MOHAMED", @"ADRIA", @"GERARD", @"KEVIN", @"JUAN MANUEL", @"JOSE MARIA", @"MARCO", @"BORJA", @"ORIOL", @"CHRISTIAN", @"JUAN CARLOS", @"YERAY", @"JUAN ANTONIO", @"ISAAC", @"XAVIER", @"FRANCISCO JOSE", @"JOAQUIN", @"JON", @"CESAR", @"GUILLEM", @"MARTI", @"ALBERT", @"JONATHAN"];
    
    NSArray* mujeres = @[@"LUCIA", @"MARIA", @"PAULA", @"LAURA", @"MARTA", @"ALBA", @"ANDREA", @"CLAUDIA", @"SARA", @"NEREA", @"CARLA", @"ANA", @"NATALIA", @"IRENE", @"MARINA", @"CRISTINA", @"ELENA", @"CARMEN", @"JULIA", @"AINHOA", @"ROCIO", @"SOFIA", @"ANGELA", @"NURIA", @"ALEJANDRA", @"SANDRA", @"ALICIA", @"CLARA", @"PATRICIA", @"CELIA", @"EVA", @"INES", @"NOELIA", @"DANIELA", @"LAIA", @"RAQUEL", @"SILVIA", @"ISABEL", @"LORENA", @"MIRIAM", @"ADRIANA", @"BLANCA", @"ARIADNA", @"AITANA", @"AROA", @"CANDELA", @"CAROLINA", @"BEATRIZ", @"CARLOTA", @"AINARA", @"VICTORIA", @"MIREIA", @"ANNA", @"LIDIA", @"JUDITH", @"MARTINA", @"MONICA", @"MAR", @"LARA", @"NAIARA", @"ERIKA", @"ANA MARIA", @"AINA", @"EMMA", @"YAIZA", @"PAOLA", @"ESTHER", @"LEIRE", @"HELENA", @"SONIA", @"SHEILA", @"BERTA", @"REBECA", @"GISELA", @"JUDIT", @"NOA", @"TERESA", @"FATIMA", @"NORA", @"BELEN", @"GABRIELA", @"NAYARA", @"VERONICA", @"JIMENA", @"LOLA", @"DESIREE", @"SALMA", @"ELSA", @"VALERIA", @"MARIA DEL CARMEN", @"ABRIL", @"LEYRE", @"ESTELA", @"NOEMI", @"IRIA"];
    
    NSArray* apellidosArr = @[@"GARCIA", @"GONZALEZ", @"RODRIGUEZ", @"FERNANDEZ", @"LOPEZ", @"MARTINEZ", @"SANCHEZ", @"PEREZ", @"GOMEZ", @"MARTIN", @"JIMENEZ", @"RUIZ", @"HERNANDEZ", @"DIAZ", @"MORENO", @"ALVAREZ", @"MUÑOZ", @"ROMERO", @"ALONSO", @"GUTIERREZ", @"NAVARRO", @"TORRES", @"DOMINGUEZ", @"VAZQUEZ", @"RAMOS", @"GIL", @"RAMIREZ", @"SERRANO", @"BLANCO", @"MOLINA", @"SUAREZ", @"MORALES", @"ORTEGA", @"DELGADO", @"CASTRO", @"ORTIZ", @"RUBIO", @"MARIN", @"SANZ", @"NUÑEZ", @"IGLESIAS", @"MEDINA", @"GARRIDO", @"SANTOS", @"CASTILLO", @"CORTES", @"LOZANO", @"GUERRERO", @"CANO", @"PRIETO", @"MENDEZ", @"CALVO", @"CRUZ", @"GALLEGO", @"VIDAL", @"LEON", @"HERRERA", @"MARQUEZ", @"PEÑA", @"CABRERA", @"FLORES", @"CAMPOS", @"VEGA", @"DIEZ", @"FUENTES", @"CARRASCO", @"CABALLERO", @"NIETO", @"REYES", @"AGUILAR", @"PASCUAL", @"HERRERO", @"SANTANA", @"LORENZO", @"HIDALGO", @"MONTERO", @"IBAÑEZ", @"GIMENEZ", @"FERRER", @"DURAN", @"VICENTE", @"BENITEZ", @"SANTIAGO", @"ARIAS", @"MORA", @"VARGAS", @"CARMONA", @"CRESPO", @"ROMAN", @"PASTOR", @"SOTO", @"SAEZ", @"VELASCO", @"SOLER", @"MOYA", @"ESTEBAN", @"PARRA", @"BRAVO", @"GALLARDO", @"ROJAS"];
    
    NSMutableArray* pacientes = [[NSMutableArray alloc] init];
    for (int p=64; p<90; p++) {
        NSString* nombre;
        NSNumber* sexo;
        int r = arc4random();
        if(r<0.5) {
            nombre = [hombres objectAtIndex:p];
            sexo = [NSNumber numberWithInt:1];
        } else {
            nombre = [mujeres objectAtIndex:p];
            sexo = [NSNumber numberWithInt:0];
        }

        NSString* apellidos = [NSString stringWithFormat:@"%@ %@", [apellidosArr objectAtIndex:arc4random()%apellidosArr.count], [apellidosArr objectAtIndex:arc4random()%apellidosArr.count]];
        
        NSString* nombrecorto = [nombre stringByReplacingOccurrencesOfString:@" " withString:@"_"].lowercaseString;
        NSString* email = [NSString stringWithFormat:@"%@%d@gmail.com", nombrecorto, p];
        NSDate* nacimiento = [NSDate dateWithTimeIntervalSince1970:30*365*24*60*60 + arc4random()%(14*365*24*60*60)];
        NSString* curso = @"Primero";
        NSString* pass = @"p";

        NSDictionary* campos = @{@"nombre":nombre.capitalizedString, @"apellidos":apellidos.capitalizedString, @"email":email, @"datos.fechaNacimiento":nacimiento, @"datos.curso":curso, @"datos.sexo":sexo, @"password":pass};
        
        Paciente* paciente = [Paciente creaConTerapeuta:self.datasource.terapeuta];
        paciente.datos = [DatosPaciente object];
        paciente.nivel = [NSNumber numberWithInt:0];
        paciente.puntos = [NSNumber numberWithInt:0];        
        for (NSString* ruta in campos) {
            [paciente setValue:[campos valueForKey:ruta] forKeyPath:ruta];
        }
        
        paciente.perfilcognitivo = [PerfilCognitivoPaciente object];
        NSArray* perfiles = @[@"memoriaOperativa", @"atencionSostenida", @"atencionSelectiva", @"atencionDividida", @"velocidadDeProcesamiento", @"tomaDeDecisiones", @"controlInhibitorio", @"flexibilidadCognitiva"];
        for (NSString* perfil in perfiles) {
            [paciente.perfilcognitivo setValue:[NSNumber numberWithInt:arc4random()%3] forKey:perfil];
        }

        Entrenamiento* entrenamiento = [EntrenamientosManager generaAutomaticoEntrenamientoParaPaciente:paciente];
        NSDate* ahora = [NSDate date];
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setDay:-(arc4random()%30)];
        [dateComponents setMonth:-1];
        entrenamiento.inicio = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:ahora options:kCFCalendarComponentsWrap];
        NSNumber* dias = [NSNumber numberWithInt:arc4random()%64];
        entrenamiento.estadoEntrenamiento.estadoGongs.diasSemana = dias;
        entrenamiento.estadoEntrenamiento.estadoInvasion.diasSemana = dias;
        entrenamiento.estadoEntrenamiento.estadoPoker.diasSemana = dias;
        [pacientes addObject:entrenamiento];
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *oneDay = [[NSDateComponents alloc] init];
        [oneDay setDay: 1];
        for (NSDate* date = entrenamiento.inicio; [DateUtils comparaSinHoraFecha:date conFecha:ahora] <= 0;
             date = [calendar dateByAddingComponents: oneDay
                                              toDate: date
                                             options: 0] ) {
                 int rand = arc4random();
                 if(rand>0.3) {
                     DiaEntrenamiento* diaEntrenamiento = [DiaEntrenamiento object];
                     diaEntrenamiento.fecha = date;
                     diaEntrenamiento.entrenamiento = entrenamiento;
                     
                     diaEntrenamiento.partidaGongs = [PartidaGongs object];
                     diaEntrenamiento.partidaGongs.sesiones = [[NSMutableArray alloc] init];
                     for (int s = 0; s<8; s++) {
                         ResultadoSesionGongs* resultado = [ResultadoSesionGongs resultadoAleatorio];
                         //float aleatorio = (float)rand() / RAND_MAX;
                         resultado.duracion = [NSNumber numberWithFloat:50.0];
                         [diaEntrenamiento.partidaGongs.sesiones addObject:resultado.valores];
                     }
                     diaEntrenamiento.partidaGongs.cambioNivel = [NSNumber numberWithInt:arc4random()%3-1];
                     
                     diaEntrenamiento.partidaInvasion = [PartidaInvasion object];
                     diaEntrenamiento.partidaInvasion.sesiones = [[NSMutableArray alloc] init];
                     for (int s = 0; s<8; s++) {
                         ResultadoSesionInvasion* resultado = [ResultadoSesionInvasion resultadoAleatorio];
                         //float aleatorio = (float)rand() / RAND_MAX;
                         resultado.duracion = [NSNumber numberWithFloat:50.0];
                         [diaEntrenamiento.partidaInvasion.sesiones addObject:resultado.valores];
                     }
                     diaEntrenamiento.partidaInvasion.cambioNivel = [NSNumber numberWithInt:arc4random()%3-1];
                     
                     diaEntrenamiento.partidaPoker = [PartidaPoker object];
                     diaEntrenamiento.partidaPoker.sesiones = [[NSMutableArray alloc] init];

                         ResultadoSesionPoker* resultado = [ResultadoSesionPoker resultadoAleatorio];
                         //float aleatorio = (float)rand() / RAND_MAX;
                         resultado.duracion = [NSNumber numberWithFloat:50.0];
                         [diaEntrenamiento.partidaPoker.sesiones addObject:resultado.valores];

                     diaEntrenamiento.partidaPoker.cambioNivel = [NSNumber numberWithInt:arc4random()%3-1];

                     [pacientes addObject:diaEntrenamiento];
                 }
             }
    }
    
    
    [ProgressHUD show:nil Interacton:NO];
    [self guardaObjeto:[NSNumber numberWithInt:0] deArray:pacientes];
}

- (void) guardaObjeto:(NSNumber *) numObjeto deArray:(NSArray *) array {
    PFObject* obj = [array objectAtIndex:numObjeto.intValue];
    [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!succeeded)
            NSLog(@"Error: %@", error);
        else {
            if(numObjeto.intValue>=array.count-1) {
                [ProgressHUD dismiss];
                NSLog(@"HECHO!");
            } else {
                NSLog(@"%d de %d", numObjeto.intValue, array.count);
                NSNumber*sig = [NSNumber numberWithInt:numObjeto.intValue+1];
                [self guardaObjeto:sig deArray:array];
            }
        }
    }];
}

- (IBAction)generaFechas:(id)sender {

    [ProgressHUD show:nil Interacton:NO];
    self.pacientesBorrame = [[NSMutableArray alloc] init];
    
    PFQuery* queryEntrenamientos = [PFQuery queryWithClassName:@"Entrenamiento"];
    [queryEntrenamientos whereKey:@"paciente" containedIn:self.datasource.pacientes];
    [queryEntrenamientos includeKey:@"paciente"];
    [queryEntrenamientos includeKey:@"estadoEntrenamiento"];
    [queryEntrenamientos includeKey:@"estadoEntrenamiento.estadoGongs"];
    [queryEntrenamientos includeKey:@"estadoEntrenamiento.estadoInvasion"];
    [queryEntrenamientos includeKey:@"estadoEntrenamiento.estadoPoker"];
    
    queryEntrenamientos.cachePolicy = kPFCachePolicyNetworkOnly;
    [queryEntrenamientos findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSLog(@"entrenamientos: %d", objects.count);
        [self generaFechasDeEntrenamiento:[NSNumber  numberWithInt:0] conObjetos:objects];
        
    }];

}

- (void) generaFechasDeEntrenamiento:(NSNumber *) numEntrenamiento conObjetos:(NSArray *) entrenamientos {

    Entrenamiento* entrenamiento = [entrenamientos objectAtIndex:numEntrenamiento.intValue];
    PFQuery* query = [PFQuery queryWithClassName:@"DiaEntrenamiento"];
    [query whereKey:@"entrenamiento" equalTo:entrenamiento];
    [query includeKey:@"entrenamiento"];
    [query includeKey:@"entrenamiento.paciente"];
    [query setLimit:1000];
    [query orderByAscending:@"fecha"];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        entrenamiento.paciente.ultimoEntrenamiento = [DateUtils fechaSinHora:((DiaEntrenamiento *)[objects lastObject]).fecha];
        entrenamiento.paciente.siguienteEntrenamiento = [DateUtils fechaSinHora:[entrenamiento fechaEntrenamientoDespuesDe:entrenamiento.paciente.ultimoEntrenamiento]];
        
        NSLog(@"%@", entrenamiento.paciente.ultimoEntrenamiento);
        NSLog(@"%@", entrenamiento.paciente.siguienteEntrenamiento);
        NSLog(@"/////////////////////////////////");
        [self.pacientesBorrame addObject:entrenamiento.paciente];
        
        if(numEntrenamiento.intValue<entrenamientos.count-1) {
            [self generaFechasDeEntrenamiento:[NSNumber numberWithInt:numEntrenamiento.intValue+1] conObjetos:entrenamientos];
        } else {

            [PFObject saveAllInBackground:self.pacientesBorrame block:^(BOOL succeeded, NSError *error) {
                [ProgressHUD dismiss];
                if(!succeeded)
                    NSLog(@"ERROR: %@", error);
                else
                    NSLog(@"GUARDADOS!");
            }];

            
        }
    }];

}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
