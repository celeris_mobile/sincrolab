//
//  TerapeutaViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TerapeutaViewController.h"

#import "ListadoPacientesViewController.h"
#import "EstadisticasViewController.h"

#import "PacientesTerapeutaModel.h"
#import "DiaEntrenamiento.h"

#import "TestEstadoPokerViewController.h"

@interface TerapeutaViewController ()
@property (weak, nonatomic) IBOutlet UIView *marcador;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnPacientes;
@property (weak, nonatomic) IBOutlet UIButton *btnEstadisticas;
@property (weak, nonatomic) IBOutlet UIButton *btnOpciones;
@property (weak, nonatomic) IBOutlet UIButton *btnSalir;
@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;

@property (weak, nonatomic) IBOutlet UIView *placeHolderView;

@property (nonatomic, weak) UIButton* botonSeleccionado;
@property (nonatomic, weak) UIButton* botonProvisional;
@property (nonatomic, strong) UIViewController* currentViewController;

@property (nonatomic, strong) PacientesTerapeutaModel* modeloPacientes;
@property (nonatomic, strong) NSString* pacienteAMostar;
@end

@implementation TerapeutaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.modeloPacientes = [[PacientesTerapeutaModel alloc] initWithTerapeuta:self.terapeuta];
    [self.modeloPacientes carga];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_terapeuta.jpg"]];
    
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.buttonsView.layer.shadowOffset = CGSizeMake(2, 0);
    self.buttonsView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.buttonsView.layer.shadowOpacity = 0.4;
    
    [self.btnHome setImage:[UIImage imageNamed:@"home_marcado.png"] forState:UIControlStateHighlighted];
    [self.btnHome setImage:[UIImage imageNamed:@"home_marcado.png"] forState:UIControlStateSelected];
    [self.btnPacientes setImage:[UIImage imageNamed:@"pacientes_marcado.png"] forState:UIControlStateHighlighted];
    [self.btnPacientes setImage:[UIImage imageNamed:@"pacientes_marcado.png"] forState:UIControlStateSelected];
    [self.btnEstadisticas setImage:[UIImage imageNamed:@"estadisticas_marcado.png"] forState:UIControlStateHighlighted];
    [self.btnEstadisticas setImage:[UIImage imageNamed:@"estadisticas_marcado.png"] forState:UIControlStateSelected];
    [self.btnOpciones setImage:[UIImage imageNamed:@"opciones_marcado.png"] forState:UIControlStateHighlighted];
    [self.btnOpciones setImage:[UIImage imageNamed:@"opciones_marcado.png"] forState:UIControlStateSelected];
    [self.btnSalir setImage:[UIImage imageNamed:@"inicio_marcado.png"] forState:UIControlStateHighlighted];
    [self.btnSalir setImage:[UIImage imageNamed:@"inicio_marcado.png"] forState:UIControlStateSelected];
    
    if(self.terapeuta)
        [self configuraVista];
    else {
        NSLog(@"No hay terapeuta?!=!");
        //TODO ¿Lanzamos un error?¿Ponemos un setter?
    }

    NSArray* array;
    [array objectEnumerator];
    [self botonDado:self.btnHome];
}


- (void) configuraVista {
    self.btnOpciones.hidden = YES;
    
    self.nombreLabel.text = self.terapeuta.nombre;
}

- (IBAction)botonDado:(UIButton *)boton {
    
    if(boton==self.btnSalir) {
        [self.delegate logout];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        
        if([self.currentViewController isKindOfClass:[ListadoPacientesViewController class]]&&
           ([((ListadoPacientesViewController *)self.currentViewController) hayCambios])) {
            
            self.botonProvisional = boton;
            
            [[[UIAlertView alloc] initWithTitle:@"Cambios sin guardar"
                                        message:@"Si continua se perderán todos los cambios que no haya guardado"
                                       delegate:self
                              cancelButtonTitle:@"Cancelar"
                              otherButtonTitles:@"Descartar", nil] show];
        } else {
            [self muestraSeccion:boton];
        }
    }
}

- (void) muestraSeccion:(UIButton *) boton {
    
    UIViewController* vc;
    if([boton isEqual:self.btnHome]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeTerapeuta"];
        ((HomeTerapeutaViewController *) vc).datasource = self.modeloPacientes;
        ((HomeTerapeutaViewController *) vc).delegate = self;
    }
    if([boton isEqual:self.btnPacientes]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"pacientesTerapeuta"];
        ((ListadoPacientesViewController *) vc).datasource = self.modeloPacientes;
        if(self.pacienteAMostar) {
            ((ListadoPacientesViewController *) vc).primeraSeleccion = self.pacienteAMostar;
            self.pacienteAMostar = nil;
        }
    }

    if([boton isEqual:self.btnEstadisticas]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"estadisticasGlobales"];
        ((EstadisticasViewController *) vc).modelo = self.modeloPacientes;
    }

    
    if(self.currentViewController) {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
    }
    
    self.currentViewController = vc;
    
    [self addChildViewController:self.currentViewController];
    self.currentViewController.view.frame = self.placeHolderView.bounds;
    [self.placeHolderView addSubview:self.currentViewController.view];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGRect marcadorFrame = self.marcador.frame;
                         marcadorFrame.origin.y = boton.frame.origin.y;
                         self.marcador.frame = marcadorFrame;
                     }];
    
    if(self.botonSeleccionado)
        self.botonSeleccionado.selected = NO;
    
    self.botonSeleccionado = boton;
    self.botonSeleccionado.selected = YES;
}

- (void)cargaFichaPaciente:(NSString *)pacienteId {
    self.pacienteAMostar = pacienteId;
    [self muestraSeccion:self.btnPacientes];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex==alertView.cancelButtonIndex)
        self.botonProvisional = nil;
    else
        [self muestraSeccion:self.botonProvisional];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
