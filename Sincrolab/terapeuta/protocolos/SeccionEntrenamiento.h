//
//  SeccionEntrenamiento.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entrenamiento.h"

@protocol SeccionEntrenamiento <NSObject>
@property (nonatomic, strong) Entrenamiento* entrenamiento;
@end
