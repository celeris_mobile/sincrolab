//
//  SeccionDatosPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntrenamientoPacienteModel.h"

@protocol SeccionDatosPaciente <NSObject>
- (BOOL) hayCambios;
@end
