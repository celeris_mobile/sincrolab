//
//  InfoViewController.m
//  Sincrolab
//
//  Created by Ricardo on 19/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()
@property (nonatomic, strong) UIWebView* webView;
@end

@implementation InfoViewController {
    NSString* tipo;
}

- (id)initWithInfo:(NSString *)info {
    self = [super init];
    if(self) {
        tipo = info;
        [self configura];
    }
    return self;
}

- (void) configura {
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Ok" style:UIBarButtonItemStylePlain target:self action:@selector(botonOkDado)];
    self.navigationItem.rightBarButtonItem = anotherButton;

    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.delegate = self;
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.webView];
    
    NSString* htmlString;
    if([tipo isEqualToString:@"ciencia"]) {
        self.title = @"Ciencia";
        htmlString = @"<font face='Lato-Bold' size='3'><h1>Neuroplasticidad.</h1></font>"
        @"<font face='Lato-Regular' size='3'><p>Nuestro cerebro tiene la capacidad para modificar su estructura en función de las experiencias vividas por el sujeto. En la Universidad de Londres UCL en el año 2000 tras el estudio de cientos de taxistas londinenses se comprobó como estos sujetos poseían una mayor actividad neuronal en áreas relacionadas con la memoria visoespacial que los sujetos controles. Haier et al 2009, mostraron que el entrenamiento cognitivo con videojuegos como el Tetris en un grupo de adolescentes creaban cambios estructurales y funcionales en el córtex. Hoekzema et al en 2010 mostró como el entrenamiento cognitivo puede aumentar la actividad eléctrica en áreas frontales y cerebelares en el cerebro de pacientes con TDAH. Más recientemente Brijidi y col en 2014 han demostrado como el aprendizaje y la memoria modifican el desarrollo molecular de nuestro cerebro.  Gazzaley y col en 2013 publicaron un trabajo en Nature en el que habían conseguido a través del entrenamiento en un videojuego (Neuroracer) una mejora significativa en atención sostenida y memoria de trabajo, en adultos mayores e incluso sus registros de EEG se parecían más a los adultos más jóvenes.</p>"
        @"<p>El entrenamiento cognitivo es una actividad que ofrece un beneficio como ayuda para los tratamientos de las dificultades de aprendizaje y estimula el neurodesarrollo.</p></font>";

    }
    
    if([tipo isEqualToString:@"sabermas"]) {
        self.title = @"¿Quieres saber más?";
        htmlString = @"<font face='Lato-Bold' size='3'><h1>Sobre Sincrolab</h1></font>"
        @"<font face='Lato-Regular'><p>Sincrolab app es una herramienta que le ayudará a gestionar, diseñar e implementar de manera más rápida y eficaz todos los tratamientos de sus pacientes que requieran de un entrenamiento neurocognitivo.</p>"
        @"<p>Si quiere disfrutar de todas las funcionalidades y ventajas de nuestro servicio personalizado,  deberá acceder a <a href='http://www.sincrolab.es'>www.sincrolab.es</a> o pedir información en <a href='mailto:info@sincrolab.es'>info@sincrolab.es</a> .</p>"
        @"<p>El servicio premium de Sicnrolab incluye</p>"
        @"<ul>"
            @"<li>Acceso a todas las tareas de Sincrolab:</li>"
            @"<ul>"
                @"<li>ToGong</li>"
                @"<li>Invasión Samurai</li>"
                @"<li>Poker Samurai</li>"
            @"</ul>"
            @"<li>Gestión de pacientes:</li>"
            @"<ul>"
                @"<li>Tendrás acceso a una herramienta sencilla e intuitiva donde introducir todos los datos personales e historia clínica de tus pacientes.</li>"
            @"</ul>"
            @"<li>Diseño  manual del entrenamiento:</li>"
            @"<ul>"
                @"<li>Capacidad para establecer todos los parámetros de las tareas creando un entrenamiento a tu gusto.</li>"
                @"<li>Capacidad para elegir el calendario semanal de las tareas a realizar.</li>"
            @"</ul>"
            @"<li>Diseño automático del entrenamiento:</li>"
            @"<ul>"
                @"<li>Con esta funcionalidad podrás diseñar los entrenamientos de tus pacientes a través de su perfil cognitivo personalizando al máximo su entrenamiento.</li>"
            @"</ul>"
            @"<li>Estadísticas:</li>"
            @"<ul>"
                @"<li>Supervisa de manera más adecuada y precisa la evolución de tus pacientes. Sincrolab app te muestra una gran cantidad de estadísticas intuitivas y gráficos muy claros sobre el conjunto de todos tus pacientes (tipos de perfiles más comunes, genero, edades, etc…) y su rendimiento (índices de mejora por tareas, por perfiles cognitivos, etc..).</li>"
            @"</ul>"
            @"<li>Servicio de ayuda:</li>"
            @"<ul>"
                @"<li>Acceso a un extenso manual con toda la información necesaria para el manejo de la aplicación.</li>"
                @"<li>Desde Sincrolab te ayudamos personalmente a través de nuestros web seminars a sacarle el mayor provecho a vuestra app.</li>"
                @"<li>Los web seminars se realizarán periódicamente con todas vuestras dudas y comentarios.</li>"
            @"</ul>"
        @"</ul>"
        @"<p>Para más información ponte en contacto con nosotros en <a href='mailto:info@sincrolab.es'>info@sincrolab.es</a></font>";
        
    }
    
    if([tipo isEqualToString:@"legal"]) {
        self.title = @"Aviso legal";
        htmlString = @"<font face='Lato-Regular'><h1>Información General:</h1>"
        @"<p>Sincrolab S.L ,  C/ duque de rivas, 5 28012, Madrid, <a href='mailto:info@sincrolab.es'>info@sincrolab.es</a></p>"
        @"<p>Inscrita en el registro mercantil de Madrid, tomo 31.673, Folio 111, Sección 8, Hoja M-5699918 informa a los usuarios que accedan o usen los servicios, aplicaciones, herramientas y, en general, contenidos incluidos en Sincrolab de las condiciones generales aplicables al acceso y navegación por el mismo.</p>"

        @"<h2>Utilización de la Aplicación.</h2>"
        @"<p>Por la mera descarga de la aplicación, usted adquiere la condición de usuario de la misma. El acceso y uso de la aplicación es responsabilidad exclusiva del usuario y significan su aceptación y conocimiento como usuario de todas y cada una de las presentes condiciones de uso.</p>"
        @"<p>Sincrolab podrá denegar o retirar en cualquier momento y sin necesidad de aviso previo, el acceso a la aplicación a aquellos usuarios que contravengan lo establecido en las presentes condiciones de uso, en la ley, las costumbres o el orden público.</p>"
        
        @"<h2>Obligaciones del usuario.</h2>"
        @"<p>El usuario se obliga a utilizar la aplicación de forma diligente, de conformidad con la ley y lo dispuesto en estas condiciones generales, y deberá abstenerse de utilizarlo en cualquier forma que pueda impedir, dañar o deteriorar el normal funcionamiento y disfrute de la aplicación por parte de los usuarios o que pudiera lesionar o causar daños a los bienes y derechos de Sincrolab, de otros usuarios o, en general, de cualquier tercero, respondiendo de cualesquiera daños que pudiera causar por la utilización de la aplicación y manteniendo a Sincrolab indemne frente a cualquier sanción, reclamación o demanda que pudiera interponerse por un tercero contra Sincrolab por la violación de cualesquiera derechos, propios o de terceros, mediante la utilización de la aplicación."
        @"<p>El usuario tiene la obligación de obtener el consentimiento de los datos de terceros que ingrese en la aplicación."
        
        @"<h2>Contacto.</h2>"
        @"<p>Para cualquier comunicación que sea precisa entre Sincrolab y el usuario, éste puede dirigirse a la dirección indicada al principio de las presentes condiciones generales o enviar un e-mail a info@sincrolab.es.</p>"
        
        @"<h2>Propiedad intelectual e industrial.</h2>"
        @"<p>El usuario reconoce que todos los elementos de la aplicación, la información y materiales contenidos en el mismo, las marcas, la estructura, selección, ordenación y presentación de sus contenidos, y los programas de ordenador utilizados en relación con ellos, están protegidos por derechos de propiedad intelectual e industrial de Sincrolab S.L o de terceros. El uso de la aplicación no le atribuye respecto a dichos derechos de propiedad industrial e intelectual ningún otro derecho distinto de los específicamente contemplados en la aplicación u otorgados a tenor de lo dispuesto en la legislación aplicable.</p>"
        
        @"<h2>Protección de datos personales.</h2>"
        @"<p>La información que facilite el usuario en el curso de la navegación se incorporará a un fichero del que es responsable Sincrolab, con el domicilio arriba indicado, para la finalidad de poder enviar información comercial relativa a Sincrolab. Los datos registrados por el usuario son de carácter personal e intransferible.</p>"
        @"<p>Los datos producidos del uso de la aplicación estarán guardados bajo un sistema de seguridad AWS.</p>"
        @"<p>El usuario puede ejercer sus derechos de acceso, rectificación, cancelación y oposición, dirigiéndose por escrito al domicilio arriba indicado, o enviando un e-mail a <a href='mailto:info@sincrolab.es'>info@sincrolab.es</a>.</p>"
        @"<p>Ultima renovación de la política de privacidad Enero de 2014, Sincrolab s.l se reserva el derecho de cambiar dicha política de privacidad sin previo aviso y poniendo las nuevas condiciones en este documento.</p>"
        
        @"<h2>Baja del servicio.</h2>"
        @"<p>Una vez el cliente se da de baja del servicio de SincroLab, los datos permanecerán en nuestra base de datos durante un año con la posibilidad de recuperarlos posteriormente se procederá al borrado de datos. El usuario puede ejercer sus derechos de borrado de datos en cualquier momento dirigiéndose por escrito a el domicilio arriba indicado, o enviando un e-mail a info@sincrolab.es</p>"
        
        @"<h2>Responsabilidad.</h2>"
        @"<p>Sincrolab no se hace responsable de los perjuicios que se pudieran derivar de, con carácter meramente enunciativo y no limitativo: (i) inferencias, omisiones, interrupciones, virus informáticos, averías y/o desconexiones en el funcionamiento operativo de este sistema electrónico o en los aparatos y equipos informáticos de los usuarios, motivadas por causas ajenas a Sincrolab que impidan o retrasen la prestación de los servicios o la navegación por el sistema; (ii) retrasos o bloqueos en el uso causados por deficiencias o sobrecargas de Internet o en otros sistemas electrónicos; (iii) de la imposibilidad de dar el servicio o permitir el acceso por causas no imputables a Sincrolab, debidas al usuario, a terceros, o a supuestos de fuerza mayor.</p>"
        @"<p>Sincrolab ofrece un servicio de entrenamiento cognitivo no sustituyendo en ningún caso a la terapia neuropsicológica, psicológica o médica. Sincrolab no se hace responsable de un mal uso de la aplicación por parte del usuario.</p>"
        @"<p>Sincrolab no se hace responsable de obtener el consentimiento de los datos o imágenes de terceras personas que el cliente introduzca en la aplicación, es la obligación del cliente obtener los consentimientos de dichas personas.</p>"
        
        @"<h2>Ley aplicable y fuero.</h2>"
        @"<p>Las disposiciones contenidas en estas condiciones generales se regirán e interpretarán de conformidad con la ley española. Salvo que la ley expresamente lo prohíba, para la resolución de todas las cuestiones litigiosas derivadas de las presentes condiciones generales, o relativas al incumplimiento, interpretación, resolución o validez de cualquier disposición de la aplicación, el usuario acepta someterse al fuero y jurisdicción de los Juzgados y Tribunales de Madrid.</p>"
        
        
        @"<h1>TÉRMINOS Y CONDICIONES DE USO</h1>"
        
        @"<p>Sincrolab S.L (en adelante, “Sincrolab”), con domicilio duque de rivas,5 28012, como titular de la aplicación  Sincrolab app ( en adelante, aplicación) le informa de que los presentes términos y condiciones se aplicarán a todas las transacciones de venta de productos que se efectúen a través del la aplicación. Para conocer las condiciones de uso de la aplicación, lea nuestro Aviso Legal.</p>"
        @"<p>Las condiciones de cada transacción serán las que se encuentren vigentes en el momento de ejecutar la misma.</p>"
        
        @"<h2>Prestación del servicio</h2>"
        @"<p>Sincrolab s.l. podrá dejar de prestar el servicio en cualquier momento y con previo aviso, cancelando las cuentas de usuarios existentes hasta el momento.</p>"
        @"<p>El usuario, salvo contrato suscrito por separado con la empresa o en su caso habilitación expresa por parte de Sincrolab, s.l., se obliga a no reproducir fuera de Sincrolab, duplicar, copiar, vender, comercializar o reutilizar comercialmente los contenidos de éste.</p>"
        @"<p>Sincrolab s.l se reserva el derecho de cambiar los precios del servicio en cualquier momento previo aviso vía mail a los usuarios del servicio.</p>"
        
        @"<h2>Obligaciones del cliente.</h2>"
        @"<p>Usted se compromete en todo momento a facilitar información veraz sobre los datos de los usuarios obteniendo el permiso pertinente para facilitar los datos a Sincrolab.</p>"
        
        @"<h2>Protección de datos personales.</h2>"
        @"<p>Consulte nuestra política de privacidad en el Aviso Legal.</p>"
        
        @"<h2>Derecho aplicable y jurisdicción.</h2>"
        @"<p>Los términos y condiciones de uso se regirán e interpretarán de conformidad con la ley española. Salvo que la ley expresamente lo prohíba, para la resolución de todas las cuestiones litigiosas derivadas de los presentes términos y condiciones de venta, usted acepta someterse al fuero y jurisdicción de los Juzgados y Tribunales de Madrid.</p>"
        
        @"<h2>Contacto.</h2>"
        @"<p>Puede dirigir sus preguntas y reclamaciones a <a href='mailto:info@sincrolab.es'>info@sincrolab.es</a> o contactar con nosotros por escrito en el domicilio arriba indicado.</p></font>";

    }
    
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    return YES;
}

- (void) botonOkDado {

    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate cierraInfo];
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
