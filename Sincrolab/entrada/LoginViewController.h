//
//  LoginViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Parse/Parse.h>
#import "UsuariosManagerDelegate.h"

@protocol LoginViewControllerDelegate <NSObject>
- (void) loggedIn;
- (void) sinRegistro;
- (void) btnInfoDado:(NSString *) info;
@end

@interface LoginViewController : PFLogInViewController <PFLogInViewControllerDelegate>
@property (nonatomic, weak) id<LoginViewControllerDelegate> loginDelegate;
@end
