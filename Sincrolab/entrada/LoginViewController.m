//
//  ViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "LoginViewController.h"
#import "Terapeuta.h"
#import "Paciente.h"
#import "UIBAlertView.h"

@interface LoginViewController ()
@property (nonatomic, strong) UIImageView* iconoUsuario;
@property (nonatomic, strong) UIImageView* iconoPass;
@property (nonatomic, strong) UIView* campoUsuario;
@property (nonatomic, strong) UIView* campoPass;
@property (nonatomic, strong) UIButton* botonNoRegistro;

@property (nonatomic, strong) UIButton* btnCiencia;
@property (nonatomic, strong) UIButton* btnSaberMas;
@property (nonatomic, strong) UIButton* btnLegal1;
@end

@implementation LoginViewController

- (id)init {
    self = [super init];
    if(self) {
        self.fields = PFLogInFieldsUsernameAndPassword|PFLogInFieldsPasswordForgotten|PFLogInFieldsLogInButton|PFLogInFieldsSignUpButton;
        self.delegate = self;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.logInView.backgroundColor = [UIColor clearColor];
    
    [self.logInView.logo removeFromSuperview];
    [self.logInView.signUpLabel removeFromSuperview];
    
    //Fondo
    CALayer* datosLayer = [CALayer layer];
    datosLayer.backgroundColor = [UIColor whiteColor].CGColor;
    datosLayer.cornerRadius = 5.0;
    datosLayer.frame = CGRectMake(0, 7, 342, 140);
    [self.view.layer insertSublayer:datosLayer atIndex:0];
    
    CALayer* btnEntrarLayer = [CALayer layer];
    btnEntrarLayer.backgroundColor = [UIColor colorWithRed:115/255.0 green:218/255.0 blue:163/255.0 alpha:1.0].CGColor;
    btnEntrarLayer.frame = CGRectMake(0, 137, 342, 96);
    [self.view.layer insertSublayer:btnEntrarLayer atIndex:1];
    
    CALayer* registroLayer = [CALayer layer];
    registroLayer.backgroundColor = [UIColor colorWithRed:31/255.0 green:171/255.0 blue:124/255.0 alpha:1.0].CGColor;
    registroLayer.frame = CGRectMake(0, 237, 342, 124);
    [self.view.layer insertSublayer:registroLayer atIndex:0];
    
    
    //Campos
    self.campoUsuario = [[UIView alloc] initWithFrame:CGRectMake(26, 33, 287, 36)];
    self.campoUsuario.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.campoUsuario.layer.borderWidth = 1.0;
    [self.logInView addSubview:self.campoUsuario];
    
    UIImage* imgUsuario = [UIImage imageNamed:@"iconousuario.png"];
    self.iconoUsuario = [[UIImageView alloc] initWithImage:imgUsuario];
    [self.campoUsuario addSubview:self.iconoUsuario];
    
    self.logInView.usernameField.keyboardType = UIKeyboardTypeEmailAddress;
    self.logInView.usernameField.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    self.logInView.usernameField.textColor = [UIColor darkGrayColor];
    self.logInView.usernameField.textAlignment = NSTextAlignmentLeft;
    self.logInView.usernameField.layer.shadowOpacity = 0.0;
    [self.campoUsuario addSubview:self.logInView.usernameField];
    self.logInView.usernameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    self.campoPass = [[UIView alloc] initWithFrame:CGRectMake(26, 80, 287, 36)];
    self.campoPass.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.campoPass.layer.borderWidth = 1.0;
    [self.logInView addSubview:self.campoPass];
    
    UIImage* imgPass = [UIImage imageNamed:@"iconopass.png"];
    self.iconoPass = [[UIImageView alloc] initWithImage:imgPass];
    [self.campoPass addSubview:self.iconoPass];
    
    self.logInView.passwordField.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    self.logInView.passwordField.textColor = [UIColor darkGrayColor];
    self.logInView.passwordField.textAlignment = NSTextAlignmentLeft;
    self.logInView.passwordField.layer.shadowOpacity = 0.0;
    [self.campoPass addSubview:self.logInView.passwordField];
    self.logInView.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contraseña" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    //Botón entrar
    self.logInView.logInButton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    [self.logInView.logInButton setTitle:@"ENTRAR" forState:UIControlStateNormal];
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"btnentrar.png"] forState:UIControlStateNormal];
    
    //Boton registro y recuperar
    [self.logInView.signUpButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setImage:[UIImage imageNamed:@"btnregistrarse.png"] forState:UIControlStateNormal];
    [self.logInView.signUpButton setImage:nil forState:UIControlStateHighlighted];
    
    [self.logInView.passwordForgottenButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.logInView.passwordForgottenButton setImage:[UIImage imageNamed:@"btnrecuperar.png"] forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setImage:nil forState:UIControlStateHighlighted];
    
    //Label external login
    /*
    self.logInView.externalLogInLabel.textColor = [UIColor whiteColor];
    self.logInView.externalLogInLabel.text = @"TAMBIÉN PUEDES ENTRAR CON";
    self.logInView.externalLogInLabel.font = [UIFont fontWithName:@"Lato-Bold" size:12];
    */
    
    
    self.botonNoRegistro = [[UIButton alloc] init];
    [self.botonNoRegistro setImage:[UIImage imageNamed:@"btnsinregistro.png"] forState:UIControlStateNormal];
    [self.botonNoRegistro addTarget:self action:@selector(sinRegistro) forControlEvents:UIControlEventTouchUpInside];
    [self.logInView addSubview:self.botonNoRegistro];
    
    /*
    UIButton* botonTerapeuta = [[UIButton alloc] initWithFrame:CGRectMake(2, 2, 100, 30)];
    [botonTerapeuta setTitle:@"Terapeuta" forState:UIControlStateNormal];
    [botonTerapeuta setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    botonTerapeuta.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:10];
    botonTerapeuta.titleLabel.textAlignment = NSTextAlignmentLeft;
    [botonTerapeuta addTarget:self action:@selector(terapeutaDado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:botonTerapeuta];
    
    UIButton* botonPaciente = [[UIButton alloc] initWithFrame:CGRectMake(100, 2, 100, 30)];
    [botonPaciente setTitle:@"Paciente" forState:UIControlStateNormal];
    [botonPaciente setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    botonPaciente.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:10];    
    botonPaciente.titleLabel.textAlignment = NSTextAlignmentLeft;
    [botonPaciente addTarget:self action:@selector(pacienteDado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:botonPaciente];
    */
    
    self.btnCiencia = [[UIButton alloc] initWithFrame:CGRectMake(342-250, 364, 80, 20)];
    [self.btnCiencia.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14]];
    [self.btnCiencia addTarget:self action:@selector(botonInfoDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCiencia setTitle:@"Ciencia" forState:UIControlStateNormal];
    [self.view addSubview:self.btnCiencia];
    
    self.btnSaberMas = [[UIButton alloc] initWithFrame:CGRectMake(342-165, 364, 80, 20)];
    [self.btnSaberMas.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14]];
    [self.btnSaberMas addTarget:self action:@selector(botonInfoDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSaberMas setTitle:@"Saber Más" forState:UIControlStateNormal];
    [self.view addSubview:self.btnSaberMas];
    
    self.btnLegal1 = [[UIButton alloc] initWithFrame:CGRectMake(342-80, 364, 80, 20)];
    [self.btnLegal1.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14]];
    [self.btnLegal1 addTarget:self action:@selector(botonInfoDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLegal1 setTitle:@"Legal" forState:UIControlStateNormal];
    [self.view addSubview:self.btnLegal1];
}

- (void) botonInfoDado:(UIButton *) boton {
    [self dismissViewControllerAnimated:YES completion:^{
        if(boton==self.btnCiencia)
            [self.loginDelegate btnInfoDado:@"ciencia"];
        if(boton==self.btnSaberMas)
            [self.loginDelegate btnInfoDado:@"sabermas"];
        if(boton==self.btnLegal1)
            [self.loginDelegate btnInfoDado:@"legal"];

    }];    
}

- (void) sinRegistro {
    UIBAlertView* alertView = [[UIBAlertView alloc] initWithTitle:@"Atención" message:@"Si te registras guardaremos tu entrenamiento y podrás ver tu progreso. ¿Quieres seguir sin registrarte?" cancelButtonTitle:@"No" otherButtonTitles:@"Sí", nil];
    [alertView showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
        if(!didCancel) {
            [self dismissViewControllerAnimated:YES completion:^{
                [self.loginDelegate sinRegistro];
            }];
        }
    }];
}


- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    logInController.logInView.usernameField.text = [logInController.logInView.usernameField.text lowercaseString];
    return YES;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.view.superview.backgroundColor = [UIColor clearColor];
    self.view.superview.bounds = CGRectMake(0, -150, 342, 528);
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.logInView.usernameField.frame = CGRectMake(self.iconoUsuario.frame.size.width+10, 0, 244-20, 36);
    self.campoUsuario.frame = CGRectMake(26, 33, 287, 36);
    
    self.logInView.passwordField.frame = CGRectMake(self.iconoPass.frame.size.width+10, 0, 244-20, 36);
    self.campoPass.frame = CGRectMake(26, 80, 287, 36);
    
    self.logInView.logInButton.frame = CGRectMake(74, 157, 196, 55);
    
    self.logInView.signUpButton.frame = CGRectMake(13, 253, 140, 39);
    self.logInView.passwordForgottenButton.frame = CGRectMake(160, 253, 168, 39);
    
    self.logInView.externalLogInLabel.frame = CGRectMake(13, 317, 193, 13);
    
    self.botonNoRegistro.frame = CGRectMake(12, 300, 318, 46);
}

/*
- (void) terapeutaDado {
    self.logInView.usernameField.text = @"terap@terap.com";
    self.logInView.passwordField.text = @"p";
}

- (void) pacienteDado {
    self.logInView.usernameField.text = @"paciente@paciente.com";
    self.logInView.passwordField.text = @"p";
}
*/
- (void) logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.loginDelegate loggedIn];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
