//
//  GranSeccionDelegate.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GranSeccionDelegate <NSObject>
- (void) logout;
@end
