//
//  RegistroViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RegistroViewController.h"

@interface RegistroViewController ()
@property (nonatomic, strong) UIImageView* iconoUsuario;
@property (nonatomic, strong) UIImageView* iconoEmail;
@property (nonatomic, strong) UIImageView* iconoPass;
@property (nonatomic, strong) UIView* campoUsuario;
@property (nonatomic, strong) UIView* campoEmail;
@property (nonatomic, strong) UIView* campoPass;
@property (nonatomic, strong) UIButton* botonNoRegistro;
@end

@implementation RegistroViewController

- (id)init {
    self = [super init];
    if(self) {
        self.fields = PFSignUpFieldsUsernameAndPassword|PFSignUpFieldsEmail|PFSignUpFieldsDismissButton|PFSignUpFieldsSignUpButton;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.signUpView.backgroundColor = [UIColor clearColor];
    
    [self.signUpView.logo removeFromSuperview];
    
    //Fondo
    CALayer* datosLayer = [CALayer layer];
    datosLayer.backgroundColor = [UIColor whiteColor].CGColor;
    datosLayer.cornerRadius = 5.0;
    datosLayer.frame = CGRectMake(0, 7, 342, 130);
    [self.view.layer insertSublayer:datosLayer atIndex:0];
    
    CALayer* btnEntrarLayer = [CALayer layer];
    btnEntrarLayer.backgroundColor = [UIColor whiteColor].CGColor;
    btnEntrarLayer.frame = CGRectMake(0, 137, 342, 96);
    [self.view.layer insertSublayer:btnEntrarLayer atIndex:1];
    
    CALayer* registroLayer = [CALayer layer];
    registroLayer.backgroundColor = [UIColor colorWithRed:31/255.0 green:171/255.0 blue:124/255.0 alpha:1.0].CGColor;
    registroLayer.frame = CGRectMake(0, 237, 342, 124);
    [self.view.layer insertSublayer:registroLayer atIndex:0];
    

    //Campo usuario
    self.campoUsuario = [[UIView alloc] initWithFrame:CGRectMake(26, 33, 287, 36)];
    self.campoUsuario.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.campoUsuario.layer.borderWidth = 1.0;
    [self.signUpView addSubview:self.campoUsuario];
    
    UIImage* imgUsuario = [UIImage imageNamed:@"iconousuario.png"];
    self.iconoUsuario = [[UIImageView alloc] initWithImage:imgUsuario];
    [self.campoUsuario addSubview:self.iconoUsuario];
    
    self.signUpView.usernameField.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    self.signUpView.usernameField.textColor = [UIColor darkGrayColor];
    self.signUpView.usernameField.textAlignment = NSTextAlignmentLeft;
    self.signUpView.usernameField.layer.shadowOpacity = 0.0;
    [self.campoUsuario addSubview:self.signUpView.usernameField];
    self.signUpView.usernameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nombre" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    //Campo email
    self.campoEmail = [[UIView alloc] initWithFrame:CGRectMake(26, 80, 287, 36)];
    self.campoEmail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.campoEmail.layer.borderWidth = 1.0;
    [self.signUpView addSubview:self.campoEmail];
    
    UIImage* imgEmail = [UIImage imageNamed:@"iconousuario.png"];
    self.iconoEmail = [[UIImageView alloc] initWithImage:imgEmail];
    [self.campoEmail addSubview:self.iconoEmail];
    
    self.signUpView.emailField.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    self.signUpView.emailField.textColor = [UIColor darkGrayColor];
    self.signUpView.emailField.textAlignment = NSTextAlignmentLeft;
    self.signUpView.emailField.layer.shadowOpacity = 0.0;
    [self.campoEmail addSubview:self.signUpView.emailField];
    self.signUpView.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    //Campo pass
    self.campoPass = [[UIView alloc] initWithFrame:CGRectMake(26, 127, 287, 36)];
    self.campoPass.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.campoPass.layer.borderWidth = 1.0;
    [self.signUpView addSubview:self.campoPass];
    
    UIImage* imgPass = [UIImage imageNamed:@"iconopass.png"];
    self.iconoPass = [[UIImageView alloc] initWithImage:imgPass];
    [self.campoPass addSubview:self.iconoPass];
    
    self.signUpView.passwordField.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    self.signUpView.passwordField.textColor = [UIColor darkGrayColor];
    self.signUpView.passwordField.textAlignment = NSTextAlignmentLeft;
    self.signUpView.passwordField.layer.shadowOpacity = 0.0;
    [self.campoPass addSubview:self.signUpView.passwordField];
    self.signUpView.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contraseña" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];

    
    //Botón registrarse
    self.signUpView.signUpButton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    [self.signUpView.signUpButton setTitle:@"REGISTRARSE" forState:UIControlStateNormal];
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"btnentrar.png"] forState:UIControlStateNormal];
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"btnentrar.png"] forState:UIControlStateHighlighted];
    
    //Botón cancelar
    self.signUpView.dismissButton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    [self.signUpView.dismissButton setTitle:@"CANCELAR" forState:UIControlStateNormal];
    [self.signUpView.dismissButton setImage:nil forState:UIControlStateNormal];
    [self.signUpView.dismissButton setImage:nil forState:UIControlStateHighlighted];
    self.signUpView.dismissButton.layer.borderWidth = 1.0;
    self.signUpView.dismissButton.layer.borderColor = [UIColor colorWithRed:87/255.0 green:202/255.0 blue:151/255.0 alpha:1.0].CGColor;

    /*
    //Boton registro y recuperar
    [self.logInView.signUpButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setImage:[UIImage imageNamed:@"btnregistrarse.png"] forState:UIControlStateNormal];
    [self.logInView.signUpButton setImage:nil forState:UIControlStateHighlighted];
    
    [self.logInView.passwordForgottenButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.logInView.passwordForgottenButton setImage:[UIImage imageNamed:@"btnrecuperar.png"] forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setImage:nil forState:UIControlStateHighlighted];
    
    //Label external login
    self.logInView.externalLogInLabel.textColor = [UIColor whiteColor];
    self.logInView.externalLogInLabel.text = @"TAMBIÉN PUEDES ENTRAR CON";
    self.logInView.externalLogInLabel.font = [UIFont fontWithName:@"Lato-Bold" size:12];
    
    
    
    self.botonNoRegistro = [[UIButton alloc] init];
    [self.botonNoRegistro setImage:[UIImage imageNamed:@"btnsinregistro.png"] forState:UIControlStateNormal];
    [self.logInView addSubview:self.botonNoRegistro];
    */
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.signUpView.usernameField.frame = CGRectMake(self.iconoUsuario.frame.size.width+10, 0, 244-20, 36);
    self.campoUsuario.frame = CGRectMake(26, 43, 287, 36);
    
    self.signUpView.emailField.frame = CGRectMake(self.iconoEmail.frame.size.width+10, 0, 244-20, 36);
    self.campoEmail.frame = CGRectMake(26, 100, 287, 36);
    
    self.signUpView.passwordField.frame = CGRectMake(self.iconoPass.frame.size.width+10, 0, 244-20, 36);
    self.campoPass.frame = CGRectMake(26, 157, 287, 36);

    self.signUpView.signUpButton.frame = CGRectMake(12, 247, 318, 46);
    self.signUpView.dismissButton.frame = CGRectMake(12, 300, 318, 46);
    /*
    self.signUpView.signUpButton.frame = CGRectMake(13, 253, 140, 39);
    self.signUpView.passwordForgottenButton.frame = CGRectMake(160, 253, 168, 39);
    
    self.signUpView.externalLogInLabel.frame = CGRectMake(13, 317, 193, 13);

    self.botonNoRegistro.frame = CGRectMake(12, 300, 318, 46);
         */
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.view.superview.backgroundColor = [UIColor clearColor];
    self.view.superview.bounds = CGRectMake(0, -150, 342, 498);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
