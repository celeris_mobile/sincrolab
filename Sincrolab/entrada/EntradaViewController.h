//
//  EntradaViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UsuariosManagerDelegate.h"
#import "LoginViewController.h"
#import "GranSeccionDelegate.h"
#import "RegistroViewController.h"
#import "InfoViewController.h"
@interface EntradaViewController : UIViewController <UsuariosManagerDelegate, LoginViewControllerDelegate, GranSeccionDelegate, PFSignUpViewControllerDelegate, InfoViewControllerDelegate>

- (void) logout;

@end
