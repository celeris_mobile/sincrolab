//
//  InfoViewController.h
//  Sincrolab
//
//  Created by Ricardo on 19/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InfoViewControllerDelegate <NSObject>
- (void) cierraInfo;
@end

@interface InfoViewController : UIViewController <UIWebViewDelegate>
- (id) initWithInfo:(NSString *) info;
@property (nonatomic, weak) id<InfoViewControllerDelegate> delegate;
@end
