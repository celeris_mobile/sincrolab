//
//  EntradaViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntradaViewController.h"
#import "UsuariosManager.h"
#import "LoginViewController.h"
#import "TerapeutaViewController.h"
#import "PacienteViewController.h"
#import "Paciente.h"
#import "Terapeuta.h"
#import "Configuraciones.h"
#import "TestEstadoGongsViewController.h"
#import "RegistroViewController.h"
#import "PacientesManager.h"
#import "EntrenamientosManager.h"
#import "InfoViewController.h"


@interface EntradaViewController ()
@property (nonatomic, strong) UsuariosManager* usuariosManager;
@end

@implementation EntradaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_inicio.png"]];
    
	
    self.usuariosManager = [[UsuariosManager alloc] init];
    self.usuariosManager.delegate = self;
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self generaLogin];
}

- (void) generaLogin {
    
    [[Configuraciones sharedConf] genera:^{
        if(![self.usuariosManager hayUsuarioLoggeado]) {
            LoginViewController* loginVC = [[LoginViewController alloc] init];
            loginVC.loginDelegate = self;
            
            RegistroViewController *signUpViewController = [[RegistroViewController alloc] init];
            signUpViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            [signUpViewController setDelegate:self];
            [loginVC setSignUpController:signUpViewController];
            
            
            loginVC.modalPresentationStyle = UIModalPresentationFormSheet;
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UsuariosManagerDelegate
- (void)usuarioLoggedIn:(Usuario *)usuario {
    UIViewController* vc;
    
    if([usuario isKindOfClass:[Terapeuta class]]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"terapeuta"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        ((TerapeutaViewController *)vc).delegate = self;
        ((TerapeutaViewController *)vc).terapeuta = (Terapeuta *)usuario;
    } else if([usuario isKindOfClass:[Paciente class]]) {
        vc = [[PacienteViewController alloc] init];
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        ((PacienteViewController *)vc).delegate = self;
        ((PacienteViewController *)vc).paciente = (Paciente *)usuario;
    } else {
        [self logout];
        return;
    }
    
    //vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)usuarioLoggedOut {
    //¿No hacemos nada?
    NSLog(@"vuelve a la entrada");
}

- (void)errorLogeandoUsuario:(NSError *)error {
    //TODO Gestionar este error
    [self logout];
}

#pragma mark LoginViewControllerDelegate
- (void) loggedIn {
    [self.usuariosManager hayUsuarioLoggeado];
}

- (void) sinRegistro {
    UIViewController* vc;
    
    vc = [[PacienteViewController alloc] init];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    ((PacienteViewController *)vc).delegate = self;
    ((PacienteViewController *)vc).paciente = nil;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)btnInfoDado:(NSString *)info {
    InfoViewController* vc = [[InfoViewController alloc] initWithInfo:info];
    vc.delegate = self;
    
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:vc];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)cierraInfo {
    [self generaLogin];
}

#pragma mark GranSeccionDelegate
- (void)logout {
    [self.usuariosManager logOutUsuario];
}

- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    
    PFQuery* queryUsuario = [PFUser query];
    [queryUsuario whereKey:@"tipo" equalTo:@"terapeuta_anonimo"];
    
    PFQuery* queryTerapeuta = [PFQuery queryWithClassName:@"Terapeuta"];
    [queryTerapeuta whereKey:@"usuario" matchesQuery:queryUsuario];
    [queryTerapeuta findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!objects.count) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Ha ocurrido un error, inténtalo de nuevo más tarde" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {

            NSDictionary* campos = @{@"nombre":signUpController.signUpView.usernameField.text,
                                     @"apellidos":@"-",
                                     @"datos.fechaNacimiento":[NSDate date],
                                     @"datos.curso":@"-",
                                     @"email":signUpController.signUpView.emailField.text,
                                     @"password":signUpController.signUpView.passwordField.text};
                                     
            [PacientesManager nuevoPacienteDeTerapeuta:[objects firstObject] ConCampos:campos completado:^(NSError * error, Paciente * paciente) {
                if(error)
                    [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Ha ocurrido un error, inténtalo de nuevo más tarde" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                else {
                    Entrenamiento* entrenamiento = [EntrenamientosManager generaEntrenamientoParaPacienteAnonimo:paciente];
                    [EntrenamientosManager guardaEntrenamiento:entrenamiento inicial:YES completado:^(NSError * error) {
                        if(error)
                            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Ha ocurrido un error, inténtalo de nuevo más tarde" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                        else
                            [signUpController dismissViewControllerAnimated:YES completion:nil];
                    }];
                }                
            }];
        }
    }];
    

    return NO;
}

@end
