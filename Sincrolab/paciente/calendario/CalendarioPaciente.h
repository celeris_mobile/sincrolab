//
//  CalendarioPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Calendario.h"
#import "Entrenamiento.h"

@interface CalendarioPaciente : UIView <CalendarioDelegate>
@property (nonatomic, strong) Entrenamiento* entrenamiento;
@end
