//
//  CalendarioPacienteViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CalendarioPacienteViewController.h"
#import "CalendarioPaciente.h"

@interface CalendarioPacienteViewController ()
@property (nonatomic, strong) CalendarioPaciente* calendario;

@end

@implementation CalendarioPacienteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView* sombra = [[UIView alloc] initWithFrame:CGRectMake(77+9, 101+9, 873, 533)];
    sombra.layer.cornerRadius = 20.0;
    sombra.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [self.view addSubview:sombra];
    
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(77, 101, 873, 533)];
    fondo.clipsToBounds = YES;
    
    UIView* zonaOscura = [[UIView alloc] initWithFrame:CGRectMake(544, 0, 873-544, 533)];
    zonaOscura.backgroundColor = [UIColor colorWithRed:47/255.0 green:43/255.0 blue:41/255.0 alpha:1.0];
    [fondo addSubview:zonaOscura];
    
    fondo.backgroundColor = [UIColor whiteColor];
    fondo.layer.cornerRadius = 20.0;
    [self.view addSubview:fondo];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.calendario = [[CalendarioPaciente alloc] initWithFrame:CGRectMake(25, 22, 492, 493)];
    [fondo addSubview:self.calendario];
}

- (void) setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    
    if(!self.modelo)
        return;
    
    if(self.isViewLoaded)
        [self compruebaEntrenamiento];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self compruebaEntrenamiento];
}

- (void) compruebaEntrenamiento {
    if(self.modelo.cargado)
        [self generaCalendario];
    else {
        [self.modelo carga:^(NSError * error) {
            if(!error) {
                [self generaCalendario];
            }
        } conProgreso:YES];
    }
}

- (void) generaCalendario {
    if(self.modelo.paciente.entrenamiento) {
        //self.textoNoHay.hidden = YES;
        [self.calendario setEntrenamiento:self.modelo.paciente.entrenamiento];
        self.calendario.hidden = NO;
    } else {
        //self.textoNoHay.hidden = NO;
        self.calendario.hidden = YES;
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
