//
//  CalendarioPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CalendarioPaciente.h"
#import "Calendario.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "DateUtils.h"
#import "DiaCalendarioPaciente.h"
#import "CustomPopOverBackgroundView.h"
#import "DiaCell.h"

@interface CalendarioPaciente()
@property (nonatomic, strong) NSCalendar* gregorianCalendar;
@property (nonatomic, strong) Calendario* calendario;
@property (nonatomic, strong) UIButton* botonSiguiente;
@property (nonatomic, strong) UIButton* botonAnterior;
@property (nonatomic, strong) UILabel* labelMes;
@property (nonatomic, strong) UIView* leyenda;
@property (nonatomic, strong) NSDictionary* diasEntrenamiento;
@property (nonatomic, strong) DiaCalendarioPaciente* diaCalendario;
@end

@implementation CalendarioPaciente

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [self.gregorianCalendar setFirstWeekday:2];
        
    }
    return self;
}

- (void)setEntrenamiento:(Entrenamiento *)entrenamiento {
    _entrenamiento = entrenamiento;
    
    NSMutableDictionary* auxDias = [[NSMutableDictionary alloc] init];
    for (DiaEntrenamiento* dia in self.entrenamiento.diasDeEntrenamiento) {
        NSDateComponents* components = [self.gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dia.fecha];
        NSString* key = [NSString stringWithFormat:@"%d%d%d", (int)components.year, (int)components.month, (int)components.day];
        [auxDias setObject:dia forKey:key];
    }
    
    self.diasEntrenamiento = auxDias.copy;
    [self generaCalendario];
}

- (void) borraCalendario {
    
    [self.botonSiguiente removeFromSuperview];
    self.botonSiguiente = nil;
    [self.botonAnterior removeFromSuperview];
    self.botonAnterior = nil;
    
    [self.labelMes removeFromSuperview];
    self.labelMes = nil;
    
    
    [self.leyenda removeFromSuperview];
    self.leyenda = nil;
    
    [self.calendario removeFromSuperview];
    self.calendario = nil;
    
    [self.diaCalendario removeFromSuperview];
    self.diaCalendario = nil;
}

- (void) generaCalendario {
    if(self.calendario) {
        [self borraCalendario];
    }
    
    
    NSDate* fechaInicio = self.entrenamiento.inicio;
    
    
    _calendario = [[Calendario alloc] initWithInicio:fechaInicio andFinal:[NSDate date]];
    _calendario.delegate = self;
    [self addSubview:_calendario];
    
    
    UIImage* imgSiguiente = [UIImage imageNamed:@"flechadercalepaciente.png"];
    self.botonSiguiente = [[UIButton alloc] initWithFrame:CGRectMake(492-10-imgSiguiente.size.width, 0, imgSiguiente.size.width, imgSiguiente.size.height)];
    [self.botonSiguiente setImage:imgSiguiente forState:UIControlStateNormal];
    [self.botonSiguiente addTarget:self action:@selector(botonSiguienteDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonSiguiente];
    
    UIImage* imgAnterior = [UIImage imageNamed:@"flechaizqcalepaciente.png"];
    self.botonAnterior = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, imgAnterior.size.width, imgAnterior.size.height)];
    [self.botonAnterior setImage:imgAnterior forState:UIControlStateNormal];
    [self.botonAnterior addTarget:self action:@selector(botonAnteriorDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonAnterior];
    
    self.labelMes = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 430, 25)];
    self.labelMes.font = [UIFont fontWithName:@"Lato-Bold" size:18];
    self.labelMes.textColor = [UIColor azulOscuro];
    self.labelMes.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.labelMes];
    
    
    self.leyenda = [[UIView alloc] init];
    NSArray* labels = @[@"Entrenado", @"Inactividad", @"Pendiente de entrenamiento", @"Hoy", @"Dia libre"];
    NSArray* colores = @[[UIColor colorWithRed:171/255.0 green:233/255.0 blue:212/255.0 alpha:1.0],
                         [UIColor colorWithRed:231/255.0 green:82/255.0 blue:82/255.0 alpha:1.0],
                         [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0],
                         [UIColor colorWithRed:88/255.0 green:200/255.0 blue:247/255.0 alpha:1.0],
                         [UIColor whiteColor]];
    [self addSubview:self.leyenda];
    
    CGRect frameAnterior = CGRectZero;
    for (int l = 0; l<labels.count; l++) {
        UILabel* label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:@"Lato-Bold" size:14.0];
        label.textColor = [UIColor darkGrayColor];
        label.text = [labels objectAtIndex:l];
        CGSize tam = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}];
        
        UIView* cuadrado = [[UIView alloc] init];
        if(l==labels.count-1) {
            cuadrado.layer.borderColor = [UIColor lightGrayColor].CGColor;
            cuadrado.layer.borderWidth = 0.5;
        }
        cuadrado.backgroundColor = [colores objectAtIndex:l];
        cuadrado.frame = CGRectMake(frameAnterior.origin.x + frameAnterior.size.width + 6, 5, 10, 10);
        
        label.frame = CGRectMake(cuadrado.frame.origin.x + cuadrado.frame.size.width + 2, 0, tam.width, tam.height);
        frameAnterior = label.frame;
        
        [self.leyenda addSubview:cuadrado];
        [self.leyenda addSubview:label];
    }
    
    self.diaCalendario = [[DiaCalendarioPaciente alloc] initWithFrame:CGRectMake(542, 57, 307, 342)];
    self.diaCalendario.hidden = YES;
    [self addSubview:self.diaCalendario];
    
    [self.calendario ultimoMes];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if(self.calendario) {
        /*
        CGRect calendarFrame = self.calendario.frame;
        calendarFrame.origin.x = 0;
        calendarFrame.origin.y = 57;
        self.calendario.frame = calendarFrame;
         */
        self.calendario.frame = CGRectMake(0, 57, 492, 402);
        
       // self.botonAnterior.center = CGPointMake(self.calendario.frame.origin.x - 40, self.calendario.center.y);
       // self.botonSiguiente.center = CGPointMake(self.calendario.frame.origin.x + self.calendario.frame.size.width + 40, self.calendario.center.y);
        
        self.labelMes.frame = CGRectMake(self.calendario.frame.origin.x, 0, self.calendario.frame.size.width, 30);
        
        self.leyenda.frame = CGRectMake(self.calendario.frame.origin.x, self.calendario.frame.origin.y+self.calendario.frame.size.height+10, self.calendario.frame.size.width, 20);
    }
}

- (void) botonSiguienteDado:(UIButton *) boton {
    [self.calendario siguienteMes];
}

- (void) botonAnteriorDado:(UIButton *) boton {
    [self.calendario anteriorMes];
}

#pragma mark CalendarioDelegate
- (void)calendar:(Calendario *)calendario haCambiadoAMes:(NSDate *)diaUno {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"MMMM yyyy"];
    
    self.labelMes.text = [formatter stringFromDate:diaUno].uppercaseString;
}

- (TipoCelda) calendar:(Calendario *)calendario tipoDeCeldaParaDeFecha:(NSDate *)fecha {
    TipoCelda tipo = TipoCeldaLibre;
    
    NSDateComponents* components = [self.gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:fecha];
    NSInteger weekday = components.weekday;
    weekday-=2;
    if(weekday<0)
        weekday = 6;
    
    int diasSemana = self.entrenamiento.estadoEntrenamiento.diasSemana.intValue;
    
    BOOL seJuega = (diasSemana&(int)pow(2, weekday))!=0;
    
    if(seJuega) {
        if([DateUtils comparaSinHoraFecha:fecha conFecha:[NSDate date]]==NSOrderedAscending)
            tipo = TipoCeldaFallado;
        else
            tipo = TipoCeldaFuturo;
    }
    NSString* key = [NSString stringWithFormat:@"%d%d%d", (int)components.year, (int)components.month, (int)components.day];
    DiaEntrenamiento* diaEntrenamiento = [self.diasEntrenamiento objectForKey:key];
    if(diaEntrenamiento)
        tipo = TipoCeldaRealizado;
    
    return tipo;
}

- (void)calendar:(Calendario *)calendario diaSeleccionado:(NSDate *)fecha deTipo:(TipoCelda) tipo conFrame:(CGRect)frame {
    if(tipo==TipoCeldaFueraDeRango)
        return;
    
    NSDateComponents* components = [self.gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:fecha];
    NSString* key = [NSString stringWithFormat:@"%d%d%d", (int)components.year, (int)components.month, (int)components.day];
    
    self.diaCalendario.hidden = NO;
    self.diaCalendario.tipoCelda = tipo;
    self.diaCalendario.fecha = fecha;
    self.diaCalendario.dia = [self.diasEntrenamiento objectForKey:key];
    [self.diaCalendario refresca];
}

- (CGSize)sizeDeCeldaDeCalendar:(Calendario *)calendario {
    return CGSizeMake(65, 59);
}

- (float)espacioEntreCeldasDeCalendar:(Calendario *)calendario {
    return 6.0;
}


@end
