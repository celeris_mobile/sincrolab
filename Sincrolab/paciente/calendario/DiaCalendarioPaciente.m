//
//  DiaCalendarioPaciente.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DiaCalendarioPaciente.h"
#import "JuegoDiaPacienteView.h"

@interface DiaCalendarioPaciente()
@property (nonatomic, strong) UILabel* tituloLabel;
@property (nonatomic, strong) UILabel* labelFecha;

@property (nonatomic, strong) UILabel* tituloNivel;
@property (nonatomic, strong) UILabel* labelNivel;

@property (nonatomic, strong) JuegoDiaPacienteView* juego1;
@property (nonatomic, strong) JuegoDiaPacienteView* juego2;
@property (nonatomic, strong) JuegoDiaPacienteView* juego3;
@end

@implementation DiaCalendarioPaciente

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    
    UILabel* tituloSec = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    tituloSec.font = [UIFont fontWithName:@"Lato-Black" size:18.0];
    tituloSec.textColor = [UIColor whiteColor];
    tituloSec.text = @"ENTRENAMIENTO";
    [self addSubview:tituloSec];
    
    
    self.tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 300, 20)];
    self.tituloLabel.font = [UIFont fontWithName:@"Lato-Black" size:15.0];
    self.tituloLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.tituloLabel];
    
    self.labelFecha = [[UILabel alloc] initWithFrame:CGRectMake(0, 48, 300, 20)];
    self.labelFecha.font = [UIFont fontWithName:@"Lato-Black" size:18.0];
    self.labelFecha.textColor = [UIColor colorWithRed:171/255.0 green:233/255.0 blue:212/255.0 alpha:1.0];
    [self addSubview:self.labelFecha];
    
    self.tituloNivel = [[UILabel alloc] initWithFrame:CGRectMake(0, 85, 200, 20)];
    self.tituloNivel.font = [UIFont fontWithName:@"Lato-Black" size:22];
    self.tituloNivel.textColor = [UIColor orangeColor];
    self.tituloNivel.text = @"NIVEL";
    [self addSubview:self.tituloNivel];
    
    self.labelNivel = [[UILabel alloc] initWithFrame:CGRectMake(80, 85, 200, 20)];
    self.labelNivel.font = [UIFont fontWithName:@"Lato-Black" size:22];
    self.labelNivel.textColor = [UIColor whiteColor];
    [self addSubview:self.labelNivel];
    
    self.juego1 = [[JuegoDiaPacienteView alloc] initWithJuego:TipoJuegoGongs];
    self.juego1.frame = CGRectMake(0, 132, 50, 300);
    [self addSubview:self.juego1];
    
    self.juego2 = [[JuegoDiaPacienteView alloc] initWithJuego:TipoJuegoInvasion];
    self.juego2.frame = CGRectMake(0, 195, 50, 300);
    [self addSubview:self.juego2];
    
    self.juego3 = [[JuegoDiaPacienteView alloc] initWithJuego:TipoJuegoPoker];
    self.juego3.frame = CGRectMake(0, 259, 50, 300);
    [self addSubview:self.juego3];
}

- (void) refresca {
    if(self.tipoCelda==TipoCeldaFueraDeRango) {
        self.juego1.alpha = 0.0;
        self.juego2.alpha = 0.0;
        self.juego3.alpha = 0.0;
        self.tituloLabel.text = @"No hay entrenamiento";
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE dd' de 'MMMM"];
        self.labelFecha.textColor = [UIColor lightGrayColor];
        self.labelFecha.text = [formatter stringFromDate:self.fecha];
        self.tituloNivel.alpha = 0.0;
        self.labelNivel.alpha = 0.0;
    }
        
    if(self.tipoCelda==TipoCeldaRealizado && self.dia) {
        self.tituloLabel.text = @"Entrenamiento realizado";
        self.juego1.alpha = 1.0;
        self.juego2.alpha = 1.0;
        self.juego3.alpha = 1.0;
        self.tituloNivel.alpha = 1.0;
        self.labelNivel.alpha = 1.0;

        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE dd' de 'MMMM"];
        self.labelFecha.textColor = [UIColor colorWithRed:171/255.0 green:233/255.0 blue:212/255.0 alpha:1.0];
        self.labelFecha.text = [formatter stringFromDate:self.fecha];
        
        if(self.dia.partidaGongs) {
            self.juego1.puntos = [self.dia.partidaGongs.sesiones valueForKeyPath:@"@sum.puntuacion"];
            self.juego1.aciertos = [self.dia.partidaGongs.sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
            self.juego1.tiempoRespuesta = [self.dia.partidaGongs.sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
        } else {
            self.juego1.alpha = 0.5;
        }
        
        if(self.dia.partidaInvasion) {
            self.juego2.puntos = [self.dia.partidaInvasion.sesiones valueForKeyPath:@"@sum.puntuacion"];
            self.juego2.aciertos = [self.dia.partidaInvasion.sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
            self.juego2.tiempoRespuesta = [self.dia.partidaInvasion.sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
        } else {
            self.juego2.alpha = 0.5;
        }
        
        if(self.dia.partidaPoker) {
            self.juego3.puntos = [self.dia.partidaPoker.sesiones valueForKeyPath:@"@sum.puntuacion"];
            self.juego3.aciertos = [self.dia.partidaPoker.sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
            self.juego3.tiempoRespuesta = [self.dia.partidaPoker.sesiones valueForKeyPath:@"@avg.tiempoMedioDeRespuestaAciertos"];
        } else {
            self.juego3.alpha = 0.5;
        }
        
        NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setPositivePrefix:@"+"];
        self.labelNivel.text = [numberFormatter stringFromNumber:self.dia.cambioNivel];
    } else {
        self.juego1.alpha = 0.0;
        self.juego2.alpha = 0.0;
        self.juego3.alpha = 0.0;
        self.tituloNivel.alpha = 0.0;
        self.labelNivel.alpha = 0.0;
        
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE dd' de 'MMMM"];
        
        switch (self.tipoCelda) {
            case TipoCeldaFallado:
                self.tituloLabel.text = @"Entrenamiento no realizado";
                self.labelFecha.textColor = [UIColor colorWithRed:231/255.0 green:82/255.0 blue:82/255.0 alpha:1.0];
                self.labelFecha.text = [formatter stringFromDate:self.fecha];
                break;
            case TipoCeldaFuturo:
                self.tituloLabel.text = @"Día de entrenamiento";
                self.labelFecha.textColor = [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0];
                self.labelFecha.text = [formatter stringFromDate:self.fecha];
                break;
            case TipoCeldaLibre:
                self.tituloLabel.text = @"Día de libre";
                self.labelFecha.textColor = [UIColor lightGrayColor];
                self.labelFecha.text = [formatter stringFromDate:self.fecha];
                break;
            default:
                break;
        }
    }
}


@end
