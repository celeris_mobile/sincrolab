//
//  DiaCalendarioPaciente.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaEntrenamiento.h"
#import "DiaCell.h"

@interface DiaCalendarioPaciente : UIView
@property (nonatomic, strong) DiaEntrenamiento* dia;
@property (nonatomic, assign) TipoCelda tipoCelda;
@property (nonatomic, assign) NSDate* fecha;
- (void) refresca;
@end
