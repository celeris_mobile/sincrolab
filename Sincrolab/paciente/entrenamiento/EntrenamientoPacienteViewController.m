//
//  EntrenamientoPacienteViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoPacienteViewController.h"
#import "CoverFlowLayout.h"
#import "EntrenamientoHoyCell.h"
#import "EntrenamientoFuturoCell.h"
#import "EntrenamientoPasadoCell.h"
#import "DateUtils.h"
#import "DiaEntrenamiento.h"
#import "JuegosHubViewController.h"
#import "ProgressHUD.h"

static NSString * const cellEntrenamientoHoy = @"cellEntrenamientoHoy";
static NSString * const cellEntrenamientoFuturo = @"cellEntrenamientoFuturo";
static NSString * const cellEntrenamientoPasado = @"cellEntrenamientoPasado";

@interface EntrenamientoPacienteViewController ()
@property (nonatomic, strong) UICollectionView* dias;
@end

@implementation EntrenamientoPacienteViewController
@synthesize modelo = _modelo;

- (void)viewDidLoad {
    [super viewDidLoad];

    CoverFlowLayout* layout = [[CoverFlowLayout alloc] init];
    
    self.dias = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.dias.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.dias.backgroundColor = [UIColor clearColor];
    self.dias.dataSource = self;
    [self.dias registerClass:[EntrenamientoHoyCell class] forCellWithReuseIdentifier:cellEntrenamientoHoy];
    [self.dias registerClass:[EntrenamientoPasadoCell class] forCellWithReuseIdentifier:cellEntrenamientoPasado];
    [self.dias registerClass:[EntrenamientoFuturoCell class] forCellWithReuseIdentifier:cellEntrenamientoFuturo];
    [self.view addSubview:self.dias];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(!self.modelo) {
        [self recalculaCoverFLow];
        return;
    }
    
    if(self.modelo.cargado)
        [self recalculaCoverFLow];
    else {
        [self.modelo carga:^(NSError * error) {
            if(!error)
                [self recalculaCoverFLow];
        } conProgreso:YES];
    }
}

- (void) entrenamientoCargado {
    [self recalculaCoverFLow];
}

- (void) recalculaCoverFLow {
    if(self.modelo && !self.modelo.entrenamiento) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
           [ProgressHUD showError:@"No hay entrenamiento"];
        });
        
        return;
    }
        
    [self.dias reloadData];
    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:self.modelo.paciente.entrenamiento.diasDeEntrenamiento.count inSection:0];
    [self.dias scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
}

#pragma mark UICollectionViewDatasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.modelo && !self.modelo.paciente.entrenamiento)
        return 0;
    
    if(!self.modelo)
        return 1;
    
    return self.modelo.paciente.entrenamiento.diasDeEntrenamiento.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(!self.modelo) {
        EntrenamientoHoyCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellEntrenamientoHoy forIndexPath:indexPath];
        cell.numCelda = indexPath.row+1;
        cell.estado = nil;
        [cell.boton addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    //Es el último entrenamiento
    if(indexPath.row==self.modelo.paciente.entrenamiento.diasDeEntrenamiento.count) {
        NSDate* hoy = [NSDate date];
        
        //Día de la semana de hoy
        NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents* components = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:hoy];
        NSInteger weekday = components.weekday;
        weekday-=2;
        if(weekday<0)
            weekday = 6;
        
        //Días en los que hay juegos
        int diasSemana = self.modelo.paciente.entrenamiento.estadoEntrenamiento.diasSemana.intValue;
        
        
        //Se tiene que jugar hoy o se tenía que haber jugado y hoy no se ha jugado ya.)
        NSComparisonResult comparisonResult = [DateUtils comparaSinHoraFecha:hoy conFecha:self.modelo.paciente.siguienteEntrenamiento];
        if(comparisonResult!=NSOrderedAscending) {
            
            //Hoy se juega
            EntrenamientoHoyCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellEntrenamientoHoy forIndexPath:indexPath];
            cell.numCelda = indexPath.row+1;
            cell.estado = self.modelo.paciente.entrenamiento.estadoEntrenamiento;
            [cell.boton addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        } else {
            
            //No es hoy, ¿Cuántos días quedan?
            int diasASumar = 0;
            BOOL encontrado = NO;
            while (!encontrado && diasASumar<7) {
                weekday+=1;
                if(weekday>6)
                    weekday = 0;
                
                diasASumar+=1;
                
                if((diasSemana&(int)pow(2, weekday))!=0)
                    encontrado = YES;
            }
            
            //Es un día entre hoy y dentro de 7 días.
            if(encontrado) {
                NSDateComponents* comp = [[NSDateComponents alloc] init];
                comp.day = diasASumar;
                NSDate* fechaFutura = [calendar dateByAddingComponents:comp toDate:[NSDate date] options:0];
                
                EntrenamientoFuturoCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellEntrenamientoFuturo forIndexPath:indexPath];
                cell.numCelda = indexPath.row+1;
                cell.fecha = fechaFutura;
                cell.estado = self.modelo.paciente.entrenamiento.estadoEntrenamiento;
                return cell;
            } else {
                //No se juega, debe ser que no quedan días de entrenamiento. ¿Qué hacemos?¿Cómo se detecta esto?
                //TODO En realidad creo que aquí no llegaría nunca si el entrenamiento tiene días de la semana asignados
            }
        }
    } else {
        //Es un día pasado.
        EntrenamientoPasadoCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellEntrenamientoPasado forIndexPath:indexPath];
        cell.numCelda = indexPath.row+1;
        cell.diaEntrenamiento = [self.modelo.paciente.entrenamiento.diasDeEntrenamiento objectAtIndex:indexPath.row];
        return cell;
    }
    
    return nil;
}

- (void) botonDado:(UIButton *) boton {
    JuegosHubViewController* hub = [[JuegosHubViewController alloc] init];
    hub.modelo = self.modelo;
    
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:hub];
    navController.navigationBarHidden = YES;
    [self presentViewController:navController animated:YES completion:nil];

}


@end
