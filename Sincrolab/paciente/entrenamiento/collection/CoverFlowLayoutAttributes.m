//
//  CoverFlowLayoutAttributes.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CoverFlowLayoutAttributes.h"

@implementation CoverFlowLayoutAttributes

- (BOOL)isEqual:(id)object {
    BOOL equal = [super isEqual:object];
    
    if(self.oscuridad != ((CoverFlowLayoutAttributes *)object).oscuridad) {
        equal = NO;
    }
    
    return equal;
}

- (id)copyWithZone:(NSZone *)zone
{
    CoverFlowLayoutAttributes *newAttributes = [super copyWithZone:zone];
    newAttributes->_oscuridad = _oscuridad;
    
    return newAttributes;
}

@end
