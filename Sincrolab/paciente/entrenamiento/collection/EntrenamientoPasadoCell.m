//
//  EntrenamientoPasadoCell.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoPasadoCell.h"
#import "UIColor+Extensions.h"
#import "MiniaturaJuego.h"
#import "CoverFlowLayoutAttributes.h"

@interface EntrenamientoPasadoCell()
@property (nonatomic, strong) UILabel* diaLabel;
@property (nonatomic, strong) UILabel* fechaLabel;
@property (nonatomic, strong) UIView* capaOscura;
@property (nonatomic, strong) NSArray* juegos;
@property (nonatomic, strong) UILabel* labelNivel;
@end

@implementation EntrenamientoPasadoCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        self.contentView.backgroundColor = [UIColor clearColor];
        
        CALayer* fondo = [CALayer layer];
        fondo.frame = CGRectMake(0, 0, 481, 436);
        fondo.backgroundColor = [UIColor whiteColor].CGColor;
        fondo.cornerRadius = 10.0;
        
        CALayer* sombra = [CALayer layer];
        sombra.frame = CGRectMake(9, 9, 481, 436);
        sombra.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4].CGColor;
        sombra.cornerRadius = 10.0;
        
        CALayer* fondoNumero = [CALayer layer];
        fondoNumero.frame = CGRectMake(48, 50, 63, 63);
        fondoNumero.backgroundColor = [UIColor juegoNaranja].CGColor;
        fondoNumero.cornerRadius = 5.0;
        
        CALayer* separador = [CALayer layer];
        separador.frame = CGRectMake(47, 126, 391, 6);
        separador.backgroundColor = [UIColor juegoMarronOscuro].CGColor;

        CALayer* separador2 = [CALayer layer];
        separador2.frame = CGRectMake(47, 326, 391, 6);
        separador2.backgroundColor = [UIColor juegoMarronOscuro].CGColor;
        
        [self.contentView.layer addSublayer:sombra];
        [self.contentView.layer addSublayer:fondo];
        [self.contentView.layer addSublayer:fondoNumero];
        [self.contentView.layer addSublayer:separador];
        [self.contentView.layer addSublayer:separador2];
        
        self.diaLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 50, 63, 63)];
        self.diaLabel.textColor = [UIColor whiteColor];
        self.diaLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:45.0];
        self.diaLabel.textAlignment = NSTextAlignmentCenter;
        self.diaLabel.text = @"09";
        [self.contentView addSubview:self.diaLabel];
        
        self.fechaLabel = [[UILabel alloc] initWithFrame:CGRectMake(92, 52, 309, 63)];
        self.fechaLabel.textColor = [UIColor juegoMarronOscuro];
        self.fechaLabel.textAlignment = NSTextAlignmentRight;
        self.fechaLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:43.0];
        [self.contentView addSubview:self.fechaLabel];
                
        NSMutableArray* auxJuegos = [[NSMutableArray alloc] init];
        for (int j = 0; j<3; j++) {
            MiniaturaJuego* carta = [[MiniaturaJuego alloc] init];
            carta.numJuego = j;
            CGRect cartaFrame = carta.frame;
            cartaFrame.origin = CGPointMake(49+133*j, 146);
            carta.frame = cartaFrame;
            [self.contentView addSubview:carta];
            [auxJuegos addObject:carta];
        }
        
        self.juegos = auxJuegos.copy;
        
        self.labelNivel = [[UILabel alloc] initWithFrame:CGRectMake(17, 357, 446, 40)];
        self.labelNivel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:43.0];
        self.labelNivel.textColor = [UIColor juegoNaranja];
        self.labelNivel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.labelNivel];
        
        self.capaOscura = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 481, 436)];
        self.capaOscura.backgroundColor = [UIColor blackColor];
        self.capaOscura.layer.cornerRadius = 10.0;
        self.capaOscura.alpha = 0.0;
        [self addSubview:self.capaOscura];
    }
    return self;
}

- (void)setNumCelda:(int)numCelda {
    _numCelda = numCelda;
    self.diaLabel.text = [NSString stringWithFormat:@"%02d", numCelda];
}

- (void)setDiaEntrenamiento:(DiaEntrenamiento *)diaEntrenamiento {
    _diaEntrenamiento = diaEntrenamiento;
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    formatter.dateFormat = @"dd / MM / yyyy";
    self.fechaLabel.text = [formatter stringFromDate:self.diaEntrenamiento.fecha];
        
    int nivelTotal = 0;
    MiniaturaJuego* cartaGongs = [self.juegos objectAtIndex:0];
    if(!diaEntrenamiento.partidaGongs)
        cartaGongs.alpha = 0.2;
    else {
        cartaGongs.alpha = 1.0;
        cartaGongs.deltaNivel = diaEntrenamiento.partidaGongs.cambioNivel.intValue;
        nivelTotal += cartaGongs.deltaNivel;
    }
    MiniaturaJuego* cartaInvasion = [self.juegos objectAtIndex:1];
    if(!diaEntrenamiento.partidaInvasion)
        cartaInvasion.alpha = 0.2;
    else {
        cartaInvasion.alpha = 1.0;
        cartaInvasion.deltaNivel = diaEntrenamiento.partidaInvasion.cambioNivel.intValue;
        nivelTotal += cartaInvasion.deltaNivel;
    }
    MiniaturaJuego* cartaPoker = [self.juegos objectAtIndex:2];
    if(!diaEntrenamiento.partidaPoker)
        cartaPoker.alpha = 0.2;
    else {
        cartaPoker.alpha = 1.0;
        cartaPoker.deltaNivel = diaEntrenamiento.partidaPoker.cambioNivel.intValue;
        nivelTotal += cartaPoker.deltaNivel;
    }

    //TODO debería ser el nivel real del usuario, no el incremento. Creo yo.
    if(nivelTotal>0)
        self.labelNivel.text = [NSString stringWithFormat:@"Nivel +%d", nivelTotal];
    else
        self.labelNivel.text = [NSString stringWithFormat:@"Nivel %d", nivelTotal];

}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    CoverFlowLayoutAttributes* attributes = (CoverFlowLayoutAttributes *)layoutAttributes;
    self.capaOscura.alpha = 1.0-attributes.oscuridad;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
