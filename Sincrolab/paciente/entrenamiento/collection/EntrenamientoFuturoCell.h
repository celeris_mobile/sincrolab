//
//  EntrenamientoFuturoCell.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EstadoEntrenamiento.h"

@interface EntrenamientoFuturoCell : UICollectionViewCell
@property (nonatomic, assign) int numCelda;
@property (nonatomic, assign) NSDate* fecha;
@property (nonatomic, assign) EstadoEntrenamiento* estado;
@end