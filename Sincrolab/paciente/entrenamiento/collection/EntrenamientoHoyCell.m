//
//  DiaEntrenamientoView.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EntrenamientoHoyCell.h"
#import "UIColor+Extensions.h"
#import "MiniaturaJuego.h"
#import "CoverFlowLayoutAttributes.h"
#import "Constantes.h"

@interface EntrenamientoHoyCell()
@property (nonatomic, strong) UILabel* diaLabel;
@property (nonatomic, strong) UIView* capaOscura;
@property (nonatomic, strong) NSArray* juegos;
@end

@implementation EntrenamientoHoyCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {

        self.contentView.backgroundColor = [UIColor clearColor];

        CALayer* fondo = [CALayer layer];
        fondo.frame = CGRectMake(0, 0, 481, 436);
        fondo.backgroundColor = [UIColor whiteColor].CGColor;
        fondo.cornerRadius = 10.0;
        
        CALayer* sombra = [CALayer layer];
        sombra.frame = CGRectMake(9, 9, 481, 436);
        sombra.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4].CGColor;
        sombra.cornerRadius = 10.0;
        
        CALayer* fondoNumero = [CALayer layer];
        fondoNumero.frame = CGRectMake(48, 50, 63, 63);
        fondoNumero.backgroundColor = [UIColor juegoNaranja].CGColor;
        fondoNumero.cornerRadius = 5.0;
        
        CALayer* separador = [CALayer layer];
        separador.frame = CGRectMake(47, 126, 391, 6);
        separador.backgroundColor = [UIColor juegoMarronOscuro].CGColor;
        
        [self.contentView.layer addSublayer:sombra];
        [self.contentView.layer addSublayer:fondo];
        [self.contentView.layer addSublayer:fondoNumero];
        [self.contentView.layer addSublayer:separador];
        
        self.diaLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 50, 63, 63)];
        self.diaLabel.textColor = [UIColor whiteColor];
        self.diaLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:45.0];
        self.diaLabel.textAlignment = NSTextAlignmentCenter;
        self.diaLabel.text = @"09";
        [self.contentView addSubview:self.diaLabel];
        
        UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(131, 51, 309, 63)];
        titulo.textColor = [UIColor juegoMarronOscuro];
        titulo.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:43.0];
        titulo.text = @"Tu entrenamiento";
        [self.contentView addSubview:titulo];
        
        UIImage* imgBoton = [UIImage imageNamed:@"btn_comenzar.png"];
        UIImage* imgBotonP = [UIImage imageNamed:@"btn_comenzar_p.png"];
        _boton = [[UIButton alloc] initWithFrame:CGRectMake(47, 312, imgBoton.size.width, imgBoton.size.height)];
        [_boton setImage:imgBoton forState:UIControlStateNormal];
        [_boton setImage:imgBotonP forState:UIControlStateHighlighted];
        [self.contentView addSubview:_boton];
        

        
        NSMutableArray* auxJuegos = [[NSMutableArray alloc] init];
        for (int j = 0; j<3; j++) {
            UIImage* numImg = [UIImage imageNamed:[NSString stringWithFormat:@"num%d.png", j+1]];
            UIImageView* numImgView = [[UIImageView alloc] initWithImage:numImg];
            numImgView.center = CGPointMake(108+133*j, 154);
            [self addSubview:numImgView];
            
            MiniaturaJuego* carta = [[MiniaturaJuego alloc] init];
            carta.numJuego = j;
            CGRect cartaFrame = carta.frame;
            cartaFrame.origin = CGPointMake(49+133*j, 179);
            carta.frame = cartaFrame;
            [self.contentView addSubview:carta];
            [auxJuegos addObject:carta];
        }
        
        self.juegos = auxJuegos.copy;
        
        self.capaOscura = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 481, 436)];
        self.capaOscura.backgroundColor = [UIColor blackColor];
        self.capaOscura.layer.cornerRadius = 10.0;
        self.capaOscura.alpha = 0.0;
        [self addSubview:self.capaOscura];
    }
    return self;
}


- (void)setNumCelda:(int)numCelda {
    _numCelda = numCelda;
    self.diaLabel.text = [NSString stringWithFormat:@"%02d", numCelda];
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {

    [super applyLayoutAttributes:layoutAttributes];
    CoverFlowLayoutAttributes* attributes = (CoverFlowLayoutAttributes *)layoutAttributes;
    self.capaOscura.alpha = 1.0-attributes.oscuridad;
}

- (void)setEstado:(EstadoEntrenamiento *)estado {
    _estado = estado;
    
    if(!estado) {
        
        for (int j = 0; j<[Constantes todosLosJuegos].count; j++) {
            TipoJuego tipo = ((NSNumber *)[[Constantes todosLosJuegos] objectAtIndex:j]).intValue;
            int diasJuego = 0;
            switch (tipo) {
                case TipoJuegoGongs:
                    diasJuego = self.estado.estadoGongs.diasSemana.intValue;
                    break;
                case TipoJuegoInvasion:
                    diasJuego = self.estado.estadoInvasion.diasSemana.intValue;
                    break;
                case TipoJuegoPoker:
                    diasJuego = self.estado.estadoPoker.diasSemana.intValue;
                    break;
            }
            
            MiniaturaJuego* carta = [self.juegos objectAtIndex:j];
            carta.alpha = 1.0;
        }
        
        return;
    }
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSInteger weekday = components.weekday;
    weekday-=2;
    if(weekday<0)
        weekday = 6;
    
    for (int j = 0; j<[Constantes todosLosJuegos].count; j++) {
        TipoJuego tipo = ((NSNumber *)[[Constantes todosLosJuegos] objectAtIndex:j]).intValue;
        int diasJuego = 0;
        switch (tipo) {
            case TipoJuegoGongs:
                diasJuego = self.estado.estadoGongs.diasSemana.intValue;
                break;
            case TipoJuegoInvasion:
                diasJuego = self.estado.estadoInvasion.diasSemana.intValue;
                break;
            case TipoJuegoPoker:
                diasJuego = self.estado.estadoPoker.diasSemana.intValue;
                break;
        }
        
        MiniaturaJuego* carta = [self.juegos objectAtIndex:j];
        if((diasJuego&(int)pow(2, weekday))!=0) {
            carta.alpha = 1.0;
        } else {
            carta.alpha = 0.2;
        }
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
