//
//  CartaEntrenamiento.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MiniaturaJuego : UIView
@property (nonatomic, assign) NSInteger numJuego;
@property (nonatomic, assign) NSInteger deltaNivel;
@end
