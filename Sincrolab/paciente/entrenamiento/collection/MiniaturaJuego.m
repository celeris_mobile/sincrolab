//
//  CartaEntrenamiento.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "MiniaturaJuego.h"
#import "Constantes.h"
#import "UIColor+Extensions.h"

@interface MiniaturaJuego()
@property (nonatomic, strong) UIImageView* thumb;
@property (nonatomic, strong) UIImageView* numImgView;
@property (nonatomic, strong) UIImageView* imgFlecha;
@property (nonatomic, strong) UILabel* labelDeltaNivel;
@end

@implementation MiniaturaJuego

- (id)init {
    self = [super init];
    if (self) {
        UIImage* img = [UIImage imageNamed:@"thumbjuego1.jpg"];
        self.thumb = [[UIImageView alloc] initWithImage:img];
        self.thumb.contentMode = UIViewContentModeScaleAspectFill;
        self.thumb.clipsToBounds = YES;
        self.thumb.layer.cornerRadius = 4.0;

        
        CALayer* sombra = [CALayer layer];
        sombra.frame = CGRectMake(4, 4, img.size.width, img.size.height);
        sombra.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4].CGColor;
        sombra.cornerRadius = 4.0;
        
        [self.layer addSublayer:sombra];
        [self addSubview:self.thumb];
        
        self.imgFlecha = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"subidanivel.png"]];
        self.imgFlecha.frame = CGRectMake(24, 132, 38, 32);
        self.imgFlecha.hidden = YES;
        [self addSubview:self.imgFlecha];
        
        self.labelDeltaNivel = [[UILabel alloc] initWithFrame:CGRectMake(64, 128, 46, 40)];
        self.labelDeltaNivel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:40];
        self.labelDeltaNivel.minimumScaleFactor = 0.5;
        self.labelDeltaNivel.adjustsFontSizeToFitWidth = YES;
        self.labelDeltaNivel.textColor = [UIColor juegoVerdeOscuro];
        self.labelDeltaNivel.hidden = YES;
        [self addSubview:self.labelDeltaNivel];
    }
    return self;
}

- (void)setNumJuego:(NSInteger)numJuego {
    _numJuego = numJuego;
    
    self.thumb.image = [Constantes miniaturaJuego:numJuego];
}

- (void)setDeltaNivel:(NSInteger)deltaNivel {
    if(deltaNivel>0) {
        self.imgFlecha.image = [UIImage imageNamed:@"subidanivel.png"];
        self.labelDeltaNivel.text = [NSString stringWithFormat:@"+%d", deltaNivel];
    }
    if(deltaNivel<0) {
        self.imgFlecha.image = [UIImage imageNamed:@"bajadanivel.png"];
        self.labelDeltaNivel.text = [NSString stringWithFormat:@"%d", deltaNivel];
    }
    if(deltaNivel!=0) {
        self.imgFlecha.hidden = NO;
        self.labelDeltaNivel.hidden = NO;
    } else {
        self.labelDeltaNivel.text = @"-";
        self.labelDeltaNivel.hidden = NO;        
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
