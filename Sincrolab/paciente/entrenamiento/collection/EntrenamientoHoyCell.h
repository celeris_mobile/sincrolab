//
//  DiaEntrenamientoView.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EstadoEntrenamiento.h"

@interface EntrenamientoHoyCell : UICollectionViewCell
@property (nonatomic, readonly) UIButton* boton;
@property (nonatomic, assign) int numCelda;
@property (nonatomic, assign) EstadoEntrenamiento* estado;
@end