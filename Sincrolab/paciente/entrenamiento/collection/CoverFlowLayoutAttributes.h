//
//  CoverFlowLayoutAttributes.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoverFlowLayoutAttributes : UICollectionViewLayoutAttributes
@property (nonatomic, assign) CGFloat oscuridad;
@end
