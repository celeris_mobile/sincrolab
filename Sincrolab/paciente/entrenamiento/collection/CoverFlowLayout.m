//
//  CoverFlowLayout.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CoverFlowLayout.h"
#import "CoverFlowLayoutAttributes.h"

#define ACTIVE_DISTANCE 200
#define ZOOM_FACTOR 0.3

@implementation CoverFlowLayout

+ (Class)layoutAttributesClass {
    return [CoverFlowLayoutAttributes class];
}

- (id)init {
    self = [super init];
    if(self) {
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = -100;
        self.itemSize = CGSizeMake(481, 436);
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.sectionInset = UIEdgeInsetsMake(0, 1024/2-241, 0, 1024/2-241);
    }
    return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray* array = [super layoutAttributesForElementsInRect:rect];
    
    CGRect visibleRect;
    visibleRect.origin = self.collectionView.contentOffset;
    visibleRect.size = self.collectionView.bounds.size;
    
    for (CoverFlowLayoutAttributes* attributes in array) {
        if(CGRectIntersectsRect(attributes.frame, rect)) {
            CGFloat distance = CGRectGetMidX(visibleRect) - attributes.center.x;
            CGFloat normalizedDistance = distance / ACTIVE_DISTANCE;
            attributes.transform3D = CATransform3DMakeScale(0.7, 0.7, 1.0);
            attributes.oscuridad = 0.5;
            if(ABS(distance) < ACTIVE_DISTANCE) {
                CGFloat zoom = 0.7 + ZOOM_FACTOR*(1 - ABS(normalizedDistance));
                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0);
                attributes.zIndex = 1.0;
                attributes.oscuridad =  0.5 + 0.5*(1 - ABS(normalizedDistance));
            }
        }
    }
    

    return array;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    CGFloat offsetAdjustment = MAXFLOAT;
    CGFloat horizontalCenter = proposedContentOffset.x + (CGRectGetWidth(self.collectionView.bounds)/2.0);
    
    CGRect targetRect = CGRectMake(proposedContentOffset.x, 0.0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    NSArray* array = [super layoutAttributesForElementsInRect:targetRect];
    
    for(UICollectionViewLayoutAttributes * lauoutAttributes in array) {
        CGFloat itemHorizontalCenter = lauoutAttributes.center.x;
        if(ABS(itemHorizontalCenter - horizontalCenter) < ABS(offsetAdjustment)) {
            offsetAdjustment = itemHorizontalCenter - horizontalCenter;
        }
    }
    
    return CGPointMake(proposedContentOffset.x + offsetAdjustment, proposedContentOffset.y);
}

@end
