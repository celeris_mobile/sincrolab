//
//  EntrenamientoPacienteViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntrenamientoPacienteModel.h"

@interface EntrenamientoPacienteViewController : UIViewController <UICollectionViewDataSource>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
