//
//  GraficaPromedioJuegoJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"
#import "GraficaTarta.h"


@protocol GraficaPromedioJuegoJugadorDatasource <NSObject>
- (NSArray *) valoresParaJuego:(TipoJuego) tipoJuego;
@end

@interface GraficaPromedioJuegoJugador : UIView <GraficaTartaDatasource, GraficaTartaDelegate>
@property (nonatomic, weak) id<GraficaPromedioJuegoJugadorDatasource> datasource;
- (id) initWithJuego:(TipoJuego) tipo;
@end
