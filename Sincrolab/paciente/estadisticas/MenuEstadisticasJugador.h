//
//  MenuEstadisticasView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef NS_ENUM(NSInteger, TipoEstadisticasJugador) {
    TipoEstadisticasJugadorGenerales,
    TipoEstadisticasJugadorGongs,
    TipoEstadisticasJugadorInvasion,
    TipoEstadisticasJugadorPoker,
};


@protocol MenuEstadisticasJugadorDelegate <NSObject>
- (void) cargaEstadisticas:(TipoEstadisticasJugador) tipo;
@end

@interface MenuEstadisticasJugador : UIView
@property (nonatomic, weak) id<MenuEstadisticasJugadorDelegate> delegate;
@end
