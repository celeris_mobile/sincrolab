//
//  MenuEstadisticasView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "MenuEstadisticasJugador.h"
#import "UIColor+Extensions.h"
#import "ImageUtils.h"


@interface MenuEstadisticasJugador()
@property (nonatomic, strong) UIButton* botonGenerales;
@property (nonatomic, strong) UIButton* botonGongs;
@property (nonatomic, strong) UIButton* botonInvasion;
@property (nonatomic, strong) UIButton* botonPoker;

@property (nonatomic, strong) UIButton* seleccionado;
@end

@implementation MenuEstadisticasJugador

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 808, 100)];
    if (self) {
        [self configura];
    }
    return self;
}

- (void) configura {
    self.backgroundColor = [UIColor clearColor];
    UIColor* naranja = [UIColor colorWithRed:255/255.0 green:112/255.0 blue:75/255.0 alpha:1.0];
    
    self.botonGenerales = [[UIButton alloc] initWithFrame:CGRectMake(42, 50, 90, 27)];
    self.botonGenerales.titleLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:30];
    [self.botonGenerales setTitleColor:naranja forState:UIControlStateSelected];
    [self.botonGenerales setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.botonGenerales setTitle:@"General" forState:UIControlStateNormal];
    [self.botonGenerales addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonGenerales];
    
    self.botonGongs = [[UIButton alloc] initWithFrame:CGRectMake(360, 55, 90, 27)];
    self.botonGongs.titleLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:20];
    [self.botonGongs setTitleColor:naranja forState:UIControlStateSelected];
    [self.botonGongs setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.botonGongs setTitle:@"TOGONG!!" forState:UIControlStateNormal];
    [self.botonGongs addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonGongs];
    
    
    self.botonInvasion = [[UIButton alloc] initWithFrame:CGRectMake(460, 55, 160, 27)];
    self.botonInvasion.titleLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:20];
    [self.botonInvasion setTitleColor:naranja forState:UIControlStateSelected];
    [self.botonInvasion setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.botonInvasion setTitle:@"INVASIÓN SAMURAI" forState:UIControlStateNormal];
    [self.botonInvasion addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonInvasion];
    
    
    self.botonPoker = [[UIButton alloc] initWithFrame:CGRectMake(630, 55, 140, 27)];
    self.botonPoker.titleLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:20];
    [self.botonPoker setTitleColor:naranja forState:UIControlStateSelected];
    [self.botonPoker setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.botonPoker setTitle:@"POKER SAMURAI" forState:UIControlStateNormal];
    [self.botonPoker addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.botonPoker];
    
    [self botonDado:self.botonGenerales];
}

- (void) botonDado:(UIButton *) boton {
    if(boton.selected)
        return;
    
    if(self.seleccionado)
        self.seleccionado.selected = NO;
    
    self.seleccionado = boton;
    self.seleccionado.selected = YES;
    
    TipoEstadisticasJugador tipo;
    if(boton==self.botonGenerales)
        tipo = TipoEstadisticasJugadorGenerales;
    if(boton==self.botonGongs)
        tipo = TipoEstadisticasJugadorGongs;
    if(boton==self.botonInvasion)
        tipo = TipoEstadisticasJugadorInvasion;
    if(boton==self.botonPoker)
        tipo = TipoEstadisticasJugadorPoker;
    
    
    [self.delegate cargaEstadisticas:tipo];
}


@end
