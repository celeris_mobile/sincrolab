//
//  EstadisticasJugadorViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadisticasJugadorViewController.h"
#import "EstadisticasDiasJugadorViewController.h"

@interface EstadisticasJugadorViewController ()
@property (nonatomic, strong) MenuEstadisticasJugador* menu;
@property (nonatomic, strong) EstadisticasDiasJugadorViewController* statsDias;
@property (nonatomic, strong) UINavigationController* contentNavController;
@end

@implementation EstadisticasJugadorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView* sombra = [[UIView alloc] initWithFrame:CGRectMake(117+9, 96+9, 808, 549)];
    sombra.layer.cornerRadius = 20.0;
    sombra.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [self.view addSubview:sombra];
    
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(117, 96, 808, 549)];
    fondo.backgroundColor = [UIColor whiteColor];
    fondo.layer.cornerRadius = 20.0;
    fondo.clipsToBounds = YES;
    
    UIView* zonaGraficas = [[UIView alloc] initWithFrame:CGRectMake(0, 144, 808, 405)];
    zonaGraficas.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [fondo addSubview:zonaGraficas];

    [self.view addSubview:fondo];
    
    self.menu = [[MenuEstadisticasJugador alloc] init];
    self.menu.delegate = self;
    [fondo addSubview:self.menu];
    

    
    self.statsDias = [[EstadisticasDiasJugadorViewController alloc] init];
    self.contentNavController = [[UINavigationController alloc] initWithRootViewController:self.statsDias];
    self.contentNavController.view.frame = CGRectMake(0, 47, self.view.frame.size.width, self.view.frame.size.height-47);
    UINavigationBar* bar = self.contentNavController.navigationBar;
    bar.backgroundColor = [UIColor whiteColor];
    bar.clipsToBounds = YES;
    bar.translucent = NO;
    //TODO podemos moverlo un poco a la derecha? Quizás haya que cambiarlo por uno custom
    
    [bar setTintColor:[UIColor lightGrayColor]];
    
    [self addChildViewController:self.contentNavController];
    self.contentNavController.view.frame = CGRectMake(0, 101, 808, 449);
    [fondo addSubview:self.contentNavController.view];
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(!self.modelo)
        return;
    
    [self cargaEstadisticas:TipoEstadisticasJugadorGenerales];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    
    if(!self.modelo)
        return;
    
    if(self.contentNavController.topViewController != [EstadisticasDiasJugadorViewController class])
        [self.contentNavController popToRootViewControllerAnimated:YES];
    
    if(self.modelo.cargado) {
        self.statsDias.modelo = self.modelo;
    } else {
        [self.modelo carga:^(NSError * error) {
            if(!error) {
                self.statsDias.modelo = self.modelo;
            }
        } conProgreso:YES];
    }
}

- (void)cargaEstadisticas:(TipoEstadisticasJugador)tipo {
    if(self.contentNavController.topViewController != [EstadisticasDiasJugadorViewController class])
        [self.contentNavController popToRootViewControllerAnimated:YES];
    
    [self.statsDias cargaEstadisticas:tipo];
}

@end
