//
//  EstaditicasDiasViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 24/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadisticasDiasJugadorViewController.h"
#import "SelectorFechas.h"
#import "PanelProgresoJugador.h"
#import "PanelPromedioJugador.h"
#import "PanelProgresoJuegoJugador.h"
#import "PanelTiemposJuegoJugador.h"
#import "PanelErroresJuegoJugador.h"
#import "PanelAciertosJuegoJugador.h"
#import "EstadisticasSesionesViewController.h"
#import "DateUtils.h"
#import "UIColor+Extensions.h"

@interface EstadisticasDiasJugadorViewController ()
@property (nonatomic, strong) SelectorFechas* selectorFechas;
@property (nonatomic, strong) NSArray* paneles;
@property (nonatomic, assign) TipoJuego tipoJuego;

@property (nonatomic, strong) UIButton* btnInicio;
@property (nonatomic, strong) UIButton* btnFinal;

@property (nonatomic, strong) NSDate* fechaInicial;
@property (nonatomic, strong) NSDate* fechaFinal;
@property (nonatomic, strong) UIDatePicker* datePicker;
@property (nonatomic, strong) UIPopoverController* popOver;

@property (nonatomic, strong) UIView* fondoCabecera;
@end

@implementation EstadisticasDiasJugadorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.fondoCabecera = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 808, 63)];
    self.fondoCabecera.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0];
    [self.view addSubview:self.fondoCabecera];
    
    
    UIImage *imgFondoBoton = [UIImage imageNamed:@"btnfechabarra.png"];
    
    self.btnInicio = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnInicio.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:14];
    [self.btnInicio setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnInicio setTitleEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 20)];
    [self.btnInicio setBackgroundImage:imgFondoBoton forState:UIControlStateNormal];
    self.btnInicio.frame = CGRectMake(0.0, 0.0, imgFondoBoton.size.width, imgFondoBoton.size.height);
    [self.btnInicio addTarget:self action:@selector(botonFechaDado:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *inicioButton = [[UIBarButtonItem alloc] initWithCustomView:self.btnInicio];
    
    self.btnFinal = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnFinal.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:14];
    [self.btnFinal setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnFinal setTitleEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 20)];
    self.btnFinal.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.btnFinal setBackgroundImage:imgFondoBoton forState:UIControlStateNormal];
    self.btnFinal.frame = CGRectMake(0.0, 0.0, imgFondoBoton.size.width, imgFondoBoton.size.height);
    [self.btnFinal addTarget:self action:@selector(botonFechaDado:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *finalButton = [[UIBarButtonItem alloc] initWithCustomView:self.btnFinal];
    
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                      target:nil action:nil];
    spacer.width = 30;
    
    self.navigationItem.rightBarButtonItems = @[spacer, finalButton, inicioButton];
    
    
    [self actualizaBotonesFecha];
    
}

- (void) botonFechaDado:(UIButton *) button {
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.datePicker addTarget:self action:@selector(cambioPickerDate:) forControlEvents:UIControlEventValueChanged];
    
    UIViewController* vc = [[UIViewController alloc] init];
    vc.preferredContentSize = self.datePicker.frame.size;
    vc.view = self.datePicker;
    
    if(button==self.btnInicio) {
        vc.title = @"Fecha Inicial";
        self.datePicker.tag = 0;
        self.datePicker.date = self.fechaInicial;
    } else {
        vc.title = @"Fecha Final";
        self.datePicker.tag = 1;
        self.datePicker.date = self.fechaFinal;
    }
    
    if(self.popOver)
        [self.popOver dismissPopoverAnimated:YES];
    
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
    //TODO ajustar este popOver en iOS 6
    nc.navigationBar.backgroundColor = [UIColor verdeAzulado];
    nc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:nc];
    
    
    [self.popOver setPopoverContentSize:vc.view.frame.size animated:NO];
    [self.popOver presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    // [self.popOver presentPopoverFromBarButtonItem:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void) cambioPickerDate:(UIDatePicker *) picker {
    NSDate* fecha = picker.date;
    switch (picker.tag) {
        case 0:
            if([fecha compare:self.fechaFinal]==NSOrderedAscending)
                self.fechaInicial = fecha;
            break;
        case 1:
            if([self.fechaInicial compare:fecha]==NSOrderedAscending)
                self.fechaFinal = fecha;
            break;
    }
    
    [self actualizaBotonesFecha];
    for (PanelEstadisticasJugador* panel in self.paneles) {
        panel.datasource = self;
    }
    
}


- (void)setModelo:(EntrenamientoPacienteModel *)modelo {
    _modelo = modelo;
    
    if(self.modelo.entrenamiento && self.modelo.entrenamiento.diasDeEntrenamiento.count) {
        DiaEntrenamiento* primerDia = [self.modelo.entrenamiento.diasDeEntrenamiento firstObject];
        DiaEntrenamiento* ultimoDia = [self.modelo.entrenamiento.diasDeEntrenamiento lastObject];
        //TODO hacer comprobaciones
        self.fechaInicial = primerDia.fecha;
        self.fechaFinal = ultimoDia.fecha;
        
        [self actualizaBotonesFecha];
        
        for (PanelEstadisticasJugador* panel in self.paneles) {
            panel.datasource = self;
        }
    }
}

- (void) actualizaBotonesFecha {
    if(self.fechaInicial && self.fechaFinal) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [self.btnInicio setTitle:[formatter stringFromDate:self.fechaInicial] forState:UIControlStateNormal];
        [self.btnFinal setTitle:[formatter stringFromDate:self.fechaFinal] forState:UIControlStateNormal];
    }
}

- (void)cargaEstadisticas:(TipoEstadisticasJugador)tipo {
    if(self.paneles) {
        for (PanelEstadisticasJugador* panel in self.paneles) {
            [panel removeFromSuperview];
        }
        self.paneles = nil;
    }
    
    self.fondoCabecera.hidden = YES;
    
    switch (tipo) {
        case TipoEstadisticasJugadorGenerales: {
            self.fondoCabecera.hidden = NO;
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJugador* panel = [[PanelProgresoJugador alloc] initWithFrame:CGRectMake(39, 0, 734, 185)];
            panel.datasource = self;
            [paneles addObject:panel];
            [self.view addSubview:panel];

            PanelPromedioJugador* panel2 = [[PanelPromedioJugador alloc] initWithFrame:CGRectMake(39, 185, 734, 206)];
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.view addSubview:panel2];

            self.paneles = paneles.copy;
            
            break;
        }
            
        case TipoEstadisticasJugadorGongs: {
            self.tipoJuego = TipoJuegoGongs;
            
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJuegoJugador* panel = [[PanelProgresoJuegoJugador alloc] initWithFrame:CGRectMake(40, 0, 352, 195) andTipoJuego:self.tipoJuego];
            panel.delegate = self;
            panel.datasource = self;
            [paneles addObject:panel];
            [self.view addSubview:panel];
            
            PanelTiemposJuegoJugador* panel2 = [[PanelTiemposJuegoJugador alloc] initWithFrame:CGRectMake(425, 0, 352, 195) andTipoJuego:self.tipoJuego];

            panel2.delegate = self;
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.view addSubview:panel2];

            PanelAciertosJuegoJugador* panel3 = [[PanelAciertosJuegoJugador alloc] initWithFrame:CGRectMake(40, 202, 352, 195) andTipoJuego:self.tipoJuego];
            panel3.delegate = self;
            panel3.datasource = self;
            [paneles addObject:panel3];
            [self.view addSubview:panel3];

            PanelErroresJuegoJugador* panel4 = [[PanelErroresJuegoJugador alloc] initWithFrame:CGRectMake(425, 202, 352, 195) andTipoJuego:self.tipoJuego];
            panel4.delegate = self;
            panel4.datasource = self;
            [paneles addObject:panel4];
            [self.view addSubview:panel4];

            self.paneles = paneles.copy;
            break;
        }
            
        case TipoEstadisticasJugadorInvasion: {
            self.tipoJuego = TipoJuegoInvasion;
            
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJuegoJugador* panel = [[PanelProgresoJuegoJugador alloc] initWithFrame:CGRectMake(40, 0, 352, 195) andTipoJuego:self.tipoJuego];
            panel.delegate = self;
            panel.datasource = self;
            [paneles addObject:panel];
            [self.view addSubview:panel];
            
            PanelTiemposJuegoJugador* panel2 = [[PanelTiemposJuegoJugador alloc] initWithFrame:CGRectMake(425, 0, 352, 195) andTipoJuego:self.tipoJuego];
            
            panel2.delegate = self;
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.view addSubview:panel2];
            
            PanelAciertosJuegoJugador* panel3 = [[PanelAciertosJuegoJugador alloc] initWithFrame:CGRectMake(40, 202, 352, 195) andTipoJuego:self.tipoJuego];
            panel3.delegate = self;
            panel3.datasource = self;
            [paneles addObject:panel3];
            [self.view addSubview:panel3];
            
            PanelErroresJuegoJugador* panel4 = [[PanelErroresJuegoJugador alloc] initWithFrame:CGRectMake(425, 202, 352, 195) andTipoJuego:self.tipoJuego];
            panel4.delegate = self;
            panel4.datasource = self;
            [paneles addObject:panel4];
            [self.view addSubview:panel4];
            
            self.paneles = paneles.copy;
            break;
        }
        case TipoEstadisticasJugadorPoker: {
            self.tipoJuego = TipoJuegoPoker;
            
            NSMutableArray* paneles = [[NSMutableArray alloc] init];
            
            PanelProgresoJuegoJugador* panel = [[PanelProgresoJuegoJugador alloc] initWithFrame:CGRectMake(40, 0, 352, 195) andTipoJuego:self.tipoJuego];
            panel.delegate = self;
            panel.datasource = self;
            [paneles addObject:panel];
            [self.view addSubview:panel];
            
            PanelTiemposJuegoJugador* panel2 = [[PanelTiemposJuegoJugador alloc] initWithFrame:CGRectMake(425, 0, 352, 195) andTipoJuego:self.tipoJuego];
            
            panel2.delegate = self;
            panel2.datasource = self;
            [paneles addObject:panel2];
            [self.view addSubview:panel2];
            
            PanelAciertosJuegoJugador* panel3 = [[PanelAciertosJuegoJugador alloc] initWithFrame:CGRectMake(40, 202, 352, 195) andTipoJuego:self.tipoJuego];
            panel3.delegate = self;
            panel3.datasource = self;
            [paneles addObject:panel3];
            [self.view addSubview:panel3];
            
            PanelErroresJuegoJugador* panel4 = [[PanelErroresJuegoJugador alloc] initWithFrame:CGRectMake(425, 202, 352, 195) andTipoJuego:self.tipoJuego];
            panel4.delegate = self;
            panel4.datasource = self;
            [paneles addObject:panel4];
            [self.view addSubview:panel4];
            
            self.paneles = paneles.copy;
            break;
        }
    }
    
}

#pragma mark PanelEstadisticasDatasource
- (NSArray *) diasDeEntrenamientoEntreFechas {

    if(!self.modelo.entrenamiento || !self.modelo.entrenamiento.diasDeEntrenamiento.count)
        return nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSArray* diasEntrenados = self.modelo.entrenamiento.diasDeEntrenamiento;
    
    NSDateComponents *oneDay = [[NSDateComponents alloc] init];
    [oneDay setDay: 1];
    
    NSMutableArray* auxDias = [[NSMutableArray alloc] init];
    for (NSDate* date = self.fechaInicial; [date compare:self.fechaFinal] <= 0;
         date = [calendar dateByAddingComponents: oneDay
                                          toDate: date
                                         options: 0] ) {
             
             BOOL encontrado = NO;
             for (DiaEntrenamiento* diaEntrenado in diasEntrenados) {
                 if([DateUtils comparaSinHoraFecha:diaEntrenado.fecha conFecha:date]==NSOrderedSame) {
                     encontrado = YES;
                     [auxDias addObject:diaEntrenado];
                 }
             }
             /*
             if(!encontrado) {
                 DiaEntrenamiento* dia = [DiaEntrenamiento object];
                 dia.fecha = date;
                 [auxDias addObject:dia];
             }
              */
         }
    
    return auxDias.copy;
}


#pragma mark PanelEstadisticasDelegate
- (void)muestraDia:(DiaEntrenamiento *)dia {
    EstadisticasSesionesViewController* vc = [[EstadisticasSesionesViewController alloc] initWithDia:dia tipo:self.tipoJuego];
    vc.view.frame = self.view.frame;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
