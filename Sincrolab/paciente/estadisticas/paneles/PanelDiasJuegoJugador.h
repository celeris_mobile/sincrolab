//
//  PanelDiasJuegoJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelEstadisticasJugador.h"
#import "GraficaPartidasJugador.h"

@interface PanelDiasJuegoJugador : PanelEstadisticasJugador <GraficaPartidasJugadorDelegate>
- (id) initWithFrame:(CGRect) frame andTipoJuego:(TipoJuego) tipo;
@property (nonatomic, assign) TipoJuego tipo;
@property (nonatomic, readonly) NSArray* diasDeJuego;
@end

