//
//  PanelRecuperacionJuegoJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelDiasJuegoJugador.h"
#import "GraficaLineas.h"
#import "Constantes.h"

@interface PanelAciertosJuegoJugador : PanelDiasJuegoJugador <GraficaLineasDatasource>

@end