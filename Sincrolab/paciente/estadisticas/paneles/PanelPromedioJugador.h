//
//  PanelPromedioJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelEstadisticasJugador.h"
#import "GraficaPromedioJuegoJugador.h"

@interface PanelPromedioJugador : PanelEstadisticasJugador <GraficaPromedioJuegoJugadorDatasource>

@end
