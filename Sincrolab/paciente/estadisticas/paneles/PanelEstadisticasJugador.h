//
//  PanelEstadisticasJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaEntrenamiento.h"

@protocol PanelEstadisticasJugadorDelegate <NSObject>
- (void) muestraDia:(DiaEntrenamiento *) dia;
@end

@protocol PanelEstadisticasJugadorDatasource <NSObject>
- (NSArray *) diasDeEntrenamientoEntreFechas;
@end

@interface PanelEstadisticasJugador : UIView
- (void) configura;
- (void) generaExtras:(NSArray*) nombres conValores:(NSArray*) valores enVista:(UIView *) vista;
@property (nonatomic, weak) id<PanelEstadisticasJugadorDelegate> delegate;
@property (nonatomic, weak) id<PanelEstadisticasJugadorDatasource> datasource;
@end
