//
//  PanelEstadisticasJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelEstadisticasJugador.h"
#import "UIColor+Extensions.h"

@interface PanelEstadisticasJugador()
@property (nonatomic, strong) NSArray* valoresExtra;
@end

@implementation PanelEstadisticasJugador

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.backgroundColor = [UIColor clearColor];
    
    [self configura];
}

- (void) configura {
    
}

- (void) generaExtras:(NSArray*) nombres conValores:(NSArray*) valores enVista:(UIView *) vista {
    if(self.valoresExtra) {
        for (UILabel* label in self.valoresExtra) {
            [label removeFromSuperview];
        }
    }
    
    UIFont* font = [UIFont fontWithName:@"Lato-Bold" size:13];
    UILabel* labelAnterior;
    
    NSMutableArray* auxExtras = [[NSMutableArray alloc] init];
    for (int e = 0; e<nombres.count; e++) {
        NSString* nombre = [nombres objectAtIndex:e];
        NSNumber* valor = [valores objectAtIndex:e];
        NSMutableAttributedString* attrNombre = [[NSMutableAttributedString alloc] initWithString:nombre
                                                                                       attributes:@{NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor azulOscuro]}];
        NSAttributedString* attrValor = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %.2f", valor.floatValue]
                                                                        attributes:@{NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor colorWithRed:255/255.0 green:101/255.0 blue:62/255.0 alpha:1.0]}];
        
        [attrNombre appendAttributedString:attrValor];
        
        CGSize labelSize = [attrNombre size];
        CGRect labelFrame = CGRectMake(self.bounds.size.width-labelSize.width-labelAnterior.frame.size.width-30-(labelAnterior?10.0:0.0), 27, labelSize.width, labelSize.height);
        UILabel* label = [[UILabel alloc] initWithFrame: CGRectIntegral(labelFrame)];
        label.attributedText = attrNombre;
        [vista addSubview:label];
        [auxExtras addObject:label];
        
        labelAnterior = label;
    }
    
    self.valoresExtra = auxExtras.copy;
}


@end
