//
//  InfoPartidaJugadorViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constantes.h"
#import "DiaEntrenamiento.h"

@class InfoPartidaJugadorViewController;

@interface InfoPartidaJugadorViewController : UIViewController
- (id) initWithTipoJuego:(TipoJuego) tipo diaDeEntrenamiento:(DiaEntrenamiento *) dia valor:(NSString *) valor;
@property (nonatomic, readonly) DiaEntrenamiento* dia;
@end

