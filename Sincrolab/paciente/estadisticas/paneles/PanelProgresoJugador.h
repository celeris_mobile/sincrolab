//
//  PanelProgresoJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaLineas.h"
#import "PanelEstadisticasJugador.h"

@interface PanelProgresoJugador : PanelEstadisticasJugador <GraficaLineasDatasource, GraficaLineasDelegate>

@end
