//
//  PanelProgresoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 19/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelProgresoJugador.h"
#import "GraficaLineas.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "Constantes.h"

@interface PanelProgresoJugador()
@property (nonatomic, strong) GraficaLineas* grafica;
@end

@implementation PanelProgresoJugador
@synthesize datasource = _datasource;

- (void) configura {
    self.backgroundColor = [UIColor clearColor];
    
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(7, 13, 270, 28)];
    titulo.font = [UIFont fontWithName:@"Lato-Black" size:24];
    titulo.text = @"Progreso";
    titulo.textColor = [UIColor azulOscuro];
    [self addSubview:titulo];
    
    UILabel* subtitulo = [[UILabel alloc] initWithFrame:CGRectMake(7, 44, 270, 14)];
    subtitulo.font = [UIFont fontWithName:@"Lato-Bold" size:9];
    subtitulo.text = @"Días entrenados";
    subtitulo.textColor = [UIColor azulOscuro];
    [self addSubview:subtitulo];
    
    self.grafica = [[GraficaLineas alloc] initWithFrame:CGRectMake(0, 63, 734, 123)];
    self.grafica.ejes = GFBordesWidthMake(0, 0.5, 0, 0);
    self.grafica.dibujaLineasY = NO;
    self.grafica.dibujaLineasX = NO;
    self.grafica.dibujaEtiquetasY = NO;
    [self addSubview:self.grafica];
    
    UIView* cuadradoGongs = [[UIView alloc] initWithFrame:CGRectMake(95, 46, 10, 10)];
    cuadradoGongs.backgroundColor = [UIColor colorGrafica:0 conAlpha:1.0];
    [self addSubview:cuadradoGongs];
    
    UILabel* gongsLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 44, 40, 15)];
    gongsLabel.font = [UIFont fontWithName:@"Lato-Bold" size:9];
    gongsLabel.textColor = [UIColor azulOscuro];
    gongsLabel.text = @"ToGong";
    [self addSubview:gongsLabel];
    
    UIView* cuadradoInvasion = [[UIView alloc] initWithFrame:CGRectMake(152, 46, 10, 10)];
    cuadradoInvasion.backgroundColor = [UIColor colorGrafica:1 conAlpha:1.0];
    [self addSubview:cuadradoInvasion];
    
    UILabel* invasionLabel = [[UILabel alloc] initWithFrame:CGRectMake(168, 44, 80, 15)];
    invasionLabel.font = [UIFont fontWithName:@"Lato-Bold" size:9];
    invasionLabel.textColor = [UIColor azulOscuro];
    invasionLabel.text = @"Invasión Samurai";
    [self addSubview:invasionLabel];
    
    UIView* cuadradoPoker = [[UIView alloc] initWithFrame:CGRectMake(249, 46, 10, 10)];
    cuadradoPoker.backgroundColor = [UIColor colorGrafica:2 conAlpha:1.0];
    [self addSubview:cuadradoPoker];
    
    UILabel* pokerLabel = [[UILabel alloc] initWithFrame:CGRectMake(265, 44, 80, 15)];
    pokerLabel.font = [UIFont fontWithName:@"Lato-Bold" size:9];
    pokerLabel.textColor = [UIColor azulOscuro];
    pokerLabel.text = @"Poker Samurai";
    [self addSubview:pokerLabel];

}

- (void) botonDado:(UIButton *) boton {
    boton.selected = !boton.selected;
    [boton setBackgroundColor:[UIColor colorGrafica:boton.tag conAlpha:boton.selected?1.0:0.5]];
    [self.grafica dibuja];
}

- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    _datasource = datasource;
    
    BOOL hayValores = YES;
    if(!self.datasource.diasDeEntrenamientoEntreFechas.count)
        hayValores = NO;
    
    if(hayValores) {
        NSArray* sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"]];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"]];
        
        if(!sesiones.count)
            hayValores = NO;
    }
    
    if(!hayValores) {
        self.grafica.hidden = YES;
        self.grafica.dataSource = nil;
        self.grafica.delegate = nil;
        
        return;
    }
    
    
    self.grafica.hidden = NO;
    self.grafica.delegate = self;
    self.grafica.dataSource = self;
    
    [self.grafica dibuja];
}

#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    if(!self.datasource.diasDeEntrenamientoEntreFechas)
        return 0;
    return 3;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    NSNumber* nivelAnterior = [NSNumber numberWithInt:0];
    NSMutableArray* array = [[NSMutableArray alloc] init];
    //for (DiaEntrenamiento* dia in self.modelo.entrenamiento.diasDeEntrenamiento) {
    for (DiaEntrenamiento* dia in self.datasource.diasDeEntrenamientoEntreFechas) {
        PFObject* partida;
        switch (numSet) {
            case 0:
                partida = dia.partidaGongs;
                break;
            case 1:
                partida = dia.partidaInvasion;
                break;
            case 2:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            //[array addObject:[partida valueForKey:@"cambioNivel"]];
            NSNumber* cambioNivel = [partida valueForKey:@"cambioNivel"];
            [array addObject:[NSNumber numberWithInt:cambioNivel.intValue+nivelAnterior.intValue]];
            nivelAnterior = cambioNivel;
            
        } else {
            [array addObject:[NSNull null]];
        }
    }
    
    /*
    NSMutableArray *valores = [[NSMutableArray alloc] init];
    NSNumber* anterior = [NSNumber numberWithInt:0];
    for (NSNumber* valor in array) {
        if(valor!=(id)[NSNull null]) {
            [valores addObject:[NSNumber numberWithInt:anterior.intValue+valor.intValue]];
            anterior = [NSNumber numberWithInt:anterior.intValue+valor.intValue];
        } else {
            [valores addObject:[NSNull null]];
        }
    }
    */
    return array.copy;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    //for (DiaEntrenamiento* dia in self.modelo.entrenamiento.diasDeEntrenamiento) {
    int d = 1;
    for (DiaEntrenamiento* dia in self.datasource.diasDeEntrenamientoEntreFechas) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        //[array addObject:[formatter stringFromDate:dia.fecha]];
        [array addObject:[NSString stringWithFormat:@"%d", d]];
        d++;
    }
    
    return array.copy;
}

#pragma mark GraficaLineasDelegate
- (void)grafica:(GraficaLineas *)grafica configuraSet:(NSInteger)numSet enContexto:(CGContextRef)context {
    CGContextSetStrokeColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:1.0].CGColor);
    CGContextSetFillColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:0.4].CGColor);
    CGContextSetLineWidth(context, 4);
    
}

- (void)grafica:(GraficaLineas *)grafica dibujaPunto:(CGPoint)punto deSet:(NSInteger) numSet conValor:(NSNumber *)valor enContexto:(CGContextRef)context {
/*    CGFloat radio = 10;
    CGContextAddEllipseInRect(context, CGRectMake(punto.x-radio/2, punto.y-radio/2, radio, radio));
    CGContextSetFillColorWithColor(context, [UIColor colorGrafica:numSet conAlpha:1.0].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:1.0 alpha:1.0].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextDrawPath(context, kCGPathEOFillStroke);
 */
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor clearColor].CGColor);

}

@end
