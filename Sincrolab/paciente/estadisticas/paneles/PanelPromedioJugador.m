//
//  PanelPromedioJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelPromedioJugador.h"
#import "GraficaPromedioJuegoJugador.h"
#import "UIColor+Extensions.h"

@interface PanelPromedioJugador()
@property (nonatomic, strong) GraficaPromedioJuegoJugador* graficaGongs;
@property (nonatomic, strong) GraficaPromedioJuegoJugador* graficaInvasion;
@property (nonatomic, strong) GraficaPromedioJuegoJugador* graficaPoker;
@end

@implementation PanelPromedioJugador
@synthesize datasource = _datasource;

- (void) configura {
    self.graficaGongs = [[GraficaPromedioJuegoJugador alloc] initWithJuego:TipoJuegoGongs];
    CGRect gongsFrame = self.graficaGongs.frame;
    gongsFrame.origin = CGPointMake(0, 15);
    self.graficaGongs.frame = gongsFrame;
    [self addSubview:self.graficaGongs];
    
    self.graficaInvasion = [[GraficaPromedioJuegoJugador alloc] initWithJuego:TipoJuegoInvasion];
    CGRect invasionFrame = self.graficaInvasion.frame;
    invasionFrame.origin = CGPointMake(249, 15);
    self.graficaInvasion.frame = invasionFrame;
    [self addSubview:self.graficaInvasion];
    
    self.graficaPoker = [[GraficaPromedioJuegoJugador alloc] initWithJuego:TipoJuegoPoker];
    CGRect pokerFrame = self.graficaPoker.frame;
    pokerFrame.origin = CGPointMake(498, 15);
    self.graficaPoker.frame = pokerFrame;
    [self addSubview:self.graficaPoker];
    
    UIView* separadorj1 = [[UIView alloc] initWithFrame:CGRectMake(245, 90, 1, 114)];
    separadorj1.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separadorj1];
    
    UIView* separadorj2 = [[UIView alloc] initWithFrame:CGRectMake(494, 90, 1, 114)];
    separadorj2.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:separadorj2];    
    
}

- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    _datasource = datasource;
    
    BOOL hayValores = YES;
    if(!self.datasource.diasDeEntrenamientoEntreFechas.count)
        hayValores = NO;
    
    if(hayValores) {
        NSArray* sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"]];
        sesiones = [sesiones arrayByAddingObjectsFromArray:[self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"]];
        
        if(!sesiones.count)
            hayValores = NO;
    }
    
    if(!hayValores) {
        self.graficaGongs.hidden = YES;
        self.graficaInvasion.hidden = YES;
        self.graficaPoker.hidden = YES;
        
        self.graficaGongs.datasource = nil;
        self.graficaInvasion.datasource = nil;
        self.graficaPoker.datasource = nil;
        
        return;
    }
    
    
    self.graficaGongs.datasource = self;
    self.graficaInvasion.datasource = self;
    self.graficaPoker.datasource = self;
    
    self.graficaGongs.hidden = NO;
    self.graficaInvasion.hidden = NO;
    self.graficaPoker.hidden = NO;
    
}

- (NSArray *) valoresParaJuego:(TipoJuego) tipoJuego {
    NSNumber* pComisiones;
    NSNumber* pOmisiones;
    NSNumber* pAciertos;
    NSArray* sesiones;
    switch (tipoJuego) {
        case TipoJuegoGongs: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaGongs.sesiones"];
            break;
        }
        case TipoJuegoInvasion: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaInvasion.sesiones"];
            break;
        }
        case TipoJuegoPoker: {
            sesiones = [self.datasource.diasDeEntrenamientoEntreFechas valueForKeyPath: @"@unionOfArrays.partidaPoker.sesiones"];
            break;
        }
    }
    
    
    pAciertos = [sesiones valueForKeyPath:@"@sum.numeroDeAciertos"];
    pOmisiones = [sesiones valueForKeyPath:@"@sum.numeroDeOmisiones"];
    pComisiones = [sesiones valueForKeyPath:@"@sum.numeroDeComisiones"];
    NSNumber* pErrores = [NSNumber numberWithInt:pOmisiones.intValue+pComisiones.intValue];

    return @[@{@"grupo":@"Aciertos", @"cantidad":pAciertos}, @{@"grupo":@"Errores", @"cantidad":pErrores}];
}


@end

