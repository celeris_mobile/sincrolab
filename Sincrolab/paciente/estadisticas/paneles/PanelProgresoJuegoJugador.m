//
//  PanelProgresoJuegoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelProgresoJuegoJugador.h"
#import "GraficaPartidasJugador.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"

@interface PanelProgresoJuegoJugador()
@property (nonatomic, strong) GraficaPartidasJugador* grafica;
@property (nonatomic, strong) NSArray* niveles;
@property (nonatomic, strong) NSArray* fechas;
@end

@implementation PanelProgresoJuegoJugador


- (void) configura {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 352, 60)];
    fondo.backgroundColor = [UIColor colorWithRed:229/255.0 green:227/255.0 blue:225/255.0 alpha:1.0];
    [self addSubview:fondo];
    
    self.grafica = [[GraficaPartidasJugador alloc] initWithTitulo:@"Progreso" colores:@[[UIColor colorGrafica:self.tipo conAlpha:1.0]]];
    self.grafica.delegate = self;
    [self addSubview:self.grafica];
}


- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    [super setDatasource:datasource];
    
    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        self.grafica.hidden = YES;
        self.grafica.grafica.dataSource = nil;
        
        return;
    }
    
    self.grafica.grafica.dataSource = self;
    self.grafica.hidden = NO;
    
    NSMutableArray* auxNiveles = [[NSMutableArray alloc] init];
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    
    int d = 1;
    NSNumber* nivelAnterior = [NSNumber numberWithInt:0]; //TODO ¿El inicial del usuario?
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* cambioNivel = [partida valueForKey:@"cambioNivel"];
            [auxNiveles addObject:[NSNumber numberWithInt:cambioNivel.intValue+nivelAnterior.intValue]];
            nivelAnterior = cambioNivel;
        } else {
            [auxNiveles addObject:[NSNull null]];
        }
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
        //[auxFechas addObject:[NSString stringWithFormat:@"%d", d]];
        d++;
    }
    
    self.niveles = auxNiveles.copy;
    self.fechas = auxFechas.copy;
    
    [self.grafica.grafica dibuja];
}

#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    return self.niveles;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}



@end