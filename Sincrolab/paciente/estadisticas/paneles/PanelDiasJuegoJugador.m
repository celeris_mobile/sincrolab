//
//  PanelDiasJuegoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelDiasJuegoJugador.h"
#import "CustomPopOverBackgroundView.h"
#import "InfoPartidaJugadorViewController.h"

@interface PanelDiasJuegoJugador()
@property (nonatomic, strong, readwrite) NSArray* diasDeJuego;
@property (nonatomic, strong) UIPopoverController* popOver;
@end

@implementation PanelDiasJuegoJugador

- (id) initWithFrame:(CGRect) frame andTipoJuego:(TipoJuego) tipo {
    self = [super initWithFrame:frame];
    if(self) {
        self.tipo = tipo;
    }
    
    return self;
}



- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    [super setDatasource:datasource];
    
    BOOL hayPartidas = NO;
    NSMutableArray* auxDiasDeJuego = [[NSMutableArray alloc] init];
    for (DiaEntrenamiento* dia in self.datasource.diasDeEntrenamientoEntreFechas) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida)
            hayPartidas = YES;
        
        [auxDiasDeJuego addObject:dia];
    }
    
    if(hayPartidas)
        self.diasDeJuego = auxDiasDeJuego.copy;
    else
        self.diasDeJuego = nil;
    
}

#pragma mark GraficaPartidasDelegate
- (void) muestraPopoverParaGrafica:(GraficaPartidasJugador *)grafica dePunto:(NSIndexPath *)indexPath enPosicion:(CGPoint)punto conValor:(NSString *)valor {
    

    if(self.popOver) {
        [self.popOver dismissPopoverAnimated:NO];
    }
    
    InfoPartidaJugadorViewController* vc = [[InfoPartidaJugadorViewController alloc] initWithTipoJuego:self.tipo diaDeEntrenamiento:[self.diasDeJuego objectAtIndex:indexPath.row] valor:valor];
    
    
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:vc];
    self.popOver.popoverBackgroundViewClass = [CustomPopOverBackgroundView class];
    [self.popOver presentPopoverFromRect:CGRectMake(punto.x-4, punto.y-4, 8, 8) inView:grafica permittedArrowDirections:UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp animated:YES];

}

@end
