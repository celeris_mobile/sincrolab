//
//  PanelAciertosJuegoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelTiemposJuegoJugador.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"

@interface PanelTiemposJuegoJugador()
@property (nonatomic, strong) GraficaPartidasJugador* graficaTiempos;


@property (nonatomic, strong) NSArray* paciertos;
@property (nonatomic, strong) NSArray* tiemposMedios;
@property (nonatomic, strong) NSArray* fechas;

@end

@implementation PanelTiemposJuegoJugador

- (void) configura {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 352, 60)];
    fondo.backgroundColor = [UIColor colorWithRed:229/255.0 green:227/255.0 blue:225/255.0 alpha:1.0];
    [self addSubview:fondo];
    

    self.graficaTiempos = [[GraficaPartidasJugador alloc] initWithTitulo:@"Tiempos de Respuesta" colores:@[[UIColor colorWithRed:149/255.0 green:189/255.0 blue:13/255.0 alpha:1.0]]];
    self.graficaTiempos.delegate = self;
    self.graficaTiempos.grafica.prefijoEtiquetasY = @"ms";
    self.graficaTiempos.grafica.minY = 0.0;
    
    
    
    [self addSubview:self.graficaTiempos];
    
}

- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    [super setDatasource:datasource];
    
    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        //TODO mensaje de que no hay datos
        self.graficaTiempos.hidden = YES;
        self.graficaTiempos.grafica.dataSource = nil;
        
        return;
    }
    
    
    self.graficaTiempos.grafica.dataSource = self;
    self.graficaTiempos.hidden = NO;
    
    NSMutableArray* auxPAciertos = [[NSMutableArray alloc] init];
    NSMutableArray* auxTiempos = [[NSMutableArray alloc] init];
    NSMutableArray* auxTiemposCalc = [[NSMutableArray alloc] init];
    
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    
    int d = 1;
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* aciertos = [partida valueForKeyPath:@"sesiones.@avg.porcentajeAciertos"];
            [auxPAciertos addObject:aciertos];
            
            NSNumber* tiempos = [partida valueForKeyPath:@"sesiones.@avg.tiempoMedioDeRespuestaAciertos"];
            [auxTiempos addObject:[NSNumber numberWithInt:(int)(tiempos.floatValue*1000)]];
            
            [auxTiemposCalc addObject:tiempos];
        } else {
            [auxPAciertos addObject:[NSNull null]];
            [auxTiempos addObject:[NSNull null]];
        }
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
        d++;
    }
    
    self.paciertos = auxPAciertos.copy;
    self.fechas = auxFechas.copy;
    self.tiemposMedios = auxTiempos.copy;
    
    NSArray* nombres = @[@"Media"];
    NSArray* valores = @[[auxTiemposCalc valueForKeyPath:@"@avg.self"]];
    [self generaExtras:nombres conValores:valores enVista:self.graficaTiempos];
    
    [self.graficaTiempos.grafica dibuja];
}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    return self.tiemposMedios;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}


@end