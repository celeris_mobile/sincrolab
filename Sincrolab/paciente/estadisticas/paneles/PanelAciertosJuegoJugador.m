//
//  PanelRecuperacionJuegoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelAciertosJuegoJugador.h"
#import "GraficaPartidasJugador.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"

@interface PanelAciertosJuegoJugador()
@property (nonatomic, strong) GraficaPartidasJugador* grafica;
@property (nonatomic, strong) NSArray* aciertos;
@property (nonatomic, strong) NSArray* fechas;
@end

@implementation PanelAciertosJuegoJugador

- (CGFloat)altura {
    return 270;
}

- (void) configura {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 352, 60)];
    fondo.backgroundColor = [UIColor colorWithRed:229/255.0 green:227/255.0 blue:225/255.0 alpha:1.0];
    [self addSubview:fondo];
    
    self.grafica = [[GraficaPartidasJugador alloc] initWithTitulo:@"Aciertos" colores:@[[UIColor colorWithRed:23/255.0 green:181/255.0 blue:212/255.0 alpha:1.0]]];
    self.grafica.delegate = self;
    self.grafica.grafica.minY = 0.0;
    self.grafica.grafica.maxY = 100.0;
    self.grafica.grafica.saltoY = 25.0;
    self.grafica.grafica.prefijoEtiquetasY = @"%";
    [self addSubview:self.grafica];
}

- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    [super setDatasource:datasource];
    
    
    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        self.grafica.hidden = YES;
        self.grafica.grafica.dataSource = nil;
        return;
    }
    
    
    self.grafica.grafica.dataSource = self;
    self.grafica.hidden = NO;
    
    NSMutableArray* auxAciertos = [[NSMutableArray alloc] init];
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    
    int d = 1;
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* recup = [partida valueForKeyPath:@"sesiones.@avg.porcentajeAciertos"];
            [auxAciertos addObject:recup];
        } else {
            [auxAciertos addObject:[NSNull null]];
        }
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
        d++;
    }
    
    self.aciertos = auxAciertos.copy;
    self.fechas = auxFechas.copy;
    
    NSArray* nombres = @[@"% Media"];
    NSArray* valores = @[[auxAciertos valueForKeyPath:@"@avg.self"]];
    [self generaExtras:nombres conValores:valores enVista:self.grafica];

    
    [self.grafica.grafica dibuja];
}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    return self.aciertos;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}



@end
