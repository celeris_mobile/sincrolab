//
//  InfoPartidaJugadorViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "InfoPartidaJugadorViewController.h"

@interface InfoPartidaJugadorViewController ()
@property (nonatomic, assign) TipoJuego tipo;
@property (nonatomic, assign) NSString* valor;
@end

@implementation InfoPartidaJugadorViewController

- (id) initWithTipoJuego:(TipoJuego) tipo diaDeEntrenamiento:(DiaEntrenamiento *) dia valor:(NSString *) valor {
    self = [super init];
    if(self) {
        _tipo = tipo;
        _dia = dia;
        _valor = valor;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    id partida;
    switch (self.tipo) {
        case TipoJuegoGongs:
            partida = self.dia.partidaGongs;
            break;
        case TipoJuegoInvasion:
            partida = self.dia.partidaInvasion;
            break;
        case TipoJuegoPoker:
            partida = self.dia.partidaPoker;
            break;
    }
    NSArray* sesiones = [partida valueForKey:@"sesiones"];
    
    self.preferredContentSize = CGSizeMake(103, 109);
    self.view.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    UILabel* labelValor = [[UILabel alloc] initWithFrame:CGRectMake(5, 13, 98, 20)];
    labelValor.textAlignment = NSTextAlignmentCenter;
    labelValor.font = [UIFont fontWithName:@"Lato-Black" size:24];
    labelValor.textColor = [UIColor colorWithRed:255/255.0 green:112/255.0 blue:75/255.0 alpha:1.0];
    labelValor.text = self.valor;
    labelValor.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:labelValor];
    
    UILabel* labelFecha = [[UILabel alloc] initWithFrame:CGRectMake(5, 44, 98, 10)];
    labelFecha.textAlignment = NSTextAlignmentCenter;
    labelFecha.font = [UIFont fontWithName:@"Lato-Black" size:10];
    labelFecha.textColor = [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0];
    labelFecha.text = [formatter stringFromDate:self.dia.fecha];
    [self.view addSubview:labelFecha];
    
    UILabel* labelNumSesiones = [[UILabel alloc] initWithFrame:CGRectMake(5, 60, 98, 10)];
    labelNumSesiones.textAlignment = NSTextAlignmentCenter;
    labelNumSesiones.font = [UIFont fontWithName:@"Lato-Black" size:10];
    labelNumSesiones.textColor = [UIColor whiteColor];
    labelNumSesiones.text = [NSString stringWithFormat:@"%d sesiones", sesiones.count];
    [self.view addSubview:labelNumSesiones];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
