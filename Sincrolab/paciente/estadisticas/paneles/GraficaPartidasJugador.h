//
//  GraficaPartidasJugador.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraficaLineas.h"

@class GraficaPartidasJugador;

@protocol GraficaPartidasJugadorDelegate <NSObject>
- (void) muestraPopoverParaGrafica:(GraficaPartidasJugador *)grafica dePunto:(NSIndexPath *) indexPath enPosicion:(CGPoint) punto conValor:(NSString *) valor;
@end

@interface GraficaPartidasJugador : UIView <GraficaLineasDelegate>
- (id) initWithTitulo:(NSString *) titulo colores:(NSArray *) colores;
- (id) initWithTitulo:(NSString *) titulo colores:(NSArray *) colores leyenda:(NSArray *) leyenda;
@property (nonatomic, weak) id<GraficaPartidasJugadorDelegate> delegate;

@property (nonatomic, strong) GraficaLineas* grafica;
@end
