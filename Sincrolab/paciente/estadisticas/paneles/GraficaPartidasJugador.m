//
//  GraficaPartidasJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaPartidasJugador.h"
#import "UIColor+Extensions.h"

@interface GraficaPartidasJugador()
@property (nonatomic, strong) NSArray* colores;
@property (nonatomic, strong) NSArray* leyenda;
@end

@implementation GraficaPartidasJugador

- (id) initWithTitulo:(NSString *) titulo colores:(NSArray *) colores {
    self = [self initWithTitulo:titulo colores:colores leyenda:nil];
    if(self) {
        
    }
    return self;
}


- (id) initWithTitulo:(NSString *) titulo colores:(NSArray *) colores leyenda:(NSArray *) leyenda {
    self = [super initWithFrame:CGRectMake(0, 0, 356, 151)];
    if (self) {
        _colores = colores;
        _leyenda = leyenda;
        
        UILabel* tituloLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 17, 220, 28)];
        tituloLabel.font = [UIFont fontWithName:@"Lato-Black" size:20];
        tituloLabel.text = titulo;
        tituloLabel.textColor = [UIColor azulOscuro];
        [self addSubview:tituloLabel];
        
        self.grafica = [[GraficaLineas alloc] initWithFrame:CGRectMake(0, 72, 350, 125)];
        self.grafica.dibujaLineasX = NO;
        self.grafica.dibujaLineasY = YES;
        self.grafica.dibujaEtiquetasY = YES;
        self.grafica.delegate = self;
        [self addSubview:self.grafica];
        
        if(self.leyenda)
            [self dibujaLeyenda];
    }
    return self;
}

- (void) dibujaLeyenda {
    float espacio = 40;
    UIFont* font = [UIFont fontWithName:@"Lato-Bold" size:11];
    for (int c = 0; c<self.colores.count; c++) {
        UIColor* color = [self.colores objectAtIndex:c];
        NSString* nombre = [self.leyenda objectAtIndex:c];
        
        UIView* elemento = [[UIView alloc] init];
        UIView* cuadrito = [[UIView alloc] initWithFrame:CGRectMake(0, 19, 15, 15)];
        cuadrito.backgroundColor = color;
        [elemento addSubview:cuadrito];
        
        NSAttributedString* attrString = [[NSAttributedString alloc] initWithString:nombre
                                                                         attributes:@{
                                                                                      NSFontAttributeName:font,
                                                                                      NSForegroundColorAttributeName:[UIColor azulOscuro]}];
        CGSize sizeLabel = [attrString size];
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 21, sizeLabel.width, sizeLabel.height)];
        label.attributedText = attrString;
        [elemento addSubview:label];
        
        CGRect elementoFrame = elemento.frame;
        elementoFrame.origin.x = self.bounds.size.width-sizeLabel.width-10-espacio;
        elemento.frame = elementoFrame;
        [self addSubview:elemento];
        
        espacio += sizeLabel.width+30+10;
    }
}


#pragma mark GraficaLineasDelegate
- (void)grafica:(GraficaLineas *)grafica configuraSet:(NSInteger)numSet enContexto:(CGContextRef)context {
    UIColor* color = (UIColor *)[self.colores objectAtIndex:numSet];
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextSetLineWidth(context, 3);
    
    CGContextSetFillColorWithColor(context, [color colorWithAlphaComponent:0.4].CGColor);
}

- (void)grafica:(GraficaLineas *)grafica dibujaPunto:(CGPoint)punto deSet:(NSInteger) numSet conValor:(NSNumber *)valor enContexto:(CGContextRef)context {
    UIColor* color = (UIColor *)[self.colores objectAtIndex:numSet];
    
    CGFloat radio = 10;
    CGContextAddEllipseInRect(context, CGRectMake(punto.x-radio/2, punto.y-radio/2, radio, radio));
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextDrawPath(context, kCGPathEOFillStroke);
}

- (void)grafica:(GraficaLineas *)grafica dadoValor:(NSString *) valor enIndexPath:(NSIndexPath *)indexPath posicion:(CGPoint)punto {
    if([self.delegate respondsToSelector:@selector(muestraPopoverParaGrafica:dePunto:enPosicion:conValor:)])
        [self.delegate muestraPopoverParaGrafica:self dePunto:indexPath enPosicion:punto conValor:valor];
}

@end

