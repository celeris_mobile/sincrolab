//
//  PanelErroresJuegoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelErroresJuegoJugador.h"
#import "DiaEntrenamiento.h"
#import "UIColor+Extensions.h"
#import "GraficaPartidasJugador.h"

@interface PanelErroresJuegoJugador()
@property (nonatomic, strong) GraficaPartidasJugador* graficaErrores;

@property (nonatomic, strong) NSArray* perrores;
@property (nonatomic, strong) NSArray* fechas;

@end

@implementation PanelErroresJuegoJugador

- (CGFloat)altura {
    return 540;
}

- (void) configura {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 352, 60)];
    fondo.backgroundColor = [UIColor colorWithRed:229/255.0 green:227/255.0 blue:225/255.0 alpha:1.0];
    [self addSubview:fondo];
    
    NSArray* colores = @[[UIColor colorWithRed:255/255.0 green:144/255.0 blue:116/255.0 alpha:1.0],
                         [UIColor colorWithRed:190/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                         [UIColor colorWithRed:175/255.0 green:103/255.0 blue:26/255.0 alpha:1.0]];
    
    self.graficaErrores = [[GraficaPartidasJugador alloc] initWithTitulo:@"Errores" colores:colores];
    self.graficaErrores.delegate = self;
    self.graficaErrores.grafica.minY = 0.0;
    self.graficaErrores.grafica.prefijoEtiquetasY = @"%";
    self.graficaErrores.grafica.saltoY = 10.0;
    [self addSubview:self.graficaErrores];
    
    
    
    
}

- (void)setDatasource:(id<PanelEstadisticasJugadorDatasource>)datasource {
    [super setDatasource:datasource];
    
    if(!self.diasDeJuego || !self.diasDeJuego.count) {
        //TODO mensaje de que no hay datos
        self.graficaErrores.hidden = YES;
        self.graficaErrores.grafica.dataSource = nil;
        
        return;
    }
    
    
    self.graficaErrores.grafica.dataSource = self;
    
    self.graficaErrores.hidden = NO;
    
    NSMutableArray* auxErrores = [[NSMutableArray alloc] init];
    
    NSMutableArray* auxFechas = [[NSMutableArray alloc] init];
    
    int d = 1;
    for (DiaEntrenamiento* dia in self.diasDeJuego) {
        PFObject* partida;
        switch (self.tipo) {
            case TipoJuegoGongs:
                partida = dia.partidaGongs;
                break;
            case TipoJuegoInvasion:
                partida = dia.partidaInvasion;
                break;
            case TipoJuegoPoker:
                partida = dia.partidaPoker;
                break;
        }
        if(partida) {
            NSNumber* comisiones = [partida valueForKeyPath:@"sesiones.@avg.porcentajeComisiones"];
            NSNumber* omisiones = [partida valueForKeyPath:@"sesiones.@avg.porcentajeOmisiones"];
            [auxErrores addObject:[NSNumber numberWithInt:comisiones.intValue+omisiones.intValue]];
        } else {
            [auxErrores addObject:[NSNull null]];
        }
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d'/'M"];
        [auxFechas addObject:[formatter stringFromDate:dia.fecha]];
        d++;
    }
    
    self.perrores = auxErrores.copy;
    self.fechas = auxFechas.copy;
    
    NSArray* nombres = @[@"% Media"];
    NSArray* valores = @[[auxErrores valueForKeyPath:@"@avg.self"]];
    [self generaExtras:nombres conValores:valores enVista:self.graficaErrores];

    
    
    [self.graficaErrores.grafica dibuja];

}


#pragma mark GraficaLineasDatasource
- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    return self.perrores;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    return self.fechas;
}


@end