//
//  GraficaPromedioJuegoJugador.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 05/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaPromedioJuegoJugador.h"
#import "GraficaTarta.h"
#import "UIColor+Extensions.h"

@interface GraficaPromedioJuegoJugador()
@property (nonatomic, assign) TipoJuego tipo;
@property (nonatomic, strong) UILabel* titulo;
@property (nonatomic, strong) GraficaTarta* grafica;
@property (nonatomic, strong) UIView* etiquetas;
@property (nonatomic, strong) NSArray* valores;
@property (nonatomic, strong) NSArray* otrosValores;
@property (nonatomic, strong) UIView* otrosView;
@property (nonatomic, assign) float total;
@end

@implementation GraficaPromedioJuegoJugador

- (id)initWithJuego:(TipoJuego)tipo {
    self = [super initWithFrame:CGRectMake(0, 0, 194, 252)];
    if (self) {
        self.tipo = tipo;
        [self configura];
    }
    return self;
}

- (void) configura {
    //240 60
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 60)];
    fondo.backgroundColor = [UIColor colorWithRed:229/255.0 green:227/255.0 blue:225/255.0 alpha:1.0];
    [self addSubview:fondo];
    
    self.titulo = [[UILabel alloc] initWithFrame:CGRectMake(10, 18, 230, 28)];
    self.titulo.textAlignment = NSTextAlignmentCenter;
    self.titulo.text = [Constantes nombreDeJuego:self.tipo];
    self.titulo.textColor = [UIColor darkGrayColor];
    self.titulo.font = [UIFont fontWithName:@"Lato-Black" size:18];
    [self addSubview:self.titulo];
    
    self.grafica = [[GraficaTarta alloc] initWithFrame:CGRectMake(15, 70, 114, 114)];
    self.grafica.dataSource = self;
    self.grafica.delegate = self;
    [self addSubview:self.grafica];
}

- (void)setDatasource:(id<GraficaPromedioJuegoJugadorDatasource>)datasource {
    _datasource = datasource;
    
    self.valores = [self.datasource valoresParaJuego:self.tipo];
    self.total = 0;
    for (NSDictionary* valor in self.valores) {
        self.total += ((NSNumber *)[valor objectForKey:@"cantidad"]).intValue;
    }
    
    [self.grafica reloadData];
    [self dibujaEtiquetas];
    
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* vista in self.etiquetas.subviews) {
            [vista removeFromSuperview];
        }
        
        [self.etiquetas removeFromSuperview];
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:self.etiquetas];
    
    for (int e = 0; e<[self numeroDeValoresEnGraficaDeTarta:self.grafica]; e++) {
        NSString* etiqueta = [self graficaDeTarta:self.grafica etiquetaParaIndice:e];
        UIColor* color = [self colorDeSet:e];
        
        UIView* cuadrito = [[UIView alloc] initWithFrame:CGRectMake(146, 102+e*20, 15, 15)];
        cuadrito.backgroundColor = color;
        [self.etiquetas addSubview:cuadrito];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(164, 105+e*20, 67, 12)];
        label.textColor = [UIColor azulOscuro];
        label.font = [UIFont fontWithName:@"Lato-Bold" size:11.0];
        label.text = etiqueta;
        [self.etiquetas addSubview:label];
    }
}

- (UIColor *) colorDeSet:(int) set {
    UIColor * color;
    switch (set) {
        case 0://Aciertos
            color = [UIColor colorWithRed:122/255.0 green:203/255.0 blue:161/255.0 alpha:1.0];
            break;
        case 1://Comisiones
            color = [UIColor colorWithRed:227/255.0 green:7/255.0 blue:32/255.0 alpha:1.0];
            break;
        case 2://Omisiones
            color = [UIColor colorWithRed:255/255.0 green:106/255.0 blue:68/255.0 alpha:1.0];
            break;
        case 3://Perseverativos
            color = [UIColor colorWithRed:249/255.0 green:195/255.0 blue:29/255.0 alpha:1.0];
            break;
    }
    
    return color;
}

#pragma mark GraficaTarta Delegate
- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context {
    
    UIColor* color = [self colorDeSet:numSet];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
}

#pragma mark GraficaTarta DataSource
- (int)numeroDeValoresEnGraficaDeTarta:(GraficaTarta *)grafica {
    return self.valores.count;
}

- (NSNumber *)graficaDeTarta:(GraficaTarta *)grafica valorParaIndice:(NSInteger)indice {
    NSDictionary* valor = [self.valores objectAtIndex:indice];
    NSInteger cantidad = ((NSNumber *)[valor objectForKey:@"cantidad"]).intValue;
    NSNumber* percent = [NSNumber numberWithFloat:100*((float)cantidad)/self.total];
    
    return percent;
}

- (NSString *)graficaDeTarta:(GraficaTarta *)grafica etiquetaParaIndice:(NSInteger)indice {
    NSDictionary* valor = [self.valores objectAtIndex:indice];
    return [valor objectForKey:@"grupo"];
}

@end
