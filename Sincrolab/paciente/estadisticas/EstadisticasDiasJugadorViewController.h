//
//  EstadisticasDiasJugadorViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 04/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntrenamientoPacienteModel.h"
#import "MenuEstadisticasJugador.h"
#import "PanelEstadisticasJugador.h"

@interface EstadisticasDiasJugadorViewController : UIViewController  <PanelEstadisticasJugadorDatasource, PanelEstadisticasJugadorDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
- (void) cargaEstadisticas:(TipoEstadisticasJugador) tipo;
@end
