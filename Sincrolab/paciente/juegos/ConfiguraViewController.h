//
//  ConfiguraViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 29/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EstadoJuego.h"

@protocol ConfiguraViewControllerDelegate
- (void) estadoModificado:(EstadoJuego *) estado;
@end

@interface ConfiguraViewController : UIViewController
@property (nonatomic, weak) id<ConfiguraViewControllerDelegate> delegate;
- (id) initWithEstado:(EstadoJuego *) estado;
@end
