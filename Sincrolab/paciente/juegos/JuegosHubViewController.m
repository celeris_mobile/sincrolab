//
//  JuegosHubViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 13/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "JuegosHubViewController.h"
#import "Constantes.h"
#import "JuegoGongsViewController.h"
#import "JuegoInvasionViewController.h"
#import "JuegoPokerViewController.h"

#import "DiaEntrenamiento.h"

#import "PartidaGongs.h"
#import "PartidaInvasion.h"
#import "PartidaPoker.h"

#import "EntrenamientosManager.h"
#import "ConfiguraViewController.h"
#import "CustomPopOverBackgroundView.h"

#import "UIBAlertView.h"

@interface JuegosHubViewController ()
@property (nonatomic, assign) NSUInteger indiceJuegoActual;
@property (nonatomic, strong) UIImageView* fondo;
@property (nonatomic, strong) UIButton* boton;
@property (nonatomic, strong) DiaEntrenamiento* diaEntrenamiento;

@property (nonatomic, strong) UIPopoverController* popOver;
@property (nonatomic, strong) UIButton* botonConfigura;
@end

@implementation JuegosHubViewController
@synthesize modelo = _modelo;

- (id)init {
    self = [super init];
    if (self) {
        self.indiceJuegoActual = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.fondo = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.fondo];
    
    self.boton = [[UIButton alloc] initWithFrame:CGRectMake(500, 300, 264, 76)];
    [self.boton setImage:[UIImage imageNamed:@"btn_continuar.png"] forState:UIControlStateNormal];
    [self.boton setImage:[UIImage imageNamed:@"btn_continuar_presionado.png"] forState:UIControlStateHighlighted];
    [self.boton addTarget:self action:@selector(botonDado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.boton];
    
    
    self.botonConfigura = [[UIButton alloc] initWithFrame:CGRectMake(1024-110, 40, 100, 20)];
    [self.botonConfigura setBackgroundColor:[UIColor blackColor]];
    [self.botonConfigura setTitle:@"Configura" forState:UIControlStateNormal];
    [self.botonConfigura addTarget:self action:@selector(configuraPartida) forControlEvents:UIControlEventTouchUpInside];
  //  [self.view addSubview:self.botonConfigura];
}

- (void) configuraPartida {
    NSNumber* numJuego = [self.modelo.paciente.entrenamiento.estadoEntrenamiento.juegosDeHoy objectAtIndex:self.indiceJuegoActual];
    
    EstadoJuego* estado;
    switch (numJuego.intValue) {
        case TipoJuegoGongs:
            estado = self.modelo.paciente.entrenamiento.estadoEntrenamiento.estadoGongs;
            break;
        case TipoJuegoInvasion:
            estado = self.modelo.paciente.entrenamiento.estadoEntrenamiento.estadoInvasion;
            break;
        case TipoJuegoPoker:
            estado = self.modelo.paciente.entrenamiento.estadoEntrenamiento.estadoPoker;
            break;
    }
    
    ConfiguraViewController* cvc = [[ConfiguraViewController alloc] initWithEstado:estado];
    cvc.delegate = self;
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:cvc];
    self.popOver.popoverContentSize = CGSizeMake(300, 620);
    [self.popOver presentPopoverFromRect:self.botonConfigura.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)estadoModificado:(EstadoJuego *)estado {
    [self.popOver dismissPopoverAnimated:YES];
    NSNumber* numJuego = [self.modelo.paciente.entrenamiento.estadoEntrenamiento.juegosDeHoy objectAtIndex:self.indiceJuegoActual];
    
    JuegoViewController* jvc;
    
    switch (numJuego.intValue) {
        case TipoJuegoGongs:
            self.modelo.paciente.entrenamiento.estadoEntrenamiento.estadoGongs = (EstadoJuegoGongs *)estado;
            jvc = [[JuegoGongsViewController alloc] initWithPaciente:self.modelo.paciente estadoEntrenamiento:self.modelo.paciente.entrenamiento.estadoEntrenamiento];
            break;
        case TipoJuegoInvasion:
            self.modelo.paciente.entrenamiento.estadoEntrenamiento.estadoInvasion = (EstadoJuegoInvasion *)estado;
            jvc = [[JuegoInvasionViewController alloc] initWithPaciente:self.modelo.paciente estadoEntrenamiento:self.modelo.paciente.entrenamiento.estadoEntrenamiento];
            break;
        case TipoJuegoPoker:
            self.modelo.paciente.entrenamiento.estadoEntrenamiento.estadoPoker = (EstadoJuegoPoker *)estado;
            jvc = [[JuegoPokerViewController alloc] initWithPaciente:self.modelo.paciente estadoEntrenamiento:self.modelo.paciente.entrenamiento.estadoEntrenamiento];
             break;
    }
    
    jvc.delegate = self;
    [self.navigationController pushViewController:jvc animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.fondo.frame = self.view.bounds;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    NSNumber* numJuego;
    if(self.modelo)
        numJuego = [self.modelo.paciente.entrenamiento.estadoEntrenamiento.juegosDeHoy objectAtIndex:self.indiceJuegoActual];
    else {
        NSArray* juegos = @[[NSNumber numberWithInt:TipoJuegoGongs], [NSNumber numberWithInt:TipoJuegoInvasion], [NSNumber numberWithInt:TipoJuegoPoker]];
        
        numJuego = [juegos objectAtIndex:self.indiceJuegoActual];
    }
    
    switch (numJuego.intValue) {
        case TipoJuegoGongs:
            self.fondo.image = [UIImage imageNamed:@"inicio_gongs.jpg"];
            self.boton.center = CGPointMake(440, 611);
            break;
        case TipoJuegoInvasion:
            self.fondo.image = [UIImage imageNamed:@"inicio_invasion.jpg"];
            self.boton.center = CGPointMake(514, 710);
            break;
        case TipoJuegoPoker:
            self.fondo.image = [UIImage imageNamed:@"inicio_poker.jpg"];
            self.boton.center = CGPointMake(514, 680);
            break;
    }
}

- (void) botonDado {
    JuegoViewController* jvc;
    
    if(self.modelo) {
        NSNumber* numJuego = [self.modelo.paciente.entrenamiento.estadoEntrenamiento.juegosDeHoy objectAtIndex:self.indiceJuegoActual];
        
        switch (numJuego.intValue) {
            case TipoJuegoGongs:
                jvc = [[JuegoGongsViewController alloc] initWithPaciente:self.modelo.paciente estadoEntrenamiento:self.modelo.paciente.entrenamiento.estadoEntrenamiento];
                break;
                
            case TipoJuegoInvasion:
                jvc = [[JuegoInvasionViewController alloc] initWithPaciente:self.modelo.paciente estadoEntrenamiento:self.modelo.paciente.entrenamiento.estadoEntrenamiento];
                break;
                
            case TipoJuegoPoker:
                jvc = [[JuegoPokerViewController alloc] initWithPaciente:self.modelo.paciente estadoEntrenamiento:self.modelo.paciente.entrenamiento.estadoEntrenamiento];
                break;
                
        }
    } else {
        NSArray* juegos = @[[NSNumber numberWithInt:TipoJuegoGongs], [NSNumber numberWithInt:TipoJuegoInvasion], [NSNumber numberWithInt:TipoJuegoPoker]];

        NSNumber* numJuego = [juegos objectAtIndex:self.indiceJuegoActual];
        
        EstadoEntrenamiento* estadoEntrenamiento = [EstadoEntrenamiento object];
        estadoEntrenamiento.estadoGongs = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:@"0" conAfectacion:@"1"];
        estadoEntrenamiento.estadoInvasion = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:@"6" conAfectacion:@"1"];
        [estadoEntrenamiento.estadoInvasion configuraParaHito:4];
        estadoEntrenamiento.estadoPoker = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:@"10" conAfectacion:@"1"];
        
        switch (numJuego.intValue) {
            case TipoJuegoGongs: {
                jvc = [[JuegoGongsViewController alloc] initWithPaciente:nil estadoEntrenamiento:estadoEntrenamiento];
                break;
            }
            case TipoJuegoInvasion:
                jvc = [[JuegoInvasionViewController alloc] initWithPaciente:nil estadoEntrenamiento:estadoEntrenamiento];
                break;
                
            case TipoJuegoPoker:
                jvc = [[JuegoPokerViewController alloc] initWithPaciente:nil estadoEntrenamiento:estadoEntrenamiento];
                break;
                
        }
    }    
    
    jvc.delegate = self;
    [self.navigationController pushViewController:jvc animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark JuegoViewController Delegate
- (void)juegoTerminado:(id)partida {
    if(self.modelo) {
        if(!self.diaEntrenamiento) {
            self.diaEntrenamiento = [DiaEntrenamiento object];
            self.diaEntrenamiento.entrenamiento = self.modelo.paciente.entrenamiento;
            self.diaEntrenamiento.fecha = [NSDate date];
        }
        
        if([partida isKindOfClass:[PartidaGongs class]]) {
            self.diaEntrenamiento.partidaGongs = partida;
        }
        if([partida isKindOfClass:[PartidaInvasion class]]) {
            self.diaEntrenamiento.partidaInvasion = partida;
        }
        if([partida isKindOfClass:[PartidaPoker class]]) {
            self.diaEntrenamiento.partidaPoker = partida;
        }
        
        self.indiceJuegoActual+=1;
        if(self.indiceJuegoActual<self.modelo.paciente.entrenamiento.estadoEntrenamiento.juegosDeHoy.count) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            int nivel = self.modelo.paciente.nivel.intValue + self.diaEntrenamiento.partidaGongs.cambioNivel.intValue + self.diaEntrenamiento.partidaInvasion.cambioNivel.intValue + self.diaEntrenamiento.partidaPoker.cambioNivel.intValue;
            nivel = nivel<0?0:nivel;
            
            int puntos = self.modelo.paciente.puntos.intValue + self.diaEntrenamiento.partidaGongs.puntos.intValue + self.diaEntrenamiento.partidaInvasion.puntos.intValue + self.diaEntrenamiento.partidaPoker.puntos.intValue;
            puntos = puntos<0?0:puntos;
            
            self.modelo.paciente.nivel = [NSNumber numberWithInt:nivel];
            self.modelo.paciente.puntos = [NSNumber numberWithInt:puntos];
            self.modelo.paciente.ultimoEntrenamiento = [NSDate date];
            
            self.modelo.paciente.siguienteEntrenamiento = [self.diaEntrenamiento.entrenamiento fechaEntrenamientoDespuesDe:[NSDate date]];
            
            [self guardaEntrenamiento];
        }
    } else {
        self.indiceJuegoActual+=1;
        if(self.indiceJuegoActual<3) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void) guardaEntrenamiento
{
    [EntrenamientosManager guardaDiaDeEntrenamiento:self.diaEntrenamiento dePaciente:self.modelo.paciente completado:^(NSError * error) {
        if(error) {
            UIBAlertView* alertView = [[UIBAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Reintentar", nil];
            [alertView showWithDismissHandler:^(NSInteger selectedIndex, BOOL didCancel) {
                if(didCancel) {
                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                } else {
                    [self guardaEntrenamiento];
                }
            }];
        } else {
            [self.modelo refresca];
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }];

}

@end
