//
//  PanelParametrosProtocol.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PanelLayerDelegate <NSObject>
- (void) panelFinalizado:(id) parametrosLayer;
@end
