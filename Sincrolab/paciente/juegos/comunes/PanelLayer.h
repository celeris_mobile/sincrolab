//
//  PanelLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "BotonParametros.h"
#import "PanelLayerDelegate.h"

@interface PanelLayer : CCLayer
@property (nonatomic, weak) id<PanelLayerDelegate> delegate;
@property (nonatomic, strong) BotonParametros* btn;
- (void) muestra;
@end
