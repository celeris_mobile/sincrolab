//
//  PanelLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelLayer.h"

@implementation PanelLayer

- (id) init {
    self = [super init];
    if(self) {
        [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self
                                                                priority:0
                                                         swallowsTouches:NO];
        
        self.visible = NO;        
    }
    
    return self;
}

- (void)muestra {
    self.position = CGPointMake(1024, 0);
    self.visible = YES;
    CCMoveTo* move = [CCMoveTo actionWithDuration:0.3 position:CGPointZero];
    CCEaseExponentialOut* elastic = [CCEaseExponentialOut actionWithAction:move];
    [self runAction:elastic];
}


- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint location = [self.btn convertTouchToNodeSpace: touch];
    
    BOOL isTouchHandled = CGRectContainsPoint(self.btn.boundingBox, location);
    if (isTouchHandled) {
        [self.btn presionado];
    }
    return isTouchHandled;
}

- (void)ccTouchEnded:(UITouch*)touch withEvent:(UIEvent *)event {
    [self.btn soltado];
    [self oculta];
}


- (void)oculta {
    CCMoveTo* move = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(-1024, 0)];
    CCEaseExponentialIn* ease = [CCEaseExponentialIn actionWithAction:move];
    
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        [self.delegate panelFinalizado:self];
    }];
    
    CCSequence* secuencia = [CCSequence actions:ease, callBlock, nil];
    
    [self runAction:secuencia];
}

@end
