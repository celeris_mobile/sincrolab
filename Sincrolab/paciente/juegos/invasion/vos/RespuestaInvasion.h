//
//  RespuestaInvasion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 26/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TipoRespuestaInvasion) {
    TipoRespuestaInvasionAcierto,
    TipoRespuestaInvasionOmision,
    TipoRespuestaInvasionComision,
    TipoRespuestaInvasionNoComision,
};

@interface RespuestaInvasion : NSObject
@property (nonatomic, assign) float tiempo;
@property (nonatomic, assign) TipoRespuestaInvasion tipo;
@property (nonatomic, assign) BOOL tieneStopSign;
@end
