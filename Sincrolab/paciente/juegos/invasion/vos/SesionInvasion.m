//
//  SesionInvasion.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SesionInvasion.h"





@implementation SesionInvasion

+ (float) floatConPropiedad:(NSString *) propiedad {
    propiedad = [propiedad stringByReplacingOccurrencesOfString:@"," withString:@"."];
    NSArray* components = [propiedad componentsSeparatedByString:@":"];
    if(components.count==1)
        return ((NSString *)[components objectAtIndex:0]).floatValue;
    else {
        float v1 = ((NSString *)[components objectAtIndex:0]).floatValue;
        float v2 = ((NSString *)[components objectAtIndex:1]).floatValue;
        float aleatorio = (float)rand() / RAND_MAX;
        return (v2-v1)*aleatorio + v1;
        
        
    }
}

+ (int) intConPropiedad:(NSString *) propiedad {
    NSArray* components = [propiedad componentsSeparatedByString:@":"];
    if(components.count==1)
        return ((NSString *)[components objectAtIndex:0]).intValue;
    else {
        float v1 = ((NSString *)[components objectAtIndex:0]).intValue;
        float v2 = ((NSString *)[components objectAtIndex:1]).intValue;

        float aleatorio = (float)rand() / RAND_MAX;
        return (int)roundf((v2-v1)*aleatorio + v1);
    }
}

+ (SesionInvasion *)sesionNum:(int)numSesion conEstado:(EstadoJuegoInvasion *)estado {
    SesionInvasion *sesion = [[SesionInvasion alloc] init];
    sesion.numSesion = numSesion;
    sesion.trials = [self intConPropiedad:estado.trials];
    sesion.tee1 = [self floatConPropiedad:estado.tee1];
    sesion.tee2 = [self floatConPropiedad:estado.tee2];
    sesion.tie = [self floatConPropiedad:estado.tie];
    sesion.dss = [self floatConPropiedad:estado.dss];
    sesion.n_cambios = [self intConPropiedad:estado.n_cambios];
    sesion.p_aparicion_ng = [self intConPropiedad:estado.p_aparicion_ng];
    sesion.tipo_ss = [self intConPropiedad:estado.tipo_ss];
    sesion.p_aparicion_ss = [self intConPropiedad:estado.p_aparicion_ss];

    switch(estado.cambio.intValue) {
        case 0:
            sesion.modalidad = EstadoInvasionModalidadNormal;
            break;
        case 1:
            sesion.modalidad = EstadoInvasionModalidadAleatoria;
            break;
        case 2:
            
            //modificado el 25 de Agosto David Hevilla
            //calculamos las sesiones pendientes restando el nº sesiones menos la sesion actual + 1

            
           
            
//            int sesionesPendientes = estado.sesiones-[numSesion-1];
            
            
            
            //calculamos los cambios pendientes restando el nº cambios menos los cambios realizados
            
           //NSInteger *cambiosPendientes = sesion.n_cambios - int _n_cambios_realizados;
            
            
//                esCambio = 2;
//            
//            
//            if (cambiosPendientes  >= sesionesPendientes && numSesion != 1) {
//                
//                //obligamos a que haya cambio
//                int esCambio = 1;
//                
//                if (modalidadAntigua == EstadoInvasionModalidadInvetida) {
//                    
//                }
//                sesion.modalidad = EstadoInvasionModalidadNormal;
//                
//                
//                else
//                    sesion.modalidad = EstadoInvasionModalidadInvetida;
//                
//            }else if (cambiosPendientes <= 0 && numSesion != 1);
//            
//                
//                //obligamos a que no hay cambio
//                int esCambio = 0
//            
//            if (modalidadAntigua == EstadoInvasionModalidadInvetida) {
            
            
            
//                
//                
//                
//            }
//            sesion.modalidad = EstadoInvasionModalidadInvetida;
//            
//            
//            else
//                sesion.modalidad = EstadoInvasionModalidadNormal;
//
//            
//            }
        //    if (int esCambio = 2) {
            
//            float aleatorio = (float)rand() / RAND_MAX;
//            NSLog(@"aleatorio %f", aleatorio);
//            
//            
//            if(aleatorio<0.5)
//                
//                
//                sesion.modalidad = EstadoInvasionModalidadNormal;
//            
//            
//            else
//                sesion.modalidad = EstadoInvasionModalidadInvetida;
//            
//            
//            
//
//            }
//
            
            //modalidadAntigua = sesion.modalidad;
           // break;

        case 3: {
            float aleatorio = (float)rand() / RAND_MAX;
            NSLog(@"aleatorio %f", aleatorio);
            
            
            if(aleatorio<0.5)
                
                
                sesion.modalidad = EstadoInvasionModalidadNormal;
            
            
            else
                sesion.modalidad = EstadoInvasionModalidadInvetida;
            
            
            break;
        }
    }
    return sesion;
}

- (NSInteger)tiempoTotal {
    return self.tie*self.trials;
}

@end
