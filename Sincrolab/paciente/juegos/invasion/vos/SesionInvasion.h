//
//  SesionInvasion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EstadoJuegoInvasion.h"

typedef NS_ENUM(NSInteger, EstadoInvasionModalidad) {
    EstadoInvasionModalidadNormal,
    EstadoInvasionModalidadInvetida,
    EstadoInvasionModalidadAleatoria,
    EstadoInvasionModalidadNueva
};


@interface SesionInvasion : NSObject

+ (SesionInvasion *) sesionNum:(int) numSesion conEstado:(EstadoJuegoInvasion *) estado;
@property (nonatomic, assign) NSInteger numSesion;
@property (nonatomic, assign) NSInteger trials;
@property (nonatomic, assign) double tee1;
@property (nonatomic, assign) double tee2;
@property (nonatomic, assign) double tie;
@property (nonatomic, assign) double dss;
@property (nonatomic, assign) NSInteger n_cambios;
@property (nonatomic, assign) NSInteger p_aparicion_ng;
@property (nonatomic, assign) NSInteger p_aparicion_ss;
@property (nonatomic, assign) NSInteger tipo_ss;
@property (nonatomic, assign) EstadoInvasionModalidad modalidad;

//Creamos la última modalidad
//Añadido 25 Agosto David hevilla


//esta es la última modalidad ejecutada
@property(nonatomic, assign) EstadoInvasionModalidad *modalidadAntigua;

//contador de los cambios que he realizado
@property (nonatomic, assign) NSInteger n_cambios_realizados;

//tres valores posibles:0 no cambio,1 cambio y 2 aleatorio
@property (nonatomic, assign) NSInteger esCambio;

//numero de sesiones pendientes = sesiones-numSesiones
@property (nonatomic, assign) NSInteger sesionesPendientes;
//numero cambios pendientes =
@property (nonatomic, assign) NSInteger cambiosPendientes;










@property (nonatomic, readonly) NSInteger tiempoTotal;

@end
