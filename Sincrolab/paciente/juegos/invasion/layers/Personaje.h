//
//  Personaje.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"

typedef NS_ENUM(NSInteger, PersonajeTipo) {
    PersonajeTipoSamurai,
    PersonajeTipoVillano
};

@interface Personaje : CCNode
@property (nonatomic, assign) int numTrial;
@property (nonatomic, assign) BOOL activo;

- (id) initWithNumero:(int) numero;
- (void) muestraPersonaje:(PersonajeTipo)tipo conDuracion:(float) duracion;
- (void) dado;

@end
