//
//  Personaje.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Personaje.h"
#import "Humo.h"

static CGPoint locations_samurai[8] =
{
    (CGPoint){726, 768-162},
    (CGPoint){830, 768-496},
    (CGPoint){690, 768-436},
    (CGPoint){160, 768-165},
    (CGPoint){62, 768-496},
    (CGPoint){206, 768-436},
    (CGPoint){436, 768-444}
};

static CGPoint locations_villano[8] =
{
    (CGPoint){742, 768-175},
    (CGPoint){818, 768-473},
    (CGPoint){684, 768-467},
    (CGPoint){116, 768-177},
    (CGPoint){69, 768-474},
    (CGPoint){195, 768-467},
    (CGPoint){391, 768-473},
};

@interface Personaje()
@property (nonatomic, assign) PersonajeTipo tipo;
@property (nonatomic, assign) int numero;
@property (nonatomic, strong) NSArray* pieles;
@property (nonatomic, strong) CCSprite* spriteSeleccionado;
@property (nonatomic, strong) Humo* humo;
@end

@implementation Personaje

- (id) initWithNumero:(int) numero {
    
    self = [super init];
    if(self) {
        
        NSMutableArray* auxPieles = [[NSMutableArray alloc] init];
        for (int p = 0; p<2; p++) {
            NSString* nombre;
            CGPoint posicion;
            switch (p) {
                case 0:
                    nombre = [NSString stringWithFormat:@"samurai%d.png", numero];
                    posicion = locations_samurai[numero-1];
                    break;
                case 1:
                    nombre = [NSString stringWithFormat:@"villano%d.png", numero];
                    posicion = locations_villano[numero-1];
                    break;
            }
            
            CCSprite * sprite = [CCSprite spriteWithFile:nombre];
            sprite.anchorPoint = CGPointMake(0, 1);
            sprite.position = posicion;
            sprite.opacity = 0;
            [self addChild:sprite];
            [auxPieles addObject:sprite];
        }
        
        self.pieles = auxPieles.copy;

        
        self.humo = [[Humo alloc] init];
        self.humo.anchorPoint = CGPointMake(0.5, 0.5);
        [self addChild:self.humo];
        

        
        self.visible = NO;
    }
    
    return self;
}

- (void) muestraPersonaje:(PersonajeTipo)tipo conDuracion:(float) duracion {
    _activo = YES;
    
    switch (tipo) {
        case PersonajeTipoSamurai:
            self.spriteSeleccionado = [self.pieles objectAtIndex:0];
            break;
        case PersonajeTipoVillano:
            self.spriteSeleccionado = [self.pieles objectAtIndex:1];
            break;
    }
    
    self.humo.position = CGPointMake(self.spriteSeleccionado.position.x+self.spriteSeleccionado.texture.contentSize.width/2, self.spriteSeleccionado.position.y-self.spriteSeleccionado.texture.contentSize.height/2+50);
    [self.humo lanza];

    [self.spriteSeleccionado stopAllActions];
    self.spriteSeleccionado.opacity = 0.0;
    self.visible = YES;

    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.15];
    CCDelayTime* delay = [CCDelayTime actionWithDuration:duracion-0.3];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.15];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        self.visible = NO;
        _activo = NO;
    }];
    
    CCSequence* secuencia = [CCSequence actions:fadeIn, delay, fadeOut, callBlock, nil];
    [self.spriteSeleccionado runAction:secuencia];

}

- (void)dado {
    _activo = NO;
    
    [self.spriteSeleccionado stopAllActions];

    
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.3];
    CCBlink* blink = [CCBlink actionWithDuration:0.3 blinks:3];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        self.visible = NO;
    }];

    CCSequence* secuencia = [CCSequence actions:fadeOut, callBlock, nil];    

    [self.spriteSeleccionado runAction:secuencia];
    [self.spriteSeleccionado runAction:blink];

}

- (CGRect)boundingBox {
    return self.spriteSeleccionado.boundingBox;
}

@end
