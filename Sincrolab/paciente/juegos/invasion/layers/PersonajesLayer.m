//
//  PersonajesLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PersonajesLayer.h"
#import "Personaje.h"

@interface PersonajesLayer()
@property (nonatomic, strong) NSArray* personajes;
@end


@implementation PersonajesLayer

- (id)init {
    self = [super init];
    if(self) {

        [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self
                                                                priority:0
                                                         swallowsTouches:NO];

    }
    
    return self;
}

- (void)setSesion:(SesionInvasion *)sesion {
    _sesion = sesion;
    
    if(self.personajes) {
        for (Personaje* personaje in self.personajes) {
            [personaje removeFromParent];
        }
        self.personajes = nil;
    }
    
    NSMutableArray* auxpersonajes = [[NSMutableArray alloc] init];
    for (int s = 1; s<=7; s++) {
        Personaje* personaje = [[Personaje alloc] initWithNumero:s];
        [self addChild:personaje];
        [auxpersonajes addObject:personaje];
    }
    
    self.personajes = auxpersonajes.copy;
    
}



- (void) lanzaEstimulo:(int) numTrial conDiana:(NSNumber *) diana invertido:(BOOL) invertido {
    int aleatorio = arc4random()%self.personajes.count;
    Personaje* personaje = [self.personajes objectAtIndex:aleatorio];
    while (personaje.visible) {
        aleatorio = arc4random()%self.personajes.count;
        personaje = [self.personajes objectAtIndex:aleatorio];
    }
    
    
    personaje.numTrial = numTrial;
    
    if(!invertido) {
        if(diana.boolValue)
            [personaje muestraPersonaje:PersonajeTipoVillano conDuracion:self.sesion.tee2];
        else
            [personaje muestraPersonaje:PersonajeTipoSamurai conDuracion:self.sesion.tee1];
    } else {
        if(diana.boolValue)
            [personaje muestraPersonaje:PersonajeTipoSamurai conDuracion:self.sesion.tee1];
        else
            [personaje muestraPersonaje:PersonajeTipoVillano conDuracion:self.sesion.tee2];

    }

}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    if(!self.visible)
        return NO;
    
    CGPoint location = [self convertTouchToNodeSpace: touch];
    
    BOOL isTouchHandled = NO;
    for (Personaje* personaje in self.personajes) {
        isTouchHandled = CGRectContainsPoint(personaje.boundingBox, location) && personaje.activo;
        if (isTouchHandled) {
            [personaje dado];
            [self.delegate dadoPersonajeEnTrial:personaje.numTrial];
            
        }
    }
    
    
    return isTouchHandled;
}

@end
