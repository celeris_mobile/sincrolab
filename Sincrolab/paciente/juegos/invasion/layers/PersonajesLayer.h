//
//  PersonajesLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CCLayer.h"
#import "SesionInvasion.h"

@protocol PersonajesLayerDelegate <NSObject>
- (void) dadoPersonajeEnTrial:(NSInteger) numTrial;
@end

@interface PersonajesLayer : CCLayer
@property (nonatomic, weak) id<PersonajesLayerDelegate> delegate;
@property (nonatomic, strong) SesionInvasion* sesion;
- (void) lanzaEstimulo:(int) numTrial conDiana:(NSNumber *) diana invertido:(BOOL) invertido;
@end
