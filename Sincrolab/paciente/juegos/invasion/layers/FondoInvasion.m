//
//  FondoInvasion.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "FondoInvasion.h"

@interface FondoInvasion()
@property (nonatomic, strong) CCSprite* fondo;
@end

@implementation FondoInvasion

- (id)init {
    self = [super init];
    if(self) {
        self.fondo = [CCSprite spriteWithFile:@"fondo_invasion.jpg"];
        self.fondo.anchorPoint = CGPointZero;
        [self addChild:self.fondo];
    }
    
    return self;
}

@end
