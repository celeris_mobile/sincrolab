//
//  StopsLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionInvasion.h"

@interface StopsLayer : CCLayer
@property (nonatomic, strong) SesionInvasion* sesion;
- (void) lanzaStopConDuracion:(float) duracion;
@end
