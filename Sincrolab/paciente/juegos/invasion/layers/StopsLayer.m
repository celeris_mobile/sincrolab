//
//  StopsLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "StopsLayer.h"
#import "SimpleAudioEngine.h"

@interface StopsLayer()
@property (nonatomic, strong) CCSprite* stopVisual;
@end

@implementation StopsLayer

- (id) init {
    self = [super init];
    if(self) {        
        self.stopVisual = [CCSprite spriteWithFile:@"stop.png"];
        self.stopVisual.position = CGPointMake(1024-80, 768-80);
        self.stopVisual.visible = NO;
        [self addChild:self.stopVisual];
    }
    
    return self;
}

- (void)lanzaStopConDuracion:(float)duracion {
    if(self.sesion.tipo_ss==1) {
        ALuint efecto = [[SimpleAudioEngine sharedEngine] playEffect:@"alarma.mp3"];
        NSNumber* numEfecto = [NSNumber numberWithInt:efecto];
        [self performSelector:@selector(paraEfecto:) withObject:numEfecto afterDelay:duracion];
    }
    if(self.sesion.tipo_ss==2) {
        self.stopVisual.visible = YES;
        [self performSelector:@selector(paraEfecto:) withObject:nil afterDelay:duracion];
    }
}

- (void) paraEfecto:(NSNumber *) numEfecto {
    if(self.sesion.tipo_ss==1) {
        [[SimpleAudioEngine sharedEngine] stopEffect:numEfecto.intValue];
    } else {
        self.stopVisual.visible = NO;
    }
}

@end
