//
//  PajaroRojo.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Humo.h"

@interface Humo()
@property (nonatomic, strong) CCSprite *sprite;
@property (nonatomic, strong) CCFiniteTimeAction *animacion;
@property (nonatomic, strong) CCSpriteBatchNode *spriteSheet;
@end

NSArray* animFrames;

@implementation Humo

- (id) init {
    self = [super init];
    if(self) {
        CCSpriteFrameCache* spriteFrameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [spriteFrameCache addSpriteFramesWithFile:@"humo.plist"];
        
        self.spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"humo.png"];
        [self addChild:self.spriteSheet];
        
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        for (int i=1; i<=26; i++) {
            [walkAnimFrames addObject:
             [spriteFrameCache spriteFrameByName:[NSString stringWithFormat:@"humo%04d.png",i]]];
        }
        
        CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:1/45.0];

        self.sprite = [CCSprite spriteWithSpriteFrameName:@"humo0001.png"];
        self.animacion = [CCAnimate actionWithAnimation:walkAnim];

        
    }
    return self;
}

- (void) lanza {
    
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        [self.sprite removeFromParent];
        [self.sprite stopAllActions];
    }];
    
    CCSequence* secuencia = [CCSequence actions:self.animacion, callBlock, nil];

    [self.spriteSheet addChild:self.sprite];
    //    [self.sprite runAction:secuencia];
    [self.sprite runAction:secuencia];
}


@end
