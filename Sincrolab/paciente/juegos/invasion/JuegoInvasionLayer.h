//
//  JuegoInvasionLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright Ricardo Sánchez Sotres 2013. All rights reserved.
//


#import "cocos2d.h"
#import "EstadoJuegoInvasion.h"
#import "PanelParametrosLayer.h"
#import "PersonajesLayer.h"
#import "Paciente.h"
#import "EstadoEntrenamiento.h"

@interface JuegoInvasionLayer : CCLayer <PersonajesLayerDelegate, PanelLayerDelegate>
- (id) initWithPaciente:(Paciente *) paciente estado:(EstadoEntrenamiento *) estadoEntrenamiento;
- (void) finalizaSesion;
- (void) finalizaPartida;
@end
