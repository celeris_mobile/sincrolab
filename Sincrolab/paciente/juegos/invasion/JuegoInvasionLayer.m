//
//  JuegoInvasionLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright Ricardo Sánchez Sotres 2013. All rights reserved.
//

#import "JuegoInvasionLayer.h"

#import "DateUtils.h"

#import "EstadoEntrenamiento.h"

#import "SesionInvasion.h"
#import "PartidaInvasion.h"
#import "RespuestaInvasion.h"
#import "ResultadoSesionInvasion.h"

#import "FondoInvasion.h"
#import "TiempoLayer.h"
#import "PersonajesLayer.h"
#import "StopsLayer.h"

#import "ConfiguracionSesionInvasionLayer.h"
#import "ResultadoSesionInvasionLayer.h"
#import "ResultadoPartidaInvasionLayer.h"

@interface JuegoInvasionLayer()
@property (nonatomic, strong) Paciente* paciente;
@property (nonatomic, strong) EstadoEntrenamiento* estadoEntrenamiento;
@property (nonatomic, strong) EstadoJuegoInvasion* estado;


@property (nonatomic, strong) SesionInvasion* sesion;

@property (nonatomic, strong) NSArray* trials;
@property (nonatomic, strong) NSArray* stops;
@property (nonatomic, strong) NSArray* cambios;
@property (nonatomic, strong) NSMutableArray* respuestas;
@property (nonatomic, strong) NSMutableArray* resultadoSesiones;

@property (nonatomic, strong) PartidaInvasion* partida;

@property (nonatomic, strong) FondoInvasion* fondo;
@property (nonatomic, strong) TiempoLayer* tiempo;
@property (nonatomic, strong) PersonajesLayer* personajes;
@property (nonatomic, strong) StopsLayer* stopsLayer;

@property (nonatomic, strong) CCSprite* errorSprite;
@property (nonatomic, strong) CCSprite* aciertoSprite;
@property (nonatomic, strong) CCSprite* marcadorVillano;
@property (nonatomic, strong) CCSprite* marcadorSamurai;

@property (nonatomic, strong) ConfiguracionSesionInvasionLayer* panelSesion;
@property (nonatomic, strong) ResultadoSesionInvasionLayer* panelResultadoSesion;
@property (nonatomic, strong) ResultadoPartidaInvasionLayer* panelResultadoPartida;


@end

@implementation JuegoInvasionLayer {
    float secs_transcurridos;
    float t_estimulo;
    int num_trial;
    
    BOOL invertido;
}

- (id) initWithPaciente:(Paciente *) paciente estado:(EstadoEntrenamiento *) estadoEntrenamiento {
    self = [super init];
    
	if(self) {
        
        self.estadoEntrenamiento = estadoEntrenamiento;
        self.estado = self.estadoEntrenamiento.estadoInvasion;
        self.paciente = paciente;
        
        self.fondo = [[FondoInvasion alloc] init];
        self.fondo.anchorPoint = CGPointMake(0, 0);
        [self addChild:self.fondo];
        
        self.errorSprite = [CCSprite spriteWithFile:@"error.png"];
        self.errorSprite.position = CGPointMake(1024-80, 80);
        self.errorSprite.opacity = 0.0;
        [self addChild:self.errorSprite];
        
        self.aciertoSprite = [CCSprite spriteWithFile:@"check.png"];
        self.aciertoSprite.position = CGPointMake(1024-80, 80);
        self.aciertoSprite.opacity = 0.0;
        [self addChild:self.aciertoSprite];

        self.marcadorVillano = [CCSprite spriteWithFile:@"marcador_villano.png"];
        self.marcadorVillano.position = CGPointMake(100, 768-100);
        self.marcadorVillano.opacity = 0.0;
        [self addChild:self.marcadorVillano];

        self.marcadorSamurai = [CCSprite spriteWithFile:@"marcador_samurai.png"];
        self.marcadorSamurai.position = CGPointMake(100, 768-100);
        self.marcadorSamurai.opacity = 0.0;
        [self addChild:self.marcadorSamurai];
        
        self.tiempo = [[TiempoLayer alloc] init];
        [self addChild:self.tiempo];
        
        self.personajes = [[PersonajesLayer alloc] init];
        self.personajes.delegate = self;
        [self addChild:self.personajes];
        
        self.stopsLayer = [[StopsLayer alloc] init];
        [self addChild:self.stopsLayer];
        
        self.resultadoSesiones = [[NSMutableArray alloc] init];
        
	}
    
	return self;
}


- (void)onEnterTransitionDidFinish {
    self.sesion = nil;
    [self performSelector:@selector(configuraSesion) withObject:nil afterDelay:0.5];
}

- (void) configuraSesion {
    secs_transcurridos = 0.0;
    num_trial = 0;
    self.respuestas = [[NSMutableArray alloc] init];
    
    int numSesion = 1;
    int modalidad = 0;
    if(self.sesion) {
        numSesion = (int)self.sesion.numSesion + 1;
        if(self.estado.cambio.intValue==3)//El cambo es por partida
            modalidad = self.sesion.modalidad;
    }
    
    self.sesion = [SesionInvasion sesionNum:numSesion conEstado:self.estado];
    if(modalidad!=0)
        self.sesion.modalidad = modalidad;
    
    if(self.sesion.modalidad == EstadoInvasionModalidadAleatoria) {
        float aleatorio = (float)rand() / RAND_MAX;
        invertido = aleatorio<0.5;
    }
    if(self.sesion.modalidad == EstadoInvasionModalidadInvetida) {
        invertido = YES;
    }
    
    if (self.sesion.modalidad == EstadoInvasionModalidadNueva) {
        
        NSLog(@"estamos en EstadoModalidadNueva");
        invertido = YES;
    }
    
    [self configuraTrials];
    
 //   if(numSesion==1 || self.estado.cambio.intValue==2) {
        self.panelSesion = [[ConfiguracionSesionInvasionLayer alloc] initWithSesion:self.sesion totalSesiones:self.estado.sesiones.intValue];
        self.panelSesion.delegate = self;
        [self addChild:self.panelSesion];
        [self muestraSesion];
    /*
    } else {
        [self iniciaSesion];
    }
     */
}

- (void) configuraTrials {
    float p_dianas;
    if(self.sesion.p_aparicion_ng>0.0)
        p_dianas = self.sesion.p_aparicion_ng/100.0;
    else
        p_dianas = 1.0;
    
    p_dianas = p_dianas>1.0?1.0:p_dianas;
    int num_dianas = self.sesion.trials*p_dianas;
    
    NSMutableSet* auxdianas = [[NSMutableSet alloc] init];
    for (int d = 0; d<num_dianas; d++) {
        BOOL encontrado = NO;
        while (!encontrado) {
            int aleatorio = arc4random()%self.sesion.trials;
            NSNumber* number = [NSNumber numberWithInt:aleatorio];
            if(![auxdianas containsObject:number]) {
                [auxdianas addObject:number];
                encontrado = YES;
            }
        }
    }
    
    NSMutableArray* auxtrials = [[NSMutableArray alloc] init];
    for (int t = 0; t<self.sesion.trials; t++) {
        NSNumber* numero = [NSNumber numberWithInt:t];
        if([auxdianas containsObject:numero]) {
            [auxtrials addObject:[NSNumber numberWithBool:YES]];
        } else
            [auxtrials addObject:[NSNumber numberWithBool:NO]];
    }
    
    self.trials = auxtrials.copy;
    
    if(self.sesion.p_aparicion_ss>0) {
        float p_stops = self.sesion.p_aparicion_ss/100.0;
        int num_stops = num_dianas*p_stops;
        
        NSMutableSet* auxNumStops = [[NSMutableSet alloc] init];
        for (int d = 0; d<num_stops; d++) {
            BOOL encontrado = NO;
            while (!encontrado) {
                int aleatorio = arc4random()%(num_dianas-1); //para que no salga stop en el último
                NSNumber* number = [NSNumber numberWithInt:aleatorio];
                if(![auxNumStops containsObject:number]) {
                    [auxNumStops addObject:number];
                    encontrado = YES;
                }
            }
        }
        
        NSMutableArray* auxstops = [[NSMutableArray alloc] init];
        int numDiana = 0;
        for (int t = 0; t<self.sesion.trials; t++) {
            NSNumber* numero = [NSNumber numberWithInt:numDiana];
            NSNumber* diana = [self.trials objectAtIndex:t];
            if(diana.boolValue) {
                if([auxNumStops containsObject:numero]) {
                    [auxstops addObject:[NSNumber numberWithBool:YES]];
                } else {
                    [auxstops addObject:[NSNumber numberWithBool:NO]];
                }
                numDiana+=1;
            } else {
                [auxstops addObject:[NSNumber numberWithBool:NO]];
            }
        }
        
        self.stops = auxstops.copy;
    }
    
    
    //Detectamos el error de los cambios
    
    if(self.sesion.n_cambios>0 && self.sesion.modalidad == EstadoInvasionModalidadAleatoria) {
        int num_cambios = self.sesion.n_cambios;
        
        int trialCounts = self.sesion.trials;
        
        NSLog(@"trialsCount devuelve:%d",trialCounts);
        

        
        
        //añadido 25 de Agosto David Hevilla
        if (num_cambios > trialCounts -1) {
            num_cambios = trialCounts -1;
            
            NSLog(@"num_cambios vale:%d", num_cambios);
        
        }
    
        
        
        //condicional para que si num_cambios > sesion.trials
        
        NSLog(@"El num_cambios devuelve:%d",num_cambios);
        
        
        NSMutableSet* auxNumCambios = [[NSMutableSet alloc] init];
        for (int d = 0; d<num_cambios; d++) {
            BOOL encontrado = NO;
            while (!encontrado) {
                int aleatorio = arc4random()%(self.sesion.trials-1)+1;
                
                NSLog(@"El aleatorio devuelve:%d",aleatorio);
                
                
                //para que no salga cambio en el primero
                NSNumber* number = [NSNumber numberWithInt:aleatorio];
                
                
                NSLog(@"El num_cambios devuelve:%d",num_cambios);
                if(![auxNumCambios containsObject:number]) {
                    [auxNumCambios addObject:number];
                    encontrado = YES;
                }
            }
        }
        
        
        
            
    
                  
                  
  
        
        NSMutableArray* auxcambios = [[NSMutableArray alloc] init];
        int numDiana = 0;
        for (int t = 0; t<self.sesion.trials; t++) {
            NSNumber* numero = [NSNumber numberWithInt:numDiana];
            
            if([auxNumCambios containsObject:numero]) {
                [auxcambios addObject:[NSNumber numberWithBool:YES]];
            } else {
                [auxcambios addObject:[NSNumber numberWithBool:NO]];
            }
            numDiana+=1;
        }
        
        self.cambios = auxcambios.copy;
    }
}

- (void) muestraSesion {
    self.personajes.sesion = self.sesion;
    self.stopsLayer.sesion = self.sesion;
    [self.tiempo setTiempoTotal:self.sesion.tiempoTotal+1.0];
    
    [self.tiempo muestra];
    
    [self.panelSesion muestra];
}

- (void) iniciaSesion {
    [self scheduleUpdate];
    [self scheduleOnce:@selector(lanzaPrimerEstimulo) delay:0.5];
    
    if(self.stops) {
        NSNumber* stop = [self.stops objectAtIndex:0];
        if(stop.boolValue) {
            [self performSelector:@selector(lanzaStop) withObject:nil afterDelay:0.5+self.sesion.dss];
        }
    }
    
    if(self.sesion.modalidad==EstadoInvasionModalidadAleatoria) {
        self.marcadorVillano.position = CGPointMake(100, 768-100);
        self.marcadorSamurai.position = CGPointMake(100, 768-100);
        
        if(invertido) {
            [self.marcadorSamurai runAction:[CCFadeIn actionWithDuration:0.15]];
            [self.marcadorVillano runAction:[CCFadeOut actionWithDuration:0.15]];
        } else {
            [self.marcadorVillano runAction:[CCFadeIn actionWithDuration:0.15]];
            [self.marcadorSamurai runAction:[CCFadeOut actionWithDuration:0.15]];
        }
    }
}

- (void) lanzaPrimerEstimulo {
    [self lanzaEstimulo];
    [self schedule:@selector(lanzaEstimulo) interval:self.sesion.tie];
}

- (void) lanzaEstimulo {
    
    if(num_trial>=self.sesion.trials)
        [self performSelector:@selector(finSesion) withObject:nil afterDelay:0.5];
    else {
        //Cuándo se lanzó este estímulo
        t_estimulo = secs_transcurridos;
        
        [self.respuestas addObject:[[RespuestaInvasion alloc] init]];
        
        if(self.cambios) {
            NSNumber* cambio = [self.cambios objectAtIndex:num_trial];
            if(cambio.boolValue) {
                invertido = !invertido;
                self.marcadorVillano.position = CGPointMake(100, 768-100);
                self.marcadorSamurai.position = CGPointMake(100, 768-100);
                
                if(invertido) {
                    [self.marcadorSamurai runAction:[CCFadeIn actionWithDuration:0.15]];
                    [self.marcadorVillano runAction:[CCMoveBy actionWithDuration:0.15 position:CGPointMake(0, -40)]];
                    [self.marcadorVillano runAction:[CCFadeOut actionWithDuration:0.15]];
                } else {
                    [self.marcadorVillano runAction:[CCFadeIn actionWithDuration:0.15]];
                    [self.marcadorSamurai runAction:[CCMoveBy actionWithDuration:0.15 position:CGPointMake(0, -40)]];
                    [self.marcadorSamurai runAction:[CCFadeOut actionWithDuration:0.15]];
                }
            }
        }
        
        [self.personajes lanzaEstimulo:num_trial conDiana:[self.trials objectAtIndex:num_trial] invertido:invertido];
        
        num_trial += 1;
        
        if(num_trial<self.trials.count-1 && self.stops) {
            NSNumber* stop = [self.stops objectAtIndex:num_trial];
            if(stop.boolValue) {
                [self performSelector:@selector(lanzaStop) withObject:nil afterDelay:self.sesion.tie+self.sesion.dss];
            }
        }
    }
}

- (void) lanzaStop {
    [self.stopsLayer lanzaStopConDuracion:self.sesion.tee2 - self.sesion.dss];
}

- (void)update:(ccTime)delta {
    secs_transcurridos += delta;
    [self.tiempo decrementa:delta];
}

- (void) finSesion {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    
    [self compruebaAciertos];
    
    ResultadoSesionInvasion* resultado = [ResultadoSesionInvasion resultadoConRespuestas:self.respuestas];
    resultado.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
    
    [self.resultadoSesiones addObject:resultado];
    
    self.panelResultadoSesion = [[ResultadoSesionInvasionLayer alloc] initWithResultado:resultado];
    self.panelResultadoSesion.delegate = self;
    [self addChild:self.panelResultadoSesion];
    [self.panelResultadoSesion muestra];
    
    
    [self.tiempo oculta];
}


- (void) compruebaAciertos {

    for (int t = 0; t<self.sesion.trials; t++) {

        RespuestaInvasion* respuesta = [self.respuestas objectAtIndex:t];

        NSNumber*diana = [self.trials objectAtIndex:t];
        
        NSNumber*stop = [NSNumber numberWithBool:NO];
        if(self.stops)
            stop = [self.stops objectAtIndex:t];
        
        respuesta.tieneStopSign = stop.boolValue;
        
        if(respuesta.tiempo == 0.0) {
            if(diana.boolValue && !stop.boolValue) {
                respuesta.tipo = TipoRespuestaInvasionOmision;
            } else {
                respuesta.tipo = TipoRespuestaInvasionNoComision;
            }
        } else {
            if(diana.boolValue && !stop.boolValue) {
                respuesta.tipo = TipoRespuestaInvasionAcierto;
            } else {
                respuesta.tipo = TipoRespuestaInvasionComision;
            }
        }
    }
}

- (void) finPartida {

    self.partida = [PartidaInvasion object];
    self.partida.sesiones = [[NSMutableArray alloc] init];
    
    for (ResultadoSesionInvasion* resultado in self.resultadoSesiones) {
        [self.partida.sesiones addObject:resultado.valores];
    }
    
    if(self.estadoEntrenamiento.tipo.intValue == TipoEntrenamientoAutomatico) {
        EstadoJuegoCambio cambioNivel = [self.estado compruebaCambioNivelConResultados:self.resultadoSesiones];
        switch (cambioNivel) {
            case EstadoJuegoCambioSubeNivel:
                self.partida.cambioNivel = [NSNumber numberWithInt:1];
                break;
            case EstadoJuegoCambioBajaNivel:
                self.partida.cambioNivel = [NSNumber numberWithInt:-1];
                break;
            case EstadoJuegoCambioSeMantiene:
                self.partida.cambioNivel = [NSNumber numberWithInt:0];
                break;
            case EstadoJuegoCambioFinal:
                //TODO Haz algo con esto
                [[[UIAlertView alloc] initWithTitle:@"¡Atención!" message:@"Entrenamiento finalizado" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                break;
        }
    } else {
        //TODO cuando es un entrenamiento manual subimos el nivel?
        self.partida.cambioNivel = [NSNumber numberWithInt:0];
    }


    
    self.panelResultadoPartida = [[ResultadoPartidaInvasionLayer alloc] initWithResultadoSesiones:self.resultadoSesiones estadoEntrenamiento:self.estadoEntrenamiento andCambioNivel:self.partida.cambioNivel];
    
    self.panelResultadoPartida.delegate = self;
    [self addChild:self.panelResultadoPartida];
    [self.panelResultadoPartida muestra];

}



- (void) muestraError {
    self.errorSprite.position = CGPointMake(1024-80, 40);
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.5];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 120)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction]];
    
    [self.errorSprite runAction:secuencia];
}

- (void) muestraAcierto {
    self.aciertoSprite.position = CGPointMake(1024-80, 40);
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.5];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 120)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction]];
    
    [self.aciertoSprite runAction:secuencia];
}

#pragma mark PersonajesLayer Delegate
- (void) dadoPersonajeEnTrial:(NSInteger)numTrial {
    RespuestaInvasion* respuesta = [self.respuestas objectAtIndex:numTrial];
    respuesta.tiempo = secs_transcurridos-t_estimulo;
    
    NSNumber* diana = [self.trials objectAtIndex:numTrial];
    NSNumber* stop = [NSNumber numberWithBool:NO];
    if(self.stops)
        stop = [self.stops objectAtIndex:numTrial];
    
    if(diana.boolValue&&!stop.boolValue)
        [self muestraAcierto];
    else
        [self muestraError];
    
    
}


#pragma mark PanelDelegate
- (void)panelFinalizado:(PanelLayer *) panelLayer {

    if(panelLayer==self.panelSesion) {
        [self.panelSesion removeFromParent];
        self.panelSesion = nil;
        [self iniciaSesion];
    }

    if(panelLayer==self.panelResultadoSesion) {
        [self.panelResultadoSesion removeFromParent];
        self.panelResultadoSesion = nil;
        
        if(self.sesion.numSesion<self.estado.sesiones.intValue)
            [self configuraSesion];
        else
            [self finPartida];
    }

    if(panelLayer==self.panelResultadoPartida) {
        NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.partida, @"partida", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificacionPartidaTerminada" object:self userInfo:userInfo];
    }

}


#pragma mark Para Debug
- (void) finalizaSesion {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    

    ResultadoSesionInvasion* resultado = [ResultadoSesionInvasion resultadoAleatorio];
    resultado.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
    
    [self.resultadoSesiones addObject:resultado];
    
    self.panelResultadoSesion = [[ResultadoSesionInvasionLayer alloc] initWithResultado:resultado];
    self.panelResultadoSesion.delegate = self;
    [self addChild:self.panelResultadoSesion];
    [self.panelResultadoSesion muestra];
    

    [self.tiempo oculta];
}

- (void) finalizaPartida {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    

    for(int s = self.resultadoSesiones.count; s<self.estado.sesiones.intValue; s++) {
        ResultadoSesionInvasion* resultado = [ResultadoSesionInvasion resultadoAleatorio];
        resultado.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
        
        [self.resultadoSesiones addObject:resultado];
    }

    [self finPartida];
    
    [self.tiempo oculta];
}


@end

