//
//  ConfiguracionSesionInvasionLayer.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguracionSesionInvasionLayer.h"
#import "DateUtils.h"
#import "ImageUtils.h"
#import "UIColor+Extensions.h"

typedef NS_ENUM(NSInteger, TipoPartidaInvasion) {
    TipoPartidaInvasionGoNoGo,
    TipoPartidaInvasionReto,
    TipoPartidaInvasionStop,
    TipoPartidaInvasionStopDistractores,
    TipoPartidaInvasionStopAudio,
    TipoPartidaInvasionStopAudioDistractores,
    TipoPartidaInvasionSalva,
    TipoPartidaInvasionAleatorio,
};

@interface ConfiguracionSesionInvasionLayer()
@property (nonatomic, strong) CCSprite* fondo;
@property (nonatomic, strong) SesionInvasion* sesion;
@end

@implementation ConfiguracionSesionInvasionLayer {
    NSInteger totalSesiones;
}

- (id) initWithSesion:(SesionInvasion *)sesion totalSesiones:(NSInteger) numSesiones {
    self = [super init];
    
    if(self) {
        self.sesion = sesion;
        totalSesiones = numSesiones;
        
        
        //capturamos los valores de configuración en manual
        //david hevilla
        
        
        NSLog(@"texto de texp:%f",sesion.tee1);
        NSLog(@"texto de tie:%f",sesion.tee2);
        NSLog(@"texto de sesion actual:%ld",(long)sesion.tie);
        
        TipoPartidaInvasion tipo;
        if(sesion.tee1 && sesion.tee2 && !sesion.dss && sesion.modalidad == EstadoInvasionModalidadNormal)
            tipo = TipoPartidaInvasionGoNoGo;
        if(sesion.tee1 && sesion.tee2 && sesion.tie<sesion.tee1 && sesion.tie<sesion.tee2 && sesion.modalidad == EstadoInvasionModalidadNormal)
            tipo = TipoPartidaInvasionReto;
        if(!sesion.tee1 && sesion.tee2 && sesion.dss && sesion.modalidad == EstadoInvasionModalidadNormal) {
            if(sesion.tipo_ss==1)
                tipo = TipoPartidaInvasionStopAudio;
            if(sesion.tipo_ss==2)
                tipo = TipoPartidaInvasionStop;
        }
        if(sesion.tee1 && sesion.tee2 && sesion.dss && sesion.modalidad == EstadoInvasionModalidadNormal){
            if(sesion.tipo_ss==1)
                tipo = TipoPartidaInvasionStopAudioDistractores;
            if(sesion.tipo_ss==2)
                tipo = TipoPartidaInvasionStopDistractores;
        }

        if(sesion.tee1 && sesion.tee2 && sesion.modalidad == EstadoInvasionModalidadInvetida)
            tipo = TipoPartidaInvasionSalva;
        if(sesion.tee1 && sesion.tee2 && sesion.modalidad == EstadoInvasionModalidadAleatoria)
            tipo = TipoPartidaInvasionAleatorio;        
        
        NSString* imageKey = [NSString stringWithFormat:@"fondo%d%d", tipo, self.sesion.numSesion];
        
        CCTexture2D* texturaFondo = [[CCTextureCache sharedTextureCache] textureForKey:imageKey];
        if(texturaFondo) {
            self.fondo = [CCSprite spriteWithTexture:texturaFondo];
        } else {
            self.fondo = [CCSprite spriteWithCGImage:[self generaTexturaFondo:tipo].CGImage key:imageKey];
        }

        self.fondo.position = CGPointMake(1024/2, 768/2);
        [self addChild:self.fondo];

        NSString* titulostr;
        NSString* marcador;
        NSString* marcador2;
        switch (tipo) {
            case TipoPartidaInvasionGoNoGo:
            case TipoPartidaInvasionReto:
            case TipoPartidaInvasionStop:
            case TipoPartidaInvasionStopDistractores:
            case TipoPartidaInvasionStopAudio:
            case TipoPartidaInvasionStopAudioDistractores:
                titulostr = @"Caza al Villano";
                marcador = @"marcador_villano.png";
                break;
            case TipoPartidaInvasionSalva:
                titulostr = @"Caza al Samurai";
                marcador = @"marcador_samurai.png";
                break;
            case TipoPartidaInvasionAleatorio:
                titulostr = @"Samurais contra Villanos";
                marcador = @"marcador_villano.png";
                marcador2 = @"marcador_samurai.png";
                break;
                
                
                
                
        }
        
        CCSprite* retrato = [CCSprite spriteWithCGImage:[UIImage imageNamed:marcador].CGImage key:marcador];
        if(!marcador2) {
            retrato.position = CGPointMake(1024/2, self.fondo.position.y+self.fondo.contentSize.height/2);
            [self addChild:retrato];
        } else {
            CCSprite* retrato2 = [CCSprite spriteWithCGImage:[UIImage imageNamed:marcador2].CGImage key:marcador2];
            retrato2.scale = 0.8;
            retrato2.position = CGPointMake(1024/2-70, self.fondo.position.y+self.fondo.contentSize.height/2);
            [self addChild:retrato2];
            
            retrato.scale = 0.8;
            retrato.position = CGPointMake(1024/2+70, self.fondo.position.y+self.fondo.contentSize.height/2);
            [self addChild:retrato];
        }
        
        CCLabelTTF* titulo = [CCLabelTTF labelWithString:titulostr fontName:@"ChauPhilomeneOne-Regular" fontSize:44];
        titulo.color = ccc3(255.0, 255.0, 255.0);
        titulo.position = CGPointMake(1024/2, retrato.position.y - retrato.contentSize.height/2 - 35);
        [self addChild:titulo];

        if(tipo==TipoPartidaInvasionStop||tipo==TipoPartidaInvasionStopDistractores) {
            CCSprite* sstop = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"stop.png"].CGImage key:@"stopSign"];
            sstop.position = CGPointMake(1024-80, 768-80);
            [self addChild:sstop];
            
            CCSprite* flecha = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"flecha.png"].CGImage key:@"flecha"];
            flecha.scaleX = -1;
            flecha.scaleY = -1;
            flecha.position = CGPointMake(1024-80-100, 768-80-100);
            [self addChild:flecha];
        }
        
        if(tipo==TipoPartidaInvasionAleatorio) {
            CCSprite* marcador = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"marcador_anonimo.png"].CGImage key:@"anonimo"];
            marcador.position = CGPointMake(100, 768-100);
            [self addChild:marcador];
            
            CCSprite* flecha = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"flecha.png"].CGImage key:@"flecha"];
            flecha.scaleY = -1;
            flecha.position = CGPointMake(100+100, 768-100-100);
            [self addChild:flecha];
        }
        
        self.btn = [[BotonParametros alloc] initWithTitle:@"Continuar"];
        self.btn.position = CGPointMake(self.fondo.position.x, self.fondo.position.y+self.fondo.contentSize.height/2-self.fondo.contentSize.height-52);
        [self addChild:self.btn];
        
    }
    
    return self;
}

- (UIImage *) generaTexturaFondo:(TipoPartidaInvasion) tipo {
    NSString* str;
    switch (tipo) {
        case TipoPartidaInvasionGoNoGo:
            str = @"Intenta ser el más rápido cazador de villanos. ¡Mucha suerte!";
            break;
        case TipoPartidaInvasionReto:
            str = @"Los Villanos se han estado entrenando y son más y más rápidos. Como samurái, deberás cazar a todos los villanos. Ten cuidado de no cazar a ningún Samurai.";
            break;
        case TipoPartidaInvasionStop:
            str = @"Caza todos los villanos menos cuando veas esta señal o escuches un pitido.¡ÁNIMO!";
            break;
        case TipoPartidaInvasionStopDistractores:
            str = @"Caza a todos los villanos que puedas, no des a ningún samurái ni tampoco a ningún villano cuando la señal te avise.¡ÁNIMO!";
            break;
        case TipoPartidaInvasionStopAudio:
            str = @"Caza todos los villanos menos cuando oigas un pitido.¡ÁNIMO!";
            break;
        case TipoPartidaInvasionStopAudioDistractores:
            str = @"Caza a todos los villanos que puedas, no des a ningún samurái ni tampoco a ningún villano cuando la señal te avise. ¡ÁNIMO!";
            break;
        case TipoPartidaInvasionSalva:
            str = @"Intenta ser el más rápido cazador de Samurais. ¡Mucha suerte!";
            break;
        case TipoPartidaInvasionAleatorio:
            str = @"Caza al villano o al samurái según te indique ésta señal. Recuerda que si escuchas un pitido o ves la señal no debes cazar a ninguno. ¡Mucha suerte samurái!";
            break;
    }
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];

    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:22.0],
                                 NSParagraphStyleAttributeName: paragraphStyle,
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:122/255.0 green:189/255.0 blue:196/255.0 alpha:1.0]
                                 };
    NSAttributedString *textoString = [[NSAttributedString alloc] initWithString:str attributes:attributes];
    
    NSDictionary *attributesLarge = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:32.0],
                                      NSParagraphStyleAttributeName: paragraphStyle,
                                      NSForegroundColorAttributeName: [UIColor whiteColor]
                                      };
    NSString* strSesion = [NSString stringWithFormat:@"Sesion %d de %d\n", self.sesion.numSesion, totalSesiones];
    NSMutableAttributedString* sesionString = [[NSMutableAttributedString alloc] initWithString:strSesion attributes:attributesLarge];
    
    [sesionString appendAttributedString:textoString];
    textoString = sesionString.copy;
    
    float ancho = str.length*3;
    float alto = 360;
    
    ancho = ancho<370?370:ancho;
    
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho, alto)];
    fondo.backgroundColor = [UIColor clearColor];
    fondo.opaque = NO;
    
    UIView* fondoVerde = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho, alto)];
    fondoVerde.backgroundColor = [UIColor juegoVerdeOscuro];
    fondoVerde.layer.cornerRadius = 10.0;
    fondoVerde.opaque = NO;
    [fondo addSubview:fondoVerde];
    
    

    UITextView* texto = [[UITextView alloc] initWithFrame:CGRectMake(25, 140, ancho-50, alto-100)];
    texto.backgroundColor = [UIColor clearColor];
    [texto setAttributedText:textoString];
    [fondo addSubview:texto];
    
    return [ImageUtils imageWithView:fondo];
}

@end
