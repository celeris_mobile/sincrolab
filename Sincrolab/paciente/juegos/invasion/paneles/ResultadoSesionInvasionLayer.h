//
//  ResultadoSesionInvasionLayer.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelParametrosLayer.h"
#import "ResultadoSesionInvasion.h"

@interface ResultadoSesionInvasionLayer : PanelParametrosLayer
- (id) initWithResultado:(ResultadoSesionInvasion *) resultado;
@end
