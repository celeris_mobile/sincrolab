//
//  ConfiguracionSesionInvasionLayer.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 16/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PanelParametrosLayer.h"
#import "SesionInvasion.h"


@interface ConfiguracionSesionInvasionLayer : PanelLayer
- (id) initWithSesion:(SesionInvasion *)sesion totalSesiones:(NSInteger) numSesiones;
@end
