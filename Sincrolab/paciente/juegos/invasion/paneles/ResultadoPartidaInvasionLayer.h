//
//  ResultadoPartidaInvasionLayer.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ResultadosPartidaLayer.h"
#import "GraficaLineas.h"
#import "cocos2d.h"
#import "EstadoEntrenamiento.h"

@interface ResultadoPartidaInvasionLayer : ResultadosPartidaLayer <GraficaLineasDelegate, GraficaLineasDatasource>
- (id) initWithResultadoSesiones:(NSArray *) resultadoSesiones estadoEntrenamiento:(EstadoEntrenamiento *) estadoEntrenamiento andCambioNivel:(NSNumber *) cambioNivel;
@end

