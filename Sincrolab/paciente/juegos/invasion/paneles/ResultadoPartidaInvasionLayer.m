//
//  ResultadoPartidaInvasionLayer.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ResultadoPartidaInvasionLayer.h"
#import "ResultadoSesionInvasion.h"
#import "GraficaLineas.h"
#import "ImageUtils.h"
#import "UIColor+Extensions.h"
#import "BotonParametros.h"

@interface ResultadoPartidaInvasionLayer()
@property (nonatomic, strong) CCSprite* fondo;

@property (nonatomic, strong) GraficaLineas* graficaAciertos;
@property (nonatomic, strong) GraficaLineas* graficaTiempos;

@property (nonatomic, weak) NSArray* resultados;
@property (nonatomic, strong) NSNumber* cambioNivel;
@property (nonatomic, strong) EstadoEntrenamiento* estadoEntrenamiento;

@property (nonatomic, strong) NSArray* mensajes;
@end

@implementation ResultadoPartidaInvasionLayer

- (id) initWithResultadoSesiones:(NSArray *) resultadoSesiones estadoEntrenamiento:(EstadoEntrenamiento *) estadoEntrenamiento andCambioNivel:(NSNumber *) cambioNivel {
    
    self = [super init];
    if(self) {
        self.estadoEntrenamiento = estadoEntrenamiento;
        self.resultados = resultadoSesiones;
        self.cambioNivel = cambioNivel;
        
        NSArray* mensajes40 = @[@"No desesperes.", @"¡Concéntrate!", @"¡Sigue intentándolo!", @"¡Fíjate bien!", @"¡ánimo!", @"Poco a poco irás mejorando.", @"Cada dáa mejorarás un poco más.", @"Ya verás como el próximo día te sale mejor", @"¡No te rindas!", @"¡Sigue esforzándote!", @"Te vas acercando ¡Sigue así!", @"¡Concéntrate un poco más!", @"¡Recuerda...la concentración es importante!"];
        NSArray* mensajes69 = @[@"¡Genial! Sigue así.", @"¡Vas por buen camino, sigue así!", @"¡No esta mal!", @"¡ánimo!", @"¡Estás a punto de conseguirlo!", @"Solo debes concéntrate un poco más.", @"El próximo día seguro que lo logras.", @"Poco a poco irás mejorando.", @"Solo esfuérzate un poco más."];
        NSArray* mensajes85 = @[@"Enhorabuena!", @"¡Bien hecho! Estás mejorando cada día.", @"¡Sigue así!", @"¡Bravo!", @"¡Genial!", @"¡Conseguido!", @"¡Alucinante!", @"¡Que bestia!", @"Buen Trabajo", @"¡Muy bien!", @"¡Que crack!", @"¡Bravo!", @"¡Buenísimo!", @"¡Genial!", @"¡Alucinante!", @"¡Enhorabuena!", @"¡Que máquina!", @"Has pasado de nivel.", @"Estás mejorando cada día, pasas de nivel.", @"Pasas al siguiente nivel", @"¿Te atreves con el siguiente nivel?", @"Has conseguido pasar al siguiente nivel.", @"Has pasado de nivel, ¡Sigue así!", @"Vamos con el siguiente nivel.", @"Veamos que tal el siguiente nivel.", @"Ahora la cosa se complica."];
        NSArray* mensajes100 = @[@"¡Genial!", @"¡Alucinante!", @"¡Que bestia!", @"Buen Trabajo", @"¡Muy bien!", @"¡Que crack!", @"¡Bravo!", @"¡Buenísimo!", @"¡Genial!", @"¡Alucinante!", @"¡Enhorabuena!", @"¡Que máquina!", @"Estás mejorando cada día.", @"No hay quien pueda contigo"];
        
        NSMutableArray* auxPAciertos = [[NSMutableArray alloc] init];
        for (ResultadoSesionInvasion* resultado in self.resultados) {
            [auxPAciertos addObject:resultado.porcentajeAciertos];
        }
        
        NSNumber* pAciertos = [auxPAciertos valueForKeyPath:@"@avg.self"];
        if(pAciertos.intValue<40)
            self.mensajes = mensajes40;
        else if (pAciertos.intValue<69)
            self.mensajes = mensajes69;
        else if (pAciertos.intValue<85)
            self.mensajes = mensajes85;
        else
            self.mensajes = mensajes100;
        
        
        [self setup];
    }
    
    return self;
}


- (void)muestra {
    [super muestra];
    
    
    CCSprite* personaje = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"mensaje.png"].CGImage key:@"personajeMensaje"];
    personaje.position = CGPointMake(104, 125);
    [self addChild:personaje];
    
    CCSprite* bocadillo = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"bocadillo.png"].CGImage key:@"bocadilloPersonaje"];
    bocadillo.position = CGPointMake(155, 457);
    [self addChild:bocadillo];
    
    
    NSString* mensaje = [self.mensajes objectAtIndex:arc4random()%self.mensajes.count];
    CCLabelTTF *texto = [CCLabelTTF labelWithString:mensaje
                                           fontName:@"ChauPhilomeneOne-Italic" fontSize:20 dimensions:CGSizeMake(179, 95) hAlignment:kCCTextAlignmentCenter lineBreakMode:kCCLineBreakModeWordWrap];
    texto.verticalAlignment=kCCVerticalTextAlignmentCenter;
    texto.color = ccc3(255,255,255);
    texto.position = ccp(155, 483);
    [self addChild:texto z:1+10];
    
    [texto setString:mensaje];
    
    bocadillo.visible = NO;
    texto.visible = NO;
    
    personaje.position = CGPointMake(104, -300);
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.6];
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(103, 125)];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        bocadillo.visible = YES;
        texto.visible = YES;
    }];
    
    CCSequence* secuencia = [CCSequence actions:delay, ease, callBlock, nil];
    [personaje runAction:secuencia];
    
    
}

- (void) setup {
    [super setup];
}

- (UIView *) graficaIzquierda {
    float maxt = 0;
    for (ResultadoSesionInvasion* resultado in self.resultados) {
        maxt = MAX(maxt, resultado.porcentajeAciertos.floatValue);
    }
    self.graficaAciertos = [[GraficaLineas alloc] initWithFrame:CGRectMake(40, 287, 215, 103)];
    self.graficaAciertos.minY = 0.0;
    self.graficaAciertos.delegate = self;
    self.graficaAciertos.dataSource = self;
    [self.graficaAciertos dibuja];
    return self.graficaAciertos;
}

- (UIView *) graficaDerecha {
    float maxt = 0;
    for (ResultadoSesionInvasion* resultado in self.resultados) {
        maxt = MAX(maxt, resultado.tiempoMedioDeRespuestaAciertos.floatValue);
    }
    self.graficaTiempos = [[GraficaLineas alloc] initWithFrame:CGRectMake(272, 287, 215, 103)];
    self.graficaTiempos.minY = 0.0;
    self.graficaTiempos.delegate = self;
    self.graficaTiempos.dataSource = self;
    [self.graficaTiempos dibuja];
    return self.graficaTiempos;
}

- (NSString *) puntuacion {
    int puntuacion = 0;
    
    for (ResultadoSesionInvasion* resultado in self.resultados) {
        puntuacion+=resultado.puntuacion.intValue;
    }
    
    //TODO ¿Le sumamos la puntuación del paciente?
    
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    return [formatter stringFromNumber:[NSNumber numberWithInt:puntuacion]];
}

- (NSString *) nivel {
    //TODO ¿Le sumamos el nivel del paciente?
    return [NSString stringWithFormat:@"%d", self.cambioNivel.intValue];
}

- (NSString *) deltaNivel {
    NSString* signo = self.cambioNivel.intValue>0?@"+":@"";
    return [NSString stringWithFormat:@"%@%@", signo, self.cambioNivel];
}

- (NSString*) aciertos {
    int aciertos = 0;
    
    for (ResultadoSesionInvasion* resultado in self.resultados) {
        aciertos+=resultado.numeroDeAciertos.intValue+resultado.numeroDeNoComisiones.intValue;
    }
    
    return [NSString stringWithFormat:@"%d", aciertos];
}

- (NSString *) tiempoRespuesta {
    float suma_ts = 0.0;
    
    for (ResultadoSesionInvasion* resultado in self.resultados) {
        suma_ts+=resultado.tiempoMedioDeRespuestaAciertos.floatValue;
    }
    
    float media = suma_ts/((float)self.resultados.count);
    
    return [NSString stringWithFormat:@"%d", (int)(media*1000.0)];
}


#pragma mark Grafica Lineas Delegate

- (void) grafica:(GraficaLineas *) grafica configuraSet:(NSInteger)numSet enContexto:(CGContextRef)context {
    if(grafica==self.graficaAciertos) {
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.0 green:136/255.0 blue:40/255.0 alpha:1.0].CGColor);
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:153/255.0 green:211/255.0 blue:175/255.0 alpha:1.0].CGColor);
        CGContextSetLineWidth(context, 4);
        CGContextSetLineJoin(context, kCGLineJoinRound);
        CGContextSetLineCap(context, kCGLineCapRound);
    }
    
    if(grafica==self.graficaTiempos) {
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:1.0 green:94/255.0 blue:18/255.0 alpha:1.0].CGColor);
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:1.0 green:190/255.0 blue:159/255.0 alpha:1.0].CGColor);
        CGContextSetLineWidth(context, 4);
        CGContextSetLineJoin(context, kCGLineJoinRound);
        CGContextSetLineCap(context, kCGLineCapRound);
    }
}


#pragma mark Grafica Lineas DataSource

- (int)numeroDeSetsEnGraficaDeLineas:(GraficaLineas *)grafica {
    return 1;
}

- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet {
    if(grafica==self.graficaAciertos)
        return [self.resultados valueForKeyPath:@"porcentajeAciertos"];
    else
        return [self.resultados valueForKeyPath:@"tiempoMedioDeRespuestaAciertos"];
    
    return nil;
}

- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica {
    NSMutableArray* etiquetas = [[NSMutableArray alloc] init];
    for (int e = 0; e<self.resultados.count; e++) {
        [etiquetas addObject:[NSString stringWithFormat:@"S%d", e+1]];
    }
    
    return etiquetas.copy;
}


@end
