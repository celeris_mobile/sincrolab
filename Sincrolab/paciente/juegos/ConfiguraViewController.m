//
//  ConfiguraViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 29/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ConfiguraViewController.h"
#import "EstadoJuegoGongs.h"
#import "EstadoJuegoInvasion.h"
#import "EstadoJuegoPoker.h"

@interface ConfiguraViewController ()
@property (nonatomic, strong) UITextField* itinerarioTf;
@property (nonatomic, strong) UITextField* hitoTf;
@property (nonatomic, strong) EstadoJuego* estado;
@property (nonatomic, strong) NSDictionary* campos;

@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIView* contentView;
@end

@implementation ConfiguraViewController {
    BOOL cambios;
}

- (id) initWithEstado:(EstadoJuego *) estado {

    self = [super init];
    if (self) {
        self.estado = estado;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cambios = NO;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.scrollView addSubview:self.contentView];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 24, 110, 21)];
    label.text = @"Itinerario";
    [self.contentView addSubview:label];
    
    self.itinerarioTf = [[UITextField alloc] initWithFrame:CGRectMake(144, 20, 136, 30)];
    self.itinerarioTf.borderStyle = UITextBorderStyleRoundedRect;
    self.itinerarioTf.text = self.estado.itinerario;
    [self.contentView addSubview:self.itinerarioTf];
    
    UILabel* label2 = [[UILabel alloc] initWithFrame:CGRectMake(20, 24+38, 110, 21)];
    label2.text = @"Hito";
    [self.contentView addSubview:label2];
    
    self.hitoTf = [[UITextField alloc] initWithFrame:CGRectMake(144, 20+38, 136, 30)];
    self.hitoTf.borderStyle = UITextBorderStyleRoundedRect;
    self.hitoTf.text = self.estado.hito;
    [self.contentView addSubview:self.hitoTf];
    
    UIButton* botonGenerar = [[UIButton alloc] initWithFrame:CGRectMake(205, 103, 75, 30)];
    [botonGenerar addTarget:self action:@selector(generar) forControlEvents:UIControlEventTouchUpInside];
    [botonGenerar setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [botonGenerar setTitle:@"Generar" forState:UIControlStateNormal];
    [self.contentView addSubview:botonGenerar];
    
    NSString* campos;
    if([self.estado isKindOfClass:[EstadoJuegoGongs class]])
        campos = @"DMIN,DMAX,ELEMENTOS,NBACK,SESIONES,TRIALS,TIE,TEXP,MODALIDAD,%_DIANAS";
    if([self.estado isKindOfClass:[EstadoJuegoInvasion class]])
        campos = @"SESIONES,TRIALS,TEE1,TEE2,TIE,DSS,CAMBIO,N_CAMBIOS,P_APARICION_NG,P_APARICION_SS,TIPO_SS";
    if([self.estado isKindOfClass:[EstadoJuegoPoker class]])
        campos = @"CARTAS,JUGADORES,CATEGORIAS,TIE,TRESP,AYUDAS,TIEMPO_AYUDAS,P_DIANAS,N_CAMBIOS";

    NSArray* campos_arr = [campos componentsSeparatedByString:@","];
    NSMutableDictionary* auxcampos = [[NSMutableDictionary alloc] init];
    int c = 0;
    for (NSString* campo in campos_arr) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 145+38*c, 110, 21)];
        label.text = campo;
        [self.contentView addSubview:label];
        
        UITextField* textField = [[UITextField alloc] initWithFrame:CGRectMake(144, 141+38*c, 136, 30)];
        textField.borderStyle = UITextBorderStyleRoundedRect;
        [self.contentView addSubview:textField];
        [textField addTarget:self action:@selector(cambioEnTexto:) forControlEvents:UIControlEventEditingChanged];
        
        c++;
        
        [auxcampos setObject:textField forKey:campo];
    }
    self.campos = auxcampos.copy;
    
    UIButton* botonJugar = [[UIButton alloc] initWithFrame:CGRectMake(205, 145+38*campos_arr.count, 75, 30)];
    [botonJugar addTarget:self action:@selector(jugar) forControlEvents:UIControlEventTouchUpInside];
    [botonJugar setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [botonJugar setTitle:@"Jugar" forState:UIControlStateNormal];
    [self.contentView addSubview:botonJugar];
    
    CGRect contentFrame = self.contentView.frame;
    contentFrame.size.height = botonJugar.frame.origin.y+60;
    self.contentView.frame = contentFrame;
    
    [self configuraCampos];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = self.contentView.frame.size;
}

- (void) cambioEnTexto:(UITextField *) textField {
    cambios = YES;
    
    for (NSString* key in self.campos) {
        UITextField* tf = [self.campos objectForKey:key];
        if(tf==textField) {
            NSString* nombreCampo = key.lowercaseString;
            nombreCampo = [nombreCampo stringByReplacingOccurrencesOfString:@"%" withString:@"porcentaje"];
            [self.estado setObject:textField.text forKey:nombreCampo];
        }
    }
}

- (void) configuraCampos {
    for (NSString* key in self.campos) {
        UITextField* tf = [self.campos objectForKey:key];

        NSString* nombreCampo = key.lowercaseString;
        nombreCampo = [nombreCampo stringByReplacingOccurrencesOfString:@"%" withString:@"porcentaje"];
        tf.text = [[[self.estado valueForKey:nombreCampo] componentsSeparatedByString:@"|"] objectAtIndex:0];
    }
}

- (void) generar {
    cambios = YES;
    
    if([self.estado isKindOfClass:[EstadoJuegoGongs class]]) {
        NSArray* componentes = [self.itinerarioTf.text componentsSeparatedByString:@":"];
        if(componentes.count==1)
            self.estado = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:self.itinerarioTf.text conAfectacion:@"1"];
        else
            self.estado = [EstadoJuegoGongs estadoParaHitoInicialdeItinerario:[componentes objectAtIndex:0] conAfectacion:[componentes objectAtIndex:1]];
    }
    
    if([self.estado isKindOfClass:[EstadoJuegoInvasion class]]) {
        NSArray* componentes = [self.itinerarioTf.text componentsSeparatedByString:@":"];
        if(componentes.count==1)
            self.estado = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:self.itinerarioTf.text conAfectacion:@"1"];
        else
            self.estado = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:[componentes objectAtIndex:0] conAfectacion:[componentes objectAtIndex:1]];
    }
    
    if([self.estado isKindOfClass:[EstadoJuegoPoker class]]) {
        NSArray* componentes = [self.itinerarioTf.text componentsSeparatedByString:@":"];
        if(componentes.count==1)
            self.estado = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:self.itinerarioTf.text conAfectacion:@"1"];
        else
            self.estado = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:[componentes objectAtIndex:0] conAfectacion:[componentes objectAtIndex:1]];
    }
    
    [self.estado configuraParaHito:self.hitoTf.text.intValue];
    [self configuraCampos];
}

- (void) jugar {
    [self.delegate estadoModificado:self.estado];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
