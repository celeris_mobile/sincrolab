//
//  JuegoPokerViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "JuegoPokerViewController.h"
#import "JuegoPokerLayer.h"
#import "EstadoJuegoPoker.h"
#import "Constantes.h"
#import "Configuraciones.h"
#import "PartidaPoker.h"

@interface JuegoPokerViewController ()
@property (nonatomic, strong) JuegoPokerLayer* layer;
@end

@implementation JuegoPokerViewController

- (CCScene *)mainScene {
    CCScene *scene = [CCScene node];
    
	self.layer = [[JuegoPokerLayer alloc] initWithPaciente:self.paciente estado:self.estadoEntrenamiento];
    [scene addChild: self.layer];
    
    return scene;
}

- (void) salirDado {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(partidaTerminada:) name:@"NotificacionPartidaTerminada" object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[CCDirector sharedDirector] setDelegate:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) partidaTerminada:(NSNotification *) notification {
    [[CCDirector sharedDirector] stopAnimation];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificacionPartidaTerminada" object:nil];
    PartidaInvasion* partida = [notification.userInfo objectForKey:@"partida"];
    
    [self.delegate juegoTerminado:partida];
}

//BOTONES AUXILIARES
- (void) finSesionDado {
    [self.layer finalizaSesion];
}

- (void) finPartidaDado {
    [self.layer finalizaPartida];
}

@end




