//
//  JuegoPokerLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 30/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "JuegoPokerLayer.h"
#import "CartaPoker.h"
#import "Carta.h"
#import "EstadoJuegoPoker.h"
#import "EstadoEntrenamiento.h"
#import "PartidaPoker.h"
#import "FondoPoker.h"
#import "TiempoPokerLayer.h"
#import "ResultadoPartidaPokerLayer.h"
#import "SesionPoker.h"
#import "RespuestaPoker.h"
#import "CartasLayer.h"
#import "CategoriasLayer.h"
#import "TutorialPoker.h"

@interface JuegoPokerLayer()
@property (nonatomic, strong) Paciente* paciente;
@property (nonatomic, strong) EstadoEntrenamiento* estadoEntrenamiento;
@property (nonatomic, strong) EstadoJuegoPoker* estado;

@property (nonatomic, strong) NSArray* trials;
@property (nonatomic, strong) NSArray* cambios;
@property (nonatomic, strong) NSMutableArray* respuestas;

@property (nonatomic, strong) ResultadoSesionPoker* resultadoSesion;

@property (nonatomic, strong) PartidaPoker* partida;

@property (nonatomic, strong) FondoPoker* fondo;
@property (nonatomic, strong) TiempoPokerLayer* tiempo;
@property (nonatomic, strong) CartasLayer* cartas;
@property (nonatomic, strong) CategoriasLayer* categorias;

@property (nonatomic, strong) CCSprite* errorSprite;
@property (nonatomic, strong) CCSprite* aciertoSprite;

@property (nonatomic, strong) ResultadoPartidaPokerLayer* panelResultadoPartida;

@property (nonatomic, strong) SesionPoker* sesion;

@property (nonatomic, strong) TutorialPoker* tutorial;
@end

@implementation JuegoPokerLayer  {
    float secs_transcurridos;
    float t_estimulo;
    int num_trial;
}

- (id) initWithPaciente:(Paciente *) paciente estado:(EstadoEntrenamiento *) estadoEntrenamiento {
    self = [super init];
    
	if(self) {
        
        self.estadoEntrenamiento = estadoEntrenamiento;
        self.estado = self.estadoEntrenamiento.estadoPoker;
        self.paciente = paciente;
        
        self.fondo = [[FondoPoker alloc] init];
        self.fondo.anchorPoint = CGPointMake(0, 0);
        [self addChild:self.fondo];
        
        self.cartas = [[CartasLayer alloc] init];
        self.cartas.delegate = self;
        [self addChild:self.cartas];
        
        self.categorias = [[CategoriasLayer alloc] init];
        [self addChild:self.categorias];
        
        self.errorSprite = [CCSprite spriteWithFile:@"error.png"];
        self.errorSprite.position = CGPointMake(1024-80, 80);
        self.errorSprite.opacity = 0.0;
        [self addChild:self.errorSprite];
        
        self.aciertoSprite = [CCSprite spriteWithFile:@"check.png"];
        self.aciertoSprite.position = CGPointMake(1024-80, 80);
        self.aciertoSprite.opacity = 0.0;
        [self addChild:self.aciertoSprite];
        
        self.tiempo = [[TiempoPokerLayer alloc] init];
        [self addChild:self.tiempo];
        
	}
    
	return self;
}

- (void)onEnterTransitionDidFinish {
    self.sesion = nil;
    
    //[self performSelector:@selector(configuraSesion) withObject:nil afterDelay:0.5];
    [self configuraSesion];
}

 
- (void) configuraSesion {
    self.sesion = [SesionPoker sesionNum:1 conEstado:self.estado];
    
    [self configuraTrials];
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber* instruccionesVistas = (NSNumber *)[userDefaults objectForKey:@"instrucciones_poker"];
    if(instruccionesVistas.boolValue)
        [self muestraSesion];
    else
        [self muestraTutorial];
}

- (void) muestraTutorial {
    self.tutorial = [[TutorialPoker alloc] initWithCartas:self.cartas andCategorias:self.categorias];
    self.tutorial.delegate = self;
    [self addChild:self.tutorial];
    self.tutorial.sesion = self.sesion;
    [self.tutorial muestra];
    
    self.fondo.sesion = self.sesion;
}

- (void) tutorialFinalizado {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:@"instrucciones_poker"];
    [userDefaults synchronize];
    
    self.tutorial.delegate = nil;
    [self.tutorial removeFromParent];
    self.tutorial = nil;
    [self muestraSesion];
}


- (void) muestraSesion {
    secs_transcurridos = 0.0;
    num_trial = 0;
    self.respuestas = [[NSMutableArray alloc] init];

    NSNumber* numCategoria = [self.sesion.categorias.allObjects objectAtIndex:arc4random()%self.sesion.categorias.count];
    self.categorias.categoria = numCategoria.intValue;
    
    self.categorias.ayuda = self.sesion.ayudas == (NSInteger)1;

    self.cartas.sesion = self.sesion;
    self.fondo.sesion = self.sesion;
    [self.tiempo setTiempoTotal:self.sesion.tiempoTotal+1.0];
        
    [self.tiempo muestra];
    [self.cartas muestra];
    self.categorias.visible = YES;
    
    [self performSelector:@selector(iniciaSesion) withObject:nil afterDelay:2.0];
}

- (void) configuraTrials {
    NSInteger num_dianas = self.sesion.cartas*self.sesion.jugadores*self.sesion.p_dianas/100;
    
    NSMutableSet* auxdianas = [[NSMutableSet alloc] init];
    for (int d = 0; d<num_dianas; d++) {
        BOOL encontrado = NO;
        while (!encontrado) {
            int aleatorio = arc4random()%(self.sesion.cartas*self.sesion.jugadores-1)+1;
            NSNumber* number = [NSNumber numberWithInt:aleatorio];
            if(![auxdianas containsObject:number]) {
                [auxdianas addObject:number];
                encontrado = YES;
            }
        }
    }
    
    NSMutableArray* auxtrials = [[NSMutableArray alloc] init];
    for (int t = 0; t<self.sesion.cartas*self.sesion.jugadores; t++) {
        NSNumber* numero = [NSNumber numberWithInt:t];
        if([auxdianas containsObject:numero]) {
            [auxtrials addObject:[NSNumber numberWithBool:YES]];
        } else
            [auxtrials addObject:[NSNumber numberWithBool:NO]];
    }
    
    self.trials = auxtrials.copy;
    

    if(self.sesion.categorias.count>1) {
        NSInteger num_cambios = self.sesion.n_cambios;
        
        NSMutableSet* auxNumCambios = [[NSMutableSet alloc] init];
        for (int d = 0; d<num_cambios; d++) {
            BOOL encontrado = NO;
            while (!encontrado) {
                int aleatorio = arc4random()%(self.sesion.cartas*self.sesion.jugadores-1)+1; //para que no salga cambio en el primero
                NSNumber* number = [NSNumber numberWithInt:aleatorio];
                if(![auxNumCambios containsObject:number]) {
                    [auxNumCambios addObject:number];
                    encontrado = YES;
                }
            }
        }
        
        NSMutableArray* auxcambios = [[NSMutableArray alloc] init];
        for (int t = 0; t<self.sesion.cartas*self.sesion.jugadores; t++) {
            NSNumber* numero = [NSNumber numberWithInt:t];
            
            if([auxNumCambios containsObject:numero]) {
                [auxcambios addObject:[NSNumber numberWithBool:YES]];
            } else {
                [auxcambios addObject:[NSNumber numberWithBool:NO]];
            }
        }
        
        self.cambios = auxcambios.copy;
    }

}

- (void) iniciaSesion {
    [self scheduleUpdate];
    [self scheduleOnce:@selector(lanzaPrimerEstimulo) delay:0.5];

}

- (void) lanzaPrimerEstimulo {
    [self lanzaEstimulo];
    [self schedule:@selector(lanzaEstimulo) interval:self.sesion.tie];
    
    if(self.sesion.tiempo_ayudas>0)
        [self.categorias performSelector:@selector(desaparece) withObject:nil afterDelay:self.sesion.tiempo_ayudas];
}

- (void) lanzaEstimulo {
    if(num_trial<self.sesion.jugadores*self.sesion.cartas-1 && self.sesion.categorias.count>1) {
        NSNumber* cambio = [self.cambios objectAtIndex:num_trial];
        if(cambio.boolValue) {
            //[self cambiaCategoria];
            [self performSelector:@selector(cambiaCategoria) withObject:nil afterDelay:self.sesion.tie/2];
        }
    }
    
    if(num_trial>=self.sesion.cartas*self.sesion.jugadores)
        [self performSelector:@selector(finSesion) withObject:nil afterDelay:0.5];
    else {
        //Cuándo se lanzó este estímulo
        t_estimulo = secs_transcurridos;
        
        [self.respuestas addObject:[[RespuestaPoker alloc] init]];
        [self.cartas lanzaEstimulo:num_trial conDiana:[self.trials objectAtIndex:num_trial] conCategoria:self.categorias.categoria];
        
        num_trial += 1;
    }
}

- (void) cambiaCategoria {
    BOOL encontrado = NO;
    NSNumber* numCategoria;
    while (!encontrado) {
        int aleatorio = arc4random()%self.sesion.categorias.count;
        numCategoria = [self.sesion.categorias.allObjects objectAtIndex:aleatorio];
        if(numCategoria.intValue!=self.categorias.categoria) {
            encontrado = YES;
            self.categorias.categoria = numCategoria.intValue;
        }
    }


    self.categorias.categoria = self.categorias.categoria;
    
    NSString* nombre;
    switch (self.categorias.categoria) {
        case CategoriaPokerTrama:
            nombre = @"trama";
            break;
        case CategoriaPokerForma:
            nombre = @"forma";
            break;
        case CategoriaPokerColor:
            nombre = @"color";
            break;
        case CategoriaPokerSimbolo:
            nombre = @"simbolo";
            break;
    }
    
    if(self.sesion.ayudas != (NSInteger)1)
        nombre = @"oculta";
    
    CCSprite* spriteCategoria = [CCSprite spriteWithCGImage:
                                 [UIImage imageNamed:[NSString stringWithFormat:@"c%@_on.png", nombre]].CGImage
                                                        key:[NSString stringWithFormat:@"%@on",nombre]];
    
    spriteCategoria.position = CGPointMake(1024/2, 768-160);
    [self addChild:spriteCategoria];
    
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024/2, 768-80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:1.0];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024/2, 768)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCCallBlock* completado = [CCCallBlock actionWithBlock:^{
        [spriteCategoria removeFromParent];
    }];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction, completado]];
    
    [spriteCategoria runAction:secuencia];
}

- (void)update:(ccTime)delta {
    secs_transcurridos += delta;
    [self.tiempo decrementa:delta];
}

- (void) finSesion {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];
    
    [self compruebaAciertos];
    
    [self.tiempo oculta];
    
    self.resultadoSesion = [ResultadoSesionPoker resultadoConRespuestas:self.respuestas];
    self.resultadoSesion.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];

    
    [self finPartida];
}


- (void) compruebaAciertos {
    for (int t = 0; t<self.sesion.cartas*self.sesion.jugadores; t++) {
        
        RespuestaPoker* respuesta = [self.respuestas objectAtIndex:t];
        
        NSNumber*diana = [self.trials objectAtIndex:t];
        
        if(respuesta.tiempo == 0.0) {
            if(diana.boolValue) {
                respuesta.tipo = TipoRespuestaPokerOmision;
            } else {
                respuesta.tipo = TipoRespuestaPokerNoComision;
            }
        } else {
            if(diana.boolValue) {
                respuesta.tipo = TipoRespuestaPokerAcierto;
            } else {
                respuesta.tipo = TipoRespuestaPokerComision;
            }
        }
    }
}

- (void) finPartida {

    self.partida = [PartidaPoker object];
    self.partida.sesiones = [[NSMutableArray alloc] init];
    
    [self.partida.sesiones addObject:self.resultadoSesion.valores];
    
    if(self.estadoEntrenamiento.tipo.intValue==TipoEntrenamientoAutomatico) {
        EstadoJuegoCambio cambioNivel = [self.estado compruebaCambioNivelConResultados:[NSArray arrayWithObject:self.resultadoSesion]];
        switch (cambioNivel) {
            case EstadoJuegoCambioSubeNivel:
                self.partida.cambioNivel = [NSNumber numberWithInt:1];
                break;
            case EstadoJuegoCambioBajaNivel:
                self.partida.cambioNivel = [NSNumber numberWithInt:-1];
                break;
            case EstadoJuegoCambioSeMantiene:
                self.partida.cambioNivel = [NSNumber numberWithInt:0];
                break;
            case EstadoJuegoCambioFinal:
                //TODO Haz algo con esto
                [[[UIAlertView alloc] initWithTitle:@"¡Atención!" message:@"Entrenamiento finalizado" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                break;
        }
    } else {
        //TODO cuando es un entrenamiento manual subimos el nivel?
        self.partida.cambioNivel = [NSNumber numberWithInt:0];
    }

    
    
    self.panelResultadoPartida = [[ResultadoPartidaPokerLayer alloc] initWithResultadoSesion:self.resultadoSesion paciente:self.paciente andCambioNivel:self.partida.cambioNivel];
    
    self.panelResultadoPartida.delegate = self;
    [self addChild:self.panelResultadoPartida];
    [self.panelResultadoPartida muestra];

}



- (void) muestraError {
    self.errorSprite.position = CGPointMake(1024-80, 40);
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.5];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 120)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction]];
    
    [self.errorSprite runAction:secuencia];
}

- (void) muestraAcierto {
    self.aciertoSprite.position = CGPointMake(1024-80, 40);
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 80)];
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.2];
    CCSpawn* inAction = [CCSpawn actionWithArray:@[mueveIn, fadeIn]];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.5];
    
    CCMoveTo* mueveOut = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(1024-80, 120)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    CCSpawn* outAction = [CCSpawn actionWithArray:@[mueveOut, fadeOut]];
    
    CCSequence* secuencia = [CCSequence actionWithArray:@[inAction, delay, outAction]];
    
    [self.aciertoSprite runAction:secuencia];
}

#pragma mark CartasLayer Delegate
- (void) dadaCartaEnTrial:(NSInteger)numTrial conAcierto:(BOOL) acierto {
    RespuestaPoker* respuesta = [self.respuestas objectAtIndex:numTrial];
    respuesta.tiempo = secs_transcurridos-t_estimulo;
    if(acierto) {
        respuesta.tipo = TipoRespuestaPokerAcierto;
        [self muestraAcierto];
        if(self.tutorial)
           [self.tutorial acierto];
    } else {
        respuesta.tipo = TipoRespuestaPokerComision;
        [self muestraError];
    }
}

- (void)cambiaTrialADiana:(NSInteger)numTrial {
    NSMutableArray * auxtrials = self.trials.mutableCopy;
    [auxtrials replaceObjectAtIndex:numTrial withObject:[NSNumber numberWithBool:YES]];
    self.trials = auxtrials.copy;
}

#pragma mark PanelDelegate
- (void)panelFinalizado:(PanelLayer *) panelLayer {
    if(panelLayer==self.panelResultadoPartida) {
        NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.partida, @"partida", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificacionPartidaTerminada" object:self userInfo:userInfo];
    }
}


#pragma mark Para Debug
- (void) finalizaSesion {
    [self finalizaPartida];
}

- (void) finalizaPartida {
    [self unscheduleUpdate];
    [self unscheduleAllSelectors];

    self.resultadoSesion = [ResultadoSesionPoker resultadoAleatorio];
    self.resultadoSesion.duracion = [NSNumber numberWithFloat:self.sesion.tiempoTotal];
    
    [self finPartida];
}

@end
