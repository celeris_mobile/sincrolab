//
//  CartaPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 30/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CartaPoker.h"
#import "ImageUtils.h"
#import "Circulo.h"
#import "Triangulo.h"
#import "Hexagono.h"
#import "Rombo.h"
#import "Forma.h"

@interface CartaPoker()
@property (nonatomic, strong) Carta* carta;
@property (nonatomic, strong) CCSprite* front;
@property (nonatomic, strong) CCSprite* back;
@property (nonatomic, strong) NSArray* colores;
@end

@implementation CartaPoker

- (id) initWithCarta:(Carta *) carta {
    self = [super init];
    
    if(self) {
        self.carta = carta;
        [self configuraCarta];
    }
    
    return self;
}

- (void) configuraCarta {
    self.colores = [NSArray arrayWithObjects:
                    [UIColor colorWithRed:0/255.0 green:158/255.0 blue:224/255.0 alpha:1.0],
                    [UIColor colorWithRed:255/255.0 green:91/255.0 blue:50/255.0 alpha:1.0],
                    [UIColor colorWithRed:95/255.0 green:66/255.0 blue:135/255.0 alpha:1.0],
                    [UIColor colorWithRed:255/255.0 green:0/255.0 blue:138/255.0 alpha:1.0],nil];
    
    self.front = [CCSprite spriteWithCGImage:[self generaImagenFront].CGImage key:[NSString stringWithFormat:@"c%d%d%d%d", self.carta.trama, self.carta.simbolo, self.carta.color, self.carta.forma]];
    [self addChild:self.front];
    self.front.visible = NO;
    
    self.back = [CCSprite spriteWithCGImage:[self generaImagenBack].CGImage key:@"cartaPokerBack"];
    [self addChild:self.back];
}

- (UIImage *) generaImagenBack {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 258, 258)];
    fondo.backgroundColor = [UIColor whiteColor];
    fondo.layer.cornerRadius = 10.0;
    fondo.opaque = NO;
    
    UIImageView* trama = [[UIImageView alloc] initWithFrame:CGRectInset(fondo.bounds, 13, 13)];
    trama.clipsToBounds = YES;
    trama.contentMode = UIViewContentModeCenter;
    trama.image = [UIImage imageNamed:@"trama_back.jpg"];
    trama.layer.cornerRadius = 8.0;
    trama.layer.borderWidth = 2.0;
    trama.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [fondo addSubview:trama];
    
    return [ImageUtils imageWithView:fondo];
}

- (UIImage *) generaImagenFront {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 258, 258)];
    fondo.backgroundColor = [UIColor whiteColor];
    fondo.layer.cornerRadius = 10.0;
    fondo.opaque = NO;
    
    UIImageView* trama = [[UIImageView alloc] initWithFrame:CGRectInset(fondo.bounds, 13, 13)];
    trama.clipsToBounds = YES;
    trama.contentMode = UIViewContentModeCenter;
    trama.image = [UIImage imageNamed:[NSString stringWithFormat:@"trama%d.jpg", self.carta.trama+1]];
    trama.layer.cornerRadius = 8.0;
    trama.layer.borderWidth = 2.0;
    trama.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [fondo addSubview:trama];
    
    Forma* forma;
    switch (self.carta.forma) {
        case 0:
            forma = [[Circulo alloc] initWithFrame:CGRectInset(fondo.bounds, 35, 35)];
            break;
        case 1:
            forma = [[Triangulo alloc] initWithFrame:CGRectInset(fondo.bounds, 20, 20)];
            break;
        case 2:
            forma = [[Hexagono alloc] initWithFrame:CGRectInset(fondo.bounds, 35, 35)];
            break;
        case 3:
            forma = [[Rombo alloc] initWithFrame:CGRectInset(fondo.bounds, 40, 40)];
            break;
    }
    forma.color = [self.colores objectAtIndex:self.carta.color];
    [fondo addSubview:forma];
    
    UIImageView* simbolo = [[UIImageView alloc] initWithFrame:CGRectInset(fondo.frame, 75, 75)];
    if([forma isKindOfClass:[Triangulo class]]) {
        simbolo.frame = CGRectMake(simbolo.frame.origin.x+10,
                                   simbolo.frame.origin.y-10,
                                   simbolo.frame.size.width-20,
                                   simbolo.frame.size.height-20);
    }
    simbolo.contentMode = UIViewContentModeScaleAspectFit;
    simbolo.image = [UIImage imageNamed:[NSString stringWithFormat:@"simbolo%d.png", self.carta.simbolo+1]];
    [fondo addSubview:simbolo];
    
    return [ImageUtils imageWithView:fondo];
}


- (void) voltea:(CGPoint) posicionMonton completado:(void (^)(void))completado {
    
    float d = 0.8f;
    
    CGPoint posicion = self.position;
    
    CGPoint movimiento = CGPointMake(1024/2-posicionMonton.x, 768/2-posicionMonton.y);
    movimiento.y = ABS(movimiento.y)>100?movimiento.y:movimiento.y*2;
    CCMoveBy* mueve1 = [CCMoveBy actionWithDuration:d/2 position:movimiento];
    

    CCOrbitCamera* orbit1;
    CCOrbitCamera* orbit2;
    if(ABS(1024/2-posicionMonton.x)<100) {
        float angulo = 768/2-posicionMonton.y<0?90:-90;
        
        orbit1 = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:1
                                            angleZ:0 deltaAngleZ:-90
                                            angleX:angulo deltaAngleX:0];
        orbit2 = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:1
                                            angleZ:-270 deltaAngleZ:-90
                                            angleX:angulo deltaAngleX:0];
        
    } else {
        float angulo = 1024/2-posicionMonton.x>0?90:-90;
        orbit1 = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:1
                                            angleZ:0 deltaAngleZ:angulo
                                            angleX:0 deltaAngleX:0];
        orbit2 = [CCOrbitCamera actionWithDuration:d/2 radius:1 deltaRadius:1
                                            angleZ:270 deltaAngleZ:angulo
                                            angleX:0 deltaAngleX:0];
    }
    
    CCEaseExponentialOut* ease1 = [CCEaseExponentialOut actionWithAction:mueve1];


    
    CCScaleTo* escala1 = [CCScaleTo actionWithDuration:d/2 scale:1.5];
    CCSpawn* rota1 = [CCSpawn actions:ease1, orbit1, escala1, nil];

    
    CCCallBlock* cambio = [CCCallBlock actionWithBlock:^{
        self.front.visible = !self.front.visible;
        self.back.visible = !self.back.visible;
        self.zOrder = 3;
        [self.delegate cartaVisible:self];
    }];
    
    CCMoveTo* mueve2 = [CCMoveTo actionWithDuration:d/2+0.3 position:posicion];
    CCEaseExponentialInOut* ease2 = [CCEaseExponentialInOut actionWithAction:mueve2];
    
    
    CCScaleTo* escala2 = [CCScaleTo actionWithDuration:d/2 scale:1.0];
    CCSpawn* rota2 = [CCSpawn actions:ease2, orbit2, escala2, nil];

   // CCDelayTime* delay = [CCDelayTime actionWithDuration:0.15];
    
    CCCallBlock* final = [CCCallBlock actionWithBlock:^{
        completado();
        //[self.delegate cartaVolteada:self];
    }];
    
    CCSequence* secuencia = [CCSequence actions:rota1, cambio, rota2, final, nil];
    //CCSequence* secuencia = [CCSequence actions:ease1, rota1, cambio, rota2, ease2, final, nil];
    //CCSequence* secuencia = [CCSequence actions:rota1, cambio, rota2, delay, final, nil];
    
    [self runAction: secuencia];
}

- (void) borra {
    self.delegate = nil;
    [self removeFromParent];
}


@end
