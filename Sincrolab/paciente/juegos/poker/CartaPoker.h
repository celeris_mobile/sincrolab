//
//  CartaPoker.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 30/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "Carta.h"

@class CartaPoker;

@protocol CartaPokerDelegate <NSObject>
- (void) cartaVisible:(CartaPoker *) carta;
//- (void) cartaVolteada:(CartaPoker *) carta;
@end

@interface CartaPoker : CCNode
@property (nonatomic, weak) id<CartaPokerDelegate> delegate;
- (id) initWithCarta:(Carta *) carta;
- (void) voltea:(CGPoint) posicionMonton completado:(void (^)(void))completado;
//- (void) voltea:(CGPoint) posicion;
- (void) borra;
@end
