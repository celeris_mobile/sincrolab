//
//  SesionPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "SesionPoker.h"

@implementation SesionPoker

+ (float) floatConPropiedad:(NSString *) propiedad {
    propiedad = [propiedad stringByReplacingOccurrencesOfString:@"," withString:@"."];
    NSArray* components = [propiedad componentsSeparatedByString:@":"];
    if(components.count==1)
        return ((NSString *)[components objectAtIndex:0]).floatValue;
    else {
        float v1 = ((NSString *)[components objectAtIndex:0]).floatValue;
        float v2 = ((NSString *)[components objectAtIndex:1]).floatValue;
        float aleatorio = (float)rand() / RAND_MAX;
        return (v2-v1)*aleatorio + v1;
    }
}

+ (int) intConPropiedad:(NSString *) propiedad {
    NSArray* components = [propiedad componentsSeparatedByString:@":"];
    if(components.count==1)
        return ((NSString *)[components objectAtIndex:0]).intValue;
    else {
        float v1 = ((NSString *)[components objectAtIndex:0]).intValue;
        float v2 = ((NSString *)[components objectAtIndex:1]).intValue;
        float aleatorio = (float)rand() / RAND_MAX;
        return (int)roundf((v2-v1)*aleatorio + v1);
    }
}

+ (SesionPoker *)sesionNum:(int)numSesion conEstado:(EstadoJuegoPoker *)estado {
    SesionPoker *sesion = [[SesionPoker alloc] init];
    sesion.cartas = [self intConPropiedad:estado.cartas];
    sesion.jugadores = [self intConPropiedad:estado.jugadores];
    
    NSMutableSet* setCategorias = [[NSMutableSet alloc] init];
    for (int c = 0; c<[self intConPropiedad:estado.categorias]; c++) {
        BOOL encontrado = NO;
        while (!encontrado) {
            CategoriaPoker randomCat = (CategoriaPoker) (arc4random() % (int) 4);
            if(![setCategorias containsObject:[NSNumber numberWithInt:randomCat]]) {
                [setCategorias addObject:[NSNumber numberWithInt:randomCat]];
                encontrado = YES;
            }
        }
    }    
    sesion.categorias = setCategorias;
    
    sesion.tie = [self floatConPropiedad:estado.tie];
    sesion.tresp = [self floatConPropiedad:estado.tresp];
    sesion.ayudas = [self intConPropiedad:estado.ayudas];
    sesion.tiempo_ayudas = [self floatConPropiedad:estado.tiempo_ayudas];
    sesion.p_dianas = [self intConPropiedad:estado.p_dianas];
    sesion.n_cambios = [self intConPropiedad:estado.n_cambios];

    return sesion;
}

- (NSInteger)tiempoTotal {
    return self.tie*self.cartas*self.jugadores;
}

@end
