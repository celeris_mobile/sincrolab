//
//  RespuestaPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "RespuestaPoker.h"

@implementation RespuestaPoker

+ (RespuestaPoker *) respuestaAleatoria {
    RespuestaPoker* respuesta = [[RespuestaPoker alloc] init];
    respuesta.tiempo = ((float)rand() / RAND_MAX)*3;
    respuesta.tipo = arc4random()%4;
    
    return respuesta;
}

@end
