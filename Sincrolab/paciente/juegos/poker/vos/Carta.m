//
//  Carta.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Carta.h"

@implementation Carta

+ (Carta *) cartaConCartaIgual:(Carta *) carta paraCategoria:(CategoriaPoker) categoria {
    Carta* cartaAux = [[Carta alloc] init];
    
    cartaAux.trama = arc4random()%4;
    cartaAux.forma = arc4random()%4;
    cartaAux.color = arc4random()%4;
    cartaAux.simbolo = arc4random()%4;
    
    switch (categoria) {
        case CategoriaPokerTrama:
            cartaAux.trama = carta.trama;
            break;
        case CategoriaPokerForma:
            cartaAux.forma = carta.forma;
            break;
        case CategoriaPokerColor:
            cartaAux.color = carta.color;
            break;
        case CategoriaPokerSimbolo:
            cartaAux.simbolo = carta.simbolo;
            break;
    }
    
    return cartaAux;
}

+ (Carta *) cartaConCartaDistinta:(Carta *) carta paraCategoria:(CategoriaPoker) categoria {
    Carta* cartaAux = [[Carta alloc] init];
    
    BOOL encontrado = NO;
    while (!encontrado) {
        cartaAux.trama = arc4random()%4;
        cartaAux.forma = arc4random()%4;
        cartaAux.color = arc4random()%4;
        cartaAux.simbolo = arc4random()%4;
        
        switch (categoria) {
            case CategoriaPokerTrama:
                if(cartaAux.trama != carta.trama)
                    encontrado = YES;
                break;
            case CategoriaPokerForma:
                if(cartaAux.forma != carta.forma)
                    encontrado = YES;
                break;
            case CategoriaPokerColor:
                if(cartaAux.color != carta.color)
                    encontrado = YES;
                break;
            case CategoriaPokerSimbolo:
                if(cartaAux.simbolo != carta.simbolo)
                    encontrado = YES;
                break;
        }
    }

    
    return cartaAux;
}

+ (Carta *) cartaAleatoria {
    Carta* cartaAux = [[Carta alloc] init];
    cartaAux.trama = arc4random()%4;
    cartaAux.forma = arc4random()%4;
    cartaAux.color = arc4random()%4;
    cartaAux.simbolo = arc4random()%4;
    
    return cartaAux;
}

+ (Carta *) cartaDistintaACartas:(NSArray *) cartas paraCategoria:(CategoriaPoker) categoria encontrada:(BOOL *) encontrada {
    
    Carta* cartaAux = [[Carta alloc] init];
    
    BOOL encontrado = NO;
    int intentos = 100;
    
    while (!encontrado) {
        cartaAux.trama = arc4random()%4;
        cartaAux.forma = arc4random()%4;
        cartaAux.color = arc4random()%4;
        cartaAux.simbolo = arc4random()%4;
        
        switch (categoria) {
            case CategoriaPokerTrama:
                encontrado = YES;
                for (Carta* carta in cartas) {
                    if(carta!=(id)[NSNull null]&&cartaAux.trama==carta.trama)
                        encontrado = NO;
                }
                break;
            case CategoriaPokerForma:
                encontrado = YES;
                for (Carta* carta in cartas) {
                    if(carta!=(id)[NSNull null]&&cartaAux.forma==carta.forma)
                        encontrado = NO;
                }
                break;
            case CategoriaPokerColor:
                encontrado = YES;
                for (Carta* carta in cartas) {
                    if(carta!=(id)[NSNull null]&&cartaAux.color==carta.color)
                        encontrado = NO;
                }
                break;
            case CategoriaPokerSimbolo:
                encontrado = YES;
                for (Carta* carta in cartas) {
                    if(carta!=(id)[NSNull null]&&cartaAux.simbolo==carta.simbolo)
                        encontrado = NO;
                }
                break;
        }
        
        intentos-=1;
        if(intentos<0) {
            *encontrada = NO;
            return cartaAux;
        }
    }
    
    *encontrada = YES;
    return cartaAux;
}

- (BOOL) coincideConCarta:(Carta *) carta enCategoria:(CategoriaPoker) categoria {
    switch (categoria) {
        case CategoriaPokerTrama:
            return self.trama == carta.trama;
            break;
        case CategoriaPokerForma:
            return self.forma == carta.forma;
            break;
        case CategoriaPokerColor:
            return self.color == carta.color;
            break;
        case CategoriaPokerSimbolo:
            return self.simbolo == carta.simbolo;
            break;
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"carta:t%df%dc%ds%d", self.trama, self.forma, self.color, self.simbolo];
}
@end
