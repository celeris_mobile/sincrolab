//
//  RespuestaInvasion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 26/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TipoRespuestaPoker) {
    TipoRespuestaPokerAcierto,
    TipoRespuestaPokerOmision,
    TipoRespuestaPokerComision,
    TipoRespuestaPokerNoComision,
};

@interface RespuestaPoker : NSObject
+ (RespuestaPoker *) respuestaAleatoria;
@property (nonatomic, assign) float tiempo;
@property (nonatomic, assign) TipoRespuestaPoker tipo;
@end
