//
//  Carta.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SesionPoker.h"

@interface Carta : NSObject
@property (nonatomic, assign) NSInteger forma;
@property (nonatomic, assign) NSInteger trama;
@property (nonatomic, assign) NSInteger color;
@property (nonatomic, assign) NSInteger simbolo;

+ (Carta *) cartaConCartaIgual:(Carta *) carta paraCategoria:(CategoriaPoker) categoria;
+ (Carta *) cartaConCartaDistinta:(Carta *) carta paraCategoria:(CategoriaPoker) categoria;
+ (Carta *) cartaAleatoria;
+ (Carta *) cartaDistintaACartas:(NSArray *) cartas paraCategoria:(CategoriaPoker) categoria encontrada:(BOOL *) encontrada;
- (BOOL) coincideConCarta:(Carta *) carta enCategoria:(CategoriaPoker) categoria;
@end
