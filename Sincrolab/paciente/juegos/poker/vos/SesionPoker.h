//
//  SesionInvasion.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EstadoJuegoPoker.h"

typedef NS_ENUM(NSInteger, CategoriaPoker) {
    CategoriaPokerTrama = 0,
    CategoriaPokerColor,
    CategoriaPokerForma,
    CategoriaPokerSimbolo
};

@interface SesionPoker : NSObject

+ (SesionPoker *) sesionNum:(int) numSesion conEstado:(EstadoJuegoPoker *) estado;
@property (nonatomic, assign) NSInteger cartas;
@property (nonatomic, assign) NSInteger jugadores;
@property (nonatomic, strong) NSSet* categorias;
@property (nonatomic, assign) double tie;
@property (nonatomic, assign) double tresp;
@property (nonatomic, assign) NSInteger ayudas;
@property (nonatomic, assign) double tiempo_ayudas;
@property (nonatomic, assign) NSInteger p_dianas;
@property (nonatomic, assign) NSInteger n_cambios;

@property (nonatomic, readonly) NSInteger tiempoTotal;

@end
