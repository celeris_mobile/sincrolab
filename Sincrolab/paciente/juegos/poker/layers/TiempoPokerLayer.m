//
//  TiempoPokerLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TiempoPokerLayer.h"
#import "ImageUtils.h"

@interface TiempoPokerLayer()
@property (nonatomic, strong) CCLabelTTF* tiempoLabel;
@property (nonatomic, strong) CCSprite* fondo;
@end

@implementation TiempoPokerLayer {
    float tiempo;
}

- (id)init {
    self = [super init];
    if(self) {
        
        CCTexture2D* texturaFondo = [[CCTextureCache sharedTextureCache] textureForKey:@"fondoTiempoPoker"];
        if(texturaFondo) {
            self.fondo = [CCSprite spriteWithTexture:texturaFondo];
        } else {
            self.fondo = [CCSprite spriteWithCGImage:[self generaTexturaFondo].CGImage key:@"fondoTiempoPoker"];
        }
        
        self.fondo.position = CGPointMake(1024-149, 768-93);
        [self addChild:self.fondo];
        
        self.tiempoLabel = [CCLabelTTF labelWithString:@"00:00" fontName:@"ChauPhilomeneOne-Regular" fontSize:75];
        self.tiempoLabel.position = self.fondo.position;

        [self addChild:self.tiempoLabel];
        
        self.position = CGPointMake(0, 200);
    }
    
    return self;
}

- (UIImage *) generaTexturaFondo {
    float ancho = 253;
    float alto = 100;
    
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho, alto)];
    fondo.layer.cornerRadius = 10;
    fondo.layer.borderColor = [UIColor colorWithRed:64/255.0 green:63/255.0 blue:58/255.0 alpha:1.0].CGColor;
    fondo.layer.borderWidth = 6;
    fondo.backgroundColor = [UIColor clearColor];
    fondo.opaque = NO;
    
    return [ImageUtils imageWithView:fondo];
}

- (void)setTiempoTotal:(float) tiempoTotal  {
    tiempo = tiempoTotal;//
    [self actualizaTiempo];
}

- (void) decrementa:(ccTime)delta {
    tiempo -= delta;
    [self actualizaTiempo];
}

- (void) actualizaTiempo {
    if(tiempo<0)
        [self.tiempoLabel setString:@"00:00"];

    int segundosTrascurridos = (int) tiempo;
    int minutos = (int) floor(segundosTrascurridos/60.0);
    int secs = tiempo-minutos*60;
    NSString* minutos_str = [NSString stringWithFormat:@"%d", minutos];
    NSString* segundos_str = [NSString stringWithFormat:@"%d", secs];
    minutos_str = minutos_str.length==2?minutos_str:[@"0" stringByAppendingString:minutos_str];
    segundos_str = segundos_str.length==2?segundos_str:[@"0" stringByAppendingString:segundos_str];
    [self.tiempoLabel setString:[NSString stringWithFormat:@"%@:%@", minutos_str, segundos_str]];
}

- (void) muestra {
    CCMoveTo* mueveIn = [CCMoveTo actionWithDuration:0.4 position:CGPointZero];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueveIn];
    [self runAction:ease];
}

- (void) oculta {
    CCFadeOut *mueveOut = [CCMoveTo actionWithDuration:0.4 position:CGPointMake(0, 200)];
    CCEaseExponentialIn* ease = [CCEaseExponentialIn actionWithAction:mueveOut];
    [self runAction:ease];
}
@end
