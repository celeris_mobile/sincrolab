//
//  Rombo.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Rombo.h"

@implementation Rombo

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    float polySize = rect.size.width/2-6;
    
    CGPoint center = CGPointMake(rect.size.width/2, rect.size.height/2);
    for(int i = 0; i < 4; ++i) {
        CGFloat x = polySize * sinf(i * 2.0 * M_PI / 4);
        CGFloat y = polySize * cosf(i * 2.0 * M_PI / 4);
        if(i==0)
            CGContextMoveToPoint(ctx, center.x + x, center.y + y);
        else
            CGContextAddLineToPoint(ctx, center.x + x, center.y + y);
    }
    
    CGContextClosePath(ctx);
    
    
    
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetFillColorWithColor(ctx, self.color.CGColor);
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 6);
    
    CGContextDrawPath(ctx, kCGPathEOFillStroke);
}

@end
