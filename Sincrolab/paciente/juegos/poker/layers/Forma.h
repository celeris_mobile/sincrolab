//
//  Forma.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Forma : UIView
@property (nonatomic, strong) UIColor* color;
@end
