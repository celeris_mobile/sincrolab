//
//  MontonLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "MontonLayer.h"
#import "CartaPoker.h"
#import "Carta.h"

@interface MontonLayer()
@property (nonatomic, strong) CCSprite* monton;
@property (nonatomic, strong) CartaPoker* spriteCarta;
@property (nonatomic, strong) CartaPoker* cartaABorrar;
@end

@implementation MontonLayer

- (id) init {
    self = [super init];
    if(self) {
        self.monton = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"monton.png"].CGImage key:@"montonCartas"];
        self.monton.zOrder = 2;
        [self addChild:self.monton];
        
        [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self
                                                                priority:0
                                                         swallowsTouches:NO];
    }
    return self;
}

- (void)setCarta:(Carta *)carta {
    _carta = carta;
    if(carta!=nil) {
        CartaPoker* spriteCarta = [[CartaPoker alloc] initWithCarta:carta];
        spriteCarta.zOrder = -10;
        spriteCarta.delegate = self;
        spriteCarta.position = CGPointMake(-40, 20);
        [self addChild:spriteCarta];

        self.cartaABorrar = self.spriteCarta;
        self.spriteCarta = spriteCarta;
        
        [spriteCarta voltea:self.position completado:^{
        }];
        
        [self.cartaABorrar performSelector:@selector(borra) withObject:nil afterDelay:0.9];
    } else {
        if(self.spriteCarta) {
            [self.spriteCarta stopAllActions];
            CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.6 position:CGPointMake(0, 900)];
            CCRotateBy* rota = [CCRotateBy actionWithDuration:0.6 angle:((float)rand() / RAND_MAX)*360];
            CCSpawn* combina = [CCSpawn actions:mueve, rota, nil];
            CCEaseExponentialInOut* ease = [CCEaseExponentialInOut actionWithAction:combina];
            CCCallBlock* block = [CCCallBlock actionWithBlock:^{
                self.spriteCarta.delegate = nil;
                [self.spriteCarta removeFromParent];
                self.spriteCarta = nil;
            }];
            CCSequence* secuencia = [CCSequence actions:ease, block, nil];
            [self.spriteCarta runAction:secuencia];
        }
    }
}
/*
- (void)cartaVolteada:(CartaPoker *)cartaV {
    if(self.cartaABorrar) {
        self.cartaABorrar.delegate = nil;
        [self.cartaABorrar removeFromParent];
        self.cartaABorrar = nil;
    }
}
*/
- (void)cartaVisible:(CartaPoker *)carta {
    [self.delegate cartaVisible:self];
}

- (void)tiembla {
    CCRotateTo* rotate1 = [CCRotateTo actionWithDuration:0.05 angle:10];
    CCRotateTo* rotate2 = [CCRotateTo actionWithDuration:0.05 angle:-10];
    CCSequence* combina = [CCSequence actions:rotate1, rotate2, nil];
    CCRepeat* repite = [CCRepeat actionWithAction:combina times:5];
    CCRotateTo* rotate = [CCRotateTo actionWithDuration:0.05 angle:0];
    CCSequence* secuencia = [CCSequence actions:repite, rotate, nil];
    [self.spriteCarta runAction:secuencia];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    if(!self.visible)
        return NO;
    
    CGPoint location = [self convertTouchToNodeSpace: touch];
    
    
    BOOL isTouchHandled = CGRectContainsPoint(self.monton.boundingBox, location);
    if (isTouchHandled) {
        [self.delegate montonDado:self];
    }
    
    return isTouchHandled;
}


@end
