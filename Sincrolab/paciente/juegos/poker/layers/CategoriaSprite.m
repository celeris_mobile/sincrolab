//
//  CategoriaSprite.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CategoriaSprite.h"

@interface CategoriaSprite()
@property (nonatomic, strong) CCSprite* spriteOn;
@property (nonatomic, strong) CCSprite* spriteOff;
@end
@implementation CategoriaSprite

- (id) initWithNombre:(NSString *) nombre {
    self = [super init];
    if (self) {
        self.spriteOn = [CCSprite spriteWithCGImage:
                         [UIImage imageNamed:[NSString stringWithFormat:@"c%@_on.png", nombre]].CGImage
                                                key:[NSString stringWithFormat:@"%@on",nombre]];
        self.spriteOff = [CCSprite spriteWithCGImage:
                         [UIImage imageNamed:[NSString stringWithFormat:@"c%@_off.png", nombre]].CGImage
                                                key:[NSString stringWithFormat:@"%@off",nombre]];
        
        [self addChild:self.spriteOn];
        [self addChild:self.spriteOff];
        
        self.spriteOn.visible = NO;
        
    }
    return self;
}

- (void) activa {
    self.spriteOn.visible = YES;
    self.spriteOff.visible = NO;
}

- (void) desactiva {
    self.spriteOn.visible = NO;
    self.spriteOff.visible = YES;
}

@end
