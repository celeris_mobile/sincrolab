//
//  CategoriasLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CategoriasLayer.h"
#import "CategoriaSprite.h"

@interface CategoriasLayer()
@property (nonatomic, strong) CategoriaSprite* trama;
@property (nonatomic, strong) CategoriaSprite* forma;
@property (nonatomic, strong) CategoriaSprite* color;
@property (nonatomic, strong) CategoriaSprite* simbolo;
@property (nonatomic, strong) CategoriaSprite* oculta;

@property (nonatomic, strong) CategoriaSprite* seleccionada;
@property (nonatomic, strong) CCLabelTTF* label;
@end

@implementation CategoriasLayer

- (id)init {
    self = [super init];
    if(self) {
        [self configura];
    }
    return self;
}

- (void) configura {
    self.trama = [[CategoriaSprite alloc] initWithNombre:@"trama"];
    self.trama.position = CGPointMake(70, 768-70);
    [self addChild:self.trama];
    
    self.forma = [[CategoriaSprite alloc] initWithNombre:@"forma"];
    self.forma.position = CGPointMake(140, 768-70);
    [self addChild:self.forma];
    
    self.color = [[CategoriaSprite alloc] initWithNombre:@"color"];
    self.color.position = CGPointMake(210, 768-70);
    [self addChild:self.color];

    self.simbolo = [[CategoriaSprite alloc] initWithNombre:@"simbolo"];
    self.simbolo.position = CGPointMake(280, 768-70);
    [self addChild:self.simbolo];

    self.oculta = [[CategoriaSprite alloc] initWithNombre:@"oculta"];
    self.oculta.position = CGPointMake(350, 768-70);
    [self addChild:self.oculta];

    self.label = [CCLabelTTF labelWithString:@"Categoría" fontName:@"ChauPhilomeneOne-Regular" fontSize:33];
    self.label.position = CGPointMake(201, 768-126);
    [self addChild:self.label];

}

- (void)setCategoria:(CategoriaPoker)categoria {
    _categoria = categoria;
    
    if(self.seleccionada)
       [self.seleccionada desactiva];
    
    switch (categoria) {
        case CategoriaPokerTrama:
            self.seleccionada = self.trama;
            [self.label setString:@"Trama de fondo"];
            break;
        case CategoriaPokerForma:
            self.seleccionada = self.forma;
            [self.label setString:@"Forma geométrica"];
            break;
        case CategoriaPokerColor:
            self.seleccionada = self.color;
            [self.label setString:@"Color de la forma"];
            break;
        case CategoriaPokerSimbolo:
            self.seleccionada = self.simbolo;
            [self.label setString:@"Símbolo interior"];
            break;
    }
    
    if(!self.ayuda) {
        self.seleccionada = self.oculta;
        [self.label setString:@"Desconocida"];
    }
    
    [self.seleccionada activa];
}

- (void) setAyuda:(BOOL)ayuda {
    _ayuda = ayuda;
    
    self.categoria = _categoria;
}

- (void)desaparece {
    for (int c = 0; c<self.children.count; c++) {
        CCSprite* child = [self.children objectAtIndex:c];
        CCDelayTime* delay = [CCDelayTime actionWithDuration:0.2*c];
        CCMoveBy* mueve = [CCMoveBy actionWithDuration:0.3 position:CGPointMake(0, 300)];
        CCSequence* secuencia = [CCSequence actions:delay, mueve, nil];
        [child runAction:secuencia];
    }
}
@end
