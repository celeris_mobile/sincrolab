//
//  Hexagono.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Hexagono.h"

@implementation Hexagono

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    float polySize = rect.size.width/2-6;

    CGPoint center = CGPointMake(rect.size.width/2, rect.size.height/2);
    for(int i = 0; i < 6; ++i) {
        CGFloat x = polySize * cosf(i * 2.0 * M_PI / 6);
        CGFloat y = polySize * sinf(i * 2.0 * M_PI / 6);
        if(i==0)
            CGContextMoveToPoint(ctx, center.x + x, center.y + y);
        else
            CGContextAddLineToPoint(ctx, center.x + x, center.y + y);
    }
    
    CGContextClosePath(ctx);
    

    
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetFillColorWithColor(ctx, self.color.CGColor);
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 6);
    
    CGContextDrawPath(ctx, kCGPathEOFillStroke);
}

@end
