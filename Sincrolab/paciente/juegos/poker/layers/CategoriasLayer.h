//
//  CategoriasLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionPoker.h"

@interface CategoriasLayer : CCLayer
@property (nonatomic, assign) CategoriaPoker categoria;
@property (nonatomic, assign) BOOL ayuda;
- (void) desaparece;
@end
