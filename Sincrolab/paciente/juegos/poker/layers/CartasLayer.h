//
//  Cartas.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionPoker.h"
#import "MontonLayer.h"

@protocol CartasDelegate <NSObject>
- (void) dadaCartaEnTrial:(NSInteger)numTrial conAcierto:(BOOL) acierto;
- (void) cambiaTrialADiana:(NSInteger) numTrial;
@end

@interface CartasLayer : CCNode <MontonLayerDelegate>
@property (nonatomic, weak) id<CartasDelegate> delegate;
@property (nonatomic, strong) SesionPoker* sesion;

- (void) muestra;
- (void) lanzaEstimulo:(int) numTrial conDiana:(NSNumber *) diana conCategoria:(CategoriaPoker) categoria;
@end
