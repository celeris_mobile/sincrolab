//
//  CategoriaSprite.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"

@interface CategoriaSprite : CCNode
- (id) initWithNombre:(NSString *) nombre;
- (void) activa;
- (void) desactiva;
@end
