//
//  FondoLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "FondoPoker.h"

@interface FondoPoker()
@property (nonatomic, strong) CCSprite* fondo;
@end

@implementation FondoPoker

- (id)init {
    self = [super init];
    if(self) {
    //    self.fondo = [CCSprite spriteWithFile:@"fondo2j.jpg"];
    //    self.fondo.anchorPoint = CGPointZero;
     //   [self addChild:self.fondo];
    }
    
    return self;
}

- (void)setSesion:(SesionPoker *)sesion {
    if(!_sesion||_sesion.jugadores!=sesion.jugadores)
        [self cambiaFondo:sesion.jugadores];
    
    _sesion = sesion;
}

- (void) cambiaFondo:(int)numJugadores {
    CCSprite* aBorrar;
    if(self.fondo)
        aBorrar = self.fondo;
    
    CCSprite* fondo = [CCSprite spriteWithFile:[NSString stringWithFormat:@"fondo%dj.jpg", numJugadores]];
    
    fondo.anchorPoint = CGPointZero;
    self.fondo = fondo;
    self.fondo.opacity = 0.0;
    [self addChild:self.fondo];
    
    CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:0.5];
    CCCallBlock* block = [CCCallBlock actionWithBlock:^{
        if(aBorrar)
            [aBorrar removeFromParent];
    }];
    CCSequence* secuencia = [CCSequence actions:fadeIn, block, nil];
    [self.fondo runAction:secuencia];    
}


@end
