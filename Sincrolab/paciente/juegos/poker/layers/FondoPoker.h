//
//  FondoLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "SesionPoker.h"

@interface FondoPoker : CCLayer
@property (nonatomic, weak) SesionPoker* sesion;
@end
