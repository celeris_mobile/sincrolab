//
//  Triangulo.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 31/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Triangulo.h"

@implementation Triangulo

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();

    CGContextMoveToPoint(ctx, 6, 25);
    CGContextAddLineToPoint(ctx, rect.size.width-12, 25);
    CGContextAddLineToPoint(ctx, rect.size.width/2, rect.size.height-12);
    CGContextClosePath(ctx);
    
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetFillColorWithColor(ctx, self.color.CGColor);
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 6);
    
    CGContextDrawPath(ctx, kCGPathEOFillStroke);
}


@end
