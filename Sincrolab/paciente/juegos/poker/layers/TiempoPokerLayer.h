//
//  TiempoPokerLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 21/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"

@interface TiempoPokerLayer : CCLayer
- (void)setTiempoTotal:(float) tiempoTotal;
- (void) decrementa:(ccTime) delta;

- (void) muestra;
- (void) oculta;
@end
