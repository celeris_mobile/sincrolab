//
//  MontonLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "Carta.h"
#import "CartaPoker.h"

@class MontonLayer;

@protocol  MontonLayerDelegate <NSObject>
- (void) montonDado:(MontonLayer *) monton;
- (void) cartaVisible:(MontonLayer *) monton;
@end


@interface MontonLayer : CCNode<CCTouchOneByOneDelegate, CartaPokerDelegate>
@property (nonatomic, weak) id<MontonLayerDelegate> delegate;
@property (nonatomic, strong) Carta* carta;
- (void) tiembla;
@end
