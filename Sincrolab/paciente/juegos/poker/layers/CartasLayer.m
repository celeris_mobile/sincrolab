//
//  Cartas.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 03/02/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CartasLayer.h"
#import "MontonLayer.h"
#import "Carta.h"

static CGPoint posiciones2[2] =
{
    (CGPoint){370, 768-431},
    (CGPoint){696, 768-431}
};
static CGPoint posiciones3[3] =
{
    (CGPoint){546, 768-441},
    (CGPoint){868, 768-459},
    (CGPoint){197, 768-459}
};
static CGPoint posiciones4[4] =
{
    (CGPoint){542, 768-625},
    (CGPoint){890, 768-588},
    (CGPoint){542, 768-292},
    (CGPoint){203, 768-588},
};
static CGPoint posiciones5[5] =
{
    (CGPoint){542, 768-615},
    (CGPoint){861, 768-612},
    (CGPoint){749, 768-298},
    (CGPoint){305, 768-298},
    (CGPoint){197, 768-612}
};

@interface CartasLayer()
@property (nonatomic, strong) NSArray* montones;
@property (nonatomic, strong) MontonLayer* montonVolteado;
@end

@implementation CartasLayer {
    int indiceMonton;
    int trialActual;
    BOOL cartaDada;
    CategoriaPoker categoriaActual;
}

- (id)init {
    self = [super init];
    if(self) {
        
    }
    
    return self;
}

- (void)setSesion:(SesionPoker *)sesion {
    _sesion = sesion;
    
    indiceMonton = 0;
    trialActual = 0;
    
    if(self.montones) {
        for (MontonLayer* monton in self.montones) {
            [monton removeFromParent];
        }
        self.montones = nil;
    }
    
    NSMutableArray* auxmontones = [[NSMutableArray alloc] init];
    for (int s = 0; s<self.sesion.jugadores; s++) {
        MontonLayer* monton = [[MontonLayer alloc] init];
      
        if(s>0)
            monton.scale = 0.8;
        
        switch (self.sesion.jugadores) {
            case 2:
                monton.position = posiciones2[s];
                break;
            case 3:
                monton.position = posiciones3[s];
                break;
            case 4:
                monton.position = posiciones4[s];
                break;
            case 5:
                monton.position = posiciones5[s];
                break;
            default:
                break;
        }
        
        [self addChild:monton];
        monton.delegate = self;
        [auxmontones addObject:monton];
        
    }
    
    self.montones = auxmontones.copy;
    
}

- (void) muestra {
    
}

- (void) lanzaEstimulo:(int) numTrial conDiana:(NSNumber *) diana conCategoria:(CategoriaPoker) categoria {
    categoriaActual = categoria;
    trialActual = numTrial;
    cartaDada = NO;
    
    MontonLayer* monton;
    int indice = 0;
    for (MontonLayer* montonAux in self.montones) {
        if(!montonAux.carta) {
            monton = montonAux;
            break;
        }
        indice+=1;
    }
    
    if(!monton) {
        monton = [self.montones objectAtIndex:indiceMonton];
        indiceMonton+=1;
    } else {
        indiceMonton = indice+1;
    }
    indiceMonton=indiceMonton<self.montones.count?indiceMonton:0;
    
    MontonLayer* montonUsuario = [self.montones objectAtIndex:0];
    
    monton.zOrder = 3;
    
    Carta* carta;
    if(numTrial==0) {
        carta = [Carta cartaAleatoria];
    } else {
        if(diana.boolValue) {
            if(monton!=montonUsuario) {
                carta = [Carta cartaConCartaIgual:montonUsuario.carta paraCategoria:categoria];
            } else {
                MontonLayer* montonAux = [self.montones objectAtIndex:arc4random()%(self.montones.count-1)+1];
                carta = [Carta cartaConCartaIgual:montonAux.carta paraCategoria:categoria];
            }
        } else {
            if(monton!=montonUsuario) {
                carta = [Carta cartaConCartaDistinta:montonUsuario.carta paraCategoria:categoria];
            } else {
                NSArray* cartas = [self.montones valueForKeyPath:@"carta"];
                cartas = [cartas subarrayWithRange:NSMakeRange(1, cartas.count-2)];
                BOOL encontrada;
                carta = [Carta cartaDistintaACartas:cartas paraCategoria:categoria encontrada:&encontrada];
                if(!encontrada) {
                    [self.delegate cambiaTrialADiana:numTrial];
                }
            }
        }
    }
    
    monton.carta = carta;
}

- (void) despejaMontonesConCategoria:(NSNumber *) categoriaNumber {
    CategoriaPoker categoria = categoriaNumber.intValue;
    MontonLayer* montonUsuario = [self.montones objectAtIndex:0];
    Carta* cartaUsuario = montonUsuario.carta;
    
    for (int m = 1; m<self.montones.count; m++) {
        MontonLayer* monton = [self.montones objectAtIndex:m];
        Carta* carta = monton.carta;
        if(carta!=(id)[NSNull null]&&[carta coincideConCarta:cartaUsuario enCategoria:categoria])
            monton.carta = nil;
    }
}

#pragma mark MontonLayerDelegate
- (void)cartaVisible:(MontonLayer *)monton {
    [self performSelector:@selector(despejaMontonesConCategoria:) withObject:[NSNumber numberWithInt:categoriaActual] afterDelay:self.sesion.tresp];
}

- (void)montonDado:(MontonLayer *)monton {
    MontonLayer* montonUsuario = [self.montones objectAtIndex:0];
    if(monton!=montonUsuario&&!cartaDada) {
        cartaDada = YES;
        BOOL acierto;
        if([monton.carta coincideConCarta:montonUsuario.carta enCategoria:categoriaActual]) {
            acierto = YES;
            monton.carta = nil;
        } else {
            acierto = NO;
            [monton tiembla];
        }
        
        [self.delegate dadaCartaEnTrial:trialActual conAcierto:acierto];
    }
}

@end
