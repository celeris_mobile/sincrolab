//
//  InstruccionPokerLayer.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "InstruccionPokerLayer.h"
#import "ImageUtils.h"

@interface InstruccionPokerLayer()
@property (nonatomic, strong) CCSprite* fondo;
@property (nonatomic, strong) CCLabelTTF* texto;
@property (nonatomic, strong) CCSprite* boton;
@end

@implementation InstruccionPokerLayer

- (id)init {
    self = [super init];
    if(self) {
        NSString* imageKey = @"fondoInstrucciones";
        
        CCTexture2D* texturaFondo = [[CCTextureCache sharedTextureCache] textureForKey:imageKey];
        if(texturaFondo) {
            self.fondo = [CCSprite spriteWithTexture:texturaFondo];
        } else {
            self.fondo = [CCSprite spriteWithCGImage:[self generaTexturaFondo].CGImage key:imageKey];
        }
        
        self.fondo.position = CGPointMake(1024/2, 768-self.fondo.contentSize.height/2-40);
        [self addChild:self.fondo];
        
        CGSize labelSize = CGSizeMake(self.fondo.contentSize.width-80, self.fondo.contentSize.height-60);
        self.texto = [CCLabelTTF labelWithString:@"" fontName:@"ChauPhilomeneOne-Regular" fontSize:22 dimensions:labelSize hAlignment:kCCTextAlignmentCenter];
        self.texto.position = CGPointMake(self.fondo.position.x, self.fondo.position.y);
        [self addChild:self.texto];
        
        self.boton = [CCSprite spriteWithFile:@"btn_siguiente.png"];
        self.boton.position = CGPointMake(self.fondo.position.x+self.fondo.contentSize.width/2+82, self.fondo.position.y);
        [self addChild:self.boton];
        
        self.visible = NO;
        
        [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self
                                                                priority:0
                                                         swallowsTouches:NO];
        
    }
    
    return self;
}


- (UIImage *) generaTexturaFondo {
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 604, 179)];
    fondo.backgroundColor = [UIColor colorWithRed:0.23 green:0.2 blue:0.18 alpha:1];
    fondo.layer.cornerRadius = 10.0;
    fondo.opaque = NO;
    
    return [ImageUtils imageWithView:fondo];
}

- (void) muestraConTexto:(NSString *) texto tipoBoton:(TipoBoton) tipoBoton {
    if(tipoBoton==TipoBotonNinguno) {
        self.boton.visible = NO;
    } else {
        self.boton.visible = YES;
        if(tipoBoton==TipoBotonTerminar) {
            CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:@"btn_terminar.png"];
            [self.boton setTexture: tex];
        } else {
            CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:@"btn_siguiente.png"];
            [self.boton setTexture: tex];
        }
    }
    
    [self.texto setString:texto];
    
    self.position = CGPointMake(1024/2, 0);
    self.visible = YES;
    
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.3 position:CGPointZero];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
    [self runAction:ease];
    
    self.fondo.opacity = 0.0;
    self.texto.opacity = 0.0;
    CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.3];
    [self.fondo runAction:fadeIn];
    fadeIn = [CCFadeIn actionWithDuration:0.3];
    [self.texto runAction:fadeIn];
    
    
    self.boton.opacity = 0.0;
    self.boton.position = CGPointMake(self.fondo.position.x+self.fondo.contentSize.width/2, self.fondo.position.y);
    CCMoveTo* mueveBoton = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(self.fondo.position.x+self.fondo.contentSize.width/2+82, self.fondo.position.y)];
    CCEaseExponentialOut* easeBoton = [CCEaseExponentialOut actionWithAction:mueveBoton];
    CCFadeIn* fadeInBoton = [CCFadeIn actionWithDuration:0.3];
    
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.3];
    CCSpawn* spawn = [CCSpawn actions:easeBoton, fadeInBoton, nil];
    CCSequence* sequence = [CCSequence actions:delay, spawn, nil];
    
    [self.boton runAction:sequence];
}

- (void) oculta {
    
    self.fondo.color = ccc3(255.0, 255.0, 255.0);
    
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.2 position:CGPointMake(-1024/2, 0)];
    CCEaseExponentialIn* ease = [CCEaseExponentialIn actionWithAction:mueve];
    CCCallFunc* callback = [CCCallFunc actionWithTarget:self selector:@selector(ocultado)];
    CCSequence* sequence = [CCSequence actions:ease, callback, nil];
    [self runAction:sequence];
    
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:0.2];
    [self.fondo runAction:fadeOut];
    fadeOut = [CCFadeOut actionWithDuration:0.2];
    [self.texto runAction:fadeOut];
    
}

- (void) ocultado {
    self.visible = NO;
    [self runAction:[CCStopGrid action]];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint location = [self convertTouchToNodeSpace: touch];
    
    if(!self.boton.visible)
        return NO;
    
    BOOL isTouchHandled = CGRectContainsPoint(self.boton.boundingBox, location);
    if (isTouchHandled) {
        self.boton.color = ccc3(150.0, 150.0, 150.0);
    }
    return isTouchHandled;
}

- (void)ccTouchEnded:(UITouch*)touch withEvent:(UIEvent *)event {
    if(!self.boton.visible)
        return;
    
    self.boton.color = ccc3(255.0, 255.0, 255.0);
    [self.delegate botonSiguienteDado:self];
}



@end