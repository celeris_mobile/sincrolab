//
//  InstruccionPokerLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"

typedef NS_ENUM(NSInteger, TipoBoton) {
    TipoBotonNinguno,
    TipoBotonSiguiente,
    TipoBotonTerminar
};

@class InstruccionPokerLayer;

@protocol InstruccionPokerLayerDelegate <NSObject>
- (void) botonSiguienteDado:(InstruccionPokerLayer *) instrucciones;
@end

@interface InstruccionPokerLayer : CCLayer
@property (nonatomic, weak) id<InstruccionPokerLayerDelegate> delegate;
- (void) muestraConTexto:(NSString *) texto tipoBoton:(TipoBoton) tipoBoton;
- (void) oculta;
@end
