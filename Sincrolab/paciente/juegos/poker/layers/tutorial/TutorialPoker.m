//
//  TutorialPoker.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TutorialPoker.h"

@interface TutorialPoker()
@property (nonatomic, strong) InstruccionPokerLayer* instrucciones;
@property (nonatomic, strong) CartasLayer* cartas;
@property (nonatomic, strong) CategoriasLayer* categorias;
@property (nonatomic, strong) NSArray* mensajes;
@property (nonatomic, strong) CCSprite* flecha;
@end

@implementation TutorialPoker {
    BOOL isInDemo;
    NSInteger indice_mensaje;
    NSInteger cartas_reales;
    NSInteger numEstimulo;
}

- (id) initWithCartas:(CartasLayer *) cartas andCategorias:(CategoriasLayer *) categorias {
    self = [super init];
    if(self) {
        self.cartas = cartas;
        self.categorias = categorias;
        isInDemo = NO;
        
        indice_mensaje = 0;
        
        self.flecha = [CCSprite spriteWithFile:@"flecha.png"];
        self.flecha.opacity = 0.0;
        [self addChild:self.flecha];
    }
    return self;
}

- (void)setSesion:(SesionPoker *)sesion {
    _sesion = sesion;
    cartas_reales = self.sesion.cartas;
    self.sesion.cartas = 500;
    numEstimulo = 1;
    
    self.mensajes = @[@"Como buen samurai, no solo tienes que estar muy concentrado, además tienes que pensar rápido, para poder anticiparte a tus adversarios",
                      @"Prepárate para el entrenamiento, concéntrate en tu carta, busca la que sea como la tuya y tócala lo más rápido que puedas.",
                      @"Las cartas contienen elementos que corresponden a diferentes categorías.",
                      @"[carta]",
                      @"Compara tu carta con las demás cartas que haya encima de la mesa, en esa categoría.",
                      @"[compara]",
                      @"Deberás ser el más rápido en apretar el botón, si tardas mucho los ninjas te irán quitando tu poder de concentración. Ánimo, fuerza y concentración."
                      ];
    
    
    self.instrucciones = [[InstruccionPokerLayer alloc] init];
    self.instrucciones.delegate = self;
    [self addChild:self.instrucciones];
    
    self.cartas.sesion = self.sesion;
}

- (void) muestra {
    [self.cartas muestra];
    self.categorias.ayuda = YES;
    
    [self muestraMensaje];
}

- (void) muestraMensaje {
    
    NSString* mensaje = [self.mensajes objectAtIndex:indice_mensaje];
    isInDemo = NO;
    
    if([mensaje isEqualToString:@"[carta]"]) {
        indice_mensaje+=1;
        [self.cartas lanzaEstimulo:0 conDiana:[NSNumber numberWithBool:NO] conCategoria:CategoriaPokerForma];
        [self performSelector:@selector(muestraCategoria:) withObject:[NSNumber numberWithInt:0] afterDelay:1.0];
        return;
    }
    if([mensaje isEqualToString:@"[compara]"]) {
        indice_mensaje+=1;
        self.categorias.categoria = arc4random()%4;
        [self lanzaEstimulo];
        [self schedule:@selector(lanzaEstimulo) interval:self.sesion.tie];
        isInDemo = YES;
        return;
    }
    
    indice_mensaje+=1;
    
    if(indice_mensaje>=self.mensajes.count)
        [self.instrucciones muestraConTexto:mensaje tipoBoton:TipoBotonTerminar];
    else
        [self.instrucciones muestraConTexto:mensaje tipoBoton:TipoBotonSiguiente];
}

- (void) muestraCategoria:(NSNumber *) cat {
    if(cat.intValue>=4) {
        [self performSelector:@selector(muestraMensaje) withObject:nil afterDelay:1.0];
    } else {
        NSArray* cats = @[@4, @2, @1, @3];
        self.categorias.categoria = ((NSNumber *)[cats objectAtIndex:cat.intValue]).intValue;
        [self performSelector:@selector(muestraCategoria:) withObject:[NSNumber numberWithInt:cat.intValue+1] afterDelay:2.0];
    }
}

- (void) lanzaEstimulo {
    BOOL diana = NO;
    float aleatorio = (float)rand() / RAND_MAX;
    if(aleatorio>0.75)
        diana = YES;
    
    [self.cartas lanzaEstimulo:numEstimulo conDiana:[NSNumber numberWithBool:diana] conCategoria:self.categorias.categoria];
    numEstimulo+=1;
    
    if(isInDemo) {
        if(diana) {
            
            self.flecha.position = CGPointMake(725, 450);
            
            CCDelayTime* delay = [CCDelayTime actionWithDuration:0.6];
            CCFadeIn* fadeIn = [CCFadeIn actionWithDuration:0.3];
            CCBlink *parpadea = [CCBlink actionWithDuration:1.0 blinks:3];
            CCFadeOut *fadeOut = [CCFadeOut actionWithDuration:0.3];
            CCSequence* secuencia = [CCSequence actions:delay, fadeIn, parpadea, fadeOut, nil];
            [self.flecha runAction:secuencia];
        }
    }
}

- (void) acierto {
    if(isInDemo) {
        [self unschedule:@selector(lanzaEstimulo)];
        [self performSelector:@selector(muestraMensaje) withObject:nil afterDelay:1.0];
    }
}

- (void) error {

}

- (void) botonSiguienteDado:(InstruccionPokerLayer *)instrucciones {
    [self.instrucciones oculta];
    if(indice_mensaje>=self.mensajes.count) {
        [self unscheduleAllSelectors];
        [self unscheduleUpdate];
        self.sesion.cartas = cartas_reales;
        
        [self.delegate tutorialFinalizado];
    } else
        [self performSelector:@selector(muestraMensaje) withObject:nil afterDelay:0.5];
}

@end