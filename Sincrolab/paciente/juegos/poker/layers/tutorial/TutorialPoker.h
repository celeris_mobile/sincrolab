//
//  TutorialPoker.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/03/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CCLayer.h"
#import "InstruccionPokerLayer.h"
#import "SesionPoker.h"
#import "CartasLayer.h"
#import "CategoriasLayer.h"

@protocol TutorialPokerDelegate <NSObject>
- (void) tutorialFinalizado;
@end

@interface TutorialPoker : CCLayer <InstruccionPokerLayerDelegate>
@property (nonatomic, strong) SesionPoker* sesion;
@property (nonatomic, weak) id<TutorialPokerDelegate> delegate;
- (id) initWithCartas:(CartasLayer *) gongs andCategorias:(CategoriasLayer *) categorias;
- (void) muestra;
- (void) acierto;
- (void) error;
@end

