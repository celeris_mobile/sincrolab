//
//  ResultadoPartidaPoker.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Paciente.h"
#import "ResultadoPartidaPokerLayer.h"
#import "GraficaLineas.h"
#import "ImageUtils.h"
#import "UIColor+Extensions.h"
#import "BotonParametros.h"

@interface ResultadoPartidaPokerLayer ()
@property (nonatomic, strong) CCSprite* fondo;

@property (nonatomic, strong) GraficaLineas* graficaAciertos;
@property (nonatomic, strong) GraficaLineas* graficaTiempos;

@property (nonatomic, strong) ResultadoSesionPoker* resultadoSesion;
@property (nonatomic, strong) NSNumber* cambioNivel;
@property (nonatomic, strong) Paciente* paciente;

@property (nonatomic, strong) NSArray* mensajes;

@end

@implementation ResultadoPartidaPokerLayer

- (id) initWithResultadoSesion:(ResultadoSesionPoker *) resultadoSesion paciente:(Paciente *) paciente andCambioNivel:(NSNumber *) cambioNivel {
    
    self = [super init];
    if(self) {
        self.paciente = paciente;
        self.resultadoSesion = resultadoSesion;
        self.cambioNivel = cambioNivel;
        
        NSArray* mensajesPasa = @[@"¡Bravo lo conseguiste! Vamos con un nuevo reto",
                                  @"¡Genial! ¿Crees que puedes con el siguiente nivel...",
                                  @"¡Estas en el camino del samurai! ¡Sigue así!",
                                  @"Sigue así, tu concentración es muy buena",
                                  @"El camino del samurai es dificll pero eres genial!"];
        NSArray* mensajesNoPasa = @[@"¡El esfuerzo siempre tiene recompensa!",
                                    @"Hoy no lo has conseguido pero mañana ¡seguro!",
                                    @"Ánimo lo conseguiras!",
                                    @"Busca trucos que te ayuden a concentrarte",
                                    @"Si te pierdes, habla contigo mismo...EN VOZ ALTA!",
                                    @"¡No te rindas!"];
        if(cambioNivel.intValue>0)
            self.mensajes = mensajesPasa;
        else
            self.mensajes = mensajesNoPasa;
        
        [self setup];
    }
    
    return self;
}

- (void)muestra {
    [super muestra];
    
    
    CCSprite* personaje = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"mensaje.png"].CGImage key:@"personajeMensaje"];
    personaje.position = CGPointMake(104, 125);
    [self addChild:personaje];
    
    CCSprite* bocadillo = [CCSprite spriteWithCGImage:[UIImage imageNamed:@"bocadillo.png"].CGImage key:@"bocadilloPersonaje"];
    bocadillo.position = CGPointMake(155, 457);
    [self addChild:bocadillo];
    
    
    NSString* mensaje = [self.mensajes objectAtIndex:arc4random()%self.mensajes.count];
    CCLabelTTF *texto = [CCLabelTTF labelWithString:mensaje
                                           fontName:@"ChauPhilomeneOne-Italic" fontSize:20 dimensions:CGSizeMake(179, 155) hAlignment:kCCTextAlignmentCenter lineBreakMode:kCCLineBreakModeWordWrap];
    texto.verticalAlignment=kCCVerticalTextAlignmentCenter;
    texto.color = ccc3(255,255,255);
    texto.position = ccp(155, 483);
    [self addChild:texto z:1+10];
    
    [texto setString:mensaje];
    
    bocadillo.visible = NO;
    texto.visible = NO;
    
    personaje.position = CGPointMake(104, -300);
    CCDelayTime* delay = [CCDelayTime actionWithDuration:0.6];
    CCMoveTo* mueve = [CCMoveTo actionWithDuration:0.3 position:CGPointMake(103, 125)];
    CCEaseExponentialOut* ease = [CCEaseExponentialOut actionWithAction:mueve];
    CCCallBlock* callBlock = [CCCallBlock actionWithBlock:^{
        bocadillo.visible = YES;
        texto.visible = YES;
    }];
    
    CCSequence* secuencia = [CCSequence actions:delay, ease, callBlock, nil];
    [personaje runAction:secuencia];
    
    
}

- (void) setup {
    NSString* imageKey = [NSString stringWithFormat:@"fondoResultadosPartida"];
    
    CCTexture2D* texturaFondo = [[CCTextureCache sharedTextureCache] textureForKey:imageKey];
    if(texturaFondo) {
        self.fondo = [CCSprite spriteWithTexture:texturaFondo];
    } else {
        self.fondo = [CCSprite spriteWithCGImage:[self generaTexturaFondo].CGImage key:imageKey];
    }
    
    self.fondo.position = CGPointMake(1024/2, 768/2+50);
    [self addChild:self.fondo];
    
    
    CGPoint origen = CGPointMake(self.fondo.position.x-self.fondo.contentSize.width/2, self.fondo.position.y+self.fondo.contentSize.height/2);
    
    
    self.btn = [[BotonParametros alloc] initWithTitle:@"Terminar"];
    self.btn.position = CGPointMake(self.fondo.position.x, origen.y-self.fondo.contentSize.height-62);
    [self addChild:self.btn];
    
    
    self.visible = NO;
}

- (UIImage *) generaTexturaFondo {
    float ancho = 528;
    float alto = 433;
    
    UIView* fondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho+20, alto+20)];
    fondo.backgroundColor = [UIColor clearColor];
    fondo.layer.cornerRadius = 10.0;
    fondo.opaque = NO;
    
    UIView* sombra = [[UIView alloc] initWithFrame:CGRectMake(20, 20, ancho, alto)];
    sombra.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
    sombra.layer.cornerRadius = 10.0;
    sombra.opaque = NO;
    [fondo addSubview:sombra];
    
    UIView* fondoBlanco = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ancho, alto)];
    fondoBlanco.backgroundColor = [UIColor whiteColor];
    fondoBlanco.layer.cornerRadius = 10.0;
    fondoBlanco.opaque = NO;
    [fondo addSubview:fondoBlanco];
    
    UIView* bordeInterior = [[UIView alloc] initWithFrame:CGRectMake(24, 70, 482, 346)];
    bordeInterior.backgroundColor = [UIColor clearColor];
    bordeInterior.layer.borderWidth = 3.0;
    bordeInterior.layer.borderColor = [UIColor lightGrayColor].CGColor;
    bordeInterior.layer.cornerRadius = 10.0;
    [fondo addSubview:bordeInterior];
    
    UILabel* titulo = [[UILabel alloc] initWithFrame:CGRectMake(24, 24, 300, 34)];
    titulo.font  = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:36.0];
    titulo.textColor  = [UIColor azulOscuro];
    titulo.text = @"Resultados";
    [fondo addSubview:titulo];
    
    //PUNTUACION
    UIView* fondoPuntuacion = [[UIView alloc] initWithFrame:CGRectMake(47, 97, 251, 132)];
    fondoPuntuacion.backgroundColor = [UIColor colorWithRed:190/255.0 green:185/255.0 blue:180/255.0 alpha:1.0];
    fondoPuntuacion.layer.cornerRadius = 5.0;
    [fondo addSubview:fondoPuntuacion];
    
    UIView* fondoClaroPuntuacion = [[UIView alloc] initWithFrame:CGRectInset(fondoPuntuacion.bounds, 7, 7)];
    fondoClaroPuntuacion.backgroundColor = [UIColor colorWithRed:222/255.0 green:217/255.0 blue:212/255.0 alpha:1.0];
    [fondoPuntuacion addSubview:fondoClaroPuntuacion];
    
    UILabel* tuPuntuacion = [[UILabel alloc] initWithFrame:CGRectMake(7, 7, 251-14, 34)];
    tuPuntuacion.backgroundColor = [UIColor azulOscuro];
    tuPuntuacion.textColor = [UIColor whiteColor];
    tuPuntuacion.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:25.0];
    tuPuntuacion.textAlignment = NSTextAlignmentCenter;
    tuPuntuacion.text = @"Tu puntuación";
    [fondoPuntuacion addSubview:tuPuntuacion];
    
    UILabel * puntosLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tuPuntuacion.frame.size.height, tuPuntuacion.frame.size.width, fondoClaroPuntuacion.frame.size.height-tuPuntuacion.frame.size.height)];
    puntosLabel.textColor = [UIColor azulOscuro];
    puntosLabel.textAlignment = NSTextAlignmentCenter;
    puntosLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:66.0];
    puntosLabel.text = [self puntuacion];
    [fondoClaroPuntuacion addSubview:puntosLabel];
    
    
    //NIVEL
    UIView* fondoNivel = [[UIView alloc] initWithFrame:CGRectMake(315, 97, 166, 132)];
    fondoNivel.backgroundColor = [UIColor colorWithRed:190/255.0 green:185/255.0 blue:180/255.0 alpha:1.0];
    fondoNivel.layer.cornerRadius = 5.0;
    [fondo addSubview:fondoNivel];
    
    UIView* fondoClaroNivel = [[UIView alloc] initWithFrame:CGRectInset(fondoNivel.bounds, 7, 7)];
    fondoClaroNivel.backgroundColor = [UIColor colorWithRed:222/255.0 green:217/255.0 blue:212/255.0 alpha:1.0];
    [fondoNivel addSubview:fondoClaroNivel];
    
    UILabel* tuNivel = [[UILabel alloc] initWithFrame:CGRectMake(7, 7, 166-14, 34)];
    tuNivel.backgroundColor = [UIColor azulOscuro];
    tuNivel.textColor = [UIColor whiteColor];
    tuNivel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:25.0];
    tuNivel.textAlignment = NSTextAlignmentCenter;
    tuNivel.text = @"Tu nivel";
    [fondoNivel addSubview:tuNivel];
    
    UILabel * nivelLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tuNivel.frame.size.height, tuNivel.frame.size.width-44, fondoClaroNivel.frame.size.height-tuNivel.frame.size.height)];
    nivelLabel.textColor = [UIColor azulOscuro];
    nivelLabel.textAlignment = NSTextAlignmentCenter;
    nivelLabel.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:66.0];
    nivelLabel.text = [self nivel];
    [fondoClaroNivel addSubview:nivelLabel];
    
    int cambioNivel = [self deltaNivel].intValue;
    if(cambioNivel!=0) {
        UIView* fondoCambioNivel = [[UIView alloc] initWithFrame:CGRectMake(fondoClaroNivel.frame.size.width-44, tuNivel.frame.size.height, 44, fondoClaroNivel.frame.size.height-tuNivel.frame.size.height)];
        UILabel* labelCambio;
        UIImageView* flecha = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flecha_nivel.png"]];
        if(cambioNivel>0) {
            fondoCambioNivel.backgroundColor = [UIColor colorWithRed:0 green:144/255.0 blue:54/255.0 alpha:1.0];
            labelCambio = [[UILabel alloc] initWithFrame:CGRectMake(0, fondoCambioNivel.frame.size.height/2, 44, fondoCambioNivel.frame.size.height/2)];
            flecha.center = CGPointMake(22, fondoCambioNivel.frame.size.height/4);
        } else {
            fondoCambioNivel.backgroundColor = [UIColor colorWithRed:248/255.0 green:59/255.0 blue:43/255.0 alpha:1.0];
            labelCambio = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 44, fondoCambioNivel.frame.size.height/2)];
            flecha.transform = CGAffineTransformMakeRotation(M_PI);
            flecha.center = CGPointMake(22, fondoCambioNivel.frame.size.height/2 + fondoCambioNivel.frame.size.height/4);
        }
        [fondoClaroNivel addSubview:fondoCambioNivel];
        
        labelCambio.textColor = [UIColor whiteColor];
        labelCambio.textAlignment = NSTextAlignmentCenter;
        labelCambio.font = [UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:36.0];
        labelCambio.text = [self deltaNivel];
        [fondoCambioNivel addSubview:labelCambio];
        
        [fondoCambioNivel addSubview:flecha];
    } else {
        nivelLabel.frame = CGRectMake(0, tuNivel.frame.size.height, tuNivel.frame.size.width, fondoClaroNivel.frame.size.height-tuNivel.frame.size.height);
    }
    

    //ACIERTOS FALLOS T_RESP
    
    UIImage* imgAciertos = [UIImage imageNamed:@"aciertos.png"];
    UIImageView* iconoAciertos = [[UIImageView alloc] initWithImage:imgAciertos];
    iconoAciertos.center = CGPointMake(85, 273);
    [fondo addSubview:iconoAciertos];
    
    UIImage* imgFallos = [UIImage imageNamed:@"fallos.png"];
    UIImageView* iconoFallos = [[UIImageView alloc] initWithImage:imgFallos];
    iconoFallos.center = CGPointMake(85, 343);
    [fondo addSubview:iconoFallos];
    
    UIImage* imgReloj = [UIImage imageNamed:@"reloj.png"];
    UIImageView* iconoReloj = [[UIImageView alloc] initWithImage:imgReloj];
    iconoReloj.center = CGPointMake(285, 274);
    [fondo addSubview:iconoReloj];
    
    UILabel * aciertosLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 261, 154, 35)];
    aciertosLabel.textAlignment = NSTextAlignmentLeft;
    [aciertosLabel setAttributedText:[self aciertosFormateado]];
    [fondo addSubview:aciertosLabel];
    
    UILabel * fallosLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 331, 154, 35)];
    fallosLabel.textAlignment = NSTextAlignmentLeft;
    [fallosLabel setAttributedText:[self fallosFormateado]];
    [fondo addSubview:fallosLabel];
    
    UILabel * trespLabel = [[UILabel alloc] initWithFrame:CGRectMake(320, 262, 154, 35)];
    trespLabel.textAlignment = NSTextAlignmentLeft;
    [trespLabel setAttributedText:[self tiempoRespuestaFormateado]];
    [fondo addSubview:trespLabel];
    
    return [ImageUtils imageWithView:fondo];;
}

- (NSAttributedString *) aciertosFormateado {
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ Aciertos", [self aciertos]]];
    
    [attrString addAttribute: NSForegroundColorAttributeName value:[UIColor colorWithRed:0/255.0 green:144/255.0 blue:54/255.0 alpha:1.0] range: NSMakeRange(0, [self aciertos].length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:30.0] range:NSMakeRange(0, [self aciertos].length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:24.0] range:NSMakeRange([self aciertos].length, attrString.length-[self aciertos].length)];
    
    return attrString.copy;
}

- (NSAttributedString *) fallosFormateado {
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ Fallos", [self fallos]]];
    
    [attrString addAttribute: NSForegroundColorAttributeName value:[UIColor colorWithRed:226/255.0 green:0/255.0 blue:26/255.0 alpha:1.0] range: NSMakeRange(0, [self fallos].length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:30.0] range:NSMakeRange(0, [self fallos].length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:24.0] range:NSMakeRange([self fallos].length, attrString.length-[self fallos].length)];
    
    return attrString.copy;
}

- (NSAttributedString *) tiempoRespuestaFormateado {
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ms", [self tiempoRespuesta]]];
    
    [attrString addAttribute: NSForegroundColorAttributeName value:[UIColor colorWithRed:255/255.0 green:92/255.0 blue:15/255.0 alpha:1.0] range: NSMakeRange(0, [self tiempoRespuesta].length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:30.0] range:NSMakeRange(0, [self tiempoRespuesta].length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ChauPhilomeneOne-Regular" size:24.0] range:NSMakeRange([self tiempoRespuesta].length, attrString.length-[self tiempoRespuesta].length)];
    
    return attrString.copy;
}


- (NSString *) puntuacion {
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    return [formatter stringFromNumber:self.resultadoSesion.puntuacion];
}

- (NSString *) nivel {
    return [NSString stringWithFormat:@"%d", self.paciente.nivel.intValue+self.cambioNivel.intValue];
}

- (NSString *) deltaNivel {
    NSString* signo = self.cambioNivel.intValue>0?@"+":@"";
    return [NSString stringWithFormat:@"%@%@", signo, self.cambioNivel];
}

- (NSString*) aciertos {
    return [NSString stringWithFormat:@"%d", self.resultadoSesion.numeroDeAciertos.intValue+self.resultadoSesion.numeroDeNoComisiones.intValue];
}

- (NSString*) fallos {
    return [NSString stringWithFormat:@"%d", self.resultadoSesion.numeroDeComisiones.intValue+self.resultadoSesion.numeroDeOmisiones.intValue];
}

- (NSString *) tiempoRespuesta {
    return [NSString stringWithFormat:@"%.0f", self.resultadoSesion.tiempoMedioDeRespuestaAciertos.floatValue*1000];
}

@end
