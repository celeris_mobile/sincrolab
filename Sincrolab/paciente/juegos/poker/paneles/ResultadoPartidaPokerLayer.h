//
//  ResultadoPartidaPokerLayer.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "ResultadosPartidaLayer.h"
#import "GraficaLineas.h"
#import "cocos2d.h"
#import "Paciente.h"
#import "ResultadoSesionPoker.h"

@interface ResultadoPartidaPokerLayer : PanelLayer
- (id) initWithResultadoSesion:(ResultadoSesionPoker *) resultadoSesion paciente:(Paciente *) paciente andCambioNivel:(NSNumber *) cambioNivel;
- (void) setup;
@end

