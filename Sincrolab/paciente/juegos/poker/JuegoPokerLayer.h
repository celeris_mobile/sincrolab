//
//  JuegoPokerLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 28/10/13.
//  Copyright Ricardo Sánchez Sotres 2013. All rights reserved.
//


#import "cocos2d.h"
#import "EstadoJuegoPoker.h"
#import "PanelParametrosLayer.h"
#import "Paciente.h"
#import "CartasLayer.h"
#import "EstadoEntrenamiento.h"
#import "TutorialPoker.h"

@interface JuegoPokerLayer : CCLayer <PanelLayerDelegate, CartasDelegate, TutorialPokerDelegate>
- (id) initWithPaciente:(Paciente *) paciente estado:(EstadoEntrenamiento *) estadoEntrenamiento;
- (void) finalizaSesion;
- (void) finalizaPartida;
@end
