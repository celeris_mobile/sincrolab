//
//  JuegosHubViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 13/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JuegoViewController.h"
#import "ConfiguraViewController.h"
#import "Paciente.h"
#import "EntrenamientoPacienteModel.h"

@interface JuegosHubViewController : UIViewController <JuegoViewControllerDelegate, ConfiguraViewControllerDelegate>
@property (nonatomic, strong) EntrenamientoPacienteModel* modelo;
@end
