//
//  PacienteViewController.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GranSeccionDelegate.h"
#import "Paciente.h"

@interface PacienteViewController : UIViewController
@property (nonatomic, strong) Paciente* paciente;
@property (nonatomic, weak) id<GranSeccionDelegate> delegate;
@end
