//
//  PacienteViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PacienteViewController.h"
#import "EntrenamientosManager.h"
#import "SeccionEntrenamiento.h"
#import "EntrenamientoPacienteViewController.h"
#import "EntrenamientoPacienteModel.h"
#import "CalendarioPacienteViewController.h"
#import "EstadisticasJugadorViewController.h"
#import "Constantes.h"

@interface PacienteViewController ()
@property (nonatomic, strong) UIButton* botonSalir;

@property (nonatomic, strong) UIButton* botonEntrenar;
@property (nonatomic, strong) UIButton* botonCalendario;
@property (nonatomic, strong) UIButton* botonProgreso;

@property (nonatomic, strong) UIView* placeHolderView;

@property (nonatomic, weak) UIButton* botonSeleccionado;
@property (nonatomic, weak) UIViewController* currentViewController;

@property (nonatomic, strong) EntrenamientoPacienteModel* entrenamientoModel;
@end

@implementation PacienteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fondo_inicio.png"]];
    
    self.placeHolderView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.placeHolderView];
    
    self.botonSalir = [[UIButton alloc] initWithFrame:CGRectMake(925, 36, 79, 28)];
    [self.botonSalir setImage:[UIImage imageNamed:@"btnsalir.png"] forState:UIControlStateNormal];
    [self.botonSalir addTarget:self action:@selector(btnSalirDado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.botonSalir];
    
    self.botonEntrenar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 186, 97)];
    [self.botonEntrenar setImage:[UIImage imageNamed:@"btn_entrenar.png"] forState:UIControlStateNormal];
    [self.botonEntrenar setImage:[UIImage imageNamed:@"btn_entrenar_activo.png"] forState:UIControlStateSelected];
    [self.botonEntrenar addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.botonEntrenar];
    
    self.botonCalendario = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 186, 97)];
    [self.botonCalendario setImage:[UIImage imageNamed:@"btn_calendario.png"] forState:UIControlStateNormal];
    [self.botonCalendario setImage:[UIImage imageNamed:@"btn_calendario_activo.png"] forState:UIControlStateSelected];
    [self.botonCalendario addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.botonCalendario];

    
    self.botonProgreso = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 186, 97)];
    [self.botonProgreso setImage:[UIImage imageNamed:@"btn_progreso.png"] forState:UIControlStateNormal];
    [self.botonProgreso setImage:[UIImage imageNamed:@"btn_progreso_activo.png"] forState:UIControlStateSelected];
    [self.botonProgreso addTarget:self action:@selector(botonDado:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.botonProgreso];
    
    if(self.paciente) {
        self.entrenamientoModel = [[EntrenamientoPacienteModel alloc] initWithPaciente:self.paciente];
    }
    
    [self botonDado:self.botonEntrenar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(!self.paciente) {
        self.botonEntrenar.hidden = YES;
        self.botonCalendario.hidden = YES;
        self.botonProgreso.hidden = YES;
        
        
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //TODO Habrá que gestionar qué pasa si el paciente no tiene ningún entrenamiento activo
    /*
    [EntrenamientosManager cargaEntrenamientoDePaciente:self.paciente onCompletado:^(NSError * error, Entrenamiento * entrenamiento) {
        if(error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            self.entrenamiento = entrenamiento;
            [self botonDado:self.botonEntrenar];
        }
    }];
     */
    


}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.placeHolderView.frame = self.view.bounds;
    
    self.botonEntrenar.center = CGPointMake(self.view.frame.size.width/2-self.botonEntrenar.frame.size.width, self.view.frame.size.height-self.botonEntrenar.frame.size.height/2);
    self.botonCalendario.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-self.botonCalendario.frame.size.height/2);
    self.botonProgreso.center = CGPointMake(self.view.frame.size.width/2+self.botonEntrenar.frame.size.width, self.view.frame.size.height-self.botonProgreso.frame.size.height/2);
}

- (void) botonDado:(UIButton *) boton {
    UIViewController* vc;
    if([boton isEqual:self.botonEntrenar]) {
        vc = [[EntrenamientoPacienteViewController alloc] init];
    }
    
    if([boton isEqual:self.botonCalendario]) {
        vc = [[CalendarioPacienteViewController alloc] init];
    }
    
    if([boton isEqual:self.botonProgreso]) {
        vc = [[EstadisticasJugadorViewController alloc] init];
    }
    
    
    if(self.currentViewController) {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
    }
    
    self.currentViewController = vc;
    
    [self addChildViewController:self.currentViewController];
    self.currentViewController.view.frame = self.placeHolderView.bounds;
    [self.placeHolderView addSubview:self.currentViewController.view];
    
    if(self.botonSeleccionado)
        self.botonSeleccionado.selected = NO;
    
    self.botonSeleccionado = boton;
    self.botonSeleccionado.selected = YES;
    
    if([self.currentViewController respondsToSelector:NSSelectorFromString(@"modelo")]) {
        [self.currentViewController setValue:self.entrenamientoModel forKey:@"modelo"];
    }
}


- (void) btnSalirDado {
    [self.delegate logout];
    [self dismissViewControllerAnimated:YES completion:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
