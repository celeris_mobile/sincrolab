//
//  ParametrosLayer.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"
#import "PanelLayer.h"

@interface PanelParametrosLayer : PanelLayer
- (id)initWithNumLineas:(int) numLineas boton:(NSString *) boton andTitulo:(NSString*) titulo;
- (void) configuraLineas:(NSArray *) lineas conValores:(NSArray *) valores;


@end
