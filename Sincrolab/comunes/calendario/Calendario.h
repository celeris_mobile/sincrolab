//
//  Calendario.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaCell.h"

@class Calendario;

@protocol CalendarioDelegate <NSObject>
- (void) calendar:(Calendario *) calendario haCambiadoAMes:(NSDate *) diaUno;
- (TipoCelda) calendar:(Calendario *)calendario tipoDeCeldaParaDeFecha:(NSDate *)fecha;
- (void)calendar:(Calendario *)calendario diaSeleccionado:(NSDate *)fecha deTipo:(TipoCelda) tipo conFrame:(CGRect)frame;
@optional
- (CGSize) sizeDeCeldaDeCalendar:(Calendario *)calendario;
- (float) espacioEntreCeldasDeCalendar:(Calendario *)calendario;
@end

@interface Calendario : UIView <UICollectionViewDataSource, UICollectionViewDelegate>
- (id)initWithInicio:(NSDate *) fechaInicio andFinal:(NSDate *) fechaFinal;
@property (nonatomic, strong) id<CalendarioDelegate> delegate;
- (void) ultimoMes;
- (void) siguienteMes;
- (void) anteriorMes;
- (void) deseleccionaDia;
- (NSArray*) celdasVisibles;
- (void) recarga;
@end
