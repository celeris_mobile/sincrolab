//
//  MesCell.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TipoCelda) {
    TipoCeldaFueraDeRango,
    TipoCeldaLibre,
    TipoCeldaFallado,
    TipoCeldaRealizado,
    TipoCeldaFuturo
};


@interface DiaCell : UICollectionViewCell


@property (nonatomic, strong) NSDate* date;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, assign) TipoCelda tipo;
@end
