//
//  CalendarioFlowLayout.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CalendarioFlowLayout.h"

static NSString * const BHPhotoAlbumLayoutPhotoCellKind = @"PhotoCell";

@interface CalendarioFlowLayout()
@property (nonatomic, strong) NSDictionary *layoutInfo;
@end

@implementation CalendarioFlowLayout

- (void)prepareLayout
{
    NSMutableDictionary *newLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView numberOfSections];
    NSIndexPath *indexPath;
    
    for (NSInteger section = 0; section < sectionCount; section++) {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++) {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            UICollectionViewLayoutAttributes *itemAttributes =
            [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self frameForAlbumPhotoAtIndexPath:indexPath];
            
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    newLayoutInfo[BHPhotoAlbumLayoutPhotoCellKind] = cellLayoutInfo;
    
    self.layoutInfo = newLayoutInfo;
}

- (CGRect)frameForAlbumPhotoAtIndexPath:(NSIndexPath *)indexPath {
    float posx = (self.itemSize.width + self.interItemSpacing) * (indexPath.row % self.numberOfColumns);
    float posy = (self.itemSize.height + self.interItemSpacing) * (indexPath.row / self.numberOfColumns);
    
    posx += (7*self.itemSize.width + 6*self.interItemSpacing+self.interSectionSpacing)*indexPath.section+self.interSectionSpacing/2;

    return CGRectMake(posx, posy, self.itemSize.width, self.itemSize.height);
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier,
                                                         NSDictionary *elementsInfo,
                                                         BOOL *stop) {
        [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                          UICollectionViewLayoutAttributes *attributes,
                                                          BOOL *innerStop) {
            if (CGRectIntersectsRect(rect, attributes.frame)) {
                [allAttributes addObject:attributes];
            }
        }];
    }];
    
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInfo[BHPhotoAlbumLayoutPhotoCellKind][indexPath];
}

- (CGSize)collectionViewContentSize {
    CGFloat width = self.collectionView.numberOfSections * (7*self.itemSize.width + 6*self.interItemSpacing+self.interSectionSpacing);
    return CGSizeMake(width, self.collectionView.bounds.size.height);
}

@end
