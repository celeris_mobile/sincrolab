//
//  CalendarioFlowLayout.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarioFlowLayout : UICollectionViewLayout
@property (nonatomic) CGSize itemSize;
@property (nonatomic) CGFloat interItemSpacing;
@property (nonatomic) CGFloat interSectionSpacing;
@property (nonatomic) NSInteger numberOfColumns;
@end
