//
//  Calendario.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CalendarioSemanal.h"
#import "CalendarioFlowLayout.h"
#import "DiaCell.h"
#import "DateUtils.h"

static NSString * const DiaCellIdentifier = @"diaCell";

@interface CalendarioSemanal()
@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDate* diaUnoPrimeraSemana;
@property (nonatomic, strong) NSDate* fromDate;
@property (nonatomic, strong) NSDate* toDate;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger indiceSemanaActual;
@end

@implementation CalendarioSemanal

- (id)initWithInicio:(NSDate *) fechaInicio andFinal:(NSDate *) fechaFinal
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
		_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		[_calendar setFirstWeekday:2];
        
        _fromDate = fechaInicio;
        _toDate = fechaFinal;
        
        NSDateComponents* components = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit fromDate:_fromDate];
        NSInteger weekday = components.weekday - 2;
        if(weekday<0)
            weekday = 6;
        
        components.day -= weekday;
        _diaUnoPrimeraSemana = [self.calendar dateFromComponents:components];
        
        /*
         //Debería ser la fecha de inicio del entrenamiento
         NSDateComponents *comps = [[NSDateComponents alloc] init];
         [comps setYear:2013];
         [comps setMonth:10];
         [comps setDay:1];
         [comps setHour:10];
         _fromDate = [self.calendar dateFromComponents:comps];
         
         NSDateComponents* components = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:_fromDate];
         _fromDate = [self.calendar dateFromComponents:components];
         
         components = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
         _toDate = [self.calendar dateFromComponents:components];
         */
        
        
        [self setup];
    }
    return self;
}

- (void) setup {
    /*
    CalendarioFlowLayout* layout = [[CalendarioFlowLayout alloc] init];
    layout.itemSize = (CGSize){ 53, 48 };
    layout.interItemSpacing = 4.0f;
    layout.interSectionSpacing = 20.0;
    layout.numberOfColumns = 7;
    */
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = (CGSize){ 53, 48 };
    layout.minimumInteritemSpacing = 4.0f;
    layout.minimumLineSpacing = 20.0;

    //self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(-10, 24, 53*7+4*6+20, 48*6+4*5) collectionViewLayout:layout];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 53*7+4*6, 48) collectionViewLayout:layout];

    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.pagingEnabled = YES;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerClass:[DiaCell class] forCellWithReuseIdentifier:DiaCellIdentifier];
    [self addSubview:self.collectionView];
    
    CGRect viewFrame = self.frame;
    viewFrame.size.height = 20 + 48;
    viewFrame.size.width = 53*7+4*6;
    self.frame = viewFrame;
    
    NSArray* dias = @[@"Lunes", @"Martes", @"Miercoles", @"Jueves", @"Viernes", @"Sábado", @"Domingo"];
    int l = 0;
    for (NSString* dia in dias) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(l*(53+4), 50, 53, 20)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Lato-Regular" size:10.0];
        label.text = dia;
        [self addSubview:label];
        l+=1;
    }
}

- (void) ultimaSemana {
    self.indiceSemanaActual = [self numberOfSectionsInCollectionView:self.collectionView]-1;
    
    NSIndexPath *cellIndexPath = [NSIndexPath indexPathForItem:0 inSection:self.indiceSemanaActual];
    [self.collectionView scrollToItemAtIndexPath:cellIndexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];

    NSDate* fechaSemana = [self dateForFirstDayInSection:self.indiceSemanaActual];
    
    if([self.delegate respondsToSelector:@selector(calendar:haCambiadoASemana:)])
        [self.delegate calendar:self haCambiadoASemana:fechaSemana];
}

- (void) siguienteSemana {
    if(self.indiceSemanaActual == [self numberOfSectionsInCollectionView:self.collectionView]-1)
        return;
    
    self.indiceSemanaActual+=1;
    [self actualizaSemana];
}

- (void) anteriorSemana {
    if(self.indiceSemanaActual == 0)
        return;
    
    self.indiceSemanaActual-=1;
    [self actualizaSemana];
}

- (void) deseleccionaDia {
    NSIndexPath* indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void) actualizaSemana {
    NSIndexPath *cellIndexPath = [NSIndexPath indexPathForItem:0 inSection:self.indiceSemanaActual];
    
    [self.collectionView scrollToItemAtIndexPath:cellIndexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    
    NSDate* fechaSemana = [self dateForFirstDayInSection:self.indiceSemanaActual];
    
    if([self.delegate respondsToSelector:@selector(calendar:haCambiadoASemana:)])
        [self.delegate calendar:self haCambiadoASemana:fechaSemana];
}

- (NSDate *) dateForFirstDayInSection:(NSInteger)section {
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.week = section;
    
	return [self.calendar dateByAddingComponents:dateComponents toDate:self.diaUnoPrimeraSemana options:0];
}

- (NSArray*) celdasVisibles {
    return self.collectionView.visibleCells;
}

- (void) recarga {
    [self.collectionView reloadData];
}

#pragma mark UICollectionViewDatasource
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger semanas = [self.calendar components:NSWeekCalendarUnit fromDate:self.diaUnoPrimeraSemana toDate:self.toDate options:0].week;
	return semanas+1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 7;
}

- (NSDate *) diaParaIndexPath:(NSIndexPath *) indexPath {
    NSDate *firstDayInWeek = [self dateForFirstDayInSection:indexPath.section];
    NSInteger weekday = [self.calendar components:NSWeekdayCalendarUnit fromDate:firstDayInWeek].weekday;
    weekday-=2;
    if(weekday<0)
        weekday = 6;
    
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.day = indexPath.item - weekday;
    NSDate *dia = [self.calendar dateByAddingComponents:dateComponents toDate:firstDayInWeek options:0];
    return dia;
}

#pragma mark UICollectionViewDelegate
- (DiaCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
    DiaCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DiaCellIdentifier forIndexPath:indexPath];

   // NSDate *firstDayInMonth = [self dateForFirstDayInSection:indexPath.section];
    
    NSDate* cellDate = [self diaParaIndexPath:indexPath];
    
    /*
    NSDateComponents *firsDayComponents = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:firstDayInMonth];
    NSDateComponents *cellDateComponents = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:cellDate];
    */
    
    cell.date = cellDate;
    BOOL menorQueInicio = [DateUtils comparaSinHoraFecha:cellDate conFecha:self.fromDate]==NSOrderedAscending;
    //BOOL mayorQueFinal = [DateUtils comparaSinHoraFecha:cellDate conFecha:self.toDate]==NSOrderedDescending;
    if(menorQueInicio)
        cell.tipo = TipoCeldaFueraDeRango;
    else {
        if([self.delegate respondsToSelector:@selector(calendar:tipoDeCeldaParaDeFecha:)])
            cell.tipo = [self.delegate calendar:self tipoDeCeldaParaDeFecha:cellDate];
        else
            cell.tipo = TipoCeldaLibre;
    }
    
    cell.enabled = YES;//((firsDayComponents.year == cellDateComponents.year) && (firsDayComponents.month == cellDateComponents.month));
    
    
    
    return cell;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.collectionView.frame.size.width;
    self.indiceSemanaActual = self.collectionView.contentOffset.x / pageWidth;
    
    NSDate* fechaMes = [self dateForFirstDayInSection:self.indiceSemanaActual];
    
    if([self.delegate respondsToSelector:@selector(calendar:haCambiadoASemana:)])
        [self.delegate calendar:self haCambiadoASemana:fechaMes];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DiaCell* cell = (DiaCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    NSDate *cellDate = [self diaParaIndexPath:indexPath];
    
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attributes.frame;
    cellRect = [self convertRect:cellRect fromView:self.collectionView];
    
    if([self.delegate respondsToSelector:@selector(calendar:diaSeleccionado:deTipo:conFrame:)])
        [self.delegate calendar:self diaSeleccionado:cellDate deTipo:cell.tipo conFrame:cellRect];
}

@end
