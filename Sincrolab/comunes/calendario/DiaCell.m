//
//  MesCell.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 10/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "DiaCell.h"

@interface DiaCell ()
@property (nonatomic, strong) UILabel* labelDia;
@property (nonatomic, strong) UIColor* color;
@end

@implementation DiaCell

- (id) initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
        
        
        self.labelDia = [[UILabel alloc] initWithFrame:self.bounds];
        self.labelDia.font = [UIFont fontWithName:@"Lato-Black" size:25.0];
        self.labelDia.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.labelDia];

        if(frame.size.width>55) {
            self.layer.cornerRadius = 10.0;
            self.labelDia.font = [UIFont fontWithName:@"Lato-Black" size:31.0];
            
        }

	}
	return self;
}

- (void) setEnabled:(BOOL)enabled {
	_enabled = enabled;
    [self setNeedsLayout];
}

- (void) setDate:(NSDate *)date {
	_date = date;
    [self setNeedsLayout];
}

- (void)setTipo:(TipoCelda)tipo {
    _tipo = tipo;
            self.labelDia.textColor = [UIColor blackColor];
    switch (tipo) {
        case TipoCeldaFueraDeRango:
        case TipoCeldaLibre:
            self.color = [UIColor whiteColor];
            self.labelDia.textColor = [UIColor lightGrayColor];
            break;
        case TipoCeldaFallado:
            self.color = [UIColor colorWithRed:231/255.0 green:82/255.0 blue:82/255.0 alpha:1.0];
            break;
        case TipoCeldaFuturo:
            self.color = [UIColor colorWithRed:255/255.0 green:218/255.0 blue:124/255.0 alpha:1.0];
            break;
        case TipoCeldaRealizado:
            self.color = [UIColor colorWithRed:171/255.0 green:233/255.0 blue:212/255.0 alpha:1.0];
            break;
    }

    [self setNeedsLayout];
}

- (void) setHighlighted:(BOOL)highlighted {
	[super setHighlighted:highlighted];
	[self setNeedsLayout];
}
- (void) setSelected:(BOOL)selected {
	[super setSelected:selected];
	[self setNeedsLayout];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.alpha = 1.0;
    self.backgroundColor = [UIColor clearColor];
    self.layer.borderColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0].CGColor;
    self.layer.borderWidth = 1.0;
    
    if(!self.enabled)
        self.alpha = 0.0;
    else {
        if(self.tipo==TipoCeldaFueraDeRango)
            self.alpha = 0.3;
    }
    
    if(self.color)
        self.backgroundColor = self.color;
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate* hoy = [NSDate date];
    NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
    NSDateComponents *dateComponents = [calendar components:comps fromDate:self.date];
    NSDateComponents *hoyComponents = [calendar components:comps fromDate: hoy];
    
    self.labelDia.text = [NSString stringWithFormat:@"%d", dateComponents.day];
    
    if(dateComponents.year == hoyComponents.year &&
       dateComponents.month == hoyComponents.month &&
       dateComponents.day == hoyComponents.day)
        self.backgroundColor = [UIColor colorWithRed:88/255.0 green:200/255.0 blue:247/255.0 alpha:1.0];
   
    if(self.selected) {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 4.0;
    }
}


@end