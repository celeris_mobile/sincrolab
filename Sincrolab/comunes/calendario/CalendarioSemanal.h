//
//  CalendarioSemanal.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaCell.h"

@class CalendarioSemanal;

@protocol CalendarioSemanalDelegate <NSObject>
- (void) calendar:(CalendarioSemanal *) calendario haCambiadoASemana:(NSDate *) diaUno;
- (TipoCelda) calendar:(CalendarioSemanal *)calendario tipoDeCeldaParaDeFecha:(NSDate *)fecha;
- (void)calendar:(CalendarioSemanal *)calendario diaSeleccionado:(NSDate *)fecha deTipo:(TipoCelda) tipo conFrame:(CGRect)frame;
@end

@interface CalendarioSemanal : UIView <UICollectionViewDataSource, UICollectionViewDelegate>
- (id)initWithInicio:(NSDate *) fechaInicio andFinal:(NSDate *) fechaFinal;
@property (nonatomic, strong) id<CalendarioSemanalDelegate> delegate;
- (void) ultimaSemana;
- (void) siguienteSemana;
- (void) anteriorSemana;
- (void) deseleccionaDia;
- (NSArray*) celdasVisibles;
- (void) recarga;
@end
