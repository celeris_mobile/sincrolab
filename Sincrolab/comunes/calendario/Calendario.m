//
//  Calendario.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "Calendario.h"
#import "CalendarioFlowLayout.h"
#import "DiaCell.h"
#import "DateUtils.h"

static NSString * const DiaCellIdentifier = @"diaCell";

@interface Calendario()
@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDate* diaUnoPrimerMes;
@property (nonatomic, strong) NSDate* fromDate;
@property (nonatomic, strong) NSDate* toDate;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger indiceMesActual;
@property (nonatomic, strong) CalendarioFlowLayout* layout;
@end

@implementation Calendario

- (id)initWithInicio:(NSDate *) fechaInicio andFinal:(NSDate *) fechaFinal
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
		_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		[_calendar setFirstWeekday:2];

        _fromDate = fechaInicio;
      
        //_toDate = fechaFinal;
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setMonth:1];
        _toDate = [self.calendar dateByAddingComponents:dateComponents toDate:fechaFinal options:0];

        
        NSDateComponents* components = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:_fromDate];
        components.day = 1;
        _diaUnoPrimerMes = [self.calendar dateFromComponents:components];
        
        /*
        //Debería ser la fecha de inicio del entrenamiento
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setYear:2013];
        [comps setMonth:10];
        [comps setDay:1];
        [comps setHour:10];
        _fromDate = [self.calendar dateFromComponents:comps];
                
        NSDateComponents* components = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:_fromDate];
        _fromDate = [self.calendar dateFromComponents:components];
        
        components = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
		_toDate = [self.calendar dateFromComponents:components];
         */
        

       // [self setup];
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    [self setup];
}

- (void) setup {
    self.layout = [[CalendarioFlowLayout alloc] init];
    if([self.delegate respondsToSelector:@selector(sizeDeCeldaDeCalendar:)])
        self.layout.itemSize = [self.delegate sizeDeCeldaDeCalendar:self];
    else
        self.layout.itemSize = (CGSize){ 53, 48 };
    
    if([self.delegate respondsToSelector:@selector(espacioEntreCeldasDeCalendar:)])
        self.layout.interItemSpacing = [self.delegate espacioEntreCeldasDeCalendar:self];
    else
        self.layout.interItemSpacing = 4.0f;

    self.layout.interSectionSpacing = 20.0;
    self.layout.numberOfColumns = 7;
    
    //self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(-10, 24, 53*7+4*6+20, 48*6+4*5) collectionViewLayout:layout];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(-10, 20, self.layout.itemSize.width*7+self.layout.interItemSpacing*6+20, self.layout.itemSize.height*6+self.layout.interItemSpacing*5) collectionViewLayout:self.layout];
    
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.pagingEnabled = YES;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerClass:[DiaCell class] forCellWithReuseIdentifier:DiaCellIdentifier];
    [self addSubview:self.collectionView];
    
    CGRect viewFrame = self.frame;
    viewFrame.size.height = 20 + self.layout.itemSize.height*6+self.layout.interItemSpacing*5;
    viewFrame.size.width = self.layout.itemSize.width*7+self.layout.interItemSpacing*6;
    self.frame = viewFrame;
    
    NSArray* dias = @[@"Lunes", @"Martes", @"Miercoles", @"Jueves", @"Viernes", @"Sábado", @"Domingo"];
    int l = 0;
    for (NSString* dia in dias) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(l*(self.layout.itemSize.width+self.layout.interItemSpacing), 0, self.layout.itemSize.width, 20)];
        label.font = [UIFont fontWithName:@"Lato-Bold" size:11.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = dia;
        [self addSubview:label];
        l+=1;
    }
}

- (void) ultimoMes {
    self.indiceMesActual = [self numberOfSectionsInCollectionView:self.collectionView]-2;
    
    NSIndexPath *cellIndexPath = [NSIndexPath indexPathForItem:0 inSection:self.indiceMesActual];
    [self.collectionView setContentOffset:CGPointMake((self.layout.itemSize.width*7+self.layout.interItemSpacing*6+20)*cellIndexPath.section, 0) animated:NO];
    NSDate* fechaMes = [self dateForFirstDayInSection:self.indiceMesActual];
    
    if([self.delegate respondsToSelector:@selector(calendar:haCambiadoAMes:)])
        [self.delegate calendar:self haCambiadoAMes:fechaMes];
}

- (void) siguienteMes {
    if(self.indiceMesActual == [self numberOfSectionsInCollectionView:self.collectionView]-1)
        return;
    
    self.indiceMesActual+=1;
    [self actualizaMes];
}

- (void) anteriorMes {
    if(self.indiceMesActual == 0)
        return;
    
    self.indiceMesActual-=1;
    [self actualizaMes];
}

- (void) deseleccionaDia {
    NSIndexPath* indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void) actualizaMes {
    NSIndexPath *cellIndexPath = [NSIndexPath indexPathForItem:0 inSection:self.indiceMesActual];

    [self.collectionView setContentOffset:CGPointMake((self.layout.itemSize.width*7+self.layout.interItemSpacing*6+20)*cellIndexPath.section, 0) animated:YES];
    
    NSDate* fechaMes = [self dateForFirstDayInSection:self.indiceMesActual];
    
    if([self.delegate respondsToSelector:@selector(calendar:haCambiadoAMes:)])
        [self.delegate calendar:self haCambiadoAMes:fechaMes];
}

- (NSDate *) dateForFirstDayInSection:(NSInteger)section {
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = section;
    
	return [self.calendar dateByAddingComponents:dateComponents toDate:self.diaUnoPrimerMes options:0];
}

- (NSArray*) celdasVisibles {
    return self.collectionView.visibleCells;
}

- (void) recarga {
    [self.collectionView reloadData];
}

#pragma mark UICollectionViewDatasource
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return [self.calendar components:NSMonthCalendarUnit fromDate:self.diaUnoPrimerMes toDate:self.toDate options:0].month+1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = section;
    NSDate* firstDayInMonth = [self.calendar dateByAddingComponents:dateComponents toDate:self.diaUnoPrimerMes options:0];
    
    NSRange weekRange = [self.calendar rangeOfUnit:NSWeekCalendarUnit inUnit:NSMonthCalendarUnit forDate:firstDayInMonth];
    NSInteger weeksCount=weekRange.length;
    
    
	return weeksCount*7;
}

- (NSDate *) diaParaIndexPath:(NSIndexPath *) indexPath {
    NSDate *firstDayInMonth = [self dateForFirstDayInSection:indexPath.section];
    NSInteger weekday = [self.calendar components:NSWeekdayCalendarUnit fromDate:firstDayInMonth].weekday;
    weekday-=2;
    if(weekday<0)
        weekday = 6;
    
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.day = indexPath.item - weekday;
    NSDate *dia = [self.calendar dateByAddingComponents:dateComponents toDate:firstDayInMonth options:0];
    return dia;
}

#pragma mark UICollectionViewDelegate
- (DiaCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
    DiaCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DiaCellIdentifier forIndexPath:indexPath];
    
    NSDate *firstDayInMonth = [self dateForFirstDayInSection:indexPath.section];
    
    NSDate* cellDate = [self diaParaIndexPath:indexPath];
    
    NSDateComponents *firsDayComponents = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:firstDayInMonth];
    NSDateComponents *cellDateComponents = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:cellDate];
    
    cell.date = cellDate;
    BOOL menorQueInicio = [DateUtils comparaSinHoraFecha:cellDate conFecha:self.fromDate]==NSOrderedAscending;
    //BOOL mayorQueFinal = [DateUtils comparaSinHoraFecha:cellDate conFecha:self.toDate]==NSOrderedDescending;
    if(menorQueInicio)
        cell.tipo = TipoCeldaFueraDeRango;
    else {
        if([self.delegate respondsToSelector:@selector(calendar:tipoDeCeldaParaDeFecha:)])
            cell.tipo = [self.delegate calendar:self tipoDeCeldaParaDeFecha:cellDate];
        else
            cell.tipo = TipoCeldaLibre;
    }
    
    cell.enabled = ((firsDayComponents.year == cellDateComponents.year) && (firsDayComponents.month == cellDateComponents.month));
    

    return cell;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.collectionView.frame.size.width;
    self.indiceMesActual = self.collectionView.contentOffset.x / pageWidth;
    
    NSDate* fechaMes = [self dateForFirstDayInSection:self.indiceMesActual];
    
    if([self.delegate respondsToSelector:@selector(calendar:haCambiadoAMes:)])
        [self.delegate calendar:self haCambiadoAMes:fechaMes];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DiaCell* cell = (DiaCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    NSDate *cellDate = [self diaParaIndexPath:indexPath];
    
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attributes.frame;
    cellRect = [self convertRect:cellRect fromView:self.collectionView];
    
    if(cell.tipo == TipoCeldaFueraDeRango) {
        [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    }
    
    if([self.delegate respondsToSelector:@selector(calendar:diaSeleccionado:deTipo:conFrame:)])
        [self.delegate calendar:self diaSeleccionado:cellDate deTipo:cell.tipo conFrame:cellRect];
}

@end
