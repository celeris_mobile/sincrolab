//
//  CustomPopOverBackgroundView.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIPopoverBackgroundView.h>

@interface CustomPopOverBackgroundView : UIPopoverBackgroundView {
    UIImageView *_borderImageView;
    UIImageView *_arrowView;
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
}

@end
