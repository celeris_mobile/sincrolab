//
//  CustomPopOverBackgroundView.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 09/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CustomPopOverBackgroundView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Extensions.h"
#import "ImageUtils.h"
#import "FlechaPopOver.h"

#define CAP_INSET 25.0

#define CONTENT_INSET 0.0
#define ARROW_BASE 36.0
#define ARROW_HEIGHT 18.0

@implementation CustomPopOverBackgroundView

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        UIView* borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        borderView.backgroundColor = [UIColor azulOscuro];
        borderView.layer.borderWidth = 1.0;
        borderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        UIImage* borderImage = [ImageUtils imageWithView:borderView];
        
        _borderImageView = [[UIImageView alloc] initWithImage:[borderImage resizableImageWithCapInsets:UIEdgeInsetsMake(CAP_INSET, CAP_INSET, CAP_INSET,CAP_INSET)]];
        
        UIView* arrowView = [[FlechaPopOver alloc] initWithFrame:CGRectMake(0, 0, ARROW_BASE, ARROW_HEIGHT)];
        arrowView.opaque = NO;
        
        
        UIImage* arrowImage = [ImageUtils imageWithView:arrowView];
        
        
        _arrowView = [[UIImageView alloc] initWithImage:arrowImage];
        
        [self addSubview:_borderImageView];
        [self addSubview:_arrowView];
        
        /*
         self.layer.shadowColor = [UIColor blackColor].CGColor;
         self.layer.shadowOffset = CGSizeMake(5.0,5.0f);
         self.layer.shadowOpacity = .5f;
         self.layer.shadowRadius = 10.0f;
         */
        self.layer.shadowColor = [UIColor clearColor].CGColor;
    }
    return self;
}


- (CGFloat) arrowOffset {
    return _arrowOffset;
}

- (void) setArrowOffset:(CGFloat)arrowOffset {
    _arrowOffset = arrowOffset;
}

- (UIPopoverArrowDirection)arrowDirection {
    return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
    _arrowDirection = arrowDirection;
}


+(UIEdgeInsets)contentViewInsets{
    return UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
}

+(CGFloat)arrowHeight{
    return ARROW_HEIGHT;
}

+(CGFloat)arrowBase{
    return ARROW_BASE;
}

-  (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat _height = self.frame.size.height;
    CGFloat _width = self.frame.size.width;
    CGFloat _left = 0.0;
    CGFloat _top = 0.0;
    CGFloat _coordinate = 0.0;
    CGAffineTransform _rotation = CGAffineTransformIdentity;
    
    
    switch (self.arrowDirection) {
        case UIPopoverArrowDirectionAny:
        case UIPopoverArrowDirectionUnknown:
        case UIPopoverArrowDirectionUp:
            _top += ARROW_HEIGHT;
            _height -= ARROW_HEIGHT;
            _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, 1, ARROW_BASE, ARROW_HEIGHT);
            break;
            
            
        case UIPopoverArrowDirectionDown:
            _height -= ARROW_HEIGHT;
            _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, _height-1, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI );
            break;
            
        case UIPopoverArrowDirectionLeft:
            _left += ARROW_HEIGHT;
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(1-ARROW_HEIGHT/2, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
            break;
            
        case UIPopoverArrowDirectionRight:
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(_width, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI_2 );
            
            break;
            
    }
    
    _borderImageView.frame =  CGRectMake(_left, _top, _width, _height);
    
    
    [_arrowView setTransform:_rotation];
    
}

@end
