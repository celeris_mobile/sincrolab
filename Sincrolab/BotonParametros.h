//
//  BotonParametros.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 25/11/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "cocos2d.h"

@interface BotonParametros : CCNode
- (id) initWithTitle:(NSString *) title;
- (void) presionado;
- (void) soltado;
@end
