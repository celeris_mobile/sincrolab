//
//  EstadoCell.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EstadoCell : UITableViewCell
@property (nonatomic, strong) NSString* estado;
@end
