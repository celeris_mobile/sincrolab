//
//  TestEstadoInvasionViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TestEstadoInvasionViewController.h"
#import "EstadoJuegoInvasion.h"
#import "ResultadoSesionInvasion.h"
#import "EstadoCell.h"

@interface TestEstadoInvasionViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfItinerario;
@property (weak, nonatomic) IBOutlet UITextField *tfHito;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

@property (nonatomic, strong) EstadoJuegoInvasion* estado;
@property (nonatomic, strong) NSMutableArray* estados;
@end

@implementation TestEstadoInvasionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tabla.dataSource = self;
    self.tabla.delegate = self;
    [self.tabla registerClass:[EstadoCell class] forCellReuseIdentifier:@"estadoCell"];
}
- (IBAction)iniciarDado:(id)sender {
    NSArray* componentes = [self.tfItinerario.text componentsSeparatedByString:@":"];
    if(componentes.count==1)
        self.estado = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:self.tfItinerario.text conAfectacion:@"1"];
    else
        self.estado = [EstadoJuegoInvasion estadoParaHitoInicialdeItinerario:[componentes objectAtIndex:0] conAfectacion:[componentes objectAtIndex:1]];
    
    if(![self.estado configuraParaHito:self.tfHito.text.intValue])
        self.estado = nil;
    
    self.estados = [[NSMutableArray alloc] init];
    
    [self.estados addObject:@"itinerario, hito, sesiones, trials, tee1, tee2, tie, dss, cambio, n_cambios, p_aparicion_ng, p_aparicion_ss, tipo_ss"];
    [self.estados addObject:self.estado.description];
    
    [self.tabla reloadData];
}

- (IBAction)subirDado:(id)sender {
    NSMutableArray* resultados = [[NSMutableArray alloc] init];
    for (int r = 0; r<6; r++) {
        ResultadoSesionInvasion* resultado = [[ResultadoSesionInvasion alloc] init];
        resultado.porcentajeAciertos = [NSNumber numberWithFloat:90.0];
        [resultados addObject:resultado];
    }
    
    if([self.estado compruebaCambioNivelConResultados:resultados]==EstadoJuegoCambioFinal) {
        [[[UIAlertView alloc] initWithTitle:@"FIN" message:@"Se han terminado los hitos de ese itinerario" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        [self.estados insertObject:self.estado.description atIndex:1];
        [self.tabla reloadData];
    }
}



- (IBAction)bajarDado:(id)sender {
    NSMutableArray* resultados = [[NSMutableArray alloc] init];
    for (int r = 0; r<6; r++) {
        ResultadoSesionInvasion* resultado = [[ResultadoSesionInvasion alloc] init];
        resultado.porcentajeAciertos = [NSNumber numberWithFloat:30.0];
        [resultados addObject:resultado];
    }
    
    [self.estado compruebaCambioNivelConResultados:resultados];
    [self.estados insertObject:self.estado.description atIndex:1];
    [self.tabla reloadData];
}

- (IBAction)disparador2:(id)sender {
    NSMutableArray* resultados = [[NSMutableArray alloc] init];
    for (int r = 0; r<6; r++) {
        ResultadoSesionInvasion* resultado = [[ResultadoSesionInvasion alloc] init];
        resultado.porcentajeAciertos = [NSNumber numberWithFloat:60.0];
        resultado.numeroDeStopSigns = [NSNumber numberWithFloat:1.0];
        resultado.numeroDeNoComisiones = [NSNumber numberWithFloat:1.0];
        [resultados addObject:resultado];
    }
    
    [self.estado compruebaCambioNivelConResultados:resultados];
    [self.estados insertObject:self.estado.description atIndex:1];
    [self.tabla reloadData];
}

- (IBAction)disparadorBajada:(id)sender {
    NSMutableArray* resultados = [[NSMutableArray alloc] init];
    for (int r = 0; r<6; r++) {
        ResultadoSesionInvasion* resultado = [[ResultadoSesionInvasion alloc] init];
        resultado.porcentajeAciertos = [NSNumber numberWithFloat:60.0];
        resultado.numeroDeStopSigns = [NSNumber numberWithFloat:5.0];
        resultado.numeroDeNoComisiones = [NSNumber numberWithFloat:1.0];
        [resultados addObject:resultado];
    }
    
    [self.estado compruebaCambioNivelConResultados:resultados];
    [self.estados insertObject:self.estado.description atIndex:1];
    [self.tabla reloadData];
}

- (IBAction)btnCerrar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.estados.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EstadoCell* cell = [self.tabla dequeueReusableCellWithIdentifier:@"estadoCell"];
    cell.estado = [self.estados objectAtIndex:indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
