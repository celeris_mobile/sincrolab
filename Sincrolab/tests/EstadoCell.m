//
//  EstadoCell.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "EstadoCell.h"

@interface EstadoCell()
@property (nonatomic, strong) NSMutableArray* textos;
@end

@implementation EstadoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEstado:(NSString *)estado {
    _estado = estado;
    
    if(self.textos) {
        for (UILabel* label in self.textos) {
            [label removeFromSuperview];
        }
    }
    
    self.textos = [[NSMutableArray alloc] init];
    
    NSArray* componentes = [self.estado componentsSeparatedByString:@", "];
    float ancho = 984/componentes.count;
    int c = 0;
    for (NSString* componente in componentes) {
        UILabel* label  = [[UILabel alloc] initWithFrame:CGRectMake(c*ancho, 0, ancho, self.frame.size.height)];
        label.text = componente;
        [self addSubview:label];
        [self.textos addObject:label];
        c++;
    }
}

@end
