//
//  TestEstadoPokerViewController.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 18/01/14.
//  Copyright (c) 2014 Ricardo Sánchez Sotres. All rights reserved.
//

#import "TestEstadoPokerViewController.h"
#import "EstadoJuegoPoker.h"
#import "ResultadoSesionPoker.h"
#import "EstadoCell.h"

@interface TestEstadoPokerViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfItinerario;
@property (weak, nonatomic) IBOutlet UITextField *tfHito;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

@property (nonatomic, strong) EstadoJuegoPoker* estado;
@property (nonatomic, strong) NSMutableArray* estados;
@end

@implementation TestEstadoPokerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tabla.dataSource = self;
    self.tabla.delegate = self;
    [self.tabla registerClass:[EstadoCell class] forCellReuseIdentifier:@"estadoCell"];
}
- (IBAction)iniciarDado:(id)sender {
    NSArray* componentes = [self.tfItinerario.text componentsSeparatedByString:@":"];
    if(componentes.count==1)
        self.estado = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:self.tfItinerario.text conAfectacion:@"1"];
    else
        self.estado = [EstadoJuegoPoker estadoParaHitoInicialdeItinerario:[componentes objectAtIndex:0] conAfectacion:[componentes objectAtIndex:1]];
    
    if(![self.estado configuraParaHito:self.tfHito.text.intValue])
        self.estado = nil;
    
    self.estados = [[NSMutableArray alloc] init];
    
    [self.estados addObject:@"itinerario, hito, cartas, jugadores, categorias, tie, tresp, ayudas, p_dianas, n_cambios"];
    [self.estados addObject:self.estado.description];
    
    [self.tabla reloadData];
}

- (IBAction)subirDado:(id)sender {

    ResultadoSesionPoker* resultado = [[ResultadoSesionPoker alloc] init];
    resultado.porcentajeAciertos = [NSNumber numberWithFloat:90.0];

    if([self.estado compruebaCambioNivelConResultados:[NSArray arrayWithObject:resultado]]==EstadoJuegoCambioFinal) {
        [[[UIAlertView alloc] initWithTitle:@"FIN" message:@"Se han terminado los hitos de ese itinerario" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        [self.estados insertObject:self.estado.description atIndex:1];
        [self.tabla reloadData];
    }
}



- (IBAction)bajarDado:(id)sender {

    ResultadoSesionPoker* resultado = [[ResultadoSesionPoker alloc] init];
    resultado.porcentajeAciertos = [NSNumber numberWithFloat:30.0];
    
    if([self.estado compruebaCambioNivelConResultados:[NSArray arrayWithObject:resultado]]==EstadoJuegoCambioFinal) {
        [[[UIAlertView alloc] initWithTitle:@"FIN" message:@"Se han terminado los hitos de ese itinerario" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];

    } else {
        [self.estados insertObject:self.estado.description atIndex:1];
        [self.tabla reloadData];
    }
}


- (IBAction)btnCerrar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.estados.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EstadoCell* cell = [self.tabla dequeueReusableCellWithIdentifier:@"estadoCell"];
    cell.estado = [self.estados objectAtIndex:indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
