//
//  GraficaBarras.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GraficaBarras;

@protocol GraficaBarrasDelegate <NSObject>
@optional
- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context;
@end

@protocol GraficaBarrasDatasource <NSObject>
- (int) numeroDeValoresEnGraficaDeBarras:(GraficaBarras *) grafica;
- (NSNumber *) graficaDeBarras:(GraficaBarras *) grafica valorParaIndice:(NSInteger) indice;
@optional
- (NSString *) graficaDeBarras:(GraficaBarras *) grafica etiquetaParaIndice:(NSInteger) indice;
@end

@interface GraficaBarras : UIView
@property (nonatomic, weak) id<GraficaBarrasDelegate> delegate;
@property (nonatomic, weak) id<GraficaBarrasDatasource> dataSource;

@property (nonatomic, assign) CGSize graphInset;
@property (nonatomic, assign) CGRect limites;
@property (nonatomic, strong) UIFont* labelFont;

- (void) reloadData;
@end
