//
//  Lineas.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaLineas.h"

static CGFloat const espaciadoEtiquetasX = 6.0;
static CGFloat const espaciadoEtiquetasY = 4.0;

@interface GraficaLineas()
@property (nonatomic, assign) CGSize graphInset;
@property (nonatomic, assign) NSInteger numValores;

@property (nonatomic, assign) UIFont* fontEtiqueta;
@property (nonatomic, assign) UIColor* colorEtiqueta;

@property (nonatomic, assign) CGSize tamEtiquetasY;
@property (nonatomic, assign) CGSize tamEtiquetasX;

@property (nonatomic, assign) CGRect drawingRect;
@property (nonatomic, strong) NSArray* puntos;

@property (nonatomic, assign) BOOL maximoAutomatico;
@property (nonatomic, assign) BOOL minimoAutomatico;
@property (nonatomic, assign) BOOL saltoYAutomatico;
@property (nonatomic, assign) BOOL saltoXAutomatico;

@end

@implementation GraficaLineas

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    self.backgroundColor = [UIColor clearColor];
    
    self.ejes = GFBordesWidthMake(0.0, 1.0, 1.0, 0.0);
    self.graphInset = CGSizeMake(5.0, 5.0);
    self.dibujaLineasX = YES;
    self.dibujaLineasY = NO;
    self.dibujaEtiquetasX = YES;
    self.dibujaEtiquetasY = NO;
    self.saltoY = 0.0;
    self.saltoX = 0.0;
    self.maximoAutomatico = YES;
    self.minimoAutomatico = YES;
    self.saltoXAutomatico = YES;
    self.saltoYAutomatico = YES;
    self.prefijoEtiquetasY = @"";
}

- (void)setMaxY:(CGFloat)maxY {
    _maxY = maxY;
    self.maximoAutomatico = NO;
}
- (void)setMinY:(CGFloat)minY {
    _minY = minY;
    self.minimoAutomatico = NO;
}
- (void)setSaltoX:(NSInteger)saltoX {
    _saltoX = saltoX;
    self.saltoXAutomatico = NO;
}
- (void)setSaltoY:(NSInteger)saltoY {
    _saltoY = saltoY;
    self.saltoYAutomatico = NO;
}

- (CGFloat) distanciaEntrePunto:(CGPoint) p1 yPunto:(CGPoint) p2 {
    CGFloat xDist = (p2.x - p1.x);
    CGFloat yDist = (p2.y - p1.y);
    CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
    return distance;
}

- (NSIndexPath *) buscaPuntoEnPosicion:(CGPoint) location {
    CGFloat distancia = MAXFLOAT;
    int s = 0;
    int p = 0;
    NSIndexPath *indexPathPunto;
    for (NSArray* sets in self.puntos) {
        for (NSValue *puntoValue in sets) {
            if(puntoValue!=(id)[NSNull null]) {
                CGPoint punto = puntoValue.CGPointValue;
                CGFloat dist = [self distanciaEntrePunto:location yPunto:punto];
                if(dist<distancia) {
                    distancia = dist;
                    indexPathPunto = [NSIndexPath indexPathForRow:p inSection:s];
                }
            }
            p+=1;
        }
        s+=1;
        p = 0;
    }
    
    if(distancia<20)
        return indexPathPunto;
    return nil;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    location = CGPointMake(location.x, self.bounds.size.height-location.y);
    NSIndexPath* indexPath = [self buscaPuntoEnPosicion:location];
    if(indexPath) {
        NSNumber* valor = [[self.dataSource graficaDeLineas:self valoresDeSet:indexPath.section] objectAtIndex:indexPath.row];
        NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setMaximumFractionDigits:2];
        NSString* valorstr = [NSString stringWithFormat:@"%@ %@", [formatter stringFromNumber:valor], self.prefijoEtiquetasY];
        NSValue* punto = [[self.puntos objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [self.delegate grafica:self dadoValor:valorstr enIndexPath:indexPath posicion:CGPointMake(self.frame.origin.x + punto.CGPointValue.x, self.frame.origin.y + self.bounds.size.height-punto.CGPointValue.y)];
    }
}


- (void) setDataSource:(id<GraficaLineasDatasource>)dataSource {
    _dataSource = dataSource;
}

- (void) configura {
    self.drawingRect = self.bounds;
    self.tamEtiquetasX = CGSizeZero;
    self.tamEtiquetasY = CGSizeZero;
    self.numValores = 0;
    
    if([self.delegate respondsToSelector:@selector(tipografiaEtiquetas)])
        self.fontEtiqueta = [self.delegate tipografiaEtiquetas];
    else
        self.fontEtiqueta = [UIFont fontWithName:@"Helvetica" size:10.0];
    
    if([self.delegate respondsToSelector:@selector(colorEtiquetas)])
        self.colorEtiqueta = [self.delegate colorEtiquetas];
    else
        self.colorEtiqueta = [UIColor blackColor];
    
    if([self.delegate respondsToSelector:@selector(grafica:dadoValor:enIndexPath:posicion:)])
        self.userInteractionEnabled = YES;
    else
        self.userInteractionEnabled = NO;
    
    //Calcula max y min
    float valorMax = -MAXFLOAT;
    float valorMin = MAXFLOAT;
    for (int s = 0; s<[self.dataSource numeroDeSetsEnGraficaDeLineas:self]; s++) {
        NSArray* valores = [self.dataSource graficaDeLineas:self valoresDeSet:s];
        if(self.numValores!=0) {
            if(self.numValores!=valores.count)
                [NSException raise:@"ERROR: No todos los sets tienen el mismo número de valores." format:nil];
        } else {
            self.numValores = [self.dataSource graficaDeLineas:self valoresDeSet:s].count;
        }
        
        /*/
         NSNumber* maxEnSet = [valores valueForKeyPath:@"@max.self"];
         valorMax = MAX(valorMax, maxEnSet.floatValue);
         NSNumber* minEnSet = [valores valueForKeyPath:@"@min.self"];
         valorMin = MIN(valorMin, minEnSet.floatValue);
         /*/
        for (NSNumber* valor in valores) {
            if(valor!=(id)[NSNull null]) {
                valorMax = MAX(valorMax, valor.floatValue);
                valorMin = MIN(valorMin, valor.floatValue);
            }
        }
        
        if(valorMax==-MAXFLOAT)
            valorMax = 1.0;
        if(valorMin==MAXFLOAT)
            valorMin = 0.0;
        //*/
    }
    
    if(self.maximoAutomatico) {
        if(ceil(valorMax)==valorMax)
            _maxY = valorMax+1;
        else
            _maxY = ceil(valorMax);
    }
    if(self.minimoAutomatico) {
        if(ceil(valorMin)==valorMin)
            _minY = valorMin-1;
        else
            _minY = floor(valorMin);
    }
    
    //Calcula tamaño etiquetas X
    if(self.dibujaEtiquetasX) {
        
        NSString* etiquetaMasLarga;
        if([self.dataSource respondsToSelector:@selector(etiquetasDeGraficaDeLineas:)]) {
            
            NSArray* etiquetas = [self.dataSource etiquetasDeGraficaDeLineas:self];
            NSNumber* maxLongitud = [etiquetas valueForKeyPath:@"@max.length"];
            
            etiquetaMasLarga = @"";
            for (int c = 0; c<maxLongitud.intValue; c++) {
                etiquetaMasLarga = [etiquetaMasLarga stringByAppendingString:@"#"];
            }
        } else {
            etiquetaMasLarga = [NSString stringWithFormat:@"%d", self.numValores];
        }
        
        //TODO esto es iOS 7
        self.tamEtiquetasX = [etiquetaMasLarga sizeWithAttributes:@{NSFontAttributeName:self.fontEtiqueta}];
    }
    
    
    //Calcula tamaño etiquetas Y
    if(self.dibujaEtiquetasY) {
        
        NSString* etiquetaMasLarga = @"";
        for (int s = 0; s<[self.dataSource numeroDeSetsEnGraficaDeLineas:self]; s++) {
            NSString* strMaxValor = [NSString stringWithFormat:@"%.0f %@", valorMax, self.prefijoEtiquetasY];
            NSString* strMinValor = [NSString stringWithFormat:@"%.0f %@", valorMin, self.prefijoEtiquetasY];
            if(strMaxValor.length>strMinValor.length) {
                if(strMaxValor.length>etiquetaMasLarga.length)
                    etiquetaMasLarga = strMaxValor;
            } else {
                if(strMinValor.length>etiquetaMasLarga.length)
                    etiquetaMasLarga = strMinValor;
            }
        }
        
        
        //TODO esto es iOS 7
        self.tamEtiquetasY = [etiquetaMasLarga sizeWithAttributes:@{NSFontAttributeName:self.fontEtiqueta}];
    }
    
    self.tamEtiquetasY = CGSizeMake(self.tamEtiquetasY.width+espaciadoEtiquetasY, self.tamEtiquetasY.height);
    self.tamEtiquetasX = CGSizeMake(self.tamEtiquetasX.width, self.tamEtiquetasX.height+espaciadoEtiquetasX);
    
    
    //CALCULA el salto Y
    if(self.saltoYAutomatico) {
        CGFloat altura = self.bounds.size.height-self.graphInset.height*2;
        altura = altura-self.tamEtiquetasX.height-self.tamEtiquetasY.height/2;
        int numEtiquetas = altura / (self.tamEtiquetasY.height + espaciadoEtiquetasX*2);
        _saltoY = (self.maxY-self.minY)/numEtiquetas;
        
        NSArray* saltos = @[@1, @2, @5, @10, @20, @50, @100, @250, @500, @1000, @5000, @10000, @50000, @100000];
        for (int s=saltos.count-2; s>0; s--) {
            int salto = ((NSNumber *)[saltos objectAtIndex:s]).intValue;
            if(_saltoY>salto) {
                _saltoY = ((NSNumber *)[saltos objectAtIndex:s+1]).intValue;
                break;
            }
        }
        
        
        if(self.saltoY<1)
            self.saltoY = 1;
    }
    
    //CALCULA el salto X
    if(self.saltoXAutomatico) {
        CGFloat ancho = self.bounds.size.width-self.graphInset.width*2;
        ancho = ancho-self.tamEtiquetasY.width-self.tamEtiquetasX.width/2;
        int numEtiquetas = ancho / (self.tamEtiquetasX.width + espaciadoEtiquetasX*2);
        _saltoX = self.numValores/numEtiquetas;
        
        NSArray* saltos = @[@1, @2, @5, @10, @20, @50, @100, @250, @500, @1000, @5000, @10000, @50000, @100000];
        for (int s=saltos.count-2; s>0; s--) {
            int salto = ((NSNumber *)[saltos objectAtIndex:s]).intValue;
            if(self.saltoX>salto) {
                _saltoX = ((NSNumber *)[saltos objectAtIndex:s+1]).intValue;
                break;
            }
        }
        
        
        if(self.saltoX<1)
            _saltoX = 1;
    }
    
    //Genera puntos
    self.drawingRect = CGRectInset(self.bounds, self.graphInset.width, self.graphInset.height);
    
    self.drawingRect = CGRectMake(self.drawingRect.origin.x+self.tamEtiquetasY.width,
                                  self.drawingRect.origin.y+self.tamEtiquetasX.height,
                                  self.drawingRect.size.width-self.tamEtiquetasY.width-self.tamEtiquetasX.width/2,
                                  self.drawingRect.size.height-self.tamEtiquetasX.height-self.tamEtiquetasY.height/2);
    
    NSMutableArray* auxPuntos = [[NSMutableArray alloc] init];
    for (int l = 0; l<[self.dataSource numeroDeSetsEnGraficaDeLineas:self]; l++) {
        NSArray* valores = [self.dataSource graficaDeLineas:self valoresDeSet:l];
        
        NSMutableArray* puntosDeSet = [[NSMutableArray alloc] init];
        CGFloat deltaX = (self.drawingRect.size.width)/(valores.count-1);
        for (int p = 0; p<valores.count; p++) {
            NSNumber * valor = [valores objectAtIndex:p];
            if(valor == (id)[NSNull null])
                [puntosDeSet addObject:[NSNull null]];
            else {
                CGPoint normalizado = CGPointMake(p, (valor.floatValue-self.minY)/(self.maxY-self.minY));
                
                CGFloat coordenada_x = self.drawingRect.origin.x+normalizado.x*deltaX+self.ejes.left;
                CGFloat coordenada_y = self.drawingRect.origin.y+normalizado.y*(self.drawingRect.size.height);
                
                [puntosDeSet addObject:[NSValue valueWithCGPoint:CGPointMake(coordenada_x, coordenada_y)]];
            }
        }
        
        [auxPuntos addObject:puntosDeSet.copy];
    }
    
    self.puntos = auxPuntos.copy;
}

- (void) dibuja {
    [self configura];
    [self setNeedsDisplay];
}

- (void) dibujaEtiqueta:(NSString *) etiqueta EnContexto:(CGContextRef) ctx posicion:(CGPoint) posicion {
    
    CGContextSaveGState(ctx);
    CGContextTranslateCTM(ctx, 0, self.bounds.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    
    
    //TODO esto es solo iOS 7
    CGContextSetTextDrawingMode(ctx, kCGTextFill);
    [[UIColor blackColor] setFill];
    [etiqueta drawAtPoint:CGPointMake(posicion.x, self.bounds.size.height-posicion.y)
           withAttributes:@{NSFontAttributeName:self.fontEtiqueta
                            }];
    
    CGContextRestoreGState(ctx);
}

- (void) dibujaEjesXConContext:(CGContextRef) ctx enRect:(CGRect) rect  {
    //TODO setup line con delegate
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    CGContextSetLineWidth(ctx, 0.5);
    
    NSArray* etiquetas;
    if([self.dataSource respondsToSelector:@selector(etiquetasDeGraficaDeLineas:)])
        etiquetas = [self.dataSource etiquetasDeGraficaDeLineas:self];
    
    
    CGFloat deltaX = (rect.size.width)/(self.numValores-1);
    for(int e = 0; e<self.numValores; e++) {
        if(e%self.saltoX == 0) {
            if(self.dibujaLineasX) {
                CGContextMoveToPoint(ctx, rect.origin.x+e*deltaX, rect.origin.y);
                CGContextAddLineToPoint(ctx, rect.origin.x+e*deltaX, rect.origin.y+rect.size.height);
            }
            
            if(self.dibujaEtiquetasX) {
                NSString* strEtiqueta;
                if(etiquetas)
                    strEtiqueta = [etiquetas objectAtIndex:e];
                else
                    strEtiqueta = [NSString stringWithFormat:@"%d", e];
                CGSize sizeEtiqueta = [strEtiqueta sizeWithAttributes:@{NSFontAttributeName:self.fontEtiqueta}];
                CGPoint posicion = CGPointMake(rect.origin.x+deltaX*e-sizeEtiqueta.width/2, rect.origin.y-espaciadoEtiquetasX);
                [self dibujaEtiqueta:strEtiqueta EnContexto:ctx posicion:CGPointMake(round(posicion.x), round(posicion.y))];
            }
        }
    }
    CGContextStrokePath(ctx);
}

- (void) dibujaEjesYConContext:(CGContextRef) ctx enRect:(CGRect) rect  {
    //TODO setup line con delegate
    CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
    CGContextSetLineWidth(ctx, 0.5);
    
    
    float numValores = (self.maxY-self.minY);
    CGFloat deltaY = (rect.size.height)/numValores;
    int p = 0;
    for(int e = self.minY; e<=self.maxY; e++) {
        if(e%self.saltoY == 0) {
            
            if(self.dibujaLineasY) {
                CGContextMoveToPoint(ctx, rect.origin.x, rect.origin.y + p*deltaY);
                CGContextAddLineToPoint(ctx, rect.origin.x + rect.size.width, rect.origin.y + p*deltaY);
            }
            
            if(self.dibujaEtiquetasY) {
                NSString* strEtiqueta = [NSString stringWithFormat:@"%d %@", e, self.prefijoEtiquetasY];
                CGSize sizeEtiqueta = [strEtiqueta sizeWithAttributes:@{NSFontAttributeName:self.fontEtiqueta}];
                CGPoint posicion = CGPointMake(rect.origin.x-espaciadoEtiquetasY-sizeEtiqueta.width, rect.origin.y+deltaY*p+sizeEtiqueta.height/2);
                
                [self dibujaEtiqueta:strEtiqueta EnContexto:ctx posicion:CGPointMake(round(posicion.x), round(posicion.y))];
            }
        }
        p++;
    }
    CGContextStrokePath(ctx);
}

- (void) dibujaBordesConContext:(CGContextRef) ctx enRect:(CGRect) rect  {
    //TODO setup line con delegate
    CGContextSetStrokeColorWithColor(ctx, [UIColor blackColor].CGColor);
    
    if(self.ejes.top) {
        CGContextSetLineWidth(ctx, self.ejes.top);
        CGContextMoveToPoint(ctx, rect.origin.x, rect.origin.y+rect.size.height);
        CGContextAddLineToPoint(ctx, rect.origin.x+rect.size.width, rect.origin.y+rect.size.height);
        CGContextStrokePath(ctx);
    }
    if(self.ejes.bottom) {
        CGContextSetLineWidth(ctx, self.ejes.bottom);
        CGContextMoveToPoint(ctx, rect.origin.x, rect.origin.y);
        CGContextAddLineToPoint(ctx, rect.origin.x+rect.size.width, rect.origin.y);
        CGContextStrokePath(ctx);
    }
    if(self.ejes.left) {
        CGContextSetLineWidth(ctx, self.ejes.left);
        CGContextMoveToPoint(ctx, rect.origin.x, rect.origin.y);
        CGContextAddLineToPoint(ctx, rect.origin.x, rect.origin.y+rect.size.height);
        CGContextStrokePath(ctx);
    }
    if(self.ejes.right) {
        CGContextSetLineWidth(ctx, self.ejes.right);
        CGContextMoveToPoint(ctx, rect.origin.x+rect.size.width, rect.origin.y);
        CGContextAddLineToPoint(ctx, rect.origin.x+rect.size.width, rect.origin.y+rect.size.height);
        CGContextStrokePath(ctx);
    }
}



- (void)drawRect:(CGRect)rect {
    if(!_dataSource)
        return;
    
    if(self.numValores<=1)
        return;
    
    rect = self.drawingRect;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, self.bounds.size.height);
    CGContextConcatCTM(ctx, flipVertical);
    
    //Dibuja eje x
    [self dibujaEjesXConContext:ctx enRect:rect];
    
    //Dibuja eje y
    [self dibujaEjesYConContext:ctx enRect:rect];
    
    [self dibujaBordesConContext:ctx enRect:rect];
    
    //SEPARADORES
    if ([self.dataSource respondsToSelector:@selector(separadoresDeGraficaDeLineas:)]) {
        if([self.delegate respondsToSelector:@selector(grafica:configuraSeparadoresEnContexto:)])
            [self.delegate grafica:self configuraSeparadoresEnContexto:ctx];
        else {
            CGContextSetStrokeColorWithColor(ctx, [UIColor blueColor].CGColor);
            CGContextSetLineWidth(ctx, 2.0);
        }
        
        NSArray* separadores = [self.dataSource separadoresDeGraficaDeLineas:self];
        NSArray* puntos = [self.puntos objectAtIndex:0];
        for (NSNumber* separador in separadores) {
            NSValue* puntoValue = [puntos objectAtIndex:separador.intValue];
            if(puntoValue!=(id)[NSNull null]) {
                CGPoint punto = puntoValue.CGPointValue;
                CGContextMoveToPoint(ctx, punto.x, rect.origin.y);
                CGContextAddLineToPoint(ctx, punto.x, rect.origin.y+rect.size.height);
            }
        }
        CGContextStrokePath(ctx);
    }
    
    //Dibuja Lineas y Relleno
    for (int l = 0; l<[self.dataSource numeroDeSetsEnGraficaDeLineas:self]; l++) {
        NSArray* valores = [self.dataSource graficaDeLineas:self valoresDeSet:l];
        NSArray* puntos = [self.puntos objectAtIndex:l];
        
        CGContextMoveToPoint(ctx, 0.0, 0.0);
        CGContextSetFillColorWithColor(ctx, [UIColor clearColor].CGColor);
        CGContextSetStrokeColorWithColor(ctx, [UIColor blackColor].CGColor);
        if([self.delegate respondsToSelector:@selector(grafica:configuraSet:enContexto:)])
            [self.delegate grafica:self configuraSet:l enContexto:ctx];
        
        
        //RELLENO
        BOOL inicial = YES;
        CGPoint punto;
        for (int p = 0; p<self.numValores; p++) {
            NSValue* puntoValue = [puntos objectAtIndex:p];
            if(puntoValue!=(id)[NSNull null]) {
                punto = puntoValue.CGPointValue;
                if(inicial) {
                    inicial = NO;
                    CGContextMoveToPoint(ctx, punto.x, rect.origin.y);
                    CGContextAddLineToPoint(ctx, punto.x, punto.y);
                } else
                    CGContextAddLineToPoint(ctx, punto.x, punto.y);
            }
        }
        
        if(!inicial) {
            CGContextAddLineToPoint(ctx, punto.x, rect.origin.y);
            CGContextAddLineToPoint(ctx, rect.origin.x, rect.origin.y);
            // CGContextSetBlendMode(ctx, kCGBlendModeColor);
            CGContextFillPath(ctx);
        }
        
        CGContextSetBlendMode(ctx, kCGBlendModeNormal);
        //LÍNEAS
        inicial = YES;
        for (NSValue* punto in puntos) {
            if(punto!=(id)[NSNull null]) {
                CGPoint pto = punto.CGPointValue;
                if(inicial) {
                    inicial = NO;
                    CGContextMoveToPoint(ctx, pto.x, pto.y);
                } else
                    CGContextAddLineToPoint(ctx, pto.x, pto.y);
            }
        }
        
        CGContextStrokePath(ctx);
        
        
        
        //DIBUJA PUNTOS
        int p = 0;
        for (NSValue* punto in puntos) {
            if(punto!=(id)[NSNull null]) {
                NSNumber* valor =  [valores objectAtIndex:p];
                CGPoint pto = punto.CGPointValue;
                
                if([self.delegate respondsToSelector:@selector(grafica:dibujaPunto:deSet:conValor:enContexto:)]) {
                    [self.delegate grafica:self dibujaPunto:pto deSet:l conValor:valor enContexto:ctx];
                } else {
                    /*
                    CGContextSetFillColorWithColor(ctx, [UIColor clearColor].CGColor);
                    CGContextAddEllipseInRect(ctx, CGRectMake(pto.x-4, pto.y-4, 8, 8));
                    CGContextDrawPath(ctx, kCGPathFill);
                     */
                }
            }
            p++;
        }
    }
}


@end