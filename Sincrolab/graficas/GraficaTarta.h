//
//  GraficaTarta.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GraficaTarta;

@protocol GraficaTartaDelegate <NSObject>
@optional
- (void) configuraValor:(NSInteger) numSet enContexto:(CGContextRef) context;
@end

@protocol GraficaTartaDatasource <NSObject>
- (int) numeroDeValoresEnGraficaDeTarta:(GraficaTarta *) grafica;
- (NSNumber *) graficaDeTarta:(GraficaTarta *) grafica valorParaIndice:(NSInteger) indice;
@optional
- (NSString *) graficaDeTarta:(GraficaTarta *) grafica etiquetaParaIndice:(NSInteger) indice;
@end

@interface GraficaTarta : UIView
@property (nonatomic, weak) id<GraficaTartaDelegate> delegate;
@property (nonatomic, weak) id<GraficaTartaDatasource> dataSource;

@property (nonatomic, strong) UIFont* labelFont;

- (void) reloadData;
@end
