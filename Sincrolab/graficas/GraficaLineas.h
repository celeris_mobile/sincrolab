//
//  GraficasLineas.h
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef struct GFBordesWidth {
    CGFloat top;
    CGFloat bottom;
    CGFloat left;
    CGFloat right;
} GFBordesWidth;
static inline GFBordesWidth
GFBordesWidthMake(float top, float bottom, float left, float right) {
    GFBordesWidth bw;
    bw.top = top;
    bw.bottom = bottom;
    bw.left = left;
    bw.right = right;
    return bw;
}

@class GraficaLineas;

@protocol GraficaLineasDelegate <NSObject>
@optional
- (void) grafica:(GraficaLineas *) grafica dibujaPunto:(CGPoint) punto deSet:(NSInteger) numSet conValor:(NSNumber *) valor enContexto:(CGContextRef) context;
- (void) grafica:(GraficaLineas *) grafica configuraSet:(NSInteger) numSet enContexto:(CGContextRef) context;
- (UIFont *) tipografiaEtiquetas;
- (UIColor *) colorEtiquetas;
- (void)grafica:(GraficaLineas *) grafica dadoValor:(NSString *) valor enIndexPath:(NSIndexPath *)indexPath posicion:(CGPoint) punto;
- (void)grafica:(GraficaLineas *)grafica configuraSeparadoresEnContexto:(CGContextRef)context;
@end

@protocol GraficaLineasDatasource <NSObject>
- (int) numeroDeSetsEnGraficaDeLineas:(GraficaLineas *) grafica;
- (NSArray *)graficaDeLineas:(GraficaLineas *)grafica valoresDeSet:(NSInteger)numSet;
@optional
- (NSArray *)etiquetasDeGraficaDeLineas:(GraficaLineas *)grafica;
- (NSArray *)separadoresDeGraficaDeLineas:(GraficaLineas *)grafica;
@end

@interface GraficaLineas : UIView

@property (nonatomic, weak) id<GraficaLineasDelegate> delegate;
@property (nonatomic, weak) id<GraficaLineasDatasource> dataSource;
@property (nonatomic, assign) BOOL dibujaLineasX;
@property (nonatomic, assign) BOOL dibujaLineasY;
@property (nonatomic, assign) BOOL dibujaEtiquetasX;
@property (nonatomic, assign) BOOL dibujaEtiquetasY;
@property (nonatomic, assign) NSString* prefijoEtiquetasY;
@property (nonatomic, assign) GFBordesWidth ejes;
@property (nonatomic, assign) NSInteger saltoY;
@property (nonatomic, assign) NSInteger saltoX;
@property (nonatomic, assign) CGFloat minY;
@property (nonatomic, assign) CGFloat maxY;

- (void) dibuja;
@end
