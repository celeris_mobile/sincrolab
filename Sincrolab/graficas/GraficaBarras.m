//
//  GraficaBarras.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaBarras.h"

@interface GraficaBarras()
@property (nonatomic, strong) UIView* etiquetas;

@property (nonatomic, readonly) int numValores;
@property (nonatomic, readonly) int valorInicial;
@property (nonatomic, readonly) int valorFinal;
@property (nonatomic, readonly) CGFloat valorMinY;
@property (nonatomic, readonly) CGFloat valorMaxY;
@end

@implementation GraficaBarras {
    CGFloat min_y;
    CGFloat max_y;
    
    int min_x;
    int max_x;
    
    int indiceInicio;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.graphInset = CGSizeZero;
    
    min_y = 0.0;
    max_y = 0.0;
    
    min_x = 0;
    max_x = 0;
    
    self.labelFont = [UIFont fontWithName:@"Lato-Regular" size:9.0];
}

//pragma mark GETTERS
- (int) numValores {
    return [self.dataSource numeroDeValoresEnGraficaDeBarras:self];
}

- (int) valorInicial {
    return 0;
}

- (int) valorFinal {
    return [self.dataSource numeroDeValoresEnGraficaDeBarras:self];
}

- (CGFloat) valorMinY {

    return min_y;
}

- (CGFloat) valorMaxY {
    return max_y;
}

//pragma mark SETTERS
- (void) setLimites:(CGRect) limitesRect {
    min_y = limitesRect.origin.y;
    max_y = limitesRect.size.height;
}

- (void) setDataSource:(id<GraficaBarrasDatasource>)dataSource {
    _dataSource = dataSource;
  //  [self load];
}

- (void) load {
    [self setNeedsDisplay];
    [self dibujaEtiquetas];        
}

- (void) reloadData {
    [self setNeedsDisplay];
    [self dibujaEtiquetas];    
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* etiqueta in self.etiquetas.subviews) {
            [etiqueta removeFromSuperview];
        }
        [self.etiquetas removeFromSuperview];
        self.etiquetas = nil;
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    self.etiquetas.backgroundColor = [UIColor clearColor];
    self.etiquetas.userInteractionEnabled = NO;
    [self addSubview:self.etiquetas];
    
    NSString* etiqueta;
    CGFloat maxwidth = CGFLOAT_MIN;
    int vfinal = self.valorFinal;
    for (int e = self.valorInicial; e<vfinal; e++) {
        etiqueta = [self.dataSource graficaDeBarras:self etiquetaParaIndice:e];

        CGSize tam = [etiqueta sizeWithAttributes:@{NSFontAttributeName:self.labelFont}];
        maxwidth = MAX(maxwidth, tam.width);
    }
    
    
    UILabel* anterior;
    CGFloat delta = (self.frame.size.height-self.graphInset.height)/self.numValores;
    for (int e = self.valorInicial; e<vfinal; e++) {
        etiqueta = [self.dataSource graficaDeBarras:self etiquetaParaIndice:e];
        
        CGPoint posicion = CGPointMake(-maxwidth, delta*(e-self.valorInicial) + self.graphInset.height-(delta/2-7));
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectIntegral(CGRectMake((int)posicion.x, (int)posicion.y, maxwidth, 15))];

        label.textAlignment = NSTextAlignmentRight;
        label.font = self.labelFont;
        label.textColor = [UIColor darkGrayColor];
        label.text = etiqueta;
        
        if(!anterior||!CGRectIntersectsRect(label.frame, anterior.frame)) {
            [self.etiquetas addSubview:label];
            anterior = label;
        }
    }
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
   // CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, self.bounds.size.height);
   // CGContextConcatCTM(ctx, flipVertical);
    
    /*/
    UIColor* color = [UIColor colorWithRed:1.0 green:1.0 blue:0.0 alpha:0.3];
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    CGContextFillRect(ctx, CGRectInset(self.bounds, self.graphInset.width, self.graphInset.height));
    //*/
    
    CGFloat delta = (self.frame.size.height - self.graphInset.height)/(self.numValores);
    
    //Dibuja eje x
    UIColor * colorBarra = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    CGContextSetFillColorWithColor(ctx, colorBarra.CGColor);
    CGContextSetLineWidth(ctx, 0.5);
    int vfinal = self.valorFinal;
    for (int e = self.valorInicial; e<vfinal; e++) {
        double valor = round(self.graphInset.height + delta*(e-self.valorInicial));
        CGContextMoveToPoint(ctx, self.graphInset.width , valor);
        CGContextAddLineToPoint(ctx, self.bounds.size.width-self.graphInset.width, valor);
        CGContextAddLineToPoint(ctx, self.bounds.size.width-self.graphInset.width, valor+10);
        CGContextAddLineToPoint(ctx, self.graphInset.width, valor+10);
    }
    CGContextFillPath(ctx);
        
    //Dibuja líneas
    int e = 0;
    for (int p = self.valorInicial; p<vfinal; p++) {
        if([self.delegate respondsToSelector:@selector(configuraValor:enContexto:)])
            [self.delegate configuraValor:p enContexto:ctx];
        
        NSNumber * valor = [self.dataSource graficaDeBarras:self valorParaIndice:p];
        
        CGFloat normalizado = (valor.floatValue-self.valorMinY)/(self.valorMaxY-self.valorMinY);
        
        double posy = round(self.graphInset.height + delta*(e-self.valorInicial));
        CGContextMoveToPoint(ctx, self.graphInset.width , posy);
        CGContextAddLineToPoint(ctx, self.graphInset.width + (self.bounds.size.width-2*self.graphInset.width)*normalizado, posy);
        CGContextAddLineToPoint(ctx, self.graphInset.width + (self.bounds.size.width-2*self.graphInset.width)*normalizado, posy+10);
        CGContextAddLineToPoint(ctx, self.graphInset.width, posy+10);
        CGContextFillPath(ctx);

        e++;
    }
    
}


@end
