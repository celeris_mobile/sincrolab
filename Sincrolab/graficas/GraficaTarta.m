//
//  GraficaTarta.m
//  Sincrolab
//
//  Created by Ricardo Sánchez Sotres on 12/10/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "GraficaTarta.h"

@interface GraficaTarta()
@property (nonatomic, strong) UIView* etiquetas;

@property (nonatomic, readonly) int numValores;
@property (nonatomic, readonly) int valorInicial;
@property (nonatomic, readonly) int valorFinal;
@end

@implementation GraficaTarta {
    int min_x;
    int max_x;
    
    int indiceInicio;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    
    self.backgroundColor = [UIColor clearColor];
    
    min_x = 0;
    max_x = 0;
    
    self.labelFont = [UIFont fontWithName:@"Lato-Regular" size:9.0];
}

//pragma mark GETTERS
- (int) numValores {
    return [self.dataSource numeroDeValoresEnGraficaDeTarta:self];
}

- (int) valorInicial {
    return 0;
}

- (int) valorFinal {
    return [self.dataSource numeroDeValoresEnGraficaDeTarta:self];
}

//pragma mark SETTERS
- (void) setDataSource:(id<GraficaTartaDatasource>)dataSource {
    _dataSource = dataSource;
    [self load];
}

- (void) load {
    [self dibujaEtiquetas];
    [self setNeedsDisplay];
}

- (void) reloadData {
    [self dibujaEtiquetas];    
    [self setNeedsDisplay];
}

- (void) dibujaEtiquetas {
    if(self.etiquetas) {
        for (UIView* etiqueta in self.etiquetas.subviews) {
            [etiqueta removeFromSuperview];
        }
        [self.etiquetas removeFromSuperview];
        self.etiquetas = nil;
    }
    
    self.etiquetas = [[UIView alloc] initWithFrame:self.bounds];
    self.etiquetas.backgroundColor = [UIColor clearColor];
    self.etiquetas.userInteractionEnabled = NO;
    [self addSubview:self.etiquetas];
    
    NSString* etiqueta;
    CGFloat maxwidth = CGFLOAT_MIN;
    for (int e = self.valorInicial; e<self.valorFinal; e++) {
        NSNumber* valor = [self.dataSource graficaDeTarta:self valorParaIndice:e];
        etiqueta = [NSString stringWithFormat:@"%d%%", valor.intValue];
        
        
        CGSize tam = [etiqueta sizeWithAttributes:@{NSFontAttributeName:self.labelFont}];
        
        maxwidth = MAX(maxwidth, tam.width);
    }
    
    CGFloat angulo_ant = 0.0;
    for (int e = self.valorInicial; e<self.valorFinal; e++) {
        
        NSNumber* valor = [self.dataSource graficaDeTarta:self valorParaIndice:e];
        if(valor.intValue==0)
            continue;
        
        etiqueta = [NSString stringWithFormat:@"%d%%", valor.intValue];
        
        
        CGFloat normalizado = (valor.floatValue/100.0) * 360.0;
        CGFloat radianes = -normalizado*M_PI/180;
        CGFloat angulo = angulo_ant + radianes/2;

        angulo_ant += radianes;
        
        CGPoint centro = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        CGFloat radio = 2*self.bounds.size.width/5;
        
        CGPoint posicion = CGPointMake(centro.x  + radio * cos(angulo), centro.y  + radio * sin(angulo));
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectIntegral(CGRectMake((int)posicion.x-maxwidth/2, (int)posicion.y-7, maxwidth, 14))];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = self.labelFont;
        label.textColor = [UIColor darkGrayColor];
        label.text = etiqueta;
        
        [self.etiquetas addSubview:label];
        


    }
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGPoint centro = CGPointMake(rect.size.width/2, rect.size.height/2);
    CGFloat radio = rect.size.width/2;
    
    CGContextSetFillColorWithColor(ctx, [UIColor orangeColor].CGColor);
    CGFloat normalizado_ant = 0.0;
    for (int v = 0; v<[self.dataSource numeroDeValoresEnGraficaDeTarta:self]; v++) {
        NSNumber* valor = [self.dataSource graficaDeTarta:self valorParaIndice:v];
        
        if([self.delegate respondsToSelector:@selector(configuraValor:enContexto:)])
            [self.delegate configuraValor:v enContexto:ctx];
        
    
        CGFloat normalizado = (valor.floatValue/100.0) * 360.0;
        
        CGContextMoveToPoint(ctx, centro.x, centro.y);
        CGContextAddArc(ctx, centro.x, centro.y, radio, -normalizado_ant*M_PI/180, -(normalizado_ant+normalizado)*M_PI/180.0, 1);
        CGContextFillPath(ctx);
        
        normalizado_ant += normalizado;
    }
    
    CGColorRef blancoTransparente = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4].CGColor;
    CGContextSetFillColorWithColor(ctx, blancoTransparente);
    CGContextSetStrokeColorWithColor(ctx, [UIColor blackColor].CGColor);
    CGContextMoveToPoint(ctx, centro.x+2*radio/3, centro.y);
    CGContextAddArc(ctx, centro.x, centro.y, 2*radio/3, 0, M_PI*2, 0);
    CGContextAddLineToPoint(ctx, centro.x+radio, centro.y);
    CGContextAddArc(ctx, centro.x, centro.y, radio, 0, M_PI*2, 0);
    CGContextEOFillPath(ctx);
}


@end
