//
//  PartidaGongs.m
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 20/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "PartidaGongs.h"

@implementation PartidaGongs
@dynamic cambioNivel, sesiones;

+ (NSString *)parseClassName {
    return @"PartidaGongs";
}

- (NSNumber *)duracion {
    return [self.sesiones valueForKeyPath:@"@sum.duracion"];
}

- (NSNumber *)puntos {
    return [self.sesiones valueForKeyPath:@"@sum.puntuacion"];
}
@end
