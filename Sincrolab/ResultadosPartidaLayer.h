//
//  ResultadosSesionLayer.h
//  sincrolab
//
//  Created by Ricardo Sánchez Sotres on 23/12/13.
//  Copyright (c) 2013 Ricardo Sánchez Sotres. All rights reserved.
//

#import "CCLayer.h"
#import "PanelLayerDelegate.h"
#import "PanelLayer.h"

@interface ResultadosPartidaLayer : PanelLayer
- (void) setup;
@end
